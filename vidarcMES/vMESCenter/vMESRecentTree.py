#----------------------------------------------------------------------------
# Name:         vMESRecentTree.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20070105
# CVS-ID:       $Id: vMESRecentTree.py,v 1.3 2007/10/13 15:53:21 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import sys,traceback,copy

import vtLgBase
import cStringIO
from wx import BitmapFromImage as wxBitmapFromImage
from wx import ImageFromStream as wxImageFromStream
from wx import EmptyIcon as wxEmptyIcon

def getPluginImage():
    return None

def getApplicationIcon():
    return None
    
def create(parent,fldBar):
    pass

import images as imgMES

class vMESRecentTree(wx.TreeCtrl):
    def __init__(self,parent,id=-1,pos=wx.DefaultPosition,size=wx.Size(-1, 200),style=0,name=u'trRecent'):
        wx.TreeCtrl.__init__(self,id=id,
              name=name, parent=parent, pos=pos,
              size=size, style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT|wx.TR_LINES_AT_ROOT)
        self.Bind(wx.EVT_LEFT_DCLICK, self.OnTrRecentLeftDclick)
        self.Bind(wx.EVT_RIGHT_DOWN, self.OnTrRecentRightDown)
        self.func=None
        self.args=()
        self.kwargs={}
        
        self.dTiGrp={}
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        img=imgMES.getModuleBitmap()
        self.imgDict['root']=self.imgLstTyp.Add(img)
        self.imgDict['mod']=self.imgLstTyp.Add(img)
        img=imgMES.getGroupingBitmap()
        self.imgDict['grp']=self.imgLstTyp.Add(img)
        dLg={}
        for lg,bmp in [('en',imgMES.getLangEnBitmap()),
                    ('de',imgMES.getLangDeBitmap()),
                    ('fr',imgMES.getLangFrBitmap()),
                    ('it',imgMES.getLangItBitmap()),
                    ('ne',imgMES.getLangNeBitmap()),
                    ('se',imgMES.getLangSeBitmap()),]:
            dLg[lg]=self.imgLstTyp.Add(bmp)
        self.imgDict['lang']=dLg
        self.SetImageList(self.imgLstTyp)
        self.AddRoot(_(u'modules'))
        self.oCryptoStore=None
        self.bAdvanced=False
    def AddPlugIn(self,oPlugIn):
        try:
            sName=oPlugIn.GetName()
            bmp=oPlugIn.GetBitmap()
            if bmp is not None:
                self.imgDict[sName]=self.imgLstTyp.Add(bmp)
            lImg=oPlugIn.GetDomainImageDataLst()
            if lImg is not None:
                for sName,imgDataFunc in lImg:
                    if sName not in self.imgDict:
                        stream = cStringIO.StringIO(imgDataFunc())
                        img=wxImageFromStream(stream)
                        bmp=wxBitmapFromImage(img)
                        self.imgDict[sName]=self.imgLstTyp.Add(bmp)
        except:
            sys.stderr.write(traceback.format_exc())
    def SetAdvanced(self,flag):
        self.bAdvanced=flag
    def SetLaunchFunc(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def Clear(self):
        self.oCryptoStore=None
        self.dTiGrp={}
        tp=self.GetRootItem()
        self.DeleteChildren(tp)
    def SetLoginStorage(self,oCryptoStore):
        self.oCryptoStore=oCryptoStore
    def SetRecent(self,dPlugIn,dRecent):
        try:
            lang=vtLgBase.getApplLang()
            self.Clear()
            tip=self.GetRootItem()
            keys=dPlugIn.keys()
            #keys.sort()
            for k in keys:
                tup=dPlugIn[k]
                if k not in dRecent:
                    continue
                try:
                    plugin=tup[2]
                except:
                    continue
                if plugin is None:
                    continue
                sName=plugin.GetName()
                if sName in self.imgDict:
                    lTrans=plugin.GetDomainTransLst(lang)
                    lImgs=plugin.GetDomainImageDataLst()
                    if lImgs is not None:
                        ti=tip
                        for i in xrange(len(lTrans)):
                            sGrpName='.'.join([lImgs[j][0] for j in xrange(i+1)])
                            if sGrpName in self.dTiGrp:
                                ti=self.dTiGrp[sGrpName]
                            else:
                                sNameImg=lImgs[i][0]
                                if sNameImg in self.imgDict:
                                    img=self.imgDict[sNameImg]
                                else:
                                    img=self.imgDict[sName]
                                tidd=wx.TreeItemData()
                                ti=self.AppendItem(ti,lTrans[i],img,img,tidd)
                                self.dTiGrp[sGrpName]=ti
                    else:
                        img=self.imgDict[sName]
                        tidd=wx.TreeItemData()
                        ti=self.AppendItem(tip,'.'.join(lTrans),img,img,tidd)
                    l=dRecent[k]
                    bFirst=True
                    for sConn in l:
                        i=sConn.find(':')
                        j=sConn.rfind('@')
                        if i>0 and j>0:
                            if self.bAdvanced:
                                s=sConn
                            else:
                                s=sConn[j+1:]+' '+sConn[:i]
                            tic=self.AppendItem(ti,s,-1,-1,None)
                            tid=wx.TreeItemData()
                            tid.SetData((k,plugin,tup[0],sConn,sConn[j+1:],sConn[:i]))
                            self.SetItemData(tic,tid)
                            if bFirst:
                                bFirst=None
                                tid=wx.TreeItemData()
                                tid.SetData((k,plugin,tup[0],sConn,sConn[j+1:],sConn[:i]))
                                self.SetItemData(ti,tid)
        except:
            sys.stderr.write(traceback.format_exc())
        tip=self.GetRootItem()
        self.SortChildren(tip)
    def OnTrRecentLeftDclick(self, evt):
        try:
            #if self.oCryptoStore.acquire('check',blocking=False)==False:
            #    vtLog.vtLngCurWX(vtLog.WARN,'acquire lock not possible yet',self)
            #    return
            #sys.stderr.write(lang+'\n')
            ti=self.GetSelection()
            tup=self.GetPyData(ti)
            if tup is not None:
                oPlugIn=tup[1]
                #self.winCount+=1
                #l=self.__getPluginNameLst__(tup[2])
                dLaunch={'plugIn':oPlugIn}
                dLaunch['lstNames']=oPlugIn.GetDomainNameLst()
                dLaunch['connection']=copy.deepcopy(tup[3])
                if self.func is not None:
                    self.func(dLaunch,oPlugIn,*self.args,**self.kwargs)
                #try:
                #    self.qLaunch.put(dLaunch,False)
                #    self.PrintMsg(_('schedule plugin %s for launch.')%(tid.GetName()))
                #except:
                #    self.PrintMsg(_('schedule plugin %s not possible yet.')%(tid.GetName()))
            else:
                evt.Skip()
        except:
            sys.stderr.write(traceback.format_exc())
        #try:
        #    self.oCryptoStore.release('check')
        #except:
        #    sys.stderr.write(traceback.format_exc())
    def OnTrRecentRightDown(self, event):
        try:
            if self.oCryptoStore is None:
                return
            ti=self.GetSelection()
            tip=self.GetItemParent(ti)
            if tip!=self.GetRootItem():
                tup=self.GetPyData(ti)
                if self.oCryptoStore.acquire('check',blocking=False):
                    try:
                        self.oCryptoStore.DelLoginConn(tup[0],tup[3])
                    except:
                        pass
                    self.oCryptoStore.release('check')
                self.Delete(ti)
        except:
            sys.stderr.write(traceback.format_exc())

