#----------------------------------------------------------------------------
# Name:         vRingBuffer.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20060804
# CVS-ID:       $Id: vRingBuffer.py,v 1.1 2006/08/29 10:58:38 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vRingBuffer:
    def __init__(self,size):
        self.iSize=size
        self.iAct=self.iSize-1
        self.lSlot=[None]*self.iSize
    def put(self,t):
        self.iAct= (self.iAct+1) % self.iSize
        self.lSlot[self.iAct]=t
    def get(self,iPos=0):
        iPos=iPos % self.iSize
        iAct=self.iAct
        for i in xrange(0,iPos):
            iAct-=1
            if iAct<0:
                iAct+=self.iSize
        return self.lSlot[iAct]
    def proc(self,func,*args,**kwargs):
        iAct=self.iAct
        for i in xrange(0,self.iSize):
            func(self.lSlot[iAct],*args,**kwargs)
            iAct-=1
            if iAct<0:
                iAct+=self.iSize
        
