#----------------------------------------------------------------------------
# Name:         vMESplugin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      200060209
# CVS-ID:       $Id: vMESplugin.py,v 1.6 2007/11/17 10:42:35 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

try:
    import vidarc.tool.lang.vtLgBase as vtLgBase
except:
    pass

try:
    import vidarc.tool.log.vtLog as vtLog
    LOGGING=1
except:
    LOGGING=0
    import sys,traceback

try:
    VID_INIT=1
except:
    VID_INIT=0

try:
    import cStringIO
    from wx import BitmapFromImage as wxBitmapFromImage
    from wx import ImageFromStream as wxImageFromStream
    from wx import EmptyIcon as wxEmptyIcon
    GUI=1
except:
    GUI=0


class vMESplugin:
    def __init__(self,name,bFrame,dn,imgData=None):
        self.name=name
        self.bFrame=bFrame
        self.dn=dn
        self.domain=''
        self.plugin=None
        self.pluginInit=None
        self.objClass=None
        self.img=None
        self.imgData=imgData
        self.lDomains=None
        self.lDomainsTrans=None
        self.lDomainsImg=None
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'dn:%s'%(dn),self.name)
            self.bLogging=True
        except:
            self.bLogging=False
    def GetWinName(self):
        strs=self.name.split('.')
        return strs[-1]
    def GetName(self):
        return self.name
    def IsFrame(self):
        return self.bFrame
    def GetDN(self):
        return self.dn
    def GetImage(self):
        try:
            if self.img is None:
                if self.imgData is None:
                    self.__importDomainInfos__()
                    self.GetDomainImageDataLst()
                    if self.lDomainsImg[-1][1] is not None:
                        self.imgData=self.lDomainsImg[-1][1]
                        stream = cStringIO.StringIO(self.imgData())
                        self.img=wxImageFromStream(stream)
                    else:
                        self.__import__()
                else:
                    stream = cStringIO.StringIO(self.imgData())
                    self.img=wxImageFromStream(stream)
            return self.img
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
    def GetBitmap(self):
        try:
            if GUI:
                img=self.GetImage()
                if img is None:
                    return None
                return wxBitmapFromImage(img)
            else:
                return None
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
        return None
    def GetIcon(self):
        icon = wxEmptyIcon()
        icon.CopyFromBitmap(self.GetBitmap())
        return icon
    def GetDomainNameLst(self):
        self.__importDomainInfos__()
        return self.lDomains
    def GetDomainTransLst(self,lang):
        if self.lDomainsTrans is not None:
            return self.lDomainsTrans
        self.__importDomainInfos__()
        if self.pluginInit is not None:
            try:
                if hasattr(self.pluginInit,'getDomainTransLst'):
                    self.lDomainsTrans=getattr(self.pluginInit,'getDomainTransLst')(lang)
                else:
                    import vidarc.__init__ as vInit
                    self.lDomainsTrans=vInit.getDomainTrans(self.lDomains,lang)
            except:
                if self.bLogging:
                    vtLog.vtLngTB(self.name)
                else:
                    sys.stderr.write(traceback.format_exc())
            if self.lDomainsTrans is not None:
                if len(self.lDomainsTrans)>0:
                    return self.lDomainsTrans
        return self.lDomains
    def GetDomainImageDataLst(self):
        if self.lDomainsImg is not None:
            if self.lDomainsImg[-1][1] is None:
                return None
            return self.lDomainsImg
        self.__importDomainInfos__()
        if self.pluginInit is not None:
            try:
                if hasattr(self.pluginInit,'getDomainImageData'):
                    self.lDomainsImg=getattr(self.pluginInit,'getDomainImageData')()
                else:
                    import vidarc.__init__ as vInit
                    if hasattr(self.pluginInit,'getPluginData'):
                        vInit.checkAndAddDomainImageDataFunc(self.lDomains[-1],getattr(self.pluginInit,'getPluginData'))
                    self.lDomainsImg=vInit.getDomainImageData(self.lDomains)
            except:
                if self.bLogging:
                    vtLog.vtLngTB(self.name)
                else:
                    sys.stderr.write(traceback.format_exc())
        if self.lDomainsImg is None:
            self.lDomainsImg=[(self.name,None)]
            return None
        elif len(self.lDomainsImg)==0:
            self.lDomainsImg=[(self.name,None)]
            return None
        return self.lDomainsImg
    def __importDomainInfos__(self):
        try:
            if self.lDomains is None:
                strs=self.name.split('.')
                self.pluginInit=__import__('.'.join(strs[:-1]),globals(),locals(),['__init__'])
                if hasattr(self.pluginInit,'DOMAINS'):
                    self.lDomains=getattr(self.pluginInit,'DOMAINS')
                else:
                    self.pluginInit=None
                    self.__import__()
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
            else:
                sys.stderr.write(traceback.format_exc())
            self.__import__()
    def __import__(self):
        try:
            if self.objClass is None:
                strs=self.name.split('.')
                self.domain=strs[-2]
                try:
                    self.plugin=__import__(self.name,[],[],[strs[-1],'getPluginImage',
                            'getCfgNames','DOMAINS','getDomainTransLst',
                            'getDomainImageData'])
                    if self.pluginInit is None:
                        self.pluginInit=self.plugin
                    self.objClass=getattr(self.plugin,strs[-1])
                    self.img=getattr(self.plugin,'getPluginImage')()
                    if hasattr(self.plugin,'DOMAINS'):
                        self.lDomains=getattr(self.plugin,'DOMAINS')
                    else:
                        self.lDomains=[self.domain]
                except:
                    if self.bLogging:
                        vtLog.vtLngTB(self.name)
                    else:
                        sys.stderr.write(traceback.format_exc())
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
            else:
                sys.stderr.write(traceback.format_exc())
    def create(self,parent,id,pos,size,style,name):
        try:
            self.__import__()
            if self.bFrame<=100:
                try:
                    parent.SetIcon(self.GetIcon())
                except:
                    if self.bLogging:
                        vtLog.vtLngTB(self.name)
            return self.objClass(parent,id,pos,size,style,name)
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
            else:
                sys.stderr.write(traceback.format_exc())
        return None
    def createFrame(self,parent,id,pos,size,style,name):
        try:
            self.__import__()
            return self.objClass(parent,id,pos,size,style,name)
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
        return None
    def getPossibleLang(self):
        try:
            return vtLgBase.getPossibleLang(self.domain,self.dn)
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
            return []
    def setLang(self,lang):
        try:
            vtLgBase.setPluginLang(self.domain,self.dn,lang)
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.name)
