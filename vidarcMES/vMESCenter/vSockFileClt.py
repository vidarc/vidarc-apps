#----------------------------------------------------------------------------
# Name:         vSockFileClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060627
# CVS-ID:       $Id: vSockFileClt.py,v 1.4 2008/02/05 17:07:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import thread,traceback
import time,random
import socket,sha,binascii,zipfile
from VidMap import VidMap
import vSystem

import sys,os,os.path,sha
from vSock import *
import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe

BUFFER_MAX=40*1024*1024
VERBOSE_TRANS=0

class vSockFileClt(vSock):
    SSL=__init__.SSL
    def __init__(self,server,conn,adr,sID='',verbose=0):
        vSock.__init__(self,server,conn,adr,sID,BUFFER_MAX,verbose=verbose)
        try:
            if self.SSL:
               self.conn.do_handshake()
               pass
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        self.bEstabilshed=False
        self.loggedin=False
        self.iStart=-1
        self.iAct=0
        self.iEnd=-1
        self.encoded_passwd=False
        self.verbose=verbose
        self.cmds=[
            #('lock_node',self.__lock_node__),
            #('unlock_node',self.__unlock_node__),
            #('getFile',self.__getFile__),
            ('getFileStart',self.__getFileStart__),
            ('getFileProc',self.__getFileProc__),
            ('getFileFin',self.__getFileFin__),
            ('getFileAbort',self.__getFileAbort__),
            ('listContent',self.__listContent__),
            ('alias',self.__alias__),
            ('login',self.__login__),
            ('loggedin',self.__loggedin__),
            ('authenticate',self.__authenticate__),
            ('browse',self.__browse__),
            (u'sessionID',self.__sessionID__),
            (u'shutdown',self.__shutdown__)]
        self.appl_alias=''
        self.appl=''
        self.par=None
        self.dFileGet={}
        
        self.sLocaleDN=vSystem.getPlugInDN()
        self.sHelpDN=vSystem.getPlugInHelpDN()
    def DoVerify(self,conn, cert, errnum, depth, ok):
        #traceback.print_stack()
        #vtLog.vtLngCur(vtLog.INFO,'ok:%d'%(ok),self.appl_alias)
        if ok:
            self.serving=True
            #vtLog.PrintMsg(_('alias:%s SSL got certificate: %s')%(self.appl_alias,cert.get_subject()),self.par)
        else:
            self.serving=False
            #vtLog.vtLngCur(vtLog.ERROR,'errnum:%d'%(errnum),self.appl_alias)
            #vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self.appl_alias)
        if self.serving==False:
            #vtLog.PrintMsg(_('alias:%s SSL verify fault')%(self.appl_alias),self.par)
            #if self.par is not None:
            #    self.par.NotifyConnectionFault()
            pass
    def CheckPause(self):
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
                if self.par is not None:
                    self.par.NotifyPause(self.paused)
        except:
            traceback.print_exc()
    def IsLoggedIn(self):
        return self.loggedin
    def HandleData(self):
        try:
            if self.iStart==-1:
                self.iStart=self.data.find(self.startMarker,self.iAct)
                #self.iAct=self.iStart   # 060225
                #self.iStart+=self.iStartMarker
                self.iAct=self.iStart+self.iStartMarker
            if self.iStart>=0:
                # start marker found
                try:
                    self.iEnd=self.data.find(self.endMarker,self.iAct)
                except:
                    pass
                #self.iAct=self.len
                if self.iEnd>0:
                    # end marker found, telegram is complete
                    self.iStart+=self.iStartMarker
                    val=self.data[self.iStart:self.iEnd]
                    for cmd in self.cmds:
                        k=cmd[0]
                        iLen=len(k)
                        if val[:iLen]==k:
                            #cmd[1](val[iLen:].encode('ISO-8859-1'))
                            cmd[1](val[iLen:])
                            break
                    self.iEnd+=self.iEndMarker
                    #self.data=self.data[self.iEnd:]   # 060225
                    #self.len-=self.iEnd               # 060225 
                    #self.iStart=self.iEnd=self.iAct=-1  # 060225
                    self.iAct=self.iEnd
                    self.iStart=self.iEnd=-1
                    if self.iAct>=self.len:
                        self.data=''
                        self.len=0
                        self.iAct=0
                        return 0
                    return 1
                else:
                    self.data=self.data[self.iStart:]
                    self.iAct=0
                    self.len-=self.iStart
                    self.iStart=-1
        except:
            traceback.print_exc()
        return 0
    def SetParent(self,par):
        self.par=par
    def SocketClosed(self):
        if self.par is not None:
            self.par.__connectionClosed__(self.bEstabilshed)
    def Stop(self):
        vSock.Stop(self)
    def IsRunning(self):
        return vSock.IsRunning(self)
    def Login(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=False
        self.bLoginSend=False
    def LoginEncoded(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=True
        self.bLoginSend=False
    def __sessionID__(self,val):
        self.sessionID=val[1:]
        self.__doLogin__()
    def __doLogin__(self):
        if len(self.passwd)>0:
            if self.encoded_passwd:
                sPasswd=self.passwd
            else:
                m=sha.new()
                m.update(self.passwd)
                sPasswd=m.hexdigest()
            m=sha.new()
            m.update(sPasswd)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        else:
            s=''
        self.SendTelegram('login|'+self.usr+','+s)
        self.bLoginSend=True
    def __login__(self,val):
        if self.bLoginSend==False:
            self.__doLogin__()
        else:
            self.par.NotifyLogin()
    def __loggedin__(self,val):
        self.loggedin=True
        try:
            self.par.NotifyLoggedIn(self.loggedin)
        except:
            traceback.print_exc()
    def __authenticate__(self,val):
        pass
    def __browse__(self,val):
        self.par.NotifyBrowse(val[1:].split(';'))
    def ShutDown(self):
        pass
    def Browse(self,appl):
        self.SendTelegram('browse|'+appl)
    def Authenticate(self,id,pubKey):
        self.SendTelegram('authenticate|'+','.join([id,pubKey]))
    def __getFileStart__(self,val):
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                sFN=val[i+1:]
                self.par.NotifyFileGetStart(sFN)
                return
        except:
            traceback.print_exc()
    def __getFileProc__(self,val):
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    iFlt=2
                    k=val.find(',',j+1)
                    if k>0:
                        iFlt=3
                        l=val.find(',',k+1)
                        
                        sFN=val[i+1:j]
                        iAct=long(val[j+1:k])
                        iSize=long(val[k+1:l])
                        blk=val[l+1:]
                        d=self.dFileGet[sFN]
                        d['file'].write(blk)
                        d['sha'].update(blk)
                        if VERBOSE_TRANS:
                            sys.stderr.write('%-40s,%08d,%08d,%s\n'%(sFN,iAct,iSize,d['sha'].hexdigest()))
                        self.par.NotifyFileGetProc(sFN,iAct,iSize)
                return
        except:
            traceback.print_exc()
    def __getFileFin__(self,val):
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                iFlt=1
                j=val.find(',',i+1)
                if j>0:
                    sFN=val[i+1:j]
                    dig=val[j+1:]
                    d=self.dFileGet[sFN]
                    d['file'].flush()
                    d['file'].close()
                    #sys.stderr.write(d['FN']+'\n'+d['sha'].hexdigest()+'\n'+dig+'\n\n')
                    if d['sha'].hexdigest()!=dig:
                        os.remove(d['tmpFN'])
                        self.par.NotifyFileGetAbort(sFN,True)
                    else:
                        bVerifyOk=False
                        #sys.stderr.write(d['FN']+'\n')
                        if d['FN'][-7:]=='.vidarc':
                            #sys.stderr.write('   verify recieved\n')
                            #sFN=os.path.join(self.path,f)
                            path,s=os.path.split(d['tmpFN'])
                            try:
                                fmFile=VidMap(d['tmpFN'],path)
                                fZip=zipfile.ZipFile(fmFile,'r')
                                fZip.testzip()
                                for n in fZip.namelist():
                                    try:
                                        if n.startswith('locale/'):
                                            sSaveFN=os.path.join(self.sLocaleDN,n)
                                        elif n.startswith('help/'):
                                            sSaveFN=os.path.join(self.sHelpDN,n)
                                        else:
                                            continue
                                        dn,fn=os.path.split(sSaveFN)
                                        if os.path.exists(dn)==False:
                                            os.makedirs(dn)
                                        fExp=open(sSaveFN,'w+b')
                                        fExp.write(fZip.read(n))
                                        fExp.close()
                                    except:
                                        sys.stderr.write(time.strftime('%Y-%m-%d %H:%M:%S ',time.localtime(time.time()))+d['FN']+'\n')
                                        sys.stderr.write('   extraction fault:%s\n',n)
                                        traceback.print_exc(file=sys.stderr)
                                fZip.close()
                                fmFile.close()
                                bVerifyOk=True
                                if os.path.exists(d['FN']):
                                    os.remove(d['FN'])
                                os.rename(d['tmpFN'],d['FN'])
                                self.par.NotifyFileGetFin(sFN)
                            except:
                                sys.stderr.write(time.strftime('%Y-%m-%d %H:%M:%S ',time.localtime(time.time()))+d['FN']+'\n')
                                sys.stderr.write('   verify fault\n')
                                traceback.print_exc(file=sys.stderr)
                                #sys.stderr.write(traceback.format_exc())
                                self.par.NotifyFileGetAbort(sFN,True)
                        else:
                            if os.path.exists(d['FN']):
                                os.remove(d['FN'])
                            os.rename(d['tmpFN'],d['FN'])
                            self.par.NotifyFileGetFin(sFN)
                            
                        #if os.path.exists(d['FN']):
                        #    os.remove(d['FN'])
                        #os.rename(d['tmpFN'],d['FN'])
                        #self.par.NotifyFileGetFin(sFN)
                    del self.dFileGet[sFN]
                return
        except:
            traceback.print_exc()
    def __getFileAbort__(self,val):
        try:
            i=val.find('|')
            iFlt=0
            if i>=0:
                j=val.find(',',i+1)
                if j>0:
                    sFN=val[i+1:j]
                    d=self.dFileGet[sFN]
                    d['file'].close()
                    os.remove(d['tmpFN'])
                    del self.dFileGet[sFN]
                    self.par.NotifyFileGetAbort(sFN)
                    return
        except:
            traceback.print_exc()
    def __listContent__(self,val):
        i=val.find(',',1)
        if i>0:
            appl_alias=val[1:i]
            self.par.NotifyListContent(appl_alias,val[i+1:].split(';'))
    def GetFile(self,appl,sDN,sFN):
        if sFN in self.dFileGet:
            return -1
        try:
            os.makedirs(sDN)
        except:
            pass
        sTmpFN=os.path.join(sDN,sFN)+'.net'
        f=open(sTmpFN,'wb')
        self.dFileGet[sFN]={'file':f,'sha':sha.new(),'FN':os.path.join(sDN,sFN),'tmpFN':sTmpFN}
        self.SendTelegram('getFile|'+','.join([appl,sFN]))
    def ListContent(self,appl):
        self.SendTelegram('listContent|'+appl)
    def __alias__(self,val):
        i=val.find('|')
        if i>=0:
            try:
                if val[i+1:]=='served':
                    # notify served
                    if self.par is not None:
                        self.par.NotifyServed(True)
                else:
                    if self.par is not None:
                        self.par.NotifyServed(False)
            except:
                pass
    def Alias(self,appl,alias):
        self.appl=appl
        self.appl_alias=':'.join([appl,alias])
        self.SendTelegram('alias|'+self.appl_alias)
    def __shutdown__(self,val):
        pass

