#!/usr/bin/env python

#----------------------------------------------------------------------------
# Name:         vMESCenterAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vMESCenterAppl_prof_full.py,v 1.1 2007/01/14 14:29:13 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

#from time import time
import time
import threading
import sys
import os
from collections import deque
try:
    from resource import getrusage, RUSAGE_SELF
except ImportError:
    RUSAGE_SELF = 0
    def getrusage(who=0):
        return [0.0, 0.0] # on non-UNIX platforms cpu_time always 0.0

p_stats = None
p_start_time = None
p_file_callstack = None
p_file_profile = None

def profiler(frame, event, arg):
    if event not in ('call','return'): return profiler
    #### gather stats ####
    rusage = getrusage(RUSAGE_SELF)
    t_cpu = rusage[0] + rusage[1] # user time + system time
    code = frame.f_code 
    fun = (code.co_name, code.co_filename, code.co_firstlineno)
    #print event,fun
    #### get stack with functions entry stats ####
    ct = threading.currentThread()
    try:
        p_stack = ct.p_stack
    except AttributeError:
        ct.p_stack = deque()
        p_stack = ct.p_stack
    #### handle call and return ####
    if event == 'call':
        p_stack.append((time.time(), t_cpu, fun))
    elif event == 'return':
        try:
            t,t_cpu_prev,f = p_stack.pop()
            assert f == fun
        except IndexError: # TODO investigate
            t,t_cpu_prev,f = p_start_time, 0.0, None
        call_cnt, t_sum, t_cpu_sum = p_stats.get(fun, (0, 0.0, 0.0))
        p_stats[fun] = (call_cnt+1, t_sum+time.time()-t, t_cpu_sum+t_cpu-t_cpu_prev)
    if p_file_callstack is not None:
        if event == 'call':
            act_frame=frame
            sNow=time.strftime('%Y-%m-%d %H:%M:%S|CS|DEBUG|',time.localtime(time.time()))
            sFN=os.path.split(code.co_filename)
            p_file_callstack.write(sNow)
            p_file_callstack.write('%s;%s;fn:%s(%04d)|'%(event,
                    code.co_name, sFN, code.co_firstlineno))
            #i=0
            #print event
            while act_frame is not None:
                code=act_frame.f_code
                sFN=os.path.split(code.co_filename)
                p_file_callstack.write('%s:%s(%d),'%(sFN,code.co_name,code.co_firstlineno))
                #print '    %03d name:%-30s fn:%-80s ln:%04d'%(i,code.co_name,code.co_filename,code.co_firstlineno)
                act_frame=act_frame.f_back
                #i+=1
            #print
            p_file_callstack.write('\n')
    return profiler


def profile_on(fn):
    global p_stats, p_start_time , p_file_callstack , p_file_profile
    sNow=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
    try:
        # search enviroment
        sLogDN=os.getenv('vidarcLogDN',None)
    except:
        sLogDN=None
    sCallstackFN='%s.%s.cs.log'%(fn,sNow)
    sProfFN='%s_%s.prof'%(fn,sNow)
    if sLogDN is not None:
        try:
            os.makedirs(sLogDN)
        except:
            pass
        lngCallstackFN=os.path.join(sLogDN,sCallstackFN)
        lngProfFN=os.path.join(sLogDN,sProfFN)
    else:
        lngCallstackFN=sCallstackFN
        lngProfFN=sProfFN
    try:
        p_file_callstack=open(lngCallstackFN,'w')
    except:
        p_file_callstack=None
    try:
        p_file_profile=open(lngProfFN,'w')
    except:
        p_file_profile=None
    p_stats = {}
    p_start_time = time.time()
    threading.setprofile(profiler)
    sys.setprofile(profiler)


def profile_off():
    threading.setprofile(None)
    sys.setprofile(None)

def get_profile_stats():
    """
    returns dict[function_tuple] -> stats_tuple
    where
      function_tuple = (function_name, filename, lineno)
      stats_tuple = (call_cnt, real_time, cpu_time)
    """
    return p_stats


if 1:
    fn='vMESCenterAppl.py'
    profile_on(fn)
    #import hotshot,hotshot.stats
    #import sys,os
    
    #prof = hotshot.Profile("vMESCenter.prof")
    #sys.path.insert(0, os.path.dirname(fn))
    execfile(fn)
    
    profile_off()

    if p_file_callstack is not None:
        p_file_callstack.close()
    #from pprint import pprint
    #pprint(get_profile_stats())
    if p_file_profile is not None:
        for tFunc,tStat in get_profile_stats().items():
            p_file_profile.write('%s;%s;%d;call:%d;zReal:%d;zCPU:%d'%(tFunc[1],
                tFunc[0],tFunc[2],tStat[0],tStat[1],tStat[2]))
            p_file_profile.write('\n')
        p_file_profile.close()
else:
    import profile
    fn='vMESCenterAppl.py'
    prof = profile.run('execfile(%r)'%fn)
