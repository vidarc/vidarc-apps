#----------------------------------------------------------------------------
# Name:         vSockClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vSockClt.py,v 1.2 2006/08/29 10:06:21 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread
import time
import socket
# Echo client program
import os
import sys,traceback
#traceback.print_stack(sys.stderr)
import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe
    def verify_cb(conn, cert, errnum, depth, ok):
        self=conn.get_app_data()
        self.DoVerify(conn,cert,errnum,depth,ok)
        return ok


class vSockClt:
    SSL=__init__.SSL
    def __init__(self,par,host,port,socketClass,verbose=0):
        self.par=par
        self.host=host
        self.port=port
        self.serving=False
        self.connecting=False
        self.stopping=False
        self.connected=False
        self.pausing=False
        self.paused=False
        self.sock=None
        self.socketClass=socketClass
        self.connections={}
        self.verbose=verbose
    def __initSSL__(self,privkey=None,cert=None,verify=None,DN=None):
        try:
            traceback.print_stack()
            if self.SSL:
                if DN is None:
                    DN=os.getcwd()
                if privkey is None:
                    privkey='vMESClt.pkey'
                if cert is None:
                    cert='vMESClt.cert'
                if verify is None:
                    verify='CA.cert'
                try:
                    DNcert=os.path.split(sys.argv[0])[0]
                except:
                    DNcert=DN
                sys.stderr.write('\n%s'%('\n'.join(traceback.format_stack())))
                sys.stderr.write('\nappl:%s\ndnCert:%s\ndn:%s %d\n'%(sys.argv[0],DNcert,os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
                sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
                sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, cert),os.path.exists(os.path.join(DN, cert))))
                sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DNcert, verify),os.path.exists(os.path.join(DNcert, verify))))
                
                self.ctx = SSL.Context(SSL.SSLv23_METHOD)
                self.ctx.set_options(SSL.OP_NO_SSLv2)
                self.ctx.set_verify(SSL.VERIFY_PEER|SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
                self.ctx.use_privatekey_file (os.path.join(DN, privkey))
                self.ctx.use_certificate_file(os.path.join(DN, cert))
                self.ctx.load_verify_locations(os.path.join(DNcert, verify))
            else:
                self.ctx=None
        except:
            traceback.print_exc()
            self.ctx=None
    def DoVerify(self,conn, cert, errnum, depth, ok):
        #traceback.print_stack(sys.stderr)
        if ok:
            self.serving=True
        else:
            self.serving=False
    def GetSocketInstance(self):
        return self.sock
    def Start(self):
        if self.serving:
            return
        if self.stopping:
            return
        self.serving=True
        self.stopping=False
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.stopping=True
        while self.connecting:
            time.sleep(1)
        if self.sock is not None:
            self.sock.Stop()
    def Pause(self,flag):
        self.pausing=flag
        if self.sock is not None:
            self.sock.Pause(flag)
    def IsRunning(self):
        return False
    def IsServing(self):
        if self.connecting:
            return True
        if self.sock is not None:
            if self.sock.IsRunning():
                if self.sock.paused:
                    return False
                return True
        else:
            return False
        return self.serving
    def StopSocket(self,adr,socket):
        try:
            lst=self.connections[adr]
            lst.remove(socket)
        except:
            pass
    def Connect(self):
        self.connecting=True
        thread.start_new_thread(self.RunConnect, ())
    def RunConnect(self):
        self.connected=False
        self.socket = None
        try:
            for res in socket.getaddrinfo(self.host, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM):
                af, socktype, proto, canonname, sa = res
                try:
                    if self.SSL:
                        self.__initSSL__()
                        self.socket = tsafe.Connection(self.ctx,socket.socket(af, socktype, proto))
                        #self.socket.set_app_data(self)
                    else:
                        self.socket = socket.socket(af, socktype, proto)
                except socket.error, msg:
                    print msg
                    print traceback.print_exc()
                    self.socket = None
                    continue
                try:
                    self.socket.connect(sa)
                except socket.error, msg:
                    self.socket.close()
                    self.socket = None
                    continue
                break
        except:
            # address fault
            pass
        if self.socket is None:
            self.par.NotifyConnectionFault()
            self.connecting=False
            return
        try:
            self.sock=self.socketClass(self,self.socket,None,verbose=self.verbose)
            if self.par is not None:
                self.par.NotifyConnected()
        except:
            traceback.print_exc()
            
        self.connected=True
        self.connecting=False
        #return 0
    def Run(self):
        while self.serving:
            if self.stopping:
                self.serving=False
                break
            self.socket.send(self.data)
            data = self.socket.recv(1024)
            
            self.socket.close()
    def Send(self,str):
        if self.connected:
            self.sock.Send(str)
    def Close(self):
        if self.sock is not None:
            self.sock.Close()
