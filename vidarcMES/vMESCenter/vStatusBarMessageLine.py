#----------------------------------------------------------------------------
# Name:         vStatusBarMessageLine.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060417
# CVS-ID:       $Id: vStatusBarMessageLine.py,v 1.5 2007/10/15 21:30:22 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from wx import PyTimer
import wx
import Queue,time,sys,threading,time
import vRingBuffer

wxEVT_V_STATUSBAR_PRINT=wx.NewEventType()
vEVT_V_STATUSBAR_PRINT=wx.PyEventBinder(wxEVT_V_STATUSBAR_PRINT,1)
def EVT_V_STATUSBAR_PRINT(win,func):
    win.Connect(-1,-1,wxEVT_V_STATUSBAR_PRINT,func)
def EVT_V_STATUSBAR_PRINT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_V_STATUSBAR_PRINT,func)
class vStatusBarPrint(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_V_STATUSBAR_PRINT(<widget_name>, self.OnProc)
    """
    def __init__(self,iPos,sVal):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_V_STATUSBAR_PRINT)
        self.iPos=iPos
        self.sVal=sVal
    def GetPos(self):
        return self.iPos
    def GetVal(self):
        return self.sVal

class vStatusBarMessageLineTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        #self.cbCancel = wx.BitmapButton(id=-1,
        #      bitmap=images.getCancelBitmap(), name=u'cbCancel',
        #      parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        #self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
        #      self.cbCancel)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        id=wx.NewId()
        self.lstMsg = wx.ListCtrl(id=id, name=u'lstStatusMsg',
              parent=self, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=wx.LC_REPORT)
        #self.lstMsg.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstBitsListColClick,id=id)
        #self.lstMsg.Bind(wx.EVT_LIST_ITEM_DESELECTED,self.OnLstBitsListItemDeselected, id=id)
        #self.lstMsg.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnLstBitsListItemSelected, id=id)
        self.lstMsg.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('message'), width=280)
        self.lstMsg.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('time'), width=120)
        self.selIdx=-1
        bxs.AddWindow(self.lstMsg, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.SetSizer(bxs)
        self.Layout()
        
        self.szOrig=(240,140)
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        iWCol=self.lstMsg.GetColumnWidth(1)
        self.lstMsg.SetColumnWidth(0,iW-iWCol-20)
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def AddMsg(self,sMsg,sTime,iPos=sys.maxint):
        if sMsg is None:
            self.lstMsg.DeleteAllItems()
        else:
            idx=self.lstMsg.InsertStringItem(iPos,sMsg)
            self.lstMsg.SetStringItem(idx,1,sTime)

class vStatusBarMessageLine:
    def __init__(self,par,iNum,zClrTime):
        self.sb=par
        EVT_V_STATUSBAR_PRINT(self.sb,self._doPrintMsg)
        self.popWinSb=None
        self.iNum=iNum
        self.qMsg=Queue.Queue()
        self.zClrTime=zClrTime
        if zClrTime>0:
            self.zMsgTimer = PyTimer(self.NotifyClrMsgLine)
            self.rbMsg=None
        else:
            self.rbMsg=vRingBuffer.vRingBuffer(100)
            self.semStatusBar=threading.Semaphore()
    def _doPrintMsg(self,evt):
        self.sb.SetStatusText(evt.GetVal(), evt.GetPos())
    def PostPrint(self,iPos,sVal):
        wx.PostEvent(self.sb,vStatusBarPrint(iPos,sVal))
    def PrintMsg(self,s,bForce=False,bImmediately=False):
        """ put a message string into queue for delayed display on status bar. this method is thread safe.
        """
        # 061009 wro
        # attention do only add information to queue, never ever set statusbar info here!!!
        # this method may be called by threads, therefore a possible wx-crash may be cased!!! 
        #return
        if bForce:
            self.PostPrint(self.iNum,s)
        if 0 and bImmediately:
            try:
                self.sb.SetStatusText(s, self.iNum)
            except:
                pass
        if self.zClrTime>0:
            self.zMsgTimer.Start(self.zClrTime,True)
            self.PostPrint(self.iNum,s)
        else:
            self.semStatusBar.acquire()
            try:
                tup=(s,time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())))
                self.rbMsg.put(tup)
                self.qMsg.put(tup)
            except:
                pass
            self.semStatusBar.release()
    def GetPrintMsgTup(self):
        if self.qMsg.empty():
            return None
        else:
            return self.qMsg.get()
    def NotifyClrMsgLine(self):
        self.sb.SetStatusText('', self.iNum)
    def __createPopupStatusBar__(self):
        if self.popWinSb is None:
            sz=self.sb.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWinSb=vStatusBarMessageLineTransientPopup(self,sz,wx.SIMPLE_BORDER)
            except:
                pass
        else:
            pass
    def ShowPopupStatusBar(self):
        self.__createPopupStatusBar__()
        if self.popWinSb.IsShown()==False:
            btn=self.sb
            rect = self.sb.GetFieldRect(self.iNum)
            iX,iY = btn.ClientToScreen( (0,0) )
            iDX,iDY = rect.x+2, rect.y+2
            iX+=iDX
            #iY+=iDY
            iW,iH =  rect.width-4, rect.height-4
            iPopW,iPopH=self.popWinSb.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY-=iPopH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWinSb.Move((iX,iY))
            self.popWinSb.SetSize((iW,50))
            self.popWinSb.Show(True)
            return True
        else:
            self.popWinSb.Show(False)
            return False
    def IsShownPopupStatusBar(self):
        if self.popWinSb is None:
            return False
        return self.popWinSb.IsShown()
    def AddMsg2PopupStatusBar(self,tup,iPos=sys.maxint):
        if self.popWinSb is not None:
            if tup is None:
                return
            sMsg,sTime=tup[0],tup[1]
            self.popWinSb.AddMsg(sMsg,sTime,iPos)

