#----------------------------------------------------------------------------
# Name:         vMESpluginFind.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      200060209
# CVS-ID:       $Id: vMESpluginFind.py,v 1.14 2008/03/22 14:39:36 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,os.path,sys
import string,thread,threading,time
import traceback,types

try:
    import vidarc.tool.log.vtLog as vtLog
except:
    pass
    
try:
    import vidarc.tool.time.vtTimeLoadBound as vtLdBd
    LOAD_BOUND=True
except:
    LOAD_BOUND=False
    
wxEVT_VMESPLUGIN_THREAD_PROC=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_PROC=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_PROC,1)
def EVT_VMESPLUGIN_THREAD_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_PROC,func)
class vMESpluginThreadProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_PROC(<widget_name>, self.OnProc)
    """

    def __init__(self,obj,iAct,iSize):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.iAct=iAct
        self.iSize=iSize
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_PROC)
    def GetAct(self):
        return self.iAct
    def GetSize(self):
        return self.iSize

wxEVT_VMESPLUGIN_THREAD_ADD=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD,1)
def EVT_VMESPLUGIN_THREAD_ADD(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD,func)
class vMESpluginThreadAdd(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD(<widget_name>, self.OnPlugInAdd)
    """

    def __init__(self,obj,name,bFrame,dn,img=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.name=name
        self.bFrame=bFrame
        self.dn=dn
        self.img=img
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD)
    def GetName(self):
        return self.name
    def IsFrame(self):
        return self.bFrame
    def GetDN(self):
        return self.dn
    def GetImg(self):
        return self.img

wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD_FINISHED=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED,1)
def EVT_VMESPLUGIN_THREAD_ADD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED,func)
class vMESpluginThreadAddFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD_FINISHED(<widget_name>, self.OnPlugInFin)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED)
    
wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD_ABORTED=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED,1)
def EVT_VMESPLUGIN_THREAD_ADD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED,func)
class vMESpluginThreadAddAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD_ABORTED(<widget_name>, self.OnPlugInAbort)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED)

class thdMESpluginFind:
    DIR_2_SKIP=['CVS','locale','build','dist']
    def __init__(self,par,verbose=-1):
        self.par=par
        self.widDis=None
        self.widWait=None
        self.lDN=[]
        self.running = False
        self.verbose=verbose
        self.updateProcessbar=True
        try:
            vtLog.vtLngCurWX(vtLog.DEBUG,'',self.par)
            self.bLogging=True
        except:
            self.bLogging=False
    def Find(self,lDN,func=None,*args):
        if self.IsRunning():
            return
        self.lDN=lDN
        self.zStart=time.clock()
        self.func=func
        self.args=args
        self.Start()
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing = False
    def IsRunning(self):
        return self.running
    def __getImport__(self,fn):
        try:
            sfn=fn.replace('\\','/')
            bLower=False
            if sys.platform=='win32':
                bLower=True
                sfn=sfn.lower()
            #vtLog.vtLngCurWX(vtLog.DEBUG,fn,self.par)
            #if self.verbose>0:
            #    vtLog.CallStack('')
            for dn in sys.path:
                sdn=dn.replace('\\','/')
                if bLower:
                    sdn=sdn.lower()
                #if self.verbose>0:
                #    vtLog.vtLngCurWX(vtLog.DEBUG,'%s;%s'%(dn,fn),self.par)
                #if sys.platform=='win32':
                #    sfn=fn.lower()
                #else:
                #    sfn=fn
                if sfn.find(sdn)==0:
                    iLen=len(dn)
                    if not dn[-1] in ['/','\\']:
                        iLen+=1
                    name=fn[iLen:]
                    name=name.replace('/','.')
                    name=name.replace('\\','.')
                    return name
            vtLog.vtLngCurWX(vtLog.WARN,'fn:%s could not be resolved by means of sys.path'%(fn),self.par)
        except:
            if self.bLogging:
                vtLog.vtLngTB(self.par.GetName())
        return None
    def __findPluginVid__(self,dn):
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,dn,self.par)
            #if LOAD_BOUND:
            #    vtLdBd.check()
            #else:
            #    time.sleep(0.25)
            lFiles=os.listdir(dn)
            lFiles.sort()
            lFiles.reverse()
            lPlugIns=[]
            for fn in lFiles:
                strs=fn.split('.')
                if strs[-1]=='vidarc':
                    sPlugIn='.'.join(strs[:-4])
                    if sPlugIn not in lPlugIns:
                        lPlugIns.append(sPlugIn)
            if self.bLogging:
                vtLog.vtLngCurWX(vtLog.DEBUG,'plugins:%s'%(vtLog.pformat(lPlugIns)),self.par)
            #sys.path.insert(0,dn)
            bPlugInFound=False
            lPlugIns.sort()
            #print lPlugIns
            dPlgInVid={}
            if len(lPlugIns)>=1:
                #print lPlugIns[0]
                if lPlugIns[0]=='vidarc':
                    sPlugIn=lPlugIns[0]
                    sImp='%s.__init__'%sPlugIn
                    if self.bLogging:
                        vtLog.vtLngCurWX(vtLog.DEBUG,sImp,self.par)
                    try:
                        plgin=__import__(sImp,[],[],['PLUGINS'])
                        if hasattr(plgin,'PLUGINS'):
                            #print plgin.PLUGINS
                            for pluginTup in plgin.PLUGINS:
                                sPlugIn=pluginTup[0]
                                dPlgInVid[sPlugIn]=pluginTup
                                for lPlug in pluginTup[1]:
                                    #print lPlug
                                    sPlgInName=lPlug[0]
                                    bPlgInFrame=lPlug[1]
                                    bmpDataFunc=lPlug[2]
                                    #print bmpDataFunc
                                    wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,sPlugIn+'.'+sPlgInName,bPlgInFrame,dn,bmpDataFunc))
                                    #if LOAD_BOUND:
                                    #    vtLdBd.check()
                                    #else:
                                    #    time.sleep(0.1)
                                    #time.sleep(0.01)
                                    try:
                                        self.par.PrintMsg(_(u'plugin %s added')%sPlugIn,bForce=False)
                                    except:
                                        traceback.print_exc(file=sys.stderr)
                                try:
                                    #wx.SafeYield()
                                    time.sleep(0.05)
                                    self.iAct+=1
                                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                                except:
                                    traceback.print_exc(file=sys.stderr)
                            lPlugIns=[]
                    except:
                        pass
                for sPlugIn in lPlugIns:
                    if sPlugIn in dPlgInVid:
                        continue
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                    #if LOAD_BOUND:
                    #    vtLdBd.check()
                    #else:
                    #    time.sleep(0.25)
                    #time.sleep(0.01)
                    #wx.SafeYield()
                    time.sleep(0.05)
                    if sPlugIn.startswith('vidarc.vSrv'):
                        if self.bLogging:
                            vtLog.vtLngCurWX(vtLog.DEBUG,'skip:%s'%(sPlugIn),self.par)
                        continue
                    sImp='%s.__init__'%sPlugIn
                    if self.bLogging:
                        vtLog.vtLngCurWX(vtLog.DEBUG,sImp,self.par)
                    try:
                        plgin=__import__(sImp,[],[],['PLUGABLE'])
                        #vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s plugin:%s has __init__'%(dn,sPlugIn),self.par)
                        #if self.verbose>0:
                        #    vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(plgin.__dict__),self.par)
                        if hasattr(plgin,'PLUGABLE')==False:
                            del plgin
                            continue
                        try:
                            if type(plgin.PLUGABLE)==types.ListType:
                                lPlgInNames=plgin.PLUGABLE
                            else:
                                try:
                                    bPlgInFrame=plgin.PLUGABLE_FRAME
                                except:
                                    bPlgInFrame=False
                                lPlgInNames=[(plgin.PLUGABLE,bPlgInFrame)]
                            for sPlgInName,bPlgInFrame in lPlgInNames:
                                #sPlgInName=plgin.PLUGABLE
                                bPlugInFound=True
                                #vtLog.vtLngCurWX(vtLog.INFO,'dir:%s is PLUGABLE'%dn,self.par)
                                if len(sPlgInName):
                                    if self.bLogging:
                                        vtLog.vtLngCurWX(vtLog.DEBUG,'PLUGABLE:%s'%sPlgInName,self.par)
                                    #try:
                                    #    bPlgInFrame=plgin.PLUGABLE_FRAME
                                    #except:
                                    #    bPlgInFrame=False
                                    wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,sPlugIn+'.'+sPlgInName,bPlgInFrame,dn))
                            try:
                                self.par.PrintMsg(_(u'plugin %s added')%sPlugIn,bForce=False)
                            except:
                                traceback.print_exc(file=sys.stderr)
                        except:
                            del plgin
                            #vtLog.vtLngTB(self.par.GetName())
                            if self.bLogging:
                                #vtLog.vtLngCurWX(vtLog.WARN,'dir:%s no PLUGABLE defined'%dn,self.par)
                                vtLog.vtLngCurWX(vtLog.WARN,'dir %s;%s'%(sPlugIn,traceback.format_exc()),self.par)
                    except:
                        if self.bLogging:
                            vtLog.vtLngTB(self.par.GetName())
                #if bPlugInFound==False:
                #    sys.path=sys.path[1:]
        except:
            try:
                vtLog.vtLngTB(self.par.GetName())
                vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s no init found here'%dn,self.par)
            except:
                pass
        return -1
    def __findPluginSrc__(self,dn):
        try:
            #if LOAD_BOUND:
            #    vtLdBd.check()
            #else:
            #    time.sleep(0.25)
            sImp=self.__getImport__(dn+'.__init__')
            #vtLog.vtLngCurWX(vtLog.DEBUG,sImp,self.par)
            if sImp is not None:
                #eval('import %s as plgin'%sImg)
                try:
                    plgin=__import__(sImp,[],[],['PLUGABLE'])
                    if self.bLogging:
                        vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s has __init__'%dn,self.par)
                        if self.verbose>0:
                            vtLog.vtLngCurWX(vtLog.DEBUG,vtLog.pformat(plgin.__dict__),self.par)
                    try:
                        if type(plgin.PLUGABLE)==types.ListType:
                            lPlgInNames=plgin.PLUGABLE
                        else:
                            try:
                                bPlgInFrame=plgin.PLUGABLE_FRAME
                            except:
                                bPlgInFrame=False
                            lPlgInNames=[(plgin.PLUGABLE,bPlgInFrame)]
                        try:
                            for sPlgInName,bPlgInFrame in lPlgInNames:
                                try:
                                    vtLog.vtLngCurWX(vtLog.INFO,'dir:%s is PLUGABLE:%s;isFrame:%d'%(dn,sPlgInName,bPlgInFrame),self.par)
                                    if len(sPlgInName):
                                        #vtLog.vtLngCurWX(vtLog.DEBUG,'PLUGABLE:%s'%sPlgInName,self.par)
                                        wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,self.__getImport__(dn+'.'+sPlgInName),bPlgInFrame,dn))
                                    vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s is PLUGABLE:%s;isFrame:%d posted'%(dn,sPlgInName,bPlgInFrame),self.par)
                                except:
                                    del plgin
                                    #vtLog.vtLngTB(self.par.GetName())
                                    vtLog.vtLngCurWX(vtLog.WARN,'dir:%s no PLUGABLE defined'%dn,self.par)
                                    if self.bLogging:
                                        vtLog.vtLngTB(self.par.GetName())
                        except:
                            if self.bLogging:
                                vtLog.vtLngCurWX(vtLog.ERROR,'dir:%s import fault'%dn,self.par)
                                vtLog.vtLngTB(self.par.GetName())
                        return 0
                    except:
                        pass
                    return 0
                except:
                    if self.bLogging:
                        vtLog.vtLngCurWX(vtLog.WARN,'dir:%s no __init__'%dn,self.par)
                return -1
        except:
            try:
                vtLog.vtLngTB(self.par.GetName())
                vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s no init found here'%dn,self.par)
            except:
                pass
        return -1
    def __walkDir__(self,dn):
        try:
            if self.keepGoing==False:
                return
            iRet=self.__findPlugin__(dn)
            if iRet<0:
                if self.updateProcessbar:
                    self.iAct+=self.__walkDirCalc__(dn)
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                return
            files=os.listdir(dn)
            #vtLog.vtLngCurWX(vtLog.DEBUG,dn,self.par)
            #if self.verbose>0:
            #    vtLog.CallStack(dn)
            #    vtLog.pprint(files)
            for fn in files:
                if self.updateProcessbar:
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                sFullFN=os.path.join(dn,fn)
                if os.path.isdir(sFullFN):
                    if fn not in self.DIR_2_SKIP:
                        self.__walkDir__(sFullFN)
        except:
            #vtLog.vtLngTB(self.par.GetName())
            traceback.print_exc()
    def __walkDirCalc__(self,dn):
        try:
            iSize=0
            if self.keepGoing==False:
                return iSize
            files=os.listdir(dn)
            #if self.verbose>0:
            #    vtLog.CallStack(dn)
            #    vtLog.pprint(files)
            iSize=len(files)
            for fn in files:
                sFullFN=os.path.join(dn,fn)
                if os.path.isdir(sFullFN):
                    if fn not in self.DIR_2_SKIP:
                        iSize+=self.__walkDirCalc__(sFullFN)
        except:
            #vtLog.vtLngTB(self.par.GetName())
            traceback.print_exc()
        return iSize
    def Run(self):
        try:
            self.running = True
            if self.updateProcessbar:
                wx.PostEvent(self.par,vMESpluginThreadProc(self.par,1,100))
            if LOAD_BOUND:
                vtLdBd.check()
            else:
                time.sleep(0.05)
            
            self.lSrcDN=[]
            self.lVidDN=[]
            for dn in self.lDN:
                for root , dirs,files in os.walk(dn):
                    for dnExcl in self.DIR_2_SKIP:
                        if dnExcl in dirs:
                            dirs.remove(dnExcl)
                    for fn in files:
                        if fn.endswith('.vidarc'):
                            self.lVidDN.append(root)
                            break
                        elif fn.startswith('__init__.'):
                            self.lSrcDN.append(root)
                            break
            #print self.lSrcDN
            #print self.lVidDN
            if self.updateProcessbar:
                self.iAct=0
                self.iSize=len(self.lSrcDN)
                if len(self.lVidDN)>0:
                    self.iSize+=reduce(lambda x,y:x+y,[len(os.listdir(dn)) for dn in self.lVidDN])
                #for dn in self.lDN:
                #    self.iSize+=self.__walkDirCalc__(dn)
                wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
            for dn in self.lSrcDN:
                if self.keepGoing==False:
                    break
                if self.updateProcessbar:
                    self.iAct+=1
                self.__findPluginSrc__(dn)
            for dn in self.lVidDN:
                if self.keepGoing==False:
                    break
                if self.updateProcessbar:
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                self.__findPluginVid__(dn)
            
            #self.par.PrintMsg(_(u'plugin search ...'))
            #for dn in self.lDN:
            #    self.__walkDir__(dn)
            #self.par.PrintMsg(_(u'plugin search finished.'))
            if self.keepGoing:
                if self.func is not None:
                    self.func(*self.args[0])
                if self.updateProcessbar:
                    wx.PostEvent(self.par,vMESpluginThreadAddFinished(self.par))
            else:
                if self.updateProcessbar:
                    wx.PostEvent(self.par,vMESpluginThreadAddAborted(self.par))
        except:
            #vtLog.vtLngTB(self.par.GetName())
            traceback.print_exc()
        self.running = False
    def BindEvents(self,funcProc=None,funcAdd=None,funcFin=None,funcAbort=None):
        if self.par is None:
            return
        if funcProc:
            EVT_VMESPLUGIN_THREAD_PROC(self.par,funcProc)
        if funcAdd:
            EVT_VMESPLUGIN_THREAD_ADD(self.par,funcAdd)
        if funcFin:
            EVT_VMESPLUGIN_THREAD_ADD_FINISHED(self.par,funcFin)
        if funcAbort:
            EVT_VMESPLUGIN_THREAD_ADD_ABORTED(self.par,funcAbort)
