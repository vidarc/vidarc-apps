#----------------------------------------------------------------------------
# Name:         vSock.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vSock.py,v 1.2 2006/08/29 10:06:21 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import thread,threading
import time
import socket,binascii,types

import sys,traceback
#traceback.print_stack(sys.stderr)
import __init__
if __init__.SSL:
    #sys.stderr.write('\nimport OpenSSL\n')
    from OpenSSL import SSL
    #sys.stderr.write('\nimport OpenSSL\n')
    def verify_cb(conn, cert, errnum, depth, ok):
        self=conn.get_app_data()
        self.DoVerify(conn,cert,errnum,depth,ok)
        return ok

class vSock:
    SEND_ALL=False
    SSL=__init__.SSL
    RECV_SIZE=8192
    VERBOSE=0
    def __init__(self,server,conn,adr,sID='',max=33554432,verbose=0):  #131072
        self.srv=server
        self.conn=conn
        self.adr=adr
        self.serving=False
        self.stopping=False
        self.pausing=False
        self.paused=False
        self.data=''
        self.len=0
        self.verbose=verbose
        self.max=max
        self.iPos=0
        self.sessionID=sID
        self.startMarker='u1u1u1u1'
        self.iStartMarker=len(self.startMarker)
        self.endMarker='u0u0u0u0'
        self.iEndMarker=len(self.endMarker)
        self.received=0
        self.send=0
        self.zConn=time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        self.conn.settimeout(1.0)
        self.semSend=threading.Semaphore()
        try:
            if self.SSL:
                #traceback.print_stack(sys.stderr)
                self.HANDSHAKE_TIMEOUT=5
                self.iVerify=0
                self.ctx=self.conn.get_context()
                self.conn.set_app_data(self)
        except:
            self.SSL=False
            traceback.print_exc()
        self.Start()
    def __initSSL__(self,privkey=None,cert=None,verify=None,DN=None):
        try:
            if self.VERBOSE:
                traceback.print_stack()
            if self.SSL:
                if DN is None:
                    DN=os.getcwd()
                if privkey is None:
                    privkey='vMESClt.pkey'
                if cert is None:
                    cert='vMESClt.cert'
                if verify is None:
                    verify='CA.cert'
                try:
                    DNcert=os.path.split(sys.argv[0])[0]
                except:
                    DNcert=DN
                sys.stderr.write('\n%s'%('\n'.join(traceback.format_stack())))
                sys.stderr.write('\nappl:%s\ndnCert:%s\ndn:%s %d\n'%(sys.argv[0],DNcert,os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
                sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
                sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DN, cert),os.path.exists(os.path.join(DN, cert))))
                sys.stderr.write('       nfn:%s %d\n'%(os.path.join(DNcert, verify),os.path.exists(os.path.join(DNcert, verify))))
                
                self.ctx = SSL.Context(SSL.SSLv23_METHOD)
                self.ctx.set_options(SSL.OP_NO_SSLv2)
                self.ctx.set_app_data(self)
                self.ctx.set_verify(SSL.VERIFY_PEER|SSL.VERIFY_FAIL_IF_NO_PEER_CERT, verify_cb) # Demand a certificate
                self.ctx.use_privatekey_file (os.path.join(DN, privkey))
                self.ctx.use_certificate_file(os.path.join(DN, cert))
                self.ctx.load_verify_locations(os.path.join(DNcert, verify))
            else:
                self.ctx=None
        except:
            traceback.print_exc()
            self.ctx=None
    def DoVerify(self,conn, cert, errnum, depth, ok):
        #traceback.print_stack()
        if ok:
            self.serving=True
        else:
            self.serving=False
    def Start(self):
        if self.serving:
            return
        if self.stopping:
            return
        self.serving=True
        self.stopping=False
        if self.SSL:
            thread.start_new_thread(self.RunSSL,())
        else:
            thread.start_new_thread(self.Run,())
    def Stop(self):
        self.stopping=True
    def Pause(self,flag):
        self.pausing=flag
    def CheckPause(self):
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
        except:
            traceback.print_exc()
    def IsThreadRunning(self):
        return False
    def IsRunning(self):
        return self.serving
    def Run(self):
        time.sleep(1)  # fixes startup problem
        while self.serving:
            if self.stopping==True:
                self.serving=False
                continue
            try:
                data=self.conn.recv(self.RECV_SIZE)
                self.CheckPause()
                if not data:
                    self.serving=False
                else:
                    iLen=len(data)
                    self.received+=iLen
                    self.len+=iLen
                    self.data+=data
                    if self.paused==False:
                        while self.HandleData()>0:
                            pass
            except socket.timeout,msg:
                self.CheckPause()
            except Exception,list:
                if list[0]!=10054:
                    traceback.print_exc()
                self.serving=False
                pass
        try:
            self.conn.close()
        except:
            pass
        try:
            self.SocketClosed()
        except:
            pass
        try:
            self.srv.StopSocket(self.adr,self)
        except:
            pass
        self.serving=False
    def RunSSL(self):
        #sys.stderr.write('RunSSL\n')
        iTime=0.0
        while self.serving==False:
            time.sleep(0.02)
            iTime+=0.02
            if iTime>self.HANDSHAKE_TIMEOUT:
                try:
                    self.conn.shutdown()
                except:
                    pass
                try:
                    self.conn.close()
                except:
                    pass
                self.SocketAborted()
                return
        
        self.conn.settimeout(0.1)
        self.conn.setblocking(0)
        if self.SSL and 0:
            try:
                while self.iVerify==0:
                    time.sleep(0.2)
            except:
                traceback.print_exc()
                self.serving=False
        self.conn.settimeout(0.1)
        #sys.stderr.write('RunSSL\n')
        while self.serving:
            if self.stopping==True:
                self.serving=False
                continue
            try:
                data=self.conn.recv(self.RECV_SIZE)
                self.CheckPause()
                if not data:
                    self.serving=False
                    pass
                else:
                    iLen=len(data)
                    self.received+=iLen
                    self.len+=iLen
                    self.data+=data
                    if self.paused==False:
                        while self.HandleData()>0:
                            pass
            except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                time.sleep(0.01)
                pass
            except SSL.ZeroReturnError:
                traceback.print_exc()
                self.conn.shutdown()
                self.serving=False
            except SSL.Error, errors:
                traceback.print_exc()
                self.serving=False
            except:
                traceback.print_exc()
                self.serving=False
        #sys.stderr.write('RunSSL exit\n')
        try:
            self.conn.shutdown()
        except:
            pass
        try:
            self.conn.close()
        except:
            pass
        try:
            self.SocketClosed()
        except:
            pass
        try:
            self.srv.StopSocket(self.adr,self)
        except:
            pass
        self.serving=False
    def SocketClosed(self):
        pass
    def HandleData(self):
        self.Send(self.data)
        self.data=''
        self.len=0
    def Send(self,data,bRaw=False):
        try:
            if bRaw==False:
                if type(data)==types.UnicodeType:
                    data=data.encode('ISO-8859-1')
            self.semSend.acquire()
            self.send+=len(data)
            if self.SSL:
                self.__sendSSL__(data)
            else:
                self.__send__(data)
            self.semSend.release()
        except:
            traceback.print_exc()
    def __sendSSL__(self,data,bRaw=False):
        try:
            if bRaw==False:
                if type(data)==types.UnicodeType:
                    data=data.encode('ISO-8859-1')
            iLen=len(data)
            iSend=0
            iCur=0
            while iSend<iLen:
                try:
                    iCur=self.conn.send(data[iSend:iLen])
                    iSend+=iCur
                except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
                    pass
                except SSL.Error, errors:
                    print 'SSL.Error',errors
                    traceback.print_exc()
                    if iCur==0:
                        self.SSL=False
                        self.__send__(data,bRaw)
                        return
                except:
                    iCur=0
                if iSend<iLen:
                    time.sleep(0.01)
        except (SSL.WantReadError, SSL.WantWriteError, SSL.WantX509LookupError):
            time.sleep(0.01)
            pass
        except SSL.ZeroReturnError:
            self.conn.shutdown()
            self.serving=False
        except SSL.Error, errors:
            self.serving=False
        except:
            traceback.print_exc()
    def __send__(self,data,bRaw=False):
        try:
            if bRaw==False:
                if type(data)==types.UnicodeType:
                    data=data.encode('ISO-8859-1')
            if self.SEND_ALL:
                self.conn.sendall(data)
            else:
                iLen=len(data)
                iSend=0
                iCur=0
                while iSend<iLen:
                    try:
                        iCur=self.conn.send(data[iSend:iLen])
                        iSend+=iCur
                    except:
                        iCur=0
                    if iSend<iLen:
                        time.sleep(0.5)
        except:
            traceback.print_exc()
    def SendTelegram(self,data):
        try:
            i=0
            #self.semSend.acquire()
            #iLen=len(data)
            #self.Send(self.startMarker+data+self.endMarker)
            self.Send(self.startMarker)
            self.Send(data)
            self.Send(self.endMarker)
            #self.send+=iLen
            #self.send+=self.iStartMarker
            #self.send+=self.iEndMarker
            #self.semSend.release()
        except:
            traceback.print_exc()
    def SendSessionID(self):
        self.Send(self.startMarker+'sessionID|'+self.sessionID+self.endMarker)
    def Close(self):
        self.conn.close()
        self.serving=False
        self.SocketClosed()
        
