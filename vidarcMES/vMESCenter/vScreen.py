# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vScreen.py
# Purpose:      screen related functions
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vScreen.py,v 1.2 2016/02/06 14:03:51 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vSystem import isPlatForm
if isPlatForm('win'):
    from vScreenWin import *
elif isPlatForm('unx'):
    from vScreenUnx import *
else:
    from vScreenDummy import *
