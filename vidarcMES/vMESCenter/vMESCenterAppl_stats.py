#!/usr/bin/env python

#----------------------------------------------------------------------------
# Name:         vMESCenterAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vMESCenterAppl_stats.py,v 1.2 2006/10/09 11:11:37 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import hotshot,hotshot.stats
import sys
outf=open("vMESCenter.stats",'w')
sys.stdout=outf
stats = hotshot.stats.load("vMESCenter.prof")
#stats.strip_dirs()
stats.sort_stats('cumulative', 'calls')
#stats.sort_stats('module', 'file')
stats.print_stats()
outf.close()
