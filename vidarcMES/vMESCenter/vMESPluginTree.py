#----------------------------------------------------------------------------
# Name:         vMESPluginTree.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20070105
# CVS-ID:       $Id: vMESPluginTree.py,v 1.8 2007/11/17 10:43:15 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import sys,traceback,copy,types

import vtLgBase
import vtLogDef
import cStringIO
from wx import BitmapFromImage as wxBitmapFromImage
from wx import ImageFromStream as wxImageFromStream
from wx import EmptyIcon as wxEmptyIcon

def getPluginImage():
    return None

def getApplicationIcon():
    return None
    
def create(parent,fldBar):
    pass

import images as imgMES
if vtLogDef.VERBOSE:
    import pprint

class vMESPluginTree(wx.TreeCtrl):
    def __init__(self,parent,id=-1,pos=wx.DefaultPosition,size=wx.Size(-1, 200),style=0,name=u'trPlugin'):
        wx.TreeCtrl.__init__(self,id=id,
              name=name, parent=parent, pos=pos,
              size=size, style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT|wx.TR_LINES_AT_ROOT)
        self.Bind(wx.EVT_LEFT_DCLICK, self.OnTrPluginLeftDclick)
        self.func=None
        self.args=()
        self.kwargs={}
        
        self.dTiGrp={'modules':{},'plugins':{}}
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        img=imgMES.getModuleBitmap()
        self.imgDict['root']=self.imgLstTyp.Add(img)
        self.imgDict['mod']=self.imgLstTyp.Add(img)
        img=imgMES.getGroupingBitmap()
        self.imgDict['grp']=self.imgLstTyp.Add(img)
        dLg={}
        for lg,bmp in [('en',imgMES.getLangEnBitmap()),
                    ('de',imgMES.getLangDeBitmap()),
                    ('fr',imgMES.getLangFrBitmap()),
                    ('it',imgMES.getLangItBitmap()),
                    ('ne',imgMES.getLangNeBitmap()),
                    ('se',imgMES.getLangSeBitmap()),]:
            dLg[lg]=self.imgLstTyp.Add(bmp)
        self.imgDict['lang']=dLg
        self.SetImageList(self.imgLstTyp)
        img=self.imgDict['root']
        ti=self.AddRoot(_(u'plugins'))
        #self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
        #self.SetItemImage(ti,img,wx.TreeItemIcon_Expanded)
        tid=wx.TreeItemData()
        tid.SetData(None)
        self.tiModuls=self.AppendItem(ti,_('modules'),img,img,tid)
        tid=wx.TreeItemData()
        tid.SetData(None)
        self.tiPlgIns=self.AppendItem(ti,_('plugins'),img,img,tid)
    def SetLaunchFunc(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def Clear(self,bFull=True):
        if vtLogDef.VERBOSE:print '\n\n\n\nclear\n\n\n\n',bFull
        self.oCryptoStore=None
        self.DeleteChildren(self.tiModuls)
        #self.SetItemHasChildren(self.tiModuls,False)
        if bFull==True:
            self.dTiGrp={'modules':{},'plugins':{}}
            self.DeleteChildren(self.tiPlgIns)
            #self.SetItemHasChildren(self.tiPlgIns,False)
        else:
            self.dTiGrp['modules']={}
    def AddPlugIn(self,d,oPlugIn):
        try:
            if vtLogDef.VERBOSE:print oPlugIn.GetName()
            lang=vtLgBase.getApplLang()
            sPlugIn=oPlugIn.GetName()
            i=sPlugIn.find('vApps')
            if i>0:
                strs=sPlugIn[i+6:].split('.')
                strs=[strs[0]]
                tip=self.tiModuls
                dTiGrp=self.dTiGrp['modules']
                #d=self.dPlugIn
            else:
                strs=sPlugIn.split('.')
                dTiGrp=self.dTiGrp['plugins']
                #d=self.dPlugIn
                #tip=self.trPlugins.GetRootItem()
                tip=self.tiPlgIns
            if vtLogDef.VERBOSE:print '+++','plugins',
            if vtLogDef.VERBOSE:pprint.pprint(d)
            if vtLogDef.VERBOSE:print '---'
            #if vtLogDef.VERBOSE:pprint.pprint(dTiGrp)
            if vtLogDef.VERBOSE:print tip,self.GetItemText(tip)
            bmp=oPlugIn.GetBitmap()
            if bmp is not None:
                self.imgDict[sPlugIn]=self.imgLstTyp.Add(bmp)
            lTrans=oPlugIn.GetDomainTransLst(lang)
            if vtLogDef.VERBOSE:print '   ','trans',lTrans
            if lTrans is None:
                return
            lImg=oPlugIn.GetDomainImageDataLst()
            if vtLogDef.VERBOSE:print '   ','img',lImg
            lImgs=[]
            if lImg is not None:
                for sName,imgDataFunc in lImg:
                    if sName not in self.imgDict:
                        stream = cStringIO.StringIO(imgDataFunc())
                        img=wxImageFromStream(stream)
                        bmp=wxBitmapFromImage(img)
                        self.imgDict[sName]=self.imgLstTyp.Add(bmp)
                    lImgs.append(self.imgDict[sName])
            else:
                img=self.imgDict['mod']
                lImgs=[img for i in lTrans[:-1]]
                lImgs.append(self.imgDict[sPlugIn])
            if vtLogDef.VERBOSE:print '   ','imgs',lImgs
            iLen=len(strs)
            i=1
            iLen=len(lTrans)
            tPlugIn=None
            for i in xrange(iLen):
            #for s in lTrans:
                s=lTrans[i]
                if vtLogDef.VERBOSE:print '                    '[:i],s,s in d,self.GetItemText(tip)
                if not d.has_key(s):
                    keys=d.keys()
                    keys.sort()
                    if vtLogDef.VERBOSE:print '   ','add',s,'present',keys
                    sGrpName='.'.join(lTrans[:i+1])
                    if vtLogDef.VERBOSE:print '   ','grps',sGrpName,'in grp',sGrpName in dTiGrp
                    if sGrpName in dTiGrp:
                        tip=dTiGrp[sGrpName]
                    else:
                        img=lImgs[i]
                        tid=wx.TreeItemData()
                        if i==iLen-1:
                            tid.SetData(oPlugIn)
                        ti=self.AppendItem(tip,s,img,img,tid)
                        if i==iLen-1:
                            #d[s]=
                            tPlugIn=(ti,{},oPlugIn)
                            try:
                                langs=oPlugIn.getPossibleLang()
                                if vtLogDef.VERBOSE:print '   ','langs',langs
                                #try:
                                #    vtLog.vtLngCurWX(vtLog.DEBUG,langs,self)
                                #except:
                                #    pass
                                for lg in langs:
                                    img=self.imgDict['lang'][lg]
                                    tid=wx.TreeItemData()
                                    tid.SetData((oPlugIn,lg))
                                    tiLg=self.AppendItem(ti,lg,img,img,tid)
                                #tid=wx.TreeItemData()
                                #tid.SetData(oPlugIn)
                                #self.SetItemData(ti,tid)
                            except:
                                traceback.print_stack(file=sys.stderr)
                                traceback.print_exc(file=sys.stderr)
                        else:
                            tip=ti
                        #    d[s]=(ti,{},None)
                        dTiGrp[sGrpName]=ti
                #tip=d[s][0]
                #d=d[s][1]
            for s in strs[:-1]:
                if s not in d:
                    d[s]=(None,{},None)
                d=d[s][1]
            if tPlugIn is not None:
                d[strs[-1]]=tPlugIn
            return
            #for s in lTrans:
            #    
            #strs=lTrans
            #return
            for s in strs:
                if vtLogDef.VERBOSE:print '                    '[:i],s,s in d,self.GetItemText(tip)
                if not d.has_key(s):
                    keys=d.keys()
                    keys.sort()
                    if vtLogDef.VERBOSE:print '   ','add',s,'present',keys
                    lTrans=oPlugIn.GetDomainTransLst(lang)
                    lImgs=oPlugIn.GetDomainImageDataLst()
                    if vtLogDef.VERBOSE:print '   ','trans',lTrans
                    if vtLogDef.VERBOSE:print '   ','imgs',lImgs
                    if lImgs is not None:
                        ti=tip
                        for k in xrange(len(lTrans)):
                            sGrpName='.'.join([lImgs[j][0] for j in xrange(k+1)])
                            if vtLogDef.VERBOSE:print '   ','grps',sGrpName,'in grp',sGrpName in dTiGrp
                            if sGrpName in dTiGrp:
                                ti=dTiGrp[sGrpName]
                            else:
                                sNameImg=lImgs[k][0]
                                if sNameImg in self.imgDict:
                                    img=self.imgDict[sNameImg]
                                else:
                                    img=self.imgDict[sName]
                                tid=wx.TreeItemData()
                                ti=self.AppendItem(ti,lTrans[k],img,img,tid)
                                dTiGrp[sGrpName]=ti
                    else:
                        #img=self.imgDict[sName]
                        #ti=self.AppendItem(tip,'.'.join(lTrans),img,img,None)
                        tid=wx.TreeItemData()
                        ti=self.AppendItem(tip,s,-1,-1,tid)
                    if vtLogDef.VERBOSE:print '   ',i,iLen
                    if i==iLen:
                        img=self.imgDict[oPlugIn.GetName()]
                    else:
                        img=self.imgDict['mod']
                    self.SetItemImage(ti,img,wx.TreeItemIcon_Normal)
                    self.SetItemImage(ti,img,wx.TreeItemIcon_Selected)
                    self.SetItemImage(ti,img,wx.TreeItemIcon_Expanded)
                    if i==iLen:
                        try:
                            langs=oPlugIn.getPossibleLang()
                            if vtLogDef.VERBOSE:print '   ','langs',langs
                            #try:
                            #    vtLog.vtLngCurWX(vtLog.DEBUG,langs,self)
                            #except:
                            #    pass
                            for lg in langs:
                                img=self.imgDict['lang'][lg]
                                tid=wx.TreeItemData()
                                tid.SetData((oPlugIn,lg))
                                tiLg=self.AppendItem(ti,lg,img,img,tid)
                            tid=wx.TreeItemData()
                            tid.SetData(oPlugIn)
                            self.SetItemData(ti,tid)
                        except:
                            traceback.print_stack(file=sys.stderr)
                            traceback.print_exc(file=sys.stderr)
                            #vtLog.vtLngTB(self.GetName())
                        d[s]=(ti,{},oPlugIn)
                    else:
                        d[s]=(ti,{},None)
                    tip=ti
                else:
                    tip=d[s][0]
                d=d[s][1]
                i+=1
            if vtLogDef.VERBOSE:pprint.pprint(dTiGrp)
            if vtLogDef.VERBOSE:print
            if vtLogDef.VERBOSE:print '----------'
            
        except:
            traceback.print_stack(file=sys.stderr)
            traceback.print_exc(file=sys.stderr)
    def __sortPlugInTree__(self,ti):
        try:
            if ti is None:
                return
            #print '++',self.GetItemText(ti)#,ti
            if self.ItemHasChildren(ti)==False:
                return
            #self.SortChildren(ti)
            triChild=self.GetFirstChild(ti)
            while triChild[0].IsOk():
                if self.ItemHasChildren(triChild[0]):
                    self.__sortPlugInTree__(triChild[0])
                    #self.SortChildren(triChild[0])
                triChild=self.GetNextChild(ti,triChild[1])
            #print '  ',self.GetItemText(ti)#,ti
            self.SortChildren(ti)
        except:
            traceback.print_exc(file=sys.stderr)
    def PlugInFin(self):
        try:
            #print self.tiModuls
            #print self.tiPlgIns
            self.__sortPlugInTree__(self.tiModuls)
            self.__sortPlugInTree__(self.tiPlgIns)
            self.Expand(self.tiModuls)
            self.Expand(self.tiPlgIns)
        except:
            traceback.print_stack(file=sys.stderr)
            traceback.print_exc(file=sys.stderr)
    def OnTrPluginLeftDclick(self,evt):
        try:
            win=None
            #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            ti=self.GetSelection()
            tid=self.GetPyData(ti)
            if tid is not None:
                #lang=None
                #lang=vtLgBase.getApplLang()
                if type(tid)==types.TupleType:
                    ti=self.GetItemParent(ti)
                    oPlugIn,lang=tid
                else:
                    oPlugIn,lang=tid,None
                dLaunch={'plugIn':oPlugIn}
                if lang is not None:
                    dLaunch['lang']=lang
                dLaunch['lstNames']=oPlugIn.GetDomainNameLst()
                if self.func is not None:
                    self.func(dLaunch,oPlugIn,*self.args,**self.kwargs)
            else:
                evt.Skip()
        except:
            traceback.print_stack(file=sys.stderr)
            traceback.print_exc(file=sys.stderr)
