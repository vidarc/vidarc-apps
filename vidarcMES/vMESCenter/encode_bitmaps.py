#!/usr/bin/env python
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         encode_bitmaps.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: encode_bitmaps.py,v 1.5 2010/05/19 13:36:33 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

"""
This is a way to save the startup time when running img2py on lots of
files...
"""

#import sys
#from wxPython.tools import img2py


command_lines = [
    #"   -u -i -n Application Tools01_16.png images.py",
#    "   -u -i -n Mondrian mondrian.ico images.py",
    "-u -i -n Add               img/add.png                     images.py",
    "-a -u -n Cancel            img/abort.png                   images.py",
    "-a -u -n Del               img/waste.png                   images.py",
    "-a -u -n Edit              img/Edit02_16.png               images.py",
    "-a -u -n AddAttr           img/Add01_16.png                images.py",
    "-a -u -n DelAttr           img/Del02_16.png                images.py",
    "-a -u -n Apply             img/ok.png                      images.py",
    "-a -u -n Build             img/Build02_16.png              images.py",
    "-a -u -n Exit              img/Exit01_16.png               images.py",
    "-a -u -n Error             img/Error01_16.png              images.py",
    "-a -u -n Find              img/Search02_16.png             images.py",
    "-a -u -n Update            img/Synch05_16.png              images.py",
    "-a -u -n Application       img/Vid_MES02_16.ico            images.py",
    "-a -u -n Appl              img/Vid_MES02_16.png            images.py",
    "-a -u -n Module            img/vidarcModule01_16.png       images.py",
    "-a -u -n LangEn            img/Lang_en_16.png              images.py",
    "-a -u -n LangDe            img/Lang_de_16.png              images.py",
    "-a -u -n LangFr            img/Lang_fr_16.png              images.py",
    "-a -u -n LangSe            img/Lang_se_16.png              images.py",
    "-a -u -n LangIt            img/Lang_it_16.png              images.py",
    "-a -u -n LangNe            img/Lang_ne_16.png              images.py",
    "-a -u -n LangML            img/Lang_ch_16.png              images.py",
    "-a -u -n LoggedIn          img/SecKey01_16x12.png          images.py",
    "-a -u -n LoggedOut         img/UnSecKey01_16x12.png        images.py",
    "-a -u -n SecLocked         img/SecLocked01_16.png          images.py",
    "-a -u -n SecUnlocked       img/SecUnlocked01_16.png        images.py",
    "-a -u -n ShutDown          img/SrvStop01_16.png            images.py",
    "-a -u -n Connect           img/Connect01_16.png            images.py",
    "-a -u -n Login             img/SrvLogin01_16.png           images.py",
    "-a -u -n Running           img/SrvRng02_16.png             images.py",
    "-a -u -n Stopped           img/SrvStpd03_16.png            images.py",
    "-a -u -n Update            img/Synch05_16.png              images.py",
    "-a -u -n Fault             img/SrvFault04_16.png           images.py",
    "-a -u -n Stop              img/Stop01_16.png               images.py",
    "-a -u -n NoIcon            img/noicon.png                  images.py",
    "-a -u -n Invisible         img/Invisible.png               images.py",
    "-a -u -n Advanced          img/Admin01_16.png              images.py",
    "-a -u -n Online            img/Link03_16x12.png            images.py",
    "-a -u -n Offline           img/UnLink03_16x12.png          images.py",
    "-a -u -n Grouping          img/TreeGrouping05_16.png       images.py",
    
    "-u -i -n Lang00            img/Lang_en_16.png              images_lang.py",
    "-a -u -n Lang01            img/Lang_de_16.png              images_lang.py",
    "-a -u -n Lang00Big         img/Lang_en_32.png              images_lang.py",
    "-a -u -n Lang01Big         img/Lang_de_32.png              images_lang.py",
    "-a -u -n ApplyMultiple     img/ApplyMultiple01_32.png      images_lang.py",
    #"-a -u -n User Usr01_16.png hum_tree_images.py",
    #"-a -u -n UserSel Usr01_16.png hum_tree_images.py",
    #"-a -u -n Human Human01_16.png hum_tree_images.py",
    #"-a -u -n HumanSel Human01_16.png hum_tree_images.py",
    #"-a -u -n Add add.png images.py",
    #"-a -u -n Del waste.png images.py",
    #"-a -u -n Apply checkmrk.png images.py",

    "-u -i -n Splash img/splashCenter01.png images_splash.py",
    ]


#for line in command_lines:
#    args = line.split()
#    img2py.main(args)

