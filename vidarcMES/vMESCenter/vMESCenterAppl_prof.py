#!/usr/bin/env python

#----------------------------------------------------------------------------
# Name:         vMESCenterAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vMESCenterAppl_prof.py,v 1.2 2006/10/09 11:11:37 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

if 1:
    import hotshot,hotshot.stats
    import sys,os
    
    prof = hotshot.Profile("vMESCenter.prof")
    fn='vMESCenterAppl.py'
    #sys.path.insert(0, os.path.dirname(fn))
    tRet = prof.run('execfile(%r)'%fn)
    prof.close()
    stats = hotshot.stats.load("vMESCenter.prof")
    #stats.strip_dirs()
    stats.sort_stats('time', 'calls')
    stats.print_stats(20)
else:
    import profile
    fn='vMESCenterAppl.py'
    prof = profile.run('execfile(%r)'%fn)
