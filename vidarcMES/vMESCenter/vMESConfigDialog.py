#Boa:Dialog:vMESConfigDialog

import wx
import wx.lib.filebrowsebutton
import wx.lib.buttons
import wx.lib.intctrl

import images
import os.path,sys,traceback

import IniFile

def create(parent):
    return vMESConfigDialog(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VMESCONFIGDIALOG, wxID_VMESCONFIGDIALOGCBAPPLY, 
 wxID_VMESCONFIGDIALOGCBCANCEL, wxID_VMESCONFIGDIALOGINTDATAPORT, 
 wxID_VMESCONFIGDIALOGINTFILEPORT, wxID_VMESCONFIGDIALOGINTMSGPORT, 
 wxID_VMESCONFIGDIALOGINTPLUGINPORT, wxID_VMESCONFIGDIALOGLBLDATASRV, 
 wxID_VMESCONFIGDIALOGLBLFILE, wxID_VMESCONFIGDIALOGLBLGENLOADBOUND, 
 wxID_VMESCONFIGDIALOGLBLHYPGRPCHAR, wxID_VMESCONFIGDIALOGLBLHYPGRPSEP, 
 wxID_VMESCONFIGDIALOGLBLMSG, wxID_VMESCONFIGDIALOGLBLPLUGIN, 
 wxID_VMESCONFIGDIALOGNBCFG, wxID_VMESCONFIGDIALOGPNGENERAL, 
 wxID_VMESCONFIGDIALOGPNHYPER, wxID_VMESCONFIGDIALOGPNUSR, 
 wxID_VMESCONFIGDIALOGPNSRV, 
 wxID_VMESCONFIGDIALOGSPGENLOADBOUND, wxID_VMESCONFIGDIALOGSPHYPGRPCHARLV0, 
 wxID_VMESCONFIGDIALOGSPHYPGRPCHARLV1, wxID_VMESCONFIGDIALOGSPHYPGRPCHARLV2, 
 wxID_VMESCONFIGDIALOGTXTDATAHOST, wxID_VMESCONFIGDIALOGTXTFILEHOST, 
 wxID_VMESCONFIGDIALOGTXTHYPGRPSEP, wxID_VMESCONFIGDIALOGTXTMSGHOST, 
 wxID_VMESCONFIGDIALOGTXTPLUGINHOST, wxID_VMESCONFIGDIALOGLBLUSRMODE,
 wxID_VMESCONFIGDIALOGTGADVANCED, wxID_VMESCONFIGDIALOGTGLOGINEXT
] = [wx.NewId() for _init_ctrls in range(31)]

class vMESConfigDialog(wx.Dialog):
    CFG_PLUGIN='vMESCfgPlugIn.ini'
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblDataSrv, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtDataHost, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.intDataPort, (0, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblPlugIn, (1, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtPlugInHost, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.intPlugInPort, (1, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblMsg, (2, 0), border=0, flag=wx.EXPAND, span=(1,
              1))
        parent.AddWindow(self.txtMsgHost, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.intMsgPort, (2, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblFile, (3, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.txtFileHost, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.intFilePort, (3, 2), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_gbsHyper_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHypGrpSep, (0, 0), border=0, flag=0, span=(1,
              1))
        parent.AddWindow(self.txtHypGrpSep, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.lblHypGrpChar, (1, 0), border=0, flag=0, span=(1,
              1))
        parent.AddWindow(self.spHypGrpCharLv0, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.spHypGrpCharLv1, (2, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.spHypGrpCharLv2, (3, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_bxsCfg_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.nbCfg, 1, border=4,
              flag=wx.TOP | wx.BOTTOM | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_coll_gbsGen_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblGenLoadBound, (0, 0), border=0, flag=0,
              span=(1, 1))
        parent.AddWindow(self.spGenLoadBound, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_gbsUsr_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsrMode, (0, 0), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.tgAdvanced, (0, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))
        parent.AddWindow(self.tgLoginExt, (1, 1), border=0, flag=wx.EXPAND,
              span=(1, 1))

    def _init_coll_nbCfg_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.pnGeneral, select=True,
              text=_(u'general'))
        parent.AddPage(imageId=-1, page=self.pnSrv, select=False,
              text=_(u'server'))
        parent.AddPage(imageId=-1, page=self.pnHyper, select=False,
              text=_(u'hyper browse'))
        parent.AddPage(imageId=-1, page=self.pnUsr, select=False,
              text=_(u'user'))

    def _init_sizers(self):
        # generated method, don't edit
        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)

        self.bxsCfg = wx.BoxSizer(orient=wx.VERTICAL)

        self.gbsHyper = wx.GridBagSizer(hgap=4, vgap=4)

        self.gbsGen = wx.GridBagSizer(hgap=4, vgap=4)

        self.gbsUsr = wx.GridBagSizer(hgap=4, vgap=4)
        
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsCfg_Items(self.bxsCfg)
        self._init_coll_gbsHyper_Items(self.gbsHyper)
        self._init_coll_gbsGen_Items(self.gbsGen)
        self._init_coll_gbsUsr_Items(self.gbsUsr)

        self.pnSrv.SetSizer(self.gbsData)
        self.SetSizer(self.bxsCfg)
        self.pnHyper.SetSizer(self.gbsHyper)
        self.pnGeneral.SetSizer(self.gbsGen)
        self.pnUsr.SetSizer(self.gbsUsr)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VMESCONFIGDIALOG,
              name=u'vMESConfigDialog', parent=prnt, pos=wx.Point(132, 132),
              size=wx.Size(313, 216),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vMESConfig Dialog')
        self.SetClientSize(wx.Size(305, 189))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESCONFIGDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(68, 159), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VMESCONFIGDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESCONFIGDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(160, 159),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VMESCONFIGDIALOGCBCANCEL)

        self.nbCfg = wx.Notebook(id=wxID_VMESCONFIGDIALOGNBCFG, name=u'nbCfg',
              parent=self, pos=wx.Point(4, 4), size=wx.Size(297, 143), style=0)

        self.pnSrv = wx.Panel(id=wxID_VMESCONFIGDIALOGPNSRV, name=u'pnSrv',
              parent=self.nbCfg, pos=wx.Point(0, 0), size=wx.Size(289, 117),
              style=wx.TAB_TRAVERSAL)

        self.pnHyper = wx.Panel(id=wxID_VMESCONFIGDIALOGPNHYPER,
              name=u'pnHyper', parent=self.nbCfg, pos=wx.Point(0, 0),
              size=wx.Size(289, 117), style=wx.TAB_TRAVERSAL)

        self.pnUsr = wx.Panel(id=wxID_VMESCONFIGDIALOGPNUSR,
              name=u'pnUsr', parent=self.nbCfg, pos=wx.Point(0, 0),
              size=wx.Size(289, 117), style=wx.TAB_TRAVERSAL)

        self.lblDataSrv = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLDATASRV,
              label=_(u'data server'), name=u'lblDataSrv', parent=self.pnSrv,
              pos=wx.Point(0, 0), size=wx.Size(76, 21), style=wx.ALIGN_RIGHT)
        self.lblDataSrv.SetMinSize(wx.Size(-1, -1))

        self.txtDataHost = wx.TextCtrl(id=wxID_VMESCONFIGDIALOGTXTDATAHOST,
              name=u'txtDataHost', parent=self.pnSrv, pos=wx.Point(80, 0),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtDataHost.SetToolTipString(u'host')
        self.txtDataHost.SetMinSize(wx.Size(-1, -1))

        self.intDataPort = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VMESCONFIGDIALOGINTDATAPORT, limited=False, max=65525,
              min=512, name=u'intDataPort', oob_color=wx.RED, parent=self.pnSrv,
              pos=wx.Point(184, 0), size=wx.Size(100, 21), style=0,
              value=50000)
        self.intDataPort.SetToolTipString(u'port')
        self.intDataPort.SetMinSize(wx.Size(-1, -1))

        self.lblPlugIn = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLPLUGIN,
              label=_(u'plugin server'), name=u'lblPlugIn', parent=self.pnSrv,
              pos=wx.Point(0, 25), size=wx.Size(76, 21), style=wx.ALIGN_RIGHT)
        self.lblPlugIn.SetMinSize(wx.Size(-1, -1))

        self.txtPlugInHost = wx.TextCtrl(id=wxID_VMESCONFIGDIALOGTXTPLUGINHOST,
              name=u'txtPlugInHost', parent=self.pnSrv, pos=wx.Point(80, 25),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtPlugInHost.SetToolTipString(u'host')
        self.txtPlugInHost.SetMinSize(wx.Size(-1, -1))

        self.intPlugInPort = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VMESCONFIGDIALOGINTPLUGINPORT, limited=False, max=65525,
              min=512, name=u'intPlugInPort', oob_color=wx.RED,
              parent=self.pnSrv, pos=wx.Point(184, 25), size=wx.Size(100, 21),
              style=0, value=50002)
        self.intPlugInPort.SetToolTipString(u'port')
        self.intPlugInPort.SetMinSize(wx.Size(-1, -1))

        self.lblMsg = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLMSG,
              label=_(u'message server'), name=u'lblMsg', parent=self.pnSrv,
              pos=wx.Point(0, 50), size=wx.Size(76, 21), style=wx.ALIGN_RIGHT)
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.txtMsgHost = wx.TextCtrl(id=wxID_VMESCONFIGDIALOGTXTMSGHOST,
              name=u'txtMsgHost', parent=self.pnSrv, pos=wx.Point(80, 50),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtMsgHost.SetToolTipString(u'host')
        self.txtMsgHost.SetMinSize(wx.Size(-1, -1))

        self.intMsgPort = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VMESCONFIGDIALOGINTMSGPORT, limited=True, max=65525,
              min=512, name=u'intMsgPort', oob_color=wx.RED, parent=self.pnSrv,
              pos=wx.Point(184, 50), size=wx.Size(100, 21), style=0,
              value=50004)
        self.intMsgPort.SetToolTipString(u'port')
        self.intMsgPort.SetMinSize(wx.Size(-1, -1))

        self.lblFile = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLFILE,
              label=_(u'file server'), name=u'lblFile', parent=self.pnSrv,
              pos=wx.Point(0, 75), size=wx.Size(76, 21), style=wx.ALIGN_RIGHT)

        self.txtFileHost = wx.TextCtrl(id=wxID_VMESCONFIGDIALOGTXTFILEHOST,
              name=u'txtFileHost', parent=self.pnSrv, pos=wx.Point(80, 75),
              size=wx.Size(100, 21), style=0, value=u'')
        self.txtFileHost.SetToolTipString(u'host')
        self.txtFileHost.SetMinSize(wx.Size(-1, -1))

        self.intFilePort = wx.lib.intctrl.IntCtrl(allow_long=False,
              allow_none=False, default_color=wx.BLACK,
              id=wxID_VMESCONFIGDIALOGINTFILEPORT, limited=False, max=65525,
              min=512, name=u'intFilePort', oob_color=wx.RED, parent=self.pnSrv,
              pos=wx.Point(184, 75), size=wx.Size(100, 21), style=0,
              value=50006)
        self.intFilePort.SetToolTipString(u'port')
        self.intFilePort.SetMinSize(wx.Size(-1, -1))

        self.lblHypGrpSep = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLHYPGRPSEP,
              label=_(u'grouping separator'), name=u'lblHypGrpSep',
              parent=self.pnHyper, pos=wx.Point(0, 0), size=wx.Size(92, 13),
              style=wx.ALIGN_RIGHT)
        self.lblHypGrpSep.SetMinSize(wx.Size(-1, -1))

        self.txtHypGrpSep = wx.TextCtrl(id=wxID_VMESCONFIGDIALOGTXTHYPGRPSEP,
              name=u'txtHypGrpSep', parent=self.pnHyper, pos=wx.Point(96, 0),
              size=wx.Size(117, 21), style=0, value=u'_')
        self.txtHypGrpSep.SetMaxLength(1)
        self.txtHypGrpSep.SetMinSize(wx.Size(-1, -1))

        self.lblHypGrpChar = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLHYPGRPCHAR,
              label=_(u'characters level 1'), name=u'lblHypGrpChar',
              parent=self.pnHyper, pos=wx.Point(0, 25), size=wx.Size(85, 13),
              style=wx.ALIGN_RIGHT)
        self.lblHypGrpChar.SetMinSize(wx.Size(-1, -1))

        self.spHypGrpCharLv0 = wx.SpinCtrl(id=wxID_VMESCONFIGDIALOGSPHYPGRPCHARLV0,
              initial=3, max=10, min=1, name=u'spHypGrpCharLv0',
              parent=self.pnHyper, pos=wx.Point(96, 25), size=wx.Size(117, 21),
              style=wx.SP_ARROW_KEYS)
        self.spHypGrpCharLv0.SetMinSize(wx.Size(-1, -1))

        self.spHypGrpCharLv1 = wx.SpinCtrl(id=wxID_VMESCONFIGDIALOGSPHYPGRPCHARLV1,
              initial=3, max=10, min=3, name=u'spHypGrpCharLv1',
              parent=self.pnHyper, pos=wx.Point(96, 50), size=wx.Size(117, 21),
              style=wx.SP_ARROW_KEYS)
        self.spHypGrpCharLv1.SetMinSize(wx.Size(-1, -1))

        self.spHypGrpCharLv2 = wx.SpinCtrl(id=wxID_VMESCONFIGDIALOGSPHYPGRPCHARLV2,
              initial=3, max=10, min=3, name=u'spHypGrpCharLv2',
              parent=self.pnHyper, pos=wx.Point(96, 75), size=wx.Size(117, 21),
              style=wx.SP_ARROW_KEYS)
        self.spHypGrpCharLv2.SetMinSize(wx.Size(-1, -1))

        self.pnGeneral = wx.Panel(id=wxID_VMESCONFIGDIALOGPNGENERAL,
              name=u'pnGeneral', parent=self.nbCfg, pos=wx.Point(0, 0),
              size=wx.Size(289, 117), style=wx.TAB_TRAVERSAL)

        self.lblGenLoadBound = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLGENLOADBOUND,
              label=_(u'load bound [%]'), name=u'lblGenLoadBound',
              parent=self.pnGeneral, pos=wx.Point(0, 0), size=wx.Size(75, 13),
              style=wx.ALIGN_RIGHT)

        self.spGenLoadBound = wx.SpinCtrl(id=wxID_VMESCONFIGDIALOGSPGENLOADBOUND,
              initial=75, max=100, min=10, name=u'spGenLoadBound',
              parent=self.pnGeneral, pos=wx.Point(79, 0), size=wx.Size(139, 21),
              style=wx.SP_ARROW_KEYS)
        
        self.lblUsrMode = wx.StaticText(id=wxID_VMESCONFIGDIALOGLBLUSRMODE,
              label=_(u'user mode'), name=u'lblUsrMode', parent=self.pnUsr,
              pos=wx.Point(3, 150), size=wx.Size(50, 13), style=0)
        self.lblUsrMode.SetMinSize((-1,-1))
        
        self.tgAdvanced = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VMESCONFIGDIALOGTGADVANCED,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Advanced'),
              name=u'tgAdvanced', parent=self.pnUsr, pos=wx.Point(57, 150),
              size=wx.Size(79, 30), style=0)
        self.tgAdvanced.SetMinSize((-1,-1))
        self.tgAdvanced.SetToggle(False)
        
        self.tgLoginExt = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VMESCONFIGDIALOGTGLOGINEXT,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'extended login'),
              name=u'tgLoginExt', parent=self.pnUsr, pos=wx.Point(140, 150),
              size=wx.Size(102, 30), style=0)
        self.tgLoginExt.SetMinSize((-1,-1))
        self.tgLoginExt.SetToggle(False)

        self._init_coll_nbCfg_Pages(self.nbCfg)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        self.tgAdvanced.SetBitmapLabel(images.getAdvancedBitmap())
        self.tgAdvanced.SetBitmapSelected(images.getAdvancedBitmap())
        self.tgLoginExt.SetBitmapLabel(images.getConnectBitmap())
        self.tgLoginExt.SetBitmapSelected(images.getConnectBitmap())
        
        self.Clear()
        self.gbsData.AddGrowableCol(1,2)
        self.gbsData.AddGrowableCol(2,1)
        self.gbsData.Layout()
        #self.gbsData.Fit(self)
        self.gbsHyper.AddGrowableCol(1,2)
        self.gbsHyper.Layout()
        self.gbsGen.AddGrowableCol(1,2)
        self.gbsGen.Layout()
        self.gbsUsr.AddGrowableCol(1,2)
        self.gbsUsr.Layout()
        self.bxsCfg.Layout()
        self.bxsCfg.Fit(self)
    def Clear(self):
        self.txtMsgHost.SetValue('')
        self.intMsgPort.SetValue(50004)
    def SetCfg(self,doc):
        self.Clear()
        #node=doc.getChild(None,'messages')
        #if node is None:
        #    return
        #sHost=doc.GetValue(node,'host',str,'localhost')
        #iPort=doc.GetValue(node,'port',int,50004)
        #self.txtMsgHost.SetValue(sHost)
        #self.intMsgPort.SetValue(iPort)
        cfg=IniFile.Config()
        try:
            cfg.readFN(self.CFG_PLUGIN)
        except:
            sys.stderr.write(traceback.format_exc())
        try:
            # general section
            section=cfg.getSection('user')
            bTmp=int(section.getInfo('mode_advanced',0))
            self.SetMode(bTmp)
            bTmp=int(section.getInfo('login_extended',0))
            self.SetLogin(bTmp)
        except:
            sys.stderr.write(traceback.format_exc())
            self.SetMode(False)
            self.SetLogin(False)
        try:
            #server section
            section=cfg.getSection('server')
            sHost=section.getInfo('data_host','vidarc')
            iPort=int(section.getInfo('data_port',50000))
            self.txtDataHost.SetValue(sHost)
            self.intDataPort.SetValue(iPort)
            sHost=section.getInfo('message_host','vidarc')
            iPort=int(section.getInfo('message_port',50004))
            self.txtMsgHost.SetValue(sHost)
            self.intMsgPort.SetValue(iPort)
            sHost=section.getInfo('plugin_host','vidarc')
            iPort=int(section.getInfo('plugin_port',50002))
            self.txtPlugInHost.SetValue(sHost)
            self.intPlugInPort.SetValue(iPort)
            sHost=section.getInfo('file_host','vidarc')
            iPort=int(section.getInfo('file_port',50006))
            self.txtFileHost.SetValue(sHost)
            self.intFilePort.SetValue(iPort)
        except:
            sys.stderr.write(traceback.format_exc())
            self.txtDataHost.SetValue('vidarc')
            self.intDataPort.SetValue(50000)
            self.txtMsgHost.SetValue('vidarc')
            self.intMsgPort.SetValue(50004)
            self.txtPlugInHost.SetValue('vidarc')
            self.intPlugInPort.SetValue(50002)
            self.txtFileHost.SetValue('vidarc')
            self.intFilePort.SetValue(50006)
        try:
            section=cfg.getSection('hyper_browse')
            self.txtHypGrpSep.SetValue(section.getInfo('separator','_'))
            self.spHypGrpCharLv0.SetValue(int(section.getInfo('char_level0','3')))
            self.spHypGrpCharLv1.SetValue(int(section.getInfo('char_level1','3')))
            self.spHypGrpCharLv2.SetValue(int(section.getInfo('char_level2','3')))
        except:
            sys.stderr.write(traceback.format_exc())
            self.txtHypGrpSep.SetValue('_')
            self.spHypGrpCharLv0.SetValue(3)
            self.spHypGrpCharLv1.SetValue(3)
            self.spHypGrpCharLv2.SetValue(3)
        try:
            # general section
            section=cfg.getSection('general')
            self.spGenLoadBound.SetValue(int(float(section.getInfo('load_bound','0.75'))*100))
            #localDN=os.path.expanduser(section.getInfo('path_local','~'))
            #self.dbbGenLocal.SetValue(localDN)
        except:
            sys.stderr.write(traceback.format_exc())
            self.spGenLoadBound.SetValue(75)
            #self.dbbGenLocal.SetValue('~')
    def GetCfg(self,doc):
        try:
            node=doc.getChildForced(None,'messages')
            if node is None:
                return
            doc.SetValue(node,'host',self.txtMsgHost.GetValue())
            doc.SetValue(node,'port',self.intMsgPort.GetValue())
            doc.AlignNode(node)
        except:
            pass
        cfg=IniFile.Config()
        try:
            cfg.readFN(self.CFG_PLUGIN)
        except:
            sys.stderr.write(traceback.format_exc())
        try:
            # user section
            section=cfg.getSection('user')
            section.setInfo('mode_advanced',str(self.GetMode()))
            section.setInfo('login_extended',str(self.GetLogin()))
        except:
            sys.stderr.write(traceback.format_exc())
        try:
            #server section
            section=cfg.getSection('server')
            sHost=self.txtDataHost.GetValue()
            iPort=self.intDataPort.GetValue()
            section.setInfo('data_host',sHost)
            section.setInfo('data_port',str(iPort))
            sHost=self.txtMsgHost.GetValue()
            iPort=self.intMsgPort.GetValue()
            section.setInfo('message_host',sHost)
            section.setInfo('message_port',str(iPort))
            sHost=self.txtPlugInHost.GetValue()
            iPort=self.intPlugInPort.GetValue()
            section.setInfo('plugin_host',sHost)
            section.setInfo('plugin_port',str(iPort))
            sHost=self.txtFileHost.GetValue()
            iPort=self.intFilePort.GetValue()
            section.setInfo('file_host',sHost)
            section.setInfo('file_port',str(iPort))
            
            # hyper browse section
            section=cfg.getSection('hyper_browse')
            section.setInfo('separator',self.txtHypGrpSep.GetValue())
            section.setInfo('char_level0',self.spHypGrpCharLv0.GetValue())
            section.setInfo('char_level1',self.spHypGrpCharLv1.GetValue())
            section.setInfo('char_level2',self.spHypGrpCharLv2.GetValue())
            
            # general section
            section=cfg.getSection('general')
            section.setInfo('load_bound','%3.2f'%(self.spGenLoadBound.GetValue()/100.0))
        except:
            sys.stderr.write(traceback.format_exc())
        try:
            cfg.writeFN(self.CFG_PLUGIN)
        except:
            pass
    def SetMode(self,bAdvanced):
        self.tgAdvanced.SetValue(bAdvanced)
    def GetMode(self):
        return int(self.tgAdvanced.GetValue())
    def SetLogin(self,bAdvanced):
        self.tgLoginExt.SetValue(bAdvanced)
    def GetLogin(self):
        return int(self.tgLoginExt.GetValue())
    def OnCbApplyButton(self, event):
        event.Skip()
        self.EndModal(1)

    def OnCbCancelButton(self, event):
        event.Skip()
        self.EndModal(0)
