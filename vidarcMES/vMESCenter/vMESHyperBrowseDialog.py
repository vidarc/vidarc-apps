#Boa:Dialog:vMESHyperBrowseDialog
#----------------------------------------------------------------------------
# Name:         vMESHyperBrowseDialog.py
# Purpose:      hyper browse dialog
#
# Author:       Walter Obweger
#
# Created:      200061228
# CVS-ID:       $Id: vMESHyperBrowseDialog.py,v 1.4 2007/07/30 20:39:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import images
import sha,Queue,traceback,os,os.path,stat,types,sys

import __init__
if __init__.VIDARC_IMPORT:
    import vidImp
import identify as vIdentify
try:
    #sys.stderr.write('\nimport vSockFileClt\n')
    from vSockXmlClt import *
    #sys.stderr.write('\nimport vSockClt\n')
    from vSockClt import *
except:
    sys.stderr.write('\nimport\n')
    sys.stderr.write(traceback.format_exc())

def create(parent):
    return vMESHyperBrowseDialog(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VMESHYPERBROWSEDIALOG, wxID_VMESHYPERBROWSEDIALOGCBCLOSE, 
 wxID_VMESHYPERBROWSEDIALOGCBCONNECT, wxID_VMESHYPERBROWSEDIALOGCBFIND, 
 wxID_VMESHYPERBROWSEDIALOGLBLHOST, wxID_VMESHYPERBROWSEDIALOGLBLRESULT, 
 wxID_VMESHYPERBROWSEDIALOGLBLUSR, wxID_VMESHYPERBROWSEDIALOGLSTPLUGIN, 
 wxID_VMESHYPERBROWSEDIALOGPBPLUGIN, wxID_VMESHYPERBROWSEDIALOGPBPLUGINS, 
 wxID_VMESHYPERBROWSEDIALOGSTBRESULT, wxID_VMESHYPERBROWSEDIALOGTXTHOST, 
 wxID_VMESHYPERBROWSEDIALOGTXTPASSWD, wxID_VMESHYPERBROWSEDIALOGTXTPORT, 
 wxID_VMESHYPERBROWSEDIALOGTXTUSER, 
] = [wx.NewId() for _init_ctrls in range(15)]

class vMESHyperBrowseDialog(wx.Dialog):
    VERBOSE=0
    VIDARC_ALIAS='vPlugInFile:vidarc'
    VIDARC_ALIAS_LOCALE='vPlugInFile:vidarcLocale'
    def _init_coll_bxsBtConnUpdate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbConnect, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtHost, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPort, 1, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsHost, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsLogin, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsBtConnUpdate, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.TOP)
        parent.AddSizer(self.bxsConn, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsPlugIn, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsPlugInBox, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbClose, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsLogin_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtUser, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 1, border=4, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbFind, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_coll_bxsPlugIn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstPlugIn, 1, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_bxsPlugInBox_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pbPlugIns, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.pbPlugIn, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsConn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.stbResult, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.lblResult, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_lstPlugIn_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'plugin'), width=200)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'remote'), width=60)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'local'), width=60)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=7, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLogin = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPlugIn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPlugInBox = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBtConnUpdate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsLogin_Items(self.bxsLogin)
        self._init_coll_bxsPlugIn_Items(self.bxsPlugIn)
        self._init_coll_bxsPlugInBox_Items(self.bxsPlugInBox)
        self._init_coll_bxsConn_Items(self.bxsConn)
        self._init_coll_bxsBtConnUpdate_Items(self.bxsBtConnUpdate)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VMESHYPERBROWSEDIALOG,
              name=u'vMESHyperBrowseDialog', parent=prnt, pos=wx.Point(371,
              105), size=wx.Size(400, 400),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vMES Hyper Browse Dialog')
        self.SetClientSize(wx.Size(392, 373))

        self.lblHost = wx.StaticText(id=wxID_VMESHYPERBROWSEDIALOGLBLHOST,
              label=_(u'Host / Port'), name=u'lblHost', parent=self,
              pos=wx.Point(0, 4), size=wx.Size(126, 21), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.txtHost = wx.TextCtrl(id=wxID_VMESHYPERBROWSEDIALOGTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(130, 4),
              size=wx.Size(126, 21), style=0, value=u'')
        self.txtHost.SetMinSize(wx.Size(-1, -1))

        self.txtPort = wx.TextCtrl(id=wxID_VMESHYPERBROWSEDIALOGTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(260, 4),
              size=wx.Size(130, 21), style=0, value=u'50002')
        self.txtPort.SetMinSize(wx.Size(-1, -1))

        self.lblUsr = wx.StaticText(id=wxID_VMESHYPERBROWSEDIALOGLBLUSR,
              label=_(u'login'), name=u'lblUsr', parent=self, pos=wx.Point(0,
              29), size=wx.Size(126, 21), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.txtUser = wx.TextCtrl(id=wxID_VMESHYPERBROWSEDIALOGTXTUSER,
              name=u'txtUser', parent=self, pos=wx.Point(130, 29),
              size=wx.Size(126, 21), style=0, value=u'')
        self.txtUser.SetMinSize(wx.Size(-1, -1))

        self.txtPasswd = wx.TextCtrl(id=wxID_VMESHYPERBROWSEDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(260, 29),
              size=wx.Size(130, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))

        self.cbConnect = wx.lib.buttons.GenBitmapTextToggleButton(ID=wxID_VMESHYPERBROWSEDIALOGCBCONNECT,
              bitmap=wx.EmptyBitmap(16, 16), label=_('connect'),
              name=u'cbConnect', parent=self, pos=wx.Point(114, 54),
              size=wx.Size(76, 30), style=0)
        self.cbConnect.SetMinSize(wx.Size(-1, -1))
        self.cbConnect.Bind(wx.EVT_BUTTON, self.OnCbConnectButton,
              id=wxID_VMESHYPERBROWSEDIALOGCBCONNECT)

        self.lstPlugIn = wx.ListCtrl(id=wxID_VMESHYPERBROWSEDIALOGLSTPLUGIN,
              name=u'lstPlugIn', parent=self, pos=wx.Point(0, 108),
              size=wx.Size(353, 206), style=wx.LC_REPORT)
        self.lstPlugIn.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstPlugIn_Columns(self.lstPlugIn)

        self.cbFind = wx.lib.buttons.GenBitmapButton(ID=wxID_VMESHYPERBROWSEDIALOGCBFIND,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbFind', parent=self,
              pos=wx.Point(357, 108), size=wx.Size(31, 30), style=0)
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VMESHYPERBROWSEDIALOGCBFIND)

        self.pbPlugIns = wx.Gauge(id=wxID_VMESHYPERBROWSEDIALOGPBPLUGINS,
              name=u'pbPlugIns', parent=self, pos=wx.Point(4, 318), range=100,
              size=wx.Size(128, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbPlugIns.SetMinSize(wx.Size(-1, 13))

        self.pbPlugIn = wx.Gauge(id=wxID_VMESHYPERBROWSEDIALOGPBPLUGIN,
              name=u'pbPlugIn', parent=self, pos=wx.Point(136, 318),
              range=10000, size=wx.Size(252, 13),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbPlugIn.SetMinSize(wx.Size(-1, 13))

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESHYPERBROWSEDIALOGCBCLOSE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Close'), name=u'cbClose',
              parent=self, pos=wx.Point(158, 339), size=wx.Size(76, 30),
              style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VMESHYPERBROWSEDIALOGCBCLOSE)

        self.stbResult = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VMESHYPERBROWSEDIALOGSTBRESULT, name=u'stbResult',
              parent=self, pos=wx.Point(8, 88), size=wx.Size(16, 16), style=0)

        self.lblResult = wx.StaticText(id=wxID_VMESHYPERBROWSEDIALOGLBLRESULT,
              label=u'', name=u'lblResult', parent=self, pos=wx.Point(28, 88),
              size=wx.Size(356, 16), style=0)
        self.lblResult.SetMinSize(wx.Size(-1, -1))

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.cbFind.SetBitmapLabel(images.getFindBitmap())
        self.cbClose.SetBitmapLabel(images.getCancelBitmap())
        self.cbConnect.SetBitmapLabel(images.getConnectBitmap())
        self.cbConnect.SetBitmapSelected(images.getShutDownBitmap())
        self.stbResult.SetBitmap(images.getStoppedBitmap())
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dImg['empty']=self.imgLst.Add(images.getInvisibleBitmap())
        self.dImg['ok']=self.imgLst.Add(images.getApplyBitmap())
        self.dImg['err']=self.imgLst.Add(images.getErrorBitmap())
        self.lstPlugIn.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.sActDN=''
        self.sockFile=None
        self.cltServ=None
        self.timer=None
        self.bLoggedIn=False
        self.bConnected=False
        self.bModeAuto=False
        self.func=None
        self.args=None
        self.kwargs=None
        self.qMsg=Queue.Queue()
        self.qContent=Queue.Queue()
        self.qFilesProc=Queue.Queue()
        self.qFiles=Queue.Queue()
        self.Clear()
    def SetModeAuto(self,bMode,func,*args,**kwargs):
        self.bModeAuto=bMode
        if self.bModeAuto:
            self.func=func
            self.args=args
            self.kwargs=kwargs
            if self.cbConnect.GetValue()==False:
                self.__connect__()
                #self.OnCbConnectButton(None)
    def SetConnection(self,sHost,sPort,sUser):
        self.txtHost.SetValue(sHost)
        self.txtPort.SetValue(sPort)
        self.txtUser.SetValue(sUser)
        self.txtPasswd.SetValue('')
    def GetConnection(self):
        sHost=self.txtHost.GetValue()
        sPort=self.txtPort.GetValue()
        sUser=self.txtUser.GetValue()
        return sHost,sPort,sUser
    def GetLease(self):
        return 366
    def GetConnectionLst(self):
        return self.lConn
    def Clear(self):
        self.lConn=[]
        while self.qContent.empty()==False:
            self.qContent.get()
        self.lstPlugIn.DeleteAllItems()
        self.__showPlugIn__()
        
    def OnCbCloseButton(self, event):
        event.Skip()
        if self.timer is not None:
            self.timer.Stop()
            self.timer=None
        if self.bConnected:
            self.EndModal(2)
        else:
            self.EndModal(0)
        
    def __connect__(self):
        try:
            while self.qMsg.empty()==False:
                sMsg=self.qMsg.get(False)
            self.cbClose.Enable(False)
            self.bLogin=True
            self.bLoggedIn=False
            self.bBrowse=False
            self.bConnected=False
            sHost=self.txtHost.GetValue()
            sPort=self.txtPort.GetValue()
            self.lConn=[]
            self.cltServ=vSockClt(self,sHost,int(sPort),vSockXmlClt)
            self.timer = wx.PyTimer(self.__CheckQueue__)
            self.timer.Start(50)
            self.cltServ.Connect()
        except:
            sys.stderr.write(traceback.format_exc())
    def __connectionClosed__(self,bEstablished=False):
        if self.cltSock is not None:
            del self.cltSock
        self.cltSock=None
        if self.cltServ is not None:
            self.qMsg.put('closed')
            del self.cltServ
        self.cltServ=None
    def __close__(self):
        try:
            if self.cltSock is not None:
                self.cltSock.Close()
            else:
                self.bActive=False
        except:
            pass
        try:
            if self.cltServ is not None:
                self.cltServ.Stop()
            else:
                self.bActive=False
        except:
            pass
        self.bLogin=False
    def CloseConnection(self):
        self.__close__()
    def __CheckQueue__(self):
        while self.__ProcessQueue__():
            pass
        #self.__ProcessQueue__()
    def __ProcessQueue__(self):
        try:
            sMsg=self.qMsg.get(False)
            if type(sMsg)==types.TupleType:
                tMsg=sMsg
                sMsg=tMsg[0]
            if sMsg=='conn':
                img=images.getConnectBitmap()
                self.lblResult.SetLabel(_('connecting'))
            elif sMsg=='connFlt':
                img=images.getFaultBitmap()
                self.lblResult.SetLabel(_('connection fault'))
                self.cbConnect.SetValue(False)
                self.cbClose.Enable(True)
                self.timer.Stop()
            elif sMsg=='connOk':
                img=images.getLoginBitmap()
                self.lblResult.SetLabel(_('connection established'))
            elif sMsg=='closed':
                img=images.getStoppedBitmap()
                self.lblResult.SetLabel(_('connection closed'))
                self.bActive=False
                while self.qMsg.empty()==False:
                    self.qMsg.get()
                self.cbConnect.SetToggle(False)
                self.cbClose.Enable(True)
                self.timer.Stop()
            elif sMsg=='Browse':
                img=images.getRunningBitmap()
                self.__showPlugIn__()
                self.bConnected=True
            else:
                img=images.getErrorBitmap()
                self.lblResult.SetLabel(_('unknown')+' '+repr(sMsg))
            #if self.bCheckOk:
            #    img=images.getApplyBitmap()
            if img is not None:
                self.stbResult.SetBitmap(img)
                self.stbResult.Refresh()
            return True
        except Queue.Empty:
            return False
        except:
            sys.stderr.write(traceback.format_exc())
            return False
    def NotifyConnect(self):
        self.qMsg.put('conn')
    def NotifyConnectionFault(self):
        self.qMsg.put('connFlt')
    def NotifyConnected(self):
        try:
            self.cltSock=self.cltServ.GetSocketInstance()
            if self.cltSock is None:
                self.qMsg.put('connFlt')
                self.__close__()
                return
            usr=self.txtUser.GetValue()
            passwd=self.txtPasswd.GetValue()
            if len(passwd)>0:
                m=sha.new()
                m.update(passwd)
                passwd=m.hexdigest()
            else:
                passwd=''
            self.cltSock.SetParent(self)
            self.cltSock.HyperBrowse(usr,'')
            self.qMsg.put('connOk')
        except:
            #traceback.print_exc()
            self.qMsg.put('connFlt')
            self.__close__()
    def NotifyLogin(self):
        self.qMsg.put('login')
    def NotifyLoggedIn(self,flag):
        self.bLoggedIn=flag
        self.qMsg.put('loggedIn')
    def NotifyBrowse(self,l):
        l.sort()
        while self.qContent.empty()==False:
            t=self.qContent.get()
        for sIt in l:
            t=sIt.split(':')
            if len(t)==2:
                self.qContent.put((t[0],t[1]))
        self.qMsg.put('Browse')
    def NotifyServed(self,served):
        self.qMsg.put('Served')
    def OnCbFindButton(self, event):
        event.Skip()
    def OnCbConnectButton(self, event):
        #if event is not None:
        #    event.Skip()
        if self.cbConnect.GetValue():
            self.__connect__()
        else:
            self.__close__()
    def __showPlugIn__(self):
        try:
            #print '__showPlugIn__'
            self.lConn=[]
            self.lstPlugIn.DeleteAllItems()
            while self.qContent.empty()==False:
                sAppl,sAlias=self.qContent.get(False)
                #print '    ',sAppl,sAlias
                self.lConn.append((sAppl,sAlias))
                idx=self.lstPlugIn.InsertStringItem(sys.maxint,sAppl)
                self.lstPlugIn.SetStringItem(idx,1,sAlias)
            if self.bModeAuto:
                if self.func is not None:
                    self.func(*self.args,**self.kwargs)
                self.__close__()
        except:
            traceback.print_exc()
class vMESHyperBrowse(wx.Panel):
    def __init__(self,*_args,**_kwargs):
        try:
            sz=_kwargs['size']
        except:
            sz=wx.Size(100,30)
            _kwargs['size']=sz
        try:
            szCb=_kwargs['size_button']
            del _kwargs['size_button']
        except:
            szCb=wx.Size(24,24)
        try:
            szTree=_kwargs['size_tree']
            del _kwargs['size_tree']
        except:
            szTree=wx.Size(sz[0]-szCb[0]-2,sz[1]-4)
        apply(wx.Panel.__init__,(self,) + _args,_kwargs)
        self.SetAutoLayout(True)
        
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        size=(szTree[0],szTree[1])
        pos=(0,(sz[1]-szTree[1])/2)
        self.trAlias = wx.TreeCtrl(id=-1,
              name=u'trAlias', parent=self, pos=wx.DefaultPosition,
              size=wx.Size(-1, 200), style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT|wx.TR_LINES_AT_ROOT)
        size=(szCb[0],szCb[1])
        pos=(szTree[0]+(sz[0]-(szTree[0]+szCb[0]))/2,(sz[1]-szCb[1])/2)
        id=wx.NewId()
        self.cbPopup = wx.lib.buttons.GenBitmapToggleButton(ID=id,
              bitmap=wx.EmptyBitmap(16, 16), name=u'cbPopup',
              parent=self, pos=pos, size=size, style=wx.BU_AUTODRAW)
        #self.cbPopup.SetConstraints(LayoutAnchors(self.cbPopup, False, True,
        #      True, False))
        bxs.AddWindow(self.cbPopup, 0, border=0, flag=wx.ALIGN_CENTER)
        self.SetSizer(bxs)
        self.cbPopup.SetBitmapLabel(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.SetBitmapSelected(vtArt.getBitmap(vtArt.Down))
        self.cbPopup.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.cbPopup)#id=id)
        self.popWin=None
    def Enable(self,flag):
        wx.Panel.Enable(self,flag)
        self.cbPopup.Refresh()
    def __createPopup__(self):
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCS(vtLog.DEBUG,'__createPopup__',
                        origin=self.GetName())
        if self.popWin is None:
            sz=self.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self.popWin=vMESHyperBrowseDIALOG(self,sz,wx.SIMPLE_BORDER)
            except:
                vtLog.vtLngTB(self.GetName())
            #self.popWin.SetVal(self.txtVal.GetValue(),self.doc.GetLang())
        else:
            pass
    def OnPopupButton(self,evt):
        self.__createPopup__()
        if self.cbPopup.GetValue()==True:
            btn=self
            iX,iY = btn.ClientToScreen( (0,0) )
            iW,iH =  btn.GetSize()
            iPopW,iPopH=self.popWin.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY+=iH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self.popWin.Move((iX,iY))
            #self.popWin.Move((pos[0],pos[1]+sz[1]))
            self.popWin.Show(True)
        else:
            self.popWin.Show(False)
        evt.Skip()

