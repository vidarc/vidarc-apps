#Boa:FramePanel:vMESSettingsPanel
#----------------------------------------------------------------------------
# Name:         vMESSettingsPanel.py
# Purpose:      configuration panel
#
# Author:       Walter Obweger
#
# Created:      200060208
# CVS-ID:       $Id: vMESSettingsPanel.py,v 1.4 2007/04/18 15:51:42 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.filebrowsebutton

import sys,os.path
import traceback
import images

import IniFile

[wxID_VMESSETTINGSPANEL, wxID_VMESSETTINGSPANELCBADD, 
 wxID_VMESSETTINGSPANELCBDEL, wxID_VMESSETTINGSPANELDBBDN, 
 wxID_VMESSETTINGSPANELLSTDN, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vMESSettingsPanel(wx.Panel):
    CFG_PLUGIN='vMESCfgPlugIn.ini'
    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.dbbDN, (0, 0), border=2, flag=wx.EXPAND | wx.ALL,
              span=(1, 1))
        parent.AddWindow(self.cbAdd, (0, 2), border=2,
              flag=wx.TOP | wx.RIGHT | wx.BOTTOM, span=(1, 1))
        parent.AddWindow(self.cbDel, (1, 2), border=2,
              flag=wx.TOP | wx.RIGHT | wx.BOTTOM, span=(1, 1))
        parent.AddWindow(self.lstDN, (1, 0), border=2, flag=wx.EXPAND | wx.ALL,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)

        self._init_coll_gbsData_Items(self.gbsData)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_VMESSETTINGSPANEL,
              name=u'vMESSettingsPanel', parent=prnt, pos=wx.Point(378, 250),
              size=wx.Size(386, 237), style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(378, 210))
        self.SetAutoLayout(True)

        self.dbbDN = wx.lib.filebrowsebutton.DirBrowseButton(buttonText='...',
              dialogTitle=_(u'choose a directory'),
              id=wxID_VMESSETTINGSPANELDBBDN, labelText=_(u'directory:'),
              parent=self, pos=wx.Point(2, 2), size=wx.Size(283, 34),
              startDirectory='.', style=0)

        self.cbAdd = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESSETTINGSPANELCBADD,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Add'), name=u'cbAdd',
              parent=self, pos=wx.Point(297, 2), size=wx.Size(76, 30), style=0)
        self.cbAdd.SetMinSize((-1,-1))
        self.cbAdd.Bind(wx.EVT_BUTTON, self.OnCbAddButton,
              id=wxID_VMESSETTINGSPANELCBADD)

        self.cbDel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VMESSETTINGSPANELCBDEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Del'), name=u'cbDel',
              parent=self, pos=wx.Point(297, 40), size=wx.Size(76, 30),
              style=0)
        self.cbDel.SetMinSize((-1,-1))
        self.cbDel.Bind(wx.EVT_BUTTON, self.OnCbDelButton,
              id=wxID_VMESSETTINGSPANELCBDEL)

        self.lstDN = wx.ListCtrl(id=wxID_VMESSETTINGSPANELLSTDN, name=u'lstDN',
              parent=self, pos=wx.Point(2, 40), size=wx.Size(283, 88),
              style=wx.LC_REPORT | wx.LC_SORT_ASCENDING)
        self.lstDN.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnLstDNListItemSelected,
              id=wxID_VMESSETTINGSPANELLSTDN)
        self.lstDN.Bind(wx.EVT_LIST_ITEM_DESELECTED,
              self.OnLstDNListItemDeselected, id=wxID_VMESSETTINGSPANELLSTDN)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
        self.gbsData.AddGrowableRow(1)
        self.gbsData.AddGrowableCol(0)
        
        self.lstDN.InsertColumn(0,_(u'path'),wx.LIST_FORMAT_LEFT,250)
        self.cbAdd.SetBitmapLabel(images.getAddAttrBitmap())
        self.cbDel.SetBitmapLabel(images.getDelAttrBitmap())
        
        self.Move(pos)
        self.SetSize(size)
        self.SetupImageList()
    def SetupImageList(self):
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        self.imgDict['dnOk']=self.imgLstTyp.Add(images.getApplyBitmap())
        self.imgDict['dnFlt']=self.imgLstTyp.Add(images.getCancelBitmap())
        self.lstDN.SetImageList(self.imgLstTyp,wx.IMAGE_LIST_SMALL)
    def Clear(self):
        self.lstDN.DeleteAllItems()
        self.dbbDN.SetValue('')
    def SetCfg(self):
        try:
            self.Clear()
            
            cfg=IniFile.Config()
            try:
                cfg.readFN(self.CFG_PLUGIN)
                section=cfg.getSection('plugins')
                try:
                    i=0
                    for i in xrange(999):
                        sDN=section.getInfo('path_%03d'%i)
                        if sDN is None:
                            break
                        sDN=sDN.strip()
                        if os.path.exists(sDN):
                            self.__insertDN__(sDN)
                except:
                    pass
                    traceback.print_exc()
            except:
                traceback.print_exc()
                section=cfg.getSection('plugins')
                inf=open(self.CFG_PLUGIN)
                lines=inf.readlines()
                inf.close()
                try:
                    for l in lines:
                        print l
                        sDN=l.strip()
                        if os.path.exists(sDN):
                            self.__insertDN__(sDN)
                except:
                    pass
                    traceback.print_exc()
            #tmp=self.doc.getChild(self.node,'pluginDNs')
            #if tmp is not None:
            #    for c in self.doc.getChilds(tmp,'pluginDN'):
            #        sVal=self.doc.getText(c)
            #        self.__insertDN__(sVal)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
            traceback.print_exc()
    def __insertDN__(self,sVal):
        try:
            if os.path.isdir(sVal):
                img=self.imgDict['dnOk']
            else:
                img=self.imgDict['dnFlt']
        except:
            img=-1
        if len(sVal)>0:
            idx=self.lstDN.InsertImageStringItem(sys.maxint,sVal,img)
    def GetCfg(self):
        try:
            cfg=IniFile.Config()
            try:
                cfg.readFN(self.CFG_PLUGIN)
            except:
                pass
            section=cfg.getSection('plugins')
            try:
                for i in xrange(999):
                    sDN=section.remove_option('path_%03d'%i)
            except:
                pass
            for i in range(self.lstDN.GetItemCount()):
                sVal=self.lstDN.GetItemText(i)
                section.setInfo('path_%03d'%i,sVal)
            
            cfg.writeFN(self.CFG_PLUGIN)
            #outf=open(self.CFG_PLUGIN,'w')
            #for i in range(self.lstDN.GetItemCount()):
            #    sVal=self.lstDN.GetItemText(i)
            #    outf.write(sVal+'\n')
            #outf.close()
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
            traceback.print_exc()
    def OnCbAddButton(self, event):
        try:
            sVal=self.dbbDN.GetValue()
            self.__insertDN__(sVal)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
        event.Skip()
    def OnCbDelButton(self, event):
        iSel=-1
        for i in range(self.lstDN.GetItemCount()):
            if self.lstDN.GetItemState(i,wx.LIST_STATE_SELECTED)>0:
                iSel=i
                break
        #iSel=self.lstDN.GetSelection()
        if iSel>=0:
            self.lstDN.DeleteItem(iSel)
        event.Skip()

    def OnLstDNListbox(self, event):
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,self.lstDN.GetSelection(),self)
            pass
        except:
            pass
        event.Skip()

    def OnLstDNListItemSelected(self, event):
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            idx=event.GetIndex()
            it=self.lstDN.GetItem(idx,0)
            sDN=it.m_text
            self.dbbDN.SetValue(sDN)
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
        event.Skip()

    def OnLstDNListItemDeselected(self, event):
        try:
            #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            self.dbbDN.SetValue('')
        except:
            #vtLog.vtLngTB(self.GetName())
            pass
        event.Skip()

