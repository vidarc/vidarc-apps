#----------------------------------------------------------------------------
# Name:         vXmlDomBasic.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20080806
# CVS-ID:       $Id: vXmlDomBasic.py,v 1.1 2008/08/13 00:11:44 wal Exp $
# Copyright:    (c) 2008 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import codecs,types,os,stat
import traceback
from platform import python_version_tuple
if python_version_tuple() >= ['2','5']:
    import xml.etree.cElementTree as ET
else:
    import cElementTree as ET

VERBOSE=0
if VERBOSE:
    import pprint

class vXmlDomBasic:
    def __init__(self):
        self.doc=None
        self.root=None
        self.sFN=None
        self.applEncoding='utf-8'
        self.xmlEncoding='ISO-8859-1'
    def __parseFile__(self,sFN,sz=1024):
        try:
            #if self.doc is not None:
            #    del self.doc
            self.doc=None
            self.root=None
            st=os.stat(sFN)
            iCount=st[stat.ST_SIZE]
            if iCount==0:
                raise IOError
            self.doc=ET.parse(sFN)
            return self.doc
        except:
            self.doc=None
            raise IOError
        return self.doc
    def Open(self,sFN):
        self.sFN=sFN[:]
        return self.open(self.sFN)
    def open(self,sFN):
        try:
            self.__parseFile__(sFN)
        except Exception,list:
            traceback.print_exc()
            return -1
        try:
            if self.doc is None:
                self.root=None
            else:
                self.root=self.doc.getroot()
        except:
            pass
        return 0
    def close(self):
        del self.doc
        self.doc=None
        self.root=None
    def __Close__(self):
        pass
    def Close(self,bAcquire=True,bThread=False):
        try:
            self.close()
            self.__Close__()
        except:
            traceback.print_exc()
    def save(self,sTmpFN,encoding='ISO-8859-1'):
        """ make sure 'dom'- is acquired and sTmpFN is valid.
        """
        if self.doc is None:
            return 0
        try:
            self.doc.write(sTmpFN,encoding)
            return 1
        except:
            pass
        return -1
    def Save(self,fn=None,encoding='ISO-8859-1'):
        if self.root is None:
            return -3
        if self.doc is None:
            return -4
        if fn is None:
            sTmpFN=self.sFN
        else:
            sTmpFN=fn
            self.sFN=sTmpFN[:]
        if sTmpFN is None:
            return -1
        
        iRet=self.save(sTmpFN,encoding=encoding)
        return iRet
    def getRoot(self):
        return self.root
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.getChild(self.root,'softwares')
    def procChildsExt(self,node,func,*args,**kwargs):
        if node is None:
            return -2
        for tmp in node:
            if func(tmp,*args,**kwargs)<0:
                return -1
        return 0
    def getTagName(self,node):
        if node is None:
            return ''
        return node.tag
    def getText(self,node):
        s=u''
        if node is None:
            return s
        #return node.text
        if node.text is None:
            return u''
        s=node.text
        if type(s)==types.UnicodeType:
            return s
        return node.text.decode(self.applEncoding)
    def setText(self,node,newValue):
        if node is None:
            return -1
        if node.text is None:
            iRet=0
        else:
            if len(node.text)>0:
                iRet=1
            else:
                iRet=0
        if type(newValue)==types.StringType:
            node.text=newValue.encode(self.applEncoding)
        else:
            node.text=newValue
        return iRet
    def getNodeText(self,node,tagName):
        return self.getText(self.getChild(node,tagName))
    def setNodeText(self,node,tagName,newValue):
        if node is None:
            return
        nNode=self.getChild(node,tagName)
        if nNode is not None:
            iRet=self.setText(nNode,newValue)
            if iRet==1:
                return 1
            elif iRet==0:
                return 3
            return iRet
        else:
            nNode=ET.SubElement(node,tagName)
            self.setText(nNode,newValue)
            return 0
            #self.setNodeText(node,tagName,newValue)
    def hasAttribute(self,node,attr):
        if node is None:
            return -1
        if attr in node.attrib:
            return 1
        if attr is None:
            if len(node.attrib)>0:
                return 1
        return 0
    def getAttribute(self,node,attr):
        if node is None:
            return ''
        if attr in node.attrib:
            return node.attrib[attr]
        return ''
    def getChildAttribute(self,node,tagName,attr):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        for tmp in node:
            if tmp.tag==tagName:
                if attr in tmp.attrib:
                    return tmp.attrib[attr]
        return None
    def getChild(self,node,tagName):
        if node is None:
            node=self.getBaseNode()
        if node is None:
            return None
        for tmp in node:
                if tagName is not None:
                    if tmp.tag==tagName:
                        return tmp
                else:
                    return tmp
        return None
    def getChildForced(self,node,tagName):
        if node is None:
            node=self.getBaseNode()
        if node is None:
            return None
        n=self.getChild(node,tagName)
        if n is None:
            n=self.createSubNode(node,tagName,True)
        return n
    def __getChilds__(self,node,tagName=None):
        lst=[]
        if node is None:
            return lst
        for tmp in node:
                bFound=False
                if tagName is not None:
                    if tmp.tag==tagName:
                        bFound=True
                else:
                    bFound=True
                if bFound:
                    lst.append(tmp)
        return lst
    def getChilds(self,node=None,tagName=None):
        if node is None:
            node=self.getBaseNode()
        return self.__getChilds__(node,tagName)
    def setChildAttribute(self,node,tagName,attr,attrVal):
        if node is None:
            node=self.getBaseNode()#self.root
        if node is None:
            return None
        for tmp in node:
            if tmp.tag==tagName:
                if attr in tmp.attrib:
                    tmp.attrib[attr]=attrVal
                    return 0
                else:
                    tmp.attrib[attr]=attrVal
                    if attr==self.attr:
                        self.iElemIDCount+=1
                    return 1
        return None
    def setAttribute(self,node,attr,attrVal):
        if node is None:
            return -1
        if attr in node.attrib:
            node.attrib[attr]=attrVal
            return 0
        else:
            node.attrib[attr]=attrVal
            if attr==self.attr:
                self.iElemIDCount+=1
            return 1
    def removeAttribute(self,node,attr):
        if node is None:
            return -1
        if attr in node.attrib:
            del node.attrib[attr]
            if attr==self.attr:
                self.iElemIDCount-=1
        return 0
    def New(self,revision='1.0',root=None,bConnection=False,bAcquire=True,bThread=False):
        if root is None:
            root=self.TAGNAME_ROOT
        self.close()
        try:
            self.root=ET.Element(root)
            self.doc=ET.ElementTree(self.root)
            try:
                self.__New__()
            except:
                traceback.print_exc()
            self.AlignDoc()
        except:
            traceback.print_exc()
    def __New__(self):
        pass
    def createNew(self,root,revision='0.0.1'):
        self.root = ET.Element(root)
        self.doc = ET.ElementTree(self.root)
    def createSubNode(self,node,tagname,align=False):
        n=ET.SubElement(node,tagname)
        return n
    def createSubNodeAttr(self,node,tagname,attr,attrVal,align=False):
        n=ET.SubElement(node,tagname)
        n.attrib[attr]=attrVal
        return n
    def createSubNodeText(self,node,tagname,val,align=False):
        n=ET.SubElement(node,tagname)
        n.text=val.encode(self.applEncoding)
        return n
    def createSubNodeTextAttr(self,node,tagname,val,attr,attrVal,align=False):
        n=ET.SubElement(node,tagname)
        n.attrib[attr]=attrVal
        n.text=val.encode(self.applEncoding)
        return n
    def createNode(self,tagname):
        n=ET.Element(tagname)
        return n
    def createSubNodeDict(self,node,dict,align=False):
        if node is None:
            return
        t=dict['tag']
        if dict.has_key('val'):
            v=dict['val']
            if dict.has_key('attr'):
                c=self.createSubNodeTextAttr(node,t,v,dict['attr'][0],dict['attr'][1],align=align)
            else:
                c=self.createSubNodeText(node,t,v,align=align)
        else:
            c=self.createSubNode(node,t,align=align)
            if dict.has_key('attr'):
                self.setAttribute(c,dict['attr'][0],dict['attr'][1])
        if dict.has_key('lst'):
            for i in dict['lst']:
                self.createSubNodeDict(c,i,align=align)
        return c
    def Align(self,node,iLevel,s,ident,verbose=0,iAct=0,iSkip=0,iRec=1000):
        prevNodeType=-1
        sNew=s+ident
        iOldAct=iAct
        if node.tail is None or node.tail.strip()=='':
            node.tail=s
        o=None
        for o in node:
            if iRec>0:
                self.Align(o,iLevel+1,sNew,ident,verbose=verbose,
                        iAct=iAct+1,iSkip=iSkip,
                        iRec=iRec-1)
        if o is not None:
            if o.tail is None or o.tail.strip()=='':
                o.tail=s
            if node.text is None or node.text.strip()=='':
                node.text=sNew
    def AlignDoc(self,verbose=0):
        if self.root is None:
            return
        self.Align(self.root,0,'\n','  ',verbose)
    def deleteNode(self,node,nodePar=None):
        self.unlinkNode(node,nodePar=nodePar)
    def cloneNode(self,node,deep):
        if deep:
            tmp=self.cloneNode(node,False)
            for c in node:
                tmp.append(self.cloneNode(c,True))
        else:
            tmp=node.makeelement(node.tag,node.attrib)
            if node.text:
                tmp.text=node.text[:]
            if node.tail:
                tmp.tail=node.tail[:]
        return tmp
    def unlinkNode(self,node,nodePar=None):
        nodePar.remove(node)
