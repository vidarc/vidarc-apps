# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vScreenDummy.py
# Purpose:      screen functions for unknown platform
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vScreenDummy.py,v 1.3 2016/02/08 06:01:30 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vLogFallBack

try:
    def getScreenDict(sScreen=None,thd=None):
        return None
except:
    vLogFallBack.logTB(__name__)
