#----------------------------------------------------------------------------
# Name:         vLogFallBack.py
# Purpose:      logging interface with capaility to fallback to stdout/stderr
#               output in case vtLog is missing
#
# Author:       Walter Obweger
#
# Created:      20070710
# CVS-ID:       $Id: vLogFallBack.py,v 1.18 2018/01/07 05:45:58 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,time,logging,traceback
import threading
import vtLogDef


gLogFBLock=threading.Lock()
gLogFB=None

class vLogFallBack:
    def __init__(self,bLogDbg=False,bLogInf=True,bLogErr=True,bForceStd=False):
        global gLogFBLock
        self.semWrite=gLogFBLock#threading.Semaphore()
        self.setLogLevel(bLogDbg,bLogInf,bLogErr)
        if bForceStd:
            self.__setFB__()
        else:
            self.doImport()
    def setLogLevel(self,bLogDbg=False,bLogInf=True,bLogErr=True):
        self.semWrite.acquire()
        try:
            self._bLogDbg=bLogDbg
            self._bLogInf=bLogInf
            self._bLogErr=bLogErr
        except:
            pass
        self.semWrite.release()
    def isLoggedDbg(self):
        self.semWrite.acquire()
        bRet=self._bLogDbg
        self.semWrite.release()
        return bRet
    def isLoggedInf(self):
        self.semWrite.acquire()
        bRet=self._bLogInf
        self.semWrite.release()
        return bRet
    def isLoggedErr(self):
        self.semWrite.acquire()
        bRet=self._bLogErr
        self.semWrite.release()
        return bRet
    def logStop(self):
        self.__logStopFile__()
        self.__logStop__()
    def logIsStopped(self):
        return self.__logIsStoppedFile__ or self.__logIsStopped__
    def __setFB__(self):
        #print '========================================='
        #traceback.print_stack(file=sys.stdout)
        #print '========================================='
        self.logDebug=self.__logDebugFB
        self.logInfo=self.__logInfoFB
        self.logError=self.__logErrorFB
        self.logTB=self.__logTBFB
        self.logGetFN=self.__logGetFNFB__
        self.__log__=self.__logFB__
        self.__logTrend__=self.__logTrendFB__
        self.__logTrendStr__=self.__logTrendStrFB__
        self.__logStopFile__=self.__logStopFileFB__
        self.__logStop__=self.__logStopFB__
        self.__logIsStoppedFile__=self.__logIsStoppedFileFB__
        self.__logIsStopped__=self.__logIsStoppedFB__
        self._iLogDbg=logging.DEBUG
        self._iLogInf=logging.INFO
        self._iLogErr=logging.ERROR
    def doImport(self):
        try:
            #print '========================================='
            #traceback.print_stack(file=sys.stdout)
            #print '========================================='
            import vidarc.tool.log.vtLog as vtLog
            try:
                #if isLoggedDbg():
                global gLogFB
                if gLogFB is not None:
                    if vtLog.LEVEL==vtLog.DEBUG:
                        logDebug('logfn:%s;name:%s;fn:%s;lv:%d;port:%d;dn:%s'%(vtLog.vtLngGetFN(),
                                vtLogDef.NAME,vtLogDef.NAME+'.log',vtLogDef.LEVEL,vtLogDef.PORT,
                                vtLogDef.DN))
                iRes=vtLog.vtLngInit(vtLogDef.NAME,
                        vtLogDef.NAME+'.log',vtLogDef.LEVEL,
                        iSockPort=vtLogDef.PORT,
                        sLogDN=vtLogDef.DN)
                #print iRes
                #print vtLog.vtLngGetFN()
                #if isLoggedDbg():
                if gLogFB is not None:
                    if vtLog.LEVEL==vtLog.DEBUG:
                        logDebug('logfn:%s;res:%d'%(vtLog.vtLngGetFN(),iRes))
            except:
                #print 'error'
                #traceback.print_exc(file=sys.stderr)
                pass
            self.logDebug=self.__logDebugLog
            self.logInfo=self.__logInfoLog
            self.logError=self.__logErrorLog
            self.logTB=self.__logTBLog
            self.logGetFN=vtLog.vtLngGetFN
            self.__logTB__=vtLog.vtLngTB
            self.__log__=vtLog.vtLngCur
            self.__logTrend__=vtLog.vtLngNumTrend
            self.__logTrendStr__=vtLog.vtLngGetTrendOrderedStr
            self.__logStopFile__=vtLog.vtLngStopFile
            self.__logStop__=vtLog.vtLngStop
            try:
                self.__logIsStoppedFile__=vtLog.vtLngIsStoppedFile
            except:
                self.__logIsStoppedFile__=self.__logIsStoppedFileFB__
            try:
                self.__logIsStopped__=vtLog.vtLngIsStopped
            except:
                self.__logIsStopped__=self.__logIsStoppedFB__
            
            self._bLogDbg=vtLog.vtLngIsLogged(vtLog.DEBUG)
            self._bLogInf=vtLog.vtLngIsLogged(vtLog.INFO)
            self._bLogErr=vtLog.vtLngIsLogged(vtLog.ERROR)
            self._iLogDbg=vtLog.DEBUG
            self._iLogInf=vtLog.INFO
            self._iLogErr=vtLog.ERROR
        except:
            self.__setFB__()
        #try:
        #    global gLogFB
        #    if gLogFB is not None:
        #        gLogFB.doImport()
        #except:
        #    traceback.print_exc(file=sys.stderr)
    def getNumDiffStr(self):
        try:
            self.__logTrend__()
            s=self.__logTrendStr__()
        except:
            self.logTB()
            s=u''
        return s
    def __getTime(self):
        s=time.strftime('%Y-%m-%d %H:%M:%S\n',time.localtime(time.time()))
        return s
    def __logDebugLog(self,sMsg,origin='vLogFallBack',level2skip=2):
        if self._bLogDbg==False:
            return
        self.__log__(self._iLogDbg,sMsg,origin,level2skip=level2skip)
    def __logInfoLog(self,sMsg,origin='vLogFallBack',level2skip=2):
        if self._bLogInf==False:
            return
        self.__log__(self._iLogInf,sMsg,origin,level2skip=level2skip)
    def __logErrorLog(self,sMsg,origin='vLogFallBack',level2skip=2):
        if self._bLogErr==False:
            return
        self.__log__(self._iLogErr,sMsg,origin,level2skip=level2skip)
    def __logTBLog(self,origin='vLogFallBack'):
        self.__logTB__(origin)
    def __logTrendFB__(self):
        pass
    def __logTrendStrFB__(self):
        return u''
    def __logGetNumDiffOrderedStrFB__(self):
        return u''
    def __logGetNumDiffSumFB__(self):
        return 0
    def __logNumStoreFB__(self):
        pass
    def __logFB__(self,lv,msg,wid,level2skip=1):
        pass
    def __logDebugFB(self,sMsg,origin='vLogFallBack',level2skip=2):
        if self._bLogDbg==False:
            return
        self.semWrite.acquire()
        try:
            sys.stdout.write(self.__getTime())
            traceback.print_stack(file=sys.stdout)
            sys.stdout.write('debug:')
            sys.stdout.write(sMsg.replace(';','\n'))
            sys.stdout.write('\n\n')
        except:
            pass
        self.semWrite.release()
    def __logInfoFB(self,sMsg,origin='vLogFallBack',level2skip=2):
        if self._bLogInf==False:
            return
        self.semWrite.acquire()
        try:
            sys.stdout.write(self.__getTime())
            traceback.print_stack(file=sys.stdout)
            sys.stdout.write('info:')
            sys.stdout.write(sMsg.replace(';','\n'))
            sys.stdout.write('\n\n')
        except:
            pass
        self.semWrite.release()
    def __logErrorFB(self,sMsg,origin='vLogFallBack',level2skip=2):
        if self._bLogErr==False:
            return
        self.semWrite.acquire()
        try:
            sys.stderr.write(self.__getTime())
            traceback.print_stack(file=sys.stderr)
            sys.stderr.write('info:')
            sys.stderr.write(sMsg.replace(';','\n'))
            sys.stderr.write('\n\n')
        except:
            pass
            traceback.print_exc(file=sys.stderr)
        self.semWrite.release()
    def __logTBFB(self,origin='vLogFallBack',level2skip=2):
        self.semWrite.acquire()
        try:
            sys.stderr.write(self.__getTime())
            traceback.print_exc(file=sys.stderr)
            #sys.stderr.write(traceback.format_exc())
            sys.stderr.write('\n\n')
        except:
            pass
        self.semWrite.release()
    def __logStopFileFB__(self):
        pass
    def __logStopFB__(self):
        pass
    def __logIsStoppedFileFB__(self):
        return True
    def __logIsStoppedFB__(self):
        return True
    def __logGetFNFB__(self):
        return None


def init(bForceStd=False):
    global gLogFB
    if gLogFB is None:
        gLogFB=vLogFallBack(vtLogDef.LEVEL==vtLogDef.DEBUG,vtLogDef.LEVEL<=vtLogDef.INFO,
                    vtLogDef.LEVEL<=vtLogDef.ERROR,bForceStd=bForceStd)

def updateLogLevel():
    global gLogFB
    if gLogFB is not None:
        gLogFB.setLogLevel(vtLogDef.LEVEL==vtLogDef.DEBUG,vtLogDef.LEVEL<=vtLogDef.INFO,
                    vtLogDef.LEVEL<=vtLogDef.ERROR)

def isLoggedDbg():
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        return gLogFB.isLoggedDbg()
    return False

def isLoggedInf():
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        return gLogFB.isLoggedInf()
    return False

def isLoggedErr():
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        return gLogFB.isLoggedErr()
    return False

def logDebug(sMsg,origin='vLogFallBack',level2skip=3):
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        gLogFB.logDebug(sMsg,origin=origin,level2skip=level2skip)

def logInfo(sMsg,origin='vLogFallBack',level2skip=3):
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        gLogFB.logInfo(sMsg,origin=origin,level2skip=level2skip)

def logError(sMsg,origin='vLogFallBack',level2skip=3):
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        gLogFB.logError(sMsg,origin=origin,level2skip=level2skip)

def logTB(origin='vLogFallBack'):
    global gLogFB
    if gLogFB is None:
        init()
    if gLogFB is not None:
        gLogFB.logTB(origin)
