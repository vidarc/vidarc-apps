#----------------------------------------------------------------------------
# Name:         vMESHyperBrowseTree.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20070105
# CVS-ID:       $Id: vMESHyperBrowseTree.py,v 1.3 2007/10/25 15:34:46 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import sys,traceback,copy,types

import vtLgBase
import cStringIO
from wx import BitmapFromImage as wxBitmapFromImage
from wx import ImageFromStream as wxImageFromStream
from wx import EmptyIcon as wxEmptyIcon

def getPluginImage():
    return None

def getApplicationIcon():
    return None
    
def create(parent,fldBar):
    pass

import images as imgMES

class vMESHyperBrowseTree(wx.TreeCtrl):
    def __init__(self,parent,id=-1,pos=wx.DefaultPosition,size=wx.Size(-1, 200),style=0,name=u'trHyperBrowse'):
        wx.TreeCtrl.__init__(self,id=id,
              name=name, parent=parent, pos=pos,
              size=size, style=wx.TR_HAS_BUTTONS|wx.TR_HIDE_ROOT|wx.TR_LINES_AT_ROOT)
        self.Bind(wx.EVT_LEFT_DCLICK, self.OnTrLeftDclick)
        self.func=None
        self.args=()
        self.kwargs={}
        
        self.dTiGrp={}
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        img=imgMES.getModuleBitmap()
        self.imgDict['root']=self.imgLstTyp.Add(img)
        self.imgDict['mod']=self.imgLstTyp.Add(img)
        img=imgMES.getGroupingBitmap()
        self.imgDict['grp']=self.imgLstTyp.Add(img)
        dLg={}
        for lg,bmp in [('en',imgMES.getLangEnBitmap()),
                    ('de',imgMES.getLangDeBitmap()),
                    ('fr',imgMES.getLangFrBitmap()),
                    ('it',imgMES.getLangItBitmap()),
                    ('ne',imgMES.getLangNeBitmap()),
                    ('se',imgMES.getLangSeBitmap()),]:
            dLg[lg]=self.imgLstTyp.Add(bmp)
        self.imgDict['lang']=dLg
        self.SetImageList(self.imgLstTyp)
        img=self.imgDict['root']
        self.AddRoot(_('connections'))
        self.bAdvanced=False
    def AddPlugIn(self,oPlugIn):
        try:
            sName=oPlugIn.GetName()
            bmp=oPlugIn.GetBitmap()
            if bmp is not None:
                self.imgDict[sName]=self.imgLstTyp.Add(bmp)
            lImg=oPlugIn.GetDomainImageDataLst()
            if lImg is not None:
                for sName,imgDataFunc in lImg:
                    if sName not in self.imgDict:
                        stream = cStringIO.StringIO(imgDataFunc())
                        img=wxImageFromStream(stream)
                        bmp=wxBitmapFromImage(img)
                        self.imgDict[sName]=self.imgLstTyp.Add(bmp)
        except:
            sys.stderr.write(traceback.format_exc())
    def __sortTree__(self,ti):
        try:
            triChild=self.GetFirstChild(ti)
            while triChild[0].IsOk():
                if self.ItemHasChildren(triChild[0]):
                    self.__sortTree__(triChild[0])
                    self.SortChildren(triChild[0])
                triChild=self.GetNextChild(ti,triChild[1])
            #if self.ItemHasChildren(ti):
            self.SortChildren(ti)
        except:
            sys.stderr.write(traceback.format_exc())
            pass
    def SetLaunchFunc(self,func,*args,**kwargs):
        self.func=func
        self.args=args
        self.kwargs=kwargs
    def SetAdvanced(self,flag):
        self.bAdvanced=flag
    def Clear(self):
        #self.oCryptoStore=None
        self.dTiGrp={}
        tp=self.GetRootItem()
        self.DeleteChildren(tp)
    def ShowHyperBrowse(self,dPlugIn,lConn,lGrpLv,sGrpSep,sHost,sPort,sLogin):
        self.Clear()
        tp=self.GetRootItem()
        #self.SetImageList(self.imgLstTyp)
        try:
            lang=vtLgBase.getApplLang()
            ti=None
            tic=None
            lTiGrp=[]
            sApplOld=''
            idxBmp=-1
            try:
                idxGrp=self.imgDict['grp']
            except:
                idxGrp=-1
            for sAppl,sAlias in lConn:
                if sApplOld!=sAppl:
                    if sAppl not in dPlugIn:
                        continue
                    tp=self.GetRootItem()
                    t=dPlugIn[sAppl]
                    if len(t)<2:
                        continue
                    plugin=t[2]
                    tid=wx.TreeItemData()
                    tid.SetData(plugin)
                    sName=plugin.GetName()
                    lTrans=plugin.GetDomainTransLst(lang)
                    lImgs=plugin.GetDomainImageDataLst()
                    if lImgs is not None:
                        ti=tp
                        for i in xrange(len(lTrans)):
                            sGrpName='.'.join([lImgs[j][0] for j in xrange(i+1)])
                            if sGrpName in self.dTiGrp:
                                ti=self.dTiGrp[sGrpName]
                            else:
                                sNameImg=lImgs[i][0]
                                if sNameImg in self.imgDict:
                                    img=self.imgDict[sNameImg]
                                else:
                                    img=self.imgDict[sName]
                                tidd=wx.TreeItemData()
                                ti=self.AppendItem(ti,lTrans[i],img,img,tidd)
                                self.dTiGrp[sGrpName]=ti
                        tp=ti
                        self.SetItemData(ti,tid)
                    else:
                        idxBmp=self.imgDict[plugin.GetName()]
                        
                        ti=self.AppendItem(tp,sAppl,idxBmp,idxBmp,tid)
                    sApplOld=sAppl
                    lTiGrp=[]
                    lGrpAct=[]
                sConn=''.join([sHost,':',sPort,'//',sLogin,'@',sAlias])
                tid=wx.TreeItemData()
                tid.SetData(sConn)
                i=sConn.find(':')
                j=sConn.rfind('@')
                if i>0 and j>0:
                    if self.bAdvanced:
                        #sConnDisp=sConn
                        sConnDisp=sConn[j+1:]
                    else:
                        sConnDisp=sConn[j+1:]#+' '+sConn[:i]
                    sAlias=sConn[j+1:]
                else:
                    sAlias=sConn
                strs=sAlias.split(sGrpSep)
                lGrp=[]
                for s in strs:
                    iStart=0
                    for iGrp in xrange(3):
                        if lGrpLv[iGrp]>0:
                            if len(s)>(iStart+lGrpLv[iGrp]):
                                lGrp.append(s[:iStart+lGrpLv[iGrp]])
                                iStart+=lGrpLv[iGrp]
                        else:
                            break
                    lGrp.append(s)
                tig=ti
                lMerge=zip(lGrp,lGrpAct,lTiGrp)
                iCount=len(lGrp)
                iGrp=0
                iGrpMatch=-1
                for iGrp in xrange(len(lMerge)):
                    if lMerge[iGrp][0]!=lMerge[iGrp][1]:
                        break
                    else:
                        iGrpMatch=iGrp
                    tig=lMerge[iGrp][2]
                iGrp=iGrpMatch+1
                lGrpAct=lGrpAct[:iGrp]
                lTiGrp=lTiGrp[:iGrp]
                for i in xrange(iGrp,iCount-1):
                    tidd=wx.TreeItemData()
                    tig=self.AppendItem(tig,lGrp[i],idxGrp,idxGrp,tidd)
                    lGrpAct.append(lGrp[i])
                    lTiGrp.append(tig)
                #tic=self.trConn.AppendItem(ti,sAlias,idxBmp,idxBmp,tid)
                tic=self.AppendItem(tig,sConnDisp,-1,-1,tid)
            tp=self.GetRootItem()
            self.__sortTree__(tp)
        except:
            sys.stderr.write(traceback.format_exc())
    def OnTrLeftDclick(self, evt):
        try:
            ti=self.GetSelection()
            tid=self.GetPyData(ti)
            if tid is not None:
                if type(tid)!=types.StringType and type(tid)!=types.UnicodeType:
                    #self.GetParent().PrintMsg(_('Only leaf can be launched.'))
                    evt.Skip()
                    return
                oPlugIn=None
                tip=ti
                while oPlugIn is None:
                    tip=self.GetItemParent(tip)
                    oPlugIn=self.GetPyData(tip)
                
                dLaunch={'plugIn':oPlugIn}
                dLaunch['lstNames']=oPlugIn.GetDomainNameLst()
                dLaunch['connection']=copy.deepcopy(tid)
                if self.func is not None:
                    self.func(dLaunch,oPlugIn,*self.args,**self.kwargs)
            else:
                evt.Skip()
        except:
            sys.stderr.write(traceback.format_exc())

