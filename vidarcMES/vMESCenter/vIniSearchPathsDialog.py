#Boa:Dialog:vIniSearchPathsDialog
#----------------------------------------------------------------------------
# Name:         vMESSettingsDialog.py
# Purpose:      configuration dialog
#
# Author:       Walter Obweger
#
# Created:      200060208
# CVS-ID:       $Id: vIniSearchPathsDialog.py,v 1.1 2007/07/10 10:30:01 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import sys,traceback
from vIniSearchPathsPanel import vIniSearchPathsPanel
import images

def create(parent):
    return vIniSearchPathsDialog(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VINISEARCHPATHSDIALOG, wxID_VINISEARCHPATHSDIALOGCBCANCEL, 
 wxID_VINISEARCHPATHSDIALOGCBOK, 
] = [wx.NewId() for _init_ctrls in range(3)]

class vIniSearchPathsDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbOk, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_gbsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsBt, (1, 0), border=0, flag=wx.ALIGN_CENTER,
              span=(1, 1))

    def _init_sizers(self):
        # generated method, don't edit
        self.gbsData = wx.GridBagSizer(hgap=0, vgap=4)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_gbsData_Items(self.gbsData)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.gbsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VINISEARCHPATHSDIALOG,
              name=u'vIniSearchPathsDialog', parent=prnt, pos=wx.Point(434,
              167), size=wx.Size(400, 316),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vMES Search Paths')
        self.SetClientSize(wx.Size(392, 289))

        self.cbOk = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VINISEARCHPATHSDIALOGCBOK, label=_(u'Ok'),
              name=u'cbOk', parent=self, pos=wx.Point(0, 24), size=wx.Size(76,
              30), style=0)
        self.cbOk.SetMinSize((-1,-1))
        self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,
              id=wxID_VINISEARCHPATHSDIALOGCBOK)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VINISEARCHPATHSDIALOGCBCANCEL, label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(108, 24),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize((-1,-1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VINISEARCHPATHSDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.cbOk.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        try:
            self.gbsData.AddGrowableCol(0)
            self.gbsData.AddGrowableRow(0)
            
            self.pnSettings=vIniSearchPathsPanel(self,-1,(4,4),(384,240),0,'pnSettings')
            self.gbsData.AddWindow(self.pnSettings, (0, 0), border=4,
                  flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND, span=(1, 1))
            self.gbsData.Layout()
            self.gbsData.Fit(self)
        except:
            sys.stderr.write(traceback.format_exc())
        self.cbOk.SetBitmapLabel(images.getApplyBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
    def SetIniFN(self,fn):
        self.pnSettings.SetIniFN(fn)
    def SetCfg(self):
        self.pnSettings.SetCfg()
    def OnCbOkButton(self, event):
        self.pnSettings.GetCfg()
        self.EndModal(1)
        event.Skip()

    def OnCbCancelButton(self, event):
        self.EndModal(0)
        event.Skip()
