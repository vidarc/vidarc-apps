#Boa:Dialog:vMESPlugInUpdateDialog
#----------------------------------------------------------------------------
# Name:         vMESPlugInUpdateDialog.py
# Purpose:      plugin update dialog
#
# Author:       Walter Obweger
#
# Created:      200060208
# CVS-ID:       $Id: vMESPlugInUpdateDialog.py,v 1.18 2014/01/12 13:58:11 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import images
import sha,Queue,traceback,os,os.path,stat,types,sys

import vLogFallBack as lgFB
import vSystem

try:
    import __init__
    if __init__.VIDARC_IMPORT:
        import vidImp
    import identify as vIdentify
except:
    lgFB.logTB()
try:
    from vMsgDialog import vMsgDialog
    #sys.stderr.write('\nimport vSockFileClt\n')
    from vSockFileClt import *
    #sys.stderr.write('\nimport vSockClt\n')
    from vSockClt import *
except:
    lgFB.logTB()
    sys.stderr.write('\nimport\n')
    sys.stderr.write(traceback.format_exc())

def create(parent):
    return vMESPlugInUpdateDialog(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VMESPLUGINUPDATEDIALOG, wxID_VMESPLUGINUPDATEDIALOGCBCLOSE, 
 wxID_VMESPLUGINUPDATEDIALOGCBCONNECT, wxID_VMESPLUGINUPDATEDIALOGCBFIND, 
 wxID_VMESPLUGINUPDATEDIALOGCBGENCERT, wxID_VMESPLUGINUPDATEDIALOGCBLOGIN, 
 wxID_VMESPLUGINUPDATEDIALOGCBUPDATE, wxID_VMESPLUGINUPDATEDIALOGLBLHOST, 
 wxID_VMESPLUGINUPDATEDIALOGLBLRESULT, wxID_VMESPLUGINUPDATEDIALOGLBLUSR, 
 wxID_VMESPLUGINUPDATEDIALOGLSTPLUGIN, wxID_VMESPLUGINUPDATEDIALOGPBPLUGIN, 
 wxID_VMESPLUGINUPDATEDIALOGPBPLUGINS, wxID_VMESPLUGINUPDATEDIALOGSTBRESULT, 
 wxID_VMESPLUGINUPDATEDIALOGTXTHOST, wxID_VMESPLUGINUPDATEDIALOGTXTPASSWD, 
 wxID_VMESPLUGINUPDATEDIALOGTXTPORT, wxID_VMESPLUGINUPDATEDIALOGTXTUSER, 
] = [wx.NewId() for _init_ctrls in range(18)]

class vMESPlugInUpdateDialog(wx.Dialog):
    VERBOSE=0
    VIDARC_ALIAS='vPlugInFile:vidarc'
    VIDARC_ALIAS_LOCALE='vPlugInFile:vidarcLocale'
    def _init_coll_bxsBtConnUpdate_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbConnect, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddWindow(self.cbUpdate, 0, border=4, flag=wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbGenCert, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbLogin, 0, border=0, flag=0)

    def _init_coll_bxsHost_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblHost, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtHost, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPort, 1, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(4)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsHost, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsLogin, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsBtConnUpdate, 0, border=4,
              flag=wx.ALIGN_CENTER | wx.TOP)
        parent.AddSizer(self.bxsConn, 0, border=4,
              flag=wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddSizer(self.bxsPlugIn, 0, border=4, flag=wx.TOP | wx.EXPAND)
        parent.AddSizer(self.bxsPlugInBox, 1, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.RIGHT | wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.cbClose, 0, border=4,
              flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER)

    def _init_coll_bxsLogin_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblUsr, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtUser, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 1, border=4, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbFind, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)

    def _init_coll_bxsPlugIn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lstPlugIn, 1, border=4, flag=wx.EXPAND)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.RIGHT | wx.LEFT)

    def _init_coll_bxsPlugInBox_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.pbPlugIns, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.pbPlugIn, 2, border=4, flag=wx.EXPAND | wx.LEFT)

    def _init_coll_bxsConn_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.stbResult, 0, border=4, flag=wx.LEFT)
        parent.AddWindow(self.lblResult, 1, border=4,
              flag=wx.RIGHT | wx.LEFT | wx.EXPAND)

    def _init_coll_lstPlugIn_Columns(self, parent):
        # generated method, don't edit

        parent.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'plugin'), width=200)
        parent.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'remote'), width=60)
        parent.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'local'), width=60)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=7, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)

        self.bxsHost = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsLogin = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPlugIn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPlugInBox = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsConn = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBtConnUpdate = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsHost_Items(self.bxsHost)
        self._init_coll_bxsLogin_Items(self.bxsLogin)
        self._init_coll_bxsPlugIn_Items(self.bxsPlugIn)
        self._init_coll_bxsPlugInBox_Items(self.bxsPlugInBox)
        self._init_coll_bxsConn_Items(self.bxsConn)
        self._init_coll_bxsBtConnUpdate_Items(self.bxsBtConnUpdate)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VMESPLUGINUPDATEDIALOG,
              name=u'vMESPlugInUpdateDialog', parent=prnt, pos=wx.Point(371,
              105), size=wx.Size(400, 400),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=u'vMES PlugIn Update Dialog')
        self.SetClientSize(wx.Size(392, 373))

        self.lblHost = wx.StaticText(id=wxID_VMESPLUGINUPDATEDIALOGLBLHOST,
              label=_(u'Host / Port'), name=u'lblHost', parent=self,
              pos=wx.Point(0, 4), size=wx.Size(126, 21), style=wx.ALIGN_RIGHT)
        self.lblHost.SetMinSize(wx.Size(-1, -1))

        self.txtHost = wx.TextCtrl(id=wxID_VMESPLUGINUPDATEDIALOGTXTHOST,
              name=u'txtHost', parent=self, pos=wx.Point(130, 4),
              size=wx.Size(126, 21), style=wx.TE_PROCESS_ENTER, value=u'')
        self.txtHost.SetMinSize(wx.Size(-1, -1))
        self.txtHost.Bind(wx.EVT_TEXT_ENTER, self.OnTxtHostTextEnter,
              id=wxID_VMESPLUGINUPDATEDIALOGTXTHOST)

        self.txtPort = wx.TextCtrl(id=wxID_VMESPLUGINUPDATEDIALOGTXTPORT,
              name=u'txtPort', parent=self, pos=wx.Point(260, 4),
              size=wx.Size(130, 21), style=wx.TE_PROCESS_ENTER, value=u'50002')
        self.txtPort.SetMinSize(wx.Size(-1, -1))
        self.txtPort.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPortTextEnter,
              id=wxID_VMESPLUGINUPDATEDIALOGTXTPORT)

        self.lblUsr = wx.StaticText(id=wxID_VMESPLUGINUPDATEDIALOGLBLUSR,
              label=_(u'login'), name=u'lblUsr', parent=self, pos=wx.Point(0,
              29), size=wx.Size(126, 21), style=wx.ALIGN_RIGHT)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))

        self.txtUser = wx.TextCtrl(id=wxID_VMESPLUGINUPDATEDIALOGTXTUSER,
              name=u'txtUser', parent=self, pos=wx.Point(130, 29),
              size=wx.Size(126, 21), style=wx.TE_PROCESS_ENTER , value=u'')
        self.txtUser.SetMinSize(wx.Size(-1, -1))
        self.txtUser.Bind(wx.EVT_TEXT_ENTER, self.OnTxtUserTextEnter,
              id=wxID_VMESPLUGINUPDATEDIALOGTXTUSER)

        self.txtPasswd = wx.TextCtrl(id=wxID_VMESPLUGINUPDATEDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(260, 29),
              size=wx.Size(130, 21), style=wx.TE_PASSWORD| wx.TE_PROCESS_ENTER, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))
        self.txtPasswd.Bind(wx.EVT_TEXT_ENTER, self.OnTxtPasswdTextEnter,
              id=wxID_VMESPLUGINUPDATEDIALOGTXTPASSWD)

        self.cbLogin = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VMESPLUGINUPDATEDIALOGCBLOGIN, label=_(u'Login'),
              name=u'cbLogin', parent=self, pos=wx.Point(304, 54),
              size=wx.Size(76, 30), style=0)
        self.cbLogin.SetMinSize(wx.Size(-1, -1))
        self.cbLogin.Bind(wx.EVT_BUTTON, self.OnCbLoginButton,
              id=wxID_VMESPLUGINUPDATEDIALOGCBLOGIN)

        self.cbConnect = wx.lib.buttons.GenBitmapTextToggleButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VMESPLUGINUPDATEDIALOGCBCONNECT, label=_('connect'),
              name=u'cbConnect', parent=self, pos=wx.Point(16, 54),
              size=wx.Size(76, 30), style=0)
        self.cbConnect.SetMinSize(wx.Size(-1, -1))
        self.cbConnect.Bind(wx.EVT_BUTTON, self.OnCbConnectButton,
              id=wxID_VMESPLUGINUPDATEDIALOGCBCONNECT)

        self.cbUpdate = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VMESPLUGINUPDATEDIALOGCBUPDATE, label=_('update'),
              name=u'cbUpdate', parent=self, pos=wx.Point(100, 54),
              size=wx.Size(76, 30), style=0)
        self.cbUpdate.SetMinSize(wx.Size(-1, -1))
        self.cbUpdate.Bind(wx.EVT_BUTTON, self.OnCbUpdateButton,
              id=wxID_VMESPLUGINUPDATEDIALOGCBUPDATE)

        self.lstPlugIn = wx.ListCtrl(id=wxID_VMESPLUGINUPDATEDIALOGLSTPLUGIN,
              name=u'lstPlugIn', parent=self, pos=wx.Point(0, 108),
              size=wx.Size(353, 206), style=wx.LC_REPORT)
        self.lstPlugIn.SetMinSize(wx.Size(-1, -1))
        self._init_coll_lstPlugIn_Columns(self.lstPlugIn)

        self.cbFind = wx.lib.buttons.GenBitmapButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VMESPLUGINUPDATEDIALOGCBFIND, name=u'cbFind',
              parent=self, pos=wx.Point(357, 108), size=wx.Size(31, 30),
              style=0)
        self.cbFind.Bind(wx.EVT_BUTTON, self.OnCbFindButton,
              id=wxID_VMESPLUGINUPDATEDIALOGCBFIND)

        self.pbPlugIns = wx.Gauge(id=wxID_VMESPLUGINUPDATEDIALOGPBPLUGINS,
              name=u'pbPlugIns', parent=self, pos=wx.Point(4, 318), range=100,
              size=wx.Size(128, 13), style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbPlugIns.SetMinSize(wx.Size(-1, 13))

        self.pbPlugIn = wx.Gauge(id=wxID_VMESPLUGINUPDATEDIALOGPBPLUGIN,
              name=u'pbPlugIn', parent=self, pos=wx.Point(136, 318),
              range=10000, size=wx.Size(252, 13),
              style=wx.GA_SMOOTH | wx.GA_HORIZONTAL)
        self.pbPlugIn.SetMinSize(wx.Size(-1, 13))

        self.cbClose = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VMESPLUGINUPDATEDIALOGCBCLOSE, label=_(u'Close'),
              name=u'cbClose', parent=self, pos=wx.Point(158, 339),
              size=wx.Size(76, 30), style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,
              id=wxID_VMESPLUGINUPDATEDIALOGCBCLOSE)

        self.stbResult = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
              id=wxID_VMESPLUGINUPDATEDIALOGSTBRESULT, name=u'stbResult',
              parent=self, pos=wx.Point(8, 88), size=wx.Size(16, 16), style=0)

        self.lblResult = wx.StaticText(id=wxID_VMESPLUGINUPDATEDIALOGLBLRESULT,
              label=u'', name=u'lblResult', parent=self, pos=wx.Point(28, 88),
              size=wx.Size(356, 16), style=0)
        self.lblResult.SetMinSize(wx.Size(-1, -1))

        self.cbGenCert = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(16,
              16), id=wxID_VMESPLUGINUPDATEDIALOGCBGENCERT, label=u'SSL',
              name=u'cbGenCert', parent=self, pos=wx.Point(212, 54),
              size=wx.Size(76, 30), style=0)
        self.cbGenCert.SetToolTipString(_(u'generate SSL certificate'))
        self.cbGenCert.SetMinSize(wx.Size(-1, -1))
        self.cbGenCert.Bind(wx.EVT_BUTTON, self.OnCbGenCertButton,
              id=wxID_VMESPLUGINUPDATEDIALOGCBGENCERT)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.cbUpdate.SetBitmapLabel(images.getUpdateBitmap())
        self.cbFind.SetBitmapLabel(images.getFindBitmap())
        self.cbClose.SetBitmapLabel(images.getCancelBitmap())
        self.cbConnect.SetBitmapLabel(images.getConnectBitmap())
        self.cbConnect.SetBitmapSelected(images.getShutDownBitmap())
        self.cbGenCert.SetBitmapLabel(images.getBuildBitmap())
        self.stbResult.SetBitmap(images.getStoppedBitmap())
        self.cbUpdate.Enable(False)
        self.cbLogin.Enable(False)
        
        self.dImg={}
        self.imgLst=wx.ImageList(16,16)
        self.dImg['empty']=self.imgLst.Add(images.getInvisibleBitmap())
        self.dImg['ok']=self.imgLst.Add(images.getApplyBitmap())
        self.dImg['err']=self.imgLst.Add(images.getErrorBitmap())
        self.lstPlugIn.SetImageList(self.imgLst,wx.IMAGE_LIST_SMALL)
        
        self.dImg['conn']=images.getConnectBitmap()
        self.dImg['connFlt']=images.getFaultBitmap()
        self.dImg['connOk']=images.getLoginBitmap()
        self.dImg['login']=images.getLoginBitmap()
        self.dImg['loggedIn']=images.getRunningBitmap()
        self.dImg['closed']=images.getStoppedBitmap()
        self.dImg['Browse']=images.getRunningBitmap()
        self.dImg['ListContent']=images.getRunningBitmap()
        self.dImg['FileGetFin']=images.getRunningBitmap()
        self.dImg['Err']=images.getErrorBitmap()
        
        self.cbLogin.SetBitmapLabel(self.dImg['connOk'])
        
        self.sPasswd=''
        self.sActDN=''
        self.sockFile=None
        self.cltServ=None
        self.cltSock=None
        self.timer=None
        self.bLoggedIn=False
        self.bConnected=False
        self.bModeAuto=False
        self.func=None
        self.args=None
        self.kwargs=None
        self.qMsg=Queue.Queue()
        self.qBrowse=Queue.Queue()
        self.qContent=Queue.Queue()
        self.qFilesProc=Queue.Queue()
        self.qFiles=Queue.Queue()
        self.Clear()
    def SetModeAuto(self,bMode,func,*args,**kwargs):
        lgFB.logInfo("bMode:%d"%(bMode))
        self.bModeAuto=bMode
        self.bConnected=False
        if self.bModeAuto:
            self.EnableTxtFields(False)
            self.func=func
            self.args=args
            self.kwargs=kwargs
            sDN=vIdentify.getCfg('baseDN',machine=False)
            self.__getLocal__(sDN)
            if self.cbConnect.GetValue()==False:
                self.cbConnect.SetToggle(True)
                self.__connect__()
                #self.OnCbConnectButton(None)
        else:
            self.EnableTxtFields(True)
            self.cbConnect.Enable(True)
            self.cbGenCert.Enable(True)
    def EnableTxtFields(self,flag):
        self.txtHost.Enable(flag)
        self.txtPort.Enable(flag)
        self.txtUser.Enable(flag)
        self.txtPasswd.Enable(flag)
    def SetConnection(self,sHost,sPort,sUser,sPasswd):
        self.txtHost.SetValue(sHost)
        self.txtPort.SetValue(sPort)
        self.txtUser.SetValue(sUser)
        self.txtPasswd.SetValue('')
        if sPasswd is None:
            self.sPasswd=''
        else:
            self.sPasswd=sPasswd
    def GetConnection(self):
        sHost=self.txtHost.GetValue()
        sPort=self.txtPort.GetValue()
        sUser=self.txtUser.GetValue()
        return sHost,sPort,sUser,self.sPasswd
    def GetLease(self):
        return 366
    def Clear(self):
        self.bUpdated=False
        self.lUpdated=[]
        self.lLocalFN=[]
        self.lRemoteFN=[]
        self.dRemoteLocaleFN={}
        self.iListContentAct=0
        self.iListContentCount=1
        while self.qBrowse.empty()==False:
            self.qBrowse.get()
        while self.qContent.empty()==False:
            self.qContent.get()
        while self.qFilesProc.empty()==False:
            self.qFilesProc.get()
        while self.qFiles.empty()==False:
            self.qFiles.get()
        self.lstPlugIn.DeleteAllItems()
        
        sDN=vIdentify.getCfg('baseDN',machine=False)
        self.__getLocal__(sDN)
        self.__showPlugIn__()
    def __reload__(self):
        if self.bUpdated:
            lMod=[]
            for s in self.lUpdated:
                strs=s.split('.')
                if strs[-1] not in ['vidarc','zip']:
                    continue
                sMod='.'.join(strs[:-4])
                lMod.append(sMod)
            import __init__
            if __init__.VIDARC_IMPORT:
                import vidImp
                sDN=vIdentify.getCfg('baseDN',machine=False)
                vidImp.vid.SetDN(sDN)
                #vidImp.vid.close()
                #vidImp.vid.prepare4reload(lMod)
    def OnCbCloseButton(self, event):
        if event is not None:
            event.Skip()
        if self.timer is not None:
            self.timer.Stop()
            self.timer=None
        if self.VERBOSE:
            print 'OnCbCloseButton'
            print '    updated:%d check:%d connected:%d'%(self.bUpdated,
                        self.bCheckOk,self.bConnected)
        if self.bUpdated:
            self.__reload__()
            if self.IsModal():
                self.EndModal(1)
            else:
                self.Show(False)
                if self.func is not None:
                    self.func(*self.args,**self.kwargs)
        elif self.bConnected:
            if self.IsModal():
                self.EndModal(2)
            else:
                self.Show(False)
        else:
            if self.IsModal():
                self.EndModal(0)
            else:
                self.Show(False)
    def __connect__(self):
        try:
            while self.qMsg.empty()==False:
                sMsg=self.qMsg.get(False)
            import vSockClt
            import vSockFileClt
            lgFB.logInfo("")
            self.cbClose.Enable(False)
            self.cbLogin.Enable(False)
            self.bLogin=True
            self.bActive=True
            self.bLoggedIn=False
            self.bCheckOk=False
            self.bBrowse=False
            self.bLicFlt=False
            sHost=self.txtHost.GetValue()
            sPort=self.txtPort.GetValue()
            self.lRemoteFN=[]
            self.dRemoteLocaleFN={}
            self.iListContentAct=0
            self.iListContentCount=1
            while self.qBrowse.empty()==False:
                self.qBrowse.get()
            self.cltServ=vSockClt.vSockClt(self,sHost,int(sPort),
                        vSockFileClt.vSockFileClt)
            self.timer = wx.PyTimer(self.__CheckQueue__)
            self.timer.Start(50)
            self.cltServ.Connect()
        except:
            traceback.print_exc(file=sys.err)
    def __connectionClosed__(self,bEstablished=False):
        lgFB.logInfo("")
        if self.VERBOSE:
            traceback.print_stack()
        if self.cltSock is not None:
            del self.cltSock
        self.cltSock=None
        #if self.cltServ is not None:
        #    del self.cltServ
        self.cltServ=None
        self.qMsg.put('closed')
    def __close__(self):
        lgFB.logInfo("")
        self.cbLogin.Enable(False)
        if self.VERBOSE:
            traceback.print_stack()
            print '__close__'
            print '    ',self.bActive
            print self.cltServ,self.cltSock
        if self.cltSock is not None:
            self.cltSock.Close()
        else:
            self.bActive=False
        if self.VERBOSE:
            print '    ',self.bActive
        if self.cltServ is not None:
            self.cltServ.Stop()
        #else:
        #    self.bActive=False
        if self.VERBOSE:
            print '    ',self.bActive
        self.bLogin=False
    def __setPlugInResult__(self,sFN,sResImg):
        lgFB.logInfo("")
        iCount=self.lstPlugIn.GetItemCount()
        if sFN is None:
            # start update
            if self.bLicFlt:
                self.bLicFlt=False
                return
            for idx in xrange(iCount):
                self.lstPlugIn.SetItemImage(idx,self.dImg['empty'])
            self.lstPlugIn.EnsureVisible(0)
            return
        if self.sActDN is not None:
            src='/'.join([self.sActDN,sFN])
        else:
            src='.'.join(sFN.split('.')[:-4])
        #sys.stderr.write(sFN+' '+src+'\n')
        for idx in xrange(iCount):
            if self.lstPlugIn.GetItem(idx,0).m_text==src:
                try:
                    self.lstPlugIn.SetItemImage(idx,self.dImg[sResImg])
                    if self.bLicFlt==False:
                        self.lstPlugIn.EnsureVisible(idx)
                    if sResImg=='err':
                        try:
                            sys.stderr.write('error on %s (bLicFlt:%d)\n'%(src,self.bLicFlt))
                        except:
                            pass
                        self.lstPlugIn.SetItemState(idx,wx.LIST_STATE_SELECTED,wx.LIST_STATE_SELECTED)
                        self.bLicFlt=True
                except:
                    pass
                return
    def __CheckQueue__(self):
        while self.__ProcessQueue__():
            pass
        #self.__ProcessQueue__()
    def __checkGet2Proc__(self):
        lgFB.logInfo("")
        if self.qFiles.empty()==False:
            t=self.qFiles.get()
            if t[0].find('vidarcLocale')>0:
                if self.bAuthenticate:
                    self.bAuthenticate=False
                    self.cltSock.Authenticate('','')
                self.sActDN=t[0][len(self.VIDARC_ALIAS_LOCALE)+1:]
            else:
                self.sActDN=None
            self.cltSock.GetFile(t[0],t[1],t[2])
            #sys.stderr.write(t[0]+' '+t[1]+' '+t[2]+'\n')
            self.qFilesProc.put(t)
            self.__updateFilesBar__()
        else:
            bClose=True
            self.lblResult.SetLabel(_('all files received'))
            if self.bLicFlt:
                if self.bModeAuto:
                    self.Show(True)
                sys.stderr.write('\nupdate finished:bLicFlt:%d\n'%(self.bLicFlt))
                dlg = vMsgDialog(self,
                            _(u'At least one errors encountered.')+'\n'+\
                            _('Consider restart update.')+'\n\n'+\
                            _('Shall connection kept open?'),
                            _(u'vMES Center')+u' '+_(u'Update'),
                            wx.YES | wx.NO  | wx.ICON_ERROR)
                if dlg.ShowModal()==wx.ID_YES:
                    bClose=False
                    self.cbUpdate.Enable(True)
                    
                dlg.Destroy()
            self.pbPlugIns.SetValue(0)
            while self.qFilesProc.empty()==False:
                self.qFilesProc.get()
            # auto close connection and dialog
            if bClose:
                self.cbConnect.SetValue(False)
                self.__close__()
    def __ProcessQueue__(self):
        try:
            sMsg=self.qMsg.get(False)
            lgFB.logDebug("sMsg:%s"%(sMsg))
            if self.VERBOSE:
                print sMsg
            if type(sMsg)==types.TupleType:
                tMsg=sMsg
                sMsg=tMsg[0]
            if sMsg in self.dImg:
                img=self.dImg[sMsg]
            else:
                img=self.dImg['Err']
            if sMsg=='conn':
                #img=images.getConnectBitmap()
                self.lblResult.SetLabel(_('connecting'))
            elif sMsg=='connFlt':
                #img=images.getFaultBitmap()
                self.lblResult.SetLabel(_('connection fault'))
                self.cbConnect.SetValue(False)
                self.__close__()
                self.cbClose.Enable(True)
                if self.bModeAuto:
                    if self.func is not None:
                        self.func(*self.args,**self.kwargs)
            elif sMsg=='connOk':
                #img=images.getLoginBitmap()
                self.lblResult.SetLabel(_('connection established'))
                self.bActive=True
            elif sMsg=='login':
                #img=images.getLoginBitmap()
                self.lblResult.SetLabel(_('login'))
                self.cbLogin.Enable(True)
                self.txtPasswd.SetFocus()
                if self.bModeAuto:
                    self.EnableTxtFields(True)
                    self.Show(True)
                if 0:
                    if self.bLogin:
                        self.bLogin=False
                        try:
                            self.cltSock.__doLogin__()
                        except:
                            self.__close__()
                            pass
                    else:
                        self.bLogin=False
                        self.__close__()
            elif sMsg=='loggedIn':
                #img=images.getRunningBitmap()
                self.lblResult.SetLabel(_('get remote content'))
                self.bCheckOk=True
                self.cltSock.Browse(self.VIDARC_ALIAS_LOCALE)
                self.cltSock.ListContent(self.VIDARC_ALIAS)
                #self.cbUpdate.Enable(True)   # 070715:wro enable after content got
            elif sMsg=='closed':
                #img=images.getStoppedBitmap()
                self.lblResult.SetLabel(_('connection closed'))
                #if self.timer is not None:
                #    self.timer.Stop()
                #    self.timer=None
                while self.qMsg.empty()==False:
                    self.qMsg.get()
                while self.qFilesProc.empty()==False:
                    self.qFilesProc.get()
                while self.qFiles.empty()==False:
                    self.qFiles.get()
                #self.cbConnect.SetValue(False)
                self.cbConnect.SetToggle(False)
                self.cbClose.Enable(True)
                self.lRemoteFN=[]
                self.dRemoteLocaleFN={}
                sDN=vIdentify.getCfg('baseDN',machine=False)
                self.__getLocal__(sDN)
                #self.__showPlugIn__()
                #if self.bCheckOk:
                #    self.EndModal(1)
                #self.cbApply.Enable(True)
                lgFB.logDebug("bModeAuto:%d;bActive:%d;"\
                        "bUpdated:%d"\
                        %(self.bModeAuto,self.bActive,
                        self.bUpdated))
                if self.bModeAuto:
                    if self.bActive:
                        if self.bUpdated:
                            if self.bLicFlt==False:
                                self.__reload__()
                                self.Show(False)
                                if self.func is not None:
                                    self.func(*self.args,**self.kwargs)
                        else:
                            if self.func is not None:
                                self.func(*self.args,**self.kwargs)
                    else:
                        if self.func is not None:
                            self.func(*self.args,**self.kwargs)
                self.bActive=False
            elif sMsg=='Browse':
                #img=images.getRunningBitmap()
                while self.qBrowse.empty()==False:
                    s=self.qBrowse.get()
                    lgFB.logDebug("Browse:%s"%(s))
                    if len(s)>0:
                        self.iListContentCount+=1
                        lgFB.logDebug("Browse:%s"%(s))
                        self.cltSock.ListContent(s)
                #self.__showPlugIn__()
            elif sMsg=='ListContent':
                #img=images.getRunningBitmap()
                lgFB.logDebug("ListContent:%d,%d"%(self.iListContentAct,
                        self.iListContentCount))
                if self.VERBOSE:
                    print self.iListContentAct,self.iListContentCount
                if self.iListContentAct==self.iListContentCount:
                    self.lblResult.SetLabel(_('got remote content'))
                    self.__showPlugIn__()
                    self.cbUpdate.Enable(True)   # 070715:wro enable after content got
                    if self.bModeAuto:
                        if self.__check2updateAuto__():
                            self.GetParent().__destroyVidarcObjs__()
                            self.qMsg.put('chkRdy2Exit')
                            self.cbConnect.Enable(False)
                            self.cbGenCert.Enable(False)
                            self.cbFind.Enable(False)
                            self.Show(True)
                        else:
                            self.__close__()
                else:
                    self.iListContentAct+=1
            elif sMsg=='close':
                self.__close__()
                self.iListContentAct=self.iListContentCount
                self.__updateAuto__()
                if wx.VERSION >= (2,8):
                    wx.CallAfter(self.OnCbCloseButton,None)
                else:
                    wx.CallAfter(self.OnCbCloseButton,None)
            elif sMsg=='chkRdy2Exit':
                #img=images.getRunningBitmap()
                if self.GetParent().__checkRdy2Exit__()==False:
                    self.qMsg.put('chkRdy2Exit')
                else:
                    self.Show(True)
                    if wx.VERSION >= (2,8):
                        wx.CallLater(30000,self.cbConnect.Enable,True)
                        wx.CallLater(30000,self.cbGenCert.Enable,True)
                        wx.CallLater(30000,self.cbFind.Enable,True)
                        wx.CallLater(30000,self.EnableTxtFields,True)
                    else:
                        wx.FutureCall(30000,self.cbConnect.Enable,True)
                        wx.FutureCall(30000,self.cbGenCert.Enable,True)
                        wx.FutureCall(30000,self.cbFind.Enable,True)
                        wx.FutureCall(30000,self.EnableTxtFields,True)
                    self.__updateAuto__()
                return False
            elif sMsg=='FileGetStart':
                img=None
                #img=images.getRunningBitmap()
                self.lblResult.SetLabel(_('receiving file:')+tMsg[1])
                self.__updateFilesBar__()
            elif sMsg=='FileGetProc':
                img=None
                #img=images.getRunningBitmap()
                self.pbPlugIn.SetValue(int(tMsg[1]/float(tMsg[2])*10000.0))
                self.__updateFilesBar__()
            elif sMsg=='FileGetFin':
                self.pbPlugIn.SetValue(0)
                if tMsg[1] is not None:
                    self.bUpdated=True
                    self.lUpdated.append(tMsg[1])
                #img=images.getRunningBitmap()
                self.__setPlugInResult__(tMsg[1],'ok')
                self.__checkGet2Proc__()
            elif sMsg=='FileGetAbort':
                img=None
                #img=images.getErrorBitmap()
                self.__setPlugInResult__(tMsg[1],'err')
                self.lblResult.SetLabel(_('receive aborted file:')+tMsg[1])
                self.__checkGet2Proc__()
                self.__updateFilesBar__()
                sys.stderr.write('\nFileGetAbort\n  bLicFlt:%d\n'%(self.bLicFlt))
                if self.bModeAuto:
                    self.cbConnect.Enable(True)
                    self.cbGenCert.Enable(True)
                    self.cbFind.Enable(True)
                    self.cbClose.Enable(True)
                    self.EnableTxtFields(True)
            else:
                #img=images.getErrorBitmap()
                self.lblResult.SetLabel(_('unknown')+' '+repr(sMsg))
            #if self.bCheckOk:
            #    img=images.getApplyBitmap()
            if img is not None:
                self.stbResult.SetBitmap(img)
                self.stbResult.Refresh()
            return True
        except Queue.Empty:
            return False
        except:
            traceback.print_exc()
            return False
    def NotifyConnect(self):
        lgFB.logInfo("")
        self.qMsg.put('conn')
    def NotifyConnectionFault(self):
        lgFB.logInfo("")
        self.qMsg.put('connFlt')
    def __doLogin__(self):
        lgFB.logInfo("")
        if self.cltSock is not None:
            usr=self.txtUser.GetValue()
            passwd=self.txtPasswd.GetValue()
            if len(passwd)>0:
                m=sha.new()
                m.update(passwd)
                passwd=m.hexdigest()
                self.sPasswd=passwd
            else:
                passwd=self.sPasswd#''
            self.cltSock.LoginEncoded(usr,passwd)
    def NotifyConnected(self):
        try:
            lgFB.logInfo("")
            wx.MutexGuiEnter()
            self.cltSock=self.cltServ.GetSocketInstance()
            self.cltSock.SetParent(self)
            self.__doLogin__()
            #self.cltSock.Alias(self.VIDARC_ALIAS)
        except:
            traceback.print_exc()
        self.qMsg.put('connOk')
        wx.MutexGuiLeave()
    def NotifyLogin(self):
        lgFB.logInfo("")
        self.qMsg.put('login')
    def NotifyLoggedIn(self,flag):
        lgFB.logInfo("")
        self.bLoggedIn=flag
        self.qMsg.put('loggedIn')
    def NotifyFileGetStart(self,sFN):
        lgFB.logDebug("")
        self.qMsg.put(('FileGetStart',sFN))
    def NotifyFileGetProc(self,sFN,iAct,iSize):
        self.qMsg.put(('FileGetProc',iAct,iSize))
    def NotifyFileGetFin(self,sFN):
        lgFB.logInfo("")
        self.qMsg.put(('FileGetFin',sFN))
    def NotifyFileGetAbort(self,sFN,bLicFlt=False):
        lgFB.logInfo("")
        if bLicFlt:
            self.bLicFlt=True
            #try:
            #    sys.stderr.write('get abort on %s (bLicFlt:%d)\n'%(sFN,self.bLicFlt))
            #except:
            #    pass
        self.qMsg.put(('FileGetAbort',sFN))
        sys.stderr.write('FileGetAbort fn:%s\n'%sFN)
    def NotifyBrowse(self,l):
        lgFB.logInfo("")
        if self.bBrowse==False:
            self.bBrowse=True
        for sIt in l:
            self.qBrowse.put(sIt)
            #self.cltSock.ListContent(sIt)
        self.qMsg.put('Browse')
    def NotifyListContent(self,applAlias,l):
        lgFB.logInfo("applAlias:%s"%(applAlias))
        lgFB.logDebug("applAlias:%s;l:%r"%(applAlias,l))
        if applAlias==self.VIDARC_ALIAS:
            self.lRemoteFN=l
            if len(self.lRemoteFN)>0:
                self.bConnected=True
                if len(self.lRemoteFN[0])>0:
                    self.bConnected=True
                else:
                    self.iListContentAct=self.iListContentCount
                    self.bActive=True
                    self.qMsg.put('close')
                    #self.qMsg.put('ListContent')
            else:
                #self.qMsg.put('close')
                self.qMsg.put('ListContent')
        else:
            sPart=applAlias[len(self.VIDARC_ALIAS_LOCALE)+1:]
            if applAlias not in self.dRemoteLocaleFN:
                self.dRemoteLocaleFN[applAlias]=[]
            lst=self.dRemoteLocaleFN[applAlias]
            for sFN in l:
                if len(sFN)>0:
                    if sFN not in lst:
                        lst.append(sFN)
            #if applAlias=='vPlugInFile:vidarcLocale':
        self.qMsg.put('ListContent')
    def NotifyServed(self,served):
        lgFB.logInfo("")
        self.qMsg.put('Served')
    def __ensureLicense__(self):
        if self.VERBOSE:
            sys.stderr.write('************************************\n')
            sys.stderr.write('************************************\n')
            sys.stderr.write('************************************\n')
            sys.stderr.write('************************************\n')
            sys.stderr.write('************************************\n')
            sys.stderr.write('************************************\n')
            sys.stderr.write('************************************\n')
            sys.stderr.write('__ensureLicense__\n')
        sDN=vIdentify.getCfg('baseDN',machine=False)
        bGen=False
        bDftDN=False
        if len(sDN)==0:
            bGen=True
            bDftDN=True
        else:
            sFN = "%s/license.pub"%sDN
            if os.path.exists(sFN)==False:
                bGen=True
            sFN = "%s/license.priv"%sDN
            if os.path.exists(sFN)==False:
                bGen=True
        if bGen:
            if bDftDN:
                #sDN=os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])),'vidarc')
                #sDN=ws.GetUserConfigDir()
                #sDN=wx.GetUserDataDir()
                sDN=vSystem.getPlugInDN()
                ret=vIdentify.setCfg('baseDN',sDN,machine=False)
                if self.VERBOSE:
                    sys.stderr.write('sDN:%s,ret:%d\n'%(sDN,ret))
                #try:
                #    os.makedirs(sDN)
                #except:
                #    pass
            mid=vIdentify.getMachineID()
            vIdentify.generateLicensePair(sDN,mid,None,None,None,machine=False)
        import __init__
        if __init__.VIDARC_IMPORT:
            import vidImp
            #vidImp.vid.close()
    def OnCbFindButton(self, event):
        event.Skip()
        sDN=vIdentify.getCfg('baseDN',machine=False)
        self.__getLocal__(sDN)
    def __check2updateAuto__(self):
        try:
            for k,d in self.dPlugIn.iteritems():
                try:
                    if 'local' in d:
                        if d['remote']>d['local']:
                            return True
                    else:
                        return True
                except:
                    traceback.print_exc(file=sys.stderr)
        except:
            return False
    def __updateAuto__(self):
        #self.lLocalFN
        try:
            self.bLicFlt=False
            self.cbUpdate.Enable(False)
            l=[]
            lLocale=[]
            for k,d in self.dPlugIn.iteritems():
                try:
                    if 'local' in d:
                        if d['remote']>d['local']:
                            l.append(d['remoteFN'])
                    else:
                        l.append(d['remoteFN'])
                except:
                    traceback.print_exc(file=sys.stderr)
            if (len(l)>0) or (len(lLocale)>0):
                if len(lLocale)==0:
                    keys=self.dRemoteLocaleFN.keys()
                    keys.sort()
                    iSkip=len(self.VIDARC_ALIAS_LOCALE)+1
                    for k in keys:
                        #sys.stderr.write(k+'\n')
                        sLocale=k[iSkip:]
                        lst=self.dRemoteLocaleFN[k]
                        for it in lst:
                            lLocale.append(sLocale+'/'+it)
                l.sort()
                lLocale.sort()
                self.__ensureLicense__()
                sDN=vIdentify.getCfg('baseDN',machine=False)
                basepath = os.path.abspath(os.path.dirname(sys.argv[0]))
                localedir = os.path.join(basepath, "locale")
                self.__updatePlugIn__(l,sDN,lLocale,localedir,self.cltSock)
                if self.bAuthenticate:
                    sPubFN = "%s/license.pub"%sDN
                    fd = open(sPubFN, "rb")
                    pubkey = fd.read()
                    fd.close()
                    mid=vIdentify.getMachineID()
                    self.lblResult.SetLabel(_('authenticate on plugin server ...'))
                    self.cltSock.Authenticate(mid,pubkey)
                self.lblResult.SetLabel(_('start update ...'))
                self.qMsg.put(('FileGetFin',None))
        except:
            traceback.print_exc(file=sys.stderr)
    def OnCbUpdateButton(self, event):
        event.Skip()
        try:
            self.bLicFlt=False
            self.cbUpdate.Enable(False)
            if self.VERBOSE:
                sys.stderr.write('\n\nOnCbUpdateButton\n')
            self.lblResult.SetLabel(_('initialise update ...'))
            iCount=self.lstPlugIn.GetItemCount()
            l=[]
            lLocale=[]
            self.__ensureLicense__()
            if self.lstPlugIn.GetSelectedItemCount()==0:
                bAll=True
            else:
                bAll=False
            for idx in xrange(iCount):
                if bAll or self.lstPlugIn.GetItemState(idx,wx.LIST_STATE_SELECTED)!=0:
                    sPlugIn=self.lstPlugIn.GetItem(idx,0).m_text
                    if sPlugIn in self.dPlugIn:
                        d=self.dPlugIn[sPlugIn]
                        if 'remoteFN' in d:
                            l.append(d['remoteFN'])
                    else:
                        lLocale.append(sPlugIn)
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'plugIn2Get:%s'%(vtLog.pformat(l)),self)
            sDN=vIdentify.getCfg('baseDN',machine=False)
            #basepath = wx.GetUserDataDir()  #os.path.abspath(os.path.dirname(sys.argv[0]))
            
            #localedir = os.path.join(basepath, "locale")
            localedir = os.path.join(sDN, "locale")
            self.__updatePlugIn__(l,sDN,lLocale,localedir,self.cltSock)
            if self.bAuthenticate:
                sPubFN = "%s/license.pub"%sDN
                fd = open(sPubFN, "rb")
                pubkey = fd.read()
                fd.close()
                mid=vIdentify.getMachineID()
                self.lblResult.SetLabel(_('authenticate on plugin server ...'))
                self.cltSock.Authenticate(mid,pubkey)
            self.lblResult.SetLabel(_('start update ...'))
            self.qMsg.put(('FileGetFin',None))
            if self.VERBOSE:
                sys.stderr.write('OnCbUpdateButton, start update\n')
        except:
            traceback.print_exc()
            self.cbUpdate.Enable(True)
            pass
    def OnCbConnectButton(self, event):
        if event is not None:
            event.Skip()
        if self.cbConnect.GetValue():
            self.cbUpdate.Enable(False)
            self.__connect__()
        else:
            self.cbUpdate.Enable(False)
            self.__close__()
    def __getLocal__(self,sDN):
        try:
            lDir=os.listdir(sDN)
            self.lLocalFN=[]
            for f in lDir:
                sFN=os.path.join(sDN,f)
                s=os.stat(sFN)
                if stat.S_ISREG(s[stat.ST_MODE]):
                    self.lLocalFN.append(f)
        except:
            pass
    def __addPlugIn__(self,d,sPlugIn,sVer,sFN,what):
        if sPlugIn in d:
            try:
                iVerNums=[int(s) for s in sVer.split('.')]
            except:
                return
            if what not in d[sPlugIn]:
                d[sPlugIn][what]=''
                d[sPlugIn][what+'FN']=''
                bNewer=True
            else:
                sVerOld=d[sPlugIn][what]
                iVerOldNums=[int(s) for s in sVerOld.split('.')]
                bNewer=False
                for iVerOldNum,iVerNum in zip(iVerOldNums,iVerNums):
                    if iVerOldNum<iVerNum:
                        bNewer=True
                        break
            if bNewer:
                d[sPlugIn][what]=sVer
                d[sPlugIn][what+'FN']=sFN
        else:
            d[sPlugIn]={what:sVer,what+'FN':sFN}
    def __showPlugIn__(self):
        try:
            d={}
            self.dPlugIn=d
            for sFN in self.lLocalFN:
                strs=sFN.split('.')
                try:
                    if len(strs)<5:
                        continue
                    if strs[-1]!='vidarc':
                        continue
                    try:
                        int(strs[-1])
                        continue
                    except:
                        pass
                    sPlugIn='.'.join(strs[:-4])
                    sVer='.'.join(strs[-4:-1])
                    self.__addPlugIn__(d,sPlugIn,sVer,sFN,'local')
                except:
                    traceback.print_exc()
            for sFN in self.lRemoteFN:
                strs=sFN.split('.')
                try:
                    if len(strs)<5:
                        continue
                    if strs[-1]!='vidarc':
                        continue
                    sPlugIn='.'.join(strs[:-4])
                    sVer='.'.join(strs[-4:-1])
                    self.__addPlugIn__(d,sPlugIn,sVer,sFN,'remote')
                except:
                    traceback.print_exc()
            self.lstPlugIn.DeleteAllItems()
            keys=d.keys()
            keys.sort()
            for k in keys:
                idx=self.lstPlugIn.InsertStringItem(sys.maxint,k)
                dd=d[k]
                if 'remote' in dd:
                    self.lstPlugIn.SetStringItem(idx,1,dd['remote'])
                if 'local' in dd:
                    self.lstPlugIn.SetStringItem(idx,2,dd['local'])
            keys=self.dRemoteLocaleFN.keys()
            keys.sort()
            iSkip=len(self.VIDARC_ALIAS_LOCALE)+1
            for k in keys:
                #sys.stderr.write(k+'\n')
                sLocale=k[iSkip:]
                lst=self.dRemoteLocaleFN[k]
                for it in lst:
                    #sys.stderr.write('    '+it+'\n')
                    idx=self.lstPlugIn.InsertStringItem(sys.maxint,sLocale+'/'+it)
        except:
            traceback.print_exc()
    def __updatePlugIn__(self,lPlugIn,sDir,lLocale,sApplDir,netFile):
        iCount=len(lPlugIn)
        iAct=0
        self.bAuthenticate=False
        for sFN in lPlugIn:
            iAct+=1
            self.qFiles.put((self.VIDARC_ALIAS,sDir,sFN))
            self.bAuthenticate=True
        for sFN in lLocale:
            try:
                iAct+=1
                strs=sFN.split('/')
                sAlias='/'.join([self.VIDARC_ALIAS_LOCALE]+strs[:-1])
                #sApplLocale=os.path.join(sApplDir,'local',os.path.sep.join(strs[:-1]))
                #sApplLocale=os.path.join(sApplDir,os.path.sep.join(strs[:-1]))
                sApplLocale=os.path.join(os.path.join(sDir,'locale'),os.path.sep.join(strs[:-1]))
                self.qFiles.put((sAlias,sApplLocale,strs[-1]))
            except:
                traceback.print_exc()
                #vtLog.vtLngTB(self.GetName())
        self.pbPlugIns.SetValue(0)
        self.pbPlugIns.SetRange(iAct)
        #if iAct>0:
        #    t=self.qFiles.get()
        #    netFile.GetFile(t[0],t[1],t[2])
    def __updateFilesBar__(self):
        self.pbPlugIns.SetValue(self.qFilesProc.qsize())
        #self.pbPlugIns.SetRange(self.qFiles.qsize())
    def OnCbGenCertButton(self, event):
        event.Skip()
        import vMESCenterTools
        vMESCenterTools.mkCertificate()
    def OnCbLoginButton(self, event):
        if event is not None:
            event.Skip()
        self.cbLogin.Enable(False)
        if self.cltSock is not None:
            self.lRemoteFN=[]
            self.dRemoteLocaleFN={}
            self.iListContentAct=0
            self.iListContentCount=1
            self.__doLogin__()
            self.cltSock.__doLogin__()
    def OnTxtHostTextEnter(self, event):
        event.Skip()
        self.txtPort.SetFocus()
    def OnTxtPortTextEnter(self, event):
        event.Skip()
        self.txtUser.SetFocus()
    def OnTxtUserTextEnter(self, event):
        event.Skip()
        self.txtPasswd.SetFocus()
    def OnTxtPasswdTextEnter(self, event):
        try:
            event.Skip()
            if self.cbLogin.IsEnabled():
                self.cbLogin.SetFocus()
                self.OnCbLoginButton(None)
            else:
                self.cbConnect.SetFocus()
                self.OnCbConnectButton(None)
        except:
            traceback.print_exc(file=sys.stderr)
