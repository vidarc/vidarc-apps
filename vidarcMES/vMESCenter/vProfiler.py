#----------------------------------------------------------------------------
# Name:         vProfiler.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070522
# CVS-ID:       $Id: vProfiler.py,v 1.5 2009/04/26 21:52:03 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,traceback,time,datetime,thread,threading
from wx import Thread_IsMain

if sys.platform.startswith('win'):
    REPLACE_DRIVE_JUNK=True
    REQ_PREFIX=os.getenv('vidarcSrc','V:\\python')
else:
    REPLACE_DRIVE_JUNK=False
    REQ_PREFIX=os.getenv('vidarcSrc','/opt/vidarc/python')

_lock=threading.Lock()

#import hotshot
from collections import deque
try:
    from resource import getrusage, RUSAGE_SELF
except ImportError:
    try:
        import win32process
        HPROCMAIN=win32process.GetCurrentProcess()
        RUSAGE_SELF = 0
        def getrusage(who=0):
            d=win32process.GetProcessTimes(HPROCMAIN)
            return [d['UserTime']*1E-9, d['KernelTime']*1E-9] # on non-UNIX platforms cpu_time always 0.0
    except ImportError:
        RUSAGE_SELF = 0
        def getrusage(who=0):
            return [0.0, 0.0] # on non-UNIX platforms cpu_time always 0.0

p_stats = None
p_start_time = None
p_file_callstack = None
p_file_profile = None
p_zDT = None
p_level=-1
p_full_callstack=False
def profiler(frame, event, arg):
    if event not in ('call','return'): return profiler
    #### gather stats ####
    rusage = getrusage(RUSAGE_SELF)
    t_cpu = rusage[0] + rusage[1] # user time + system time
    code = frame.f_code 
    if REQ_PREFIX is not None:
        if p_full_callstack==False:
            if code.co_filename.startswith(REQ_PREFIX)==False:
                return profiler
    #if REQ_PREFIX is not None:
    #            if code.co_filename.startswith(REQ_PREFIX)==False:
    #                return profiler
    fun = (code.co_name, code.co_filename, code.co_firstlineno)
    #print event,fun
    #### get stack with functions entry stats ####
    ct = threading.currentThread()
    bMainThread=Thread_IsMain()
    if bMainThread:
        sThdIdent='main'
    else:
        try:
            sThdIdent=str(thread.get_ident())
        except:
            sThdIdent='main'
    try:
        p_stack = ct.p_stack
    except AttributeError:
        ct.p_stack = deque()
        p_stack = ct.p_stack
    #### handle call and return ####
    if event == 'call':
        #p_stack.append((time.time(), t_cpu, fun))
        p_stack.append((p_zDT.now(), t_cpu, fun))
    elif event == 'return':
        try:
            t,t_cpu_prev,f = p_stack.pop()
            assert f == fun
        except IndexError: # TODO investigate
            t,t_cpu_prev,f = p_start_time, 0.0, None
        call_cnt, t_sum, t_cpu_sum = p_stats.get(fun, (0, 0.0, 0.0))
        zNow=p_zDT.now()
        zDiff=zNow-t
        fDiff=zDiff.seconds + zDiff.microseconds/1.0e6
        #p_stats[fun] = (call_cnt+1, t_sum+time.time()-t, t_cpu_sum+t_cpu-t_cpu_prev)
        p_stats[fun] = (call_cnt+1, t_sum+fDiff, t_cpu_sum+t_cpu-t_cpu_prev)
    if p_file_callstack is not None:
        if event in ['call','return']:
            act_frame=frame
            zNow=time.time()
            iMilliSec=(zNow - long(zNow)) * 1000
            sNow=','.join([time.strftime(u"%Y-%m-%d %H:%M:%S", time.localtime(zNow)),
                            u"%03d" % (iMilliSec)])
            #sNow='|'.join([sNow,'CS',event,' '])
            if REPLACE_DRIVE_JUNK:
                sFN=code.co_filename[2:]
            else:
                sFN=code.co_filename
            if event == 'call':
                deep=p_level
            else:
                deep=1
            def doFmt(t):
                fn,lno,sMethod,cmd=t
                if REPLACE_DRIVE_JUNK:
                    return ''.join([fn[2:],':',sMethod,'(%d)'%lno])
                else:
                    return ''.join([fn,':',sMethod,'(%d)'%lno])
            #print len(lStack)
            #print lStack
            l=[doFmt(t) for t in traceback.extract_stack(act_frame,limit=deep)]
            l.reverse()
            
            s='|'.join([sNow,'CS',event,
                    '%s(%04d);thd:%s;(%04d)'%(
                        code.co_name, code.co_firstlineno,sThdIdent,
                        act_frame.f_lineno),
                    #'%s;thd:%s'%(l[1],sThdIdent),
                    '%s\n'%(','.join(l))])
            _lock.acquire()
            p_file_callstack.write(s)
            _lock.release()
    return profiler


def profile_on(fn,lv):
    global p_stats, p_start_time , p_file_callstack , p_file_profile
    global p_level,p_zDT,p_full_callstack
    
    if lv<-1:
        lv=-lv
        p_full_callstack=True
    p_level=lv
    p_zDT=datetime.datetime(2007,1,15)
    sNow=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
    
    try:
        # search enviroment
        sLogDN=os.getenv('vidarcLogDN',None)
    except:
        sLogDN=None
    sCallstackFN='%s.%s.cs.log'%(fn,sNow)
    
    sProfFN='%s.%s.prof.csv'%(fn,sNow)
    if sLogDN is not None:
        try:
            os.makedirs(sLogDN)
        except:
            pass
        lngCallstackFN=os.path.join(sLogDN,sCallstackFN)
        lngProfFN=os.path.join(sLogDN,sProfFN)
    else:
        lngCallstackFN=sCallstackFN
        lngProfFN=sProfFN
    if lv>0:
        try:
            p_file_callstack=open(lngCallstackFN,'w')
        except:
            p_file_callstack=None
    else:
        p_file_callstack=None
    try:
        p_file_profile=open(lngProfFN,'w')
    except:
        p_file_profile=None
    p_stats = {}
    p_start_time = p_zDT.now()      #time.time()
    threading.setprofile(profiler)
    sys.setprofile(profiler)


def profile_off():
    threading.setprofile(None)
    sys.setprofile(None)
def profile_close():
        if p_file_callstack is not None:
            p_file_callstack.close()
        if p_file_profile is not None:
            p_file_profile.write('file;func;line;call;zReal;zCPU\n')
            for tFunc,tStat in get_profile_stats().items():
                p_file_profile.write('%s;%s;%d;%d;%f;%f\n'%(tFunc[1],
                    tFunc[0],tFunc[2],tStat[0],tStat[1],tStat[2]))
                #p_file_profile.write('\n')
            p_file_profile.close()

def get_profile_stats():
    """
    returns dict[function_tuple] -> stats_tuple
    where
      function_tuple = (function_name, filename, lineno)
      stats_tuple = (call_cnt, real_time, cpu_time)
    """
    return p_stats
