#----------------------------------------------------------------------------
# Name:         vMESCenterTools.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060711
# CVS-ID:       $Id: vMESCenterTools.py,v 1.4 2009/04/25 13:52:51 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,traceback,sys
import vLogFallBack
import vSystem

import __init__
def buildCrypto(sApplName,sLogin,sPasswd,oCrypto,sLocalDN):  #'~'
    try:
        if oCrypto is None:
            try:
                import vidarc.tool.sec.vtSecCrypto as vtSecCrypto
                oCrypto=vtSecCrypto.vtSecCrypto(sPasswd,None,dn=sLocalDN,keySize=512)
                iRet=oCrypto.ImportPrivKey(''.join([sApplName,'_',sLogin,'.priv']),phrase=sPasswd)
            except:
                iRet=0
        else:
            iRet=oCrypto.ImportPrivKey(''.join([sApplName,'_',sLogin,'.priv']),phrase=sPasswd)
        if iRet==0:
            return oCrypto,True
        elif iRet==-2:
            if oCrypto.CreateKey(sPasswd,keySize=512)==False:
                dlg=wx.MessageDialog(None,_(u'Error creating logon infos.') ,
                                sApplName,
                                wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return oCrypto,False
            if oCrypto.ExportPrivKey(''.join([sApplName,'_',sLogin,'.priv']),sLocalDN,phrase=sPasswd)==False:
                dlg=wx.MessageDialog(None,_(u'Error exporting logon infos.') ,
                            sApplName,
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.ShowModal()
                dlg.Destroy()
                return oCrypto,False
            return oCrypto,True
        dlg=wx.MessageDialog(None,_(u'Login incorrect.') ,
                            sApplName,
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
        dlg.ShowModal()
        dlg.Destroy()
        return oCrypto,False
    except:
        sys.stderr.write(traceback.format_exc())

def buildLoginStore(sApplName,sLogin,sPasswd,sLocalDN):
    try:
        import vidarc.tool.sec.vtSecLoginStore as vtSecLoginStore
        
        sPrivKey=''.join([sApplName,'_',sLogin,'.priv'])
        #sDN=os.path.expanduser('~')
        sDN=sLocalDN
        sCfgFN=os.path.join(sDN,''.join([sApplName,'_',sLogin,'Cfg','.xml']))
        sCfgAppl=''.join([sApplName,'Cfg'])
        oCryptoStore=vtSecLoginStore.vtSecLoginStore(appl=sCfgAppl,
                phrase=sPasswd,fn=sPrivKey,dn=sLocalDN,keySize=512)
        if oCryptoStore.Open(sCfgFN)<0:
            oCryptoStore.New()
            oCryptoStore.Save(sCfgFN)
        oCryptoStore.SetFallback(sLogin,sPasswd)
    except:
        sys.stderr.write(traceback.format_exc())
        oCryptoStore=None
    return oCryptoStore

if __init__.SSL or 1:
    def mkCertificate(bForce=True,privkey=None,cert=None,verify=None,DN=None,host=None,lCreate=None):
        try:
            if DN is None:
                #DN=os.getcwd()
                DN=vSystem.getUsrCfgDN('VIDARC')
            if privkey is None:
                privkey='vMESClt.pkey'
            if cert is None:
                cert='vMESClt.cert'
            if verify is None:
                verify='CA.cert'
            #sys.stderr.write('\nfn:%s %d\n'%(os.path.join(DN, privkey),os.path.exists(os.path.join(DN, privkey))))
            if bForce or os.path.exists(os.path.join(DN, privkey))==False:
                import certgen
                certgen.mkClientCert(DN,host,lCreate)
                return
        except:
            sys.stderr.write(traceback.format_exc())
            vLogFallBack.logTB()
else:
    def mkCertificate(bForce=True,privkey=None,cert=None,verify=None,DN=None):
        pass
    

