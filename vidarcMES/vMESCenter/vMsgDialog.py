#Boa:Dialog:vMsgDialog
#----------------------------------------------------------------------------
# Name:         vMsgDialog.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070710
# CVS-ID:       $Id: vMsgDialog.py,v 1.3 2008/02/05 17:07:11 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import cStringIO
import sys

import vLogFallBack as lgFB
#import vidarc.tool.log.vtLog as vtLog
#import vidarc.tool.art.vtArt as vtArt
import vtLgBase


def create(parent):
    return vMsgDialog(parent)

def getApplData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x02\
\x00\x00\x00\x90\x91h6\x00\x00\x00\x03sBIT\x08\x08\x08\xdb\xe1O\xe0\x00\x00\
\x00\x96IDAT(\x91c\\~n\x07\x03)\x80\x05S\xa8}\xcf|8\xbb\xd2%\x11M\x96\tS\x03\
\\\x11\xa6j\x06\x06\x06F\xb8\x93\x90\r\xc6jJ\xfb\x9e\xf9\x95.\x89Xl\xc0\n\
\xe0\xc6\xa1k8_\xb2\xf2|\xc9J\\\\\xa8\x06d\xc7\x18\xf6\x84\xa3\x19\x01\x11\
\x81\xfb\x87\t\xd3\x03\x86=\xe1\x10S\xcf\x97\xacD\xd6\x0f\xd1C\xac\x1fP\x9c\
\xc4\x80\x11\x82\x10K\xd0\x9c\x07q\x05\x13\xa6j\xac\x00g(![\x82\xcfI\x04\x01\
\xdc\x15\x8cX\x13\x1f\xc4\x01X\x9d\x8a\xc5\x06\xb8s\xb1&\x16\xec6\xe0\x01\
\x00\x98\xff>\xd3\x83on\xdb\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplBitmap():
    return wx.BitmapFromImage(getApplImage())

def getApplImage():
    stream = cStringIO.StringIO(getApplData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

_icon=None
def getApplIcon():
    global _icon
    if _icon is None:
        _icon = wx.EmptyIcon()
        _icon.CopyFromBitmap(getApplBitmap())
    return _icon
    
vEVT_MSGDLG_OK=wx.NewEventType()
def EVT_MSGDLG_OK(win,func):
    win.Connect(-1,-1,vEVT_MSGDLG_OK,func)
def EVT_MSGDLG_OK_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vEVT_MSGDLG_OK,func)
class vMsgDlgOk(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_MSGDLG_OK(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_MSGDLG_OK)

vEVT_MSGDLG_CANCEL=wx.NewEventType()
def EVT_MSGDLG_CANCEL(win,func):
    win.Connect(-1,-1,vEVT_MSGDLG_CANCEL,func)
def EVT_MSGDLG_CANCEL_DISCONNECT(win,func):
    win.Disconnect(-1,-1,vEVT_MSGDLG_CANCEL,func)
class vMsgDlgCancel(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_MSGDLG_CANCEL(<widget_name>, xxx)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_MSGDLG_CANCEL)

[wxID_VMSGDIALOG, wxID_VMSGDIALOGBMPINFO, wxID_VMSGDIALOGCBAPPLY, 
 wxID_VMSGDIALOGCBCANCEL, wxID_VMSGDIALOGLBLMSG, 
] = [wx.NewId() for _init_ctrls in range(5)]

class vMsgDialog(wx.Dialog):
    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(32, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_fgsMain_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.bmpInfo, 0, border=8, flag=wx.ALL)
        parent.AddWindow(self.lblMsg, 0, border=4, flag=wx.EXPAND | wx.ALL)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4,
              flag=wx.BOTTOM | wx.ALIGN_CENTER | wx.RIGHT | wx.LEFT | wx.TOP)

    def _init_coll_fgsMain_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(1)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMain = wx.FlexGridSizer(cols=2, hgap=0, rows=2, vgap=0)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsMain_Items(self.fgsMain)
        self._init_coll_fgsMain_Growables(self.fgsMain)
        self._init_coll_bxsBt_Items(self.bxsBt)

        self.SetSizer(self.fgsMain)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VMSGDIALOG, name=u'vMsgDialog',
              parent=prnt, pos=wx.Point(47, 8), size=wx.Size(244, 113),
              style=wx.STAY_ON_TOP | wx.DEFAULT_DIALOG_STYLE,
              title=u'vMsgDialog')
        self.SetClientSize(wx.Size(236, 86))

        self.bmpInfo = wx.StaticBitmap(bitmap=wx.EmptyBitmap(32, 32),
              id=wxID_VMSGDIALOGBMPINFO, name=u'bmpInfo', parent=self,
              pos=wx.Point(8, 8), size=wx.Size(32, 32), style=0)

        self.lblMsg = wx.StaticText(id=wxID_VMSGDIALOGLBLMSG, label=u' ',
              name=u'lblMsg', parent=self, pos=wx.Point(52, 4),
              size=wx.Size(180, 40), style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.lblMsg.SetMinSize(wx.Size(-1, -1))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(32, 32),#vtArt.getBitmap(vtArt.Apply),
              id=wxID_VMSGDIALOGCBAPPLY, label=_(u'Ok'), name=u'cbApply',
              parent=self, pos=wx.Point(52, 52), size=wx.Size(76, 30), style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VMSGDIALOGCBAPPLY)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(bitmap=wx.EmptyBitmap(32, 32),#vtArt.getBitmap(vtArt.Cancel),
              id=wxID_VMSGDIALOGCBCANCEL, label=_(u'Cancel'), name=u'cbCancel',
              parent=self, pos=wx.Point(160, 52), size=wx.Size(76, 30),
              style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VMSGDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent,sMsg,sCaption,style=wx.OK|wx.CANCEL,bmp=None,sApply=None,sCancel=None):
        global _
        _=vtLgBase.assignPluginLang()#('vMESCenter')
        self._init_ctrls(parent)
        self.cbApply.SetBitmapLabel(getApplyBitmap())
        self.cbCancel.SetBitmapLabel(getCancelBitmap())
        try:
            if style & wx.YES_NO:
                self.bYesNo=True
                self.cbApply.SetLabel(_(u'Yes'))
                self.cbCancel.SetLabel(_(u'No'))
                if style & wx.YES_DEFAULT:
                    self.SetDefaultItem(self.cbApply)
                    self.cbApply.SetFocus()
                elif style & wx.NO_DEFAULT:
                    self.SetDefaultItem(self.cbCancel)
                    self.cbCancel.SetFocus()
                else:
                    pass
            else:
                self.bYesNo=False
                wid=None
                if (style & wx.CANCEL)==0:
                    self.cbCancel.Show(False)
                    wid=self.cbCancel
                if (style & wx.OK)==0:
                    self.cbApply.Show(False)
                    wid=self.cbApply
                if wid is not None:
                    self.SetDefaultItem(wid)
                    wid.SetFocus()
            if bmp is None:
                if style & wx.ICON_EXCLAMATION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_WARNING,size=(32,32))
                elif style & wx.ICON_ERROR:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_ERROR,size=(32,32))
                elif style & wx.ICON_INFORMATION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_INFORMATION,size=(32,32))
                elif style & wx.ICON_INFORMATION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_INFORMATION,size=(32,32))
                elif style & wx.ICON_QUESTION:
                    bmp=wx.ArtProvider.GetBitmap(wx.ART_QUESTION,size=(32,32))
            if bmp is not None:
                self.bmpInfo.SetBitmap(bmp)
            else:
                self.bmpInfo.Show(False)
            #img=wx.EmptyIcon()
            #img.CopyFromBitmap(bmp)
            self.SetIcon(getApplIcon())#img)
            self.lblMsg.SetLabel(sMsg)
            self.SetTitle(sCaption)
            self.fgsMain.Layout()
            self.fgsMain.Fit(self)
        except:
            lgFB.logTB()
            #vtLog.vtLngTB(self.GetName())
        self.Centre()
    def OnCbApplyButton(self, event):
        event.Skip()
        try:
            if self.IsModal():
                if self.bYesNo:
                    self.EndModal(wx.ID_YES)
                else:
                    self.EndModal(wx.ID_OK)
            else:
                wx.PostEvent(self,vMsgDlgOk(self))
                self.Show(False)
        except:
            lgFB.logTB()
            #vtLog.vtLngTB(self.GetName())
    def OnCbCancelButton(self, event):
        event.Skip()
        try:
            if self.IsModal():
                if self.bYesNo:
                    self.EndModal(wx.ID_NO)
                else:
                    self.EndModal(wx.ID_CANCEL)
            else:
                wx.PostEvent(self,vMsgDlgCancel(self))
                self.Show(False)
        except:
            lgFB.logTB()
            #vtLog.vtLngTB(self.GetName())
    def BindEvents(self,funcOk=None,funcCancel=None):
        if funcOK is not None:
            EVT_MSGDLG_OK(self,funcOk)
        if funcCancel is not None:
            EVT_MSGDLG_CANCEL(self,funcCancel)
