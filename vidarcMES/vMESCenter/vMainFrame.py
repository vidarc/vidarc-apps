#----------------------------------------------------------------------------
# Name:         vMainFrame.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060515
# CVS-ID:       $Id: vMainFrame.py,v 1.1 2006/07/17 11:38:57 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

class vMainFrame:
    def __init__(self):
        
        pass
    def AddMenus(self,frmMain,widTree=None,widNetMaster=None,
                    mnFile=None,mnView=None,mnTools=None):
        if widTree is not None:
            if mnTools is not None and frmMain is not None:
                widTree.AddMenuTool(mnTools,frmMain,0)
            iPos=0
            if mnView is not None and frmMain is not None:
                mn,iPos=widTree.AddMenuGroup(mnView,frmMain,iPos)
            if mnView is not None and frmMain is not None:
                widTree.AddMenuLang(mnView,frmMain,iPos)
        if widNetMaster is not None:
            if mnFile is not None and frmMain is not None:
                widNetMaster.AddMenuGeneral(mnFile,frmMain,0)
