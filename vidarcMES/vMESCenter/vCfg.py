#----------------------------------------------------------------------------
# Name:         vCfg.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20070520
# CVS-ID:       $Id: vCfg.py,v 1.2 2008/02/04 16:35:26 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,traceback,types

class vCfgData:
    def __init__(self,docCfg):
        self.docCfg=docCfg
    def __getPluginCfg__(self,l):
        try:
            if self.docCfg is None:
                return {}
            d={}
            cfgNode=self.docCfg.getChildByLstForced(None,l)
            if cfgNode is not None:
                for c in self.docCfg.getChilds(cfgNode):
                    sTagName=self.docCfg.getTagName(c)
                    if self.docCfg.hasChilds(c):
                        dd=self.__getPluginCfg__(l+[sTagName])
                        d[sTagName]=dd
                    else:
                        d[sTagName]=self.docCfg.getText(c)
            return d
        except:
            sys.stderr.write(traceback.format_exc())
            #vtLog.vtLngTB(self.GetName())
            return {}
    def __setPluginCfg__(self,l,d):
        try:
            if self.docCfg is None:
                return 0
            import vidarc.tool.InOut.fnUtil as fnUtil
            cfgNode=self.docCfg.getChildByLstForced(None,l)
            for c in self.docCfg.getChilds(cfgNode):
                self.docCfg.deleteNode(c,cfgNode)
            def setCfgData(cfgNode,d):
                keys=d.keys()
                keys.sort()
                for k in keys:
                    kr=fnUtil.replaceSuspectChars(k)
                    if type(d[k])==types.DictionaryType:
                        cc=self.docCfg.getChildForced(cfgNode,k)
                        setCfgData(cc,d[k])
                    else:
                        self.docCfg.setNodeText(cfgNode,kr,unicode(d[k]))
            setCfgData(cfgNode,d)
            self.docCfg.AlignNode(cfgNode)
            self.docCfg.Save()
            return 0
        except:
            sys.stderr.write(traceback.format_exc())
            #vtLog.vtLngTB(self.GetName())
            return -1
