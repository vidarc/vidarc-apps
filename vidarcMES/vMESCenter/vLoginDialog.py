#Boa:Dialog:vLoginDialog
#----------------------------------------------------------------------------
# Name:         vLoginDialog.py
# Purpose:      login dialog panel
#
# Author:       Walter Obweger
#
# Created:      200060208
# CVS-ID:       $Id: vLoginDialog.py,v 1.3 2010/02/26 00:59:52 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons

import vtLgBase
import images
import sha

def create(parent):
    return vLoginDialog(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VLOGINDIALOG, wxID_VLOGINDIALOGCBAPPLY, wxID_VLOGINDIALOGCBCANCEL, 
 wxID_VLOGINDIALOGCBCHANGE, wxID_VLOGINDIALOGLBLLOGIN, 
 wxID_VLOGINDIALOGLBLPASSWD, wxID_VLOGINDIALOGLBLPASSWDCONF, 
 wxID_VLOGINDIALOGLBLPASSWDNEW, wxID_VLOGINDIALOGTXTLOGIN, 
 wxID_VLOGINDIALOGTXTPASSWD, wxID_VLOGINDIALOGTXTPASSWDCONF, 
 wxID_VLOGINDIALOGTXTPASSWDNEW, 
] = [wx.NewId() for _init_ctrls in range(12)]

class vLoginDialog(wx.Dialog):
    def _init_coll_bxsLogin_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblLogin, 1, border=4, flag=wx.EXPAND | wx.RIGHT)
        parent.AddWindow(self.txtLogin, 2, border=4, flag=wx.EXPAND)

    def _init_coll_bxsPasswdNew_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswdNew, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswdNew, 2, border=0, flag=wx.EXPAND)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.bxsLogin, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsPasswd, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsPasswdNew, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSizer(self.bxsPasswxConf, 1, border=4,
              flag=wx.EXPAND | wx.TOP | wx.RIGHT | wx.LEFT)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.bxsBt, 0, border=4, flag=wx.ALIGN_CENTER)

    def _init_coll_bxsPasswd_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswd, 1, border=4, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswd, 2, border=0, flag=wx.EXPAND)

    def _init_coll_bxsBt_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.cbApply, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbChange, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(16, 8), border=0, flag=0)
        parent.AddWindow(self.cbCancel, 0, border=0, flag=0)

    def _init_coll_bxsPasswxConf_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.lblPasswdConf, 1, border=4,
              flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.txtPasswdConf, 2, border=4, flag=wx.EXPAND)

    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=6, vgap=0)

        self.bxsLogin = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPasswd = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPasswxConf = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.bxsPasswdNew = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)
        self._init_coll_bxsLogin_Items(self.bxsLogin)
        self._init_coll_bxsPasswd_Items(self.bxsPasswd)
        self._init_coll_bxsBt_Items(self.bxsBt)
        self._init_coll_bxsPasswxConf_Items(self.bxsPasswxConf)
        self._init_coll_bxsPasswdNew_Items(self.bxsPasswdNew)

        self.SetSizer(self.fgsData)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_VLOGINDIALOG, name=u'vLoginDialog',
              parent=prnt, pos=wx.Point(380, 172), size=wx.Size(264, 171),
              style=wx.RESIZE_BORDER | wx.DEFAULT_DIALOG_STYLE,
              title=_(u'vLogin Dialog'))
        self.SetClientSize(wx.Size(256, 144))

        self.lblLogin = wx.StaticText(id=wxID_VLOGINDIALOGLBLLOGIN,
              label=_(u'login'), name=u'lblLogin', parent=self, pos=wx.Point(4,
              4), size=wx.Size(78, 21), style=wx.ALIGN_RIGHT)
        self.lblLogin.SetMinSize(wx.Size(-1, -1))

        self.txtLogin = wx.TextCtrl(id=wxID_VLOGINDIALOGTXTLOGIN,
              name=u'txtLogin', parent=self, pos=wx.Point(86, 4),
              size=wx.Size(165, 21), style=0, value=u'')
        self.txtLogin.SetMinSize(wx.Size(-1, -1))

        self.lblPasswd = wx.StaticText(id=wxID_VLOGINDIALOGLBLPASSWD,
              label=_(u'password'), name=u'lblPasswd', parent=self,
              pos=wx.Point(4, 29), size=wx.Size(78, 21), style=wx.ALIGN_RIGHT)
        self.lblPasswd.SetMinSize(wx.Size(-1, -1))

        self.txtPasswd = wx.TextCtrl(id=wxID_VLOGINDIALOGTXTPASSWD,
              name=u'txtPasswd', parent=self, pos=wx.Point(86, 29),
              size=wx.Size(165, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswd.SetMinSize(wx.Size(-1, -1))

        self.lblPasswdNew = wx.StaticText(id=wxID_VLOGINDIALOGLBLPASSWDNEW,
              label=_(u'new password'), name=u'lblPasswdNew', parent=self,
              pos=wx.Point(4, 54), size=wx.Size(78, 21), style=wx.ALIGN_RIGHT)
        self.lblPasswdNew.SetMinSize(wx.Size(-1, -1))

        self.txtPasswdNew = wx.TextCtrl(id=wxID_VLOGINDIALOGTXTPASSWDNEW,
              name=u'txtPasswdNew', parent=self, pos=wx.Point(86, 54),
              size=wx.Size(165, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswdNew.SetMinSize(wx.Size(-1, -1))

        self.lblPasswdConf = wx.StaticText(id=wxID_VLOGINDIALOGLBLPASSWDCONF,
              label=_(u'confirm'), name=u'lblPasswdConf', parent=self,
              pos=wx.Point(4, 79), size=wx.Size(78, 21), style=wx.ALIGN_RIGHT)
        self.lblPasswdConf.SetMinSize(wx.Size(-1, -1))

        self.txtPasswdConf = wx.TextCtrl(id=wxID_VLOGINDIALOGTXTPASSWDCONF,
              name=u'txtPasswdConf', parent=self, pos=wx.Point(86, 79),
              size=wx.Size(165, 21), style=wx.TE_PASSWORD, value=u'')
        self.txtPasswdConf.SetMinSize(wx.Size(-1, -1))

        self.cbApply = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOGINDIALOGCBAPPLY,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Apply'), name=u'cbApply',
              parent=self, pos=wx.Point(-2, 108), size=wx.Size(76, 30),
              style=0)
        self.cbApply.SetMinSize(wx.Size(-1, -1))
        self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
              id=wxID_VLOGINDIALOGCBAPPLY)

        self.cbChange = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOGINDIALOGCBCHANGE,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Change'),
              name=u'cbChange', parent=self, pos=wx.Point(90, 108),
              size=wx.Size(76, 30), style=0)
        self.cbChange.SetMinSize(wx.Size(-1, -1))
        self.cbChange.Bind(wx.EVT_BUTTON, self.OnCbChangeButton,
              id=wxID_VLOGINDIALOGCBCHANGE)

        self.cbCancel = wx.lib.buttons.GenBitmapTextButton(ID=wxID_VLOGINDIALOGCBCANCEL,
              bitmap=wx.EmptyBitmap(16, 16), label=_(u'Cancel'),
              name=u'cbCancel', parent=self, pos=wx.Point(182, 108),
              size=wx.Size(76, 30), style=0)
        self.cbCancel.SetMinSize(wx.Size(-1, -1))
        self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
              id=wxID_VLOGINDIALOGCBCANCEL)

        self._init_sizers()

    def __init__(self, parent):
        self._init_ctrls(parent)
        icon = getApplicationIcon()
        self.SetIcon(icon)
        self.cbApply.SetBitmapLabel(images.getApplyBitmap())
        self.cbApply.SetDefault()
        self.cbChange.SetBitmapLabel(images.getEditBitmap())
        self.cbCancel.SetBitmapLabel(images.getCancelBitmap())
        self.Clear()
        self.SetType(0)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def SetType(self,iNum):
        if iNum==0:
            self.txtLogin.Enable(True)
            self.txtPasswd.Enable(True)
            self.lblPasswdNew.Show(False)
            self.txtPasswdNew.Show(False)
            self.lblPasswdConf.Show(False)
            self.txtPasswdConf.Show(False)
            self.cbChange.Show(True)
            self.txtLogin.SetFocus()
        elif iNum==1:
            self.txtLogin.Enable(False)
            self.txtPasswd.Enable(False)
            self.lblPasswdNew.Show(True)
            self.txtPasswdNew.Show(True)
            self.lblPasswdConf.Show(True)
            self.txtPasswdConf.Show(True)
            self.cbChange.Show(False)
            self.txtPasswdNew.SetFocus()
        elif iNum==2:
            self.lblPasswdNew.Show(True)
            self.txtPasswdNew.Show(True)
            self.lblPasswdConf.Show(True)
            self.txtPasswdConf.Show(True)
            self.cbChange.Show(False)
        elif iNum==10:
            self.txtLogin.Enable(True)
            self.txtPasswd.Enable(True)
            self.lblPasswdNew.Show(False)
            self.txtPasswdNew.Show(False)
            self.lblPasswdConf.Show(False)
            self.txtPasswdConf.Show(False)
            self.cbChange.Show(False)
            self.txtLogin.SetFocus()
        elif iNum==11:
            self.txtLogin.Enable(False)
            self.txtPasswd.Enable(True)
            self.lblPasswdNew.Show(False)
            self.txtPasswdNew.Show(False)
            self.lblPasswdConf.Show(False)
            self.txtPasswdConf.Show(False)
            self.cbChange.Show(False)
            self.txtPasswd.SetFocus()
        self.iNum=iNum
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def Clear(self):
        self.sLogin=''
        self.sPasswd=''
        self.txtLogin.SetValue('')
        self.txtPasswd.SetValue('')
        self.txtPasswdNew.SetValue('')
        self.txtPasswdConf.SetValue('')
        self.txtLogin.SetFocus()
    def GetLogin(self):
        return self.sLogin
    def GetPasswd(self):
        return self.sPasswd
    def OnCbApplyButton(self, event):
        event.Skip()
        self.sLogin=self.txtLogin.GetValue()
        if self.iNum==0:
            self.sPasswd=self.txtPasswd.GetValue()
            if len(self.sPasswd)>0:
                m=sha.new()
                m.update(self.sPasswd)
                self.sPasswd=m.hexdigest()
                
            if len(self.sLogin)>0:
                self.EndModal(1)
        elif self.iNum==1:
            sPasswd=self.txtPasswdNew.GetValue()
            sPasswdConf=self.txtPasswdConf.GetValue()
            if sPasswd==sPasswdConf:
                self.sPasswd=sPasswd
                if len(self.sPasswd)>0:
                    m=sha.new()
                    m.update(self.sPasswd)
                    self.sPasswd=m.hexdigest()
                    
                if len(self.sLogin)>0:
                    self.EndModal(3)
            else:
                dlg = wx.MessageDialog(self,
                            _(u'Passwords do not match.'),
                            _(u'vMES Center')+u' '+_(u'Password Change'),
                            wx.OK  | wx.ICON_ERROR)
                dlg.ShowModal()
                dlg.Destroy()
                self.txtPasswdNew.SetValue('')
                self.txtPasswdConf.SetValue('')
                self.txtPasswdNew.SetFocus()
        elif self.iNum==10:
            self.sPasswd=self.txtPasswd.GetValue()
            self.EndModal(10)
        elif self.iNum==11:
            self.sPasswd=self.txtPasswd.GetValue()
            self.EndModal(11)
    def OnCbCancelButton(self, event):
        event.Skip()
        self.EndModal(0)

    def OnCbChangeButton(self, event):
        event.Skip()
        self.sLogin=self.txtLogin.GetValue()
        self.sPasswd=self.txtPasswd.GetValue()
        if len(self.sPasswd)>0:
            m=sha.new()
            m.update(self.sPasswd)
            self.sPasswd=m.hexdigest()
            
        if len(self.sLogin)>0:
            self.EndModal(2)
