#Boa:MDIParent:vMESCenterParentFrame
#----------------------------------------------------------------------------
# Name:         vMESCenterParentFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vMESCenterParentFrame.py,v 1.49 2012/01/29 17:14:58 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import wx.lib.buttons
import wx.lib.foldpanelbar as fpb

import Queue,time,copy

from vStatusBarMessageLine import vStatusBarMessageLine
import vAboutDialog
import vSystem as vSystem
import IniFile

VERBOSE_LANG=0
TOP_SASH_MAX=260
TOP_SASH_MIN=5
NAV_SASH_MAX=240
NAV_SASH_MIN=5

import string,os,os.path,types,traceback,sys
try:
    #sys.stderr.write('\nimport vLoginDialog\n')
    from vLoginDialog import vLoginDialog
    #sys.stderr.write('\nimport vMESSettingsDialog\n')
    from vMESSettingsDialog import *
    #sys.stderr.write('\nimport vMESConfigDialog\n')
    from vMESConfigDialog import *
    #sys.stderr.write('\nimport vMESpluginFind\n')
    #sys.stderr.write('\nimport vMESPlugInUpdateDialog\n')
    #sys.stderr.write('\nimport vMESCenterTools\n')
    import vMESCenterTools
    import vGuiThread
    import vMESRecentTree
    import vMESPluginTree
    import vMESHyperBrowseTree
    from vMESPluginContainer import vMESPluginContainer
    from vLogFallBack import vLogFallBack
    from vMESHyperBrowseDialog import vMESHyperBrowseDialog
    from vMsgDialog import vMsgDialog
except:
    #sys.stderr.write('\nimport\n')
    sys.stderr.write(traceback.format_exc())
import vtLogDef
import vtLgBase
import os,os.path
#sys.stderr.write('\n\n'+vtLgBase.getApplLang()+'\n')
import identify as vIdentify

import images


def GetCollapsedIconData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x8eIDAT8\x8d\xa5\x93-n\xe4@\x10\x85?g\x03\n6lh)\xc4\xd2\x12\xc3\x81\
\xd6\xa2I\x90\x154\xb9\x81\x8f1G\xc8\x11\x16\x86\xcd\xa0\x99F\xb3A\x91\xa1\
\xc9J&\x96L"5lX\xcc\x0bl\xf7v\xb2\x7fZ\xa5\x98\xebU\xbdz\xf5\\\x9deW\x9f\xf8\
H\\\xbfO|{y\x9dT\x15P\x04\x01\x01UPUD\x84\xdb/7YZ\x9f\xa5\n\xce\x97aRU\x8a\
\xdc`\xacA\x00\x04P\xf0!0\xf6\x81\xa0\xf0p\xff9\xfb\x85\xe0|\x19&T)K\x8b\x18\
\xf9\xa3\xe4\xbe\xf3\x8c^#\xc9\xd5\n\xa8*\xc5?\x9a\x01\x8a\xd2b\r\x1cN\xc3\
\x14\t\xce\x97a\xb2F0Ks\xd58\xaa\xc6\xc5\xa6\xf7\xdfya\xe7\xbdR\x13M2\xf9\
\xf9qKQ\x1fi\xf6-\x00~T\xfac\x1dq#\x82,\xe5q\x05\x91D\xba@\xefj\xba1\xf0\xdc\
zzW\xcff&\xb8,\x89\xa8@Q\xd6\xaaf\xdfRm,\xee\xb1BDxr#\xae\xf5|\xddo\xd6\xe2H\
\x18\x15\x84\xa0q@]\xe54\x8d\xa3\xedf\x05M\xe3\xd8Uy\xc4\x15\x8d\xf5\xd7\x8b\
~\x82\x0fh\x0e"\xb0\xad,\xee\xb8c\xbb\x18\xe7\x8e;6\xa5\x89\x04\xde\xff\x1c\
\x16\xef\xe0p\xfa>\x19\x11\xca\x8d\x8d\xe0\x93\x1b\x01\xd8m\xf3(;x\xa5\xef=\
\xb7w\xf3\x1d$\x7f\xc1\xe0\xbd\xa7\xeb\xa0(,"Kc\x12\xc1+\xfd\xe8\tI\xee\xed)\
\xbf\xbcN\xc1{D\x04k\x05#\x12\xfd\xf2a\xde[\x81\x87\xbb\xdf\x9cr\x1a\x87\xd3\
0)\xba>\x83\xd5\xb97o\xe0\xaf\x04\xff\x13?\x00\xd2\xfb\xa9`z\xac\x80w\x00\
\x00\x00\x00IEND\xaeB`\x82' 

def GetCollapsedIconBitmap():
    return wx.BitmapFromImage(GetCollapsedIconImage())

def GetCollapsedIconImage():
    import cStringIO
    stream = cStringIO.StringIO(GetCollapsedIconData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def GetExpandedIconData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01\x9fIDAT8\x8d\x95\x93\xa1\x8e\xdc0\x14EO\xb2\xc4\xd0\xd2\x12\xb7(mI\
\xa4%V\xd1lQT4[4-\x9a\xfe\xc1\xc2|\xc6\xc2~BY\x83:A3E\xd3\xa0*\xa4\xd2\x90H!\
\x95\x0c\r\r\x1fK\x81g\xb2\x99\x84\xb4\x0fY\xd6\xbb\xc7\xf7>=\'Iz\xc3\xbcv\
\xfbn\xb8\x9c\x15 \xe7\xf3\xc7\x0fw\xc9\xbc7\x99\x03\x0e\xfbn0\x99F+\x85R\
\x80RH\x10\x82\x08\xde\x05\x1ef\x90+\xc0\xe1\xd8\ryn\xd0Z-\\A\xb4\xd2\xf7\
\x9e\xfbwoF\xc8\x088\x1c\xbbae\xb3\xe8y&\x9a\xdf\xf5\xbd\xe7\xfem\x84\xa4\
\x97\xccYf\x16\x8d\xdb\xb2a]\xfeX\x18\xc9s\xc3\xe1\x18\xe7\x94\x12cb\xcc\xb5\
\xfa\xb1l8\xf5\x01\xe7\x84\xc7\xb2Y@\xb2\xcc0\x02\xb4\x9a\x88%\xbe\xdc\xb4\
\x9e\xb6Zs\xaa74\xadg[6\x88<\xb7]\xc6\x14\x1dL\x86\xe6\x83\xa0\x81\xba\xda\
\x10\x02x/\xd4\xd5\x06\r\x840!\x9c\x1fM\x92\xf4\x86\x9f\xbf\xfe\x0c\xd6\x9ae\
\xd6u\x8d \xf4\xf5\x165\x9b\x8f\x04\xe1\xc5\xcb\xdb$\x05\x90\xa97@\x04lQas\
\xcd*7\x14\xdb\x9aY\xcb\xb8\\\xe9E\x10|\xbc\xf2^\xb0E\x85\xc95_\x9f\n\xaa/\
\x05\x10\x81\xce\xc9\xa8\xf6><G\xd8\xed\xbbA)X\xd9\x0c\x01\x9a\xc6Q\x14\xd9h\
[\x04\xda\xd6c\xadFkE\xf0\xc2\xab\xd7\xb7\xc9\x08\x00\xf8\xf6\xbd\x1b\x8cQ\
\xd8|\xb9\x0f\xd3\x9a\x8a\xc7\x08\x00\x9f?\xdd%\xde\x07\xda\x93\xc3{\x19C\
\x8a\x9c\x03\x0b8\x17\xe8\x9d\xbf\x02.>\x13\xc0n\xff{PJ\xc5\xfdP\x11""<\xbc\
\xff\x87\xdf\xf8\xbf\xf5\x17FF\xaf\x8f\x8b\xd3\xe6K\x00\x00\x00\x00IEND\xaeB\
`\x82' 

def GetExpandedIconBitmap():
    return wx.BitmapFromImage(GetExpandedIconImage())

def GetExpandedIconImage():
    import cStringIO
    stream = cStringIO.StringIO(GetExpandedIconData())
    return wx.ImageFromStream(stream)

def create(parent):
    return vMESCenterParentFrame(parent)

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VMESCENTERPARENTFRAME, wxID_VMESCENTERPARENTFRAMEPNTOP, 
 wxID_VMESCENTERPARENTFRAMESBMAIN, wxID_VMESCENTERPARENTFRAMESLWNAV, 
 wxID_VMESCENTERPARENTFRAMESLWTOP, 
] = [wx.NewId() for _init_ctrls in range(5)]

[wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_CONFIG, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_EXIT, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_UPDATE, 
 wxID_VMESCENTERPARENTFRAMEMNFILEMN_FILE_SETTINGS, 
] = [wx.NewId() for _init_coll_mnFile_Items in range(4)]

STATUS_ONLINE_POS=0
STATUS_LOGIN_POS=1
STATUS_LOGCB_POS=2
STATUS_PROC_POS=3
STATUS_TEXT_POS=4
STATUS_LOG_POS=5
STATUS_CLK_POS=6

[wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_HELP, 
 wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_LOG, 
] = [wx.NewId() for _init_coll_mnHelp_Items in range(3)]

class vMESCenterParentFrame(wx.MDIParentFrame,vMESPluginContainer,
                vStatusBarMessageLine,vLogFallBack):
    CFG_PLUGIN_FN='vMESCfgPlugIn.ini'
    GUI_THREAD  = True
    def _init_coll_fgsMsg_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_UPDATE,
              kind=wx.ITEM_NORMAL, text=_(u'Update'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_CONFIG,
              kind=wx.ITEM_NORMAL, text=_(u'Configuration'))
        parent.Append(help='',
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMN_FILE_SETTINGS,
              kind=wx.ITEM_NORMAL, text=_(u'Search Paths'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tALT+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_exitMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_EXIT)
        self.Bind(wx.EVT_MENU, self.OnMnFileMn_file_settingsMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMN_FILE_SETTINGS)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_updateMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_UPDATE)
        self.Bind(wx.EVT_MENU, self.OnMnFileMnfile_configMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNFILEMNFILE_CONFIG)

    def _init_coll_mnBar_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnHelp, title=_(u'Help'))

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.Append(help='', id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VMESCENTERPARENTFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbMain_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(7)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')
        parent.SetStatusText(number=5, text=u'')
        parent.SetStatusText(number=6, text=u'')
        
        parent.SetStatusWidths([24, 120,24,120, -1, 100, 200])

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsMsg = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsMsg_Growables(self.fgsMsg)

        self.pnTop.SetSizer(self.fgsMsg)

    def _init_utils(self):
        # generated method, don't edit
        self.mnBar = wx.MenuBar()

        self.mnFile = wx.Menu(title='')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mnBar_Menus(self.mnBar)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.MDIParentFrame.__init__(self, id=wxID_VMESCENTERPARENTFRAME,
              name=u'vMES', parent=prnt, pos=wx.Point(114, 25),
              size=wx.Size(1008, 676),
              style=wx.DEFAULT_FRAME_STYLE | wx.VSCROLL | wx.HSCROLL,
              title=u'VIDARC MES Center')
        self._init_utils()
        self.SetClientSize(wx.Size(1000, 649))
        self.SetMenuBar(self.mnBar)
        self.Bind(wx.EVT_SIZE, self.OnVMESCenterParentFrameSize)
        self.Bind(wx.EVT_MOVE, self.OnVMESCenterParentFrameMove)
        self.Bind(wx.EVT_IDLE, self.OnMESCenterFrameIdle)

        self.slwTop = wx.SashLayoutWindow(id=wxID_VMESCENTERPARENTFRAMESLWTOP,
              name=u'slwTop', parent=self, pos=wx.Point(0, 28),
              size=wx.Size(1000, 80), style=wx.NO_BORDER | wx.SW_3D)
        self.slwTop.SetDefaultSize((1000, 60))
        self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
        self.slwTop.SetAlignment(wx.LAYOUT_TOP)
        self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
        self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,
              id=wxID_VMESCENTERPARENTFRAMESLWTOP)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VMESCENTERPARENTFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 108),
              size=wx.Size(120, 504), style=wx.NO_BORDER | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(120, 1000))
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VMESCENTERPARENTFRAMESLWNAV)
        
        self.pnTop = wx.Panel(id=wxID_VMESCENTERPARENTFRAMEPNTOP, name=u'pnTop',
              parent=self.slwTop, pos=wx.Point(0, 0), size=wx.DefaultSize,
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)
        
        self.sbMain = wx.StatusBar(id=wxID_VMESCENTERPARENTFRAMESBMAIN,
              name=u'sbMain', parent=self, style=wx.ST_SIZEGRIP)
        self._init_coll_sbMain_Fields(self.sbMain)
        self.sbMain.Bind(wx.EVT_RIGHT_UP, self.OnSbMainRightUp)
        self.SetStatusBar(self.sbMain)

        self._init_sizers()

    def __init__(self, parent,name=None):
        vLogFallBack.__init__(self)
        
        self.lang=vtLgBase.getApplLang()
        self.localDN=vSystem.getDefaultLocalDN()
        self.cfg=IniFile.Config()
        
        lDN,sLocaleDN=self.__getPlugInPaths__()
        try:
            import vidarc.tool.lang.vtLgBase as vtLgBaseLib
            vtLgBaseLib.setApplLang(vtLgBase.getApplLang())
            vtLgBaseLib.setPluginLang(None,sLocaleDN,vtLgBase.getApplLang())
            vtLgBaseLib.setPluginLang('vtXml',sLocaleDN,vtLgBase.getApplLang())
        except:
            pass
        
        self._init_ctrls(parent)
        if name is not None:
            self.SetName(name)
        wx.CallAfter(self.Bind,wx.EVT_CLOSE, 
                    self.OnVMESCenterParentFrameClose)
        vMESPluginContainer.__init__(self)
        try:
            vStatusBarMessageLine.__init__(self,self.sbMain,STATUS_TEXT_POS,-1)#5000)
        except:
            sys.stderr.write(traceback.format_exc())
        self.zMsgLast=time.clock()
        
        self.sLogin=''
        self.sPasswd=''
        self.sLibXml=None
        self.sAutoConn=None
        self.oCrypto=None
        self.bExitPending=False
        self.zExit=-1
        self.zClose=-1
        self.qClose=Queue.Queue()
        self.qLaunch=Queue.Queue(3)
        self.dClose={}
        
        col2 = wx.Colour(46, 138,87)
        col1 = wx.Colour(167, 206,184)
        
        self.Freeze()
        style = fpb.CaptionBarStyle()
        
        style.SetFirstColour(col1)
        style.SetSecondColour(col2)
        style.SetCaptionStyle(fpb.CAPTIONBAR_GRADIENT_H)
        self.fldNav = fpb.FoldPanelBar(self.slwNav, -1, wx.DefaultPosition,
                                     wx.Size(-1,-1), fpb.CAPTIONBAR_GRADIENT_H, 0)
        
        Images = wx.ImageList(16,16)
        Images.Add(GetExpandedIconBitmap())
        Images.Add(GetCollapsedIconBitmap())
        
        #self.lbbLogin = wx.StaticBitmap(bitmap=wx.EmptyBitmap(16, 16),
        #      id=-1, name=u'lbbLogin',
        #      parent=item, pos=wx.DefaultPosition, size=wx.Size(16, 16),
        #      style=0)
        #self.fldNav.AddFoldPanelWindow(item, self.lbbLogin,fpb.FPB_ALIGN_WIDTH, 5, 0)
        #self.fldNav.AddFoldPanelWindow(item, self.tgbLogin,fpb.FPB_ALIGN_WIDTH, 5, 0)
        
        item = self.fldNav.AddFoldPanel(_(u"recent"), collapsed=False,
                                      foldIcons=Images,cbstyle=style)
        self.trRecent=vMESRecentTree.vMESRecentTree(item)
        self.trRecent.SetLaunchFunc(self.doLaunchPlugInConnection)
        self.fldNav.AddFoldPanelWindow(item, self.trRecent,
                                     fpb.FPB_ALIGN_WIDTH, 5, 0) 
        item.Bind(fpb.EVT_CAPTIONBAR,self.OnFoldPanelRecentChanged)
        
        item = self.fldNav.AddFoldPanel(_(u'hyper browse'), collapsed=False,
                                      foldIcons=Images,cbstyle=style)
        self.trConn=vMESHyperBrowseTree.vMESHyperBrowseTree(item)
        self.trConn.SetLaunchFunc(self.doLaunchPlugInConnection)
        self.fldNav.AddFoldPanelWindow(item, self.trConn,
                                     fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.cbConnUpdate = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=images.getUpdateBitmap(), label=_(u'update'), name=u'cbClose',
              parent=item, pos=wx.DefaultPosition, size=wx.Size(76, 30),
              style=0)
        self.cbConnUpdate.SetMinSize(wx.Size(-1, -1))
        self.cbConnUpdate.Bind(wx.EVT_BUTTON, self.OnCbConnUpdateButton)
        self.fldNav.AddFoldPanelWindow(item, self.cbConnUpdate,
                                     fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        item = self.fldNav.AddFoldPanel(_(u"active"), collapsed=True,
                                      foldIcons=Images,cbstyle=style)
        self.lstPlugIn = wx.ListCtrl(id=-1,
              name=u'lstPlugIn', parent=item, pos=wx.DefaultPosition,
              size=wx.Size(-1, 300),
              style=wx.LC_SINGLE_SEL | wx.LC_ICON|wx.LC_SORT_ASCENDING)
        self.lstPlugIn.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'plug in'), width=80)
        self.lstPlugIn.InsertColumn(col=1, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'alias'), width=140)
        self.lstPlugIn.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
              heading=_(u'started'), width=200)
        self.lstPlugIn.Bind(wx.EVT_LEFT_DCLICK, self.OnLstPlugInLeftDclick)
        self.fldNav.AddFoldPanelWindow(item, self.lstPlugIn,
                                     fpb.FPB_ALIGN_WIDTH, 5, 0)
        
        item = self.fldNav.AddFoldPanel(_(u"Plug Ins"), collapsed=False,
                                      foldIcons=Images,cbstyle=style)
        #self.trPlugins=vMESPluginTree.vMESPluginTree(item)
        vMESPluginContainer.__setTreePlugIns__(self,vMESPluginTree.vMESPluginTree(item))
        self.trPlugins.SetLaunchFunc(self.doLaunchPlugInConnection)
        self.fldNav.AddFoldPanelWindow(item, self.trPlugins,
                                     fpb.FPB_ALIGN_WIDTH, 5, 0)
        
        item = self.fldNav.AddFoldPanel(_(u"various"), collapsed=True,
                                      foldIcons=Images,cbstyle=style)
        self.lblUsr = wx.StaticText(id=-1,
              label=_(u'login')+':____', name=u'lblUsr', parent=item,
              pos=wx.DefaultPosition, size=wx.Size(50, -1), style=0)
        self.lblUsr.SetMinSize(wx.Size(-1, -1))
        self.fldNav.AddFoldPanelWindow(item, self.lblUsr,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        self.cbClose = wx.lib.buttons.GenBitmapTextButton(ID=-1,
              bitmap=images.getExitBitmap(), label=_(u'close'), name=u'cbClose',
              parent=item, pos=wx.DefaultPosition, size=wx.Size(76, 30),
              style=0)
        self.cbClose.SetMinSize(wx.Size(-1, -1))
        self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton)
        self.fldNav.AddFoldPanelWindow(item, self.cbClose,fpb.FPB_ALIGN_WIDTH, 5, 0) 
        
        self.slwNav.SizeWindows()
        
        self.winCount=0
        self.bBlockCfgEventHandling=True
        self.sCfgFN=None
        self.docCfg=None
        self.dlgLogin=None
        self.dlgSettings=None
        self.dlgConfig=None
        self.dlgHyperBrowse=None
        self.oCryptoStore=None
        self.oCrypto=None
        self.bAdvanced=False
        self.bLoginExtended=False
        self.dMDI={}                # store MDI-child window id as key; values [plugin obj,MDI-child window,MDI number,title]
        self.dNetMasters={}         # store netMaster-id as key; values [MDI-child window id, netMasterObj]
        self.dAutoRun={}            # store child window-id as key; values [plugin obj,child window id, netMasterObj]
        icon = getApplicationIcon()
        self.SetIcon(icon)
        # setup statusbar
        
        try:
            rect = self.sbMain.GetFieldRect(STATUS_ONLINE_POS)
            self.tgbOnline = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
                  bitmap=wx.EmptyBitmap(16, 16), name=u'tgbOnline', 
                  parent=self.sbMain, 
                  pos=(rect.x+2, rect.y+2), size=(rect.width-4, rect.height-4),
                  style=0)
            self.tgbOnline.SetBitmapLabel(images.getOfflineBitmap())
            self.tgbOnline.SetBitmapSelected(images.getOnlineBitmap())
            self.tgbOnline.SetValue(True)
            #self.tgbLogin.Bind(wx.EVT_BUTTON, self.OnTgbLoginButton)
        except:
            #sys.stderr.write(traceback.format_exc())
            self.logTB()
        rect = self.sbMain.GetFieldRect(STATUS_PROC_POS)
        self.gProcess = wx.Gauge(
                    self.sbMain, -1, 50, (rect.x+2, rect.y+2), 
                    (rect.width-4, rect.height-4), 
                    wx.GA_HORIZONTAL|wx.GA_SMOOTH|wx.NO_3D|wx.NO_BORDER
                    )
        self.gProcess.SetRange(1000)
        self.gProcess.SetValue(0)
        try:
            rect = self.sbMain.GetFieldRect(STATUS_LOGCB_POS)
            self.tgbLogin = wx.lib.buttons.GenBitmapToggleButton(ID=-1,
                  bitmap=wx.EmptyBitmap(16, 16), name=u'tgbPasswd', 
                  parent=self.sbMain, 
                  pos=(rect.x+2, rect.y+2), size=(rect.width-4, rect.height-4),
                  style=0)
            self.tgbLogin.SetBitmapLabel(images.getLoggedOutBitmap())
            self.tgbLogin.SetBitmapSelected(images.getLoggedInBitmap())
            self.tgbLogin.Bind(wx.EVT_BUTTON, self.OnTgbLoginButton)
        except:
            #sys.stderr.write(traceback.format_exc())
            self.logTB()
        try:
            rect = self.sbMain.GetFieldRect(STATUS_CLK_POS)
            #self._zClk=vtDateTime(True)
            self._txtClk=wx.TextCtrl(self.sbMain,
                    pos=(rect.x+2, rect.y+2),
                    size=(rect.width-4, rect.height-4),
                    style=wx.TE_READONLY|wx.ALIGN_RIGHT|wx.NO_BORDER|wx.NO_3D)
            bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
            self._txtClk.SetBackgroundColour(bkgCol)
            #self._txtClk.Bind(wx.EVT_RIGHT_DOWN, self.OnTextClkRightDown)
            #self._txtClk.Bind(wx.EVT_MIDDLE_DOWN, self.OnTextClkMiddleDown)
            self._dlgCalendar=None
        except:
            #sys.stderr.write(traceback.format_exc())
            self.logTB()
            
        self.thdGui=vGuiThread.vGuiThread(self)
        vGuiThread.EVT_VGUI_THREAD_FINISHED(self,self.OnGuiFin)
        
        self.imgDict={}
        self.imgLstTyp=wx.ImageList(16,16)
        img=images.getModuleBitmap()
        self.imgDict['root']=self.imgLstTyp.Add(img)
        self.imgDict['mod']=self.imgLstTyp.Add(img)
        img=images.getGroupingBitmap()
        self.imgDict['grp']=self.imgLstTyp.Add(img)
        dLg={}
        for lg,bmp in [('en',images.getLangEnBitmap()),
                    ('de',images.getLangDeBitmap()),
                    ('fr',images.getLangFrBitmap()),
                    ('it',images.getLangItBitmap()),
                    ('ne',images.getLangNeBitmap()),
                    ('se',images.getLangSeBitmap()),]:
            dLg[lg]=self.imgLstTyp.Add(bmp)
        self.imgDict['lang']=dLg
        
        #self.__setCfg__()
        
        self.Thaw()
        self.__initVidarcObjs__()
        self.timer=wx.Timer(self)
        self.Bind(wx.EVT_TIMER,self.OnNotify)
        #self.timer.Start(1000)
        self.timer.Start(250)
    def OnMESCenterFrameIdle(self, event):
        if self.qLaunch.empty()==False:
            self.Notify()
            return
            for tup in self.dNetMasters.values():
                netMaster=tup[1]
                try:
                    if netMaster.IsAllSettled()==False:
                        return
                except:
                    pass
            dLaunch=self.qLaunch.get()
            self.__doLaunchPlugIn(dLaunch)
        pass
    def __clearLaunch__(self):
        while self.qLaunch.empty()==False:
            t=self.qLaunch.get()
    def OnFoldPanelRecentChanged(self,evt):
        evt.Skip()
        if evt.GetFoldStatus():
            self.__showRecent__()
    def __initVidarcObjs__(self):
        self.logInfo('')
        self.PrintMsg(_(u'creating main objects ...'),bForce=True)
        try:
            wx.BeginBusyCursor()
            self.Freeze()
            lDN,sLocaleDN=self.__getPlugInPaths__()
            try:
                import vidarc.tool.lang.vtLgBase as vtLgBaseLib
                vtLgBaseLib.setApplLang(vtLgBase.getApplLang())
                vtLgBaseLib.setPluginLang(None,sLocaleDN,vtLgBase.getApplLang())
                vtLgBaseLib.setPluginLang('vtXml',sLocaleDN,vtLgBase.getApplLang())
            except:
                pass
            try:
                import vidarc.config.vcCust as vcCust
                vcCust.USR_LOCAL_DN=self.localDN
            except:
                sys.stderr.write(traceback.format_exc())
            import vidarc.tool.time.vtTimeLoadBound as vtLdBd
            try:
                section=self.cfg.getSection('general')
                vtLdBd.vtTimeLoadBoundInit(int(section.getInfo('load_bound','75'))/100.0)
            except:
                vtLdBd.vtTimeLoadBoundInit(0.75)
            try:
                import vidarc.tool.xml.vtXmlDom as vtXmlDom
                self.docCfg=vtXmlDom.vtXmlDom(appl='vMESCenterCfg',audit_trail=False)
            except:
                self.logTB()
                self.docCfg=None
        except:
            sys.stderr.write(traceback.format_exc())
        self.Thaw()
        wx.EndBusyCursor()
        self.PrintMsg(_(u'main objects created.'),bForce=True)
    def __cleanUpVid__(self):
        if vtLogDef.VERBOSE:
            self.logInfo('')
        self.__setFB__()
        self.logStop()
        try:
            import vidarc
            vidarc.CleanUp()
        except:
            pass
        #import __init__
        #if __init__.VIDARC_IMPORT:
        #    import vidImp
        #    vidImp.vid.close()
    def __destroyVidarcObjs__(self,bFull=True):
        self.logInfo('')
        try:
            self.PrintMsg(_(u'destroying main objects ...'),bForce=True)
            try:
                if self.dlgHyperBrowse is not None:
                    self.dlgHyperBrowse.CloseConnection()
            except:
                #sys.stderr.write(traceback.format_exc())
                self.logTB()
            self.OnCbCloseButton(None)
            self.trRecent.Clear()
            self.trConn.Clear()
            self.__clrPlugIns__(bFull=bFull)
            
            self.oCrypto=None
            if self.oCryptoStore is not None:
                self.oCryptoStore.Save()
                self.oCryptoStore.Close()
            self.oCryptoStore=None
            if self.docCfg is None:
                self.__cleanUpVid__()
                return
            self.docCfg.Save()
            self.docCfg.Close()
            self.docCfg=None
        except:
            self.logTB()
        self.__cleanUpVid__()
        self.PrintMsg(_(u'main objects destroyed.'),bForce=True)
    def OnTgbLoginButton(self,evt):
        evt.Skip()
        if self.tgbLogin.GetValue()==False:
            self.trRecent.Clear()
            self.trConn.Clear()
            wx.CallAfter(self.SetLogin,'','',None,None,False)
        else:
            wx.CallAfter(self.doLogin)
    def doLogin(self):
        self.logInfo('')
        try:
            if self.dlgLogin is None:
                self.dlgLogin=vLoginDialog(self)
            self.dlgLogin.Centre()
            self.dlgLogin.SetType(0)
            self.dlgLogin.Clear()
            iRet=self.dlgLogin.ShowModal()
            if iRet>0:
                sLogin=self.dlgLogin.GetLogin()
                sPasswd=self.dlgLogin.GetPasswd()
                oCrypto,bRet=vMESCenterTools.buildCrypto('vMESCenter',sLogin,sPasswd,None,self.localDN)
                if bRet:
                    oCryptoStore=vMESCenterTools.buildLoginStore('vMESCenter',sLogin,sPasswd,self.localDN)
                    if iRet==2:
                        self.dlgLogin.SetType(1)
                        iRet=self.dlgLogin.ShowModal()
                        if iRet==3:
                            # change passwd
                            sPasswd=self.dlgLogin.GetPasswd()
                            if oCrypto.ExportPrivKey(''.join(['vMESCenter','_',sLogin,'.priv']),self.localDN,phrase=sPasswd)==False:
                                dlg=vMsgDialog(self,_(u'Error exporting logon infos.') ,
                                            sApplName,
                                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                                dlg.ShowModal()
                                dlg.Destroy()
                                wx.CallAfter(self.doLogin)
                            else:
                                iRet=oCryptoStore.ImportPrivKey(''.join(['vMESCenter','_',sLogin,'.priv']),self.localDN,phrase=sPasswd)
                    self.SetLogin(sLogin,sPasswd,oCrypto,oCryptoStore,False)
                    self.__showRecent__()
                else:
                    wx.CallAfter(self.doLogin)
            else:
                wx.CallAfter(self.SetLogin,'','',None,None,False)
        except:
            self.logTB()
    def SetLogin(self,sLogin,sPasswd,oCrypto,oCryptoStore,bCheckPlugin=True):
        self.logInfo('bCheckPlugin:%d'%bCheckPlugin)
        try:
            self.logInfo('sLogin:%s'%(sLogin))
            self.cfg.readFN(self.CFG_PLUGIN_FN)
            self.__setCfg__()
            self.sLogin=sLogin
            if len(self.sLogin)==0:
                st=':'.join([_(u'login'),'____'])
                self.lblUsr.SetLabel(st)
                self.SetStatusText(st,STATUS_LOGIN_POS)
                #self.tgbLogin.SetLabel(_(u'login'))
                self.tgbLogin.SetValue(False)
            else:
                st=':'.join([_(u'login'),self.sLogin])
                self.lblUsr.SetLabel(st)
                self.SetStatusText(st,STATUS_LOGIN_POS)
                #self.tgbLogin.SetLabel(_(u'logout'))
                self.tgbLogin.SetValue(True)
            self.sPasswd=sPasswd
            #if len(self.sPasswd)==0:
            #    self.lbbLogin.SetBitmap(images.getSecUnlockedBitmap())
            #else:
            #    self.lbbLogin.SetBitmap(images.getSecLockedBitmap())
            
            self.oCrypto=oCrypto
            self.oCryptoStore=oCryptoStore
            bAutoUpdate=True
            if bCheckPlugin==False:
                bAutoUpdate=False
            if bAutoUpdate:
                self.__updatePlugIn__(bShow=False)
            else:
                if self.oCryptoStore is not None:
                    iLease=self.oCryptoStore.GetLease2Expire()
                    import __init__
                    if __init__.VIDARC_IMPORT==0:
                        iLease=31
                    if iLease<7:
                        if iLease>0:
                            dlg = vMsgDialog(self,
                                    _(u'VIDARC software lease for this host will expire in %s days.')%iLease+'\n\n'+_('Please connect to server as soon as possible.'),
                                    _(u'vMES Center')+u' '+_(u'License Expire'),
                                    wx.OK  | wx.ICON_INFORMATION)
                        else:
                            dlg = vMsgDialog(self,
                                    _(u'VIDARC software lease for this host has expired.')+'\n\n'+\
                                    _('Please connect to server as soon as possible.'),
                                    _(u'vMES Center')+u' '+_(u'License Expired'),
                                    wx.OK  | wx.ICON_INFORMATION)
                        dlg.ShowModal()
                        dlg.Destroy()
                        if iLease<0:
                            self.cfg.readFN(self.CFG_PLUGIN_FN)
                            self.OnMnFileMnfile_updateMenu(None)
                            return
                        self.__checkLease__()
                #self.OnMnFileMnfile_updateMenu(None)
                #self.__initVidarcObjs__()
                if bCheckPlugin:
                    self.logInfo('cwd:%s'%(os.getcwd()))
                    if self.GUI_THREAD:
                        self.logDebug('__check4Plugins__ scheduled')
                        self.thdGui.Do('__check4Plugins__',self.__check4PlugIns__)
                    else:
                        self.__check4PlugIns__()
                
                self.trRecent.SetLoginStorage(self.oCryptoStore)
        except:
            self.logTB()
    def __addPlugIn__(self,plugin):
        try:
            self.trRecent.AddPlugIn(plugin)
            self.trConn.AddPlugIn(plugin)
            name=plugin.GetName()
            bmp=plugin.GetBitmap()
            if bmp is not None:
                self.imgDict[name]=self.imgLstTyp.Add(bmp)
            else:
                #print 'no bmp',evt.GetName(),evt.IsFrame(),evt.GetDN()
                return
        except:
            self.logTB()
    def OnPlugInFin(self,evt):
        vMESPluginContainer.OnPlugInFin(self,evt)
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            wx.CallAfter(self.__showRecent__)
            wx.CallAfter(self.OnCbConnUpdateButton,None)
        except:
            self.logTB()
    def __updateVidObjs__(self):
        if vtLogDef.VERBOSE:self.logInfo('')
        self.Enable(False)
        try:
            self.PrintMsg(_(u'creating main objects ...'),bForce=True)
            if self.docCfg is None:
                try:
                    self.doImport()
                    import __init__
                    #sys.stderr.write('\n__updateVidObjs__'+str(__init__.VIDARC_IMPORT)+'\n')
                    if __init__.VIDARC_IMPORT:
                        try:
                            import vidImp
                            vidImp.vid.SetBlock(False)
                            dn=vIdentify.getCfg('baseDN',machine=False)
                            vidImp.vid.SetDN(dn)
                            #reload(vidImp)
                        except:
                            sys.stderr.write(traceback.format_exc())
                    self.__initVidarcObjs__()
                    if 0:
                        lDN,sLocaleDN=self.__getPlugInPaths__()
                        try:
                            import vidarc.tool.lang.vtLgBase as vtLgBaseLib
                            vtLgBaseLib.setApplLang(vtLgBase.getApplLang())
                            vtLgBaseLib.setPluginLang(None,sLocaleDN,self.lang)
                            vtLgBaseLib.setPluginLang('vtXml',sLocaleDN,self.lang)
                        except:
                            pass
                        
                        import vidarc.tool.time.vtTimeLoadBound as vtLdBd
                        vtLdBd.vtTimeLoadBoundInit(0.75)
                        import vidarc.tool.InOut.fnUtil as fnUtil
                        import vidarc.tool.xml.vtXmlDom as vtXmlDom
                        self.docCfg=vtXmlDom.vtXmlDom(appl='vMESCenterCfg',audit_trail=False)
                    self.OpenCfgFile(self.sCfgFN)
                except:
                    self.logTB()
            else:
                self.Enable(True)
                return False
            self.PrintMsg(_(u'setup security'),bForce=True)
            dlgWait=wx.BusyInfo(_(u'Please wait, while setting up security ...'))
            #self.Freeze()
            if self.oCrypto is None:
                self.oCrypto,bRet=vMESCenterTools.buildCrypto('vMESCenter',self.sLogin,self.sPasswd,self.oCrypto,self.localDN)
                if bRet==False:
                    self.__showLicenseError__()
            #sys.stderr.write('--> updateMenu 011'+'\n')
            self.PrintMsg(_(u'reading secure login ...'),bForce=True)
            if self.oCryptoStore is None:
                self.oCryptoStore=vMESCenterTools.buildLoginStore('vMESCenter',self.sLogin,self.sPasswd,self.localDN)
                if self.oCryptoStore is None:
                    self.__showLicenseError__()
                #else:
                    #sys.stderr.write('--> updateMenu 015'+'\n')
                #    self.oCryptoStore.SetLease(self.dlgUpdate.GetLease())
                #    sHost,sPort,sUser=self.dlgUpdate.GetConnection()
                #    self.oCryptoStore.SetPlugInConnection(sHost,sPort,sUser)
                #    self.oCryptoStore.SetLastOnline()
                #sys.stderr.write('--> updateMenu 015'+'\n')
                if self.dlgUpdate is not None:
                    if self.__bUpdated__:
                        try:
                            self.oCryptoStore.SetLease(self.dlgUpdate.GetLease())
                            sHost,sPort,sUser,sPasswd=self.dlgUpdate.GetConnection()
                            self.oCryptoStore.SetPlugInConnection(sHost,sPort,sUser,sPasswd)
                            self.oCryptoStore.SetLastOnline()
                        except:
                            self.logTB()
                self.__checkLease__()
                self.trRecent.SetLoginStorage(self.oCryptoStore)
                #sys.stderr.write('--> updateMenu 016'+'\n')
            dlgWait.Destroy()
        except:
            self.logTB()
        #self.Thaw()
        self.Enable(True)
        self.PrintMsg(_(u'main objects created.'),bForce=True)
        return True
    def __checkLease__(self):
        if self.oCryptoStore is None:
            iLease=-1
        else:
            iLease=self.oCryptoStore.GetLease2Expire()
        self.logInfo('%d'%iLease)
        if iLease<0:
            self.__destroyVidarcObjs__(bFull=False)
            try:
                try:
                    import __init__
                    if __init__.VIDARC_IMPORT:
                        import vidImp
                        vidImp.vid.SetBlock(True)
                        vidImp.vid.close()
                except:
                    sys.stderr.write(traceback.format_exc())
            except:
                self.logTB()
    def __getPluginNameLst__(self,ti):
        l=[]
        while ti is not None and ti != self.trPlugins.GetRootItem():
            l.append(self.trPlugins.GetItemText(ti))
            ti=self.trPlugins.GetItemParent(ti)
        l.reverse()
        return l
    def __getPluginCfg__(self,l):
        self.logInfo('')
        try:
            if self.docCfg is None:
                self.logError('config doc is None')
                return {}
            d={}
            self.docCfg.acquire('dom')
            try:
                cfgNode=self.docCfg.getChildByLstForced(None,l)
                if cfgNode is not None:
                    #for c in self.docCfg.getChilds(cfgNode):
                    #    d[self.docCfg.getTagName(c)]=self.docCfg.getText(c)
                    def __getCfgData__(node,dd):
                        for c in self.docCfg.getChilds(node):
                            if self.docCfg.hasChilds(c):
                                ddd={}
                                __getCfgData__(c,ddd)
                                dd[self.docCfg.getTagName(c)]=ddd
                            else:
                                dd[self.docCfg.getTagName(c)]=self.docCfg.getText(c)
                    __getCfgData__(cfgNode,d)
            except:
                self.logTB()
            self.docCfg.release('dom')
            if self.isLoggedDbg():
                self.logDebug('l:%s;dCfg:%s'%(`l`,`d`))
            return d
        except:
            self.logTB()
            return {}
    def __setPluginCfg__(self,l,d):
        self.logInfo('')
        try:
            if self.isLoggedDbg():
                self.logDebug('l:%s;dCfg:%s'%(`l`,`d`))
            if self.docCfg is None:
                self.logError('config doc is None')
                return 0
            import vidarc.tool.InOut.fnUtil as fnUtil
            self.docCfg.acquire('dom')
            try:
                cfgNode=self.docCfg.getChildByLstForced(None,l)
                for c in self.docCfg.getChilds(cfgNode):
                    self.docCfg.deleteNode(c,cfgNode)
                #keys=d.keys()
                #keys.sort()
                #for k in keys:
                #    kr=fnUtil.replaceSuspectChars(k)
                #    self.docCfg.setNodeText(cfgNode,kr,unicode(d[k]))
                def __setCfgData__(cfgNode,d):
                    keys=d.keys()
                    keys.sort()
                    for k in keys:
                        kr=fnUtil.replaceSuspectChars(k)
                        if type(d[k])==types.DictionaryType:
                            cc=self.docCfg.getChildForced(cfgNode,kr)
                            __setCfgData__(cc,d[k])
                        else:
                            self.docCfg.setNodeText(cfgNode,kr,unicode(d[k]))
                __setCfgData__(cfgNode,d)
                self.docCfg.AlignNode(cfgNode)
            except:
                self.logTB()
            self.docCfg.release('dom')
            self.docCfg.Save()
            return 0
        except:
            self.logTB()
            return -1
    def __addRecentDict__(self,tip,d,dRecent):
        self.logError('you are not supposed to be here.')
        keys=d.keys()
        keys.sort()
        for k in keys:
            tup=d[k]
            if k not in dRecent:
                continue
            plugin=self.trPlugins.GetPyData(tup[0])
            if plugin is None:
                continue
            sName=plugin.GetName()
            if sName in self.imgDict:
                img=self.imgDict[sName]
                ti=self.trRecent.AppendItem(tip,k,img,img,None)
                l=dRecent[k]
                bFirst=True
                for sConn in l:
                    i=sConn.find(':')
                    j=sConn.rfind('@')
                    if i>0 and j>0:
                        if self.bAdvanced:
                            s=sConn
                        else:
                            s=sConn[j+1:]+' '+sConn[:i]
                        tic=self.trRecent.AppendItem(ti,s,-1,-1,None)
                        tid=wx.TreeItemData()
                        tid.SetData((k,plugin,tup[0],sConn,sConn[j+1:],sConn[:i]))
                        self.trRecent.SetItemData(tic,tid)
                        if bFirst:
                            bFirst=None
                            tid=wx.TreeItemData()
                            tid.SetData((k,plugin,tup[0],sConn,sConn[j+1:],sConn[:i]))
                            self.trRecent.SetItemData(ti,tid)
    def __showRecent__(self):
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            #self.__clearLaunch__()
            #vtLog.vtLngCurWX(vtLog.DEBUG,'dPlugIn:%s'%(vtLog.pformat(self.dPlugIn)),self)
            #tp=self.trRecent.GetRootItem()
            #self.trRecent.DeleteChildren(tp)
            self.trRecent.Clear()
            if self.oCryptoStore is not None:
                if self.oCryptoStore.acquire('check',blocking=False):
                    try:
                        dRecent=copy.deepcopy(self.oCryptoStore.GetRecentDict(4))
                    except:
                        pass
                    self.oCryptoStore.release('check')
                else:
                    return
            else:
                return
            self.trRecent.SetRecent(self.dPlugIn,dRecent)
            self.trRecent.SetLoginStorage(self.oCryptoStore)
            #self.__addRecentDict__(tp,self.dPlugIn,dRecent)
            #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
            #vtLog.vtLngCurWX(vtLog.DEBUG,'dPlugIn:%s fin'%(vtLog.pformat(self.dPlugIn)),self)
        except:
            self.logTB()
    def __doLaunchPlugIn(self,dLaunch):
        if vtLogDef.VERBOSE or 1:
            self.logInfo('')
            self.logDebug('%s'%(`dLaunch`))
        try:
            tid=dLaunch['plugIn']
            lang=dLaunch['lang']
            tid.setLang(lang)
            if len(self.dMDI)>16:
                self.PrintMsg(_('maximum concurrent plugins reached. launch plugin %s is not permitted.')%(tid.GetName()))
                return
            if tid.IsFrame()==1:
                if len(self.dNetMasters)>12:
                    self.PrintMsg(_('maximum concurrent plugins reached. launch plugin %s is not permitted.')%(tid.GetName()))
                    return
                self.PrintMsg(_('launch plugin %s ...')%(tid.GetName()))
                l=dLaunch['lstNames']
                dCfg=self.__getPluginCfg__(l)
                
                sz=(800,300)
                wx.BeginBusyCursor()
                #self.Freeze()
                #wx.YieldIfNeeded()
                win=tid.createFrame(self, -1, wx.DefaultPosition , sz, 0 , "%s: %d" % (tid.GetWinName(),self.winCount))
                if win is None:
                    self.PrintMsg(_('plugin %s launch fault.')%(tid.GetName()))
                    wx.EndBusyCursor()
                    #self.Thaw()
                    return
                strs=tid.GetName().split('.')
                netMaster=win.GetNetMaster()
                if netMaster is None:
                    self.PrintMsg(_('plugin %s launch fault.')%(tid.GetName()))
                    win.Destroy()
                    wx.EndBusyCursor()
                    #self.Thaw()
                    return
                self.logDebug('localDN:%s'%`self.localDN`)
                if netMaster==False:
                    bSkipNetMaster=True
                else:
                    netMaster.SetLoginStorage(self.oCryptoStore)
                    netMaster.SetLocalDN(self.localDN)
                    bSkipNetMaster=False
                    try:
                        EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED(netMaster, self.OnMDINetMasterAliasChanged)
                    except:
                        try:
                            from vidarc.tool.net.vtNetSecXmlGuiMaster import EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED
                            EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED(netMaster, self.OnMDINetMasterAliasChanged)
                        except:
                            bSkipNetMaster=True
                if bSkipNetMaster==False:
                    self.dNetMasters[netMaster.GetId()]=[win.GetId(),netMaster]
                #win.Show(True)
                try:
                    if self.isLoggedDbg():
                        self.logDebug('winId:%d;dMDI:%s;netId:%d;dNetMasters:%s'%\
                            (win.GetId(),`self.dMDI`,
                            netMaster.GetId(),`self.dNetMasters`))
                except:
                    pass
                try:
                    if self.isLoggedDbg():
                        self.logDebug('l:%s;dCfg:%s'%(`l`,`dCfg`))
                    win.SetCfgData(self.__setPluginCfg__,l,dCfg)
                except:
                    self.logTB()
                #if self.oCryptoStore.acquire('check',blocking=False):
                if 'connection' in dLaunch:
                    try:
                        sConnCpy=dLaunch['connection']
                        win.DoAutoConnect(sConnCpy,not self.tgbOnline.GetValue())
                        #wx.CallAfter(win.DoAutoConnect,sConnCpy)
                    except:
                        pass
                idx=self.lstPlugIn.InsertImageStringItem(sys.maxint,'%s:%d'%(strs[2],self.winCount),self.imgDict[tid.GetName()])
                self.lstPlugIn.SetItemData(idx,win.GetId())
                
                win.Show(True)
                self.dMDI[win.GetId()]=[tid,win,self.winCount,strs[2]]
                self.PrintMsg(_('plugin %s launched.')%(tid.GetName()))
                #wx.LayoutAlgorithm().LayoutMDIFrame(self)
                #self.GetClientWindow().Refresh()
                #wx.EndBusyCursor()
                #self.Thaw()
            elif tid.IsFrame()>100:
                # auto run
                self.PrintMsg(_('launch plugin %s ...')%(tid.GetName()))
                l=dLaunch['lstNames']
                dCfg=self.__getPluginCfg__(l)
                
                wx.BeginBusyCursor()
                #self.Freeze()
                #wx.YieldIfNeeded()
                sz=(800,300)
                win=tid.create(self.pnTop, -1, wx.DefaultPosition , wx.DefaultSize, 0 , '.'.join(l) )
                if win is None:
                    self.PrintMsg(_('plugin %s launch fault.')%(tid.GetName()))
                    wx.EndBusyCursor()
                    #self.Thaw()
                    return
                strs=tid.GetName().split('.')
                netMaster=win.GetNetMaster()
                if netMaster is None:
                    self.PrintMsg(_('plugin %s launch fault.')%(tid.GetName()))
                    win.Destroy()
                    wx.EndBusyCursor()
                    #self.Thaw()
                    return
                netMaster.SetLoginStorage(self.oCryptoStore)
                #netMaster.SetLocalDN(self.localDN)
                try:
                    win.SetCfgData(self.__setPluginCfg__,l,dCfg)
                except:
                    self.logTB()
                #if self.oCryptoStore.acquire('check',blocking=False):
                if 'connection' in dLaunch:
                    try:
                        sConnCpy=dLaunch['connection']
                        win.DoAutoConnect(sConnCpy,not self.tgbOnline.GetValue())
                        #wx.CallAfter(win.DoAutoConnect,sConnCpy)
                    except:
                        self.logTB()
                #idx=self.lstPlugIn.InsertImageStringItem(sys.maxint,'%s:%d'%(strs[2],self.winCount),self.imgDict[tid.GetName()])
                #self.lstPlugIn.SetItemData(idx,win.GetId())
                self.fgsMsg.AddWindow(win,1, border=0, flag=wx.EXPAND | wx.LEFT)
                self.fgsMsg.Layout()
                win.BindEvents([
                        (None,[
                            ['navigate',self.OnMsgNavGoTo]])])
                
                #win.Show(True)
                self.dAutoRun[win.GetId()]=[tid,win,self.winCount,strs[2]]
                self.PrintMsg(_('plugin %s launched.')%(tid.GetName()))
                win.Bind(wx.EVT_CLOSE, self.OnAutoRunClose)
                wx.EndBusyCursor()
                #self.Thaw()
                return
            else:
                wx.BeginBusyCursor()
                #self.Freeze()
                #wx.YieldIfNeeded()
                win = wx.MDIChildFrame(self, -1, "%s: %d" % (tid.GetWinName(),self.winCount))
                plugin=tid.create(win,-1,(0,0),wx.DefaultSize,wx.TAB_TRAVERSAL, 'child')
                l=dLaunch['lstNames']
                dCfg=self.__getPluginCfg__(l)
                #self.dMDI[win.GetId()]=[tid,plugin,self.winCount,'']
                #win.SetClientSize(plugin.GetSize())
                #win.Show(True)
                #wx.LayoutAlgorithm().LayoutMDIFrame(self)
                #self.GetClientWindow().Refresh()
                try:
                    plugin.SetCfgData(self.__setPluginCfg__,l,dCfg)
                except:
                    self.logTB()
                self.dMDI[win.GetId()]=[tid,plugin,self.winCount,'']
                win.Show(True)
                #wx.EndBusyCursor()
                #self.Thaw()
                #return
            #win.Bind(wx.EVT_SIZE, self.OnMDISize)
            #win.Bind(wx.EVT_MOVE, self.OnMDIMove)
            win.Bind(wx.EVT_CLOSE, self.OnMDIClose)
        except:
            self.logTB()
            try:
                self.PrintMsg(_('plugin %s launch fault.')%(tid.GetName()))
            except:
                self.PrintMsg(_('plugin launch fault.'))
            if win is not None:
                try:
                    win.Close()
                except:
                    self.logTB()
        try:
            wx.EndBusyCursor()
        except:
            self.logTB()
        #try:
        #    self.Thaw()
        #except:
        #    self.logTB()
    
    def doLaunchPlugInConnection(self,dLaunch,oPlugIn):
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            if 'lang' not in dLaunch:
                dLaunch['lang']=self.lang
            self.qLaunch.put(dLaunch,False)
            self.PrintMsg(_('schedule plugin %s for launch.')%(oPlugIn.GetName()))
        except:
            self.PrintMsg(_('schedule plugin %s not possible yet.')%(oPlugIn.GetName()))
            self.logTB()
    def OnCbConnUpdateButton(self,evt):
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            if self.tgbOnline.GetValue()==False:
                self.PrintMsg(_('Connection is offline.'))
                dlg = vMsgDialog(self,
                        _(u"Connection is offline."),
                        _(u'vMES Center')+u' '+_(u'Browse'),
                        wx.OK | wx.ICON_INFORMATION
                        )
                dlg.ShowModal()
                dlg.Destroy()
                return
            if self.tgbLogin.GetValue()==False:
                self.PrintMsg(_('Not loggedin.'))
                dlg = vMsgDialog(self,
                        _(u"Connection is offline."),
                        _(u'vMES Center')+u' '+_(u'Browse'),
                        wx.OK | wx.ICON_INFORMATION
                        )
                dlg.ShowModal()
                dlg.Destroy()
                return
            try:
                sec=self.cfg.getSection('server')
                sHost=sec.getInfo('data_host','vidarc')
                sPort=sec.getInfo('data_port','50000')
            except:
                #sys.stderr.write(traceback.format_exc())
                self.logTB()
                return 
            if self.dlgHyperBrowse is None:
                self.dlgHyperBrowse=vMESHyperBrowseDialog(self)
            self.dlgHyperBrowse.SetConnection(sHost,sPort,self.sLogin)
            if vtLogDef.VERBOSE:
                self.logDebug('host:%s;port:%s;login:%s evt==%d\n'%(sHost,sPort,self.sLogin,evt is not None))
                #sys.stderr.write('       host:%s;port:%s;login:%s evt==%d\n'%(sHost,sPort,self.sLogin,evt is not None))
            if evt is None:
                self.dlgHyperBrowse.SetModeAuto(True,self.__showHyperBrowse__)
            else:
                self.dlgHyperBrowse.SetModeAuto(False,None)
                if self.dlgHyperBrowse.ShowModal()>0:
                    self.__showHyperBrowse__()
        except:
            self.logTB()
    def __showHyperBrowse__(self):
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            section=self.cfg.getSection('hyper_browse')
            sGrpSep=section.getInfo('separator','_')
            lGrpLv=[int(section.getInfo('char_level0','8')),
                    int(section.getInfo('char_level1','8')),
                    int(section.getInfo('char_level2','8'))]
        except:
            sGrpSep='_'
            lGrpLv=[3,3,3]
        try:
            sec=self.cfg.getSection('server')
            sHost=sec.getInfo('data_host')
            sPort=sec.getInfo('data_port')
                
            lConn=self.dlgHyperBrowse.GetConnectionLst()
            self.trConn.ShowHyperBrowse(self.dPlugIn,lConn,lGrpLv,sGrpSep,
                    sHost,sPort,self.sLogin)
        except:
            self.logTB()
    def OnMDISize(self,evt):
        evt.Skip()
    def OnMDIMove(self,evt):
        evt.Skip()
    def OnMDIClose(self,evt):
        evt.Skip()
        win=evt.GetEventObject()
        id=win.GetId()
        if vtLogDef.VERBOSE:self.logInfo('')
        self.__doMDIClose__(id)     #070702:wro
    def OnAutoRunClose(self,evt):
        evt.Skip()
        win=evt.GetEventObject()
        id=win.GetId()
        if vtLogDef.VERBOSE:self.logInfo('')
        self.__doAutoRunClose__(id)     #070702:wro
    def __doAutoRunClose__(self,id):
        if vtLogDef.VERBOSE:self.logInfo('')
        if id not in self.dAutoRun:
            self.logInfo('id:%d;already removed;%s'%(id,`self.dMDI`))
            return
        try:
            if vtLogDef.VERBOSE:self.logDebug('id:%d;%s'%(id,`self.dMDI`))
            tup=self.dAutoRun[id]
            plgCls,plugin=tup[0],tup[1]
            del self.dAutoRun[id]
            
            self.fgsMsg.Remove(plugin)
            self.fgsMsg.Layout()
            plugin.Show(False)
            #plugin.Destroy()
            #del self.dClose[id]     #070702:wro
        except:
            self.logTB()
        try:
            if vtLogDef.VERBOSE:self.logDebug('id:%d;%s;%s'%(id,`self.dMDI`,`self.dNetMasters`))
        except:
            self.logTB()
    def __doMDIClose__(self,id):
        if vtLogDef.VERBOSE:self.logInfo('')
        if id not in self.dMDI:
            self.logInfo('id:%d;already remove;%s'%(id,`self.dMDI`))
            return
        try:
            if vtLogDef.VERBOSE:self.logDebug('id:%d;%s'%(id,`self.dMDI`))
            for k,tup in  self.dNetMasters.items():
                if tup[0]==id:
                    iCount=self.lstPlugIn.GetItemCount()
                    for idx in xrange(iCount):
                        idWin=self.lstPlugIn.GetItemData(idx)
                        if idWin==tup[0]:
                            self.lstPlugIn.DeleteItem(idx)
                            break
                    #try:
                    #    EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED_DISCONNECT(tup[1], self.OnMDINetMasterAliasChanged)
                    #except:
                    #    from vidarc.tool.net.vtNetSecXmlGuiMaster import EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED_DISCONNECT
                    #    EVT_NET_SEC_XML_MASTER_ALIAS_CHANGED_DISCONNECT(tup[1], self.OnMDINetMasterAliasChanged)
                    del self.dNetMasters[k]
                    break
        except:
            self.logTB()
        try:
            tup=self.dMDI[id]
            plgCls,plugin=tup[0],tup[1]
            #plugin.Destroy()
            del self.dMDI[id]
            #del self.dClose[id]     #070702:wro
        except:
            #del self.dMDI[id]     #070702:wro
            #del self.dClose[id]     #070702:wro
            self.logTB()
        try:
            if vtLogDef.VERBOSE:self.logDebug('id:%d;%s;%s'%(id,`self.dMDI`,`self.dNetMasters`))
        except:
            self.logTB()
    def OnLstPlugInLeftDclick(self, event):
        event.Skip()
        try:
            iCount=self.lstPlugIn.GetItemCount()
            for idx in xrange(iCount):
                if self.lstPlugIn.GetItemState(idx,wx.LIST_STATE_SELECTED)!=0:
                    idWin=self.lstPlugIn.GetItemData(idx)
                    tup=self.dMDI[idWin]
                    plgCls,plugin=tup[0],tup[1]
                    plugin.SetFocus()
                    break
        except:
            pass
    def __checkRdy2Exit__(self):
        try:
            bExit=False
            iLenNet=len(self.dNetMasters.keys())
            iLenMdi=len(self.dMDI.keys())
            iLenAutoRun=len(self.dAutoRun.keys())
            if iLenNet==0:
                if iLenMdi==0:
                    if iLenAutoRun==0:
                        bExit=True
            def checkTimeOut(zTimeOut,bExit):
                if zTimeOut<0:
                    self.logInfo('iNet:%d;iMDI:%d;iAut:%d;bExit:%d;zTimeOut:%d'%(
                                iLenNet,iLenMdi,iLenAutoRun,bExit,zTimeOut))
                    try:
                        self.logInfo('net:%s'%repr(self.dNetMasters))
                        self.logInfo('mdi:%s'%repr(self.dMDI))
                        self.logInfo('autorun:%s'%repr(self.dAutoRun))
                    except:
                        self.logTB()
                    self.dNetMasters={}
                    self.dMDI={}
                    self.dAutoRun={}
                    return True
                else:
                    return bExit
            if self.zExit>0:
                zTimeOut=int((self.zExit+120)-time.clock())
                bExit=checkTimeOut(zTimeOut,bExit)
            else:
                #zTimeOut=-1
                if self.zClose>0:
                    zTimeOut=int((self.zClose+120)-time.clock())
                    bExit=checkTimeOut(zTimeOut,bExit)
                    if bExit:
                        self.zClose=-1
                else:
                    self.zClose=time.clock()
                    zTimeOut=-1
            #self.logInfo('iNet:%d;iMDI:%d;iAut:%d;bExit:%d;zTimeOut:%d'%(
            #            iLenNet,iLenMdi,iLenAutoRun,bExit,zTimeOut))
            if bExit==False:
                if zTimeOut>=0:
                    self.PrintMsg(_(u'exit pending ...')+' '+
                        _(u'open connection:%d open plugins:%d (timeout in %d[s])')%
                        (iLenNet,iLenMdi,zTimeOut))
                else:
                    self.PrintMsg(_(u'exit pending ...')+' '+
                        _(u'open connection:%d open plugins:%d')%(iLenNet,iLenMdi))
            return bExit
        except:
            self.logTB()
            return True
    def OnMDINetMasterAliasChanged(self,evt):
        self.logInfo('')
        try:
            appl=evt.GetAppl()
            alias=evt.GetAlias()
            host=evt.GetHost()
            self.logInfo('appl:%s;alias:%s;host:%s'%(appl,alias,host))
            idNetMaster=evt.GetId()
            if idNetMaster not in self.dNetMasters:
                self.logError('netmaster id:%s not found;dNetMasters:%s'%(idNetMaster,`self.dNetMasters`))
                return
            self.logDebug('netmaster id:%s found;dNetMasters:%s'%(idNetMaster,`self.dNetMasters`))
            tup=self.dNetMasters[idNetMaster]
            if tup[1].GetMainAppl()!=appl:
                # i don't care about sub appls
                return
            idWin=tup[0]
            if idWin not in self.dMDI:
                self.logError('mdi id:%s not found;dMDI:%s'%(idWin,`self.dMDI`))
                return
            self.logDebug('mdi id:%s found;dMDI:%s'%(idWin,`self.dMDI`))
            iCount=self.lstPlugIn.GetItemCount()
            for idx in xrange(iCount):
                id=self.lstPlugIn.GetItemData(idx)
                if id==idWin:
                    tupMDI=self.dMDI[idWin]
                    sTitle=' '.join([alias,host,':%d'%tupMDI[2]])
                    self.lstPlugIn.SetItemText(idx,sTitle)
                    break
        except:
            self.logTB()
    def OnMsgNavGoTo(self,evt):
        self.logInfo('')
        try:
            appl,alias,fid=evt.GetInfos()
            for k,tup in self.dNetMasters.items():
                netMaster=tup[1]
                netDoc=netMaster.GetMainDoc()
                if netDoc.GetAppl()!=appl:
                    continue
                if netDoc.GetAlias()==alias:
                    netDoc.SelectById(fid)
                    try:
                        winId=tup[0]
                        if winId in self.dMDI:
                            tupWin=self.dMDI[winId]
                            tupWin[1].Activate()#=[tid,win,self.winCount,strs[2]]
                    except:
                        self.logTB()
                    return
            if appl in self.dPlugIn:
                #self.trPlugins.SelectItem(self.dPlugIn[appl][0])
                self.sAutoConn=None
                if self.oCryptoStore is not None:
                    if self.oCryptoStore.acquire('check',blocking=False):
                        try:
                            self.sAutoConn=self.oCryptoStore.FindConnection(appl,alias,self.sLogin)
                        except:
                            pass
                        self.oCryptoStore.release('check')
                if self.sAutoConn is None:
                    dlg = vMsgDialog(self,
                            _(u"Connection to %s:%s couldn't be resolved.")%(appl,alias),
                            _(u'vMES Center')+u' '+_(u'Navigation'),
                            wx.OK | wx.ICON_INFORMATION
                            #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                            )
                    dlg.ShowModal()
                    dlg.Destroy()
                ti=self.dPlugIn[appl][0]
                tid=self.trPlugins.GetPyData(self.dPlugIn[appl][0])
                if tid is not None:
                    lang=self.lang
                    if VERBOSE_LANG:
                        sys.stderr.write(lang+'\n')
                    if type(tid)==types.TupleType:
                        ti=self.trPlugins.GetItemParent(ti)
                        tid,lang=tid
                    #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
                    self.winCount+=1
                    
                    oPlugIn=self.dPlugIn[appl][2]
                    l=oPlugIn.GetDomainNameLst()
                    dLaunch={'plugIn':tid,'lang':lang}
                    dLaunch['lstNames']=l
                    if self.sAutoConn is not None:
                        dLaunch['connection']=copy.deepcopy(self.sAutoConn)
                    try:
                        self.qLaunch.put(dLaunch,False)
                        self.PrintMsg(_('schedule plugin %s for launch.')%(tid.GetName()))
                    except:
                        self.PrintMsg(_('schedule plugin %s not possible yet.')%(tid.GetName()))
                    self.Notify()
                #self.OnTrPluginsLeftDclick(None)
                #self.sAutoConn=None
        except:
            self.logTB()
    def OnMnFileMnfile_exitMenu(self, event):
        #self.OnCbCloseButton(None)
        self.Close()
        #if self.__checkRdy2Exit__()==False:
        #    self.bExitPending=True
            #self.PrintMsg(_(u'exit pending ...'))
        #else:
        #    event.Skip()

    def OnCbCloseButton(self, event):
        if event is not None:
            event.Skip()
        if vtLogDef.VERBOSE:self.logInfo('')
        self.zClose=time.clock()
        kMID=[]
        try:
            kMDI=self.dMDI.keys()
            for k,tup in self.dNetMasters.items():
                if tup[0] in self.dMDI:
                    tupMDI=self.dMDI[tup[0]]
                    kMDI.remove(tup[0])
                    try:
                        tupMDI[1].Close()
                    except:
                        self.logTB()
                        del self.dMDI[tup[0]]
                        del self.dNetMasters[k]
                else:
                    del self.dNetMasters[k]
        except:
            self.logTB()
        try:
            for k in kMDI:
                tupMDI=self.dMDI[k]
                try:
                    tupMDI[1].Close()
                except:
                    self.logTB()
        except:
            self.logTB()
        try:
            for k,tup in self.dAutoRun.items():
                try:
                    tup[1].Close()
                except:
                    self.logTB()
                    del self.dAutoRun[k]
        except:
            self.logTB()
    def OnNotify(self,evt):
        self.Notify()
    def Notify(self):
        # 061006 wro read out queue infos and display them in statusbar and related popup-window
        try:
            if wx.Thread_IsMain():
                pass
            else:
                #self.logError('should not be called by thread')
                return
            
            bVidLog=False
            t = time.localtime(time.time())
            st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%W"), t)
            iInsPos,iInsLast=self._txtClk.GetSelection()
            self._txtClk.SetValue(st)
            if iInsPos>=0:
                self._txtClk.SetSelection(iInsPos,iInsLast)
            
            #self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            def showMsg():
                tup=self.GetPrintMsgTup()
                if tup is not None:
                    #self.rbMsg.put(tup)
                   # self.sb.SetStatusText(tup[0], self.iNum)
                    bIsShown=self.IsShownPopupStatusBar()
                    self.zMsgLast=time.clock()
                    while tup is not None:
                        if bIsShown:
                            self.AddMsg2PopupStatusBar(tup,0)
                        tupLast=tup
                        tup=self.GetPrintMsgTup()
                    self.sb.SetStatusText(tupLast[0], self.iNum)
                else:
                    if (time.clock()-self.zMsgLast)>5:
                        self.sb.SetStatusText('', self.iNum)
            showMsg()
            
            if self.qLaunch.empty()==False:
                if wx.Thread_IsMain():
                    bRdy2Launch=True
                    self.logDebug('launch')
                    #for tup in self.dNetMasters.values():
                    #    netMaster=tup[1]
                    #    try:
                    #        if netMaster.IsAllSettled()==False:
                    #            bRdy2Launch=False
                    #            break
                    #    except:
                    #        pass
                    if bRdy2Launch:
                        dLaunch=self.qLaunch.get()
                        tid=dLaunch['plugIn']
                        self.PrintMsg(_('launching plugin %s ...')%(tid.GetName()))
                        showMsg()
                        self.logDebug('launching')
                        self.__doLaunchPlugIn(dLaunch)
                    self.logDebug('launched')
                else:
                    self.logError('launch not allowed by thread call')
            pass
            if self.bExitPending:
                if self.zExit>=0:
                    if self.__checkRdy2Exit__():
                        self.Close()
                        #wx.CallAfter(self.Close)
                        #self.timer.Stop()
                        #wx.CallLater(2000,self.Close)
                    elif  (self.zExit+120)<time.clock():
                        sys.stderr.write('time:%s timeout exit:%s,qClose:%s\n    zExit:%d zAct:%d'%(st,self.bExitPending,self.qClose.empty(),int(self.zExit),int(time.clock())))
                        while self.qClose.empty()==False:
                            self.qClose.get()
                        self.dMDI={}
                        self.dNetMaster={}
                        self.dAutoRun={}
                        self.Close()
                        #self.timer.Stop()
                        #wx.CallLater(2000,self.Close)
            try:
                for id,win in self.dClose.items():
                    try:
                        bIsShown=win.IsShown()
                    except:
                        bIsShown=False
                    if bIsShown==False:
                        if id in self.dMDI:
                            self.__doMDIClose__(id)
                        if id in self.dAutoRun:
                            self.__doAutoRunClose__(id)
                        
                if self.qClose.empty()==False:
                    try:
                        id=self.qClose.get()
                        tup=None
                        if id in self.dMDI:
                            tup=self.dMDI[id]
                            if tup[1].IsShown():
                                self.qClose.put(id)
                            else:
                                self.__doMDIClose__(id)
                        if id in self.dAutoRun:
                            tup=self.dAutoRun[id]
                            tup[1].Close()
                    except:
                        self.logTB()
                        self.__doMDIClose__(id)
            except:
                self.logTB()
            s=self.getNumDiffStr()
            self.SetStatusText(s, STATUS_LOG_POS)
        except:
            self.logTB()
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=''.join([_("VIDARC MES Center"),' (',self.localDN,')'])
        if s!=sOldTitle:
            self.SetTitle(s)
    
    def OpenFile(self,sFN):
        pass
    def SetLocalDN(self,dn):
        try:
            self.logInfo(`dn`)
        except:
            self.logTB()
        self.localDN=dn
        try:
            import vidarc.config.vcCust as vcCust
            vcCust.USR_LOCAL_DN=self.localDN
        except:
            self.logTB()
    def SetXmlLib(self,sXmlLib):
        self.sXmlLib=sXmlLib
        try:
            import vidarc.config.vcCust as vcCust
            print self.sXmlLib
            vcCust.set2Import('vtXmlDom',self.sXmlLib,'lib')
        except:
            self.logTB()
        
    def OpenCfgFile(self,sFN):
        try:
            self.sCfgFN=sFN
            self.logInfo(self.sCfgFN)
            if self.docCfg is None:
                #self.PrintMsg(_('open config currently not available due missing license.'))
                self.__setCfg__()
                return 
            self.PrintMsg(_('open config ...'))
            
            if self.docCfg.Open(sFN)<0:
                self.docCfg.New(root='configuration')
                self.docCfg.Save(sFN)
            
            self.__setCfg__()
            self.PrintMsg(_('config opened.'))
        except:
            self.logTB()
    def __clrPlugIns__(self,bFull=True):
        try:
            vMESPluginContainer.__clrPlugIns__(self,bFull=bFull)
            if vtLogDef.VERBOSE:self.logInfo('')
            self.trConn.Clear()
            self.trRecent.Clear()
        except:
            self.logTB()
    def __rememberCfg__(self):
        try:
            import vidarc.config.vcCust as vcCust
            section=self.cfg.getSection('server')
            vcCust.NET_DFT_XML_SRV_HOST=section.getInfo('data_host','vidarc')
            vcCust.NET_DFT_XML_SRV_PORT=int(section.getInfo('data_port','50000'))
            vcCust.NET_DFT_FILE_SRV_HOST=section.getInfo('file_host','vidarc')
            vcCust.NET_DFT_FILE_SRV_PORT=int(section.getInfo('file_port','50006'))
            vcCust.NET_DFT_MSG_SRV_HOST=section.getInfo('message_host','vidarc')
            vcCust.NET_DFT_MSG_SRV_PORT=int(section.getInfo('message_port','50004'))
            vcCust.NET_DFT_PLGIN_SRV_HOST=section.getInfo('plugin_host','vidarc')
            vcCust.NET_DFT_PLGIN_SRV_PORT=int(section.getInfo('plugin_port','50002'))
        except:
            pass
    def __getPlugInPaths__(self):
            lDN=[]
            self.cfg.readFN(self.CFG_PLUGIN_FN)     ## activated 070421
            section=self.cfg.getSection('plugins')
            for i in xrange(999):
                sDN=section.getInfo('path_%03d'%i)
                if sDN is None:
                    break
                sDN=sDN.strip()
                if os.path.exists(sDN):
                    lDN.append(sDN)
            sLocaleDN=None
            import __init__
            if __init__.VIDARC_IMPORT:
                try:
                    dn=vIdentify.getCfg('baseDN',machine=False)
                    sLocaleDN=dn
                    if dn not in lDN:
                        lDN.insert(0,dn)
                        i=0
                        for dn in lDN:
                            section.setInfo('path_%03d'%i,dn)
                            i+=1
                        self.cfg.writeFN(self.CFG_PLUGIN_FN)
                except:
                    #self.logTB()
                    pass
            else:
                #sLocaleDN=os.getenv('vidarcLocaleDN')
                sLocaleDN=vSystem.getPlugInLocaleDN()
            return lDN,sLocaleDN
    def __waitRdy2Exit__(self):
        if self.GUI_THREAD:
            while self.__checkRdy2Exit__()==False:
                time.sleep(0.1)
    def __check4PlugIns__(self):
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            if self.GUI_THREAD:
                while self.__checkRdy2Exit__()==False:
                    time.sleep(0.1)
                
                self.PrintMsg(_('plugin searching ...'),bForce=True)
                wx.MutexGuiEnter()
                wx.CallAfter(self.__updateVidObjs__)
            else:
                if self.__checkRdy2Exit__()==False:
                    wx.CallLater(500,self.__check4PlugIns__)
                    return
                wx.CallAfter(self.__updateVidObjs__)
                self.PrintMsg(_('plugin searching ...'),bForce=True)
            #if self.docCfg is None:
            #    self.PrintMsg(_('open config currently not available due missing license.'))
            #    return
            self.__clrPlugIns__()
            #self.PrintMsg(_('plugin searching ...'))
            lDN,sLocaleDN=self.__getPlugInPaths__()
            try:
                import vidarc.tool.lang.vtLgBase as vtLgBaseLib
                vtLgBaseLib.setApplLang(self.lang)
                vtLgBaseLib.setPluginLang(None,sLocaleDN,self.lang)
                vtLgBaseLib.setPluginLang('vtXml',sLocaleDN,self.lang)
            except:
                pass
            try:
                self.__rememberCfg__()
            except:
                self.logTB()
            if vtLogDef.VERBOSE:
                self.logDebug('lDN:%s\n'%repr(lDN))
            self.__searchPlugIns__(lDN)
        except:
            self.logTB()
            dlg = vMsgDialog(self,
                        _(u'Settings are not defined! Please setup plugin-search directories.'),
                        _(u'vMES Center')+u' '+_(u'plug-in search'),
                        wx.OK | wx.ICON_INFORMATION
                        )
            dlg.ShowModal()
            dlg.Destroy()
            # FIXME
            #if self.dlgUpdate is not None:
            #    self.__initVidarcObjs__()
            #    self.dlgUpdate.Destroy()
            #    self.dlgUpdate=None
        if self.GUI_THREAD:
            wx.MutexGuiLeave()
    def __setCfg__(self):
        #sys.stderr.write('__setCfg__\n')
        self.logInfo('')
        try:
            self.cfg.readFN(self.CFG_PLUGIN_FN)
            sec=self.cfg.getSection('MESCenter_layout')
            iX=int(sec.getInfo('x',0))
            iY=int(sec.getInfo('y',0))
            iWidth=int(sec.getInfo('width',1008))
            iHeight=int(sec.getInfo('height',676))
                
            size=self.slwNav.GetSize()
            iNav=int(sec.getInfo('nav_sash',120))
            iTop=int(sec.getInfo('nav_top',80))
            
            try:
                for i in xrange(self.fldNav.GetCount()):
                    it=self.fldNav.GetFoldPanel(i)
                    try:
                        iExpand=int(sec.getInfo('nav_fold_%02d'%i,'0'))
                    except:
                        iExpand=0
                    if iExpand>0:
                        it.Expand()
                    else:
                        it.Collapse()
            except:
                self.logTB()
            #self.slwNav.SizeWindows()
            #self.fldNav.RedisplayFoldPanelItems()
            self.fldNav.RefreshPanelsFrom(self.fldNav.GetFoldPanel(0))
            #self.fldNav.Refresh()
            
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            iNav=self.__checkSash__(0,iNav,NAV_SASH_MIN,NAV_SASH_MAX)
            self.slwNav.SetDefaultSize((iNav, 1000))
            iTop=self.__checkSash__(0,iTop,TOP_SASH_MIN,TOP_SASH_MAX)
            self.slwTop.SetDefaultSize((1000, iTop))
            
            sec=self.cfg.getSection('user')
            self.bAdvanced=int(sec.getInfo('mode_advanced','0'))
            self.bLoginExtended=int(sec.getInfo('login_extended','0'))
            self.trRecent.SetAdvanced(self.bAdvanced)
            self.trConn.SetAdvanced(self.bAdvanced)
            
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
            
            self.bBlockCfgEventHandling=False
        except:
            self.logTB()
    def __checkSash__(self,modified,val,min,max):
        self.logInfo('')
        try:
            sz=self.GetClientSize()
            szTop=self.slwTop.GetSize()
            szNav=self.slwNav.GetSize()
            val+=10
            if modified==1:
                # bottom
                if val+szTop[1]>=sz[1]:
                    val=sz[1]-szTop[1]
            elif modified==2:
                # nav
                pass
            val-=10
            if val<min:
                val=min
            if val>max:
                val=max
        except:
            self.logTB()
        return val
    def OnSlwTopSashDragged(self, event):
        self.logInfo('')
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            #if self.bBlockCfgEventHandling:
            #    return
            iHeight=event.GetDragRect().height
            iHeight=self.__checkSash__(0,iHeight,TOP_SASH_MIN,TOP_SASH_MAX)
            #if self.docCfg is not None:
            #    node=self.docCfg.getChildForced(None,'layout')
            #    self.docCfg.SetValue(node,'top_sash',iHeight)
            #    self.docCfg.Save()
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
        except:
            self.logTB()
    def OnSlwNavSashDragged(self, event):
        self.logInfo('')
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            #if self.bBlockCfgEventHandling:
            #    return
            iWidth=event.GetDragRect().width
            iWidth=self.__checkSash__(2,iWidth,NAV_SASH_MIN,NAV_SASH_MAX)
            #if self.docCfg is not None:
            #    node=self.docCfg.getChildForced(None,'layout')
            #    self.docCfg.SetValue(node,'nav_sash',iWidth)
            #    self.docCfg.Save()
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            self.GetClientWindow().Refresh()
        except:
            self.logTB()
    def OnMnFileMnfile_closeMenu(self, event):
        self.OnCbCloseButton(None)
        self.bExitPending=True
    def OnVMESCenterParentFrameSize(self, event):
        self.logInfo('')
        try:
            wx.LayoutAlgorithm().LayoutMDIFrame(self)
            #if self._txtLog is not None:
            #    rect = self.sbStatus.GetFieldRect(self.STATUS_LOG_POS)
            #    pos=(rect.x+2, rect.y+2)
            #    size=(rect.width-4, rect.height-4)
            #    self._txtLog.Move(pos)
            #    self._txtLog.SetSize(size)
            if self._txtClk is not None:
                rect = self.sbMain.GetFieldRect(STATUS_CLK_POS)
                pos=(rect.x+2, rect.y+2)
                size=(rect.width-4, rect.height-4)
                self._txtClk.Move(pos)
                self._txtClk.SetSize(size)
        except:
            self.logTB()
    def OnVMESCenterParentFrameMove(self, event):
        self.logInfo('')
        try:
            return
        except:
            self.logTB()
    def OnVMESCenterParentFrameClose(self, event):
        #self.logInfo('')
        try:
            import vidarc
            vidarc.CleanUp()
        except:
            pass
        try:
            if event.CanVeto()==False:
                event.Skip()
                return
            #print 'OnVMESCenterParentFrameClose'
            if self.bExitPending:
                if self.zExit>0:
                    if  (self.zExit+120)<time.clock():
                        event.Skip()
                if self.__checkRdy2Exit__():
                    if vtLogDef.VERBOSE:self.logInfo('ready2exit')
                    #self.Close()
                    event.Skip()
                else:
                    event.Veto()
                    if vtLogDef.VERBOSE:self.logInfo('not ready2exit'%())
            else:
                #self.__destroyVidarcObjs__()   # 070711:wro don't do that
                
                #self.logInfo('close all')
                pos=self.GetPosition()
                size=self.GetSize()
                self.cfg.readFN(self.CFG_PLUGIN_FN)
                sec=self.cfg.getSection('MESCenter_layout')
                sec.setInfo('x',str(pos[0]))
                sec.setInfo('y',str(pos[1]))
                sec.setInfo('width',str(size[0]))
                sec.setInfo('height',str(size[1]))
                
                size=self.slwNav.GetSize()
                sec.setInfo('nav_sash',str(size[0]))
                size=self.slwTop.GetSize()
                sec.setInfo('nav_top',str(size[1]))
                
                try:
                    for i in xrange(self.fldNav.GetCount()):
                        it=self.fldNav.GetFoldPanel(i)
                        if it.IsExpanded():
                            iExpand=1
                        else:
                            iExpand=0
                        sec.setInfo('nav_fold_%02d'%i,str(iExpand))
                    self.cfg.writeFN(self.CFG_PLUGIN_FN)
                except:
                    self.logTB()
                #self.logInfo('clear trees')
                tp=self.trRecent.GetRootItem()
                self.trRecent.DeleteChildren(tp)
                tp=self.trConn.GetRootItem()
                self.trConn.DeleteChildren(tp)
                self.__clrPlugIns__()
                self.__clearLaunch__()
                
                if vtLogDef.VERBOSE:self.logInfo('save loginstore')
                self.bExitPending=True
                self.zExit=time.clock()
                if self.oCryptoStore is not None:
                    self.oCryptoStore.Save()
                if vtLogDef.VERBOSE:self.logInfo('loginstore saved')
                if self.__checkRdy2Exit__()==False:
                    if vtLogDef.VERBOSE:self.logInfo('not ready2exit')
                    self.OnCbCloseButton(None)
                    if False==True:
                        dlg = vMsgDialog(self,
                                    _(u'Unsaved data present! Do you want to save data?'),
                                    _(u'vMES Center')+u' '+_(u'Close'),
                                    wx.OK | wx.CANCEL | wx.ICON_QUESTION
                                    #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                                    )
                        if dlg.ShowModal()==wx.ID_OK:
                            self.SaveFile()
                            pass
                    event.Veto()
                else:
                    if vtLogDef.VERBOSE:self.logInfo('ready2exit')
                    event.Skip()
        except:
            self.logTB()
            event.Skip()
    def OnMnHelpMn_help_helpMenu(self, event):
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        event.Skip()
        dlg=vAboutDialog.create(self,images.getApplBitmap())
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()

    def OnMnHelpMn_help_logMenu(self, event):
        try:
            #import vidarc.tool.log.vtLog as vtLog
            import vidarc.tool.log.vtLogFileViewerFrame as vtLogFileViewerFrame
    
            frm=vtLogFileViewerFrame.vtLogFileViewerFrame(self)
            sFN=self.logGetFN() #vtLog.vtLngGetFN()
            if sFN is not None:
                frm.OpenFile(sFN)
            frm.Show()
        except:
            self.logTB()
            self.__showLicenseError__()
        event.Skip()
    def __showLicenseError__(self):
        self.logInfo('')
        #sys.stderr.write('\n__showLicenseError__\n')
        dlg = vMsgDialog(self,
            _(u'Please perform plugin update to use this feature.')+'\n\n'+_(u'License currently not found.'),
            _(u'vMES Center')+u' '+_(u'License Error'),
            wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

    def OnVMESCenterParentFrameIdle(self, event):
        #vtLog.CallStack('')
        #self.Disconnect(wx.EVT_IDLE, self.OnVMESCenterParentFrameIdle)
        #self.Disconnect(self.OnVMESCenterParentFrameIdle)
        #self.Disconnect(wxID_VMESCENTERPARENTFRAME,eventType=wx.EVT_IDLE_PROCESS_ALL)
        try:
            self.Disconnect(wx.wxEVT_IDLE, self.GetId())#,self.OnVMESCenterParentFrameIdle)
        except:
            self.logTB()
            #vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnMnFileMn_file_settingsMenu(self, event):
        self.logInfo('')
        try:
            if self.dlgSettings is None:
                self.dlgSettings=vMESSettingsDialog(self)
                self.dlgSettings.Centre()
            self.dlgSettings.SetCfg()
            #self.dlgSettings.SetMode(self.bAdvanced)
            iRet=self.dlgSettings.ShowModal()
            if iRet>0:
                #self.bAdvanced=self.dlgSettings.GetMode()
                #section=self.cfg.getSection('user')
                #section.setInfo('mode_advanced',str(int(self.bAdvanced)))
                #if self.docCfg:
                #    node=self.docCfg.getChildForced(None,'user')
                #    self.docCfg.SetValue(node,'mode',int(self.bAdvanced))
                    #self.__showRecent__()
                #    self.docCfg.Save()
                self.cfg.readFN(self.CFG_PLUGIN_FN)
                self.__destroyVidarcObjs__()
                self.__bUpdated__=True
                if self.GUI_THREAD:
                    if vtLogDef.VERBOSE:self.logDebug('__check4Plugins__ scheduled')
                    self.thdGui.Do('__check4Plugins__',self.__check4PlugIns__)
                else:
                    wx.CallAfter(self.__check4PlugIns__)
                return
            #self.__check4PlugIns__()
            self.__bUpdated__=False
        except:
            self.logTB()
    def getPlugInConnectionInfos(self):
            sHost='vidarc'
            sPort='50002'
            sUsr=self.sLogin
            sPasswd=self.sPasswd
            try:
                self.cfg.readFN(self.CFG_PLUGIN_FN)
                section=self.cfg.getSection('server')
                sHost=section.getInfo('plugin_host','vidarc')
                sPort=section.getInfo('plugin_port','50002')
            except:
                sHost='vidarc'
                sPort='50002'
                
            if self.oCryptoStore is not None:
                try:
                    sHostStore,sPortStore,sUsrStore,sPasswdStore=self.oCryptoStore.GetPlugInConnection()
                    if (len(sHostStore)>0) and (len(sPortStore)>0):
                        sHost,sPort,sUsr,sPasswd=sHostStore,sPortStore,sUsrStore,sPasswdStore
                except:
                    pass
            return sHost,sPort,sUsr,sPasswd
    def OnMnFileMnfile_updateMenu(self, event):
        self.logInfo('')
        try:
            if event is not None:
                event.Skip()
            if len(self.sLogin)==0:
                if self.dlgLogin is None:
                    self.dlgLogin=vLoginDialog(self)
                    self.dlgLogin.Centre()
                self.dlgLogin.Clear()
                if self.dlgLogin.ShowModal()>0:
                   self.sLogin=self.dlgLogin.GetLogin()
                   self.sPasswd=self.dlgLogin.GetPasswd()
                else:
                    dlg = vMsgDialog(self,
                            _(u'Login error, you have to login before udpate is allowed.'),
                            _(u'vMES Center')+u' '+_(u'Login Error'),
                            wx.OK | wx.ICON_INFORMATION)
                    dlg.ShowModal()
                    dlg.Destroy()
                    return
            if VERBOSE_LANG:
                self.logDebug(self.lang)
            
            self.__updatePlugIn__(bShow=True)
            #else:
            #    sHost='vidarc'
            #    sPort='50002'
            #    sUser=''
            #    try:
                    #import vidarc.config.vcCust as vcCust
            #        section=self.cfg.getSection('server')
            #        sHost=section.getInfo('plugin_host','vidarc')
            #        sPort=section.getInfo('plugin_port','50002')
            #    except:
            #        pass
        except:
            self.logTB()
    def OnCbMsgViewButton(self, event):
        event.Skip()
    def OnCbMsgCfgButton(self, event):
        event.Skip()
    def OnLstMsgListColClick(self, event):
        event.Skip()
    def OnLstMsgListItemDeselected(self, event):
        event.Skip()
    def OnLstMsgListItemSelected(self, event):
        event.Skip()
    def OnMnFileMnfile_configMenu(self, event):
        event.Skip()
        self.logInfo('')
        try:
            #if self.docCfg is None:
            #    self.__showLicenseError__()
            #    return
            if self.dlgConfig is None:
                self.dlgConfig=vMESConfigDialog(self)
                self.dlgConfig.Centre()
            self.dlgConfig.SetCfg(self.docCfg)
            iRet=self.dlgConfig.ShowModal()
            if iRet>0:
                self.dlgConfig.GetCfg(self.docCfg)
                if self.docCfg is not None:
                    self.docCfg.Save()
                try:
                    self.cfg.readFN(self.CFG_PLUGIN_FN)
                except:
                    pass
                self.__rememberCfg__()
                try:
                    import vidarc.tool.time.vtTimeLoadBound as vtLdBd
                    try:
                        section=self.cfg.getSection('general')
                        vtLdBd.vtTimeLoadBoundInit(int(section.getInfo('load_bound','75'))/100.0)
                    except:
                        vtLdBd.vtTimeLoadBoundInit(0.75)
                    try:
                        section=self.cfg.getSection('user')
                        self.bAdvanced=int(section.getInfo('mode_advanced','0'))
                    except:
                        self.bAdvanced=False
                        self.logTB()
                    self.trRecent.SetAdvanced(self.bAdvanced)
                    self.trConn.SetAdvanced(self.bAdvanced)
                except:
                    pass
        except:
            self.logTB()
    def OnSbMainRightUp(self, event):
        event.Skip()
        self.logDebug('')
        try:
            if self.ShowPopupStatusBar():
                self.AddMsg2PopupStatusBar((None,None))
                self.rbMsg.proc(self.AddMsg2PopupStatusBar)
                return
                for i in xrange(100):
                    tup=self.rbMsg.get(i)
                    if tup is None:
                        break
                    self.AddMsg2PopupStatusBar(tup[0],tup[1])
        except:
            self.logTB()
    def OnScreenShot(self,evt):
        evt.Skip()
        try:
            import vidarc.tool.log.vtLog as vtLog
            sLogFN=vtLog.vtLngGetFN()
            sDN,sFN=os.path.split(sLogFN)
            sPicFN=genScreenShot(sDN,'Main')
            vtLog.PrintMsg(_(u'screenshot %s')%(sPicFN),self)
            return
        except:
            pass
    def OnGuiFin(self,evt):
        evt.Skip()
        if 0:
            print 'OnGuiFin'
            print '   ',evt.GetCmd(),evt.GetRetVal(),evt.IsFaulty()
        pass
        
def genScreenShot(sDN,sPostfixFN,sPrefixFN=None):
    """generate screenshot from vMESCenter window to file
    output filename:
        <sPrefixFN>_<hostname>_yyyymmdd_HHMMSS_<sPostfixFN>_xxx.png
        xxx is unique number starting at 000
    output directory is defined by sDN
    return value:
        output filename 
            is None is returned a failure has occurred, either sDN does
            not exist, or output process has failed.
            trackback information is written to stderr
    """
    try:
        import os.path
        import platform
        import time
        try:
            os.stat(sDN)
        except:
            return None
        zDt=time.localtime(time.time())
        sDt=time.strftime('%y%m%d_%H%M%S',zDt)
        app=wx.GetApp()
        frm=app.GetTopWindow()
        sz=frm.GetSize()
        cdc = wx.WindowDC(frm)
        mdc=wx.MemoryDC()
        bmp=wx.EmptyBitmap(sz[0],sz[1])
        mdc.SelectObject(bmp)
        mdc.Blit(0,0,sz[0],sz[1],cdc,0,0)
        if sPrefixFN is None:
            sPrefixFN='vMESCenter'
        i=0
        bOk=False
        while bOk==False:
            sPicFN='.'.join(['_'.join([sPrefixFN,platform.uname()[1],sDt,sPostfixFN,'%03d'%i]),
                        'png'])
            sPicFullFN=os.path.join(sDN,sPicFN)
            try:
                os.stat(sPicFullFN)
                i+=1
            except:
                bOk=True
        bmp.SaveFile(sPicFullFN,wx.BITMAP_TYPE_PNG)
        return sPicFullFN
    except:
        sys.stderr.write(traceback.format_exc())
        return None
