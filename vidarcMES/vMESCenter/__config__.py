#----------------------------------------------------------------------
# flags and values that affect this script
#----------------------------------------------------------------------
#----------------------------------------------------------------------------
# Name:         __config__.py
# Purpose:      package configuration informations
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: __config__.py,v 1.24 2012/01/29 17:16:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

VER_MAJOR        = 2      # The first three must match wxWidgets
VER_MINOR        = 6
VER_RELEASE      = 6
VER_SUBREL       = 0      # wxPython release num for x.y.z release of wxWidgets
VER_FLAGS        = ""     # release flags, such as prerelease or RC num, etc.
VERSION          = u'%d.%d.%d'%(VER_MAJOR,VER_MINOR,VER_RELEASE)

DESCRIPTION      = "VIDARC MES Center"
AUTHOR           = "Walter Obweger"
AUTHOR_EMAIL     = "Walter Obweger <walter.obweger@vidarc.com>"
URL              = "http://www.vidarc.com/"
DOWNLOAD_URL     = "http://www.vidarc.com/"
LICENSE          = "commerical, VIDARC license"
PLATFORMS        = "WIN32,OSX,POSIX"
KEYWORDS         = "MES,center"

LONG_DESCRIPTION = """\
This is the MES core application from where all modules can be accessed.
The modules are defined by plugins, which may be VIDARC licensed, free or
third party plugins.

The MES Center can be updated without restarting.

Designed by VIDARC Automation GmbH, Walter Obweger.
"""

CLASSIFIERS      = """\
Development Status :: 2 - Development
Environment :: Win32 (MS Windows)
Environment :: X11 Applications :: GTK
Intended Audience :: Developers
License :: 
Operating System :: MacOS :: MacOS X
Operating System :: Microsoft :: Windows :: Windows 95/98/2000/XP
Operating System :: POSIX
Programming Language :: Python
Topic :: Software Development :: User Interfaces
"""

SEC_LEVEL_LIMIT_ADMIN=24
SEC_LEVEL_LIMIT_CONFIG=16

NODE_GUI=1


INST_PATH="vidarc/apps"
GROUP_BASE="VIDARC"

APP='vMESCenter'
ICONS=[(1,'img/Vid_MES02_16.ico')]
APP_PY='vExplorerAppl.py'
APP_INCL=['vMESCenterParentFrame']
APP_EXCL=[]
LANG=['en','de']
