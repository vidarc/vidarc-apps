#----------------------------------------------------------------------------
# Name:         vGuiThread.py
# Purpose:
# Author:       Walter Obweger
#
# Created:      20070105
# CVS-ID:       $Id: vGuiThread.py,v 1.1 2007/05/07 18:51:05 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx

import Queue
import thread,threading,time,sys,traceback

VERBOSE=0

wxEVT_VGUI_THREAD_FINISHED=wx.NewEventType()
vEVT_VGUI_THREAD_FINISHED=wx.PyEventBinder(wxEVT_VGUI_THREAD_FINISHED,1)
def EVT_VGUI_THREAD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VGUI_THREAD_FINISHED,func)
def EVT_VGUI_THREAD_FINISHED_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_VGUI_THREAD_FINISHED,func)
class vGuiThreadFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VGUI_THREAD_FINISHED(<widget_name>, self.OnFinished)
    """
    def __init__(self,obj,cmd,ret):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VGUI_THREAD_FINISHED)
        self.cmd=cmd
        self.ret=ret
    def GetCmd(self):
        return self.cmd
    def GetRetVal(self):
        return self.ret
    def IsFaulty(self):
        return self.ret==sys.maxint

class vGuiThread:
    def __init__(self,par,verbose=0):
        self.verbose=verbose
        if self.verbose<0:
            self.verbose=0
        self.par=par
        self.Clear()
        self.keepGoing=self.running=False
        self.qSched=Queue.Queue()
    def Clear(self):
        pass
    def Do(self,cmd,func,*args,**kwargs):
        #print 
        #traceback.print_stack()
        #print cmd,func,args,kwargs
        if self.running:
            self.qSched.put((cmd,func,args,kwargs))
            return
        self.qSched.put((cmd,func,args,kwargs))
        self.keepGoing=self.running=True
        thread.start_new_thread(self.Run,())
    def Stop(self):
        self.keepGoing = False
    def Is2Stop(self):
        return self.keepGoing==False
    def IsRunning(self):
        return self.running
    def Run(self):
        self.running=True
        try:
            while self.keepGoing:
                cmd,func,args,kwargs=self.qSched.get(0)
                try:
                    ret=func(*args,**kwargs)
                    wx.PostEvent(self.par,vGuiThreadFinished(self.par,cmd,ret))
                except:
                    wx.PostEvent(self.par,vGuiThreadFinished(self.par,cmd,sys.maxint))
            if self.keepGoing==False:
                while self.qSched.empty()==False:
                    t=self.qSched.get(0)
        except:
            pass
        self.keepGoing=self.running=False

