#----------------------------------------------------------------------------
# Name:         vMESPluginContainer.py
# Purpose:      combine all relevant objects/widgets for plugin support
#               it performs search and display features
#
# Usage:        inherit this class
# Requirements: properties
#               gProcess        ... wx.Gauge object
#                                   to display progress
#               qLaunch         ... queue to schedule module start
#               trPlugins       ... plugin tree widgets 
#                                   (derived from vMESPluginTree)
#               methods
#               logDebug(msg)           ... log parameter msg at level 'DEBUG'
#               logInfo(msg)            ... log parameter msg at level 'INFO'
#               logError(msg)           ... log parameter msg at level 'ERRPR'
#               logTB()                 ... log error traceback
#               PrintMsg(msg)           ... print message for user
#               __addPlugIn__(plugin)   ... add normal plugin
# Author:       Walter Obweger
#
# Created:      20070710
# CVS-ID:       $Id: vMESPluginContainer.py,v 1.11 2012/01/29 17:15:10 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys,traceback

import wx

try:
    import vtLogDef
    from vMESpluginFind import EVT_VMESPLUGIN_THREAD_PROC
    from vMESpluginFind import EVT_VMESPLUGIN_THREAD_ADD
    from vMESpluginFind import EVT_VMESPLUGIN_THREAD_ADD_FINISHED
    from vMESpluginFind import thdMESpluginFind
    from vMESplugin import vMESplugin
    from vMESPlugInUpdateDialog import vMESPlugInUpdateDialog
except:
    sys.stderr.write(traceback.format_exc())

class vMESPluginContainer:
    def __init__(self):
        self.thdPlugin=thdMESpluginFind(self,verbose=0)
        self.thdPlugin.BindEvents(funcProc=self.OnPlugInProc,
                funcAdd=self.OnPlugInAdd,
                funcFin=self.OnPlugInFin)
        self.dPlugIn={}
        self.dPlugInAutoRun={}
        # some flags
        self.__bUpdated__=False
        # gui widgets
        self.dlgUpdate=None
        self.trPlugins=None
    def __setTreePlugIns__(self,tr):
        self.trPlugins=tr
    def __clrPlugIns__(self,bFull=True):
        try:
            if vtLogDef.VERBOSE:self.logInfo('')
            self.dPlugIn={}
            self.dPlugInAutoRun={}
            self.trPlugins.Clear(bFull)
        except:
            self.logTB()
    def __searchPlugIns__(self,lDN):
        self.thdPlugin.Find(lDN)
    def OnPlugInProc(self,evt):
        evt.Skip()
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            iAct=evt.GetAct()
            iSize=evt.GetSize()
            if iSize>0:
                iVal=((iAct/float(iSize))*1000)
            else:
                iVal=0
            self.gProcess.SetValue(iVal)
        except:
            self.logTB()
    def OnPlugInAdd(self,evt):
        evt.Skip()
        try:
            name=evt.GetName()
            #print traceback.print_stack()
            #print name
            #self.PrintMsg(_('plugin searching ... %s found ')%(name))
            #if vtLogDef.VERBOSE:self.logInfo('%s'%(name))
            i=name.find('vApps')
            if i>0:
                strs=name[i+6:].split('.')
                strs=[strs[0]]
            #    tip=self.tiModuls
            #    d=self.dPlugIn
            else:
                strs=name.split('.')
                d=self.dPlugIn
                #tip=self.trPlugins.GetRootItem()
                #tip=self.tiPlgIns
            plugin=vMESplugin(evt.GetName(),evt.IsFrame(),evt.GetDN(),evt.GetImg())
            if evt.IsFrame()>100:
                self.dPlugInAutoRun[plugin.GetName()]=(None,{},plugin)
                return
            self.trPlugins.AddPlugIn(self.dPlugIn,plugin)
            self.__addPlugIn__(plugin)
        except:
            self.logTB()
    def OnPlugInFin(self,evt):
        evt.Skip()
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            self.trPlugins.PlugInFin()
            self.gProcess.SetValue(0)
            #wx.CallAfter(self.__updateVidObjs__)   # 070711:wro
            if self.oCrypto is None:
                self.PrintMsg(_('plugin search finished.')+_('VIDARC license not found.'))
            else:
                self.PrintMsg(_('plugin search finished.'))
            #if self.dlgUpdate is not None:
            #    self.dlgUpdate.Destroy()
            #    self.dlgUpdate=None
            #wx.CallAfter(self.__initVidarcObjs__)
            wx.CallAfter(self.launchAutoRun)
        except:
            self.logTB()
    def launchAutoRun(self):
        if vtLogDef.VERBOSE:self.logInfo('')
        try:
            l=[]
            for tup in self.dPlugInAutoRun.itervalues():
                plugin=tup[2]
                iPrior=plugin.IsFrame()
                name=plugin.GetName()
                i=name.find('vApps')
                if i>0:
                    strs=name[i+6:].split('.')
                    strs=[strs[0]]
                else:
                    strs=name.split('.')
                if iPrior>100:
                    dLaunch={'plugIn':plugin,'lang':self.lang}
                    dLaunch['lstNames']=strs
                    dLaunch['connection']=''
                    l.append((iPrior,dLaunch))
            l.sort()
            for iPrior,dLaunch in l:
                try:
                    self.qLaunch.put(dLaunch,False)
                    self.PrintMsg(_('schedule plugin %s for launch.')%(plugin.GetName()))
                except:
                    self.PrintMsg(_('schedule plugin %s not possible yet.')%(plugin.GetName()))
        except:
            self.logTB()
    def __updatePlugIn__(self,bShow=True):
        try:
            self.logInfo('bShow:%d'%(bShow))
            if self.dlgUpdate is None:
                self.dlgUpdate=vMESPlugInUpdateDialog(self)
                self.dlgUpdate.Centre()
            self.dlgUpdate.Clear()
            sHost,sPort,sUser,sPwd=self.getPlugInConnectionInfos()
            self.dlgUpdate.SetConnection(sHost,sPort,sUser,sPwd)
            if bShow==True:
                # manual mode
                self.__destroyVidarcObjs__()
                self.dlgUpdate.SetModeAuto(False,None)
                if self.dlgUpdate.ShowModal()>0:
                    self.__bUpdated__=True
                    self.__updatePlugInFin__(bUpdated=True)
                else:
                    self.__bUpdated__=False
                    self.__updatePlugInFin__(bUpdated=False)
            else:
                # automatic mode
                self.dlgUpdate.SetModeAuto(True,self.__updatePlugInFin__)
                #self.dlgUpdate.SetModeAuto(False,self.__updatePlugInFin__)
        except:
            self.logTB()
    def __updatePlugInFin__(self,bUpdated=True):
        try:
            if vtLogDef.VERBOSE:self.logInfo('')
            self.__bUpdated__=bUpdated
            self.doImport()
            if self.GUI_THREAD:
                #self.thdGui.Do('__updateVidObjs__',self.__updateVidObjs__)
                #self.logDebug('__check4Plugins__ scheduled')
                #self.thdGui.Do('__waitRdy2Exit__',self.__waitRdy2Exit__)
                self.thdGui.Do('__check4Plugins__',self.__check4PlugIns__)
            else:
                #wx.CallAfter(self.__updateVidObjs__)
                wx.CallAfter(self.__check4PlugIns__)
        except:
            self.logTB()
