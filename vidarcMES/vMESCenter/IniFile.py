#----------------------------------------------------------------------------
# Name:         IniFile.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20100214
# CVS-ID:       $Id: IniFile.py,v 1.4 2011/08/26 09:44:06 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vLogFallBack
vLogFallBack.logDebug('import;start',__name__)
try:
    import ConfigParser,sys,os
    #import tool.InOut.fnUtil as fnUtil
except:
    vLogFallBack.logTB(__name__)
vLogFallBack.logDebug('import;done',__name__)

class Section:
	def __init__(self,cfg,section):
		self.section=section
		self.cfg=cfg
		self.generate()
		self.modified=0
	def getInfo(self,info,dft=None):
		try:
			return self.__dict__[info]
		except:
			self.__dict__[info]=dft
			return dft
	def generate(self,verbose=0):
		if self.cfg is None:
			return
		if self.cfg.has_section(self.section)==0:
			if verbose>0:
				print 'Section::generate add section'
			self.cfg.add_section(self.section)
		for o in self.cfg.options(self.section):
			if verbose>0:
				print 'Section::generate set option',o
			self.__dict__[o]=self.cfg.get(self.section,o)
		self.modified=0
	def get_options(self):
		return self.cfg.options(self.section)
	def remove_option(self,option):
		return self.cfg.remove_option(self.section,option)
	def setInfo(self,info,value):
		try:
			if self.__dict__[info]!=vslue:
				self.modified=1
		except:
			self.modified=1
		self.__dict__[info]=value
		if self.cfg is None:
			return
		self.cfg.set(self.section,info,value)
class Config(ConfigParser.ConfigParser):
	def __init__(self,cfgFN=None):
		ConfigParser.ConfigParser.__init__(self)
		self.cfgFN=None
		self.readFN(cfgFN)
		self.sec={}
	def __gen__(self):
		self.sec={}
		for s in self.sections():
			self.sec[s]=Section(self,s)
	def isModified(self):
		self.modified=0
		for s in self.sec.values():
			if s.modified>0:
				self.modified=1
				break
		return self.modified
	def addSection(self,section):
		try:
			self.sec[section]
			#raise ConfigSection("section already exists")
		except:
			self.sec[section]=Section(self,section)
	def getSection(self,section):
		try:
			return self.sec[section]
		except:
			self.addSection(section)
			return self.sec[section]
	def readFN(self,cfgFN):
		#print os.getcwd(),cfgFN
		#vLogFallBack.logDebug('readFN;fn:%s'%(cfgFN),__name__)
		if cfgFN is not None:
			self.cfgFN=cfgFN
		if self.cfgFN is None:
			return
		ConfigParser.ConfigParser.__init__(self)
		self.read(self.cfgFN)
		self.__gen__()
	def writeFN(self,cfgFN=None,baks=2):
		"""write configuration to file
		cfgFN = None take known filename
		      <> None write to filename
		baks = count of bakup files
		"""
		if cfgFN is not None:
			self.cfgFN=cfgFN
		if self.cfgFN is None:
			return
		# bakup old file
		#if baks>0:
	#		fnUtil.shiftFile(self.cfgFN,baks)
		ofn=open(self.cfgFN,'w')
		self.write(ofn)
		ofn.close()

if __name__=='__main__':
	cfg=Config()
	print 'read configfile'
	print __name__
	baseDir=os.getenv('PythonProjBaseDir')+'/tool/InOut'
	#print __file__
	
	cfg.readFN(baseDir+'/cfgFile_Test.ini')
	
	print 'sections',cfg.sections()

	for sec in cfg.sections():
		print 'section:',sec
		print '   ','option',cfg.options(sec)
		print 
		print cfg.sections()[0]
	cfg.set(cfg.sections()[0],'keys','primary')
	cfg.writeFN(baseDir+'/cfgFile_Out.ini')

	cfg.addSection(cfg.sections()[0])
