#----------------------------------------------------------------------------
# Name:         vSockXmlClt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20061228
# CVS-ID:       $Id: vSockXmlClt.py,v 1.1 2007/01/07 17:45:23 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import thread,traceback
import time,random
import socket,sha,binascii,zipfile

import sys,os,os.path,sha
from vSock import *
import __init__
if __init__.SSL:
    from OpenSSL import SSL
    from OpenSSL import tsafe

BUFFER_MAX=40*1024*1024
VERBOSE_TRANS=0

class vSockXmlClt(vSock):
    SSL=__init__.SSL
    def __init__(self,server,conn,adr,sID='',verbose=0):
        vSock.__init__(self,server,conn,adr,sID,BUFFER_MAX,verbose=verbose)
        try:
            if self.SSL:
               self.conn.do_handshake()
               pass
        except:
            #vtLog.vtLngTB(self.__class__.__name__)
            pass
        self.bEstabilshed=False
        self.loggedin=False
        self.iStart=-1
        self.iAct=0
        self.iEnd=-1
        self.encoded_passwd=False
        self.verbose=verbose
        self.cmds=[
            ('alias',self.__alias__),
            ('login',self.__login__),
            ('loggedin',self.__loggedin__),
            ('authenticate',self.__authenticate__),
            ('browse',self.__browse__),
            ('hypbrowse',self.__hyper_browse__),
            (u'sessionID',self.__sessionID__),
            (u'shutdown',self.__shutdown__)]
        self.appl_alias=''
        self.appl=''
        self.par=None
        self.dFileGet={}
    def DoVerify(self,conn, cert, errnum, depth, ok):
        #traceback.print_stack()
        #vtLog.vtLngCur(vtLog.INFO,'ok:%d'%(ok),self.appl_alias)
        if ok:
            self.serving=True
            #vtLog.PrintMsg(_('alias:%s SSL got certificate: %s')%(self.appl_alias,cert.get_subject()),self.par)
        else:
            self.serving=False
            #vtLog.vtLngCur(vtLog.ERROR,'errnum:%d'%(errnum),self.appl_alias)
            #vtLog.vtLngCurCls(vtLog.ERROR,'errnum:%s;depth:%s;ok:%s'%(`errnum`,`depth`,`ok`),self.appl_alias)
        if self.serving==False:
            #vtLog.PrintMsg(_('alias:%s SSL verify fault')%(self.appl_alias),self.par)
            #if self.par is not None:
            #    self.par.NotifyConnectionFault()
            pass
    def CheckPause(self):
        try:
            if self.paused!=self.pausing:
                self.paused=self.pausing
                if self.par is not None:
                    self.par.NotifyPause(self.paused)
        except:
            traceback.print_exc()
    def IsLoggedIn(self):
        return self.loggedin
    def HandleData(self):
        try:
            if self.iStart==-1:
                self.iStart=self.data.find(self.startMarker,self.iAct)
                #self.iAct=self.iStart   # 060225
                #self.iStart+=self.iStartMarker
                self.iAct=self.iStart+self.iStartMarker
            if self.iStart>=0:
                # start marker found
                try:
                    self.iEnd=self.data.find(self.endMarker,self.iAct)
                except:
                    pass
                #self.iAct=self.len
                if self.iEnd>0:
                    # end marker found, telegram is complete
                    self.iStart+=self.iStartMarker
                    val=self.data[self.iStart:self.iEnd]
                    for cmd in self.cmds:
                        k=cmd[0]
                        iLen=len(k)
                        if val[:iLen]==k:
                            #cmd[1](val[iLen:].encode('ISO-8859-1'))
                            cmd[1](val[iLen:])
                            break
                    self.iEnd+=self.iEndMarker
                    #self.data=self.data[self.iEnd:]   # 060225
                    #self.len-=self.iEnd               # 060225 
                    #self.iStart=self.iEnd=self.iAct=-1  # 060225
                    self.iAct=self.iEnd
                    self.iStart=self.iEnd=-1
                    if self.iAct>=self.len:
                        self.data=''
                        self.len=0
                        self.iAct=0
                        return 0
                    return 1
                else:
                    self.data=self.data[self.iStart:]
                    self.iAct=0
                    self.len-=self.iStart
                    self.iStart=-1
        except:
            traceback.print_exc()
        return 0
    def SetParent(self,par):
        self.par=par
    def SocketClosed(self):
        if self.par is not None:
            self.par.__connectionClosed__(self.bEstabilshed)
    def Stop(self):
        vSock.Stop(self)
    def IsRunning(self):
        return vSock.IsRunning(self)
    def Login(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=False
        self.bLoginSend=False
    def LoginEncoded(self,usr,passwd):
        self.usr=usr
        self.passwd=passwd
        self.encoded_passwd=True
        self.bLoginSend=False
    def __sessionID__(self,val):
        self.sessionID=val[1:]
        #self.__doLogin__()
    def __doLogin__(self):
        if len(self.passwd)>0:
            if self.encoded_passwd:
                sPasswd=self.passwd
            else:
                m=sha.new()
                m.update(self.passwd)
                sPasswd=m.hexdigest()
            m=sha.new()
            m.update(sPasswd)
            m.update(self.sessionID)
            s=binascii.b2a_uu(m.digest())
        else:
            s=''
        self.SendTelegram('login|'+self.usr+','+s)
        self.bLoginSend=True
    def __login__(self,val):
        if self.bLoginSend==False:
            self.__doLogin__()
        else:
            self.par.NotifyLogin()
    def __loggedin__(self,val):
        self.loggedin=True
        try:
            self.par.NotifyLoggedIn(self.loggedin)
        except:
            traceback.print_exc()
    def __authenticate__(self,val):
        pass
    def __browse__(self,val):
        self.par.NotifyBrowse(val[1:].split(';'))
    def __hyper_browse__(self,val):
        self.par.NotifyBrowse(val[1:].split(';'))
    def ShutDown(self):
        pass
    def Browse(self,appl):
        self.SendTelegram('browse|'+appl)
    def HyperBrowse(self,usr,appl):
        self.SendTelegram('hypbrowse|'+','.join([usr,appl]))
    def __alias__(self,val):
        i=val.find('|')
        if i>=0:
            try:
                if val[i+1:]=='served':
                    # notify served
                    if self.par is not None:
                        self.par.NotifyServed(True)
                else:
                    if self.par is not None:
                        self.par.NotifyServed(False)
            except:
                pass
    def Alias(self,appl,alias):
        self.appl=appl
        self.appl_alias=':'.join([appl,alias])
        self.SendTelegram('alias|'+self.appl_alias)
    def __shutdown__(self,val):
        pass

