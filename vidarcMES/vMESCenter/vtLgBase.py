#----------------------------------------------------------------------------
# Name:         vtLgBase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLgBase.py,v 1.6 2009/10/03 22:32:35 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import os,sys,getopt
import gettext,traceback
import wx
import locale

#import __init__
VERBOSE=0

MAP_LOCALE_ON_MSWIN={
    'de_DE':'German_Germany',
    'de_CH':'German_Switzerland',
    'de_AT':'German_Austria',
    }
    
APPL_BASE_DN=''
def getApplBaseDN():
    global APPL_BASE_DN
    return APPL_BASE_DN
APPL_LANG='en'
def getApplLang():
    global APPL_LANG
    return APPL_LANG
def setApplLang(lang):
    global APPL_LANG
    APPL_LANG=lang
    #setLocale(APPL_LANG)        # 070907:wro
APPL_DOMAIN={}
def getApplDomain():
    global APPL_DOMAIN
    return APPL_DOMAIN
def initAppl(optshort,optlang,domain):
    langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    lang=locale.getdefaultlocale()[0]
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlang)
        for o in optlist:
            if o[0] in ['--lang','-l']:
                lang=o[1]
    except:
        traceback.print_exc()
    ll=wx.Locale.FindLanguageInfo(lang)
    if ll is None:
        ll=wx.Locale.FindLanguageInfo('en')
    langid=ll.Language
    lang=lang[:2]
    
    basepath = os.path.abspath(os.path.dirname(sys.argv[0]))
    localedir = os.path.join(basepath, "locale")
    if os.path.isdir(localedir)==False:
        sTmp=os.getenv('vidarcLocaleDN')
        if sTmp is not None:
            localedir=sTmp
            basepath,sTmp=os.path.split(localedir)
    if VERBOSE:
        print basepath,localedir,domain,langid
    #sys.stderr.write('02'+str(langid)+'\n')
    #sys.stderr.write('09'+lang+'\n')
    global APPL_LANG
    APPL_LANG=lang
    global APPL_BASE_DN
    APPL_BASE_DN=basepath
    global APPL_DOMAIN
    APPL_DOMAIN=domain
    # Set locale for wxWidgets
    if wx.GetApp() is not None:
        mylocale = wx.Locale(langid)
        mylocale.AddCatalogLookupPathPrefix(localedir)
        mylocale.AddCatalog(domain)
    if VERBOSE:
        print lang
    # Set up Python's gettext
    mytranslation = gettext.translation(domain, localedir,
        #[mylocale.GetCanonicalName()], fallback = True)
        [lang], fallback = True)
    mytranslation.install(unicode=1)
    locale.setlocale(locale.LC_ALL,'')
def getPossibleLang(domain,basepath):
    if VERBOSE:
        traceback.print_stack()
    langs=[]
    localedir = os.path.join(basepath, "locale")
    try:
        if os.path.isdir(localedir)==False:
            sTmp=os.getenv('vidarcLocaleDN')
            if sTmp is not None:
                localedir=sTmp
                basepath,sTmp=os.path.split(localedir)
        files=os.listdir(localedir)
        for fn in files:
            try:
                sFullFN=os.path.join(localedir,fn)
                if os.path.isdir(sFullFN):
                    if os.path.isdir(os.path.join(sFullFN,'LC_MESSAGES')):
                        langs.append(fn)
            except:
                pass
        if VERBOSE:
            print ' '.join(['    ']+langs+['\n'])
    except:
        traceback.print_exc()
    return langs
def setPluginLang(domain,dn,lang):
    if VERBOSE:
        traceback.print_stack()
        sys.stderr.write('    domain:%d dn:%s lang:%s'%(domain,dn,lang))
    langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    if lang=='en':
        langid = wx.LANGUAGE_ENGLISH    # use OS default; or use LANGUAGE_JAPANESE, etc.
    elif lang=='de':
        langid = wx.LANGUAGE_GERMAN
    if langid==wx.LANGUAGE_ENGLISH:
        lang='en'
    elif langid==wx.LANGUAGE_GERMAN:
        lang='de'
    else:
        lang='en'
    if VERBOSE:
        sys.stderr.write('    domain:%d dn:%s lang:%s'%(domain,dn,lang))
    localedir = os.path.join(dn, "locale")
    # Set locale for wxWidgets
    #mylocale = wx.Locale(langid)
    #mylocale.AddCatalogLookupPathPrefix(localedir)
    #mylocale.AddCatalog(domain)

    # Set up Python's gettext
    try:
        mytranslation = gettext.translation(domain, localedir,
                    [lang], fallback = True)
    except:
        #if VERBOSE_LOG:
        #    vtLog.vtLngTB(__name__)
        try:
            dnTmp=os.getenv('vidarcLocaleDN')
            if dnTmp is not None:
                if len(dnTmp)>0:
                    dn=dnTmp
            localedir = os.path.join(dn, "locale")
            mytranslation = gettext.translation(domain, localedir,
                    [lang], fallback = True)
        except:
            traceback.print_exc()
    locale.setlocale(locale.LC_ALL,'')
    try:
        __init__.PLUGIN_LANG=lang
        __init__.PLUGIN_TRANS=mytranslation
        if VERBOSE:
            sys.stderr.write('    lang:%s fn:%s\n'%(__init__.PLUGIN_LANG,__init__.__file__))
    except:
        pass
def getPluginLang():
    try:
        if VERBOSE:
            traceback.print_stack()
            sys.stderr.write('    lang:%s'%(__init__.PLUGIN_LANG))
        return __init__.PLUGIN_LANG
    except:
        return 'en'
def assignPluginLang():
    if VERBOSE:
        traceback.print_stack()
    try:
        return __init__.PLUGIN_TRANS.ugettext
    except:
        return _
class vtLangSetLocale(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return 'unknow locale:'+self.value
def setLocale(str=None):
    if str is None:
        locale.setlocale(locale.LC_ALL,str)
    if len(str)==0:
        locale.setlocale(locale.LC_ALL,str)
    if wx.Platform=='__WXMSW__':
        
        try:
            locale.setlocale(locale.LC_ALL,MAP_LOCALE_ON_MSWIN[str])
        except:
            raise vtLangSetLocale(str)
    else:
        locale.setlocale(locale.LC_ALL,str)
def format(fmt,val):
    return locale.format(fmt,val)
def getNumSeperatorsFromNumMask():
    ld=locale.localeconv()
    sDec=ld['decimal_point']
    sTho=ld['thousands_sep']
    if len(sTho)==0:
        if sDec=='.':
            sTho=','
        else:
            sTho='.'
    return sDec,sTho
