#----------------------------------------------------------------------------
# Name:         vSystem.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060219
# CVS-ID:       $Id: vSystem.py,v 1.12 2010/05/21 17:17:44 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------


import wx
import os
import sys

gUsrCfgDN=None

MAP_PLATFORM_IMP_SUF={0:'Unx',1:'Win'}

if sys.platform.startswith('win'):
    SYSTEM=1
else:
    SYSTEM=0

def isWin():
    global SYSTEM
    return SYSTEM==1

def isUnx():
    global SYSTEM
    return SYSTEM==0

def isPlatForm(platform):
    if platform=='win':
        return isWin()
    elif platform=='unx':
        return isUnx()
    return False

def getPlatForm():
    global SYSTEM
    return SYSTEM

def getPlatFormImpSuf():
    global SYSTEM
    return MAP_PLATFORM_IMP_SUF.get(SYSTEM,'')

from vScreen import getScreenDict

def LimitWindowToScreen(iX,iY,iWidth,iHeight):
    try:
        dScreens=getScreenDict()
        if dScreens is None:
            iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iXa,iYa=0,0
            iXe,iYe=iMaxW,iMaxH
        else:
            dScr=dScreens[None]
            iXa=dScr.get('iXa',0)
            iYa=dScr.get('iYa',0)
            iXe=dScr.get('iXe',0)
            iYe=dScr.get('iYe',0)
            iMaxW=iXe-iXa
            iMaxH=iYe-iYa
        if iX>iMaxW:
            iX=iMaxW-iWidth
        if iY>iMaxH:
            iY=iMaxH-iHeight
        if iX<iXa:
            iX=0
        if iY<iYa:
            iY=0
        if iX+iWidth>iXe:
            iX=iXe-iWidth
        if iY+iHeight>iYe:
            iY=iYe-iHeight
        if iX<iXa:
            iX=iXa
        if iY<iYa:
            iY=iYa
        if iX+iWidth>iXe:
            iWidth=iMaxW-iX
        if iY+iHeight>iMaxH:
            iHeight=iMaxH-iY
    except:
        iX=iY=0
        iWidth=100
        iHeight=100
    return iX,iY,iWidth,iHeight

def ArrangeWidget(widBase,widSub,how):
    """
    how = 0     ... bottom left of widBase
    """
    iBaseX,iBaseY = widBase.ClientToScreen( (0,0) )
    iBaseW,iBaseH =  widBase.GetSize()
    iSubX,iSubY=widSub.GetPosition()
    iSubW,iSubH=widSub.GetSize()
    if how==0:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    elif how==10:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX+iBaseW,iBaseY,iSubW,iBaseH)
    else:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    widSub.Move((iX,iY))
    widSub.SetSize((iW,iH))

def getDefaultLocalDN():
    try:
        sDN=getUsrCfgDN('VIDARC')
        return sDN
        if platform.system()=='Windows':
            sDN=os.getenv('APPDATA')
            if len(sDN)>0:
                return os.path.join(sDN,'VIDARC')
    except:
        pass
    return os.path.join(os.path.expanduser('~'),'VIDARC')

def setUsrCfgDN(sDN):
    global gUsrCfgDN
    gUsrCfgDN=sDN

def getUsrCfgDN(sAppl=None):
    global gUsrCfgDN
    if gUsrCfgDN is None:
        try:
            if sys.platform.startswith('win'):
                sCfgDN=os.getenv('APPDATA')
            else:
                sCfgDN=None
        except:
            sCfgDN=None
        if sCfgDN is None:
            sCfgDN=os.path.join(os.path.expanduser('~'),'VIDARC')
    else:
        sCfgDN=gUsrCfgDN
    #sCfgDN=wx.StandardPaths.Get().GetUserConfigDir()
    if sAppl is not None:
        sDN=os.path.join(sCfgDN,sAppl)
        try:
            os.makedirs(sDN)
        except:
            pass
        return sDN
    return sCfgDN

def getPlugInDN():
    return getUsrCfgDN('VIDARCplugins')

def getPlugInLocaleDN():
    return os.path.join(getPlugInDN(),'locale')

def getPlugInHelpDN():
    return os.path.join(getPlugInDN(),'help')

def getLogDN():
    sDN=os.getenv('vidarcLogDN',None)
    if sDN is None:
        return getUsrCfgDN('VIDARClog')
    else:
        try:
            os.makedirs(sDN)
        except:
            pass
    return sDN

def getErrFN(sAppl):
    sDN=getLogDN()
    sLogFN=os.path.join(sDN,u'.'.join([sAppl,'err.log']))
    return sLogFN

def getHost():
    try:
        return wx.GetHostName().lower()
    except:
        return 'localhost'
    
def getHostFull():
    return wx.GetFullHostName()
