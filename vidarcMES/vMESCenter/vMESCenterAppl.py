#!/usr/bin/env python
#Boa:App:BoaApp
#----------------------------------------------------------------------------
# Name:         vMESCenterAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vMESCenterAppl.py,v 1.16 2008/03/22 14:38:07 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback,time,datetime,thread,threading
import wx
import vSystem

from __init__ import *
import vtLogDef
import vtLgBase
import vProfiler
import vSplashFrame as vSplashFrame
import images_splash as images_splash

modules ={
 u'vMESCenterParentFrame': [0, '', u'vMESCenterParentFrame.py']}

class TrackingEvtHandler(wx.EvtHandler):
    def ProcessEvent(self,event):
        try:
            print event
        except:
            traceback.print_exc()
        return False
class BoaApp(wx.App):
#class BoaApp(wx.PyApp):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort,localDN,sXmlLib):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        self.localDN=localDN
        self.sXmlLib=sXmlLib
        #self.lang=vtLgBase.getApplLang()
        #sys.stderr.write('\n\nappl:'+self.lang+'\n')
        #wx.App.__init__(self,num)
        wx.App.__init__(self,False)
        #wx.PyApp.__init__(self)
    def RedirectStdio(self, filename=None):
        """Redirect sys.stdout and sys.stderr to a file or a popup window."""
        if filename:
            sys.stderr = open(filename, 'w')
            sys.stdout = open(filename.replace('.err.','.out.'), 'w')
            #if self.iLogLv==vtLogDef.DEBUG or self.iLogLv==vtLogDef.INFO:
            #    sys.stdout = self.outputWindowClass()
            #else:
            #    sys.stdout = sys.stderr
        else:
            self.stdioWin = self.outputWindowClass()
            sys.stdout = sys.stderr = self.stdioWin
    def OnInit(self):
        #wx.InitAllImageHandlers()
        self.RedirectStdio(vSystem.getErrFN(self.sAppl))
        optshort,optlong='l:f:c:hv',['lang=','file=','config=','local=','log=','help','profile=','verbose']
        vtLgBase.initAppl(optshort,optlong,'vMESCenter')
        lang=vtLgBase.getApplLang()
        
        if 1==0:
            vtLog.vtLngInit(self.sAppl,'%s.log'%self.sAppl,self.iLogLv,iSockPort=self.iSockPort)
            self.main = vMESCenterParentFrame.create(None)
            self.main.OpenCfgFile(self.cfgFN)
            self.main.Show()
            self.SetTopWindow(self.main)
        else:
            actionList=[]
            actionListProc=[]
            import __init__
            if __init__.VIDARC_IMPORT:
                d={'label':_(u'  import library ...'),
                    'eval':'__import__("vidImp")'}
                actionList.append(d)
            else:
                d={'label':_(_(u'  import library ...')),
                    'eval':'0'}
                actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc',
                'eval':'__import__("vidarc",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool',
                'eval':'__import__("vidarc.tool",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.config',
                'eval':'__import__("vidarc.config",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool.lang',
                'eval':'__import__("vidarc.tool.lang",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool.InOut',
                'eval':'__import__("vidarc.tool.InOut",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool.art',
                'eval':'__import__("vidarc.tool.art",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool.time',
                'eval':'__import__("vidarc.tool.time",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool.sec',
                'eval':'__import__("vidarc.tool.sec",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            d={'label':_(u'  import %s ...')%'vidarc.tool.xml',
                'eval':'__import__("vidarc.tool.xml",globals(),locals(),["vidarc"])'}
            actionList.append(d)
            if 0:
                d={'label':_(u'  import %s ...')%'vidarc.tool.draw',
                    'eval':'__import__("vidarc.tool.draw",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.tool.report',
                    'eval':'__import__("vidarc.tool.report",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.tool.input',
                    'eval':'__import__("vidarc.tool.input",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.tool.office',
                    'eval':'__import__("vidarc.tool.office",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.tool.loc',
                    'eval':'__import__("vidarc.tool.loc",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.ext',
                    'eval':'__import__("vidarc.ext",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.ext.state',
                    'eval':'__import__("vidarc.ext.state",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.vApps',
                    'eval':'__import__("vidarc.vApps",globals(),locals(),["vidarc"])'}
                actionList.append(d)
                d={'label':_(u'  import %s ...')%'vidarc.vApps.vHum',
                    'eval':'__import__("vidarc.vApps.vHum",globals(),locals(),["vidarc"])'}
                actionList.append(d)
            
            d={'label':_(u'  import vMES Center ...'),
                'eval':'__import__("vMESCenterParentFrame")'}
            actionList.append(d)
            d={'label':_(u'  Create vMES Center ...'),
                #'eval':'self.res[1].create'}
                'eval':'self.res[-1].create'}
            actionList.append(d)
            d={'label':_(u'  Create vMES Center ...'),
                #'eval':'self.res[2](None)'}
                'eval':'self.res[-1](None)'}
            actionList.append(d)
            
            d={'label':_(u'  Finished.')}
            actionList.append(d)
            #print len(actionList)
            self.iRes=len(actionList)-2
            #d={'label':_(u'  Set Local Directory...'),
                #'eval':'self.res[3].OpenFile("%s")'%self.fn}
            #    'eval':'self.res[%d].SetLocalDN("%s")'%(self.iRes,self.localDN)}
            #actionListProc.append(d)
            
            #d={'label':_(u'  Open Config...'),
                #'eval':'self.res[3].OpenCfgFile("%s")'%self.cfgFN}
            #    'eval':'self.res[%d].OpenCfgFile("%s")'%(self.iRes,self.cfgFN)}
            #actionListProc.append(d)
            
            #d={'label':_(u'  Open File...'),
                #'eval':'self.res[3].OpenFile("%s")'%self.fn}
            #    'eval':'self.res[%d].OpenFile("%s")'%(self.iRes,self.fn)}
            #actionListProc.append(d)
            
            self.splash = vSplashFrame.create(None,'MES','Center',
                images_splash.getSplashBitmap(),
                actionList,
                actionListProc,
                self.sAppl,
                self.iLogLv,
                self.iSockPort,
                self.localDN)
            vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
            vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
            vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
            vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
            self.splash.Show()
        return True
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[self.iRes]
        sLogin,sPasswd,oCrypto,oCryptoStore=self.splash.GetLoginInfo()
        self.splash.Show(False)
        try:
            import vidarc.tool.log.vtLog as vtLog
            vtLog.vtLngSetLevel(self.splash.GetLogLv())
            vtLogDef.LEVEL=self.splash.GetLogLv()
        except:
            pass
        #self.SetTopWindow(self.main)
        #try:
        #    self.main.PushEventHandler(TrackingEvtHandler())
        #except:
        #    traceback.print_exc()
        try:
            wx.CallAfter(self.SetTopWindow,self.main)
        except:
            traceback.print_exc()
        try:
            wx.CallAfter(vtLog.InitMsgWid)
        except:
            traceback.print_exc()
        try:
            wx.CallAfter(self.main.Show)
        except:
            traceback.print_exc()
        try:
            self.main.SetLocalDN(self.localDN)
            self.main.SetXmlLib(self.sXmlLib)
            self.main.OpenCfgFile(self.cfgFN)
            self.main.SetLogin(sLogin,sPasswd,oCrypto,oCryptoStore)
        except:
            traceback.print_exc()
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        try:
            self.main=self.splash.res[self.iRes]
            self.main.Destroy()
        except:
            pass
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnExceptionInMainLoop(self):
        sys.stderr.write('fatal exception\n')
        sys.stderr.write(traceback.format_exc())
        return False
    def OnFatalException(self):
        sys.stderr.write('fatal exception\n')
        sys.stderr.write(traceback.format_exc())
    def ProcessEvent(self,evt):
        try:
            print evt
        except:
            traceback.print_exc()
        return wx.App.ProcessEvent(self,evt)
    def ProcessMessage(self,msg):
        try:
            print msg
        except:
            traceback.print_exc()
        return wx.App.ProcessMessage(self,msg)
    def PreTranslateMessage(self,msg):
        try:
            print msg
        except:
            traceback.print_exc()
        return wx.App.PreTranslateMessage(self,msg)
    def FilterEvent(self,evt):
        try:
            print evt
        except:
            traceback.print_exc()
        return wx.App.FilterEvent(self,evt)
    def ProcessPendingEvents(self,*args, **kwargs):
        try:
            print args,kwargs
        except:
            traceback.print_exc()
        return wx.App.FilterEvent(self,*args,**kwargs)
        

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ",_("--local <directory>")
    print "        ",_("specify local <directory>")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    print "  ",_("--xmllib <default xml library>")
    print "       ",_("LibXml2 = full compliance")
    print "       ",_("ElementTree = peformance")
    
def main():
    optshort,optlong='l:f:c:hv',['lang=','file=','config=','local=','log=','help','profile=','verbose',
                'xmllib=']
    #vtLgBase.initAppl(optshort,optlong,'vMESCenter')
    #lang=vtLgBase.getApplLang()
    #sys.stderr.write('\n\nappl:'+lang+'\n')
    fn=None
    cfgFN='vMESCfg.xml'
    #localDN=os.path.expanduser('~')
    localDN=vSystem.getDefaultLocalDN()
    iLogLv=vtLogDef.ERROR
    #iLogLv=vtLogDef.DEBUG
    #prof=None
    prof=False
    sXmlLib=None
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                vtLgBase.initAppl(optshort,optlong,'vMESCenter')
                lang=vtLgBase.getApplLang()
                showHelp()
                return
            if o[0] in ['--verbose','-v']:
                vtLogDef.VERBOSE=True
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--local']:
                localDN=o[1]
            if o[0] in ['--xmllib']:
                sXmlLib=o[1]
            if o[0] in ['--profile']:
                #prof = hotshot.Profile("vMESCenter.prof")
                prof=True
                prfLv=1
                try:
                    if len(o)>1:
                        prfLv=int(o[1])
                    else:
                        prfLv=1
                except:
                    prfLv=0
                if prfLv<0:
                    prfLv=0
                vProfiler.profile_on('vMESCenter',prfLv)
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL
    except:
        vtLgBase.initAppl(optshort,optlong,'vMESCenter')
        lang=vtLgBase.getApplLang()
        showHelp()
        traceback.print_exc()
    try:
        sTmpDN,sTmpFN=os.path.split(cfgFN)
        if sTmpDN is None:
            cfgFN=os.path.join(localDN,cfgFN)
        if len(sTmpDN)==0:
            cfgFN=os.path.join(localDN,cfgFN)
    except:
        traceback.print_exc()
    vtLogDef.LEVEL=iLogLv
    def runAppl():
        application = BoaApp(0,'vMESCenter',cfgFN,fn,iLogLv,60099,
                localDN,sXmlLib)
        application.MainLoop()
    #if prof is None:
    try:
        os.makedirs(localDN)
    except:
        pass
    os.chdir(localDN)
    if prof:
        runAppl()
        vProfiler.profile_off()
        vProfiler.profile_close()
    else:
        #tRet = prof.runcall(runAppl)
        #prof.close()
        runAppl()

if __name__ == '__main__':
    main()
