#!/usr/bin/env python
#----------------------------------------------------------------------------
# Name:         vAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20070411
# CVS-ID:       $Id: vAppl.py,v 1.2 2007/07/30 20:39:14 wal Exp $
# Copyright:    (c) 2007 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback,time,datetime,thread,threading
from optparse import OptionParser

from collections import deque
try:
    from resource import getrusage, RUSAGE_SELF
except ImportError:
    RUSAGE_SELF = 0
    def getrusage(who=0):
        return [0.0, 0.0] # on non-UNIX platforms cpu_time always 0.0

p_stats = None
p_start_time = None
p_file_callstack = None
p_file_profile = None
p_zDT = None
p_level=-1
def profiler(frame, event, arg):
    if event not in ('call','return'): return profiler
    #### gather stats ####
    rusage = getrusage(RUSAGE_SELF)
    t_cpu = rusage[0] + rusage[1] # user time + system time
    code = frame.f_code 
    fun = (code.co_name, code.co_filename, code.co_firstlineno)
    #print event,fun
    #### get stack with functions entry stats ####
    ct = threading.currentThread()
    #try:
    #    sThdIdent=str(thread.get_ident())
    #except:
    #    sThdIdent='main'
    try:
        p_stack = ct.p_stack
    except AttributeError:
        ct.p_stack = deque()
        p_stack = ct.p_stack
    #### handle call and return ####
    if p_file_callstack is not None:
        if event == 'call':
            act_frame=frame
            zNow=p_zDT.now()
            sNow='|'.join([zNow.isoformat(),'CS','DEBUG'])
            sFN=os.path.split(code.co_filename)[1]
            p_file_callstack.write(sNow)
            p_file_callstack.write('|%s:%s(%04d);%s|'%(
                    sFN,code.co_name, code.co_firstlineno,event))
            i=0
            #print event
            while (i<p_level) and (act_frame is not None):
                code=act_frame.f_code
                sFN=os.path.split(code.co_filename)[1]
                p_file_callstack.write('%s:%s(%d),'%(sFN,code.co_name,code.co_firstlineno))
                #print '    %03d name:%-30s fn:%-80s ln:%04d'%(i,code.co_name,code.co_filename,code.co_firstlineno)
                act_frame=act_frame.f_back
                i+=1
            #print
            p_file_callstack.write('\n')
    if event == 'call':
        #p_stack.append((time.time(), t_cpu, fun))
        p_stack.append((p_zDT.now(), t_cpu, fun))
    elif event == 'return':
        try:
            t,t_cpu_prev,f = p_stack.pop()
            assert f == fun
        except IndexError: # TODO investigate
            t,t_cpu_prev,f = p_start_time, 0.0, None
        call_cnt, t_sum, t_cpu_sum = p_stats.get(fun, (0, 0.0, 0.0))
        zNow=p_zDT.now()
        zDiff=zNow-t
        fDiff=zDiff.seconds + zDiff.microseconds/1.0e6
        #p_stats[fun] = (call_cnt+1, t_sum+time.time()-t, t_cpu_sum+t_cpu-t_cpu_prev)
        p_stats[fun] = (call_cnt+1, t_sum+fDiff, t_cpu_sum+t_cpu-t_cpu_prev)
    if p_file_callstack is not None:
        if event == 'return':
            act_frame=frame
            zNow=p_zDT.now()
            sNow='|'.join([zNow.isoformat(),'CS','DEBUG'])
            #sNow=time.strftime('%Y-%m-%d %H:%M:%S|CS|DEBUG|',time.localtime(time.time()))
            sFN=os.path.split(code.co_filename)[1]
            p_file_callstack.write(sNow)
            p_file_callstack.write('|%s:%s(%04d);%s|'%(
                    sFN, code.co_name, code.co_firstlineno,event))
            p_file_callstack.write('%s:%s(%d)\n'%(sFN,code.co_name,code.co_firstlineno))
    return profiler


def profile_on(fn,lv):
    global p_stats, p_start_time , p_file_callstack , p_file_profile
    global p_level,p_zDT
    p_level=lv
    p_zDT=datetime.datetime(2007,1,15)
    sNow=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
    
    try:
        # search enviroment
        sLogDN=os.getenv('vidarcLogDN',None)
    except:
        sLogDN=None
    sCallstackFN='%s.%s.cs.log'%(fn,sNow)
    
    sProfFN='%s.%s.prof'%(fn,sNow)
    if sLogDN is not None:
        try:
            os.makedirs(sLogDN)
        except:
            pass
        lngCallstackFN=os.path.join(sLogDN,sCallstackFN)
        lngProfFN=os.path.join(sLogDN,sProfFN)
    else:
        lngCallstackFN=sCallstackFN
        lngProfFN=sProfFN
    if lv>0:
        try:
            p_file_callstack=open(lngCallstackFN,'w')
        except:
            p_file_callstack=None
    else:
        p_file_callstack=None
    try:
        p_file_profile=open(lngProfFN,'w')
    except:
        p_file_profile=None
    p_stats = {}
    p_start_time = p_zDT.now()      #time.time()
    threading.setprofile(profiler)
    sys.setprofile(profiler)


def profile_off():
    threading.setprofile(None)
    sys.setprofile(None)

def get_profile_stats():
    """
    returns dict[function_tuple] -> stats_tuple
    where
      function_tuple = (function_name, filename, lineno)
      stats_tuple = (call_cnt, real_time, cpu_time)
    """
    return p_stats

import vtLogDef
import vtLgBase

OPTSHORT,OPTLONG='l:hv',['lang=','log=','help','profile=','verbose']

def main(sAppl='vAppl',optshort='',optlong=[],func=None,*args,**kwargs):
    if func is None:
        return
    optshort=optshort+OPTSHORT
    optlong=optlong+OPTLONG
    iLogLv=vtLogDef.ERROR
    prof=False
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                vtLgBase.initAppl(optshort,optlong,sAppl)
                lang=vtLgBase.getApplLang()
                showHelp()
                return
            if o[0] in ['--verbose','-v']:
                vtLogDef.VERBOSE=True
            if o[0] in ['--profile']:
                #prof = hotshot.Profile("vMESCenter.prof")
                prof=True
                prfLv=1
                try:
                    if len(o)>1:
                        prfLv=int(o[1])
                    else:
                        prfLv=1
                except:
                    prfLv=0
                if prfLv<0:
                    prfLv=0
                profile_on(sAppl,prfLv)
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL
    except:
        vtLgBase.initAppl(optshort,optlong,sAppl)
        lang=vtLgBase.getApplLang()
        showHelp()
        traceback.print_exc()
    
    if prof:
        func(optshort,optlong,*args,**kwargs)
        profile_off()
        if p_file_callstack is not None:
            p_file_callstack.close()
        if p_file_profile is not None:
            for tFunc,tStat in get_profile_stats().items():
                p_file_profile.write('%s;%s;%d;call:%d;zReal:%f;zCPU:%f'%(tFunc[1],
                    tFunc[0],tFunc[2],tStat[0],tStat[1],tStat[2]))
                p_file_profile.write('\n')
            p_file_profile.close()
    else:
        func(optshort,optlong,*args,**kwargs)
