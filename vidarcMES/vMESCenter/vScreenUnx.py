# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vScreenWin.py
# Purpose:      screen functions for Windows Platform
#
# Author:       Walter Obweger
#
# Created:      20100226
# CVS-ID:       $Id: vScreenUnx.py,v 1.1 2016/02/06 14:03:38 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import sys

import vLogFallBack

vScreenIfc=-1   #screen interface not found 

try:
    from gi.repository import Gdk
    vScreenIfc=1     # screen interface found =gtk
    
    def getScreenDict(sScreen=None,thd=None):
        bLoop=True
        iMode=0
        dRes={'taskbar':None}
        if 0:   # check display present
            dTask=None
        else:
            dTask={}
            dTask['x']=0
            dTask['y']=0
            dTask['width']=screen.get_width()
            dTask['height']=screen.get_height()
            dTask['iXa']=dTask['x']
            dTask['iYa']=dTask['y']
            dTask['iXe']=dTask['width']
            dTask['iYe']=dTask['height']
            dTask['edges']=[
                (dTask['x'],dTask['y']),
                (dTask['x']+dTask['width'],dTask['y']),
                (dTask['x']+dTask['width'],dTask['y']+dTask['height']),
                (dTask['x'],dTask['y']+dTask['height']),
                
                ]
            iScr=0
            iIdx=0

        dRes['taskbar']=dTask
        return dRes
except:
    vLogFallBack.logTB(__name__)
