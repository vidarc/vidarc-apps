# vidarc-apps

vidarc-apps is a pure python package.

## Dependencies

Python 3 is required
use of anaconda recommended.

packages used:

+ [pandas][pandas]
+ [pysimplegui][pysimplegui]
+ [wx][wx]

## features

+ [vMESClient](#vMESClient)
+ [vMESSrv](#vMESSrv)

## install

Package is published at [pypi](https://pypi.org/project/lindworm).

```shell
pip install lindworm
```

```shell
python setup.py install
```

## vMESClient{#vMESClient}

:construction:

## vMESSrv{#vMESSrv}

:construction:
