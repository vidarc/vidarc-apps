#----------------------------------------------------------------------------
# Name:         vGuiSplashFrmWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20121201
# CVS-ID:       $Id: vGuiSplashFrmWX.py,v 1.4 2015/04/27 06:48:18 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    from wx.lib.anchors import LayoutAnchors
    import cStringIO
    import types
    
    import imgCore
    import imgSplash
    import imgStat
    import vLang as vtLgBase
    from vGuiFrmCoreWX import vGuiFrmCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    from vGuiCoreWX import vGuiCoreGetIcon
    from vGuiCoreWX import vGuiCoreTopWidReg
    from vGuiPanelWX import vGuiPanelWX
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

import images
import vtLogDef

def getLogoData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\xff\x00\x00\x001\x08\x06\
\x00\x00\x00\x15\xea\x7f\x0e\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\
\x00\x00\x04\xdcIDATx\x9c\xed\x9dOn\xd3@\x14\x87\xc7I\x01\xd1\xb2Be\x87\x04b\
I\xa5J\xbd\t]\xe5"U\xb6\xa8\xdb\xaa\'@\\ \xb7\x80\x13T\xca\x01({$v\xb0"1\x8b\
hR\xcb\xb1\xe7\xefof\xde\xc4\xbfOb\x81\xecx>\xbf\xf8\xbd\x19{&n\xd3\xcc\xe6\
\nA\xbb\xdd\xb4\xa1\x9fmf\xf3\x06"a\x81\x8e\x18\xe8\x88\xa1\xb4\xe3I\xec\x01\
\xba\\\xde-\xbc?\xb3^\xae\x90\nV\xe8\x88\x81\x8e\x18J:\xce GQ\xbbJ\xe4+\xb5^\
\xae\xb2UY\xa5\xe8\x88\x82\x8e\x18J;\xc2\x92\x9f\x10R\x17\xd0\xe4\xf7\xa9d\
\xb9\xab\xac\x86\x8e\x18\xe8\x88\xa1\xa4#{~B&\n<\xf9]*Y\xa9*\xab\xa1#\x06:b(\
\xe5h}\xda\x1f3\x1d\x81>\xee\xd0\xc9\xa7\xf2\x0b=6\x1d\x0f\xa1#\x06\xb4cc\
\x9b\xe7o\xb7\x9b6d:\x02M\xbf\xf2\xe9\x13\xb8\xbc[\xa8\xf5r\x154e\x82\x86\
\x8e\x18\xe8\x88\xc1\xe6\x08\x9d\xe7\xcf\x85\x94\x82d\x82\x8e\x18\xe8\x88a\
\xc8\xb1\xaa\xe4\xefV.\xa9\xd0\x11\x03\x1d1\x98\x1c\xabJ~\xc9A\xd6\xd0\x11\
\x03\x1d1\x98\x1c\xab\x99\xea\xab=\xd0R\xa0#\x86cp\xac&\xf9\t!X\xa2\x86\xfd\
\x0f7n+\x93\xae\xee\x17\xce\xfb\xeb}S\xe2\xea\xad\x94<\x9f>9\xfc\x86\x08qN\
\xedZc\x1c56\xf7\xae_\x7f\xdfPw\xf6\xfc\x95\xf3p\xb3\x8a\xba\xe8\xc9\x8e\x92\
qti7\x85\x1f\x93\xffH`\x01\xc0\x903\x8e!\t\x8d\xf4\x9b)\x95vU\x12\x8a\xdc\
\xbf\xb3\x0e\xa1\xb4\xa3\xcb\x85Q\xda\xd1\x85)8\xe6(26\xc7\xfd=\xff~\xc9\x9f\
\xc7\xfaa\xd3}H\x7f{\xf7\xff!\xf7,g\xef\xcf\xf7\'\x13\xfb\xa4\xd5\xd7\xdb\
\x87\x10G\xdb3\x91!\x9f\xb1}\x1fnVV\x7fD\x1c]\x9f\xe3\x84\xc62&\x8ec^\xaeqt\
\x89a\xa8\xe3X\x9b\x1aS\xbb!\x05\xc3\xe4\xb8O~\xbdq\xbd\\y\x17\x811\\\x83\
\xe8B\xf3l\xdeuTJ\xc9\x9cn\xc9\xe5xu\xbf\x08\xee=\x18\xc7\'$\xc5\xd1\x96+!\
\xae:\x8fu^w\x1d\x0f\x9e\xf6\xf7\x8b\x80+\xaeb\x88\'\x95\xfd\xa0K$\x87\xe3X\
\xcc]\x8bn\n\xc7\xbeSl\x070\x95\xef\xda5F\xa1\xc5\xaa_\x04\x942<\xf0\xbb\xbc\
[\x88\xec\x11\xba\xd0\x11C\xa8#j\xca\xc9\x85TqD\xde{\xbb8"\xda\x8b\x89s3\x9b\
7\xba\x10X\xe7\xf9c\x03\x8e\x1c\xfa\x8f!=\xb9\x94\x929l\xed\x93\xca\x11y\r\
\xc48\xfa\xaeK\t\xc5\xd71\xa4\xbdX\xc7f6o\xa0S}6\xa1\x9c=\x05!!L\xe9\x9a\xcc\
2\xcf\xcf9\xe8\xe3\xc3T\xc8kM\xa0Z\xbdC\xa9\xeaW}D.\xb6\x02\x9f\xe3\xf6/\x06\
\xc9n\xa9\x80\xf7\xfc>O-\t\xc9\xc9\xd5\xfdb\xff\xaffP#ik\xf2\xa3\xa6XR\x0e\
\xfd%O\x03iR9"\xe3\xea\xeaX\xf26\xee\x98\xe2\x18\xd2\xb6\xde7v\xad\x7f\xbb\
\xdd\xb4\xa3\xc3\xfe)\'\x14\x92)8\x8e\xf5\xa4\xfd\x8b3f\xe8\xaf\x1dO\xdf\xbe\
\x0e\xfa|\x1f\xe4,\x89\xc6%\x8e\xe8v}c\xaaW\xf2\x9e\xbe;?\xbc\xe7\xd7\'\xa0\
\xe7\x02C\xd6\xfd\xdbN0v\xd8\xd5_Q%1\xc1r8\xfa,\x07\x1e\xa2\xc68\xfe\xf8\xf2\
-Y[\xa1\xc5\t\x11G\x97\xb6c{z\xa5\x9e\x1c\x1f\xbf~\x7fJ\xfe~\xd2K\xa3\xfd\
\xb7\x15\xbd\x1cU\x93\xcb1\xe6B\x88q\xcc5]\x9b#\x8eC\x9d\x94O\x01@;\x9a\xda\
\x0e-\xf4N\xef\xf0\x93\x9a\xf4\x9a?\x8f\xbf\xa0Av\xdd\xee{q#\x12\xcaw{\x17\
\x9fe\xbd>\x98~H\xd4o\xdb\xe4\xeb\x1a\xdb\xd4qt\xfd\x01\r\xdaQ\x1f\xd3%\x9e.\
\xc7\xb1a}\x87_\x8a\xc4\x1f\x13\x0b\xed)$\xf7\xf6\x9a\xd2\x8e\xa9\x12?79\x1d\
K]\x8f\xb1#&\xc4w\xcd\x97y\x1c\t\xb5O_M\x91\xd0\xef\x0c\xf5]3\xf9\x8f\x00&~\
\x1c%\xe3\xe7\xb3\xee\x00\xbdF!\xe9\n?\xa9\x17\xa54/i>C\xf88&=\x1f\xc3\rjL\
\xbb\xa5\xbf\x83\x12\xed\xb3\xe7\'d\xa2T\x93\xfc\x12\xe7\xa0\xfb\xd0\x11\x03\
\x1d1\xd8\x1c\xabI~\xa5v\'#=\xe8t\xc4@G\x0c&\xc7\xaa\x92_\xbf\x85Dr\xc0\xe9\
\x88\x81\x8e\x18L\x8eU%\xbfF\x9f\x8c\xf4\xa0\xd31\x1e:b\x18rl\x9a\xd9\xdc\
\xf8\xa1\x98w\xfa\x0f-2\x88\t\xd0\xd0b\xa4\xd8\xbf9@\xc7\x1dt\xf4G\xba\xe3\
\xd9\x877\xea\xef\xcf\xdf\xa3\x8e\xd6\xe4\x0f\xa1\xddnZ\xd3\xea\xa2\xf5rU|9q\
-\x8e\x17\xb7\xd7j\xfe\xf2\xf9\xe0v)\x8e5\xc4\x91\x8e\x87T9\xec\x9f\x12c\x89\
OH,\xf0\xe4\xb7U0\xa5vC\x9b\x92\x7f"\xac\x16\xc7\x8b\xdbk\xe3>\x12\x1ck\x88#\
\x1d\x87a\xcf/\x18\xf6\xfa$%\xd0\xe4w\xa9`\x9aR\xd5\xb6\x16\xc7\x8f\x9f?9\
\xed\xcb8\x8eCG3|{\xafPN^\xbd(\xad@\x8e\x1c\xd8\xd3\xfe\x98\x8a\x94\xebI+\
\x1d1\xd0\x11Ci\xc7\xff\xeey`\x1ae\x99*\xfc\x00\x00\x00\x00IEND\xaeB`\x82' 

def getLogoBitmap():
    return wx.BitmapFromImage(getLogoImage())
def getLogoImage():
    stream = cStringIO.StringIO(getLogoData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getLaunchData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00 \x00\x00\x00 \x08\x06\x00\
\x00\x00szz\xf4\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\x00\x027ID\
ATX\x85\xbd\x96=r\xa30\x18\x86\x1fa\x9fB]\xc69\xc7\x96)\xe87\xb5\xdb\r\x07\
\xd8]\x0e\xe0$\x07\x08\xb9CR\x87\x99\xf8\x14>\x80\xdd\xfa\x16\xf0m\x01\x92?\
\xc4\x8f1\xc6\xfb\xceh@\x12\xf0|\x7f\x920&Z\xf0?$e!\xbao\xa2\x85\x01\x88n\ru\
\r\xc0ZK\x9a\xa6\x8dg\xcc\xad"\xa0=\xb6\xd6\xb2^\xaf\xd9\xedv\xe4y^\x81\xeb\
\x08`\xa2\xc5\xac\r\x10@\xbe\xebk\x1c\xc7\x92\xa6\xa9Xk\xc5\xcd\x01\xe2\xdf\
\x993\x02R\x16\xf2\r\xac\xea\xfeJ\x84\x831\xfc\xb0\x96\xe3\xf1\xe8\x9f\xf3\
\xde3c\nZp\xe0\xa0\xe6\xef\xd5\xbd6`\x96"\xec\x82#\xd2\xff\x82\xd2\xd5\x06\
\xf4\xc1\x0f\xc6;\xc9\x16\xc8\xea{\xed\xfd,\x06x\xa8\xb7\xa8\x82\xbb\xb1m8\
\x1f\xe8*\x03\xa4,d\xaf\rQ\xf0\x83\x82\x1f\x80\'\xda\xde_e\xc098\xb4\x0b\xb1\
K\xcb[\xc1\xb7j\xbe\xcf{\xb82\x05+\x05\'\x80\xbb\xbc\x0f\xc1\'\x19\xd0\xf0>(\
8\r\x1f\xca\xbbVo\n\xc2\xd3+T_\xd8/\x81\xf7\x1a e!\x92$\x8d\xb1\xf7\xb77\x9e\
\x80=\xb0J\x12\x8c1\xeckXXtc\xe1@\xfb0\x02d_\xedc\xb2\x07\xc9\xea\xe6\xc7\
\x93\xa4\xd1\xcf\xea\x83G?w\xd1\xe15\x15\x9e\x05\x8d\t\xf0\x86\x01\xa8\x8ft\
\xb5>\xf8T\xb0kK\x95\x8aV\xce\xa4,$\x8ec\xbe\xee\xee|\rdj\xde\xd5\xc4}\xf8b\
\x87\xfe\xfe\xf9\xddY\xd4g7"\rw\xd0L]\xa7@\xb5:\xff\x07\xdc\x12\x94\x0f0\x8f\
\xa7\xf18\x8e\xfd/\x95\x83?PE@Gp\x0c\x18\xe0\xf9\xe5\xd5\xb4" e!\xf2Qw~\x9e\
\x8eu\xd3\xb1\xa8~\xd1\xdc\xeb\xc7\x82\xb5\x1a;a\x08\xf7\xfa\xac.y\x9e{O\x1f\
\xea)\xbd\xe7_\xa2\xe7\x97W\x03c\x0e\xa3\xcf*\r\xeawZ6\x9b\r\x00\xefT\xb5P\
\xcf\x8d\x0e\xbb\xee{\x03Z\xde\x7f\x0e\x7fh\xdb\x84_\x0cv\xf2E\xe8\r\x08\xe0\
\xe6\xb1]`.\x02\xe7\xe0}P\xade\x1f\xdcU\xbf\x89\x16\xa6\xab\xb8\x86\xe0c\xc0\
N\xc6D\x8b\xca\x00\xc1\xe7{\x08<\xa4K\xc0N\xa7\x9d\xd0\x9c\xc00mIM\xd1RC\xc7\
\x80\xb5\x97s\x18\xe9#\xd0\xf7\xb1\xb1a\x9d\x12~\x80\x7f\xf0\xb3a\xfc\xec\
\xfcO]\x00\x00\x00\x00IEND\xaeB`\x82' 

def getLaunchBitmap():
    return wx.BitmapFromImage(getLaunchImage())

def getLaunchImage():
    stream = cStringIO.StringIO(getLaunchData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getCancelData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00oIDAT8\x8d\xad\x93\xc1\x11\xc0 \x08\x04\x0f\xd3E\xfa\xaf\xcd6\xcc+\
\x19\xd1;p\x86\xf0fw\x80S\xb3v\xa1R\xadD\xff*\xe8\x86\xd1\r#\x03\xd6\xbeOp\
\x0f\xd8\xdb\x10\xc1s\xaf\x13d\x12\x06o\x02%Q0\x00\x98\x8aq\x9d\x82\xc1t\x02\
\x06(8\x14\xb0\x15\x8e\x05\xf3\xceY:\x9b\x80\x1d,\x928Atm%q/Q\xc1\x91D\xc6xZ\
\xe5\xcf\xf4\x00\xe0\xc8:\xc8\xd18`E\x00\x00\x00\x00IEND\xaeB`\x82' 

def getCancelBitmap():
    return wx.BitmapFromImage(getCancelImage())

def getCancelImage():
    stream = cStringIO.StringIO(getCancelData())
    return wx.ImageFromStream(stream)

#----------------------------------------------------------------------
def getApplyData():
    return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x00pIDAT8\x8dcddbf\xa0\x040\x91\xa3Ix\x89\xe0\x7f\xe1%\x82\xff\x19\x18\
\x18\x18XH\xd5\x08c\xbf\x8dy\xcfH\x92\x010\xcd0\x8d0@\x94\x17pi&\xda\x00\\\
\x9a\x892\x00\xd9\xdfd\x19@\x08\xe05\x00\x9f\xdfQ\x0c@\x8eW\x8a\\@\x8eAL0\'\
\xa2;\x93\x18\xe7c\xb8\x00\xa6\x98\x14W`\x04"\xb2\x8d\x84l\xc7j\x00\xa9\x80\
\x91\xd2\xec\x0c\x00y\x1c/\xbdxe+\x9e\x00\x00\x00\x00IEND\xaeB`\x82' 

def getApplyBitmap():
    return wx.BitmapFromImage(getApplyImage())

def getApplyImage():
    stream = cStringIO.StringIO(getApplyData())
    return wx.ImageFromStream(stream)

def create(parent,sGrp,sAppl,sVersion,img,lActPre,lActPost,sApplName,
            iLogLv,iSockPort,bVidImp=True):
    return vGuiSplashFrmWX(parent,sGrp,sAppl,sVersion,img,
                lActPre,lActPost,sApplName,iLogLv,iSockPort,bVidImp=bVidImp)

class vGuiSplashFrmWX(vGuiFrmCoreWX):
    SIZE_DFT=(100,100)
    def __init__(self, parent,sGrp,sAppl,sVersion,img,lActPre,lActPost,
                sApplName,iLogLv,iSockPort,
                bmp=None,bVidImp=True,
                autoLaunch=10,**kwargs):
        vGuiFrmCoreWX.__init__(self)
        global _
        _=vtLgBase.assignPluginLang('vGui')
        
        _args,_kwargs=self.GetGuiArgs({#'id':id,
                'parent':parent,
                #'title':title,'name':name,
                #'pos':pos,'size':size,
                'style':wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE},
                ['id','name','parent','pos','size','style','title'],{
                'name':u''.join([sAppl,'Frame']),
                'title':_(u'VIDARC loader %s')%sApplName,
                'pos':(0,0),'size':self.SIZE_DFT,'style':wx.TAB_TRAVERSAL
                })
        self.appl=_kwargs['title']
        self.__logDebug__(['args;',_args,'kwargs;',_kwargs])
        self.frm=wx.Frame(**_kwargs)
        self.SetOriginByWid(self.frm)
        self.__logDebug__(''%())
        
        if bmp is None:
            icon=vGuiCoreGetIcon(imgCore.getModBitmap())
        else:
            icon=vGuiCoreGetIcon(bmp)
        self.frm.SetIcon(icon)
        self.frm.Bind(wx.EVT_ACTIVATE, self.OnVgfSplashActivate)
        self.frm.Bind(wx.EVT_IDLE, self.OnVgfSplashIdle)
        
        lWid=[
            ('lblBmp',  (0,0),(1,1),{'name':'lblCompany','bitmap':getLogoBitmap()},None),
            ('lblCt',   (1,0),(1,1),{'name':'lblGrp','label':sGrp},None),
            ('lblCt',   (2,0),(1,1),{'name':'lblAppl','label':sAppl},None),
            ('lblCt',   (3,0),(1,1),{'name':'lblVersion','label':sVersion},None),
            ('lblBmp',  (4,0),(1,1),{'name':'lblSplash','bitmap':imgSplash.getSplashBitmap()},None),
            ('szBoxHor',(5,0),(1,1),{'name':'bxsLogFile'},[
                ('lblRg',1,4,{'name':'lblApplLogName','label':_(u'Log Name'),
                        },None),
                ('txt',3,0,{'name':'txtLogName','value':sApplName,
                        },None),
                ]),
            ('szBoxHor',(6,0),(1,1),{'name':'bxsLogSocket'},[
                ('lblRg',1,4,{'name':'lblApplLogSocket','label':_(u'Log Port'),
                        },None),
                ('txt',3,0,{'name':'txtLogPort','value':str(iSockPort),
                        },None),
                ]),
            ('szBoxHor',(7,0),(1,1),{'name':'bxsLogLevel'},[
                ('lblRg',1,4,{'name':'lblApplLogLv','label':_(u'Log Level'),
                        },None),
                ('chc',1,4,{'name':'chcLogLevel',
                        'choices':[u'debug', u'information',
                            u'warning', u'error', u'critical', u'fatal']},
                        None),
                ('lbl',  2,16,{'name':'lblFillLv0','label':''},None),
                ]),
            ('szBoxVert',(8,0),(1,1),{'name':'bxsBt',},[
                ('txtRd',   1,4,{'name':'txtStat','value':''},None),
                ('barHor',  0,9,{'name':'gagProcess','size':(-1,8)},None),
                ]),
            ('szBoxHor',(9,0),(1,1),{'name':'bxsBt',},[
                ('lbl',  2,16,{'name':'lblFill0','label':''},None),
                ('cbBmp',0,4,{'name':'cbStop','bitmap':imgStat.getExitBitmap()},
                        {'btn':self.OnStop},),
                ('lbl',  2,16,{'name':'lblFill0','label':''},None),
                ('cbBmp',0,4,{'name':'cbPause','bitmap':imgStat.getPauseBitmap()},
                        {'btn':self.OnPause},),
                ('cbBmp',0,4,{'name':'cbStart','bitmap':imgStat.getStartBitmap()},
                        {'btn':self.OnStart},),
                ('lbl',  2,16,{'name':'lblFill0','label':''},None),
                ]),
            ]
        self.pnMain = wx.Panel(id=wx.NewId(),
              name='pnMain', parent=self.frm, 
              style=wx.TAB_TRAVERSAL)
        self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)
        self.pn=vGuiPanelWX(name='pnSplash',parent=self.pnMain,
                    lGrowRow=[],
                    lGrowCol=[(0,1)],
                    lWid=lWid)
        self.pn.lblGrp.SetFont(wx.Font(16, wx.SWISS, wx.ITALIC, wx.NORMAL, False,
              u'Swis721 Blk BT'))
        self.pn.lblGrp.SetMinSize(wx.Size(-1, -1))
        self.pn.lblGrp.SetForegroundColour(wx.Colour(46, 138, 87))
        
        self.pn.lblAppl.SetFont(wx.Font(16, wx.SWISS, wx.ITALIC, wx.NORMAL, False,
              u'Swis721 Blk BT'))
        self.pn.lblAppl.SetMinSize(wx.Size(-1, -1))
        self.pn.lblAppl.SetForegroundColour(wx.Colour(46, 138, 87))
        
        self.pn.lblVersion.SetFont(wx.Font(10, wx.SWISS, wx.ITALIC, wx.NORMAL, False,
              u'Swis721 Blk BT'))
        self.pn.lblVersion.SetMinSize(wx.Size(-1, -1))
        self.pn.lblVersion.SetForegroundColour(wx.Colour(46, 138, 87))
        
        self.bxsData.AddWindow(self.pn.GetWid(), 1, border=4,
                flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND)
        self.pnMain.SetSizer(self.bxsData)
        self.bxsData.Layout()
        self.bxsData.Fit(self.frm)
        
        self.pn.cbStart.Enable(False)
        #self.cbLaunch.SetBitmapLabel(getLaunchBitmap())
        #self.cbApply.SetBitmapLabel(getApplyBitmap())
        #self.cbCancel.SetBitmapLabel(getCancelBitmap())
        #self.lblCompany.SetBitmap(getLogoBitmap())
        #self.lblPrg.SetLabel(sPrg)
        #self.lblAppl.SetLabel(sAppl)
        #if img is not None:
        #    self.lBitmap.SetBitmap(img)
        
        self.res=[]
        self.lActPre=lActPre
        self.lActPost=lActPost
        self.iAct=-2
        self.iLen=-2
        self.iAction=-2
        self.iActionPost=-2
        self.bActivated=False
        self.sLogName=sApplName
        self.iLogName=-1
        self.SetLogLv(iLogLv)
        self.zAutoLaunch=autoLaunch
        self.frm.Center()
        self.InitState()
        if bVidImp:
            self.__logDebug__('vidImp'%())
            __import__("vidImp")
    def InitState(self):
        self.oState=vpc.vcsStateFlag(self.GetOrigin(),'oState')
        self.oState.AddState(1000,'init',u'init')
        self.oState.AddState(2000,'prcPre',u'processing prepeare')
        self.oState.AddState(2090,'finPre',u'finished prepeare')
        self.oState.AddState(6000,'pause',u'pause')
        self.oState.AddState(7000,'prcPost',u'processing post')
        self.oState.AddState(7090,'finPost',u'finished post')
        self.oState.AddState(8000,'abort',u'abort')
        self.oState.AddState(9000,'launch',u'launch')
        
        self.oState.AddFlag(0x001,'rng',u'running',1)
        self.oState.AddFlag(0x002,'psd',u'paused',2)
        self.oState.AddFlag(0x008,'fin',u'finshed',10)
        self.oState.AddFlag(0x100,'pre',u'prepeare',20)
        self.oState.AddFlag(0x200,'post',u'post',21)
        self.oState.AddFlag(0x010,'finPre',u'finshed prepeare',10)
        self.oState.AddFlag(0x020,'finPost',u'finshed post',11)
        
        self.oState.StateSetFlag(2000,vpc.vcsStateFlagType_ENTER,0x001,0xf0a)
        self.oState.StateSetFlag(2090,vpc.vcsStateFlagType_ENTER,0x010,0x000)
        self.oState.StateSetFlag(6000,vpc.vcsStateFlagType_ENTER,0x002,0x009)
        self.oState.StateSetFlag(7000,vpc.vcsStateFlagType_ENTER,0x001,0x000)
        self.oState.StateSetFlag(7090,vpc.vcsStateFlagType_ENTER,0x020,0x000)
        self.oState.StateSetFlag(9000,vpc.vcsStateFlagType_ENTER,0x008,0x000)
        
        self.oState.StateAdd(2000,vpc.vcsStateFlagType_ENTER,self.DoStartPre)
        self.oState.StateAdd(2090,vpc.vcsStateFlagType_ENTER,self.DoFinPre)
        self.oState.StateAdd(7000,vpc.vcsStateFlagType_ENTER,self.DoStartPost)
        self.oState.StateAdd(7090,vpc.vcsStateFlagType_ENTER,self.DoFinPost)
        self.oState.StateAdd(9000,vpc.vcsStateFlagType_ENTER,self.prcLaunch)
        self.oState.Build()
        self.oState.SetState(1000)
    def GetWid(self):
        return self.frm
    def Destroy(self):
        try:
            self.frm.Destroy()
        except:
            self.__logTB__()
    def Show(self,bFlag=True):
        try:
            self.frm.Show(bFlag)
            if bFlag==True:
                self.frm.SetFocus()
        except:
            self.__logTB__()
    def Close(self):
        try:
            self.__logDebug__('')
            self.frm.Close()
        except:
            self.__logTB__()
    def OnPause(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnStart(self,evt):
        try:
            self.__logDebug__(''%())
            self.DoLaunch()
        except:
            self.__logTB__()
    def OnStop(self,evt):
        try:
            self.__logDebug__(''%())
            self.Post('stop')
            self.Close()
        except:
            self.__logTB__()
    def DoAction(self):
        try:
            if self.oState.IsState(2000)>0:
                self.__logDebug__('iAction:%3d-%3d'%(self.iAct,self.iLen))
                if self.iAct>=-1:
                    self.iAct=self.iAct+1
                    if self.iAct<self.iLen:
                        self.pn.gagProcess.SetValue(self.iAct+1)
                        dAction=self.lActPre[self.iAct]
                        self.pn.txtStat.SetValue(dAction['label'])
                        self.CB(self.Post,'process',self.iAct)
                    else:
                        self.oState.SetState(2090)
                        self.pn.cbStart.Enable(True)
            elif self.oState.IsState(7000)>0:
                self.__logDebug__('iAction:%3d-%3d'%(self.iAct,self.iLen))
                if self.iAct>=-1:
                    self.iAct=self.iAct+1
                    if self.iAct<self.iLen:
                        self.pn.gagProcess.SetValue(self.iAct+1)
                        dAction=self.lActPost[self.iAct]
                        self.pn.txtStat.SetValue(dAction['label'])
                        self.Post('process',self.iAct)
                    else:
                        self.oState.SetState(7090)
            else:
                self.oState.LogOrg(vpc.vLog_DEBUG,0xffff)
        except:
            vpc.logTB(__name__)
    def DoProcess(self):
        try:
            if self.oState.IsState(2000)>0:
                self.__logDebug__('iAction:%3d-%3d'%(self.iAct,self.iLen))
                if self.iAct<self.iLen:
                    dAction=self.lActPre[self.iAct]
                    self.__logDebug__('iAction:%3d-%3d;%r'%(self.iAct,self.iLen,dAction))
                    if dAction.has_key('eval'):
                        r=eval(dAction['eval'])
                        self.res.append(r)
                        self.__logDebug__('iAction:%3d-%3d;type:%r;%r'%(self.iAct,self.iLen,type(r),r))
                        if type(r)==types.InstanceType:
                            self.__logDebug__('instance found')
                            self.Post('result',r)
                    else:
                        self.res.append('')
                    self.CB(self.Post,'action',self.iAct)
            elif self.oState.IsState(7000)>0:
                self.__logDebug__('iAction:%3d-%3d'%(self.iAct,self.iLen))
                if self.iAct<self.iLen:
                    dAction=self.lActPost[self.iAct]
                    if dAction.has_key('eval'):
                        self.res.append(eval(dAction['eval']))
                    else:
                        self.res.append('')
                    self.CB(self.Post,'action',self.iAct)
        except:
            vpc.logTB(__name__)
    def Notify(self):
        self.zAuto+=1
        if self.zAutoLaunch<0:
            self.timer.Stop()
            return
        if self.zAuto>self.zAutoLaunch:
            self.oState.SetState(9000)
        else:
            self.pn.gagProcess.SetValue(self.zAuto)
    def OnVgfSplashActivate(self, event):
        event.Skip()
    def OnVgfSplashIdle(self, event):
        try:
            event.Skip()
            self.oState.LogOrg(vpc.vLog_DEBUG,0xffff)
            if self.oState.IsState(1000)>0:
                if self.oState.IsFlag(0x10)==0:
                    if self.GetSockPort() is None:
                        self.__showBasicSettings__()
                        self.pn.txtStat.SetValue(_(u'Please setup logging port (%s).')%'Port')
                        return
                    try:
                        sLogName=self.pn.txtLogName.GetValue()
                        import vidarc.tool.log.vtLog as vtLog
                        iRes=vtLog.vtLngInit(sLogName,sLogName+'.log',
                                self.GetLogLv(),
                                iSockPort=self.GetSockPort())
                        if iRes==-1:
                            self.__showBasicSettings__()
                            self.pn.txtStat.SetValue(_(u'Please change logging name (%s).')%'Log Name')
                            return
                        if iRes==-2:
                            self.__showBasicSettings__()
                            self.pn.txtStat.SetValue(_(u'Please change logging port (%s).')%'Port')
                            return
                    except:
                        vpc.logTB(__name__)
                        return
                else:
                    return
                self.iAction=-1
                self.oState.SetState(2000)
        except:
            vpc.logTB(__name__)
    def DoStartPre(self):
        try:
            self.__logDebug__(''%())
            self.iAct=-1
            self.iLen=len(self.lActPre)
            self.Post('action',self.iAct)
            self.pn.cbStart.Enable(False)
        except:
            self.__logTB__()
    def DoFinPre(self):
        try:
            iLen=len(self.res)
            self.__logDebug__(self.res)
            try:
                r=self.res[iLen-2]
                self.Post('result',r)
            except:
                self.__logTB__()
            for iOfs in xrange(iLen-1,-1,-1):
                o=self.res[iOfs]
                self.__logDebug__({'iOfs':iOfs,'iLen':iLen,'o':o})
                try:
                    print iOfs,type(o),o
                    if len(o)>0:
                        self.Post('result',o)
                        break
                except:
                    pass
            self.__logDebug__(''%())
            if self.zAutoLaunch>0:
                self.timer = wx.PyTimer(self.Notify)
                self.timer.Start(1000)
                self.zAuto=0
                self.Notify()
                self.pn.gagProcess.SetValue(0)
                self.pn.gagProcess.SetRange(self.zAutoLaunch)
                self.pn.cbStart.Enable(True)
            elif self.zAutoLaunch==0:
                self.DoLaunch()
                return
            else:
                self.pn.gagProcess.Show(False)
                self.pn.cbStart.Enable(True)
        except:
            self.__logTB__()
    def DoStartPost(self):
        try:
            self.__logDebug__(''%())
            self.iAct=-1
            self.iLen=len(self.lActPost)
            self.Post('action',self.iAct)
            self.pn.cbStart.Enable(False)
        except:
            self.__logTB__()
    def DoFinPost(self):
        try:
            self.__logDebug__(''%())
            self.CB(self.oState.SetState,9000)
            self.pn.cbStart.Enable(True)
        except:
            self.__logTB__()
    def prcLaunch(self):
        try:
            self.__logDebug__(''%())
            self.oState.LogOrg(vpc.vLog_DEBUG,0xffff)
        except:
            self.__logTB__()
        try:
            self.timer.Stop()
        except:
            pass
        try:
            try:
                iPort=int(self.GetSockPort())
            except:
                self.pn.txtStat.SetValue(_(u'Please setup logging port (%s).')%'Port')
                iPort=69999
            try:
                sLogName=self.pn.txtLogName.GetValue()
                if type(sLogName)==types.UnicodeType:
                    sLogName=sLogName.encode()
                iRes=vpc.vLogSetUp(sLogName,iPort)
                vpc.vLogSetLevel(self.GetLogLv())
                if iRes==-1:
                    self.__showBasicSettings__()
                    self.pn.txtStat.SetValue(_(u'Please change logging name (%s).')%'Log Name')
                    return
                if iRes==-2:
                    self.__showBasicSettings__()
                    self.pn.txtStat.SetValue(_(u'Please change logging port (%s).')%'Port')
                    return
            except:
                vpc.logTB(__name__)
                return
            try:
                import vidarc.tool.log.vtLog as vtLog
                iRes=vtLog.vtLngInit(sLogName,sLogName+'.log',
                            self.GetLogLv(),
                            iSockPort=self.GetSockPort())
            except:
                pass
                self.__logWarn__('vtLog not present')
                #vpc.logTB(__name__)
            self.Post('fin')
        except:
            vpc.logTB(__name__)
    def DoLaunch(self):
        try:
            if self.oState.IsFlag(0x008)>0:
                self.oState.SetState(9000)
                #self.Post('start')
            elif self.oState.IsFlag(0x010)>0:
                self.oState.SetState(7000)
            elif self.oState.IsFlag(0x030)>0:
                self.oState.SetState(9000)
            elif 0:
                if len(self.lActPost)>0:
                    self.iAct=-1
                    self.oState.SetState(7000)
                    self.pn.cbStart.Enable(False)
                    return
                else:
                    pass
        except:
            self.__logTB__()
    def OnCbLaunchButton(self, event):
        self.DoLaunch()
        event.Skip()
    def OnLBitmapLeftDown(self, event):
        self.zAutoLaunch=-1
        self.pn.gagProcess.Show(False)
        self.__showBasicSettings__()
        event.Skip()
    def __showBasicSettings__(self,flag=True):
        return
        self.lBitmap.Show(not flag)
        self.lblPort.Show(flag)
        self.txtPort.Show(flag)
        self.chcLogLevel.Show(flag)
        self.txtLogName.Show(flag)
        self.lblApplLogName.Show(flag)
        self.pn.cbCancel.Show(flag)
        self.cbApply.Show(flag)
        self.snbPort.Show(flag)
        self.snbLogName.Show(flag)
        self.fgsData.Layout()
        self.fgsData.Fit(self)
    def GetLogLv(self):
        iSel=self.pn.chcLogLevel.GetSelection()
        if iSel==0:
            iLogLv=vpc.vLog_DEBUG
        elif iSel==1:
            iLogLv=vpc.vLog_INFO
        elif iSel==2:
            iLogLv=vpc.vLog_WARN
        elif iSel==3:
            iLogLv=vpc.vLog_ERROR
        elif iSel==4:
            iLogLv=vpc.vLog_CRITICAL
        elif iSel==5:
            iLogLv=vpc.vLog_FATAL
        else:
            #iLogLv=vpc.vLog_DEBUG
            #self.pn.chcLogLevel.SetSelection(0)
            iLogLv=vpc.vLog_ERROR
            self.pn.chcLogLevel.SetSelection(3)
        return iLogLv
    def SetLogLv(self,iLogLv):
        if iLogLv==vpc.vLog_DEBUG:
            iSel=0
        elif iLogLv==vpc.vLog_INFO:
            iSel=1
        elif iLogLv==vpc.vLog_WARN:
            iSel=2
        elif iLogLv==vpc.vLog_ERROR:
            iSel=3
        elif iLogLv==vpc.vLog_CRITICAL:
            iSel=4
        elif iLogLv==vpc.vtLog_FATAL:
            iSel=5
        self.pn.chcLogLevel.SetSelection(iSel)
    def GetSockPort(self):
        try:
            return int(self.pn.txtLogPort.GetValue())
        except:
            return None
    def OnCbCancelButton(self, event):
        wx.PostEvent(self,vgaSplashAborted())
        event.Skip()
    def OnSnbPortSpinDown(self, event):
        iPort=self.GetSockPort()
        if iPort is not None:
            self.txtPort.SetValue(str(iPort-1))
        event.Skip()
    def OnSnbPortSpinUp(self, event):
        iPort=self.GetSockPort()
        if iPort is not None:
            self.txtPort.SetValue(str(iPort+1))
        event.Skip()
    def OnSnbLogNameSpinDown(self, event):
        self.iLogName-=1
        if self.iLogName<-1:
            self.iLogName=-1
        if self.iLogName<0:
            self.txtLogName.SetValue(self.sLogName)
        else:
            self.txtLogName.SetValue(self.sLogName+str(self.iLogName))
        event.Skip()
    def OnSnbLogNameSpinUp(self, event):
        self.iLogName+=1
        if self.iLogName<0:
            self.iLogName=0
        self.txtLogName.SetValue(self.sLogName+str(self.iLogName))
        event.Skip()
    def OnCbApplyButton(self, event):
        event.Skip()
        self.__showBasicSettings__(False)
