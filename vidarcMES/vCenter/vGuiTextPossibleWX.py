#----------------------------------------------------------------------------
# Name:         vGuiTextPossibleWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20121214
# CVS-ID:       $Id: vGuiTextPossibleWX.py,v 1.6 2015/07/25 16:03:32 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import os,types
    import vLang
    import vSystem
    from vGuiTextPopupWX import vGuiTextPopupWX
    from vGuiFrmWX import vGuiFrmWX
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)

class vGuiTextPossibleWX(vGuiTextPopupWX):
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                dPossible=None,
                bAnchor=True,iArrange=-1):
    """
    def __init__(self,*args,**kwargs):
        vGuiTextPopupWX.__init__(self,*args,**kwargs)
        global _
        _=vLang.assignPluginLang('vGui')
        try:
            self.SEP_FILE_NAME_PART=u'_'
            self.zNow=vpc.vctDateTime()
            wid=kwargs.get('wid',None)
            szCb=kwargs.get('size_button',(24,24))
            szLst=kwargs.get('size_list',(80,60))
            if wid is None:
                lWid=[
                    ('lblRg',   (0,0),(1,1),{'name':'lblOld','label':_(u'current')},None),
                    ('txt',     (0,1),(1,3),{'name':'txtOld','value':u''},None),
                    ('lblCt',   (1,1),(1,1),{'name':'lblPos','label':_(u'possible')},None),
                    ('lblCt',   (1,3),(1,1),{'name':'lblCur','label':_(u'current')},None),
                    ('lst',     (2,1),(2,1),{'name':'lstPos','size':szLst,'mod':0,},None),
                    ('lstMulti',(2,3),(2,1),{'name':'lstCur','size':szLst,'mod':0,},None),
                    ('szBoxVert',(2,2),(2,1),
                            {'name':'bxsBt',},[
                            ('cbBmp',   0,4,{'name':'cbRight','size':szCb,
                                'bitmap':self.getBmp('core','Right'),},
                                {'btn':self.OnAdd}),
                            ('cbBmp',   0,4,{'name':'cbLeft','size':szCb,
                                'bitmap':self.getBmp('core','Left'),},
                                {'btn':self.OnDel}),
                            ('cbBmp',   0,0,{'name':'cbErase','size':szCb,
                                'bitmap':self.getBmp('core','Erase'),},
                                {'btn':self.OnClr}),
                            ]),
                    ('lblRg',   (4,0),(1,1),{'name':'lblNew','label':_(u'new')},None),
                    ('txt',     (4,1),(1,3),{'name':'txtNew','value':u''},None),
                    #('lblRg', (1,0),(1,1),{'name':'lblExec','label':_(u'executable')},None),
                    #('txt',   (1,1),(1,2),{'name':'txtExec','value':''},None),
                    ]
                p=vGuiFrmWX(name='popPos',parent=self.wid,kind='popup',
                        iArrange=0,widArrange=self.wid,bAnchor=True,
                        lGrowRow=[(1,1),],
                        lGrowCol=[(1,1),(3,1)],
                        lWid=lWid)
                self.popFrm=p
                self.SetWidPopup(p)
                self.popFrm.BindEvent('cmd',self.OnPopupCmd)
        except:
            self.__logTB__()
        try:
            self.__initPossible__()
            self.ShowPossible()
        except:
            self.__logTB__()
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # possible
    def GetDateGM(self,*args,**kwargs):
        return self.zNow.GetDateSmallStr()
    def GetTimeGM(self,*args,**kwargs):
        return self.zNow.GetTimeSmallStr()
    def GetDateTimeGM(self,*args,**kwargs):
        return '_'.join([self.zNow.GetDateSmallStr(),
                self.zNow.GetTimeSmallStr()])
    def GetHost(self,*args,**kwargs):
        return vSystem.getHost()
    def GetHostFull(self,*args,**kwargs):
        return vSystem.getHostFull()
    def GetPossibleDictShow(self):
        return self.dPossibleShow
    def GetPossibleDictConv(self):
        return self.dPossibleConv
    def __initPossible__(self):
        self.dPossibleShow={
            '%date%':       _(u'current date'),
            '%time%':       _(u'current time'),
            '%now%':        _(u'current date and time'),
            '%host%':       _(u'hostname'),
            '%hostname%':   _(u'full hostname'),
            }
        self.dPossibleConv={
            #'%date%':       (self.GetDateGM,(),{}),
            '%date%':       self.GetDateGM,
            '%time%':       self.GetTimeGM,
            '%now%':        self.GetDateTimeGM,
            '%host%':       self.GetHost,
            '%hostname%':   self.GetHostFull,
            }
        self.dPosExtShow=None
        self.dPosExtConv=None
    def GetValueConv(self,sVal,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(5)
            self.zNow.Now()
            dPos=self.dPossibleConv.copy()
            if self.dPosExtConv is not None:
                dPos.update(self.dPosExtConv)
            if sVal is None:
                sVal=self.txtVal.GetValue()
            iLen=len(sVal)
            iOfs=iLen
            while iOfs>=0:
                iE=sVal.rfind('%',0,iOfs)
                if iE>=0:
                    iS=sVal.rfind('%',0,iE-1)
                    if bDbg:
                        self.__logDebug__('%s;iS:%8d;iE:%8d'%(sVal[iS:iE+1],iS,iE))
                    if iS>=0:
                        if (iS+1)!=iE:
                            sK=sVal[iS:iE+1]
                            if sK in dPos:
                                v=dPos[sK]
                                t=type(v)
                                if t==types.TupleType:
                                    func,argsF,kwargsF=v
                                    a=args+argsF
                                    k=kwargs.copy()
                                    k.update(kwargsF)
                                    vr=func(*a,**k)
                                    sVal=sVal[:iS]+vr+sVal[iE+1:]
                                elif t==types.MethodType:
                                    sVal=sVal[:iS]+v(*args,**kwargs)+sVal[iE+1:]
                                elif t==types.StringType:
                                    sVal=sVal[:iS]+v+sVal[iE+1:]
                                elif t==types.UnicodeType:
                                    sVal=sVal[:iS]+v+sVal[iE+1:]
                                else:
                                    pass
                                if bDbg:
                                    self.__logDebug__('sVal:%s'%(sVal))
                            else:
                                s=os.getenv(sK[1:-1])
                                if s is None:
                                    self.__logError__('key:%s not found'%(sK[1:-1]))
                                    s=''
                                sVal=sVal[:iS]+s+sVal[iE+1:]
                            iOfs=iS
                        else:
                            iOfs=iS-1
                    else:
                        iOfs=-1
                else:
                    iOfs=-1
            return sVal
        except:
            self.__logTB__()
        return None
    # possible
    # ---------------------------------------------------------
    def SetValue(self,sVal):
        try:
            vGuiTextPopupWX.SetValue(self,sVal)
            pn=self.GetPanel()
            pn.txtOld.SetValue(sVal)
            pn.txtNew.SetValue(sVal)
            pn.SetModified(False,pn.txtOld)
            pn.SetModified(False,pn.txtNew)
        except:
            self.__logTB__()
    def GetPanel(self):
        return self.popFrm
    def SetPossible(self,dPosShow,dPosConv):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebugAdd__(dPosShow,dPosConv)
            self.dPossibleShow=dPosShow
            self.dPossibleConv=dPosConv
            self.ShowPossible()
        except:
            self.__logTB__()
    def SetPossibleExt(self,dPosShow,dPosConv):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebugAdd__(dPosShow,dPosConv)
            self.dPosExtShow=dPosShow
            self.dPosExtConv=dPosConv
            self.ShowPossible()
        except:
            self.__logTB__()
    def ShowPossible(self):
        try:
            pn=self.GetPanel()
            pn.lstPos.Clear()
            l=[]
            for k,v in self.dPossibleShow.iteritems():
                l.append((v,k))
            if self.dPosExtShow is not None:
                for k,v in self.dPosExtShow.iteritems():
                    l.append((v,k))
            def cmpArg(a,b):
                iR=cmp(a[0],b[0])
                if iR==0:
                    return cmp(a[1],b[1])
                return iR
            l.sort(cmpArg)
            self.lPosArg=[t[1] for t in l]
            
            for t in l:
                pn.lstPos.Append(t[0])
        except:
            self.__logTB__()
    def OnPopupCmd(self,evt):
        try:
            sCmd=evt.GetCmd()
            oDat=evt.GetData()
            pn=self.GetPanel()
            if sCmd=='cancel':
                self.__logDebug__(''%())
                sVal=pn.txtOld.GetValue()
                pn.txtNew.SetValue(sVal)
                self.txtVal.SetValue(sVal)
                pn.SetModified(False,pn.txtOld)
                pn.SetModified(False,pn.txtNew)
            elif sCmd=='ok':
                sOld=pn.txtOld.GetValue()
                sNew=pn.txtNew.GetValue()
                if cmp(sOld,sNew)!=0:
                    mod=True
                else:
                    mod=-1
                self.SetValue(sNew)
                pn.txtOld.SetValue(sNew)
                pn.SetModified(False,pn.txtOld)
                pn.SetModified(False,pn.txtNew)
            elif sCmd=='show':
                sVal=self.GetValue()
                pn.txtOld.SetValue(sVal)
                pn.txtNew.SetValue(sVal)
                pn.SetModified(False,pn.txtOld)
                pn.SetModified(False,pn.txtNew)
                pass
            elif sCmd=='hide':
                pass
        except:
            self.__logTB__()
    def OnAdd(self,evt):
        try:
            bDbg=False
            if self.__isLogDebug__():
                self.__logDebug__(''%())
                bDbg=True
            pn=self.GetPanel()
            iSel=pn.lstPos.GetSelection()
            sSel=pn.lstPos.GetStringSelection()
            pn.lstCur.Append(sSel,iSel)
            sNew=pn.txtNew.GetValue()
            if bDbg:
                self.__logDebug__('sCur:%s'%(sNew))
            if len(sNew)>0:
                sNew=sNew+ self.SEP_FILE_NAME_PART+self.lPosArg[iSel]
            else:
                sNew=self.lPosArg[iSel]
            pn.txtNew.SetValue(sNew)
            if bDbg:
                self.__logDebug__('sNew:%s'%(sNew))
        except:
            self.__logTB__()
    def OnDel(self,evt):
        try:
            bDbg=False
            if self.__isLogDebug__():
                self.__logDebug__(''%())
                bDbg=True
            pn=self.GetPanel()
            lSel=list(pn.lstCur.GetSelections())
            sVal=pn.txtNew.GetValue()
            if bDbg:
                self.__logDebug__({'sVal':sVal,'lSel':lSel})
            for iIdx in lSel:
                sArg=self.lPosArg[pn.lstCur.GetClientData(iIdx)]
                iS=sVal.find(sArg)
                if iS>=0:
                    iE=iS+len(sArg)
                    if iS>0:
                        if sVal[iS-1]==u' ':
                            sVal=sVal[:iS-1]+sVal[iE:]
                        else:
                            sVal=sVal[:iS]+sVal[iE:]
                    else:
                        if iE>=len(sVal):
                            sVal=sVal[iE:]
                        else:
                            if sVal[iE]==u' ':
                                sVal=sVal[iE+1:]
                            else:
                                sVal=sVal[iE:]
            pn.txtNew.SetValue(sVal)
            lSel.reverse()
            for iIdx in lSel:
                pn.lstCur.Delete(iIdx)
            if bDbg:
                self.__logDebug__('sVal:%s'%(sVal))
        except:
            self.__logTB__()
    def OnClr(self,evt):
        try:
            self.__logDebug__(''%())
            pn=self.GetPanel()
            pn.txtNew.SetValue(u'')
            pn.lstCur.Clear()
        except:
            self.__logTB__()
