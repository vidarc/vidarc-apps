#----------------------------------------------------------------------------
# Name:         vGuiDft.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20150121
# CVS-ID:       $Id: vGuiDft.py,v 1.1 2015/01/23 21:12:21 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

gdDft={
    '':{
        'size_button':(24,24),
        'size_list':(120,80),
        },
    'vGuiChoiceWX':{
        'size_button':(24,24),
        'size_list':(120,100),
        },
    'vGuiNumWX':{
        'size_val':(30,24),
        'size_button':(24,24),
        'bmpDec':('core','DnBlSml'),
        'bmpInc':('core','UpBlSml'),
        },
    }

def vGuiDftGet(sCls,sKey,fb):
    try:
        global gdDft
        if VERBOSE>10:
            vpc.logDebug('sCls:%s;sKey:%s'%(sCls,sKey))
        if sCls in gdDft:
            d=gdDft[sCls]
            iR=0
        else:
            d=gdDft['']
            iR=-1
        
        if sKey in d:
            return iR,d[sKey]
        else:
            return -2,fb
    except:
        vpc.logTB(__name__)
        return -9,fb

