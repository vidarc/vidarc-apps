#----------------------------------------------------------------------------
# Name:         vGuiTreeXmlWX.py
# Purpose:      tree ctrl attached to xml dom tree
# Author:       Walter Obweger
#
# Created:      20131103
# CVS-ID:       $Id: vGuiTreeXmlWX.py,v 1.5 2015/01/21 12:05:37 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiTreeCtrlWX import vGuiTreeCtrlWX
    from vGuiImgLstWX import vGuiImgLstGet
    from vGuiImgConvWX import vGuiImgConvWX
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiTreeXmlCB(vpc.vcxRegCB):
    def __init__(self,widTr):
        vpc.logDebug('start',__name__)
        vpc.vcxRegCB.__init__(self)
        vpc.logDebug('done',__name__)
        self.lDef=[]
        self.widTr=widTr
    def __del__(self):
        self.widTr.__logDebug__('destory')
    def prcReg(self,sName):
        o=self.GetRegDat()
        self.widTr.__logDebug__({'sName':sName,'o':o,'tag':o.GetTagName()})
        self.lDef.append((o.GetTagName(),o.GetBitmap()))
        return 1

class vGuiTreeXmlWX(vGuiTreeCtrlWX):
    def __init__(self,*args,**kwargs):
        vGuiTreeCtrlWX.__init__(self,*args,**kwargs)
        self.SetVerbose(__name__)
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        self.SetVerbose(__name__)
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            wid=vGuiTreeCtrlWX.__initWid__(self,*args,**kwargs)
            return wid
        except:
            self.__logTB__()
    def __initProperties__(self,*args,**kwargs):
        try:
            vGuiTreeCtrlWX.__initProperties__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.doc=None
            self._bSort=kwargs.get('bSort',True)
            
        except:
            self.__logTB__()
    def __initImageLst__(self):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('_bImgLstCreated:%d'%(self._bImgLstCreated))
            if self._bImgLstCreated==False:
                return
            w=self._getImgLstObj()
        except:
            self.__logTB__()
    def prcRegNodeImg(self,o,lDef,w):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            lDef.append((o.GetTagName(),o.GetBitmap()))
        except:
            self.__logTB__()
            return 0
        return 1
    def __extendImageLstRegNodes__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self._bImgLstCreated==False:
                return
            if self.doc is None:
                self.__logError__('doc is None')
                return
            w=self._getImgLstObj()
            oCB=vpc.vcxRegCB()
            self.__logDebug__({})
            oCB=vGuiTreeXmlCB(self)
            self.__logDebug__({})
            self.doc.PrcReg(oCB)
            self.__logDebug__(oCB.lDef)
            w.AddImageList(oCB.lDef)
            self.__logDebug__('done')
        except:
            self.__logTB__()
    def SetDoc(self,doc,bNet):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            if bNet:
                # add event
                pass
            self.doc=doc
            self.oIter=doc.GetIter(self.GetOrigin(),"tr")
            self.oIter.SetNodeProp(vpc.vcxNodePropTree_SKIP,
                        vpc.vcxNodePropTree_DISP)
            self.__extendImageLstRegNodes__()
        except:
            self.__logTB__()
