#----------------------------------------------------------------------------
# Name:         vCtrSys.py
# Purpose:      derived from vSystem.py
# Author:       Walter Obweger
#
# Created:      20060219
# CVS-ID:       $Id: vCtrSys.py,v 1.3 2016/02/07 06:49:47 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import os
    import sys
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

gLocalDN=None
gUsrCfgDN=None
gLogDatDN=None
gLogCfgDN=None

MAP_PLATFORM_IMP_SUF={0:'Unx',1:'Win'}

if sys.platform.startswith('win'):
    SYSTEM=1
else:
    SYSTEM=0

vpc.logDebug('SYSYEM:%d'%(SYSTEM),__name__)

def isWin():
    global SYSTEM
    return SYSTEM==1

def isUnx():
    global SYSTEM
    return SYSTEM==0

def isPlatForm(platform):
    if platform=='win':
        return isWin()
    elif platform=='unx':
        return isUnx()
    return False

def getPlatForm():
    global SYSTEM
    return SYSTEM

def getPlatFormImpSuf():
    global SYSTEM
    return MAP_PLATFORM_IMP_SUF.get(SYSTEM,'')

from vGuiScreen import getScreenDict

def LimitWindowToScreen(iX,iY,iWidth,iHeight):
    try:
        dScreens=getScreenDict()
        if dScreens is None:
            iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iXa,iYa=0,0
            iXe,iYe=iMaxW,iMaxH
        else:
            dScr=dScreens[None]
            iXa=dScr.get('iXa',0)
            iYa=dScr.get('iYa',0)
            iXe=dScr.get('iXe',0)
            iYe=dScr.get('iYe',0)
            iMaxW=iXe-iXa
            iMaxH=iYe-iYa
        if iX>iMaxW:
            iX=iMaxW-iWidth
        if iY>iMaxH:
            iY=iMaxH-iHeight
        if iX<iXa:
            iX=0
        if iY<iYa:
            iY=0
        if iX+iWidth>iXe:
            iX=iXe-iWidth
        if iY+iHeight>iYe:
            iY=iYe-iHeight
        if iX<iXa:
            iX=iXa
        if iY<iYa:
            iY=iYa
        if iX+iWidth>iXe:
            iWidth=iMaxW-iX
        if iY+iHeight>iMaxH:
            iHeight=iMaxH-iY
    except:
        iX=iY=0
        iWidth=100
        iHeight=100
    return iX,iY,iWidth,iHeight

def ArrangeWidget(widBase,widSub,how):
    """
    how = 0     ... bottom left of widBase
    """
    iBaseX,iBaseY = widBase.ClientToScreen( (0,0) )
    iBaseW,iBaseH =  widBase.GetSize()
    iSubX,iSubY=widSub.GetPosition()
    iSubW,iSubH=widSub.GetSize()
    if how==0:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    elif how==10:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX+iBaseW,iBaseY,iSubW,iBaseH)
    else:
        iX,iY,iW,iH=LimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    #print 'base',iBaseX,iBaseY,iBaseW,iBaseH
    #print 'sub ',iSubX,iSubY,iSubW,iSubH
    #print 'res',iX,iY,iW,iH
    widSub.Move((iX,iY))
    widSub.SetSize((iW,iH))

def getDefaultLocalDN():
    try:
        #sDN=getUsrCfgDN('VIDARC')
        sDN=getUsrCfgDN(None)
        return sDN
    except:
        pass
    return os.path.join(os.path.expanduser('~'),'VIDARC')

def setLocalDN(sDN):
    global gLocalDN
    try:
        gLocalDN=sDN[:]
        vpc.logDebug('gLocalDN:%s'%(gLocalDN),__name__)
    except:
        vpc.logTB(__name__)
def getLocalDN(sAppl=None):
    global gLocalDN
    if gLocalDN is None:
        try:
            sDN=os.getenv('vidarcLocalDN')
        except:
            vpc.logTB(__name__)
        if sDN is None:
            sDN=''
        if len(sDN)==0:
            try:
                if isWin():
                    sDN=os.getenv('APPDATA')
                else:
                    sDN=None
            except:
                sDN=None
                vpc.logTB(__name__)
            try:
                if sDN is None:
                    sDN=os.path.expanduser('~')
                vpc.logDebug('gLocalDN:%s'%(sDN),__name__)
            except:
                vpc.logTB(__name__)
        else:
            vpc.logDebug('gLocalDN:%s;set by environment'%(sCfgDN),__name__)
        gLocalDN=sDN[:]
    else:
        sDN=gLocalDN
    if sAppl is not None:
        sApplDN=os.path.join(sDN,sAppl)
        try:
            os.makedirs(sApplDN)
        except:
            pass
        return sApplDN
    return sDN
getLocalDN()

def setUsrCfgDN(sDN):
    global gUsrCfgDN
    try:
        gUsrCfgDN=sDN
        vpc.logDebug('gUsrCfgDN:%s'%(gUsrCfgDN),__name__)
    except:
        vpc.logTB(__name__)
def getUsrCfgDN(sAppl=None):
    global gUsrCfgDN
    if gUsrCfgDN is None:
        try:
            sDN=os.getenv('vidarcUsrCfgDN')
        except:
            vpc.logTB(__name__)
        if sDN is None:
            sDN=getLocalDN()
        else:
            if len(sDN)==0:
                sDN=getLocalDN()
        try:
            if len(sDN)>0:
                sCfgDN=os.path.join(sDN,'VIDARC')
            else:
                sCfgDN=None
        except:
            sCfgDN=None
            vpc.logTB(__name__)
        try:
            if sCfgDN is None:
                sCfgDN=os.path.join(os.path.expanduser('~'),'VIDARC')
            vpc.logDebug('gUsrCfgDN:%s'%(sCfgDN),__name__)
        except:
            vpc.logTB(__name__)
        gUsrCfgDN=sCfgDN[:]
    else:
        sCfgDN=gUsrCfgDN
    if sAppl is not None:
        sDN=os.path.join(sCfgDN,sAppl)
        try:
            os.makedirs(sDN)
        except:
            pass
        return sDN
    return sCfgDN
getUsrCfgDN(None)

def getPlugInDN():
    return getLocalDN('VIDARCplugins')

def getPlugInLocaleDN():
    return os.path.join(getPlugInDN(),'locale')

def getPlugInHelpDN():
    return os.path.join(getPlugInDN(),'help')

def setLogDatDN():
    global gLogDatDN
    try:
        gLogDatDN=sDN[:]
        vpc.logDebug('gLogDatDN:%s'%(gLogDatDN),__name__)
    except:
        vpc.logTB(__name__)
def getLogDatDN():
    global gLogDatDN
    if gLogDatDN is None:
        sDN=os.getenv('vidarcLogDN',None)
        if sDN is None:
            return getLocalDN('VIDARClog')
        else:
            try:
                os.makedirs(sDN)
            except:
                pass
        gLogDatDN=sDN[:]
    return sLogDatDN
def getLogDN():
    return getLogDatDN()

def setLogCfgDN():
    global gLogCfgDN
    try:
        gLogCfgDN=sDN[:]
        vpc.logDebug('gUsrCfgDN:%s'%(gUsrCfgDN),__name__)
    except:
        vpc.logTB(__name__)
def getLogCfgDN():
    global gLogCfgDN
    if gLogCfgDN is None:
        sDN=os.getenv('vidarcLogCfgDN',None)
        if sDN is None:
            return getLocalDN('VIDARClogCfg')
        else:
            try:
                os.makedirs(sDN)
            except:
                pass
        gLogCfgDN=sDN[:]
    return gLogCfgDN

def getErrFN(sAppl):
    sDN=getLogDN()
    sLogFN=os.path.join(sDN,u'.'.join([sAppl,'err.log']))
    return sLogFN

def getHost():
    try:
        return wx.GetHostName().lower()
    except:
        return 'localhost'
    
def getHostFull():
    return wx.GetFullHostName()
