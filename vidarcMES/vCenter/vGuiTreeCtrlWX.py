#----------------------------------------------------------------------------
# Name:         vGuiTreeCtrlWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
#   init args
#       bTreeButtons    ... wx hasbutton style
#
# Created:      20131027
# CVS-ID:       $Id: vGuiTreeCtrlWX.py,v 1.13 2015/07/30 16:46:45 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import sys
    import types
    import wx
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    from vGuiImgLstWX import vGuiImgLstGet
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiTreeCtrlIntWX(wx.TreeCtrl):
    def __init__(self,widGui,*args,**kwargs):
        wx.TreeCtrl.__init__(self,*args,**kwargs)
        self.widGui=widGui
    def OnCompareItems(self,ti1,ti2):
        l1=self.widGui.GetItemStr(ti1,0)
        l2=self.widGui.GetItemStr(ti2,0)
        return cmp(l1,l2)

class vGuiTreeCtrlWX(vGuiCoreWX):
    #def __init__(self,id=-1,parent=None,pos=(0,0),size=(32,32),name='trCtrl',
    #            style=wx.LC_REPORT,cols=None,default_col_width=100,
    #           bVert=0,
    #            multiple_sel=True,imgLst=None,
    #            map=None,value=None):
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.wid=None
            self.MAP=[]
            self.IMG_EMPTY=-1
            self.dCache={}
            
            self.FORCE_CHILDREN=kwargs.get('bForceChildren',False)
            self._bHideRoot=kwargs.get('bHideRoot',False)
            self._bSort=kwargs.get('bSort',False)
            
            self._funcVal=None
            self._argsVal=()
            self._kwargsVal={}
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        wid=None
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            style=0
            self.IMG_EMPTY=-1
            if kwargs.get('bTreeButtons',1)>0:
                style|=wx.TR_HAS_BUTTONS
            if kwargs.get('bHideRoot',False)>0:
                style|=wx.TR_HIDE_ROOT
            if kwargs.get('multiple_sel',False)>0:
                style|=wx.TR_MULTIPLE
                self._bMultiple=True
            else:
                self._bMultiple=False
            _args,_kwargs=self.GetGuiArgs(kwargs,
                    ['id','name','parent','pos','size','style'],
                    {'pos':(0,0),'size':(-1,-1),'style':style})
            self.widCls=wx.TreeCtrl
            wid=self.widCls(*_args,**_kwargs)
            self.wid=wid
            #wid=vGuiTreeCtrlIntWX(self,*_args,**_kwargs)
            wid.Bind(wx.EVT_TREE_ITEM_ACTIVATED,self.OnItemActivate)
            self.CB(wid.Bind,wx.EVT_TREE_ITEM_COLLAPSING,self.OnItemCollapsing)
            #if self._bSort:
            #    self.CB(self.wid.Bind,wx.EVT_TREE_ITEM_EXPANDING,self.OnItemSort)
        except:
            self.__logTB__()
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            self._dImg=None
            self._imgLst=None
            imgLst=kwargs.get('imgLst',None)
            if imgLst is not None:
                if type(imgLst)==types.StringType:
                    w,bImgLstCreated=vGuiImgLstGet(imgLst)
                    if w is not None:
                        self._sImgLst=imgLst
                        dImg0,imgLst0=w.GetObjs()
                        self.SetImageListByDict(dImg0,imgLst0)
            if self._imgLst is None:
                w,bImgLstCreated=vGuiImgLstGet(self.__class__.__name__,dft='tree')
                if w is not None:
                    self._sImgLst=None
                    self._bImgLstCreated=bImgLstCreated
                    dImg0,imgLst0=w.GetObjs()
                    self.SetImageListByDict(dImg0,imgLst0)
            self.__initImageLst__()
            # ++++ setup mapping
            oTmp=kwargs.get('map',None)
            if oTmp is not None:
                self.MAP=oTmp
            # ---- setup mapping
        except:
            self.__logTB__()
        return wid
    def _getImgLstObj(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            w,bFlag=vGuiImgLstGet(self._sImgLst or self.__class__.__name__,dft='tree')
            return w
        except:
            self.__logTB__()
        return w
    def __initProperties__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            # ++++ assign values
            oTmp=kwargs.get('value',None)
            if oTmp is not None:
                self.SetValue(oTmp)
            # ---- assign values
            tFunc=kwargs.get('funcValue',None)
            if tFunc is not None:
                self.SetFuncValue(tFunc[0],*tFunc[1],**tFunc[2])
                oThd=vGuiCoreWX.__getThread__(self,'tree')
                if kwargs.get('build',True):
                    self.CB(self.Build)
        except:
            self.__logTB__()
    # ++++++++++++++++++++++++++++++++++++++++
    
    def GetTreeEvtDat(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(8)
            if bDbg:
                self.__logDebug__('')
            if evt is None:
                return None
            ti=evt.GetItem()
            tr=evt.GetEventObject()
            if ti.IsOk():
                dat=tr.GetPyData(ti)
                txt=tr.GetItemText(ti)
                v={ 'dat':dat,
                    'lbl':txt,
                    }
                return v
        except:
            self.__logTB__()
        return None
    def OnItemActivate(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.Post('exec',self.GetTreeEvtDat(evt))
            if evt is not None:
                evt.Skip()
        except:
            self.__logTB__()
    def _prcBuild(self,tr,ti,idx,dDat,bExpand):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            iR=self._funcVal(idx,dDat,*self._argsVal,**self._kwargsVal)
            self.__logDebug__({'iR':iR,'dDat':dDat})
            if iR<=0:
                return
            #self.__setValue__(tr,ti,dDat)
            #if ti is None:
            #    ti=tr.AddRoot('')
            #tc=tr.AppendItem(ti,'')
            self.CB(self.__setTreeItem__,tr,ti,dDat)
            if bExpand:
                self.CB(tr.Expand,ti)
        except:
            self.__logTB__()
    def _build(self,tr,ti,bExpand=False):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            oThd=vGuiCoreWX.__getThread__(self,'tree')
            #oThd.DoWX
            #tr.Freeze()
            idx=None
            dDat={}
            if ti is not None:
                if ti.IsOk():
                    idx=tr.GetPyData(ti)
                    t=self.GetItemTup(ti,0)
                    dDat[-1]=idx
                    if t is not None:
                        dDat[0]=t[1]
                        dDat[1]=t[0]
            #else:
            #    ti=tr.AddRoot('')
            oThd.Do(self._prcBuild,tr,ti,idx,dDat,bExpand)
            #oThd.Do(self._funcVal,*self._argsVal,**self._kwargsVal)
        except:
            self.__logTB__()
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            #tr.Thaw()
        except:
            self.__logTB__()
        return ti
    def OnItemSort(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            ti=evt.GetItem()
            tr=self.wid
            bC=tr.ItemHasChildren(ti)
            if bC:
                tr.SortChildren(ti)
        except:
            self.__logTB__()
    def OnItemExpanding(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            dEvt=self.GetTreeEvtDat(evt)
            if bDbg:
                self.__logDebug__(dEvt)
            if evt is None:
                return None
            else:
                evt.Skip()
            ti=evt.GetItem()
            tr=self.wid
            bC=tr.ItemHasChildren(ti)
            if bC:
                tc,ck=tr.GetFirstChild(ti)
                if tc.IsOk():
                    pass
                else:
                    self._build(tr,ti,bExpand=True)
        except:
            self.__logTB__()
    def OnItemCollapsing(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if evt is None:
                return None
            ti=evt.GetItem()
            tr=self.wid
            if tr.GetRootItem()==ti:
                evt.Veto()
            else:
                evt.Skip()
        except:
            self.__logTB__()
    def SetFuncValue(self,func,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self._funcVal=func
            self._argsVal=args
            self._kwargsVal=kwargs
            wid=self.wid
            self.CB(wid.Bind,wx.EVT_TREE_ITEM_EXPANDING,self.OnItemExpanding)
        except:
            self.__logTB__()
    def Build(self,idx=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if idx is not None:
                ti=self.dCache[idx]
            else:
                ti=None
                self.CB(self.Clear)
            #tc=self._build(self.wid,ti)
            self.CB(self._build,self.wid,ti)
        except:
            self.__logTB__()
    def BindEvent(self,name,func,par=None):
        if name.startswith('tr_'):
            wid=self.GetWid()
            if name in ['tr_item_sel','tr_item_selected']:
                wid.Bind(wx.EVT_TREE_ITEM_SELECTED,func, par or wid)
            elif name in ['tr_item_desel','tr_item_deselected']:
                wid.Bind(wx.EVT_TREE_ITEM_DESELECTED,func, par or wid)
            elif name in ['tr_item_act','tr_item_activated']:
                wid.Bind(wx.EVT_TREE_ITEM_ACTIVATED,func, par or wid)
            elif name in ['tr_item_del','tr_item_delete']:
                wid.Bind(wx.EVT_TREE_ITEM_DELETE,func, par or wid)
            elif name in ['tr_item_clg','tr_item_collapsing']:
                wid.Bind(wx.EVT_TREE_ITEM_COLLAPSING,func, par or wid)
            elif name in ['tr_item_cld','tr_item_collapsed']:
                wid.Bind(wx.EVT_TREE_ITEM_COLLAPSED,func, par or wid)
            elif name in ['tr_item_exg','tr_item_expanding']:
                wid.Bind(wx.EVT_TREE_ITEM_EXPANDING,func, par or wid)
            elif name in ['tr_item_exd','tr_item_expanded']:
                wid.Bind(wx.EVT_TREE_ITEM_EXPANDED,func, par or wid)
            elif name in ['tr_item_selg','tr_item_sel_changing']:
                wid.Bind(wx.EVT_TREE_ITEM_SEL_CHANGING,func, par or wid)
            elif name in ['tr_item_seld','tr_item_selchanged']:
                wid.Bind(wx.EVT_TREE_ITEM_SEL_CHANGED,func, par or wid)
            elif name in ['tr_item_right_click','tr_item_rgClk']:
                wid.Bind(wx.EVT_TREE_ITEM_RIGHT_CLICK,func, par or wid)
            elif name in ['tr_drag_begin']:
                wid.Bind(wx.EVT_TREE_BEGIN_DRAG,func, par or wid)
            elif name in ['tr_drag_right_begin']:
                wid.Bind(wx.EVT_TREE_BEGIN_RDRAG,func, par or wid)
            elif name in ['tr_drag_end']:
                wid.Bind(wx.EVT_TREE_END_DRAG,func, par or wid)
            elif name in ['tr_key_click']:
                wid.Bind(wx.EVT_LIST_COL_LEFT_CLICK,func, par or wid)
            elif name=='tr_col_right_click':
                wid.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,
                        func, par or wid)
        else:
            vGuiCoreWX.BindEvent(self,name,func,par=par)
    def GetImg(self,sTag,sFB):
        try:
            if self._dImg is not None:
                iImg=self._dImg.get(sTag,None)
                if iImg is None:
                    iImg=self._dImg.get(sFB,-1)
                return iImg
        except:
            self.__logTB__()
        return -1
    def __initImageLst__(self):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def SetImageListByDict(self,dImg,imgLst,which=None):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            if imgLst is not None:
                self.IMG_EMPTY=dImg.get('empty',0)
                #iImgSortAsc=dImg.get('asc',-1)
                #iImgSortDesc=dImg.get('desc',-1)
            self.wid.SetImageList(imgLst)
            self._dImg,self._imgLst=dImg,imgLst
        except:
            self.__logTB__()
    def EnsureVisibleIdx(self,iIdx):
        try:
            if iIdx in self.dCache:
                ti=self.dCache[iIdx]
                self.CB(self.wid.EnsureVisible,ti)
        except:
            self.__logTB__()
    def ExpandIdx(self,iIdx):
        try:
            if iIdx in self.dCache:
                ti=self.dCache[iIdx]
                self.CB(self.wid.Expand,ti)
        except:
            self.__logTB__()
    def _delete(self,tr,ti):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            l=[]
            self.__getValue__(tr,ti,None,[-1],l)
            iIdx=l[0][0]
            if bDbg:
                self.__logDebug__({'l':l,'iIdx':iIdx,
                        'iIdxIn':iIdx in self.dCache})
            if iIdx in self.dCache:
                del self.dCache[iIdx]
        except:
            self.__logTB__()
        return 1
    def Delete(self,ti):
        try:
            if self.IsMainThread()==False:
                return
            tr=self.wid.GetRootItem()
            if ti==tr:
                self.DeleteAllItems()
                return 
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('walk through children')
            self.__walk__(self.wid,ti,self._delete)
            if bDbg:
                self.__logDebug__('delete ti now')
            self._delete(self.wid,ti)
            self.wid.Delete(ti)
        except:
            self.__logTB__()
    def DelIdx(self,iIdx):
        try:
            if iIdx in self.dCache:
                ti=self.dCache[iIdx]
                del self.dCache[iIdx]
                self.CB(self.Delete,ti)
        except:
            self.__logTB__()
    def DelSelected(self):
        try:
            lSel=self.GetSelected([-2,-1])
            self.__logDebug__(lSel)
            lSel.reverse()
            for t in lSel:
                self.DelIdx(t[1])
        except:
            self.__logTB__()
    def DeleteAllItems(self):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            self.widCls.DeleteAllItems(self.wid)
            self.dCache={}
        except:
            self.__logTB__()
    ClearAll=DeleteAllItems
    Clear=DeleteAllItems
    def __getTreeItemByHier__(self,tr,ti,lHier):
        if self.__isLogDebug__():
            self.__logDebug__([lHier[0],tr.GetItemText(ti),
                    '.'.join(lHier)])
        if lHier[0]==tr.GetItemText(ti):
            if len(lHier)>1:
                t=tr.GetFirstChild(ti)
                while t[0].IsOk():
                    r=self.__getTreeItemByHier__(tr,t[0],lHier[1:])
                    if r is not None:
                        return r
                    t=tr.GetNextChild(ti,t[1])
            return ti
        return None
    def __setTreeItemDict__(self,tr,ti,d):
        #print tr.GetItemText(ti,0)
        for s,i in self.MAP:
            if i>0:
                continue
            if s in d:
                v=d[s]
                t=type(v)
                if t==types.TupleType:
                    #tr.SetItemText(ti,v[0],i)
                    #tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
                    tr.SetItemText(ti,v[0])
                    tr.SetItemImage(ti,v[1],which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    #tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
                    tr.SetItemImage(ti,v,which=wx.TreeItemIcon_Normal)
                else:
                    #tr.SetItemText(ti,v,i)
                    tr.SetItemText(ti,v)
                    
                    tr.SetItemImage(ti,self.IMG_EMPTY,which=wx.TreeItemIcon_Normal)
        bSkipImg=True
        if 0 in d:
            img=d[0]
            t=type(img)
            if t==types.IntType:
                bSkipImg=False
            elif t==types.StringType:
                if self._dImg is not None:
                    img=self.GetImg(img,'empty')
                    bSkipImg=False
            if bSkipImg==False:
                tr.SetItemImage(ti,img,which=wx.TreeItemIcon_Normal)
        if bSkipImg==True:
            tr.SetItemImage(ti,self.IMG_EMPTY,which=wx.TreeItemIcon_Normal)
        if -1 in d:
            v=d[-1]
            tr.SetPyData(ti,v)
            self.dCache[v]=ti
        if -2 in d:
            v=d[-2]
            if v is None:
                tr.SetItemHasChildren(ti,True)
            else:
                if self._bSort:
                    t=type(v[0])
                    if t==types.DictType:
                        def cmpDict(a,b):
                            try:
                                for s,i in self.MAP:
                                    return cmp(a.get(s,None),b.get(s,None))
                            except:
                                pass
                            return 0
                        v.sort(cmpDict)
                self.__setValue__(tr,ti,v)
    def __setTreeItemLst__(self,tr,ti,l):
        try:
            if l[0] is not None:
                tr.SetPyData(ti,l[0])
                self.dCache[l[0]]=ti
            i=0
            for v in l[1]:
                if i>0:
                    continue
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0])
                    tr.SetItemImage(ti,v[1],which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v)
                    tr.SetItemImage(ti,self.IMG_EMPTY,which=wx.TreeItemIcon_Normal)
                i+=1
            iLen=len(l)
            if iLen>2:
                self.__setValue__(tr,ti,l[2])
        except:
            self.__logTB__()
            self.__logError__({'l':l,'l[0]':l[0],'l[1]':l[1]})
    def __setTreeItemTup__(self,tr,ti,t):
        pass
    def __setTreeItem__(self,tr,ti,val):
        if ti is None:
            ti=tr.AddRoot('')
            self.CB(tr.Expand,ti)
        self.__logDebug__(val)
        t=type(val)
        if t==types.DictType:
            self.__setTreeItemDict__(tr,ti,val)
        elif t==types.ListType:
            self.__setTreeItemLst__(tr,ti,val)
        elif t==types.TupleType:
            self.__setTreeItemTup__(tr,ti,val)
    def __setValue__(self,tr,tp,l,bRoot=False):
        for ll in l:
            if tp is None:
                ti=tr.AddRoot('')
            else:
                ti=tr.AppendItem(tp,'')
            if self.FORCE_CHILDREN:
                tr.SetItemHasChildren(ti,True)
            self.__setTreeItem__(tr,ti,ll)
            if tp is None:
                #if self._bSort:
                #    tr.SortChildren(ti)
                return
            #if self._bSort:
            #    tr.SortChildren(ti)
    def __getValue__(self,tr,ti,lHier,lMap,l):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'lHier':lHier,'lMap':lMap,'l':l})
            
            def getVal(tr,ti,lHier,i):
                if i is None:
                    return lHier
                if i==-1:
                    try:
                        return tr.GetPyData(ti)
                    except:
                        return None
                elif i==-2:
                    return ti
                else:
                    if i==0:
                        try:
                            return tr.GetItemText(ti)
                        except:
                            return u''
                    else:
                        return u''
                    #return tr.GetItemText(ti,i)
            l.append([getVal(tr,ti,lHier,i) for i in lMap])
            return 1
        except:
            self.__logTB__()
        return 0
    def __getValueData__(self,tr,ti,l):
        try:
            data=tr.GetPyData(ti)
        except:
            data=None
        l.append(data)
        # 20150123 wro: catch exception and add data always
        #   this method is call also on multiple selections enabled
        #if data is not None:
        #    l.append(data)
    def __getSelectionSng__(self,tr):
        l=[]
        def cmpSel(tr,ti,iMpdSub,l):
            if tr.IsSelected(ti):
                l.append(ti)
                return 1
            return 0
        iRet=tr.WalkDeepFirst(cmpSel,l)
        if iRet>0:
            return False,l[0]
        return False,None
    def __getSelectionMulti__(self,tr):
        l=[]
        def cmpSel(tr,ti,iMpdSub,l):
            if tr.IsSelected(ti):
                l.append(ti)
            return 0
        iRet=tr.WalkDeepFirst(cmpSel,l)
        if len(l)>0:
            return True,l
        return True,[]
    def __walkFind__(self,tr,ti,func,*args,**kwargs):
        if ti.IsOk()==False:
            return None
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            if func(tr,t[0],*args,**kwargs):
                return t[0]
            if tr.ItemHasChildren(t[0]):
                r=self.__walkFind__(tr,t[0],func,*args,**kwargs)
                if r is not None:
                    return r
            t=tr.GetNextChild(ti,t[1])
        return None
    def __walk__(self,tr,ti,func,*args,**kwargs):
        if ti.IsOk()==False:
            return 0
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            iRet=func(tr,t[0],*args,**kwargs)
            if iRet<0:
                return iRet
            if tr.ItemHasChildren(t[0]):
                iRet=self.__walk__(tr,t[0],func,*args,**kwargs)
                if iRet<0:
                    return iRet
            t=tr.GetNextChild(ti,t[1])
        return 0
    def __walkHier__(self,tr,ti,lHier,func,*args,**kwargs):
        bDbg=self.GetVerboseDbg(5)
        
        if ti.IsOk()==False:
            return 0
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            #lH=lHier+[tr.GetItemText(t[0],0)]
            lH=lHier+[tr.GetItemText(t[0])]
            iRet=func(tr,t[0],lH,*args,**kwargs)
            if bDbg:
                self.__logDebug__({'lH':lH,'lHier':lHier,'iRet':iRet})
            if iRet<0:
                return iRet
            if tr.ItemHasChildren(t[0]):
                iRet=self.__walkHier__(tr,t[0],lH,func,*args,**kwargs)
                if bDbg:
                    self.__logDebug__({'lH':lH,'lHier':lHier,'iRet':iRet})
                if iRet<0:
                    return iRet
            t=tr.GetNextChild(ti,t[1])
        return 0
    def __walkDeepFirst__(self,tr,ti,iMod,func,*args,**kwargs):
        if ti.IsOk()==False:
            return iMod
        t=tr.GetFirstChild(ti)
        iModRet=iMod
        while t[0].IsOk():
            if tr.ItemHasChildren(t[0]):
                iRet=self.__walkDeepFirst__(tr,t[0],iMod,func,*args,**kwargs)
                if iRet<0:
                    return iRet
                iModSub=max(iRet,iMod)
            else:
                iModSub=iMod
            iModRet=max(iModSub,iModRet)
            iRet=func(tr,t[0],iModSub,*args,**kwargs)
            if iRet<0:
                return iRet
            iModRet=max(iRet,iModRet)
            t=tr.GetNextChild(ti,t[1])
        return iModRet
    def __walkBreathFirst__(self,tr,ti,iLimit,func,*args,**kwargs):
        if ti.IsOk()==False:
            return None
        t=tr.GetFirstChild(ti)
        while t[0].IsOk():
            ret=func(tr,t[0],*args,**kwargs)
            if ret>0:
                return t[0]
            elif ret<0:
                return None
            t=tr.GetNextChild(ti,t[1])
        if iLimit>0:
            t=tr.GetFirstChild(ti)
            while t[0].IsOk():
                if tr.ItemHasChildren(t[0]):
                    r=self.__walkBreathFirst__(tr,t[0],iLimit-1,func,*args,**kwargs)
                    if r is not None:
                        return r
                t=tr.GetNextChild(ti,t[1])
        return None
    def WalkDeepFirst(self,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return None
            ti=self.wid.GetRootItem()
            iRet=func(self,ti,0,*args,**kwargs)
            if iRet!=0:
                return iRet
            return self.__walkDeepFirst__(self.wid,ti,0,func,*args,**kwargs)
        except:
            self.__logTB__()
        return None
    def WalkBreathFirst(self,ti,iLimit,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return None
            if ti is None:
                ti=self.wid.GetRootItem()
            return self.__walkBreathFirst__(self.wid,ti,iLimit,func,*args,**kwargs)
        except:
            self.__logTB__()
        return None
    def Walk(self,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            self.__walk__(self.wid,self.GetRootItem(),func,*args,**kwargs)
        except:
            self.__logTB__()
    def WalkHier(self,ti,lHier,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            if ti is None:
                ti=self.wid.GetRootItem()
            if lHier is None:
                tp=ti
                lHier=[]
                while tp is not None:
                    lHier.append(self.GetItemText(tp,0))
                    tp=self.GetItemParent(tp)
                    if tp.IsOk()==False:
                        break
                lHier.reverse()
                
            self.__walkHier__(self.wid,ti,lHier,func,*args,**kwargs)
        except:
            self.__logTB__()
    def WalkFind(self,ti,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return None
            if ti is None:
                ti=self.wid.GetRootItem()
            return self.__walkFind__(self.wid,ti,func,*args,**kwargs)
        except:
            self.__logTB__()
        return None
    def WalkSel(self,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            lTi=self.GetSelections()
            for ti in lTi:
                self.__walk__(self.wid,ti,func,*args,**kwargs)
                func(self,ti,*args,**kwargs)
        except:
            self.__logTB__()
    def WalkSelDeepFirst(self,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return -1
            lTi=self.GetSelections()
            iMod=0
            for ti in lTi:
                iRet=self.__walkDeepFirst__(self.wid,ti,0,func,*args,**kwargs)
                if iRet<0:
                    return iRet
                iMod=max(iRet,iMod)
                iRet=func(self,ti,iMod,*args,**kwargs)
                if iRet<0:
                    return iRet
                iMod=max(iRet,iMod)
            return iMod
        except:
            self.__logTB__()
        return None
    def GetItemTup(self,ti,iKind):
        try:
            #bDbg=self.GetVerboseDbg(5)
            #if bDbg:
            #    self.__logDebug__('')
            if self.IsMainThread()==False:
                return None
            if ti.IsOk()==False:
                return None
            tr=self.wid
            if iKind==-1:
                return tr.GetPyData(ti)
            elif iKind<0:
                return None
            elif iKind==0:
                return (tr.GetItemText(ti),tr.GetItemImage(ti))
            else:
                return None
        except:
            self.__logTB__()
        return None
    def __getItemStr__(self,tr,ti,iKind):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            if ti.IsOk()==False:
                return None
            tr=self.wid
            if iKind==-1:
                return tr.GetPyData(ti)
            elif iKind<0:
                return None
            elif iKind==0:
                return tr.GetItemText(ti)
            else:
                return None
        except:
            self.__logTB__()
    def GetItemStr(self,ti,iKind):
        try:
            #bDbg=self.GetVerboseDbg(5)
            #if bDbg:
            #    self.__logDebug__('')
            if self.IsMainThread()==False:
                return None
            return self.__getItemStr__(self.wid,ti,iKind)
        except:
            self.__logTB__()
        return None
    def GetSelections(self):
        try:
            if self.IsMainThread()==False:
                return []
            l=self.wid.GetSelections()
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(l)
            return l
        except:
            self.__logTB__()
        return []
    def GetSelected(self,lMap=None):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return None
            l=self.GetValue(lMap=lMap or [-1,None],ti=-1)
            return l
        except:
            self.__logTB__()
        return None
    def __setSelected__(self,tr,ti,dSel,iKind,bFlag,lSelTi):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__('')
            v=self.__getItemStr__(tr,ti,iKind)
            if v is None:
                self.__logError__('tree item invalid')
            else:
                if v in dSel:
                    dSel[v]=dSel[v]+1
                    if bDbg:
                        self.__logDebugAdd__('select',{'v':v})
                    tr.SelectItem(ti,bFlag)
                    lSelTi.append(ti)
                    if self._bMultiple==False:
                        return -1
        except:
            self.__logTB__()
        return 0
    def SetSelected(self,lSel,iKind=-1,ti=None,bFlag=True,bKeepSel=False,bEnsure=True):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'bEnsure':bEnsure,'iKind':iKind,
                        'bFlag':bFlag,'bKeepSel':bKeepSel,
                        'lSel':lSel,})
            else:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return -2
            tr=self.wid
            if ti is None:
                ti=tr.GetRootItem()
            if self._bMultiple==True:
                if bKeepSel==False:
                    lSelTi=self.GetSelected([-2])
                    for tiOld in lSelTi:
                        if bDbg:
                            self.__logDebugAdd__('deselect',{'tiOld':tiOld})
                        tr.SelectItem(tiOld[0],False)
        except:
            self.__logTB__()
        try:
            dSel={}
            lSelTi=[]
            lSelKey=[]
            for iK in lSel:
                if iK in dSel:
                    self.__logError__(['key already present',iK])
                else:
                    dSel[iK]=0
                    lSelKey.append(iK)
                    if self._bMultiple==False:
                        break
            #dSel[None]=0
            if iKind==-1:
                for iK in lSel:
                    if iK in self.dCache:
                        ti=self.dCache[iK]
                        if bDbg:
                            self.__logDebugAdd__('select',{'iK':iK})
                        tr.SelectItem(ti,bFlag)
                        lSelTi.append(ti)
                        if self._bMultiple==False:
                            break
            else:
                self.__walk__(tr,ti,self.__setSelected__,dSel,iKind,bFlag,lSelTi)
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'bEnsure':bEnsure,
                        'lSel':lSel,'lSelKey':lSelKey,'lSelTi':lSelTi})
            if bEnsure:
                if len(lSelTi)>0:
                    tr.EnsureVisible(lSelTi[0])
            return lSelKey
            return lSelTi
            #return self.__setSelected__(lst,l,bFlag=bFlag)
        except:
            self.__logTB__()
        return -1
    def SetValue(self,l,lHier=None,bChildren=False):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return -2
            
            tr=self.wid
            #iCols=tr.GetColumnCount()
            
            if lHier is None:
                tr.DeleteAllItems()
                ti=tr.AddRoot('')
                if self._bHideRoot:
                    self.__setValue__(tr,ti,l)
                else:
                    self.__setTreeItem__(tr,ti,l)
                    tr.Expand(ti)
                #self.__setValue__(tr,None,l)
            else:
                if type(lHier)==types.ListType:
                    ti=self.__getTreeItemByHier__(tr,tr.GetRootItem(),lHier)
                    if ti is None:
                        self.__logError__('%s;ti not found'%('.'.join(lHier)))
                        return
                else:
                    ti=lHier
                if bChildren==True:
                    self.__setValue__(tr,ti,l)
                else:
                    tc=tr.AppendItem(ti,'')
                    self.__setTreeItem__(tr,tc,l)
                #tc=tr.AppendItem(ti,'')
                #self.__setTreeItem__(tr,tc,l)
        except:
            self.__logTB__()
    def __getHier__(self,tr,ti,l,r):
        #l.append(tr.GetItemText(ti,0))
        if self._bHideRoot:
            if ti==r:
                return
        if ti.IsOk():
            l.append(tr.GetItemText(ti))
            tp=tr.GetItemParent(ti)
            self.__getHier__(tr,tp,l,r)
    def GetHier(self,ti):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return None
            l=[]
            if self._bHideRoot:
                r=self.wid.GetRootItem()
            else:
                r=None
            self.__getHier__(self.wid,ti,l,r)
            l.reverse()
            return l
        except:
            self.__logTB__()
        return None
    def GetValue(self,lMap=None,ti=None):
        try:
            if self.IsMainThread()==False:
                return None
            bDbg=self.GetVerboseDbg(5)
            self.__logDebug__(''%())
            l=[]
            tr=self.wid
            if ti is None:
                ti=tr.GetRootItem()
                #if self._bHideRoot:
                #    self.__logError__('well this is kind of stupid')
            elif type(ti)!=types.IntType:
                if lMap is None:
                    self.__getValueData__(tr,ti,l)
                else:
                    lH=self.GetHier(ti)
                    self.__getValue__(tr,ti,lH,lMap,l)
                if bDbg:
                    self.__logDebug__(l)
                return l
                
            elif ti==-1:
                lTi=self.GetSelections()
                for ti in lTi:
                    if lMap is None:
                        self.__getValueData__(tr,ti,l)
                    else:
                        lH=self.GetHier(ti)
                        self.__getValue__(tr,ti,lH,lMap,l)
                if bDbg:
                    self.__logDebug__(l)
                return l
            if lMap is None:
                self.__getValueData__(tr,ti,l)
                self.__walk__(tr,ti,self.__getValueData__,l)
            else:
                lH=self.GetHier(ti)
                if bDbg:
                    self.__logDebug__({'lH':lH,'ti':ti,
                            'r':self.wid.GetRootItem()})
                self.__getValue__(tr,ti,lH,lMap,l)
                self.__walkHier__(tr,ti,lH,
                                self.__getValue__,lMap,l)
            if bDbg:
                self.__logDebug__(l)
            return l
        except:
            self.__logTB__()
        return None
