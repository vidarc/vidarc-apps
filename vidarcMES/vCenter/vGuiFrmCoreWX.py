#----------------------------------------------------------------------------
# Name:         vGuiFrmCoreWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120215
# CVS-ID:       $Id: vGuiFrmCoreWX.py,v 1.19 2016/02/25 14:17:49 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import os,types,sys,time
    import Queue
    import vRingBuffer
    from vGuiCoreWX import vGuiCoreWX,vGuiCoreTopWidReg,vGuiCoreTopWidUnReg
    #import vidarc.config.vcLog as vcLog
    #VERBOSE=vcLog.is2Log(__name__,'__VERBOSE__')
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)


wxEVT_V_STATUSBAR_PRINT=wx.NewEventType()
vEVT_V_STATUSBAR_PRINT=wx.PyEventBinder(wxEVT_V_STATUSBAR_PRINT,1)
def EVT_V_STATUSBAR_PRINT(win,func):
    win.Connect(-1,-1,wxEVT_V_STATUSBAR_PRINT,func)
def EVT_V_STATUSBAR_PRINT_DISCONNECT(win,func):
    win.Disconnect(-1,-1,wxEVT_V_STATUSBAR_PRINT,func)
class vStatusBarPrint(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_V_STATUSBAR_PRINT(<widget_name>, self.OnProc)
    """
    def __init__(self,iPos,sVal):
        wx.PyEvent.__init__(self)
        self.SetEventType(wxEVT_V_STATUSBAR_PRINT)
        self.iPos=iPos
        self.sVal=sVal
    def GetPos(self):
        return self.iPos
    def GetVal(self):
        return self.sVal

class vStatusBarMessageLineTransientPopup(wx.Dialog):
    #SIZE_PN_SCROLL=30
    def __init__(self, parent, size,style,name=''):
        self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
        wx.Dialog.__init__(self, parent,style=wx.RESIZE_BORDER)#wx.BORDER_SIMPLE)#|wx.STAY_ON_TOP)
        id=wx.NewId()
        wx.EVT_SIZE(self,self.OnSize)
        #self.cbCancel = wx.BitmapButton(id=-1,
        #      bitmap=images.getCancelBitmap(), name=u'cbCancel',
        #      parent=self, pos=(0,0), size=(30,30), style=wx.BU_AUTODRAW)
        #self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
        #      self.cbCancel)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        if size[1]<40:
            size=(size[0],40)
        i=0
        iY=40
        iH=size[1]+4
        iSzHeight=0
        id=wx.NewId()
        self.lstMsg = wx.ListCtrl(id=id, name=u'lstStatusMsg',
              parent=self, pos=wx.DefaultPosition, size=wx.DefaultSize,
              style=wx.LC_REPORT)
        #self.lstMsg.Bind(wx.EVT_LIST_COL_CLICK, self.OnLstBitsListColClick,id=id)
        #self.lstMsg.Bind(wx.EVT_LIST_ITEM_DESELECTED,self.OnLstBitsListItemDeselected, id=id)
        #self.lstMsg.Bind(wx.EVT_LIST_ITEM_SELECTED,self.OnLstBitsListItemSelected, id=id)
        self.lstMsg.InsertColumn(col=0, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('message'), width=280)
        self.lstMsg.InsertColumn(col=2, format=wx.LIST_FORMAT_LEFT,
                                    heading=_('time'), width=120)
        self.selIdx=-1
        bxs.AddWindow(self.lstMsg, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.SetSizer(bxs)
        self.Layout()
        
        self.szOrig=(240,140)
        self.SetSize(self.szOrig)
    def OnSize(self,evt):
        iW,iH=self.GetSize()
        if iW<self.szOrig[0]:
            iW=self.szOrig[0]
        if iH<self.szOrig[1]:
            iH=self.szOrig[1]
        self.SetSize((iW,iH))
        iWCol=self.lstMsg.GetColumnWidth(1)
        self.lstMsg.SetColumnWidth(0,iW-iWCol-20)
        evt.Skip()
    def OnCbCancelButton(self,evt):
        self.Show(False)
    def AddMsg(self,sMsg,sTime,iPos=sys.maxint):
        if sMsg is None:
            self.lstMsg.DeleteAllItems()
        else:
            idx=self.lstMsg.InsertStringItem(iPos,sMsg)
            self.lstMsg.SetStringItem(idx,1,sTime)

class vGuiFrmCoreWX(vGuiCoreWX):
    #def __init__(self,sOrigin=None,bExtended=False):
    #    vGuiCoreWX.__init__(self,sOrigin=sOrigin,bExtended=bExtended)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            self.SIZE_PN_SCROLL=wx.SystemSettings.GetMetric(wx.SYS_VSCROLL_X)
            self.bTopWid=False
        except:
            self.__logTB__()
        #self._mnbMain=None
        #self._tbMain=None
    def EnableTopWid(self,bFlag):
        try:
            self.__logDebug__('bFlag:%d;bTopWid:%d'%(bFlag,self.bTopWid))
            self.bTopWid=bFlag
            if bFlag:
                vGuiCoreTopWidReg(self)
            else:
                iIdx=vGuiCoreTopWidUnReg(self)
                self.__logDebug__('top widget unregistered:%d'%(iIdx))
        except:
            self.__logTB__()
    def IsTopWid(self):
        return self.bTopLevel
    def GetStatusBar(self):
        return getattr(self,'_sbMain')
    def __createSched__(self):
        try:
            self.__logDebug__(''%())
            if hasattr(self,'oSched')==False:
                self.oSched=vpc.vcpScheduler()
                self.oSched.Inz(self.GetOrigin(),"sched")   # 20160225 wro:use common interface
                self.oSched.SetTimeBase(0.1)
                self.oSched.Start()
        except:
            self.__logTB__()
    def AddStatusBar(self,l,zClrTime=0):
        """
        l=[
            [sType  ,sName  ,iSz    ,func   ,args   ,kwargs ],
            ...
            ]
        sType   ... lbl     static label
                ... txt     text
                ... cb      command button
                ... tg      toggle button
                ... log     log counter
                ... prc     processbar
                ... gag     custom gauge
                ... msg     print message
                ... clk     clock
        iSz     ... x       number > 0 ... fixed size, < 0 growable
                    s       default string
                    
        """
        try:
            if hasattr(self,'_sbMain')==False:
                iId=wx.NewId()
                self._sbMain=wx.StatusBar(id=iId,
                    name=u'sbMain',parent=self.GetWid(),style=wx.ST_SIZEGRIP)
                oFont=self.GetFontByName('fixed')
                self._sbMain.SetFont(oFont)
                self._sbMain.SetFieldsCount(len(l))
                self._dStatusBarId={}
                self._dStatusBar={}
                self._lStatusBar=[]
                lSz=[]
                for t in l:
                    sType=t[0]
                    iSz=t[2]
                    sExt=None
                    t=type(iSz)
                    if t==types.StringType:
                        sExt=iSz
                    elif t==types.UnicodeType:
                        sExt=iSz
                    elif t==types.IntType:
                        pass
                    else:
                        iSz=-1
                    if sType=='clk':
                        #sExt='XXXX YYYY-MM-DD HH:MM:SS +00:00 xx:000xxxx'
                        sExt='YYYY-MM-DD HH:MM:SS (00,000) xxxxx'
                    
                    if sExt is not None:
                        tSz=self._sbMain.GetTextExtent(sExt)
                        iSz=tSz[0]
                    lSz.append(iSz)
                self._sbMain.SetStatusWidths(lSz)
                self._sbMain.Bind(wx.EVT_RIGHT_UP, self.OnSbMainRightUp)
                #self._sbMain.Bind(wx.EVT_SIZE, self.OnSizeStatusBar)
                #wx.EVT_SIZE(self._sbMain,self.DoSizeStatusBar)
                self.SetStatusBar(self._sbMain)
                
                iOfs=0
                bkgCol=wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENU)
                for t in l:
                    sType=t[0]
                    sName=t[1]
                    rect=self._sbMain.GetFieldRect(iOfs)
                    iId=None
                    w=None
                    if sType=='lbl':
                        iId=wx.NewId()
                        w=wx.TextCtrl(self._sbMain,iId,
                            pos=(rect.x+2, rect.y+2),
                            size=(rect.width-4, rect.height-4),
                            style=wx.TE_READONLY|wx.NO_BORDER|wx.NO_3D)
                        w.SetBackgroundColour(bkgCol)
                        w.SetFont(oFont)
                    elif sType=='txt':
                        iId=wx.NewId()
                        w=wx.TextCtrl(self._sbMain,iId,
                            pos=(rect.x+2, rect.y+2),
                            size=(rect.width-4, rect.height-4),
                            style=wx.TE_READONLY|wx.NO_BORDER|wx.NO_3D)
                        w.SetBackgroundColour(bkgCol)
                        w.SetFont(oFont)
                    elif sType=='log':
                        iId=wx.NewId()
                        w=wx.TextCtrl(self._sbMain,iId,
                            pos=(rect.x+2, rect.y+2),
                            size=(rect.width-4, rect.height-4),
                            style=wx.TE_READONLY|wx.NO_BORDER|wx.NO_3D)
                        w.SetBackgroundColour(bkgCol)
                        w.SetFont(oFont)
                        self._txtLog=w
                        self._txtLog.Bind(wx.EVT_RIGHT_DOWN, self.OnTextLogRightDown)
                        self._txtLog.Bind(wx.EVT_MIDDLE_DOWN, self.OnTextLogMiddleDown)
                        self._dlgLog=None
                    elif sType=='msg':
                        iId=wx.NewId()
                        w=wx.TextCtrl(self._sbMain,iId,
                            pos=(rect.x+2, rect.y+2),
                            size=(rect.width-4, rect.height-4),
                            style=wx.TE_READONLY|wx.NO_BORDER|wx.NO_3D)
                        w.SetBackgroundColour(bkgCol)
                        w.SetFont(oFont)
                        self._txtMsg=w
                        self._txtMsg.Bind(wx.EVT_RIGHT_DOWN, self.OnMsgRightDown)
                        #wx.EVT_RIGHT_DOWN(self._txtMsg,self.OnMsgRightDown)#,self._txtMsg)
                        #self._txtMsg.Bind(wx.EVT_MIDDLE_DOWN, self.OnMsgMiddleDown)
                        self._dlgMsg=None
                        self._iMsgNum=iOfs
                        self.zMsgLast=time.clock()
                        #self.__createPopupStatusBar__()
                        self.__createSched__()
                        
                        EVT_V_STATUSBAR_PRINT(self._sbMain,self._doPrintMsg)
                        #self.popWinSb=None
                        self._qMsg=Queue.Queue()
                        self._zClrTime=zClrTime
                        if zClrTime>0:
                            #self._zMsgTimer = wx.PyTimer(self.NotifyClrMsgLine)
                            self._rbMsg=None
                            self.oSched.FuncAdd(vpc.vcpSchedulerFuncID_TIMEOUT_0,
                                    self.NotifyClrMsgLine)
                        else:
                            self._rbMsg=vRingBuffer.vRingBuffer(100)
                            #self.semStatusBar=threading.Semaphore()
                    elif sType=='clk':
                        iId=wx.NewId()
                        w=wx.TextCtrl(self._sbMain,iId,
                            pos=(rect.x+2, rect.y+2),
                            size=(rect.width-4, rect.height-4),
                            style=wx.TE_READONLY|wx.NO_BORDER|wx.NO_3D)
                        w.Bind(wx.EVT_CONTEXT_MENU,self.OnContextClk)
                        w.SetBackgroundColour(bkgCol)
                        w.SetFont(oFont)
                        self._txtClk=w
                        self._zClk=vpc.vctDateTime(1)
                        self._zClk.Now()
                        w.SetValue(self._zClk.GetDateTimeStr(' '))
                        
                        self.__createSched__()
                        self.oSched.FuncAdd(vpc.vcpSchedulerFuncID_BASE,
                                self.OnNotify)
                    elif sType=='tg':
                        iId=wx.NewId()
                        w=wx.lib.buttons.GenBitmapToggleButton(ID=iId,
                            bitmap=wx.EmptyBitmap(16, 16), name=sName, 
                            parent=self._sbMain,
                            pos=(rect.x+2, rect.y+2), size=(rect.width-4, rect.height-4),
                            style=0)
                    elif sType=='prc':
                        iId=wx.NewId()
                        w=wx.Gauge(self._sbMain, iId, 50, (rect.x+2, rect.y+2),
                                (rect.width-4,rect.height-4),
                                wx.GA_HORIZONTAL|wx.GA_SMOOTH|wx.GA_PROGRESSBAR#|wx.NO_BORDER#|wx.NO_3D
                                )
                        w.SetRange(1000)
                        w.SetValue(0)
                        self.gProcess=w
                    elif sType=='gag':
                        iId=wx.NewId()
                        w=wx.Gauge(self._sbMain, iId, 50, (rect.x+2, rect.y+2), 
                                (rect.width-4, rect.height-4), 
                                wx.GA_HORIZONTAL|wx.GA_SMOOTH|wx.NO_BORDER#|wx.NO_3D
                                )
                    if sName is not None:
                        setattr(self,sName,w)
                    self._lStatusBar.append(w)
                    try:
                        if iId is not None:
                            if len(t)>5:
                                self._dStatusBarId[iId]=(t[3],t[4],t[5])
                    except:
                        self.__logTB__()
                    iOfs=iOfs+1
        except:
            self.__logTB__()
    def DoSizeStatusBar(self):
        try:
            if hasattr(self,'_sbMain'):
                self.__logDebug__('')
                iOfs=0
                for w in self._lStatusBar:
                    rect=self._sbMain.GetFieldRect(iOfs)
                    if w is not None:
                        w.Move((rect.x+2,rect.y+2))
                        w.SetSize((rect.width-4,rect.height-4))
                    iOfs=iOfs+1
                #print evt.GetSize(),evt.GetEventType()
        except:
            self.__logTB__()
    def OnNotify(self):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__('')
            self.CB(self.Notify)
        except:
            self.__logTB__()
        return 0
    def OnTextLogRightDown(self,evt):
        try:
            self.__logDebug__('')
        except:
            self.__logTB__()
    def OnTextLogMiddleDown(self,evt):
        try:
            self.__logDebug__('')
        except:
            self.__logTB__()
    def OnSbMainRightUp(self,evt):
        try:
            self.__logDebug__('')
        except:
            self.__logTB__()
    def GetToolBar(self):
        return getattr(self,'_tbMain')
    def AddToolBar(self,iOfs,l,sz=(20,20),sName='_tbMain'):
        try:
            if hasattr(self,'_tbMain')==False:
                iId=wx.NewId()
                self._tbMain = wx.ToolBar(id=iId,
                    name=u'tbMain', parent=self.GetWid(), pos=wx.Point(0,0),
                    size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
                self._tbMain.SetToolBitmapSize(sz)
                self.SetToolBar(self._tbMain)
                self._dToolBarId={}
                self._dToolBar={}
                if self._bLayout==True:
                    wx.CallAfter(self.gbsData.Layout)
                    wx.CallAfter(self.gbsData.Fit,self)
                
            tb=getattr(self,'_tbMain')
            d=getattr(self,'_dToolBar')
            dId=getattr(self,'_dToolBarId')
            for tup in l:
                t=type(tup)
                if tup is None:
                    tb.InsertSeparator(iOfs)
                    iOfs+=1
                elif t==types.ListType:
                    pass
                elif t==types.TupleType:
                    if tup[0] in d:
                        self.__logError__('key:%s already in d;%s'%(tup[0],
                                self.__logFmt__(d)))
                        continue
                    if tup[1]=='txt':
                        iId=wx.NewId()
                        txtCtrl=wx.TextCtrl(tb, iId, tup[2], size=(150, -1))
                        if tup[3] is not None:
                            pass
                        #tb.AddControl(txtCtrl)
                        tb.InsertControl(iOfs,txtCtrl)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
                    elif tup[1]=='tg':
                        iId=wx.NewId()
                        if type(tup[2])==types.TupleType:
                            bmp0=tup[2][0]
                            bmp1=tup[2][1]
                        else:
                            bmp0=tup[2]
                            bmp1=None
                        tgCtrl=wx.lib.buttons.GenBitmapToggleButton(bitmap=bmp0,
                                id=iId, name=tup[0],parent=tb, 
                                pos=wx.DefaultPosition, size=wx.DefaultSize,
                                style=0)
                        if bmp1 is not None:
                            tgCtrl.SetBitmapSelected(bmp1)
                        tb.InsertControl(iOfs,tgCtrl)
                        tb.Bind(wx.EVT_BUTTON, self.OnAddToolBar, id=iId)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
                    elif tup[1]=='bt':
                        iId=wx.NewId()
                        #tb.InsertSimpleTool(iOfs,iId, tup[2],  
                        tb.InsertTool(iOfs,iId, tup[2],  
                                shortHelpString=tup[3])
                        tb.Bind(wx.EVT_TOOL, self.OnAddToolBar, id=iId)
                        d[tup[0]]=iId
                        dId[iId]=(tup[4],tup[5],tup[6])
                        iOfs+=1
            tb.Realize()
        except:
            self.__logTB__()
    def OnAddToolBar(self,evt):
        try:
            eId=evt.GetId()
            if eId in self._dToolBarId:
                t=self._dToolBarId[eId]
                t[0](*t[1],**t[2])
        except:
            self.__logTB__()
    def EnableTool(self,v,bFlag=True):
        try:
            if type(v)==types.ListType:
                for k in v:
                    if k in self._dToolBar:
                        self._tbMain.EnableTool(self._dToolBar[k],bFlag)
            else:
                if v in self._dToolBar:
                    self._tbMain.EnableTool(self._dToolBar[v],bFlag)
        except:
            self.__logTB__()
    def AddMenu(self,iOfs,l,lHier=None):
        try:
            bDbg=True
            if VERBOSE>10:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg:
                self.__logDebug__({'iOfs':iOfs,'l':l,'lHier':lHier})
            if hasattr(self,'_mnbMain')==False:
                mnb=wx.MenuBar()
                setattr(self,'_mnbMain',mnb)
                self.SetMenuBar(mnb)
                if self._bLayout==True:
                    wx.CallAfter(self.gbsData.Layout)
                    wx.CallAfter(self.gbsData.Fit,self)
            if hasattr(self,'_mnbMain'):
                mnb=getattr(self,'_mnbMain')
                mn=None
                if lHier is not None:
                    mn=mnb
                    iPos=lHier[0]
                    iCnt=mn.GetMenuCount()
                    if iPos>=iCnt:
                        self.__logError__('iPos:%d;iCnt:%d'%(iPos,iCnt))
                        mn=mn.GetMenu(iCnt-1)
                    else:
                        mn=mn.GetMenu(iPos)
                    for iPos in lHier[1:]:
                        iCnt=mn.GetMenuItemCount()
                        if iPos>=iCnt:
                            self.__logError__('iPos:%d;iCnt:%d'%(iPos,iCnt))
                            mn=mn.GetMenuItems(iCnt-1)
                        else:
                            mn=mn.GetMenuItems(iPos)
                #mn=getattr(self,sMnName)
                if bDbg:
                    self.__logDebug__({'mn':mn})
                dId,dCall,widEvt=self.GetMenu()
                self.AddMenuItems(mn,dCall,iOfs,l,mnb,dId,widEvt,bDbg)
                #if self.__isLogDebug__():
                #    self.__logDebug__('menu:%s;d:;%s'%(sMnName,
                #            vtLog.pformat(self._popIdAddMenuItems)))
            else:
                self.__logError__('menu:%s not found'%(sMnName))
        except:
            self.__logTB__()
    def OnAddMenuItems(self,evt):
        try:
            eId=evt.GetId()
            sLang=None
            self.__logDebug__('eId:%d processing;'%(eId))
            if eId in self._dMenuIdSys:
                t=self._dMenuIdSys[eId]
                if t[1] is None:
                    self.DoMenuAction(t[0])
                else:
                    t[1](*t[2],**t[3])
            else:
                self.__logError__(['eId:%d not found;'%(eId),
                        self._popIdAddMenuItems])
        except:
            self.__logTB__()
    def EnableMenu(self,v,bFlag=True):
        try:
            if type(v)==types.ListType:
                for k in v:
                    if k in self._dMenu:
                        self._mnbMain.Enable(self._dMenu[k],bFlag)
            else:
                if v in self._dMenu:
                    self._mnbMain.Enable(self._dMenu[v],bFlag)
        except:
            self.__logTB__()
    def DoMenuAction(self,sAct):
        try:
            self.__logDebug__('sAct:%s'%(sAct))
        except:
            self.__logTB__()
    # ++++++++++
    # statusbar
    # processbar
    def SetProcBarVal(self,iVal,iCount=-1,iRange=1000,iNum=0,bForce=False):
        if self.IsMainThread()==False:
            return
        try:
            #self.gProcess.SetRange(iCount())
            def getNormalized(iVal,iCount):
                f=iVal/float(iCount)
                if f>1.0:
                    f=1.0
                if f<0.0:
                    f=0.0
                return long(f*iRange)
            if iCount>0:
                self.iProcCount=iCount
            else:
                iCount=self.iProcCount
            self.gProcess.SetValue(getNormalized(iVal,iCount))
        except:
            self.__logTB__()
    # message bar
    def _doPrintMsg(self,evt):
        iInsPos,iInsLast=self._txtMsg.GetSelection()
        self._txtMsg.SetValue(evt.GetVal())
        if iInsPos>=0:
            self._txtMsg.SetSelection(iInsPos,iInsLast)
    def PostPrint(self,iPos,sVal):
        wx.PostEvent(self._sbMain,vStatusBarPrint(iPos,sVal))
    def PrintMsg(self,s,bForce=False,bImmediately=False):
        """ put a message string into queue for delayed display on status bar. this method is thread safe.
        """
        # 061009 wro
        # attention do only add information to queue, never ever set statusbar info here!!!
        # this method may be called by threads, therefore a possible wx-crash may be cased!!! 
        #return
        if bForce:
            self.PostPrint(0,s)
        if 0 and bImmediately:
            try:
                self._sbMain.SetStatusText(s, self.iNum)
            except:
                pass
        if self._zClrTime>0:
            #self._zMsgTimer.Start(self._zClrTime,True)
            self.oSched.SetTimeOut(0,self._zClrTime)
            self.PostPrint(0,s)
        else:
            #self.semStatusBar.acquire()
            try:
                self._zClk.Now()
                tup=(s,self._zClk.GetDateTimeStr(' '))
                self._rbMsg.put(tup)
                self._qMsg.put(tup)
            except:
                pass
                self.__logTB__()
            #self.semStatusBar.release()
    def GetPrintMsgTup(self):
        if self._qMsg.empty():
            return None
        else:
            return self._qMsg.get()
    def NotifyClrMsgLine(self):
        self.PostPrint(0,'')
    def __createPopupStatusBar__(self):
        if self._dlgMsg is None:
            sz=self._sbMain.GetSize()
            sz=(sz[0],sz[1]-20)
            try:
                self._dlgMsg=vStatusBarMessageLineTransientPopup(self.frm,sz,wx.SIMPLE_BORDER)
            except:
                self.__logTB__()
        else:
            pass
    def ShowPopupStatusBar(self):
        self.__createPopupStatusBar__()
        if self._dlgMsg.IsShown()==False:
            btn=self._sbMain
            rect = self._sbMain.GetFieldRect(self._iMsgNum)
            iX,iY = btn.ClientToScreen( (0,0) )
            iDX,iDY = rect.x+2, rect.y+2
            iX+=iDX
            #iY+=iDY
            iW,iH =  rect.width-4, rect.height-4
            iPopW,iPopH=self._dlgMsg.GetSize()
            iScreenW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iScreenH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            if iX+iPopW>iScreenW:
                iX=iScreenW-iPopW-4
                if iX<0:
                    iX=0
            iY-=iPopH
            if iY+iPopH>iScreenH:
                iY=iScreenH-iPopH
                if iY<0:
                    iY=0
            
            self._dlgMsg.Move((iX,iY))
            self._dlgMsg.SetSize((iW,50))
            self._dlgMsg.Show(True)
            return True
        else:
            self._dlgMsg.Show(False)
            return False
    def IsShownPopupStatusBar(self):
        if self._dlgMsg is None:
            return False
        return self._dlgMsg.IsShown()
    def AddMsg2PopupStatusBar(self,tup,iPos=sys.maxint):
        if self._dlgMsg is not None:
            if tup is None:
                return
            sMsg,sTime=tup[0],tup[1]
            self._dlgMsg.AddMsg(sMsg,sTime,iPos)
    def OnMsgRightDown(self,evt):
        try:
            self.__logDebug__('')
            if self._rbMsg is not None:
                if self.ShowPopupStatusBar():
                    self.AddMsg2PopupStatusBar((None,None))
                    self._rbMsg.proc(self.AddMsg2PopupStatusBar)
        except:
            self.__logTB__()
    def __SetStatusValueWid_(self,w,s):
        iInsPos,iInsLast=w.GetSelection()
        w.SetValue(s)
        if iInsPos>=0:
            w.SetSelection(iInsPos,iInsLast)
    def OnContextClk(self,evt):
        #evt.Skip()
        pass
    def Notify(self):
        # 061006 wro read out queue infos and display them in statusbar and related popup-window
        try:
            if hasattr(self,'oSched'):
                #self.oSched.LogOrg(vpc.vLog_DEBUG,0xffff)
                if self.oSched.IsBusy()==False:
                    # 20141207 wro: scheduler is not running anymore
                    return
            if self.IsMainThread()==False:
                return
            
            bVidLog=False
            self._zClk.Now()
            str=self._zClk.GetDateTimeExtStr(' ')
            #t = time.localtime(time.time())
            #str = time.strftime(_("%Y-%m-%d %H:%M:%S wk:%W"), t)
            self.__SetStatusValueWid_(self._txtClk,str)
            
            #self.SetStatusText(st, STATUS_CLK_POS)
            self.__makeTitle__()
            def showMsg():
                tup=self.GetPrintMsgTup()
                if tup is not None:
                    #self.rbMsg.put(tup)
                   # self.sb.SetStatusText(tup[0], self.iNum)
                    bIsShown=self.IsShownPopupStatusBar()
                    self.zMsgLast=time.clock()
                    while tup is not None:
                        if bIsShown:
                            self.AddMsg2PopupStatusBar(tup,0)
                        tupLast=tup
                        tup=self.GetPrintMsgTup()
                    #self.sb.SetStatusText(tupLast[0], self.iNum)
                    self.__SetStatusValueWid_(self._txtMsg,tupLast[0])
                else:
                    if (time.clock()-self.zMsgLast)>5:
                        #self.sb.SetStatusText('', self.iNum)
                        self.__SetStatusValueWid_(self._txtMsg,'')
            showMsg()
        except:
            self.__logTB__()
    def __getTitleInfo__(self):
        return None
    def __makeTitle__(self):
        pass
    def IsShutDownActive(self):
        try:
            self.__logDebug__('')
            return False
        except:
            self.__logTB__()
    def IsShutDownFinished(self):
        try:
            self.__logDebug__('')
            if hasattr(self,'oSched'):
                #self.oSched.LogOrg(vpc.vLog_DEBUG,0xffff)
                if self.oSched.IsBusy():
                    return False;
            return True
        except:
            self.__logTB__()
    def ShutDown(self):
        try:
            self.__logDebug__('')
            if hasattr(self,'oSched'):
                #self.oSched.LogOrg(vpc.vLog_DEBUG,0xffff)
                self.oSched.Stop()
        except:
            self.__logTB__()
    def DoShutDown(self):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__('')
            self.Close()
        except:
            self.__logTB__()
