#----------------------------------------------------------------------------
# Name:         vGuiChoiceWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20121214
# CVS-ID:       $Id: vGuiChoiceWX.py,v 1.11 2015/08/03 07:22:57 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import os,types
    import vLang
    import vSystem
    from vGuiTextDataWX import vGuiTextDataWX
    #from vGuiFrmWX import vGuiFrmWX
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)

class vGuiChoiceWX(vGuiTextDataWX):
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                dPossible=None,
                lWid=[],
                cols=[
                    (u'',-1,32,0),
                    (_(u'name',-1,-1,1),
                    (_(u'sys'),-1,-1,1),
                    (_(u'status'),-1,-2,1),
                    ],
                size_list=(120,60),
                bAnchor=True,iArrange=-1
                kwVal={'pos'...}):
    """
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            vGuiTextDataWX.__initObj__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            self._iSelDat=None
        except:
            self.__logTB__()
    def __initProperties__(self,*args,**kwargs):
        try:
            vGuiTextDataWX.__initProperties__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            lMapGet=kwargs.get('mapGet',None)
            if lMapGet is None:
                self.lMapGet=[-1,0]
            else:
                self.lMapGet=lMapGet
        except:
            self.__logTB__()
    def __get_widget_lst__(self,*args,**kwargs):
        try:
            lW=vGuiTextDataWX.__get_widget_lst__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'lW':lW})
            if len(lW[0])>3:
                dKw=lW[0][3]
                if 'evt' in dKw:
                    dEvt=dKw['evt']
                else:
                    dEvt={}
                    dKw['evt']=dEvt
                dEvt['lfDn']=self.OnTxtLeft
            if bDbg:
                self.__logDebug__({'lW':lW})
            return lW
        except:
            self.__logTB__()
        return []
    def __setUpPopUp__(self,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        try:
            global _
            _=vLang.assignPluginLang('vGui')
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            if 'popup' in kwargs:
                return vGuiTextDataWX.__setUpPopUp__(self,*args,**kwargs)
            
            szCb=self.GetDft('size_button',(24,24),kwargs)
            szLst=self.GetDft('size_list',(120,100),kwargs)
            lCols=kwargs.get('cols',[(_(u'name'),-1,-1,0)])
            lMap=kwargs.get('map',[('sN',0)])
            lVal=kwargs.get('choice',[])
            lSel=kwargs.get('sel',None)
            if lSel is None:
                oTmp=kwargs.get('value',None)
                if oTmp is None:
                    lSel=[0]
                else:
                    t=type(oTmp)
                    if t==types.StringType:
                        lSel=([oTmp],[0])
                    elif t==types.UnicodeType:
                        lSel=([oTmp],[0])
                    else:
                        lSel=oTmp
                    if bDbg:
                        self.__logDebug__({'lSel':lSel,'oTmp':oTmp,'t':t})
            dPopup={
                'name':'popPos',
                'parent':self.wid,'kind':'popSml',#'popupCls',
                'iArrange':0,
                'widArrange':self.wid,
                'bAnchor':True,
                'lGrowRow':[(0,1),],
                'lGrowCol':[(0,1),],
                'lWid':[
                    ('lstCtrl', (0,0),(1,1),{'name':'lstPos','size':szLst,
                        'mod':0,'cols':lCols,'map':lMap,
                        'value':lVal,'sel':lSel,
                        },{
                        'dblClk':self.OnLstPosOk,
                        'key':self.OnLstPosKey,
                        }),
                    ]
            }
            vGuiTextDataWX.__setUpPopUp__(self,*args,**{'popup':dPopup})
            if lSel is not None:
                #self.CB(self.OnPopupOk,None)
                self.CB(self.__getSel__)
        except:
            self.__logTB__()
    # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # possible
    def SetValueDict(self,dVal):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'dVal':dVal})
            self.widPop.pn.lstPos.SetSelected(dVal)
            self.CB(self.__getSel__)
        except:
            self.__logTB__()
    def GetValueDict(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            dVal=self.widPop.pn.lstPos.GetSelected(lMap=self.lMapGet)
            if bDbg:
                self.__logDebug__(dVal)
            return dVal
        except:
            self.__logTB__()
        return None
    def SetValue(self,sVal):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'sVal':sVal})
            
            vGuiTextDataWX.SetValue(self,sVal)
            #pn=self.GetPanel()
            #pn.SetModified(False,pn.txtOld)
        except:
            self.__logTB__()
    def SetSel(self,):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def GetPanel(self):
        return self.popFrm
    def SetChoice(self,lChoice):
        try:
            self.widPop.pn.lstPos.SetValue(lChoice)
        except:
            self.__logTB__()
    def OnLstPosOk(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if evt is not None:
                evt.Skip()
            self.OnPopupOk(None)
        except:
            self.__logTB__()
    def OnLstPosKey(self,evt):
        try:
            #if evt is not None:
            #    evt.Skip()
            bDbg=self.GetVerbose(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def OnPopupButton(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            vGuiTextDataWX.OnPopupButton(self,evt)
            self.widPop.pn.lstPos.SetFocus()
        except:
            self.__logTB__()
    def OnPopupCmd(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def __getSel__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            lVal=self.widPop.pn.lstPos.GetSelected([-1,0])
            if bDbg or 1:
                self.__logDebug__(lVal)
            if len(lVal)>0:
                sOld=vGuiTextDataWX.GetValue(self)
                sVal=lVal[0][1]
            else:
                sOld=vGuiTextDataWX.GetValue(self)
                sVal=''
            if sOld is not None:
                if type(sOld)==types.ListType:
                    sOld=sOld[0]
            vGuiTextDataWX.SetValue(self,[sVal])
            return sOld,sVal,lVal[0][0],lVal[0]
        except:
            self.__logTB__()
            return None,None,None,None
    def OnPopupOk(self,evt):
        if evt is not None:
            evt.Skip()
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            sOld,sVal,iDat,lV=self.__getSel__()
            if self._iSelDat!=iDat:
            #if sOld!=sVal:
                #self.PostADT('mod',sOld,sVal)
                self.PostNFY('sel',lV)
                self.PostNFY('mod',[sOld,sVal,iDat])
                self.PostMOD('val',sVal)
                lVal=self.widPop.pn.lstPos.GetSelected([-3,-2,-1,0])
                if len(lVal)>0:
                    l=lVal[0]
                    dDat={'ref':l[0],'idx':l[1],'dat':l[2],'txt':l[3]}
                    self.PostNFY('dat',dDat)
                self._iSelDat=iDat
            self.PostOk('ok',lV)
        except:
            self.__logTB__()
        self.__Close__()
    def OnTxtLeft(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(10)
        except:
            self.__logTB__()
    def OnKeyPressed(self,evt):
        try:
            if self.IsMainThread()==False:
                return
            bDbg=self.GetVerboseDbg(10)
            key=self.GetKeyCode(evt)
            dKeyMod=self.GetKeyModifiers(evt)
            if bDbg:
                self.__logDebug__({'key':key,'dKeyMod':dKeyMod})
            lstPos=self.widPop.pn.lstPos
            iIdx=None
            sK=chr(dKeyMod['key'])
            iIdx=lstPos.FindChar(sK,iSel=1)
            #lIdx=lstPos.FindValue([sK+'*'],lMapChk=[0],lMapGet=[-1,0],
            #        idx=iIdx,iMode=1)
            if iIdx is not None:
                #self.CB(self.__getSel__)
                self.CB(self.OnPopupOk,None)
        except:
            self.__logTB__()
