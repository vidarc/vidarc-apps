#----------------------------------------------------------------------
# Name:         vGuiFileBrowser.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121214
# CVS-ID:       $Id: vGuiFileBrowser.py,v 1.5 2015/02/22 11:37:16 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import os
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiFileBrowser(vGuiCoreWX):
    SIZE_DFT=(76,24)
    def __initWid__(self,*args,**kwargs):
        #vGuiCoreWX.__init__(self)
        #self.SetVerbose(__name__)
        
        szCb=kwargs.get('size_button',wx.Size(32,32))
        sz=kwargs.get('size',self.SIZE_DFT)
        bmp=kwargs.get('bmp',None)
        bAnchor=kwargs.get('bAnchor',False)#True)
        iArrange=kwargs.get('iArrange',0)
        wid=kwargs.get('wid',None)
        if bmp is None:
            bmp=self.getBmp('core','Open')
        
        _args,_kwargs=self.GetGuiArgs(kwargs,
                ['id','name','parent','pos','size','style'],
                {'pos':(0,0),'size':(-1,-1),'style':wx.TAB_TRAVERSAL})
        self.wid=wx.Panel(*_args,**_kwargs)
        self.wid.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        _args,_kwargs=self.GetGuiArgs({},
                ['id','name','parent','pos','size','style'],
                {
                'pos':(0,0),'size':sz,'style':0,
                'name':'txtFN','parent':self.wid,
                })
        self.txtFN = wx.TextCtrl(*_args,**_kwargs)
        bxs.AddWindow(self.txtFN, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.txtFN.Bind(wx.EVT_TEXT,self.OnTxtFileNameText)
        self.txtFN.Bind(wx.EVT_TEXT_ENTER,self.OnTxtFileNameEnter)
        
        _args,_kwargs=self.GetGuiArgs({},
                ['id','name','parent','pos','size','style','bitmap'],
                {
                'pos':(0,0),'size':szCb,'style':wx.BU_AUTODRAW,
                'name':'cbBmp','parent':self.wid,
                'bitmap':bmp,
                })
        self.cbBmp=wx.lib.buttons.GenBitmapButton(*_args,**_kwargs)
        self.cbBmp.Bind(wx.EVT_BUTTON,self.OnCbBrowseClick,self.cbBmp)
        bxs.AddWindow(self.cbBmp, 0, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.bSave=kwargs.get('bSave',False)
        self.bMulti=kwargs.get('bMulti',False)
        self.bPosixFN=kwargs.get('bPosixFN',True)
        self.sMsg=kwargs.get('sMsg',_(u'choose file'))
        self.sWildCard=kwargs.get('sWildCard',_(u'xml files|*.xml|all files|*.*'))
        self.sDftFN=kwargs.get('sDftFN',u'tmpl.xml')
        self.sFN=kwargs.get('sFN',self.sDftFN)
        self.SetOriginByWid(self.wid)
        self.wid.SetSizer(bxs)
        bxs.Layout()
        bxs.Fit(self.wid)
        return self.wid
        lWid=[]
        lWid.append(('txt',(0,0),(1,1),{'name':'txtFN','value':self.sFN,},
                {'txt':self.OnTxtFileNameText,'ent':self.OnTxtFileNameEnter}))
        lWid.append(('cbBmp',(0,1),(1,1),
                    {'name':'cbBrowse','bitmap':vtArt.getBitmap(vtArt.Browse),},
                    {'btn':self.OnCbBrowseClick}))
        vtgPanel.__initCtrl__(self,lWid=lWid)
        return None,[(0,1)]
    def GetWid(self):
        return self.wid
    def GetWidMod(self):
        return self.txtFN
    def GetValue(self):
        return self.sFN
    def GetDN(self):
        try:
            sTmpDN,sTmpFN=os.path.split(self.sFN)
            return sTmpDN
        except:
            return None
    def GetFN(self):
        try:
            sTmpDN,sTmpFN=os.path.split(self.sFN)
            return sTmpFN
        except:
            return None
    def SetValue(self,sFN):
        try:
            if self.bPosixFN==True:
                sFN=sFN.replace('\\','/')
            else:
                sFN=sFN.replace('/','\\')
            self.sFN=sFN
            self.txtFN.SetValue(self.sFN)
        except:
            self.__logTB__()
    def OnTxtFileNameText(self,evt):
        try:
            self.sFN=self.txtFN.GetValue()
        except:
            self.__logTB__()
    def OnTxtFileNameEnter(self,evt):
        try:
            self.sFN=self.txtFN.GetValue()
            self.Post('file',self.sFN)
        except:
            self.__logTB__()
    def OnCbBrowseClick(self,evt):
        try:
            self.__logDebug__('sFN:%r'%(self.sFN))
            if self.ChooseFile() is not None:
                self.__logDebug__('sFN:%r'%(self.sFN))
                self.txtFN.SetValue(self.sFN)
        except:
            self.__logTB__()
    def ChooseFile(self):
        try:
            self.__logInfo__(''%())
            if self.bSave==True:
                iStyle=wx.SAVE
            else:
                iStyle=wx.OPEN
            if self.bMulti==True:
                iStyle|=wx.MULTIPLE
            dlg=wx.FileDialog(self.wid,message=self.sMsg,
                defaultDir='.',defaultFile=self.sDftFN,
                wildcard=self.sWildCard,
                style=iStyle)
            dlg.Centre()
            try:
                if self.sFN is not None:
                    if len(self.sFN)>0:
                        sTmpDN,sTmpFN=os.path.split(self.sFN)
                        dlg.SetDirectory(sTmpDN)
                        dlg.SetFilename(sTmpFN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sTmpDN=dlg.GetDirectory()
                if self.bPosixFN==True:
                    sTmpDN=sTmpDN.replace('\\','/')
                if self.bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if self.bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    dlg.Destroy()
                    self.PostMOD('fileSel',lFN)
                    self.sFN=lFN[0]
                    self.Post('files',lFN)
                    for sFN in lFN:
                        self.PostFB('fileSel',sFN)
                    return lFN
                else:
                    sTmpFN=dlg.GetFilename()
                    if self.bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    dlg.Destroy()
                    if self.sFN!=sFN:
                        self.PostMOD('fileSel',sFN)
                    self.sFN=sFN
                    self.Post('file',self.sFN)
                    self.PostFB('fileSel',sFN)
                    return sFN
            dlg.Destroy()
        except:
            self.__logTB__()
        return None
