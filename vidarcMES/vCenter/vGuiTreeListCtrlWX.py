#----------------------------------------------------------------------------
# Name:         vGuiTreeListCtrlWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
#   init args
#       bTreeButtons    ... wx hasbutton style
#
# Created:      20140621
# CVS-ID:       $Id: vGuiTreeListCtrlWX.py,v 1.7 2015/07/30 16:46:31 wal Exp $
# Copyright:    (c) 2014 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import sys
    import types
    import wx
    import wx.gizmos
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    from vGuiImgLstWX import vGuiImgLstGet
    from vGuiTreeCtrlWX import vGuiTreeCtrlWX
    from vGuiListCtrlWX import vGuiColumnStretch
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiTreeListCtrlWX(vGuiTreeCtrlWX,vGuiColumnStretch):
    def __init__(self,*args,**kwargs):
        vGuiTreeCtrlWX.__init__(self,*args,**kwargs)
        bDbg=self.GetVerboseDbg(10)
        try:
            pass
        except:
            self.__logTB__()
        if bDbg:
            self.__logDebug__('done')
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            vGuiTreeCtrlWX.__initObj__(self,*args,**kwargs)
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        wid=None
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            style=0
            self.IMG_EMPTY=-1
            style|=wx.TR_TWIST_BUTTONS
            style|=wx.TR_HAS_BUTTONS
            if kwargs.get('bTreeButtons',1)>0:
                style|=wx.TR_HAS_BUTTONS
            if kwargs.get('bHideRoot',False)>0:
                style|=wx.TR_HIDE_ROOT
            if kwargs.get('multiple_sel',False)>0:
                style|=wx.TR_MULTIPLE
                self._bMultiple=True
            else:
                self._bMultiple=False
            if kwargs.get('force_children',False)>0:
                self.FORCE_CHILDREN=1
            else:
                self.FORCE_CHILDREN=0
            if kwargs.get('bFullRow',True)>0:
                style|=wx.TR_FULL_ROW_HIGHLIGHT
            if kwargs.get('bScrollBarOnDemand',False)>0:
                pass
            else:
                style|=wx.ALWAYS_SHOW_SB
                
            _args,_kwargs=self.GetGuiArgs(kwargs,
                    ['id','name','parent','pos','size','style'],
                    {'pos':(0,0),'size':(-1,-1),'style':style})
            self.widCls=wx.gizmos.TreeListCtrl
            wid=self.widCls(*_args,**_kwargs)
            self.wid=wid
            
            vGuiColumnStretch.__init__(self)
            self._GetColCount=self.wid.GetColumnCount
            self._GetColWidth=self.wid.GetColumnWidth
            self._SetColWidth=self.wid.SetColumnWidth
            wx.CallAfter(wx.EVT_SIZE,self.wid,self.OnSizeStretch)
            wx.CallAfter(self.OnSizeStretch,None)
            
            #self.wid.Bind(wx.EVT_LIST_COL_CLICK,self.OnListColClick,
            #        id=self.wid.GetId())
            
            #wid=vGuiTreeCtrlIntWX(self,*_args,**_kwargs)
            wid.Bind(wx.EVT_TREE_ITEM_ACTIVATED,self.OnItemActivate)
            self.CB(wid.Bind,wx.EVT_TREE_ITEM_COLLAPSING,self.OnItemCollapsing)
            #if self._bSort:
            #    self.CB(self.wid.Bind,wx.EVT_TREE_ITEM_EXPANDING,self.OnItemSort)
        except:
            self.__logTB__()
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            self._dImg=None
            self._imgLst=None
            imgLst=kwargs.get('imgLst',None)
            if imgLst is not None:
                if type(imgLst)==types.StringType:
                    w,bImgLstCreated=vGuiImgLstGet(imgLst)
                    if w is not None:
                        self._sImgLst=imgLst
                        dImg0,imgLst0=w.GetObjs()
                        self.SetImageListByDict(dImg0,imgLst0)
            if self._imgLst is None:
                w,bImgLstCreated=vGuiImgLstGet(self.__class__.__name__,dft='tree')
                if w is not None:
                    self._sImgLst=None
                    self._bImgLstCreated=bImgLstCreated
                    dImg0,imgLst0=w.GetObjs()
                    self.SetImageListByDict(dImg0,imgLst0)
            self.__initImageLst__()
            # ++++ setup mapping
            oTmp=kwargs.get('map',None)
            if oTmp is not None:
                self.MAP=oTmp
            # ---- setup mapping
            # ++++ setup columns
            self._iColDftWidth=kwargs.get('default_col_width',100)
            oTmp=kwargs.get('cols',None)
            if oTmp is not None:
                self.SetColumns(oTmp)
            # ---- setup columns
        except:
            self.__logTB__()
        return wid
    def __initProperties__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            # ++++ assign values
            oTmp=kwargs.get('value',None)
            if oTmp is not None:
                self.SetValue(oTmp)
            # ---- assign values
            tFunc=kwargs.get('funcValue',None)
            if tFunc is not None:
                self.SetFuncValue(tFunc[0],*tFunc[1],**tFunc[2])
                oThd=vGuiCoreWX.__getThread__(self,'tree')
                if kwargs.get('build',True):
                    self.CB(self.Build)
        except:
            self.__logTB__()
    def SetForceChildren(self,bFlag=True):
        self.FORCE_CHILDREN=bFlag
    def _getAlignment(self,iAlign):
        if iAlign is None:
            iAlign=wx.ALIGN_LEFT
        elif iAlign<0:
            if iAlign==-1:
                iAlign=wx.ALIGN_LEFT
            elif iAlign==-2:
                iAlign=wx.ALIGN_RIGHT
            elif iAlign==-3:
                iAlign=wx.ALIGN_CENTRE
            else:
                self.__logError__('unknown format;%d',(iAlign))
                iAlign=wx.ALIGN_LEFT
        if wx.VERSION < (2,8):
            if iAlign==wx.ALIGN_LEFT:
                iAlign=wx.LIST_FORMAT_LEFT
            elif iAlign==wx.ALIGN_CENTRE:
                iAlign=wx.LIST_FORMAT_CENTRE
                #iAlign=wx.LIST_FORMAT_RIGHT
            elif iAlign==wx.ALIGN_RIGHT:
                iAlign=wx.LIST_FORMAT_RIGHT
            else:
                self.__logError__('unknown format;%d;0x%x'%(iAlign,iAlign))
                iAlign=wx.LIST_FORMAT_LEFT
        else:
            pass
        return iAlign
    def SetColumnAlignment(self,iCol,iAlign):
        self.SetColumn(iCol,None,iAlign,None,None)
    def SetColumn(self,iCol,sName,iAlign=None,iWidth=None,iSort=None,iImg=None):
        try:
            wid=self.wid
            if self.__isLogDebug__():
                self.__logDebug__('iCol:%d;sName:%r;iAlign:%r;iWith:%r;iSort:%r;iImg:%r'%(\
                        iCol,sName,iAlign,iWidth,iSort,iImg))
            if type(sName)==type(self._it):
                wx.ListCtrl.SetColumn(wid,iCol,sName)
                return
            it=wid.GetColumn(iCol)
            mask=0
            if sName is not None:
                it.SetText(sName)
                mask+=1
            if iAlign is not None:
                it.SetAlignment(self._getAlignment(iAlign))
                mask+=1
            if iImg is not None:
                it.SetImage(iImg)
                mask+=1
            elif iSort is not None:
                pass
            if mask!=0:
                it.m_mask=mask
                self.widCls.SetColumn(wid,iCol,it)
                return 1
            return 0
        except:
            self.__logTB__()
        return -1
    def SetColumns(self,cols):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            #self.DeleteAllItems()
            iCnt=self.wid.GetColumnCount()
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg==True:
                self.__logDebug__(['cols;',cols])
            if cols is None:
                self.__logDebug__('exit nothing to do'%())
                return
            # check sortable col def
            iColSort=-1
            dSortDef=None
            iTmp=0
            for lCol in cols:
                if len(lCol)>3:
                    if lCol[3]!=0:
                        if iColSort==-1:
                            iColSort=iTmp
                            dSortDef={-1:{'iSort':1}}
                            dSortDef[iTmp]={'iSort':lCol[3]}
                        else:
                            dSortDef[iTmp]={'iSort':lCol[3]}
                iTmp=iTmp+1
            if iColSort>=0:
                if self.IMG_SORT_ASC==-1:
                    imgLst,dImg=self.CreateImageList([
                        ('empty',       self.getBmp('core','Invisible')),
                        ('asc',         self.getBmp('core','UpSml')),
                        ('desc',        self.getBmp('core','DnSml')),
                        ])
                    self.SetImageList(imgLst,dImg['empty'],
                            dImg['asc'],dImg['desc'])
                    wx.ListCtrl.AssignImageList(self.wid,imgLst,
                            which=wx.IMAGE_LIST_SMALL)
            iCol=0
            lStretch=[]
            iSort=0
            for lCol in cols:
                if bDbg==True:
                    self.__logDebug__('iCol:%d;iCnt:%d;lCol;%r'%(iCol,iCnt,lCol))
                iW=lCol[2] or self._iColDftWidth
                iWType=type(iW)
                if iWType==types.FloatType:
                    lStretch.append((iCol,iW))
                    iW=self._iColDftWidth
                elif iWType==types.StringType:
                    iWsz=self.wid.GetTextExtent(iW)
                    iWedge=wx.SystemSettings.GetMetric(wx.SYS_EDGE_X)
                    iW=iWsz[0]+(iWedge<<2)#+2
                elif iW<0:
                    lStretch.append((iCol,iW))
                    iW=self._iColDftWidth
                if iSort==0:
                    iSortTmp=lCol[3]
                    iSort=iSortTmp
                else:
                    iSortTmp=0
                oColInfo=wx.gizmos.TreeListColumnInfo()
                oColInfo.SetText(lCol[0])
                oColInfo.SetAlignment(self._getAlignment(lCol[1]))
                oColInfo.SetWidth(iW)
                if iCol<iCnt:
                    #self.SetColumn(iCol,lCol[0],lCol[1],iW,iSortTmp,None)
                    self.wid.SetColumnInfo(oColInfo)
                else:
                    self.wid.AddColumnInfo(oColInfo)
                    #self.SetColumn(iCol,lCol[0],lCol[1],None,iSortTmp,None)
                iCol+=1
            for i in xrange(iCol,iCnt):
                self.wid.DeleteColumn(iCol)
            self.SetStretchLst(lStretch)
            #if dSortDef is not None:
            #    self.SetSortDef(dSortDef)
            #    self.SetSort(iColSort,dSortDef[iColSort]['iSort'])
        except:
            self.__logTB__()
    def __setTreeItemDict__(self,tr,ti,d):
        #print tr.GetItemText(ti,0)
        for s,i in self.MAP:
            #if i>0:
            #    continue
            if s in d:
                v=d[s]
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0],i)
                    tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
                    #tr.SetItemText(ti,v[0])
                    #tr.SetItemImage(ti,v[1],which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
                    #tr.SetItemImage(ti,v,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v,i)
                    if i==0:
                        tr.SetItemImage(ti,self.IMG_EMPTY,i,
                                    which=wx.TreeItemIcon_Normal)
                    #tr.SetItemText(ti,v)
                    #tr.SetItemImage(ti,self.IMG_EMPTY,which=wx.TreeItemIcon_Normal)
        if self.FORCE_CHILDREN:
            tr.SetItemHasChildren(ti,True)
        bSkipImg=True
        if 0 in d:
            img=d[0]
            t=type(img)
            if t==types.IntType:
                bSkipImg=False
            elif t==types.StringType:
                if self._dImg is not None:
                    img=self.GetImg(img,'empty')
                    bSkipImg=False
            if bSkipImg==False:
                tr.SetItemImage(ti,img,which=wx.TreeItemIcon_Normal)
        if bSkipImg==True:
            tr.SetItemImage(ti,self.IMG_EMPTY,which=wx.TreeItemIcon_Normal)
        if -1 in d:
            v=d[-1]
            tr.SetPyData(ti,v)
            self.dCache[v]=ti
        if -2 in d:
            v=d[-2]
            if v is None:
                tr.SetItemHasChildren(ti,True)
            else:
                if self._bSort:
                    t=type(v[0])
                    if t==types.DictType:
                        def cmpDict(a,b):
                            try:
                                for s,i in self.MAP:
                                    return cmp(a.get(s,None),b.get(s,None))
                            except:
                                pass
                            return 0
                        v.sort(cmpDict)
                self.__setValue__(tr,ti,v)
    def __setTreeItemLst__(self,tr,ti,l):
        try:
            if l[0] is not None:
                tr.SetPyData(ti,l[0])
                self.dCache[l[0]]=ti
            i=0
            for v in l[1]:
                t=type(v)
                if t==types.TupleType:
                    tr.SetItemText(ti,v[0],i)
                    tr.SetItemImage(ti,v[1],i,which=wx.TreeItemIcon_Normal)
                elif t==types.IntType:
                    tr.SetItemImage(ti,v,i,which=wx.TreeItemIcon_Normal)
                else:
                    tr.SetItemText(ti,v,i)
                    if i==0:
                        tr.SetItemImage(ti,self.IMG_EMPTY,i,
                                    which=wx.TreeItemIcon_Normal)
                i+=1
            if self.FORCE_CHILDREN:
                tr.SetItemHasChildren(ti,True)
            iLen=len(l)
            if iLen>2:
                self.__setValue__(tr,ti,l[2])
        except:
            self.__logTB__()
            self.__logError__('l:%s;l[0]:%s;l[1]:%s'%(self.__logFmt__(l),
                    self.__logFmt__(l[0]),self.__logFmt__(l[1])))
    def __setTreeItemTup__(self,tr,ti,t):
        pass
    def __getItemStr__(self,tr,ti,iKind):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            if ti.IsOk()==False:
                return None
            tr=self.wid
            if iKind==-1:
                return tr.GetPyData(ti)
            elif iKind<0:
                return None
            elif iKind==0:
                return tr.GetItemText(ti)
            elif iKind>0:
                return tr.GetItemText(ti,iKind)
            else:
                return None
        except:
            self.__logTB__()
    def BindEvent(self,name,func,par=None):
        if name.startswith('lst_'):
            wid=self.GetWid()
            if name in ['lst_item_sel','lst_item_selected']:
                wid.Bind(wx.EVT_LIST_ITEM_SELECTED,func, par or wid)
            elif name in ['lst_item_desel','lst_item_deselected']:
                wid.Bind(wx.EVT_LIST_ITEM_DESELECTED,func, par or wid)
            elif name in ['lst_item_right_click','lst_item_rgClk']:
                wid.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,func, par or wid)
            elif name in ['lst_col_drag_begin']:
                wid.Bind(wx.EVT_LIST_COL_BEGIN_DRAG,func, par or wid)
            elif name in ['lst_col_dragging']:
                wid.Bind(wx.EVT_LIST_COL_DRAGGING,func, par or wid)
            elif name in ['lst_col_drag_end']:
                wid.Bind(wx.EVT_LIST_COL_END_DRAG,func, par or wid)
            elif name in ['lst_col_left_click']:
                wid.Bind(wx.EVT_LIST_COL_LEFT_CLICK,func, par or wid)
            elif name=='lst_col_right_click':
                wid.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,
                        func, par or wid)
        else:
            vGuiTreeCtrlWX.BindEvent(self,name,func,par=par)
    def SetSelected(self,lSel,iKind=-1,ti=None,bFlag=True,bKeepSel=False,bEnsure=True):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return -2
            tr=self.wid
            if ti is None:
                ti=tr.GetRootItem()
            if self._bMultiple==True:
                if bKeepSel==False:
                    lSelTi=self.GetSelected([-2])
                    for tiOld in lSelTi:
                        tr.SelectItem(tiOld[0],False)
        except:
            self.__logTB__()
        try:
            dSel={}
            lSelTi=[]
            lSelKey=[]
            for iK in lSel:
                if iK in dSel:
                    self.__logError__(['key already present',iK])
                else:
                    dSel[iK]=0
                    lSelKey.append(iK)
                    if self._bMultiple==False:
                        break
            #dSel[None]=0
            if iKind==-1:
                for iK in lSel:
                    if iK in self.dCache:
                        ti=self.dCache[iK]
                        tr.SelectItem(ti,bFlag)
                        lSelTi.append(ti)
                        if self._bMultiple==False:
                            break
            else:
                self.__walk__(tr,ti,self.__setSelected__,dSel,iKind,bFlag,lSelTi)
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'bEnsure':bEnsure,
                        'lSel':lSel,'lSelKey':lSelKey,'lSelTi':lSelTi})
            if bEnsure:
                if len(lSelTi)>0:
                    tr.EnsureVisible(lSelTi[0])
            return lSelKey
            return lSelTi
            #return self.__setSelected__(lst,l,bFlag=bFlag)
        except:
            self.__logTB__()
        return -1
