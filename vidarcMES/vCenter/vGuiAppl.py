#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vGuiAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121201
# CVS-ID:       $Id: vGuiAppl.py,v 1.2 2012/12/02 19:51:07 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vCust
    
    import os,sys,getopt,traceback
    import vSystem
    import wx
    import vGuiSplashFrmWX
    import vLang
    import vGuiCoreWX

    import images_splash
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiAppl(wx.App):
    def __init__(self,num,sGrp,sAppl,sVersion,cfgFN=None,
            fn=None,iLogLv=None,iSockPort=None,lActPre=[],lActPost=[],
            bVidImp=True):
        self.lActPre=lActPre
        self.lActPost=lActPost
        try:
            vLang.initAppl(sAppl)
        except:
            vpc.logTB(__name__)
        self.main=None
        self.fn=fn
        self.cfgFN=cfgFN or sAppl+'Cfg.xml'
        self.iLogLv=iLogLv or vpc.vLog_ERROR
        self.iSockPort=iSockPort
        self.sGrp=sGrp
        self.sAppl=sAppl
        self.sVersion=sVersion
        self.bVidImp=bVidImp
        self.DoCmdLineArg()
        wx.App.__init__(self,num)
    def PrintHelp(self):
        try:
            print _('help')
            print sys.version
            print "  ",_("valid flags:")
            print "  ",_("-c <filename>")
            print "  ",_("--config <filename>")
            print "        ",_("open the <filename> to configure application")
            print "  ",_("-f <filename>")
            print "  ",_("--file <filename>")
            print "        ",_("open the <filename> at start")
            print "  ","-h"
            print "  ",_("--help")
            print "        ",_("show this screen")
            print "  ","-l"
            print "  ",_("--lang <language id according ISO639>")
            print "  ",_("--log <level>")
            print "       ",_("0 = debug")
            print "       ",_("1 = information")
            print "       ",_("2 = waring")
            print "       ",_("3 = error")
            print "       ",_("4 = critical")
            print "       ",_("5 = fatal")
        except:
            vpc.logTB(__name__)
    def DoCmdLineArg(self):
        try:
            sTmp=vCust.getCmdLineOpt(None,'--help',None)
            if sTmp is not None:
                self.PrintHelp()
            sLog=vCust.getCmdLineOpt(None,'--log',None)
            if sLog is not None:
                    if sLog=='0':
                        iLogLv=vpc.vLog_DEBUG
                    elif sLog=='1':
                        iLogLv=vpc.vLog_INFO
                    elif sLog=='2':
                        iLogLv=vpc.vLog_WARN
                    elif sLog=='3':
                        iLogLv=vpc.vLog_ERROR
                    elif sLog=='4':
                        iLogLv=vpc.vLog_CRITICAL
                    elif sLog=='5':
                        iLogLv=vpc.FATAL
            sTmp=vCust.getCmdLineOpt(None,'--config',None)
            if sTmp is not None:
                self.cfgFN=sTmp[:]
            sTmp=vCust.getCmdLineOpt(None,'--file',None)
            if sTmp is not None:
                self.fn=sTmp[:]
        except:
            vpc.logTB(__name__)
    def RedirectStdio(self, filename=None):
        """Redirect sys.stdout and sys.stderr to a file or a popup window."""
        if filename:
            sys.stderr = open(filename, 'w')
            sys.stdout = open(filename.replace('.err.','.out.'), 'w')
            #if self.iLogLv==vtLogDef.DEBUG or self.iLogLv==vtLogDef.INFO:
            #    sys.stdout = self.outputWindowClass()
            #else:
            #    sys.stdout = sys.stderr
        else:
            self.stdioWin = self.outputWindowClass()
            sys.stdout = sys.stderr = self.stdioWin
    def OnInit(self):
        self.RedirectStdio(vSystem.getErrFN(self.sAppl))
        vLang.initAppl(self.sAppl)
        wx.InitAllImageHandlers()
        self.splash = vGuiSplashFrmWX.create(None,
            self.sGrp,self.sAppl,self.sVersion,
            None,#images_splash.getSplashBitmap(),
            self.lActPre,
            self.lActPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort,
            bVidImp=self.bVidImp)
        self.splash.Show()
        self.splash.BindEvent('cmd',self.OnSplashCmd)
        return True
    def OnSplashCmd(self,evt):
        try:
            sCmd=evt.GetCmd()
            vpc.logDebug('cmd;%s'%sCmd,__name__)
            if sCmd=='action':
                self.splash.DoAction()
            elif sCmd=='process':
                self.splash.DoProcess()
            elif sCmd=='result':
                self.main=evt.GetData()
            elif sCmd=='fin':
                if self.splash is not None:
                    try:
                        #self.main=self.splash.res[self.iRes]
                        if self.main is not None:
                            if hasattr(self.main,'GetWid'):
                                self.splash.CB(self.SetTopWindow,self.main.GetWid())
                            else:
                                self.splash.CB(self.SetTopWindow,self.main)
                    except:
                        vpc.logTB(__name__)
                    try:
                        self.splash.CB(vGuiCoreWX.InitMsgWid)     # fixme
                    except:
                        vpc.logTB(__name__)
                    try:
                        if self.main is not None:
                            self.splash.CB(self.main.Show)
                    except:
                        vpc.logTB(__name__)
                    self.splash.Destroy()
                    self.splash=None
            elif sCmd=='stop':
                if self.main is not None:
                    if self.main is not None:
                        if hasattr(self.main,'GetWid'):
                            self.main.GetWid().Destroy()
                        else:
                            self.main.Destroy()
                    self.main=None
                if self.splash is not None:
                    self.splash.Destroy()
                    self.splash=None
        except:
            vpc.logTB(__name__)
def main(sGrp='gui',sAppl='vGuiAppl',sVersion='0.0.0',
        iSockPort=69999,lActPre=[],lActPost=[],bVidImp=True):
    try:
        vpc.logDebug('main',__name__)
        application = vGuiAppl(0,sGrp=sGrp,sAppl=sAppl,
                sVersion=sVersion,iSockPort=iSockPort,
                lActPre=lActPre,lActPost=lActPost,bVidImp=bVidImp)
        application.MainLoop()
    except:
        vpc.logTB(__name__)

if __name__ == '__main__':
    main()
