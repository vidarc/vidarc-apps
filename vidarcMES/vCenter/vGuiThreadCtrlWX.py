#----------------------------------------------------------------------------
# Name:         vGuiThreadCtrlWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120317
# CVS-ID:       $Id: vGuiThreadCtrlWX.py,v 1.6 2015/09/11 09:57:34 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    
    from vGuiPanelWX import vGuiPanelWX
    from vGuiFrmWX import vGuiFrmWX
    
    VERBOSE=vpc.getVerboseType(__name__)
    from vidarc.tool.db.vtDbDatabase import vtDbDatabase
except:
    vpc.logTB(__name__)

class vGuiThreadCtrlWX(vGuiPanelWX):
    FLEX_SIZER=1
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('vGui')
        lWidAdd=[]
        bEnableCfg=False
        if kwargs.get('bEnableCfg',False)==True:
            bEnableCfg=True
            lWidAdd.append(('tgPopup', 0,0,{
                    'name':'tgCfg',#'iArrange':-1,
                    },None))
        lWidAdd.append(('tgPopup', 0,0,{
                    'name':'tgAct',#'iArrange':-1,
                    },None))
        lWid=[
            ('szBoxHor',1,0,
                    {'name':'bxsInfo',},[
                ('cbBmp',   0,4,{'name':'cbStart',
                        'tip':_(u'start thread'),
                        'bitmap':self.getBmp('stat','Start')},
                        {'btn':self.OnCbStartButton}),
                ('cbBmp',   0,4,{'name':'cbStop',
                        'tip':_(u'stop thread'),
                        'bitmap':self.getBmp('stat','Stop')},
                        {'btn':self.OnCbStopButton}),
                ('txtRd',   4,4,{'name':'txtVal','value':u''},None),
                ('txtRd',   0,4,{'name':'txtCnt','size':(60,-1),'value':u''},None),
                ]+lWidAdd),
            ('szBoxHor',1,0,
                    {'name':'bxsInfo',},[
                ('bar',      1,0,{'name':'pbProc','range':1000},None),
                ]),
            ]
        vGuiPanelWX.__initCtrl__(self,lWid=lWid)
        
        p=vGuiFrmWX(parent=self.tgAct.GetWid(),kind='popup',
                iArrange=0,widArrange=self.txtVal,bAnchor=True,
                lGrowRow=[(0,1),(1,1)],
                lGrowCol=[(0,1),(1,1)],
                lWid=[
                        ('lstCtrl', (0,0),(2,2),{'name':'lstAction','size':(400,200),
                                'cols':[
                                    ['',-3,20,0],
                                    [_(u'name'),-1,-1,1],
                                    [_(u'result'),-2,80,1],
                                    ],
                                'map':[('name',1),('result',2)],
                                'tip':_(u'action'),
                                },None),
                        #('txt',     (0,1),(1,1),{'name':'txtDSN','value':kwargs.get('dsn',u'')},None),
                        ('lblRg',   (2,0),(1,1),{'name':'lblCnt','label':_(u'count')},None),
                        ('txt',     (2,1),(1,1),{'name':'txtCnt','value':kwargs.get('count',u'')},None),
                        ('szBoxVert',(0,2),(2,1),{'name':'bxsBt'},[
                            ('cbBmp',   0,4,{'name':'cbClr',
                                'tip':_(u'connect to database'),
                                'bitmap':self.getBmp('core','Erase')},
                                {'btn':self.OnConnect}),
                            #('cbBmp',   (0,4),(1,1),{'name':'cbConn',
                            #    'tip':_(u'connect to database'),
                            #    'bitmap':vtArtState.getBitmap(vtArtState.Start)},
                            #    {'btn':self.OnConnect}),
                            ]),
                        ])
        self.tgAct.SetWidPopup(p)
        if bEnableCfg==True:
            self.tgCfg.SetWidPopup(p)
        #v=kwargs.get('createThread',0)
        v=kwargs.get('createThread',0)
        self.thd2Ctrl=None
        self.funcStart=None
        self.funcStop=None
        if v!=0:
            self.thd2Ctrl=vpc.vcpThd()
            self.thd2Ctrl.Inz(self.GetOrigin(),'thd')
            self.thd2Ctrl.Start()
            self.thd2Ctrl.Do(self.GetThread)
        return [],[(0,1)]
    def SetThread(self,thd):
        try:
            self.__logDebug__(''%())
            self.thd2Ctrl=thd
        except:
            self.__logTB__()
    def GetThread(self):
        try:
            self.__logDebug__(''%())
            return self.thd2Ctrl
        except:
            self.__logTB__()
        return None
    def Do(self,func,*args,**kwargs):
        try:
            self.thd2Ctrl.Do(func,*args,**kwargs)
        except:
            self.__logTB__()
    def DoCB(self,func,*args,**kwargs):
        try:
            self.thd2Ctrl.Do(self.CB,func,*args,**kwargs)
        except:
            self.__logTB__()
    def GetActionWid(self):
        try:
            p=self.tgCfg.GetWidPopup()
            return p.lstAction
        except:
            self.__logTB__()
        return None
    def AddActCB(self,sName,func,*args,**kwargs):
        try:
            w=self.GetActionWid()
            w.Insert([{-1:(sName,1,func,args,kwargs),
                    'name':sName
                    }])
            #self.thd2Ctrl.Do(self.CB,func,*args,**kwargs)
        except:
            self.__logTB__()
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # + database processing
    def OnThdDbProc(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnThdDbAbort(self,evt):
        try:
            self.__logDebug__(''%())
            if self.oDB.IsRunning():
                s=self.txtStat.GetValue()
                self.txtStat.SetValue(s+' '+_(u'failed.'))
            else:
                self.txtStat.SetValue(+_(u'disconnected.'))
        except:
            self.__logTB__()
    def OnThdDbFin(self,evt):
        try:
            self.__logDebug__(''%())
            s=self.txtStat.GetValue()
            self.txtStat.SetValue(s+' '+_(u'done.'))
            if self.iStat==0:
                #self.__do_call__('doDbUse',self.sDBname)
                self.doDbUse(self.sDB)
                self.iStat=1
            elif self.iStat==1:
                self.doDbGetTbl()
                self.iStat=10
        except:
            self.__logTB__()
    
    # - database processing
    # --------------------------------------------------------------------------------
    def DoConnect(self):
        try:
            if self.IsMainThread()==False:
                return
            if self.oDB is not None:
                self.oDB.Stop()
            widPop=self.tgCfg.GetWidPopup()
            sDSN=   widPop.txtDSN.GetValue()
            sUsr=   widPop.txtUsr.GetValue()
            sPwd=   widPop.txtPwd.GetValue()
            self.sDB=widPop.txtDB.GetValue()
            self.iStat=0
            self.oDB=vtDbDatabase(sDSN,sUsr,sPwd)
            self.oDB.Start()
            self.txtStat.SetValue(_(u'connecting...'))
            self.oDB.SetPostWid(self.GetWid(),True)
            self.oDB.BindEvents(self.OnThdDbProc,self.OnThdDbAbort,
                        self.OnThdDbFin)
        except:
            self.__logTB__()
    def DoDisConnect(self):
        try:
            if self.IsMainThread()==False:
                return
            if self.oDB is not None:
                self.oDB.Stop()
                self.oDB=None
        except:
            self.__logTB__()
    def OnConnect(self,evt):
        try:
            self.DoConnect()
        except:
            self.__logTB__()
    def OnDisConnect(self,evt):
        try:
            self.__logDebug__(''%())
            self.DoDisConnect()
        except:
            self.__logTB__()
    def DoStart(self):
        try:
            self.__logInfo__(''%())
            self.Post('start')
            if self.thd2Ctrl is not None:
                self.thd2Ctrl.Start()
            if self.funcStart is not None:
                func,args,kwargs=self.funcStart
                func(*args,**kwargs)
            #self.Start()
        except:
            self.__logTB__()
    def DoStop(self):
        try:
            self.Post('stop')
            if self.funcStop is not None:
                func,args,kwargs=self.funcStop
                func(*args,**kwargs)
            if self.thd2Ctrl is not None:
                self.thd2Ctrl.Stop()
                #self.__stop__(bEnStart=not self.thd2Ctrl.IsRunning(),bClr=False)
            #else:
            #    self.__stop__(bEnStart=True,bClr=False)
        except:
            self.__logTB__()
    def OnCbStartButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.DoStart()
        except:
            self.__logTB__()
    def OnCbStopButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.DoStop()
        except:
            self.__logTB__()
