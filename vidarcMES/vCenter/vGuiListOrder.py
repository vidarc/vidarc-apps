#----------------------------------------------------------------------------
# Name:         vGuiListOrder.py
# Purpose:      list control ordered
# Author:       Walter Obweger
#
# Created:      20131030
# CVS-ID:       $Id: vGuiListOrder.py,v 1.4 2015/07/25 15:59:35 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    from vGuiPanelWX import vGuiPanelWX
    from vGuiImgLstWX import vGuiImgLstGet
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiListOrder(vGuiPanelWX):
    def __initListCtrlDef__(self):
        return [
            ('iNr'      ,0),
            ('sN'       ,1),
            ],[
            [_('nr')            ,-2,    60,1],
            [_('name')          ,-1,    -1,1],
            ]
    def __initCtrlWid__(self):
        vpc.logDebug('start',__name__)
        global _
        _=vLang.assignPluginLang('vCenter')
        MAP,COLS=self.__initListCtrlDef__()
        lWid=[#{
            #'pnData':{
            #    'lWid':[
                    ('lstCtrl', (0,0),(1,1),{'name':'lstMain',
                        'cols':COLS,
                        'map':MAP,
                        },
                        {
                        },
                    )
                    #],
                #'lGrowRow':[(0,1),],
                #'lGrowCol':[(0,1),],
            #},
            #'ctrl':{
            #    },
            #}
            ]
        vpc.logDebug('done',__name__)
        return lWid
    def __initCtrl__(self,*args,**kwargs):
        vpc.logDebug('start',__name__)
        try:
            #self.SetVerbose(__name__)
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
        vGuiPanelWX.__initCtrl__(self,*args,**kwargs)
        vpc.logDebug('done',__name__)
        return [(0,1),],[(0,1),]
