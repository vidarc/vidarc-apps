#----------------------------------------------------------------------------
# Name:         vGuiFrmWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120215
# CVS-ID:       $Id: vGuiFrmWX.py,v 1.37 2015/07/25 15:53:34 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import wx.lib.buttons
    import types,os

    import vLang as vtLgBase
    VERBOSE=vpc.getVerboseType(__name__)
    
    from vGuiFrmCoreWX import vGuiFrmCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    from vGuiCoreWX import vGuiCoreGetIcon
    from vGuiCoreWX import vGuiCoreTopWidReg
    from vGuiPanelWX import vGuiPanelWX
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

def create(parent,title,pnCls,name='vGuiFrmWX',
            pos=None,sz=None,
            pnPos=wx.DefaultPosition,pnSz=wx.DefaultSize,pnStyle=0,
            pnName='pn',bmp=None,bToolBar=False,bStatusBar=False,
            lMnCfg=None):
    return vGuiFrameWX(parent,title,pnCls,name=name,pos=pos,sz=sz,
            pnPos=nPos,pnSz=pnSz,pnStyle=pnStyle,pnName=pnName,
            bmp=bmp,bToolBar=bToolBar,bStatusBar=bStatusBar,
            lMnCfg=lMnCfg)

class vGuiFrmWX(vGuiFrmCoreWX):
    SIZE_DFT=(100,100)
    MAP_KIND={
        #'popup':    (wx.RESIZE_BORDER|wx.STAY_ON_TOP,0),
        'popup':    (wx.RESIZE_BORDER|wx.FRAME_FLOAT_ON_PARENT|wx.FRAME_NO_TASKBAR,0),
        'popupCls': (wx.RESIZE_BORDER|wx.FRAME_FLOAT_ON_PARENT|wx.FRAME_NO_TASKBAR,6),
        'popSml':   (wx.RESIZE_BORDER|wx.FRAME_FLOAT_ON_PARENT|wx.FRAME_NO_TASKBAR,20),
        'frame':    (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,1),
        'frameCls': (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,2),
        'frmMain':  (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,11),
        'frm':      (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,11),
        'frmCls':   (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,2),
        'frmApyCls':(wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,1),
        'frmOk':    (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,3),
        'frmOkCnc': (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,4),
        'frmYesNo': (wx.RESIZE_BORDER|wx.DEFAULT_FRAME_STYLE,5),
        #'dlg':      (wx.RESIZE_BORDER,10),
        'dialog':   (wx.RESIZE_BORDER,10),
        'dlg':      (wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,10),
        'dlgCls':   (wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,2),
        'dlgApyCls':(wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,1),
        'dlgOk':    (wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,3),
        'dlgOkCnc': (wx.RESIZE_BORDER|wx.DEFAULT_DIALOG_STYLE,4),
        }
    def __initCreate__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def __init__(self, id=None,parent=None,title=None,name='vGuiFrame',
                pos=None,size=None,style=None,kind='frame',
                iArrange=-1,widArrange=None,bAnchor=False,
                pnName='pn',widVal=None,
                pnPos=wx.DefaultPosition,pnSz=wx.DefaultSize,pnStyle=0,
                bmp=None,bToolBar=False,
                lMnCfg=None,lSbCfg=None,lWid=[],**kwargs):
        vGuiFrmCoreWX.__init__(self)
        global _
        _=vtLgBase.assignPluginLang('vGui')
        bDbg=False
        if VERBOSE>10:
            if self.__isLogDebug__():
                bDbg=True
        if bDbg:
            self.__logDebug__({'id':id,'parent':parent,
                    'title':title,'name':name,'pos':pos,'size':size,
                    'style':style,'kind':kind,'iArrange':iArrange,
                    'widArrange':widArrange,'bAnchor':bAnchor,
                    'pnName':pnName,'widVal':widVal,
                    'pnPos':pnPos,'pnSz':pnSz,'pnStyle':pnStyle,
                    'bmp':bmp,'bToolBar':bToolBar,
                    'lMnCfg':lMnCfg,'lSbCfg':lSbCfg,'lWid':lWid,
                    'kwargs':kwargs})
        _kwargs={}
        for s in ['id','name','parent','pos','size','style','title']:
            if s in kwargs:
                _kwargs[s]=kwargs[s]
        if 'size' not in _kwargs:
            sz=self.SIZE_DFT
            _kwargs['size']=sz
        else:
            sz=_kwargs['size']
        if 'id' not in _kwargs:
            iId=wx.NewId()
            _kwargs['id']=iId
        else:
            iId=_kwargs['id']
        if 'style' not in _kwargs:
            _kwargs['style']=wx.TAB_TRAVERSAL
        else:
            if _kwargs['style'] is None:
                _kwargs['style']=wx.TAB_TRAVERSAL
        if kind not in self.MAP_KIND:
            self.__logError__({'not found kind:':kind})
        style,iKind=self.MAP_KIND.get(kind,(wx.DEFAULT_DIALOG_STYLE,1))
        if name is None:
            name=parent.GetName()+'_frm'
        p=self.GetAnchor(parent)
        _args,_kwargs=self.GetGuiArgs({'id':id,'parent':p,
                'title':title,'name':name,
                'pos':pos,'size':size,'style':style},
                ['id','name','parent','pos','size','style','title'],{
                'name':u''.join([name,'Frame']),
                'title':'vGuiFrmWX',
                'pos':(0,0),'size':self.SIZE_DFT,'style':wx.TAB_TRAVERSAL
                })
        self.appl=_kwargs['title']
        self.frm=wx.Frame(*_args,**_kwargs)
        self.SetOriginByWid(self.frm)
        self.__initCls__(**kwargs)
        bDbg=self.GetVerboseDbg(10)
        if bDbg:
            self.__logDebug__({'id':id,'parent':parent,
                    'title':title,'name':name,'pos':pos,'size':size,
                    'style':style,'kind':kind,'iArrange':iArrange,
                    'widArrange':widArrange,'bAnchor':bAnchor,
                    'pnName':pnName,'widVal':widVal,
                    'pnPos':pnPos,'pnSz':pnSz,'pnStyle':pnStyle,
                    'bmp':bmp,'bToolBar':bToolBar,
                    'lMnCfg':lMnCfg,'lSbCfg':lSbCfg,'lWid':lWid,
                    'kwargs':kwargs})
            self.__logDebugAdd__('frm',['args;',_args,'kwargs;',_kwargs])
        self.pn=None
        #wx.EVT_SIZE(self.frm,self.OnSize)
        self.frm.Bind(wx.EVT_SIZE, self.OnSize)
        if kind=='frmMain':
            self.frm.Bind(wx.EVT_CLOSE, self.OnFrameMainClose)
            self.EnableTopWid(True)
            #vGuiCoreTopWidReg(self)
        else:
            self.frm.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        self.frm.Bind(wx.EVT_WINDOW_DESTROY, self.OnDestroy)
        self._iArrange=iArrange
        self._bLayout=False
        self.__logDebug__({'lMnCfg':lMnCfg})
        
        # menubar
        if lMnCfg is not None:
            t=type(lMnCfg)
            if t==types.ListType:
                for iIdx,lMn in lMnCfg:
                    self.AddMenu(iIdx,lMn,None)
            elif t==types.IntType:
                if lMnCfg==0:
                    self.AddMenu(0,[
                        [u'file',_(u'&File'),None,None,(),{},[
                            (u'open',_(u'&Open\tCtrl+O'),
                                self.getBmp('core','Open'),
                                None,(),{}
                                ),
                            (u'save',_(u'&Save\tCtrl+S'),
                                self.getBmp('core','Save'),
                                None,(),{}
                                ),
                            (u'saveAs',_(u'Save &As\tCtrl+Shift+S'),
                                self.getBmp('core','SaveAs'),
                                None,(),{}),
                            None,
                            (u'exit',_(u'E&xit\tAlt+X'),
                                self.getBmp('core','ShutDown'),
                                self.DoShutDown,(),{}),
                            ]],
                        ],None)
                    self.AddMenu(1,[
                        [u'edit',_(u'&Edit'),None,None,(),{},[
                            (u'undo',_(u'&Undo\tCtrl+Z'),
                                self.getBmp('core','UnDo'),
                                None,(),{}),
                            (u'redo',_(u'&Redo\tCtrl+Y'),
                                self.getBmp('core','ReDo'),
                                None,(),{}),
                            None,
                            (u'cut',_(u'Cut\tCtrl+X'),
                                self.getBmp('core','Cut'),
                                None,(),{}),
                            (u'copy',_(u'Copy\tCtrl+C'),
                                self.getBmp('core','Copy'),
                                None,(),{}),
                            (u'paste',_(u'Paste\tCtrl+V'),
                                self.getBmp('core','Paste'),
                                None,(),{}),
                            (u'dup',_(u'Duplicat&e\tCtrl+D'),
                                self.getBmp('core','Duplicate'),
                                None,(),{}),
                            None,
                            (u'add',_(u'&Add\tCtrl+A'),
                                self.getBmp('core','Add'),
                                None,(),{}),
                            (u'edit',_(u'&Edit\tCtrl+E'),
                                self.getBmp('core','Edit'),
                                None,(),{}),
                            (u'del',_(u'&Delete\tDel'),
                                self.getBmp('core','Del'),
                                None,(),{}),
                            None,
                            (u'apply',_(u'Apply\tCtrl+Enter'),
                                self.getBmp('core','Apply'),
                                None,(),{}),
                            (u'cancel',_(u'Cancel\tEsc'),
                                self.getBmp('core','Cancel'),
                                None,(),{}),
                            ]],
                        ],None)
                    self.AddMenu(2,[
                        [u'search',_(u'&Search'),None,None,(),{},[
                            (u'findStart',_(u'&Find Start\tCtrl+F'),
                                self.getBmp('core','Search'),
                                None,(),{}),
                            (u'findNext',_(u'Find &Next\tF3'),
                                self.getBmp('core','SearchNext'),
                                None,(),{}),
                            (u'findPrev',_(u'Find &Prev\tShift+F3'),
                                self.getBmp('core','SearchPrev'),
                                None,(),{}),
                            None,
                            (u'filter',_(u'Fil&ter\tCtrl+T'),
                                self.getBmp('core','Filter'),
                                None,(),{}),
                            None,
                            (u'goto',_(u'&Go to\tCtrl+G'),
                                self.getBmp('core','Navigate'),
                                None,(),{}),
                            ]],
                        ],None)
                    self.AddMenu(3,[
                        [u'analyse',_(u'&Analyse'),None,None,(),{},[
                            (u'play',_(u'Play'),
                                self.getBmp('cmd','Play'),
                                self.DoAbout,(),{}),
                            (u'rec',_(u'Record'),
                                self.getBmp('cmd','Record'),
                                self.DoAbout,(),{}),
                            None,
                            (u'tbl',_(u'Table'),
                                self.getBmp('dat','Table'),
                                self.DoAbout,(),{}),
                            (u'pie',_(u'Pie'),
                                self.getBmp('core','Pie'),
                                self.DoAbout,(),{}),
                            None,
                            (u'audit',_(u'Audit Trail'),
                                self.getBmp('core','AuditTrail'),
                                None,(),{}),
                            ]],
                        ],None)
                    self.AddMenu(4,[
                        [u'tools',_(u'&Tools'),None,None,(),{},[
                            [u'imp',_(u'&Import'),
                                self.getBmp('core','ImportFree'),
                                None,(),{},[
                                (u'impXml',_(u'&XML'),
                                    self.getBmp('core','Import'),
                                    None,(),{}),
                                (u'impExc',_(u'&Excel'),
                                    self.getBmp('core','ExcelImport'),
                                    None,(),{}),
                                (u'impCsv',_(u'&CSV'),
                                    self.getBmp('core','Import'),
                                    None,(),{}),
                                ]],
                            [u'exp',_(u'&Export'),
                                self.getBmp('core','ExportFree'),
                                None,(),{},[
                                (u'expXml',_(u'&XML'),
                                    self.getBmp('core','Export'),
                                    None,(),{}),
                                (u'expExc',_(u'&Excel'),
                                    self.getBmp('core','ExcelExport'),
                                    None,(),{}),
                                (u'expCsv',_(u'&CSV'),
                                    self.getBmp('core','ExportFree'),
                                    None,(),{}),
                                ]],
                            None,
                            (u'align',_(u'Align'),
                                self.getBmp('core','Right'),
                                self.DoAbout,(),{}),
                            (u'rebuild',_(u'Rebuild'),
                                self.getBmp('core','Refresh'),
                                self.DoAbout,(),{}),
                            (u'cfg',_(u'Configuration'),
                                self.getBmp('core','Settings'),
                                self.DoAbout,(),{}),
                            None,
                            (u'reqLck',_(u'Request &Lock\tF4'),
                                self.getBmp('core','Pin'),
                                self.DoAbout,(),{}),
                            (u'cmd',_(u'Commands'),
                                self.getBmp('core','Build'),
                                self.DoAbout,(),{}),
                            ]],
                        ],None)
                    self.AddMenu(5,[
                        [u'helpMain',_(u'Help'),None,None,(),{},[
                            (u'help',_(u'?\tF1'),
                                self.getBmp('core','Help'),
                                self.DoHelp,(),{}),
                            (u'about',_(u'About'),
                                self.getBmp('core','About'),
                                self.DoAbout,(),{}),
                            (u'log',_(u'Log'),
                                self.getBmp('core','FirstAid'),
                                self.DoLog,(),{}),
                            None,
                            (u'scrTop',_(u'Screenshot All'),
                                self.getBmp('core','Snap'),
                                self.DoScreenShotAll,(),{}),
                            (u'scrWnd',_(u'Screenshot'),
                                self.getBmp('core','Snap'),
                                self.DoScreenShotWnd,(),{}),
                            None,
                            (u'err',_(u'Error Report'),
                                self.getBmp('core','Bug'),
                                self.DoErrRep,(),{}),
                            ]],
                        ],None)
            
        # statusbar
        self.__logDebug__({'lSbCfg':lSbCfg})
        if lSbCfg is not None:
            self.AddStatusBar(lSbCfg)
            
        self.pnMain = wx.Panel(id=wx.NewId(),
              name='pnMain', parent=self.frm, 
              style=wx.TAB_TRAVERSAL)
        #self.pnMain.SetAutoLayout(True)
        #vGuiPanelWX
        self.pnMain.AddMenu=self.AddMenu
        self.pnMain.AddToolBar=self.AddToolBar
        self.pnMain.EnableTool=self.EnableTool
        if bDbg:
            self.__logDebug__({'kind':kind,'iKind':iKind})
        self.bxsData=None
        self.bxsBt=None
        if iKind==20:
            self.bxsData = wx.BoxSizer(orient=wx.HORIZONTAL)
            self.bxsBt = wx.BoxSizer(orient=wx.VERTICAL)
        if iKind<12:
            self.bxsData = wx.BoxSizer(orient=wx.VERTICAL)
        if iKind<10:
            self.bxsBt = wx.BoxSizer(orient=wx.HORIZONTAL)
            if iKind in [0,6]:
                #self.gbsData.AddSizer(self.bxsBt, (0, 0), border=4,
                #        flag=wx.BOTTOM | wx.ALIGN_LEFT, span=(1, 1))
                self.bxsData.AddSizer(self.bxsBt, 0, border=4,
                        flag=wx.BOTTOM | wx.ALIGN_LEFT)
            #else:
            #    self.gbsData.AddSizer(self.bxsBt, (1, 0), border=4,
            #            flag=wx.BOTTOM | wx.TOP | wx.ALIGN_CENTER, span=(1, 1))
        
        if iKind not in [0,6,20]:
            if bmp is None:
                icon=vGuiCoreGetIcon(self.getBmp('core','Mod'))
            else:
                icon=vGuiCoreGetIcon(bmp)
            self.frm.SetIcon(icon)
        try:
            #if iKind==0:
            #    self.gbsData.AddGrowableCol(0)
            #    self.gbsData.AddGrowableRow(1)
            #    iRowPn=1
            #else:
            #    self.gbsData.AddGrowableCol(0)
            #    self.gbsData.AddGrowableRow(0)
            #    iRowPn=0
            iRowPn=0
            _args,_kwargs=self.GetGuiArgs({'id':-1,'parent':self.pnMain,
                'name':pnName,#'widVal':widVal,
                'pos':pnPos,'size':pnSz,'style':pnStyle},
                    ['id','name','parent','pos','size','style'],{
                    'name':u'pn',#'widVal':None,
                    'pos':None,'size':None,'style':None
                    })
            t=type(lWid)
            if (t==types.ListType) or (t==types.DictType):
                __args,_kwargs=self.GetGuiArgs(kwargs,
                    ['lGrowRow','lGrowCol','lWid','iLayout',
                    'bEnMark','bEnMod','bLogRelWid','widVal',
                    'bFlexGridSizer','VERBOSE'],{
                    'lGrowRow':None,'lGrowCol':None,'lWid':lWid,
                    'bFlexGridSizer':0,'widVal':None,'VERBOSE':-1,
                    },_kwargs)
                if bDbg:
                    self.__logDebugAdd__('pn',{
                            't':t,'lWid':lWid,'iKind':iKind,
                            'kwargs':kwargs,'_kwargs':_kwargs,
                            })
                self.pn=vGuiPanelWX(**_kwargs)
                self.bxsData.AddWindow(self.pn.GetWid(), 1, border=4,
                        flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | \
                            wx.EXPAND)
                if iKind in [0,6]:
                    pass
                elif iKind in [20]:
                    if self.bxsBt is not None:
                        self.bxsData.AddSizer(self.bxsBt, 0, border=0,
                            flag=wx.EXPAND)
                else:
                    if self.bxsBt is not None:
                        self.bxsData.AddSizer(self.bxsBt, 0, border=4,
                            flag=wx.BOTTOM | wx.ALIGN_CENTER)
            else:
                self.__logWarn__('may cause application crash')
                #print lWid
                if bDbg:
                    self.__logDebugAdd__('cls',{
                            't':t,'lWid':lWid,'iKind':iKind,
                            'kwargs':kwargs,'_kwargs':_kwargs,
                            })
                self.__logInfo__('construct instance')
                self.pn=lWid(self.pnMain,id=wx.NewId(),pos=pnPos,size=pnSz,
                            style=pnStyle,name=pnName,**kwargs)
                if hasattr(self.pn,'GetWid'):
                    pnTmp=self.pn.GetWid()
                else:
                    pnTmp=self.pn
                self.bxsData.AddWindow(pnTmp, 1, border=4,
                        flag=wx.BOTTOM | wx.TOP | wx.LEFT | wx.RIGHT | wx.EXPAND)
                if iKind==0:
                    pass
                else:
                    if self.bxsBt is not None:
                        self.bxsData.AddSizer(self.bxsBt, 0, border=4,
                            flag=wx.BOTTOM | wx.ALIGN_CENTER)
            self.bxsData.Layout()
            self.bxsData.Fit(self.frm)
        except:
            self.__logTB__()
        
        if iKind==2:
            self.cbClose = wx.lib.buttons.GenBitmapTextButton(id=-1, 
                label=_(u'Close'),name=u'cbClose',
                bitmap=self.getBmp('core','Close'),
                parent=self.pnMain, pos=wx.Point(0, 24),
                size=wx.Size(76, 30), style=0)
            self.cbClose.Bind(wx.EVT_BUTTON, self.OnCbCloseButton,self.cbClose)
            self.bxsBt.AddWindow(self.cbClose, 0, border=0, flag=0)
        elif iKind==1:
            self.cbApply = wx.lib.buttons.GenBitmapTextButton(id=-1,
                label=_(u'Apply'),name=u'cbApply',
                bitmap=self.getBmp('core','Apply'),
                parent=self.pnMain, pos=(0, 0), size=(120, 30),style=0)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
            
            self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
            
            self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=-1,
                label=_(u'Cancel'),name=u'cbCancel',
                bitmap=self.getBmp('core','Cancel'),
                parent=self.pnMain, pos=(0,0), size=(120,30), style=0)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==0:
            self.cbCancel = wx.BitmapButton(id=-1,name=u'cbCancel',
                    bitmap=self.getBmp('core','Cancel'),
                    parent=self.pnMain, pos=(0,0), size=(30,30), 
                    style=wx.BU_AUTODRAW)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                    self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, 
                    flag=wx.ALIGN_CENTER)
            
            self.cbApply = wx.BitmapButton(id=-1,name=u'cbApply',
                    bitmap=self.getBmp('core','Apply'),
                    parent=self.pnMain, pos=(0, 0), size=(30,30),
                    style=wx.BU_AUTODRAW)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                    self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0, 
                    flag=wx.ALIGN_CENTER)
        elif iKind==3:
            self.cbOk = wx.lib.buttons.GenBitmapTextButton(id=-1, 
                    label=_(u'Okay'),name=u'cbOk',
                    bitmap=self.getBmp('core','Apply'),
                    parent=self.pnMain, pos=(0,0),size=(76, 30), 
                    style=0)
            self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,self.cbOk)
            self.bxsBt.AddWindow(self.cbOk, 0, border=0, flag=0)
        elif iKind==4:
            self.cbOk = wx.lib.buttons.GenBitmapTextButton(id=-1,
                    label=_(u'Okay'),name=u'cbOk',
                    bitmap=self.getBmp('core','Apply'),
                    parent=self.pnMain, pos=(0, 0), size=(120, 30),
                    style=0)
            self.cbOk.Bind(wx.EVT_BUTTON, self.OnCbOkButton,self.cbOk)
            self.bxsBt.AddWindow(self.cbOk, 0, border=0, flag=wx.ALIGN_CENTER)
            
            self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
            
            self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=-1,
                    label=_(u'Cancel'),name=u'cbCancel',
                    bitmap=self.getBmp('core','Close'),
                    parent=self.pnMain, pos=(0,0), size=(120,30),
                    style=0)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==5:
            self.cbYes = wx.lib.buttons.GenBitmapTextButton(id=-1,
                    label=_(u'Yes'),name=u'cbYes',
                    bitmap=self.getBmp('core','Apply'),
                    parent=self.pnMain, pos=(0, 0), size=(120, 30),
                    style=0)
            self.cbYes.Bind(wx.EVT_BUTTON, self.OnCbYesButton,self.cbYes)
            self.bxsBt.AddWindow(self.cbYes, 0, border=0, flag=wx.ALIGN_CENTER)
            
            self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
            
            self.cbNo = wx.lib.buttons.GenBitmapTextButton(id=-1,
                    label=_(u'No'),name=u'cbNo',
                    bitmap=self.getBmp('core','Close'),
                    parent=self.pnMain, pos=(0,0), size=(120,30),
                    style=0)
            self.cbNo.Bind(wx.EVT_BUTTON, self.OnCbNoButton,self.cbNo)
            self.bxsBt.AddWindow(self.cbNo, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==6:
            self.cbApply = wx.BitmapButton(id=-1,name=u'cbApply',
                    bitmap=self.getBmp('core','Close'),
                    parent=self.pnMain, pos=(0,0),size=(30,30),
                    style=wx.BU_AUTODRAW)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                    self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0, flag=wx.ALIGN_CENTER)
        elif iKind==20:
            self.cbCancel = wx.BitmapButton(id=-1,name=u'cbCancel',
                    bitmap=self.getBmp('core','Close'),
                    parent=self.pnMain,pos=(0,0),size=(24,24),
                    style=wx.BU_AUTODRAW)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                    self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0,
                    flag=wx.ALIGN_CENTER)
            
            self.cbApply = wx.BitmapButton(id=-1,name=u'cbApply',
                    bitmap=self.getBmp('core','Apply'),
                    parent=self.pnMain,pos=(0,0),size=(24,24),
                    style=wx.BU_AUTODRAW)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                    self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0,
                    flag=wx.ALIGN_CENTER)
        elif iKind>=10:
            #if iKind==11:
            #    InitMsgWid()
            pass
        else:
            self.cbApply = wx.lib.buttons.GenBitmapTextButton(id=-1,
                    label=_(u'Apply'),name=u'cbApply',
                    bitmap=self.getBmp('core','Apply'),
                    parent=self.pnMain,pos=(0, 0),size=(120, 30),style=0)
            self.cbApply.Bind(wx.EVT_BUTTON, self.OnCbApplyButton,
                    self.cbApply)
            self.bxsBt.AddWindow(self.cbApply, 0, border=0,
                    flag=wx.ALIGN_CENTER)
            
            self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
            
            self.cbCancel = wx.lib.buttons.GenBitmapTextButton(id=-1,
                    label=_(u'Cancel'),name=u'cbCancel',
                    bitmap=self.getBmp('core','Cancel'),
                    parent=self.pnMain, pos=(0,0), size=(120,30), style=0)
            self.cbCancel.Bind(wx.EVT_BUTTON, self.OnCbCancelButton,
                    self.cbCancel)
            self.bxsBt.AddWindow(self.cbCancel, 0, border=0,
                    flag=wx.ALIGN_CENTER)
        try:
            if 'kwTopCmd' in kwargs:
                kwTopCmd=kwargs['kwTopCmd']
                self.__logDebug__({'kwTopCmd':kwTopCmd})
                kwTopCmd['parent']=self.pnMain
                self.pnTopCmd=vGuiPanelWX(**kwTopCmd)
                self.bxsBt.AddSpacer(wx.Size(32, 8), border=0, flag=0)
                self.bxsBt.AddWindow(self.pnTopCmd.GetWid(), 0, border=0,
                        flag=wx.ALIGN_CENTER)
        except:
            self.__logTB__()
        #self.pnMain.SetSizer(self.gbsData)
        self.pnMain.SetSizer(self.bxsData)
        if bDbg:
            self.__logDebug__('iArrange:%d;widArrange:%r'%(iArrange,widArrange))
        if iArrange>=0:
            if (iArrange<10) or ((iArrange&0x40)!=0):
                if bDbg:
                    self.__logDebug__({'size':size,
                            'pnMain':self.pnMain.GetSize(),
                            'frm':self.frm.GetSize(),
                            'iArrange':iArrange,
                            '_iArrange':self._iArrange})
                bFound=False
                if size is not None:
                    if size!=(-1,-1):
                        self.frm.SetSize(size)
                        bFound=True
                if bFound==False:
                    self.bxsData.Layout()
                    self.bxsData.Fit(self.frm)
                    #self.gbsData.Layout()
                    #self.gbsData.Fit(self.frm)
                    self._bLayout=True
                    if bDbg:
                        self.__logDebugAdd__('layout',{'size':size,
                            'pnMain':self.pnMain.GetSize(),
                            'frm':self.frm.GetSize(),
                            'iArrange':iArrange,
                            '_iArrange':self._iArrange})
            
            if widArrange is None:
                widArrange=parent
            vtGuiCoreArrangeWidget(widArrange,self.frm,iArrange)
            
            if bAnchor:
                widAnchor=self.GetAnchor(widArrange)
                
                #wx.CallAfter(self._bindWidArrange,widArrange)
                wx.CallAfter(widAnchor.Bind,wx.EVT_MOVE,self.OnMoveWidArrange)
                wx.CallAfter(self.OnMoveWidArrange,None)
                if (iArrange&0x40)!=0:
                    wx.CallAfter(widAnchor.Bind,wx.EVT_SIZE,self.OnSizeWidArrange)
                    #widAnchor.Bind(wx.EVT_SIZE,self.OnSizeWidArrange)
                self._widArrange=widArrange
                self._iArrange=iArrange
            #self.gbsData.Layout()
            if bDbg:
                self.__logDebugAdd__('arrange done',{'size':size,
                            'pnMain':self.pnMain.GetSize(),
                            'frm':self.frm.GetSize(),
                            'iArrange':iArrange,
                            '_iArrange':self._iArrange})
        else:
            if size is not None:
                self.frm.SetSize(size)
            else:
                #self.Fit()
                #self.gbsData.Layout()
                #self.gbsData.Fit(self)
                self.bxsData.Layout()
                self.bxsData.Fit(self.frm)
                self._bLayout=True
            if pos is not None:
                self.frm.Move(pos)
            else:
                if parent is not None:
                    self.frm.Centre()
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def _bindWidArrange(self,widAnchor):
        try:
            self.__logDebug__('widAnchor:%r'%(widAnchor))
            wx.CallAfter(widAnchor.Bind,wx.EVT_MOVE,self.OnMoveWidArrange)
            wx.CallAfter(self.OnMoveWidArrange,None)
            self.__logCritical__('_iArrange:%4d 0x%04x'%(self._iArrange,
                    self._iArrange))
            if (self._iArrange&0x40)!=0:
                wx.CallAfter(widAnchor.Bind,wx.EVT_SIZE,self.OnSizeWidArrange)
                #widAnchor.Bind(wx.EVT_SIZE,self.OnSizeWidArrange)
        except:
            self.__logTB__()
    def __makeTitle__(self):
        sOldTitle=self.frm.GetTitle()
        #l=["VIDARC",self.appl]
        l=[self.appl]
        sAdd=self.__getTitleInfo__()
        if sAdd is not None:
            l.append(sAdd)
        s=' '.join(l)
        if sOldTitle!=s:
            self.frm.SetTitle(s)
    def OnMoveWidArrange(self,evt):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'evt':evt,
                        'sz pnMain':self.pnMain.GetSize(),
                        'sz':self.frm.GetSize(),
                        'iArrange':self._iArrange,
                        '_widArrange':self._widArrange,
                        '_bLayout':self._bLayout,
                        })
            
        except:
            self.__logTB__()
        if evt is not None:
            evt.Skip()
        try:
            if self._iArrange>=0:
                vtGuiCoreArrangeWidget(self._widArrange,self.frm,self._iArrange)
        except:
            self.__logTB__()
    def OnSizeWidArrange(self,evt):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'evt':evt,
                        'sz pnMain':self.pnMain.GetSize(),
                        'sz':self.frm.GetSize(),
                        'iArrange':self._iArrange,
                        '_widArrange':self._widArrange,
                        '_bLayout':self._bLayout,
                        })
        except:
            self.__logTB__()
        if evt is not None:
            evt.Skip()
        try:
            if self._iArrange>=0:
                wx.CallAfter(vtGuiCoreArrangeWidget,self._widArrange,self.frm,self._iArrange)
        except:
            self.__logTB__()
    def GetWid(self):
        return self.frm
    def GetWidChild(self):
        return [self]
    def SetSize(self,sz):
        try:
            self._bLayout=False
            self.frm.SetSize(sz)
        except:
            self.__logTB__()
    def OnCbCloseButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.Post('close')
        except:
            self.__logTB__()
    def OnCbOkButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.Post('ok')
        except:
            self.__logTB__()
    def OnCbYesButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.Post('yes')
        except:
            self.__logTB__()
    def OnCbNoButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.Post('no')
        except:
            self.__logTB__()
    def OnCbCancelButton(self, event):
        event.Skip()
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.Post('cancel')
            self.PostCancel(None)
        except:
            self.__logTB__()
    def OnCbApplyButton(self,evt):
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.Post('ok')
            self.PostOk(None)
            #wx.PostEvent(self,vtgxDialogOk(self))
            #if self.IsModal():
            #    self.EndModal(1)
            #else:
            #    self.Show(False)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            if hasattr(self.pn,'__Close__'):
                self.pn.__Close__()
        except:
            self.__logTB__()
    def OnFrameClose(self,evt):
        try:
            self.__logDebug__(''%())
            self.Show(False)
            self.__Close__()
        except:
            self.__logTB__()
    def OnFrameMainClose(self,evt):
        try:
            self.__logDebug__(''%())
            iVeto=evt.CanVeto()
            self.__logInfo__('CanVeto:%s'%(iVeto))
            if self.IsShutDownFinished()==False:
                self.__logDebug__('shutdown not finished'%())
                if self.IsShutDownActive()==False:
                    self.__logDebug__('shutdown initiate'%())
                    self.Post('shutdown')
                    if iVeto:
                        evt.Veto()
                    else:
                        evt.Skip()
                    self.ShutDown()
                else:
                    if iVeto:
                        evt.Veto()
                    else:
                        evt.Skip()
                self.__logDebug__('try close bit later'%())
                self.CallBackDelayed(1,self.Close)
            else:
                self.__logDebug__('shutdown finished'%())
                evt.Skip()
                if self.bTopWid==True:
                    self.__logDebug__('i am toplevel widget'%())
                    self.EnableTopWid(False)
        except:
            self.__logTB__()
            evt.Skip()
    def OnDestroy(self,evt):
        try:
            self.__logDebug__(''%())
            if self.IsShutDownFinished()==False:
                self.__logDebug__('shutdown not finished'%())
                if self.IsShutDownActive()==False:
                    self.__logDebug__('shutdown initiate'%())
                    self.Post('shutdown')
                    self.ShutDown()
                self.__logDebug__('try destory bit later'%())
                iTimeOut=100
                while (self.IsShutDownFinished()==False) and (iTimeOut>0):
                    vpc.vcOsUSleep(100)
                    iTimeOut=iTimeOut-1
            else:
                self.__logDebug__('shutdown finished'%())
                evt.Skip()
                if self.bTopWid==True:
                    self.__logDebug__('i am toplevel widget'%())
                    self.EnableTopWid(False)
        except:
            self.__logTB__()
            evt.Skip()
        self.__logDebug__('done')
    def Show(self,bFlag=True):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'bFlag':bFlag})
            self.frm.Show(bFlag)
            if bFlag==True:
                self.Post('show')
                self.frm.SetFocus()
            else:
                self.Post('hide')
                self.__Close__()
        except:
            self.__logTB__()
    def Close(self):
        try:
            self.__logDebug__('')
            self.frm.Close()
        except:
            self.__logTB__()
    def IsShown(self):
        try:
            return self.frm.IsShown()
        except:
            self.__logTB__()
        return False
    #def ShutDown(self):
    #        if self.IsShutDownFinished()==False:
    #            self.__logDebug__('shutdown not finished'%())
    #            if self.IsShutDownActive()==False:
    #                self.__logDebug__('shutdown initiate'%())
    #                self.Post('shutdown')
    #    
    #    vGuiFrmCoreWX.ShutDown(self)
    def SetFocus(self):
        try:
            if self.IsShown()==False:
                self.frm.Show(True)
            if hasattr(self,'cbClose'):
                self.cbClose.SetFocus()
            else:
                self.frm.SetFocus()
        except:
            self.__logTB__()
    def GetPosFrm(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            return self.frm.GetPositionTuple()
        except:
            self.__logTB__()
    def SetPosFrm(self,t):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.frm.Move(t)
        except:
            self.__logTB__()
    def GetSizeFrm(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            return self.frm.GetSize()
        except:
            self.__logTB__()
    def SetSizeFrm(self,t):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.frm.SetSize(t)
        except:
            self.__logTB__()
    def GetPanel(self):
        return self.pn
    def __getattr__(self,name):
        try:
            if hasattr(self._vcOrg,name):
                return getattr(self._vcOrg,name)
            if hasattr(self.pn,name):
                return getattr(self.pn,name)
            if hasattr(self.frm,name):
                return getattr(self.frm,name)
            #if hasattr(self,name):
            #    return getattr(self,name)
        except:
            #vLogFallBack.logTB(__name__)
            #self.__logTB__()
            if VERBOSE>10:
                if name!='pn':
                    vpc.logDebug('name:%s'%(name),__name__)
        raise AttributeError(name)
    def OnMove(self,evt):
        evt.Skip()
        try:
            pos=evt.GetPosition()
            if VERBOSE>5:
                self.__logDebug__('pos:%s;iArrage:%d'%(self.__logFmt__(pos),
                        self._iArrange))
        except:
            self.__logTB__()
    def OnSize(self,evt):
        evt.Skip()
        try:
            sz=evt.GetSize()
            self.DoSizeStatusBar()
            if VERBOSE>5:
                self.__logDebug__('sz:%r;iArrage:%r'%(sz,
                        self._iArrange))
        except:
            self.__logTB__()
    def OnClose(self,evt):
        try:
            self.Show(False)
        except:
            self.__logTB__()
    def HandleKey(self,evt,bRetDict=False,bApply=True,bCancel=True):
        evt.Skip()
        iCode=-1
        bAlt=False
        bCtrl=False
        bMeta=False
        bShift=False
        try:
            cVal=u'%c'%evt.GetUnicodeKey()
            iCode=evt.GetKeyCode()
            bAlt=evt.AltDown()
            bCtrl=evt.ControlDown()
            bMeta=evt.MetaDown()
            bShift=evt.ControlDown()
            if bCancel==True:
                if iCode==wx.WXK_ESCAPE:
                    self.OnCbCancelButton(None)
            if bApply==True:
                if iCode==wx.WXK_RETURN:
                    self.OnCbApplyButton(None)
                if iCode==wx.WXK_NUMPAD_ENTER:
                    self.OnCbApplyButton(None)
        except:
            self.__logTB__()
        if bRetDict==True:
            return {'v':cVal,'code':iCode,'shift':bShift,'alt':bAlt,'ctrl':bCtrl,'meta':bMeta}
    def DoHelp(self):
        try:
            self.PrintMsg(_(u'show help'))
            self.__logDebug__('')
        except:
            self.__logTB__()
    def DoAbout(self):
        try:
            tup=self.pn.GetAboutData()
            dlg=vAboutDialog.create(self,self.__getPluginBitmap__(),
                    tup[0],tup[1],tup[2])
            dlg.Centre()
            dlg.ShowModal()
            dlg.Destroy()
        except:
            self.__logTB__()
    def DoLog(self):
        try:
            frm=vtLogFileViewerFrame(self)
            frm.Centre()
            sFN=vtLog.vtLngGetFN()
            if sFN is not None:
                frm.OpenFile(sFN)
            frm.Show()
        except:
            self.__logTB__()
    def DoErrRep(self):
        try:
            self.PrintMsg(_(u'collecting application status ...'))
            sLogFN=vtLog.vtLngGetFN()
            #sPicFN=self.ScreenShotTop()
            sDT=time.strftime('%Y%m%d_%H%M%S',time.localtime(time.time()))
            sFN=''.join([sLogFN,'_',sDT,'.zip'])
            zipFile=zipfile.ZipFile(sFN,'w',zipfile.ZIP_DEFLATED)
            if type(sLogFN)==types.UnicodeType:
                    sLogFN=sLogFN.encode('ISO-8859-1')
            zipFile.write(str(sLogFN))
            for fn in self.lScreeShots:
                if type(fn)==types.UnicodeType:
                        fn=fn.encode('ISO-8859-1')
                zipFile.write(str(fn))
            zipFile.close()
            vtLog.PrintMsg(_(u'application status collected.'))
            dlg = wx.MessageDialog(self, _(u'Please send file\n%s\nto us (mailto:office@vidarc.com).\n\nDo you want to send it now?')%(sFN),
                                    _(u'VIDARC Error Report Notification'),
                                    wx.YES | wx.NO | wx.ICON_INFORMATION
                                    )
            if dlg.ShowModal()==wx.ID_YES:
                webbrowser.open("mailto:office@vidarc.com?subject=error report %s&body=do not forget to attach file:%s manually!!!"%(sDT,sFN))
            dlg.Destroy()
        except:
            self.__logTB__()
    def DoScreenShotAll(self):
        try:
            app=wx.GetApp()
            frm=app.GetTopWindow()
            sPicFN=vtLogScreen.genScreenShot(frm,frm)
            self.lScreeShots.append(sPicFN)
        except:
            self.__logTB__()
    def DoScreenShotWnd(self):
        try:
            sPicFN=vtLogScreen.genScreenShot(self,self,self.appl)
            self.lScreeShots.append(sPicFN)
        except:
            self.__logTB__()
