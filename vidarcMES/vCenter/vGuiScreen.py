# -*- coding: iso-8859-1 -*-
#----------------------------------------------------------------------
# Name:         vGuiScreen.py
# Purpose:      screen related functions
#
# Author:       Walter Obweger
#
# Created:      20120215
# CVS-ID:       $Id: vGuiScreen.py,v 1.1 2012/02/17 07:06:29 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vSystem import isPlatForm
if isPlatForm('win'):
    from vGuiScreenWin import *
else:
    from vGuiScreenDummy import *
