#----------------------------------------------------------------------------
# Name:         vGuiPanelWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120210
# CVS-ID:       $Id: vGuiPanelWX.py,v 1.62 2016/02/23 14:43:14 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import types,os
    import wx
    import wx.lib.buttons
    VERBOSE=vpc.getVerboseType(__name__)
    #from vidarc.tool.log.vtLog import vtLogOriginWX
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCorePanWX import vGuiCorePanWX
    from vGuiPopupButtonWX import vGuiPopupButtonWX
    from vGuiTextPopupWX import vGuiTextPopupWX
    from vGuiTextStyledWX import vGuiTextStyledWX
    from vGuiPopupEditListWX import vGuiPopupEditListWX
    from vGuiFileBrowser import vGuiFileBrowser
    from vGuiDirBrowser import vGuiDirBrowser
    from vGuiDlgMsg import vGuiDlgMsg
    from vGuiTextDataWX import vGuiTextDataWX
    from vGuiListCtrlWX import vGuiListCtrlWX
    from vGuiTreeCtrlWX import vGuiTreeCtrlWX
    from vGuiImgLstWX import vGuiImgLstWX,vGuiImgLstGet
    from vGuiNumWX import vGuiNumWX
    from vGuiTreeListCtrlWX import vGuiTreeListCtrlWX
    from vGuiChoiceWX import vGuiChoiceWX
    from vGuiBitsWX import vGuiBitsWX
    #from vidarc.tool.misc.vtmMsgDialog import vtmMsgDialog
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

CACHE_ID={}

def getWidId(par,sN):
    global CACHE_ID
    try:
        kPar=par.GetName()#par.GetId()
        if kPar in CACHE_ID:
            d=CACHE_ID[kPar]
        else:
            d={}
            CACHE_ID[kPar]=d
        if sN in d:
            return d[sN]
        else:
            iId=wx.NewId()
            d[sN]=iId
            return iId
    except:
        vpc.logTB(__name__)
        try:
            par.__logTB__()
        except:
            vpc.logTB(__name__)
    return wx.NewId
def GetRelevantNamesWX(par,appl=None):
    if hasattr(par,'_originLogging'):
        #if VERBOSE:
        #    vtLngCur(DEBUG,';'.join(['par def',par._originLogging]),'vtLog')
        return par._originLogging
    lName=[]
    if appl is not None:
        lName.append(appl)
    lName.append(par.GetName())
    wid=par
    par=par.GetParent()
    while par is not None:
        if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog']:
            lName.append(par.GetName())
        elif par.GetClassName() in ['wxMDIParentFrame',]:
            lName.append(par.GetName())
            break
        elif getattr(par,'IsLogRelevant',False):
            lName.append(par.GetName())
        par=par.GetParent()
    lName.reverse()
    s='.'.join(lName)
    if VERBOSE>10:
        vpc.logDebug('lName=%r'%(lName),__name__)
    #    vtLngCur(DEBUG,';'.join(['calc',s]),'vtLog')
    wid._originLogging=s
    del lName
    return s
def GetPrintMsgWid(wid,orig=None):
    if hasattr(wid,'_originPrint'):
        if orig is not None:
            orig._originPrint=wid._originPrint
        return wid._originPrint
    if orig is None:
        orig=wid
    if hasattr(wid,'PrintMsg'):
        if orig is not None:
            orig._originPrint=wid
        return wid
    else:
        par=wid.GetParent()
        if par is None:
            return None
        return GetPrintMsgWid(par,orig)

class vGuiPanelWX(vGuiCoreWX):
    SIZE_DFT=(100,34)
    YESNO=0x1
    OK=0x2
    OK=0x4
    OPEN=0x10
    SAVE=0x20
    MULTIPLE=0x40
    EXCLAMATION=0x100
    ERROR=0x200
    INFORMATION=0x300
    QUESTION=0x400
    FLEX_SIZER=0
    def __initCreate__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                if self.GetVerboseDbg(10):
                    self.__logDebug__({'args':args,'kwargs':kwargs})
                else:
                    self.__logDebug__('')
            wid=self.__initWid__(*args,**kwargs)
            if wid is not None:
                self.SetOriginByWid(wid)
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
    #def __init__(self,*args,**kwargs):
    #    vGuiCoreWX.__init__(self)#,bExtended=True)
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__({'args':args,'kwargs':kwargs})
        _args,_kwargs=self.GetGuiArgs(kwargs,
                ['id','name','parent','pos','size','style'],
                {'pos':(0,0),'size':self.SIZE_DFT,'style':wx.TAB_TRAVERSAL})
        bDbgDetail=self.GetVerboseDbg(10)       # 20150426 wro:for more detail
        #apply(wx.Panel.__init__,(self,) + args,_kwargs)
        self.pn=wx.Panel(*args,**_kwargs)       # 120515 wro what right
        #self.pn=wx.Panel(*_args,**_kwargs)
        self.SetOriginByWid(self.pn)
        #s=GetRelevantNamesWX(self.pn)
        #self.SetOrigin(s)
        self.lWid=[]
        self._iSizerLayout=-1
        if 'lWid' in kwargs:
            lWid=kwargs['lWid']
            t=type(lWid)
        else:
            t=self.__initCtrlWid__()
            if t is not None:
                lWid=t
                t=type(lWid)
            else:
                #lWid=[]
                lWid=None       # 20150426 wro:do not add what is not there
        if bDbgDetail:          # 20150426 wro:I need to know
            self.__logDebug__({'lWid':lWid,'t':t})
        
        self.bMod=False
        self.bBlk=False
        
        self.SetBlock()
        self.ClrBlockDelayed()
        
        self.bEnMark=kwargs.get('bEnMark',True)
        self.bEnMod=kwargs.get('bEnMod',True)
        self.bLogRelWid=kwargs.get('bLogRelWid',True)
        self.widVal=kwargs.get('widVal',None)
        #lWid=[]
        self.iLayout=kwargs.get('iLayout',None)
        self.__logDebug__({'iLayout':self.iLayout})
        if t==types.DictType:
            self.iLayout=1
            self.pn.Bind(wx.EVT_SIZE, self.OnSize)
            iId=getWidId(self.pn,'slwNav')
            self.slwNav = wx.SashLayoutWindow(id=iId,
                  name=u'slwNav', parent=self.pn, pos=wx.Point(0, 0), size=wx.Size(50,
                  100), style=wx.CLIP_CHILDREN | wx.SW_3D)
            self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
            self.slwNav.SetAutoLayout(True)
            self.slwNav.SetLabel(_(u'Navigation'))
            self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
            self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
            self.slwNav.SetToolTipString(_(u'navigation'))
            #self.slwNav.SetDefaultSize(wx.Size(50, 1000))
            self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,id=iId)
            if 'slwNav' in lWid:
                _kwargs=lWid['slwNav']
                _args,_kwargs=self.GetGuiArgs({'id':-1,'parent':self.slwNav,
                    'name':'pnNav',},
                    ['id','name','parent','pos','size','style'],{
                    'name':u'pnNav',
                    'pos':None,'size':(100,100),'style':wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL
                    },_kwargs)
                sz=_kwargs['size']
                if bDbgDetail:                  # 20150426 wro:I need to know
                    self.__logDebugAdd__('slwNav',
                            {'sz':sz,'_kwargs':_kwargs})
                self.slwNav.SetDefaultSize(wx.Size(sz[0], -1))
                self.pnNav=vGuiPanelWX(**_kwargs)
            
            if 'slwTop' in lWid:
                self.iLayout=self.iLayout+1
                iId=getWidId(self.pn,'slwTop')
                self.slwTop = wx.SashLayoutWindow(id=iId,
                      name=u'slwTop', parent=self.pn, pos=wx.Point(50, 0),
                      size=wx.Size(100, 50), style=wx.CLIP_CHILDREN | wx.SW_3D)
                self.slwTop.SetDefaultSize(wx.Size(1000, 50))
                self.slwTop.SetAlignment(wx.LAYOUT_TOP)
                self.slwTop.SetAutoLayout(True)
                self.slwTop.SetSashVisible(wx.SASH_BOTTOM, True)
                self.slwTop.SetOrientation(wx.LAYOUT_HORIZONTAL)
                self.slwTop.SetLabel(_(u'information'))
                self.slwTop.SetToolTipString(_(u'information'))
                self.slwTop.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwTopSashDragged,id=iId)
                _kwargs=lWid['slwTop']
                _args,_kwargs=self.GetGuiArgs({'id':-1,'parent':self.slwTop,
                    'name':'pnTop',},
                    ['id','name','parent','pos','size','style'],{
                    'name':u'pnTop',
                    'pos':None,'size':(100,100),'style':wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL
                    },_kwargs)
                sz=_kwargs['size']
                if bDbgDetail:                  # 20150426 wro:I need to know
                    self.__logDebugAdd__('slwTop',
                            {'sz':sz,'_kwargs':_kwargs})
                self.slwTop.SetDefaultSize(wx.Size(1000, sz[1]))
                #self.pnTop=vGuiPanelWX(**_kwargs)
                self.pnTop=vGuiPanelWX(**_kwargs)
            
            iId=getWidId(self.pn,'pnData')
            if 'pnData' in lWid:
                _kwargs=lWid['pnData']
                _args,_kwargs=self.GetGuiArgs({'id':-1,'parent':self.pn,
                    'name':'pnData','size':(100,100),
                    'style':wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL},
                    ['id','name','parent','pos','size','style',],{
                    'name':u'pnData',
                    'pos':None,'size':None,'style':None
                    },_kwargs)
                if bDbgDetail:                  # 20150426 wro:I need to know
                    self.__logDebugAdd__('pnData',
                            {'sz':sz,'_kwargs':_kwargs})
                self.pnData=vGuiPanelWX(**_kwargs)
                self.pnDataWX=self.pnData.pn
                wx.LayoutAlgorithm().LayoutWindow(self.pn, self.pnDataWX)
                self.pnDataWX.Refresh()
            else:
                if bDbgDetail:                  # 20150426 wro:I need to know
                    self.__logDebug__('pnData;dft')
                self.pnData = wx.Panel(id=iId,
                    name=u'pnData', parent=self.pn,
                    style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

                wx.LayoutAlgorithm().LayoutWindow(self.pn, self.pnData)
                self.pnData.Refresh()
            if 'ctrl' in lWid:
                _kwargs=lWid['pnData']
                self.__initCtrl__(**_kwargs)
            self.__initObj__(*args,**kwargs)
            return
        elif lWid is None:
            if bDbgDetail:                  # 20150426 wro:I need to know
                self.__logDebugAdd__('lWid;unknown',
                        {'lWid':lWid,'kwargs':kwargs})
            pass
        else:
            if bDbgDetail:                  # 20150426 wro:I need to know
                self.__logDebugAdd__('lWid;added',
                        {'lWid':lWid,'kwargs':kwargs})
            kwargs['lWid']=lWid
        bFlexGridSizer=kwargs.get('bFlexGridSizer',self.FLEX_SIZER)
        if bDbg:
            self.__logDebug__([kwargs,_kwargs])
            self.__logDebug__({'FLEX_SIZER':self.FLEX_SIZER,
                    'bFlexGridSizer':bFlexGridSizer})
        bGowable=False
        if bFlexGridSizer==0:
            self.gbsData = wx.GridBagSizer(hgap=4, vgap=4)
            bGowable=True
            self.FLEX_SIZER=0
        elif bFlexGridSizer==-1:
            self.gbsData = wx.GridBagSizer(hgap=0, vgap=0)
            bGowable=True
            self.FLEX_SIZER=0
        elif bFlexGridSizer in [10,'bxVert']:
            self.gbsData = wx.BoxSizer(orient=wx.VERTICAL)
            self.FLEX_SIZER=2
        elif bFlexGridSizer in [20,'bxHor']:
            self.gbsData = wx.BoxSizer(orient=wx.HORIZONTAL)
            self.FLEX_SIZER=3
        else:
            t=type(bFlexGridSizer)
            if bDbg:
                self.__logDebug__({'t':t,
                        'TupleType':types.TupleType,
                        'bFlexGridSizer':bFlexGridSizer})
            if t==types.TupleType:
                self.gbsData = wx.FlexGridSizer(hgap=4, vgap=4,
                        cols=bFlexGridSizer[1],
                        rows=bFlexGridSizer[0])
                bGowable=True
            else:
                self.gbsData = wx.FlexGridSizer(hgap=4, vgap=4,
                        cols=bFlexGridSizer,
                        rows=1)
                bGowable=True
            self.FLEX_SIZER=1
        
        #self.IsMainThread()
        lGrowRow,lGrowCol=self.__initCtrl__(*args,**kwargs)
        if lGrowRow is None:
            if 'lGrowRow' in kwargs:
                lGrowRow=kwargs['lGrowRow']
        if lGrowCol is None:
            if 'lGrowCol' in kwargs:
                lGrowCol=kwargs['lGrowCol']
        if bGowable==True:
            if lGrowRow is not None:
                for t in lGrowRow:
                    self.gbsData.AddGrowableRow(t[0],t[1])
            if lGrowCol is not None:
                for t in lGrowCol:
                    self.gbsData.AddGrowableCol(t[0],t[1])
        self.pn.SetSizer(self.gbsData)
        if (lGrowRow is not None) and (lGrowCol is not None):
            #self.gbsData.Layout()      # 20150426
            self.gbsData.Fit(self.pn)
            self.gbsData.Layout()       # 20150426
        if bDbg:
            self.__logDebug__([lGrowRow,lGrowCol,
                'pos',self.pn.GetPosition(),'size',self.pn.GetSize()])
    def __initCtrlWid__(self):
        return None
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.__logWarn__('should be overloaded')
        except:
            self.__logTB__()
    def GetWid(self):
        return self.pn
    def GetWidMod(self):
        return None#self.pn
    def GetWidChild(self):
        return [self,self.pn]
    def IsLogRelevant(self):
        return self.bLogRelWid
    def __getattr__(self,name):
        try:
            if hasattr(self._vcOrg,name):
                return getattr(self._vcOrg,name)
            if hasattr(self.pn,name):
                return getattr(self.pn,name)
        except:
            vpc.logTB()
            vpc.logError('cls:%s;nmae:%s'%(__name__,name))
            if VERBOSE>10:
                if name!='pn':
                    vpc.logDebug('name:%s'%(name),__name__)
        raise AttributeError(name)
    def __initCtrl__(self,*args,**kwargs):
        """ override this
          return lGrowRow,lGrowCol
          lGrowRow=[] or [(1,2),(2,1)] or None
          lGrowCol=[] or [(1,2),(2,1)] or None
          tuple[0] ... number row or col
          tuple[1] ... proportion
          if both or None no layout is calculated
        """
        try:
            if VERBOSE>0:
                self.__logDebug__('started')
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            if 'lWid' in kwargs:
                self.__add_widget__(kwargs['lWid'])
        except:
            self.__logTB__()
        if VERBOSE>0:
            self.__logDebug__('done')
        return None,None
    def __getClsDftArgs__(self,cls):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
                self.__logDebug__('cls:%s'%(cls))
        if cls=='slLf':return {
            'cls':  'slLf',
            'CLS':  wx.SashLayoutWindow,
            'kw':   {'style':wx.CLIP_CHILDREN | wx.SW_3D},
            }
        if cls=='slTp':return {
            'cls':  'slTp',
            'CLS':  wx.SashLayoutWindow,
            'kw':   {
                    'style':wx.CLIP_CHILDREN | wx.SW_3D,
                    },
            }
        if cls=='lblLf':return {
            'cls':  'lblLf',
            'CLS':  wx.StaticText,
            'key':  ['id','name','parent','pos','size','style','label'],
            'kw':   {
                    'style':wx.ALIGN_LEFT,
                    },
            'flag': wx.EXPAND,
            #'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='lblRg':return {
            'cls':  'lblRg',
            'CLS':  wx.StaticText,
            'key':  ['id','name','parent','pos','size','style','label'],
            'kw':   {
                    'style':wx.ALIGN_RIGHT,
                    },
            'flag': wx.EXPAND,
            #'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='lblCt':return {
            'cls':  'lblCt',
            'CLS':  wx.StaticText,
            'key':  ['id','name','parent','pos','size','style','label'],
            'kw':   {
                    'style':wx.ALIGN_CENTRE,
                    },
            'flag': wx.EXPAND,
            #'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='bar':return {
            'cls':  'bar',
            'CLS':  wx.Gauge,
            'key':  ['id','name','parent','pos','size','style','range'],
            'kw':   {
                    'size':(-1,8),
                    'style':wx.GA_HORIZONTAL|wx.GA_SMOOTH,
                    },
            'flag': wx.EXPAND,
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='barHor':return {
            'cls':  'barHor',
            'CLS':  wx.Gauge,
            'key':  ['id','name','parent','pos','size','style','range'],
            'kw':   {
                    'style':wx.GA_HORIZONTAL|wx.GA_SMOOTH,
                    },
            'flag': wx.EXPAND,
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='barVert':return {
            'cls':  'barVert',
            'CLS':  wx.Gauge,
            'key':  ['id','name','parent','pos','size','style','range'],
            'kw':   {
                    'style':wx.GA_VERTICAL|wx.GA_SMOOTH,
                    },
            'flag': wx.EXPAND,
            }
        if cls=='lbl':return {
            'cls':  'lbl',
            'CLS':  wx.StaticText,
            'key':  ['id','name','parent','pos','size','style','label'],
            'kw':   {
                    'style':wx.ALIGN_RIGHT,
                    },
            'flag': wx.EXPAND,
            #'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='lblBx':return {
            'cls':  'lblBx',
            'CLS':  wx.StaticBox,
            'key':  ['id','name','parent','pos','size','style',],
            'kw':   {
                    'style':wx.ALIGN_RIGHT,
                    },
            'flag': wx.EXPAND,
            #'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='lblBmp':return {
            'cls':  'lblBmp',
            'CLS':   wx.lib.buttons.GenBitmapButton,
            'key':  ['id','name','parent','pos','size','style','bitmap'],
            'kw':   {
                    'style':wx.NO_3D | wx.NO_BORDER,
                    },
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            #'mod':  ['btn'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='cbLbl':return {
            'cls':  'cbLbl',
            'CLS':   wx.Button,
            'key':  ['id','name','parent','pos','size','style',
                    'label',],
            'kw':   {
                    'style':wx.SUNKEN_BORDER,
                },#{'bitmap':wx.EmptyButton(16,16)},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            #'mod':  ['btn'],
            'szMin':(80,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='cbBmp':return {
            'cls':  'cbBmp',
            'CLS':   wx.lib.buttons.GenBitmapButton,
            'key':  ['id','name','parent','pos','size','style',
                    'bitmap',],
            'kw':   {
                    'style':wx.SUNKEN_BORDER,
                },#{'bitmap':wx.EmptyButton(16,16)},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            #'mod':  ['btn'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='cbBmpLbl':return {
            'cls':  'cbBmpLbl',
            'CLS':  wx.lib.buttons.GenBitmapTextButton,
            'key':  ['id','name','parent','pos','size','style',
                    'bitmap','label',],
            'kw':   {
                    'style':wx.SUNKEN_BORDER,
                    },#{'bitmap':wx.EmptyButton(16,16)},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': True,
            #'mod':  ['btn'],
            'szMin':(80,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='cbAdd':return {
            'cls':  'cbAdd',
            'CLS':   wx.lib.buttons.GenBitmapButton,
            'key':  ['id','name','parent','pos','size','style','bitmap'],
            'kw':   {
                    'bitmap':self.getBmp('core','Add'),
                    },
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            #'mod':  ['btn'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='chc':return {
            'cls':  'chc',
            'CLS':  wx.Choice,
            'key':  ['id','name','parent','pos','size','style',
                    'choices'],
            'kw':   {
                    'choices':[],
                    },
            'evtyp':{
                    'choice':wx.EVT_CHOICE,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['choice'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='chk':return {
            'cls':  'chk',
            'CLS':  wx.CheckBox,
            'key':  ['id','name','parent','pos','size','style',],
            'kw':   {
                    'style':0,
                    },
            'evtyp':{
                    'check':wx.EVT_CHECKBOX,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['check'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='chkLst':return {
            'cls':  'chkLst',
            'CLS':  wx.CheckListBox,
            'key':  ['id','name','parent','pos','size','style',
                    'choices'],
            'kw':   {
                    'choices':[],
                    },
            'evtyp':{
                    'check':wx.EVT_CHECKLISTBOX,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['check'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='lst':return {
            'cls':  'lst',
            'CLS':  wx.ListBox,
            'key':  ['id','name','parent','pos','size','style',
                    'choices'],
            'kw':   {
                    'style':0,
                    'size':(100,100),
                    },
            'evtyp':{
                    'sel':wx.EVT_LISTBOX,
                    'dblclk':wx.EVT_LISTBOX_DCLICK,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['sel'],
            'szMin':(30,50),                # 20151209 wro:wx3.0 port
            }
        if cls=='lstMulti':return {
            'cls':  'lstMulti',
            'CLS':  wx.ListBox,
            'key':  ['id','name','parent','pos','size','style',
                    'choices'],
            'kw':   {
                    'style':wx.LB_MULTIPLE,
                    'size':(100,100),
                    },
            'evtyp':{
                    'sel':wx.EVT_LISTBOX,
                    'dblclk':wx.EVT_LISTBOX_DCLICK,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['sel'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='lstExt':return {
            'cls':  'lstExt',
            'CLS':  wx.ListCtrl,
            'key':  ['id','name','parent','pos','size','style',],
            'kw':   {
                    'style':wx.LC_REPORT,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['check'],
            'szMin':(30,50),                # 20151209 wro:wx3.0 port
            }
        if cls=='imgLst':return {
            'cls':  'imgLst',
            'CLS':  vGuiImgLstWX,
            'key':  ['which','dft','lDef',],
            'kw':   {
                    'which':0,
                    'dft':None,
                    'lDef':None,
                    },
            'imgLst':True,
            }
        if cls=='imgLst4lst':return {
            'cls':  'imgLst4lst',
            'CLS':  vGuiImgLstWX,
            'key':  ['which','dft','lDef',],
            'kw':   {
                    'which':0,
                    'dft':'list',
                    'lDef':None,
                    },
            'imgLst':True,
            }
        if cls=='imgLst4tr':return {
            'cls':  'imgLst4tr',
            'CLS':  vGuiImgLstWX,
            'key':  ['which','dft','lDef',],
            'kw':   {
                    'which':0,
                    'dft':'tree',
                    'lDef':None,
                    },
            'imgLst':True,
            }
        if cls=='chcCtrl':return {
            'cls':  'chcCtrl',
            'CLS':  vGuiChoiceWX,
            'key':  None,
            'kw':   {},
            'evtyp':{},
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='lstCtrl':return {
            'cls':  'lstCtrl',
            'CLS':  vGuiListCtrlWX,
            'key':  None,
            'kw':   {
                    'style':wx.LC_REPORT,
                    },
            'evtyp':{
                    'sel':wx.EVT_LIST_ITEM_SELECTED,
                    'desel':wx.EVT_LIST_ITEM_DESELECTED,
                    'dblClk':wx.EVT_LIST_ITEM_ACTIVATED,
                    'act':wx.EVT_LIST_ITEM_ACTIVATED,
                    'activate':wx.EVT_LIST_ITEM_ACTIVATED,
                    'colClk':wx.EVT_LIST_COL_CLICK,
                    'colClkLf':wx.EVT_LIST_COL_CLICK,
                    'colClkRg':wx.EVT_LIST_COL_RIGHT_CLICK,
                    'key':wx.EVT_LIST_KEY_DOWN,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['fb'],
            }
        if cls=='trCtrl':return {
            'cls':  'trCtrl',
            'CLS':  vGuiTreeCtrlWX,
            'key':  None,
            'kw':   {},
            'evtyp':{
                    #'sel':wx.EVT_TREE_ITEM_SELECTED,
                    #'desel':wx.EVT_TREE_ITEM_DESELECTED,
                    'dbClk':wx.EVT_TREE_ITEM_ACTIVATED,
                    'act':wx.EVT_TREE_ITEM_ACTIVATED,
                    'activate':wx.EVT_TREE_ITEM_ACTIVATED,
                    'del':wx.EVT_TREE_DELETE_ITEM,
                    'collapsing':wx.EVT_TREE_ITEM_COLLAPSING,
                    'collapsed':wx.EVT_TREE_ITEM_COLLAPSED,
                    'expanding':wx.EVT_TREE_ITEM_EXPANDING,
                    'expanded':wx.EVT_TREE_ITEM_EXPANDED,
                    'selChg':wx.EVT_TREE_SEL_CHANGING,
                    'sel':wx.EVT_TREE_SEL_CHANGED,
                    #'clk':wx.EVT_TREE_ITEM_LEFT_CLICK,
                    'mclk':wx.EVT_TREE_ITEM_MIDDLE_CLICK,
                    'rclk':wx.EVT_TREE_ITEM_RIGHT_CLICK,
                    'menu':wx.EVT_TREE_ITEM_MENU,
                    'dragBeg':wx.EVT_TREE_BEGIN_DRAG,
                    'dragBegRg':wx.EVT_TREE_BEGIN_RDRAG,
                    'dragEnd':wx.EVT_TREE_END_DRAG,
                    'key':wx.EVT_TREE_KEY_DOWN,
                    },
                'flag': wx.EXPAND,
                'mark': True,
                'mod':  ['fb'],
                }
        if cls=='trlst':return {
            'cls':  'trlst',
            'CLS':  vGuiTreeListCtrlWX,
            'key':  None,
            'kw':   {},
            'evtyp':{
                    #'sel':wx.EVT_TREE_ITEM_SELECTED,
                    #'desel':wx.EVT_TREE_ITEM_DESELECTED,
                    'dbClk':wx.EVT_TREE_ITEM_ACTIVATED,
                    'act':wx.EVT_TREE_ITEM_ACTIVATED,
                    'activate':wx.EVT_TREE_ITEM_ACTIVATED,
                    'del':wx.EVT_TREE_DELETE_ITEM,
                    'collapsing':wx.EVT_TREE_ITEM_COLLAPSING,
                    'collapsed':wx.EVT_TREE_ITEM_COLLAPSED,
                    'expanding':wx.EVT_TREE_ITEM_EXPANDING,
                    'expanded':wx.EVT_TREE_ITEM_EXPANDED,
                    'selChg':wx.EVT_TREE_SEL_CHANGING,
                    'sel':wx.EVT_TREE_SEL_CHANGED,
                    #'lclk':wx.EVT_TREE_ITEM_LEFT_CLICK,
                    'mclk':wx.EVT_TREE_ITEM_MIDDLE_CLICK,
                    'rclk':wx.EVT_TREE_ITEM_RIGHT_CLICK,
                    'menu':wx.EVT_TREE_ITEM_MENU,
                    'dragBeg':wx.EVT_TREE_BEGIN_DRAG,
                    'dragBegRg':wx.EVT_TREE_BEGIN_RDRAG,
                    'dragEnd':wx.EVT_TREE_END_DRAG,
                    
                    #'sel':wx.EVT_LIST_ITEM_SELECTED,
                    #'desel':wx.EVT_LIST_ITEM_DESELECTED,
                    #'dblClk':wx.EVT_LIST_ITEM_ACTIVATED,
                    #'activate':wx.EVT_LIST_ITEM_ACTIVATED,
                    'colClk':wx.EVT_LIST_COL_CLICK,
                    'colClkLf':wx.EVT_LIST_COL_CLICK,
                    'colClkRg':wx.EVT_LIST_COL_RIGHT_CLICK,
                    
                    'key':wx.EVT_TREE_KEY_DOWN,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['fb'],
            }
        if cls=='fileBrowser':return {
            'cls':  'fileBrowser',
            'CLS':  vGuiFileBrowser,
            'key':  None,
            'kw':   {
                    'bSave':False,'bMulti':False,'bPosixFN':True,
                    'sMsg':_(u'choose file'),
                    'sWildCard':_(u'xml files|*.xml|all files|*.*'),
                    'sDftFN':u'tmpl.xml','sFN':None,
                    'bmp':None,'size_button':(32,32),
                    'bAnchor':False,'iArrange':0,
                },
            'evtyp':{
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='dirBrowser':return {
            'cls':  'dirBrowser',
            'CLS':  vGuiDirBrowser,
            'key':  None,
            'kw':   {
                    'bNew':False,'bMulti':False,'bPosixFN':True,
                    'sMsg':_(u'choose file'),
                    #'sDftFN':u'tmpl.xml','sDN':None,
                    'bmp':None,'size_button':(32,32),
                    'bAnchor':False,'iArrange':0,
                    },
            'evtyp':{
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='txtDat':return {
            'cls':  'txtDat',
            'CLS':  vGuiTextDataWX,
            'key':  None,
            'kw':   {},
            'evtyp':{},
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='tg':return {
            'cls':  'tg',
            'CLS':  wx.ToggleButton,
            'key':  ['id','name','parent','pos','size','style',
                    'label'],
            'kw':   {},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['btn'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='tgLbl':return {
            'cls':  'tgLbl',
            'CLS':  wx.ToggleButton,
            'key':  ['id','name','parent','pos','size','style',
                    'label'],
            'kw':   {},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['btn'],
            'szMin':(80,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='tgBmp':return {
            'cls':  'tgBmp',
            'CLS':  wx.lib.buttons.GenBitmapToggleButton,
            'key':  ['id','name','parent','pos','size','style',
                    'bitmap',],
            'kw':   {},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            #'mark': True,
            'mod':  ['btn'],
            'fct':  {
                    None:['bitmapLbl','bitmapSel','bitmapDis','bitmapFcs',],
                    'bitmapLbl':'SetBitmapLabel',
                    'bitmapSel':'SetBitmapSelected',
                    'bitmapDis':'GetBitmapDisabled',
                    'bitmapFcs':'GetBitmapFocus',
                    },
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='tgBmpLbl':return {
            'cls':  'tgBmpLbl',
            'CLS':  wx.lib.buttons.GenBitmapTextToggleButton,
            'key':  ['id','name','parent','pos','size','style',
                    'bitmap','label'],
            'kw':   {},#{'bitmap':wx.EmptyButton(16,16)},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['btn'],
            'szMin':(80,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='pnc':return {
            'cls':  'pnc',
            'CLS':  vGuiCorePanWX,
            'key':  None,
            'kw':   {},
            'evtyp':{
                    #'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            }
        if cls=='bits':return {
            'cls':  'bits',
            'CLS':  vGuiBitsWX,
            'key':  None,
            'kw':   {},
            'evtyp':{
                    #'btn':wx.EVT_BUTTON,
                    },
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='tgPopup':return {
            'cls':  'tgPopup',
            'CLS':  vGuiPopupButtonWX,
            'key':  None,
            'kw':   {},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            #'mod':  ['btn'],
            }
        if cls=='tgEdtLst':return {
            'cls':  'tgEdtLst',
            'CLS':  vGuiPopupEditListWX,
            'key':  None,
            'kw':   {},
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'mark': False,
            #'mod':  ['btn'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='txtPopup':return {
            'cls':  'txtPopup',
            'CLS':  vGuiTextPopupWX,
            'key':  None,
            'kw':   {},
            'evtyp':{},#{'btn':wx.EVT_BUTTON},
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='txtStyled':return {
            'cls':  'txtStyled',
            'CLS':  vGuiTextStyledWX,
            'key':  None,
            'kw':   {},
            'evtyp':{},#{'btn':wx.EVT_BUTTON},
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='txtNum':return {
            'cls':  'txtNum',
            'CLS':  vGuiNumWX,
            'key':  None,
            'kw':   {
                    'numMin':-999,'numMax':999,'numDelta':1,
                    #'numFmt':'%d','numConv':int,
                    'numBase':'d','numDft':0,
                    'mode':'int','value':'value',
                    'size_button':'size_button',
                    },
            'evtyp':{},#{'btn':wx.EVT_BUTTON},
            #'evt':  {'txt':wx.EVT_TEXT,'ent':wx.EVT_TEXT_ENTER},
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['mod'],
            }
        if cls=='tr':return {
            'cls':  'tr',
            'CLS':  wx.TreeCtrl,
            'kw':   {
                    'style':wx.TR_HAS_BUTTONS,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['check'],
            }
        if cls=='txt':return {
            'cls':  'txt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':0,
                    },
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtSilent':return {
            'cls':  'txtSilent',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':0,
                    },
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtEnt':return {
            'cls':  'txtEnt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style': wx.TE_PROCESS_ENTER,
                    },
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtPwd':return {
            'cls':  'txtPwd',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_PASSWORD,
                    },
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtPwdEnt':return {
            'cls':  'txtPwdEnt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_PASSWORD | wx.TE_PROCESS_ENTER,
                    },
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtLn':return {
            'cls':  'txtLn',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_MULTILINE,
                    },
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            #'flagSz': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRd':return {
            'cls':  'txtRd',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY,
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': False,
            #'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRdWrp':return {
            'cls':  'txtRdWrp',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.TE_WORDWRAP,
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': False,
            #'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRdRg':return {
            'cls':  'txtRdRg',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': False,
            #'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRdInt':return {
            'cls':  'txtRdInt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,
                    'size':(60,-1),
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': False,
            #'mod':  ['txt'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='txtLck':return {
            'cls':  'txtLck',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY,
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtLckWrp':return {
            'cls':  'txtLckWrp',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.TE_WORDWRAP,
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtLckRg':return {
            'cls':  'txtLckRg',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,},
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtLckInt':return {
            'cls':  'txtLckInt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,
                    'size':(60,-1),
                    },# | wx.TE_PROCESS_ENTER},
            'evtyp':{
                    'txt':wx.EVT_TEXT,
                    'ent':wx.EVT_TEXT_ENTER,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['txt'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        if cls=='szFlex':return {
            'cls':  'szFlex',
            'CLS':  wx.FlexGridSizer,
            'key':  ['rows','cols','vgap','hgap'],
            'kw':   {
                    'cols':1,'rows':1,'hgap':4,'vgap':0,
                    'lGrowRow':None,'lGrowCol':None,
                    },
            'flag': wx.EXPAND,
            'mark': False,
            'sizer':True,
            }
        if cls=='szBoxHor':return {
            'cls':  'szBoxHor',
            'CLS':  wx.BoxSizer,
            'key':  ['orient'],
            'kw':   {
                    'orient':wx.HORIZONTAL,
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'mark': False,
            'sizer':True,
            }
        if cls=='szBoxVert':return {
            'cls':  'szBoxVert',
            'CLS':  wx.BoxSizer,
            'key':  ['orient'],
            'kw':   {
                    'orient':wx.VERTICAL,
                    },
            'flag': wx.EXPAND|wx.BOTTOM,
            'mark': False,
            'sizer':True,
            }
        if cls=='szBoxHorLf':return {
            'cls':  'szBoxHorLf',
            'CLS':  wx.BoxSizer,
            'key':  ['orient'],
            'kw':   {
                    'orient':wx.HORIZONTAL,
                    },
            'flag': wx.LEFT,
            'mark': False,
            'sizer':True,
            }
        if cls=='szBoxVertTp':return {
            'cls':  'szBoxVertTp',
            'CLS':  wx.BoxSizer,
            'key':  ['orient'],
            'kw':   {
                    'orient':wx.VERTICAL,
                    },
            'flag': wx.TOP,
            'mark': False,
            'sizer':True,
            }
        if cls=='spInt':return {
            'cls':  'spInt',
            'CLS':  wx.SpinCtrl,
            'key':  ['id','name','parent','pos','size','style',
                    'value','min','max','initial',],
            'kw':   {
                    'style':wx.SP_ARROW_KEYS,
                    'min':0,'max':100,'initial':0,
                    },
            'evtyp':{
                    'chg':wx.EVT_SPIN,
                    'up':wx.EVT_SPIN_UP,
                    'dn':wx.EVT_SPIN_DOWN,
                    },
            'flag': wx.EXPAND,
            'mark': True,
            'mod':  ['chg'],
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
        
    def __get_widget_map__Old(self):
        dEvtWid={'char':wx.EVT_CHAR,'char_hook':wx.EVT_CHAR_HOOK,
            'keyDn':wx.EVT_KEY_DOWN,'keyUp':wx.EVT_KEY_UP,
            'mouse':wx.EVT_MOUSE_EVENTS,'wheel':wx.EVT_MOUSEWHEEL,
            'rgDbl':wx.EVT_RIGHT_DCLICK,'mdDbl':wx.EVT_MIDDLE_DCLICK,
            'lfDbl':wx.EVT_LEFT_DCLICK,
            'motion':wx.EVT_MOTION,
            'rgDn':wx.EVT_RIGHT_DOWN,'rgUp':wx.EVT_RIGHT_UP,
            'mdUp':wx.EVT_MIDDLE_UP,'mdDn':wx.EVT_MIDDLE_DOWN,
            'lfUp':wx.EVT_LEFT_UP,'lfDn':wx.EVT_LEFT_DOWN,
            'enter':wx.EVT_ENTER_WINDOW,'leave':wx.EVT_LEAVE_WINDOW,
            'move':wx.EVT_MOVE,'size':wx.EVT_SIZE,
            'erase':wx.EVT_ERASE_BACKGROUND,'paint':wx.EVT_PAINT,
            'focusKill':wx.EVT_KILL_FOCUS,'focusSet':wx.EVT_SET_FOCUS,
            'help':wx.EVT_HELP,'colorChanged':wx.EVT_SYS_COLOUR_CHANGED
            }
        dEvtDlg={
            'idle':wx.EVT_IDLE,'navKey':wx.EVT_NAVIGATION_KEY,
            'iconize':wx.EVT_ICONIZE,'maximize':wx.EVT_MAXIMIZE,
            'dropFiles':wx.EVT_DROP_FILES,'close':wx.EVT_CLOSE,
            'activate':wx.EVT_ACTIVATE,'init':wx.EVT_INIT_DIALOG,
            }
        
        return dEvtWid,{}
    def AddWidget(self,sK,pos,span,kwargs,evt,tSz=None,tDef=None,bDbg=False):
        try:
            #if tDef is None:
            #    dEvtWid,dMap=self.__get_widget_map__()
            #else:
            #    dEvtWid,dMap=tDef
            if sK is None:
                if bDbg==True:
                    self.__logDebug__(['sK',sK,'pos',pos,'span',span,
                            'kwargs',kwargs,'tSz',tSz,'flexsizer',self.FLEX_SIZER])
                if self.FLEX_SIZER==0:
                    if type(pos)==types.TupleType:
                        if tSz is None:
                            if type(span)==types.TupleType:
                                self.gbsData.AddSpacer(span,pos, border=0, flag=0,span=(1,1))
                            else:
                                self.gbsData.AddSpacer((span,span),pos, border=0, flag=0,span=(1,1))
                        else:
                            oSz,iFlag=tSz
                            oSz.AddSpacer(pos, border=span, flag=0)
                    else:   # 20150210 wro: added
                        if tSz is None:
                            pass
                        else:
                            oSz,iFlag=tSz
                            oSz.AddSpacer((pos,span), border=0, flag=0)
                            
                elif self.FLEX_SIZER==2:
                    # 20141127 wro: actually vertical box sizer
                    if type(pos)==types.TupleType:
                        self.__logCritical__({'sK':sK,'pos':pos,'span':span,
                                'kwargs':kwargs,'tSz':tSz,
                                'flexsizer':self.FLEX_SIZER,})
                        #if tSz is None:
                        #    self.gbsData.AddSpacer(pos, border=span, flag=0)
                        #else:
                        #    oSz,iFlag=tSz
                        #    oSz.AddSpacer(pos, border=span, flag=0)
                    else:
                        if tSz is None:
                            self.gbsData.AddSpacer((pos,span))
                        else:
                            self.__logCritical__({'sK':sK,'pos':pos,'span':span,
                                    'kwargs':kwargs,'tSz':tSz,
                                    'flexsizer':self.FLEX_SIZER,})
                            oSz,iFlag=tSz
                            #oSz.AddSpacer(pos, border=span, flag=0)
                elif self.FLEX_SIZER==3:
                    # 20141127 wro: actually horizontal box sizer
                    if type(pos)==types.TupleType:
                        self.__logCritical__({'sK':sK,'pos':pos,'span':span,
                                'kwargs':kwargs,'tSz':tSz,
                                'flexsizer':self.FLEX_SIZER,})
                    else:
                        if tSz is None:
                            self.gbsData.AddSpacer((pos,span))
                        else:
                            self.__logCritical__({'sK':sK,'pos':pos,'span':span,
                                    'kwargs':kwargs,'tSz':tSz,
                                    'flexsizer':self.FLEX_SIZER,})
                            oSz,iFlag=tSz
                else:
                    if type(pos)==types.TupleType:
                        if tSz is None:
                            self.gbsData.AddSpacer(pos, border=span, flag=0)
                        else:
                            oSz,iFlag=tSz
                            oSz.AddSpacer(pos, border=span, flag=0)
                return
            
            t=type(sK)
            if t in [types.ClassType,types.TypeType]:
                cls=sK
                kw={
                    }
                dW={'flag': wx.EXPAND,
                    'mark': True,
                    }
            else:
                dW=self.__getClsDftArgs__(sK)
                if dW is None:
                    cls=None
                else:
                    cls=dW['CLS']
                    kw=dW['kw']
            # 20150222 wro:automatic name generation added
            bNameReq=False
            if 'imgLst' in dW:
                bNameReq=True
            if bNameReq==True:
                if 'name' not in kwargs:
                    self.__logErrorAdd__('name unknown',{
                        'pos':pos,'span':span,'kwargs':kwargs,'sK':sK,
                        })
                    return
            if bDbg==True:
                self.__logDebug__(['sK',sK,'pos',pos,'span',span,
                        'kwargs',kwargs,'t',t,'dW',dW,'CLS',cls])
            if cls is not None:
                if 'sizer' in dW:
                    kw.update(kwargs)
                    kkw=kw.copy()
                    if 'name' in kkw:
                        del kkw['name']
                    lGrowCol=None
                    if 'lGrowCol' in kkw:
                        lGrowCol=kkw['lGrowCol']
                        del kkw['lGrowCol']
                    lGrowRow=None
                    if 'lGrowRow' in kkw:
                        lGrowRow=kkw['lGrowRow']
                        del kkw['lGrowRow']
                    if bDbg==True:
                        self.__logDebug__(['kkw',kkw])
                    w=cls(**kkw)
                    if 'name' in kwargs:
                        setattr(self.pn,kwargs['name'],w)
                    if lGrowCol is not None:
                        for i in lGrowCol:
                            if type(i)==types.TupleType:
                                w.AddGrowableCol(i[0],i[1])
                            else:
                                w.AddGrowableCol(i,1)
                    if lGrowRow is not None:
                        for i in lGrowRow:
                            if type(i)==types.TupleType:
                                w.AddGrowableRow(i[0],i[1])
                            else:
                                w.AddGrowableRow(i,1)
                    if tSz is None:
                        if self.FLEX_SIZER==0:
                            self.gbsData.AddSizer(w, pos,span, flag=dW.get('flag',wx.ALIGN_CENTRE))
                        else:
                            self.gbsData.AddSizer(w, pos,border=span, flag=dW.get('flag',wx.ALIGN_CENTRE))
                        #self.gbsData.Add(w, 
                        #    wx.GBPosition(pos[0],pos[1]),wx.GBSpan(span[0],span[1]),
                        #    flag=dW.get('flag',wx.ALIGN_CENTRE))
                    else:
                        oSz,iFlag=tSz
                        oSz.AddSizer(w,pos,border=span, flag=dW.get('flag',wx.ALIGN_CENTRE)|iFlag)
                    for it in evt:
                        self.AddWidget(it[0],it[1],it[2],it[3],it[4],
                                tSz=(w,dW.get('flag',wx.ALIGN_CENTRE)),
                                tDef=None,bDbg=bDbg)
                    return w
                elif 'imgLst' in dW:
                    kw.update(kwargs)
                    kkw=kw.copy()
                    sName=kkw['name']
                    del kkw['name']
                    w,bImgLstCreated=vGuiImgLstGet(sName,**kkw)
                    return None
                lKey=dW.get('key',None)
                if lKey is None:        # 20150120 wro:pass all keyword args
                    kw.update(kwargs)
                    kw['parent']=self.pn
                    if 'tip' in kw:
                        sTip=kw['tip']
                        del kw['tip']
                    else:
                        sTip=None
                    if 'mark' in kw:
                        sMark=kw['mark']
                        del kw['mark']
                    else:
                        sMark=None
                    if 'mod' in kw:
                        sMod=kw['mod']
                        del kw['mod']
                    else:
                        sMod=None
                    if 'guiStore' in kw:
                        sGuiStore=kw['guiStore']
                        del kw['guiStore']
                    else:
                        sGuiStore=kw.get('name',None)
                    if 'szMin' in kw:           # 20160223 wro:min size by kwargs
                        tSzMin=kw['szMin']
                        del kw['szMin']
                    else:
                        if 'size' in kw:
                            tSzMin=kw['size']
                        else:
                            if 'szMin' in dW:
                                tSzMin=dW['szMin']
                            else:
                                tSzMin=None
                    if bDbg==True:
                        self.__logDebug__(['sK',sK,'kw',kw,'CLS',cls])
                    w=cls(**kw)
                    #self.__dict__[sGuiStore]=w
                else:                   # 20150120 wro: filter keywaord args
                    kw.update(kwargs)
                    if lKey is None:
                        lKey=['id','name','pos','size','style','parent']
                        self.__logDebug__({'sK':sK,'dW':dW})
                    sNameWid='widDat%02d'%(self.iOfsWid)
                    self.iOfsWid+=1
                    _args,_kwargs=self.GetGuiArgs(kw,lKey,{
                            #'pos':(0,0),'size':(-1,-1),
                            'style':0,
                            'name':sNameWid,
                            'parent':self.pn,
                            },par=self.pn)
                    for sK in ['bitmap']:
                        if sK in _kwargs:
                            sN=_kwargs[sK]
                            _kwargs[sK]=self.GetBmp(sN)
                    if bDbg==True:
                        self.__logDebug__(['kw',kw,'_kwargs',_kwargs,])
                    #w=cls(**kw)
                    w=cls(**_kwargs)
                    if bDbg==True:
                        self.__logDebug__(['kw',kw,'_kwargs',_kwargs,
                                'created'])
                    
                    if 'tip' in kw:
                        sTip=kw['tip']
                        del kw['tip']
                    else:
                        sTip=None
                    if 'mark' in kw:
                        sMark=kw['mark']
                        del kw['mark']
                    else:
                        sMark=None
                    if 'mod' in kw:
                        sMod=kw['mod']
                        del kw['mod']
                    else:
                        sMod=None
                    if 'guiStore' in kw:
                        sGuiStore=kw['guiStore']
                        del kw['guiStore']
                    else:
                        sGuiStore=None
                    if 'szMin' in kw:           # 20160223 wro:min size by kwargs
                        tSzMin=kw['szMin']
                        del kw['szMin']
                    else:
                        if 'size' in kw:
                            tSzMin=kw['size']
                        else:
                            if 'szMin' in dW:
                                tSzMin=dW['szMin']
                            else:
                                tSzMin=None
                            
                    
                #kw['parent']=self.pn
                if sGuiStore is not None:       # 121024:wro store object
                    self.__dict__[sGuiStore]=w
                if sTip is not None:
                    if hasattr(w,'SetToolTipString'):
                        w.SetToolTipString(sTip)
                        setattr(w,'_tip',sTip)
                sN=kwargs.get('name',None)      # 20150222 wro:check name set
                if sN is not None:
                    setattr(self.pn,kwargs['name'],w)
                bDbgWid=False
                if hasattr(w,'GetWid'):
                    wid=w.GetWid()
                    bDbgWid=w.GetVerboseDbg(20)
                    if wid is None:     # 20150109 wro:is this really needed?
                        self.__logWarnAdd__('what is going on here?',
                                {'tSz':tSz,'pos':pos,'span':span,
                                'w':w,'wid':wid,
                                'FLEX_SIZER':self.FLEX_SIZER})
                        wid=w
                else:
                    wid=w
                if bDbg:
                    self.__logDebug__({'tSz':tSz,'pos':pos,'span':span,
                            'w':w,'wid':wid,'name':wid.GetName(),
                            'FLEX_SIZER':self.FLEX_SIZER})
                if tSz is None:
                    # 20141127 wro: flag replaced with flagSz (flag for sizer)
                    if self.FLEX_SIZER==0:
                        self.gbsData.AddWindow(wid, pos,span, 
                                flag=dW.get('flag',wx.EXPAND))    #flagSz
                    elif self.FLEX_SIZER==2:
                        self.gbsData.AddWindow(wid, pos,border=span, 
                                flag=dW.get('flag',wx.EXPAND|wx.BOTTOM))    #flagSz
                    elif self.FLEX_SIZER==3:
                        self.gbsData.AddWindow(wid, pos,border=span, 
                                flag=dW.get('flag',wx.EXPAND|wx.RIGHT))    #flagSz
                    else:
                        self.gbsData.AddWindow(wid, pos,border=span, 
                                flag=dW.get('flag',wx.ALIGN_CENTRE))    #flagSz
                    #self.gbsData.Add(w, 
                    #        wx.GBPosition(pos[0],pos[1]),wx.GBSpan(span[0],span[1]),
                    #        flag=dW.get('flag',wx.ALIGN_CENTRE))
                else:
                    oSz,iFlag=tSz
                    oSz.AddWindow(wid,pos,border=span, 
                            flag=dW.get('flagSz',wx.ALIGN_CENTRE)|iFlag)
                if sK=='cbAdd':
                    wid.Bind(wx.EVT_BUTTON,self.OnButtonAdd)#,id=iId)
                if evt is not None:
                    #iId=_kwargs.get('id',wx.ID_ANY)
                    if 'evtyp' in dW:
                        dEvt=dW['evtyp']
                    else:
                        dEvt={}
                    if None in evt:
                        wPost=evt[None]
                    else:
                        wPost=wid
                    for e,f in evt.iteritems():
                        if e is None:
                            continue
                        t=type(e)
                        if bDbg or bDbgWid:
                            self.__logDebug__({'bind':'evt','t':t,'f':f,
                                    'w':w,'wid':wid,'wPost':wPost,
                                    'evt':e,})
                        if self.GetVerboseDbg(20):
                            self.__logDebug__({'dEvt':dEvt})
                        if t==types.ClassType:
                            wPost.Bind(e,f)#,id=iId)
                        elif e in dEvt:
                            wPost.Bind(dEvt[e],f)#,id=iId)
                        #elif e in dEvtWid:
                        #    wPost.Bind(dEvtWid[e],f,id=iId)
                        elif t==types.StringType:
                            #self.BindEventWid(w,e,f)
                            if w.BindEvent(e,f)==-2:
                                self.BindEventWid(w,e,f)
                if self.bEnMark:
                    #self.__logDebug__(dW)
                    #self.__logDebug__(self.lWid)
                    if dW.get('mark',False)==True:
                        if sMark in [None,True,1]:
                            #self.__logDebug__('sK:%s;sMark:%r'%(sK,sMark))
                            #vpc.CallStack(level2skip=0)
                            #print sK,sMark
                            if hasattr(w,'__SetWidMod__'):
                                self.lWid.append(w)
                            elif hasattr(w,'GetWidMod'):
                                wMod=w.GetWidMod()
                                #self.__logDebug__('sK:%s;sMark:%r;wMod:%r'%(sK,sMark,wMod))
                                #print wMod
                                if wMod is not None:
                                    if type(wMod)==types.ListType:
                                        for wTmp in wMod:
                                            self.lWid.append(wTmp)
                                    else:
                                        self.lWid.append(wMod)
                            else:
                                self.lWid.append(wid)
                                if wid is None:
                                    self.__logError__(['sK',sK,
                                            'pos',pos,'span',span,
                                            'kwargs',kwargs,'t',t,
                                            'dW',dW,'CLS',cls])
                    self.__logDebug__({'lWid':self.lWid})
                if self.bEnMod:
                    l=dW.get('mod',None)
                    #vpc.CallStack()
                    #print sK,sMod
                    #print l
                    #print dW
                    if (l is not None) and ('evtyp' in dW):
                        if sMod in [None,True,1]:
                            dEvt=dW['evtyp']
                            #print dEvt
                            for e in l:
                                t=type(e)
                                #print e,t
                                if bDbg or bDbgWid:
                                    self.__logDebug__({'bind':'mod',
                                            't':t,'w':w,'wid':wid,
                                            'evt':e,})
                                if self.GetVerboseDbg(20):
                                    self.__logDebug__({'dEvt':dEvt})
                                if t==types.ClassType:
                                    if bDbg or bDbgWid:
                                        self.__logDebug__({'bind':'cls',
                                                'evt':e})
                                    wid.Bind(dEvt[e],self.OnChange)#,id=iId)
                                elif e in dEvt:
                                    if bDbg or bDbgWid:
                                        self.__logDebug__({'bind':'dict',
                                                'evt':e,'dEvt[e]':dEvt[e]})
                                    wid.Bind(dEvt[e],self.OnChange)#,id=iId)
                                elif t==types.StringType:
                                    if bDbg or bDbgWid:
                                        self.__logDebug__({'bind':'sys',
                                                'evt':e})
                                    w.BindEvent(e,self.OnChange)
                if None in self.lWid:
                    self.__logError__(['sK',sK,'pos',pos,'span',span,
                        'kwargs',kwargs,'t',t,'dW',dW,'CLS',cls,'w',w])
                if 'fct' in dW:     # 20150727 wro:use cool feature for vGuiCoreWidWX
                    kkw=kwargs
                    dFct=dW.get('fct')
                    for sK in dFct[None]:
                        if sK in kkw:
                            try:
                                if bDbg:
                                    self.__logDebug__('sK:%s'%(sK))
                                f=getattr(w,dFct[sK])
                                if sK.startswith('bitmap'):
                                    a=self.GetBmp(kkw[sK])
                                    f(a)
                                else:
                                    v=kkw[sK]
                                    t=type(v)
                                    if t==types.TupleType:
                                        f(*v)
                                    elif t==types.ListType:
                                        f(*v[0],**v[1])
                                    else:
                                        f(v)
                            except:
                                self.__logTB__()
                try:
                    #if 'szMin' in dW:
                    #    w.SetMinSize(dW.get('szMin',(-1,-1)))
                    if tSzMin is not None:
                        w.SetMinSize(tSzMin)
                except:
                    self.__logTB__()
                return w
            else:
                self.__logError__('widget:%s unknown;pos:%s;span:%s;kwargs:%s'%(sK,
                    `pos`,`span`,`kwargs`))
        except:
            self.__logTB__()
            try:
                self.__logError__('widget:%s;pos:%s;span:%s;kwargs:%s;kw:%s'%(sK,
                        `pos`,`span`,`kwargs`,`kw`))
            except:
                pass
        return None
    def __OnAddWidSizer__(self,bxSizer=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            bDbg=True
            if bDbg:
                self.__logDebug__('')
            if bxSizer is None:
                bxSizer=self.pn.GetSizer()
            
            bxSizer.Layout()
            #bxSizer.Fit(self.pn)
        except:
            self.__logTB__()
    def DoLayout(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread(bSilent=True):
                self.__OnAddWidSizer__()
            else:
                self.CB(self.__OnAddWidSizer__)
        except:
            self.__logTB__()
    def AddWid(self,wid,**kw):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            bxSizer=self.pn.GetSizer()
            if hasattr(wid,'GetWid'):
                w=wid.GetWid()
            else:
                w=wid
            bxSizer.Add(w,flag=wx.EXPAND|wx.ALL,
                    proportion=kw.get('iProportion',1),
                    border=kw.get('iBorder',4))
            if self._iSizerLayout<0:
                self._iSizerLayout=1
                self.CB(self.__OnAddWidSizer__,bxSizer)
            else:
                self._iSizerLayout+=self._iSizerLayout
        except:
            self.__logTB__()
    def OnButtonAdd(self,evt):
        try:
            self.Post('add',evt.GetEventObject().GetName())
        except:
            self.__logTB__()
    def __add_widget__(self,lWid):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebugAdd__('lWid',lWid)
            #dEvtWid,dMap=self.__get_widget_map__()
            self.iOfsWid=0
            for it in lWid:
                t=type(it)
                if t==types.TupleType:
                    if bDbg:
                        self.__logDebug__(it)
                    self.AddWidget(it[0],it[1],it[2],it[3],it[4],
                            #tDef=(dEvtWid,dMap),
                            tDef=None,
                            bDbg=bDbg)
        except:
            self.__logTB__()
    def SetEnableMark(self,flag):
        self.__logDebug__('flag:%d'%(flag))
        self.bEnMark=flag
    def SetEnableModification(self,flag):
        self.__logDebug__('flag:%d'%(flag))
        self.bEnMod=flag
    def OnChange(self,evt):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__('bBlk:%d'%(self.bBlk))
            evt.Skip()
            obj=None
            if hasattr(evt,'GetEventObj'):
                obj=evt.GetEventObj()
                if bDbg:
                    self.__logDebugAdd__('GetEventObj',{'obj':obj})
            if hasattr(evt,'GetEventObject'):
                obj=evt.GetEventObject()
                if bDbg:
                    self.__logDebugAdd__('GetEventObject',{'obj':obj})
            if hasattr(evt,'GetGUI'):
                obj=evt.GetGUI()
                if bDbg:
                    self.__logDebugAdd__('GetGUI',{'obj':obj})
            
            if obj is None:
                if bDbg:
                    self.__logDebug__('no obj'%())
                return
            if self.bBlk>0:
                return
        except:
            self.__logTB__()
        try:
            if bDbg:
                if hasattr(obj,'GetName'):
                    self.__logDebug__('obj:%s(%d)'%(obj.GetName(),obj.GetId()))
                else:
                    obj.__logDebug__('')
        except:
            self.__logTB__()
        self.SetModified(True,obj)
    def IsBlocked(self):
        return self.bBlk>0
    def GetValue(self,sName=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            bDbgWid=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'sName':sName,
                        'widVal':self.widVal,'lWid':self.lWid})
            if sName is not None:
                w=getattr(self.pn,sName,None)
                if w is not None:
                    return w.GetValue()
                return None
            if self.widVal is not None:
                w=getattr(self.pn,self.widVal,None)
                if w is not None:
                    return w.GetValue()
                return None
            dVal={}
            for w in self.lWid:
                if bDbgWid:
                    if hasattr(w,'__logDebug__'):
                        w.__logDebug__('')
                try:
                    if hasattr(w,'GetValue'):
                        k=w.GetName()
                        v=w.GetValue()
                        if k in dVal:
                            self.__logCriticalAdd__('name:%s already present'%(k),dVal)
                        else:
                            dVal[k]=v
                except:
                    self.__logTB__()
            if bDbg:
                self.__logDebugAdd__('values',dVal)
            return dVal
        except:
            self.__logTB__()
        return None
    def SetValue(self,sName,*args,**kwargs):
        try:
            self.SetBlock()
            self.ClrBlockDelayed()
            self.SetModified(False,None)
            if sName is not None:
                w=getattr(self.pn,sName,None)
                if w is not None:
                    w.SetValue(*args,**kwargs)
                else:
                    self.__logError__('widget with name:%s not found'%(sName))
            if self.widVal is not None:
                w=getattr(self.pn,self.widVal,None)
                if w is not None:
                    w.SetValue(*args,**kwargs)
                else:
                    self.__logError__('widget with name:%s not found'%(sName))
            
        except:
            self.__logTB__()
    def SetValueDict(self,dVal):
        try:
            self.SetBlock()
            self.ClrBlockDelayed()
            self.SetModified(False,None)
            lK=dVal.keys()
            for sName in lK:
                try:
                    w=getattr(self.pn,sName,None)
                    if w is not None:
                        w.SetValue(dVal[sName])
                    else:
                        self.__logError__('widget with name:%s not found'%(sName))
                except:
                    self.__logTB__()
        except:
            self.__logTB__()
    def GetValueDict(self,sName=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            bDbgWid=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'sName':sName,
                        'widVal':self.widVal,'lWid':self.lWid})
            if sName is not None:
                w=getattr(self.pn,sName,None)
                if w is not None:
                    return w.GetValueDict()
                return None
            if self.widVal is not None:
                w=getattr(self.pn,self.widVal,None)
                if w is not None:
                    return w.GetValueDict()
                return None
            dVal={}
            for w in self.lWid:
                if bDbgWid:
                    if hasattr(w,'__logDebug__'):
                        w.__logDebug__('')
                try:
                    if hasattr(w,'GetValueDict'):
                        k=w.GetName()
                        v=w.GetValueDict()
                        if k in dVal:
                            self.__logCriticalAdd__('name:%s already present'%(k),dVal)
                        else:
                            dVal[k]=v
                except:
                    self.__logTB__()
            if bDbg:
                self.__logDebugAdd__('values',dVal)
            return dVal
        except:
            self.__logTB__()
        return None
    def SetBlock(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        self.bBlk=True
        #self.bBlock+=1
    def ClrBlockDelayed(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'',self)
        wx.CallAfter(self.ClrBlock)
    def ClrBlock(self):
        #vtLog.vtLngCurWX(vtLog.DEBUG,'bBlock:%d'%(self.bBlock),self)
        self.bBlk=False
        #if self.bBlock>0:
        #    self.bBlock-=1
    def _getWid4ModMark(self,obj):
        if hasattr(obj,'GetWidMod'):
            w=obj.GetWidMod()
        else:
            if hasattr(obj,'GetWid'):
                w=obj.GetWid()
            else:
                w=obj
        return w
    def SetModified(self,state,obj=None):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'state':state})
            
            #if vtLog.vtLngIsLogged(vtLog.DEBUG):
            #    vtLog.vtLngCurWX(vtLog.DEBUG,'bModified:%d;state:%d'%
            #                (self.bModified,state),self)
            self.IsMainThread()
            if self.bMod!=state:
                if self.__isLogDebug__():
                    self.__logDebug__('bMod:%d;state:%d;obj is None:%d'%(self.bMod,
                            state,obj is None))
            if self.bEnMod:
                self.bMod=state
            if self.bEnMark==False:
                return
            if type(obj)==types.ListType:
                for o in obj:
                    self.SetModified(state,o)
                return
            if state:
                if obj is None:
                    for obj in self.lWid:
                        try:
                            if hasattr(obj,'__SetWidMod__'):
                                if bDbg:
                                    self.__logDebug__({'state':state,
                                            'obj':obj,'method':'found'})
                                obj.__SetWidMod__(state)
                            else:
                                if bDbg:
                                    self.__logDebug__({'state':state,
                                            'obj':obj,'method':'nop'})
                                w=self._getWid4ModMark(obj)
                                f=w.GetFont()
                                f.SetWeight(wx.FONTWEIGHT_BOLD)
                                w.SetFont(f)
                                w.Refresh()
                        except:
                            self.__logTB__()
                            self.__logErrorAdd__('obj name',obj)
                else:
                    if hasattr(obj,'__SetWidMod__'):
                        if bDbg:
                            self.__logDebug__({'state':state,
                                    'obj':obj,'method':'found'})
                        obj.__SetWidMod__(state)
                    else:
                        if bDbg:
                            self.__logDebug__({'state':state,
                                    'obj':obj,'method':'nop'})
                        w=self._getWid4ModMark(obj)
                        f=w.GetFont()
                        f.SetWeight(wx.FONTWEIGHT_BOLD)
                        w.SetFont(f)
                        w.Refresh()
            else:
                if obj is None:
                    for obj in self.lWid:
                        try:
                            if hasattr(obj,'__SetWidMod__'):
                                if bDbg:
                                    self.__logDebug__({'state':state,
                                            'obj':obj,'method':'found'})
                                obj.__SetWidMod__(state)
                            else:
                                if bDbg:
                                    self.__logDebug__({'state':state,
                                            'obj':obj,'method':'nop'})
                                w=self._getWid4ModMark(obj)
                                if w is None:
                                    self.__logErrorAdd__('lWid',self.lWid)
                                else:
                                    f=w.GetFont()
                                    f.SetWeight(wx.FONTWEIGHT_NORMAL)
                                    w.SetFont(f)
                                    w.Refresh()
                        except:
                            self.__logTB__()
                            self.__logErrorAdd__('obj name',obj)
                else:
                    if hasattr(obj,'__SetWidMod__'):
                        if bDbg:
                            self.__logDebug__({'state':state,
                                    'obj':obj,'method':'found'})
                        obj.__SetWidMod__(state)
                    else:
                        if bDbg:
                            self.__logDebug__({'state':state,
                                    'obj':obj,'method':'nop'})
                        w=self._getWid4ModMark(obj)
                        f=w.GetFont()
                        f.SetWeight(wx.FONTWEIGHT_NORMAL)
                        w.SetFont(f)
                        w.Refresh()
        except:
            self.__logTB__()
    def GetModified(self):
        if self.__isLogDebug__():
            self.__logDebug__('bModified:%d'%(self.bMod))
        return self.bMod
    def GetWidgetByIdx(self,iIdx):
        try:
            return self.lWid[iIdx]
        except:
            pass#self.__logTB__()
        return None
    def GetMainWidget(self):
        return self.GetWidgetByIdx(0)
    def ShowMsgDlg(self,iStyle,sMsg,func,*args,**kwargs):
        try:
            iRet=-1
            dlg=None
            style=0
            iStyleChk=iStyle&0xff
            if iStyleChk==self.YESNO:
                style|=wx.YES_NO|wx.NO_DEFAULT
            elif iStyleChk==self.OK:
                style=wx.OK|wx.NO_DEFAULT
            else:
                style=wx.OK
            iStyleChk=iStyle&0xff00
            if (iStyle==0xff00)==self.EXCLAMATION:
                style|=wx.ICON_EXCLAMATION
            elif (iStyle==0xff00)==self.ERROR:
                style|=wx.ICON_ERROR
            elif (iStyle==0xff00)==self.INFORMATION:
                style|=wx.ICON_INFORMATION
            elif (iStyle==0xff00)==self.QUESTION:
                style|=wx.ICON_QUESTION
            else:
                style|=wx.ICON_EXCLAMATION
            sTitle='vtgPanel'
            if hasattr(self,'applName'):
                sTitle=self.applName
            #dlg=vtmMsgDialog(self.pn,sMsg,sTitle,style)
            dlg=vGuiDlgMsg(self.pn,sMsg,sTitle,iStyle)
            if dlg.ShowModal()==wx.ID_YES:
                if func is not None:
                    func(1,*args,**kwargs)
                iRet=1
            else:
                if func is not None:
                    func(0,*args,**kwargs)
                iRet=0
        except:
            self.__logTB__()
        if dlg is not None:
            dlg.Destroy()
        return iRet
    def ChooseFile(self,sFN='',sMsg=None,sWildCard=None,sDftFN=None,
                bSave=False,bMulti=False,bPosixFN=True):
        try:
            self.__logInfo__(''%())
            if bSave==True:
                iStyle=wx.SAVE
            else:
                iStyle=wx.OPEN
            if bMulti==True:
                iStyle|=wx.MULTIPLE
            dlg=wx.FileDialog(self.GetWid(),message=sMsg or _(u'choose file'),
                defaultDir='.',defaultFile=sDftFN or 'empty.xml',
                wildcard=sWildCard or _(u'xml files|*.xml|all files|*.*'),
                style=iStyle)
            dlg.Centre()
            try:
                if len(sFN)>0:
                    sTmpDN,sTmpFN=os.path.split(sFN)
                    dlg.SetDirectory(sTmpDN)
                    dlg.SetFilename(sTmpFN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sTmpDN=dlg.GetDirectory()
                if bPosixFN==True:
                    sTmpDN=sTmpDN.replace('\\','/')
                if bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    dlg.Destroy()
                    return lFN
                else:
                    sTmpFN=dlg.GetFilename()
                    if bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    dlg.Destroy()
                    return sFN
            dlg.Destroy()
        except:
            self.__logTB__()
        return None
    def ChooseFileStyle(self,iStyle,sMsg,sWildCard,sFN,sDftFN,func,*args,**kwargs):
        try:
            dlg=None
            self.__logInfo__(''%())
            iStyleChk=iStyle&0xff
            style=wx.CHANGE_DIR#wx.OPEN
            bMulti=False
            if (iStyleChk&self.SAVE)==self.SAVE:
                style|=wx.SAVE
            elif (iStyleChk&self.OPEN)==self.OPEN:
                style|=wx.OPEN
            else:
                style|=wx.OPEN
            if (iStyleChk&self.MULTIPLE)==self.MULTIPLE:
                style|=wx.MULTIPLE
                bMulit=True
            bPosixFN=True
            dlg=wx.FileDialog(self.GetWid(),message=sMsg,
                defaultDir='.',defaultFile=sDftFN or '',
                wildcard=sWildCard,
                style=style)
            dlg.Centre()
            try:
                if sFN is not None:
                    if len(sFN)>0:
                        sTmpDN,sTmpFN=os.path.split(sFN)
                        dlg.SetDirectory(sTmpDN)
                        dlg.SetFilename(sTmpFN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sTmpDN=dlg.GetDirectory()
                sTmpDN=sTmpDN.replace('\\','/')
                if bPosixFN==True:
                    sTmpDN=sTmpDN.replace('\\','/')
                if bMulti==True:
                    lTmpFN=dlg.GetFilenames()
                    if bPosixFN==True:
                        lFN=['/'.join([sTmpDN,sTmpFN]) for sTmpFN in lTmpFN]
                    else:
                        lFN=[os.path.join(sTmpDN,sTmpFN) for sTmpFN in lTmpFN]
                    lFN.sort()
                    iCnt=len(lFN)
                    sFN=lFN[0]
                    self.Post('files',lFN)
                    if func is not None:
                        iOfs=0
                        for sFN in lFN:
                            func(iOfs,iCnt,sFN,*args,**kwargs)
                            iOfs=iOfs+1
                else:
                    sTmpFN=dlg.GetFilename()
                    if bPosixFN==True:
                        sFN='/'.join([sTmpDN,sTmpFN])
                    else:
                        sFN=os.path.join(sTmpDN,sTmpFN)
                    if func is not None:
                        func(0,1,sFN,*args,**kwargs)
        except:
            self.__logTB__()
        if dlg is not None:
            dlg.Destroy()
    def ShutDown(self):
        try:
            self.__logDebug__('')
        except:
            self.__logTB__()
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iWidth=event.GetDragRect().width
            #self.dCfg['nav_sash']=str(iWidth)
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self.pn, self.pnDataWX)
            self.pnDataWX.Refresh()
            #self.GetCfgData()
            #event.Skip()
        except:
            self.__logTB__()
    def OnSlwTopSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            iHeight=event.GetDragRect().height
            #self.dCfg['nav_top']=str(iHeight)
            
            self.slwTop.SetDefaultSize((1000, iHeight))
            wx.LayoutAlgorithm().LayoutWindow(self.pn, self.pnDataWX)
            self.pnDataWX.Refresh()
            #self.GetCfgData()
        except:
            self.__logTB__()
    def OnSize(self, event):
        if event is not None:
            event.Skip()
        try:
            self.__logDebug__('size:%r'%(event.GetSize()))
            if self.iLayout>=0:
                bMod=False
                wx.LayoutAlgorithm().LayoutWindow(self.pn, self.pnDataWX)
                self.pnDataWX.Refresh()
        except:
            self.__logTB__()
