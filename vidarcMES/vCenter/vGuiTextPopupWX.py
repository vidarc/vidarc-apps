#----------------------------------------------------------------------------
# Name:         vGuiTextPopupWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20120516
# CVS-ID:       $Id: vGuiTextPopupWX.py,v 1.22 2015/08/03 07:22:57 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiCoreWidWX import vGuiCoreWidWX
    #from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiTextPopupWX(vGuiCoreWidWX):
    SIZE_DFT=(76,24)
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=-1,mode='txt'|'txtRd'....,
                kwVal={'pos'...}
                ):
    """
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            vGuiCoreWidWX.__initObj__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.widVal=None
            self.txtVal=None
        except:
            self.__logTB__()
    def __get_widget_lst__(self,*args,**kwargs):
        lW=[]
        sWid=kwargs.get('mode','txt')
        iProp=kwargs.get('iProp',1)
        kkw=kwargs.get('kwVal',{'name':'txtVal'})
        dKw={'name':'txtVal','value':''}
        dKw.update(kkw)
        if sWid=='txt':
            dKw['evt']={
                'txt':              self.OnTextText,
                'keyDn':            self.OnKeyPressed,
                }
        lW.append((sWid,   iProp,    0, dKw))
        return lW
    def __initProperties__Old(self,*args,**kwargs):
        try:
            vGuiCoreWidWX.__initProperties__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
    def __initEvt__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            dEvt=kwargs.get('evt',None)
            if dEvt is None:
                return
            self.BindEventDict(self.txtVal,dEvt)
        except:
            self.__logTB__()
    def SetValueOld(self,sVal):
        try:
            self.txtVal.SetValue(sVal)
        except:
            self.__logTB__()
    def GetValueOld(self):
        try:
            sVal=self.txtVal.GetValue()
            return sVal
        except:
            self.__logTB__()
        return ''
    def OnTextText(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return 
            if evt is not None:
                evt.Skip()
            #if self.bBlock:
            #    return
            #self.__markModified__()
            sVal=self.txtVal.GetValue()
            self.PostFB('txt',sVal)
            #wx.PostEvent(self,vtInputTextChanged(self,sVal))
        except:
            self.__logTB__()
    def _doPostKey(self,w,key):
        try:
            lVal=self.GetValue()
            self.PostNFY('key',key)
            self.PostMOD('val',lVal)
        except:
            self.__logTB__()
    def OnKeyPressed(self,evt):
        try:
            evt.Skip()
            bDbg=self.GetVerboseDbg(10)
            if self.IsMainThread()==False:
                return #-1
            #if self.bBlock:
            #    return
            #self.__markModified__()
            key=self.GetKeyCode(evt)
            dKeyMod=self.GetKeyModifiers(evt)
            if bDbg:
                self.__logDebugAdd__('key:0x%03x %d'%(key,key),dKeyMod)
            bOk=False
            if key>=40 and key<300:
                bOk=True
            elif key==8:
                bOk=True
            if bOk==False:
                return #0
            #sVal=self.txtVal.GetValue()
            #self.PostMOD('key',sVal)
            self.__SetWidMod__(1)
            self.CB(self._doPostKey,evt.GetEventObject(),dKeyMod)
            return #1
            #wx.PostEvent(self,vtInputTextChanged(self,sVal))
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            self.__logDebug__(''%())
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            if self.widPop is not None:
                if hasattr(self.widPop,'GetWid'):
                    widPop=self.widPop.GetWid()
                else:
                    widPop=self.widPop
                widPop.Show(False)
                if hasattr(widPop,'CallWidChild'):
                    widPop.CallWidChild(-1,'__Close__')
            self.widBt.SetValue(False)
        except:
            self.__logTB__()
    Close=__Close__
    def __Clear__(self):
        try:
            self.__logDebug__(''%())
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            self.txtVal.SetValue('')
        except:
            self.__logTB__()
    def GetWid(self):
        #return self.pn
        return self.wid
    def GetWidMod(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
            return self.txtVal
        except:
            self.__logTB__()
            return None
    def __SetWidMod__(self,state):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'state':state})
            
            self.__setWidModGui__(state,self.txtVal)
        except:
            self.__logTB__()
    def Popup(self,bFlag=True):
        self.SetValue(bFlag)
        self.OnPopupButton(None)
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #self.Post('ok')
            self.__Close__()
        except:
            self.__logTB__()
    def OnPopupCancel(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #self.Post('cancel')
            self.__Close__()
        except:
            self.__logTB__()
