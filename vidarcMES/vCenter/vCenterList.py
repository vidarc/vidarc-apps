#----------------------------------------------------------------------------
# Name:         vCenterList.py
# Purpose:      vCenter list
# Author:       Walter Obweger
#
# Created:      20131030
# CVS-ID:       $Id: vCenterList.py,v 1.3 2015/01/21 12:07:36 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiListOrder import vGuiListOrder
    from vGuiImgLstWX import vGuiImgLstGet
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vCenterList(vGuiListOrder):
    pass
