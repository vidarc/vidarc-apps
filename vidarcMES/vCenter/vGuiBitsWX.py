#----------------------------------------------------------------------------
# Name:         vGuiBitsWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20150212
# CVS-ID:       $Id: vGuiBitsWX.py,v 1.2 2015/07/25 15:58:24 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiCoreWidWX import vGuiCoreWidWX
    #from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiBitsWX(vGuiCoreWidWX):
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=-1,mode='txt'|'txtRd'....,
                kwVal={'pos'...}
                ):
    """
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            vGuiCoreWidWX.__initObj__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            self.UpdKw('size_button',None,kwargs,bForce=True)
            kwargs['size_button']=None
            self.iDft=kwargs.get('default',[])
            self.lBits=kwargs.get('bits',[])
            self.lBitsName=[]
            self.lBitsWid=[]
            if bDbg:
                self.__logDebug__(self.lBits)
        except:
            self.__logTB__()
    def __get_widget_lst__(self,*args,**kwargs):
        lW=[]
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(self.lBits)
            szBut=self.GetDft('size_bit',(16,24),kwargs)
            
            bDbg=self.GetVerboseDbg(0)
            
            iOfs=0
            iLen=len(self.lBits)
            for tup in self.lBits:
                iBit,iVal,bmp,tool=tup
                sN='widBit%02d'%iOfs
                if bDbg:
                    self.__logDebugAdd__(sN,tup)
                iBorder=0
                if bmp is None:
                    lW.append(('tg',0,iBorder,{'name':sN,'label':tool,
                            'size':szBut,'evt':{
                                'tg':self.OnBut
                                },
                            },))
                else:
                    lW.append(('tgBmp',0,iBorder,{'name':sN,'bitmap':bmp,
                            'tool':tool,'size':szBut,
                            }))
                iOfs+=1
                self.lBitsName.append(sN)
        except:
            self.__logTB__()
        return lW
    def OnBut(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            #print evt
            obj=evt.GetEventObject()
            #print obj
            sName=obj.GetName()
            sOfs=sName[6:]
            iOfs=int(sOfs)
            #print sName
            iSel=obj.GetValue()
            iBit=self.lBits[iOfs][0]
            iVal=self.lBits[iOfs][1]
            #print sBit,iBit,iVal
            d={'iSel':iSel,'iBit':iBit,'iOfs':iOfs,
                'sName':sName,'iVal':iVal}
            #print d
            if bDbg:
                self.__logDebug__(d)
            
            self.PostFB('bit',d)
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            vGuiCoreWidWX.__initCtrl__(self,*args,**kwargs)
            for sN in self.lBitsName:
                w=getattr(self,sN)
                self.lBitsWid.append(w)
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            for w in self.lBitsWid:
                w.SetValue('')
        except:
            self.__logTB__()
    def __SetWidMod__(self,state):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'state':state})
            for w in self.lBitsWid:
                self.__setWidModGui__(state,w)
        except:
            self.__logTB__()
    def SetValue(self,val):
        try:
            if self.IsMainThread()==False:
                return 
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('val:%d,%xh'%(val,val))
            #self.bBlock=True
            #self.__markModified__(False)
            i=0
            for w in self.lBitsWid:
                if w is not None:
                    if (self.lBits[i][1]&val)!=0:
                        w.SetValue(True)
                    else:
                        w.SetValue(False)
                i+=1
            #wx.CallAfter(self.__clearBlock__)
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            if self.IsMainThread()==False:
                return 
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            val=0
            i=0
            for w in self.lBitsWid:
                if w is not None:
                    if w.GetValue():
                        val|=self.lBits[i][1]
                i+=1
        except:
            self.__logTB__()
        return val
    def GetValueStr(self):
        return '%08d'%self.GetValue()
    def SetValueStr(self,val):
        try:
            self.SetValue(long(val))
        except:
            self.SetValue(self.iDft)
