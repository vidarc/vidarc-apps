#----------------------------------------------------------------------------
# Name:         vGuiListCtrlWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
#   cols    ... [lColDef0,lColDef1,...]
#
#        lColDefx =[
#        name,       ... column name
#        align,      ... column alignment 
#                                -1=left
#                                -3=center
#                                -2=right
#                                else unchanged
#        width,      ... column width
#        sort,       ... column sortable 
#                                0=not sortable
#                                1=sortable initially ascending 
#                               -1=sortable initially descending
#        ]
#
#   map     ... [(key0,col0),(key1,col1)]
#
#   set value by dict
#      {-1:data,            ... data to set, value is hashed in dict self.dDat
#       0:imgIdx,           ... image index for fist column
#       key0:(s0,img0),     ... s0=string value, img0=image index
#       key1:s1,            ... s1=string value, image is cleared with emtpy index
#       key2:img2,          ... img2=image index, string value is cleared by u''
#       }
#
#   set value by list
#      [data,               ... data to set, value is hashed in dict self.dDat
#       [
#       (s0,img0),          ... s0=string value, img0=image index
#       s1,                 ... s1=string value, image is cleared with emtpy index
#       img2                ... img2=image index, string value is cleared by u''
#       ]
#
# Created:      20120317
# CVS-ID:       $Id: vGuiListCtrlWX.py,v 1.23 2015/08/03 07:22:57 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import sys
    import types
    import wx
    import fnmatch
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    from vGuiImgLstWX import vGuiImgLstGet
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiColumnStretch:
    def __init__(self,iMinWidth=100,iMinProp=10,iScrollBarWidth=20):
        self._lStretchFact=None
        self._dStretchFact=None
        self._iMinWidth=iMinWidth
        self._iMinProp=iMinProp
        self._iScrollBarWidth=iScrollBarWidth
    def __setup__(self):
        self._dStretchFact={'__width':0,'__prop':[],'__rel':0}
        iRel=0
        if self._lStretchFact is None:
            return
        for iCol,iFact in self._lStretchFact:
            self._dStretchFact[iCol]=iFact
            if iFact<0:
                self._dStretchFact['__prop'].append(iCol)
                iRel+=-iFact
        self._dStretchFact['__rel']=iRel
        iCols=self._GetColCount()
        iFixedW=0
        for i in xrange(iCols):
            if i in self._dStretchFact:
                continue
            iW=self._GetColWidth(i)
            self._dStretchFact[i]=iW
            iFixedW+=iW
        self._dStretchFact['__width']=iFixedW
    def SetStretchLst(self,l):
        self._lStretchFact=l
        self._dStretchFact=None
        self.OnSizeStretch(None)
    def OnSizeStretch(self,evt):
        if evt is not None:
            evt.Skip()
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            w=self.GetWid()
            w.Freeze()
            if self._dStretchFact is None:
                self.__setup__()
            sz=self.wid.GetClientSize()
            iW=max(self._iMinWidth,sz.GetWidth()-self._iScrollBarWidth)
            iWUsed=0
            if bDbg:
                self.__logDebug__({'sz':sz,'iW':iW,
                        '_lStretchFact':self._lStretchFact})
            if self._lStretchFact is not None:
                for iCol,iFact in self._lStretchFact:
                    if iFact>0:
                        iWh=int(iFact*iW)
                        iWUsed+=iWh
                        self._SetColWidth(iCol,iWh)
            iWUsed+=self._dStretchFact['__width']
            l=self._dStretchFact['__prop']
            if self._dStretchFact['__rel']>0:
                iWtmp=max(iW-iWUsed,self._iMinProp)/float(self._dStretchFact['__rel'])
                for iCol in l:
                    self._SetColWidth(iCol,int(iWtmp*(-self._dStretchFact[iCol])))
            w.Thaw()
        except:
            self.__logTB__()
            w.Thaw()

def cmpExact(a,b):
    return a==b
def cmpSimilar(lA,lB):
    for a,b in zip(lA,lB):
        t=type(b)
        if t==types.StringType:
            if fnmatch.fnmatch(a,b)==False:
                return -1
        elif t==types.UnicodeType:
            if fnmatch.fnmatch(a,b)==False:
                return -2
        else:
            if cmp(a,b)!=0:
                return -3
    return 0
class vGuiListCtrlWX(vGuiCoreWX,vGuiColumnStretch):
    #def __init__(self,id=-1,parent=None,pos=(0,0),size=(32,32),name='tgPopup',
    #            style=wx.LC_REPORT,cols=None,default_col_width=100,
    #            multiple_sel=True,imgLst=None,
    #            map=None,value=None):
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.wid=None
            self.widPop=None
            self.IMG_SORT_ASC=-1
            self.IMG_SORT_DESC=-1
            self.IMG_EMPTY=-1
            self.iDat=1L
            self.dDat={}
            self._dSort=None
            self._iSort=0
            self._iSortCol=-2
            self._funcSort=None
            #self._argsSort=()
            self._kwargsSort={}
            self._dSortDef=None
            if kwargs.get('multiple_sel',0)==0:
                self._bMultiple=False
            else:
                self._bMultiple=True
            self._dFindChar=None        # 20150728 wro:add find char
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        wid=None
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            style=kwargs.get('style',0)
            # +++++
            if self._bMultiple==False:
                style|=wx.LC_SINGLE_SEL
            _args,_kwargs=self.GetGuiArgs(kwargs,
                    ['id','name','parent','pos','size','style'],
                    {'pos':(0,0),'size':(-1,-1),'style':style})
            _kwargs['style']=style
            wid=wx.ListCtrl(*_args,**_kwargs)
            self._it=wx.ListItem()
            self.wid=wid
            
            vGuiColumnStretch.__init__(self)
            self._GetColCount=self.wid.GetColumnCount
            self._GetColWidth=self.wid.GetColumnWidth
            self._SetColWidth=self.wid.SetColumnWidth
            #self.CB
            wx.CallAfter(wx.EVT_SIZE,self.wid,self.OnSizeStretch)
            wx.CallAfter(self.OnSizeStretch,None)
            
            self.wid.Bind(wx.EVT_LIST_COL_CLICK,self.OnListColClick,
                    id=self.wid.GetId())
            # -----
        except:
            self.__logTB__()
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('setup widget')
            # ++++ setup mapping
            oTmp=kwargs.get('map',None)
            if oTmp is not None:
                self.MAP=oTmp
            # ---- setup mapping
            self._dImg=None
            self._imgLst=None
            imgLst=kwargs.get('imgLst',None)
            if imgLst is not None:
                if type(imgLst)==types.StringType:
                    w,bImgLstCreated=vGuiImgLstGet(imgLst)
                    if w is not None:
                        dImg0,imgLst0=w.GetObjs()
                        self.SetImageListByDict(dImg0,imgLst0)
                        self._dImg,self._imgLst=dImg0,imgLst0
            self.__initImageLst__()
            # ++++ setup columns
            self._iColDftWidth=kwargs.get('default_col_width',100)
            oTmp=kwargs.get('cols',None)
            if oTmp is not None:
                self.SetColumns(oTmp)
            # ---- setup columns
        except:
            self.__logTB__()
        return wid
    def __initProperties__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            #self.FORCE_CHILDREN=kwargs.get('bForceChildren',False)
            #self._bHideRoot=kwargs.get('bHideRoot',False)
            #self._bSort=kwargs.get('bSort',False)
            # ++++ assign values
            oTmp=kwargs.get('value',None)
            if oTmp is not None:
                self.SetValue(oTmp)
            # ---- assign values
            # ++++ select values
            oTmp=kwargs.get('sel',None)
            if oTmp is not None:
                t=type(oTmp)
                if t==types.TupleType:
                    l=oTmp[0]
                    lMapChk=oTmp[1]
                    self.SetSelected(l,lMapChk=lMapChk)
                else:
                    self.SetSelected(oTmp)
            # ---- select values
        except:
            self.__logTB__()
    def __initImageLst__(self):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def SetWidPopup(self,wid):
        """
        """
        try:
            if self.IsMainThread()==False:
                return
            self.widPop=wid
            w=self.GetWid()
            w.Enable(True)
            self.bBusy=True
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                if hasattr(self.widPop,'BindEvent'):
                    #print 'bindevent'
                    self.widPop.BindEvent('ok',self.OnPopupOk)
                    self.widPop.BindEvent('cancel',self.OnPopupCancel)
        except:
            self.__logTB__()
    def GetWidPopup(self):
        return self.widPop
    def OnPopupButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            w=self.GetWid()
            if self.widPop is not None:
                if w.GetValue()==True:
                    if self.iArrange>=0:
                        vtGuiCoreArrangeWidget(w,self.widPop,self.iArrange)
                    self.widPop.Show(True)
                else:
                    self.widPop.Show(False)
            else:
                self.__logError__('no popup widget set yet'%())
                #vtGuiCoreArrangeWidget(self,self,iArrange)
        except:
            self.__logTB__()
    def OnMoveButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            w=self.GetWid()
            if self.widPop is not None:
                if self.iArrange>=0:
                    vtGuiCoreArrangeWidget(w,self.widPop,self.iArrange)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            self.__logDebug__(''%())
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            if self.widPop is not None:
                if hasattr(self.widPop,'GetWid'):
                    widPop=self.widPop.GetWid()
                else:
                    widPop=self.widPop
                widPop.Show(False)
            #self.wid.SetValue(False)
        except:
            self.__logTB__()
    Close=__Close__
    def GetWid(self):
        return self.wid
    def GetWidMod(self):
        return self.wid
    def Popup(self,bFlag=True):
        self.__logCritical__(''%())
        #self.SetValue(bFlag)
        self.OnPopupButton(None)
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
    def OnPopupCancel(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
    # ++++++++++++++++++++++++++++++++++++++++
    def BindEvent(self,name,func,par=None):
        if name.startswith('lst_'):
            wid=self.GetWid()
            if name in ['lst_item_sel','lst_item_selected']:
                wid.Bind(wx.EVT_LIST_ITEM_SELECTED,func, par or wid)
            elif name in ['lst_item_desel','lst_item_deselected']:
                wid.Bind(wx.EVT_LIST_ITEM_DESELECTED,func, par or wid)
            elif name in ['lst_item_right_click','lst_item_rgClk']:
                wid.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,func, par or wid)
            elif name in ['lst_col_drag_begin']:
                wid.Bind(wx.EVT_LIST_COL_BEGIN_DRAG,func, par or wid)
            elif name in ['lst_col_dragging']:
                wid.Bind(wx.EVT_LIST_COL_DRAGGING,func, par or wid)
            elif name in ['lst_col_drag_end']:
                wid.Bind(wx.EVT_LIST_COL_END_DRAG,func, par or wid)
            elif name in ['lst_col_left_click']:
                wid.Bind(wx.EVT_LIST_COL_LEFT_CLICK,func, par or wid)
            elif name=='lst_col_right_click':
                wid.Bind(wx.EVT_LIST_COL_RIGHT_CLICK,
                        func, par or wid)
        else:
            vGuiCoreWX.BindEvent(self,name,func,par=par)
    def GetCacheData(self):
        return self.dDat
    def GetData(self,iCltData):
        if iCltData in self.dDat:
            return self.dDat[iCltData]
        return None
    def GetDataKey(self,cltData):
        for k,v in self.dDat.iteritems():
            if v==cltData:
                return k
        return -1
    def SetMap(self,l):
        self.MAP=l
    def AddImageList(self,dImg,imgLst,lDef,bForce=False):
        for sK,it in lDef:
            if bForce==False:
                if sK in dImg:
                    continue
            t=type(it)
            if t==types.TupleType:
                lImg=[imgLst.Add(bmp) for bmp in it]
            elif t==types.ListType:
                lImg=[imgLst.Add(bmp) for bmp in it]
            else:
                lImg=imgLst.Add(it)
            dImg[sK]=lImg
    def CreateImageList(self,lDef,which=None,lDft=None,bSet=False):
        """ 
        lDef
            [
            ['name0',bmp0],
            ['name1',[bmp0,bmp1],
            ]
        """
        dImg={}
        if (which is None) or (which==wx.IMAGE_LIST_SMALL):
            imgLst=wx.ImageList(16,16)
        else:
            imgLst=wx.ImageList(16,16)
            which=None
        if lDft is None:
            lDft=[
                ('empty',       self.getBmp('core','Invisible')),
                ('asc',         self.getBmp('core','UpSml')),
                ('desc',        self.getBmp('core','DnSml')),
                ]
        self.AddImageList(dImg,imgLst,lDft,bForce=False)
        self.AddImageList(dImg,imgLst,lDef)
        if bSet in [True,1]:
            self.SetImageListByDict(dImg,imgLst)
        elif bSet==2:
            self.SetImageListByDict(dImg,imgLst)
            self._dImg=dImg
            self._imgLst=imgLst
        return imgLst,dImg
    def GetImg(self,sTag,sFB):
        try:
            if self._dImg is not None:
                iImg=self._dImg.get(sTag,None)
                if iImg is None:
                    iImg=self._dImg.get(sFB,-1)
                return iImg
        except:
            self.__logTB__()
        return -1
    def SetImageListByDict(self,dImg,imgLst):
        iImgEmptyIdx=dImg.get('empty',0)
        iImgSortAsc=dImg.get('asc',-1)
        iImgSortDesc=dImg.get('desc',-1)
        self.SetImageList(imgLst,iImgEmptyIdx=iImgEmptyIdx,
                iImgSortAsc=iImgSortAsc,iImgSortDesc=iImgSortDesc)
    def SetImageList(self,imgLst,iImgEmptyIdx=0,iImgSortAsc=-1,iImgSortDesc=-1,
                which=None):
        self.__logDebug__({
            'IMG_SORT_ASC':self.IMG_SORT_ASC,
            'IMG_SORT_DESC':self.IMG_SORT_DESC,
            'IMG_EMPTY':self.IMG_EMPTY,
            })
        wx.ListCtrl.SetImageList(self.wid,imgLst,which=which or wx.IMAGE_LIST_SMALL)
        self.IMG_SORT_ASC=iImgSortAsc
        self.IMG_SORT_DESC=iImgSortDesc
        self.IMG_EMPTY=iImgEmptyIdx
        self.__logDebug__({
            'IMG_SORT_ASC':self.IMG_SORT_ASC,
            'IMG_SORT_DESC':self.IMG_SORT_DESC,
            'IMG_EMPTY':self.IMG_EMPTY,
            })
    def DeleteAllItems(self):
        wx.ListCtrl.DeleteAllItems(self.wid)
        self.iDat=1L
        self.dDat={}
        self._dSort=None
    def ClearAll(self):
        wx.ListCtrl.ClearAll(self.wid)
        self.iDat=1L
        self.dDat={}
        self._dSort=None
    Clear=ClearAll
    def __add_data__(self,data,i=0):
        try:
            if i is 0:
                i=self.iDat
                self.iDat+=1
            self.dDat[i]=data
            return i
        finally:
            pass
        return -1
    def _getAlignment(self,iAlign):
        if iAlign is None:
            iAlign=wx.ALIGN_LEFT
        elif iAlign<0:
            if iAlign==-1:
                iAlign=wx.ALIGN_LEFT
            elif iAlign==-2:
                iAlign=wx.ALIGN_RIGHT
            elif iAlign==-3:
                iAlign=wx.ALIGN_CENTRE
            else:
                self.__logError__('unknown format;%d',(iAlign))
                iAlign=wx.ALIGN_LEFT
        if wx.VERSION < (2,8):
            if iAlign==wx.ALIGN_LEFT:
                iAlign=wx.LIST_FORMAT_LEFT
            elif iAlign==wx.ALIGN_CENTRE:
                iAlign=wx.LIST_FORMAT_CENTRE
                #iAlign=wx.LIST_FORMAT_RIGHT
            elif iAlign==wx.ALIGN_RIGHT:
                iAlign=wx.LIST_FORMAT_RIGHT
            else:
                self.__logError__('unknown format;%d;0x%x'%(iAlign,iAlign))
                iAlign=wx.LIST_FORMAT_LEFT
        else:
            pass
        return iAlign
    def SetColumnAlignment(self,iCol,iAlign):
        self.SetColumn(iCol,None,iAlign,None,None)
    def SetColumn(self,iCol,sName,iAlign=None,iWidth=None,iSort=None,iImg=None):
        try:
            wid=self.wid
            if self.__isLogDebug__():
                self.__logDebug__('iCol:%d;sName:%r;iAlign:%r;iWith:%r;'\
                        'iSort:%r;iImg:%r'%(\
                        iCol,sName,iAlign,iWidth,iSort,iImg))
            if type(sName)==type(self._it):
                wx.ListCtrl.SetColumn(wid,iCol,sName)
                return
            it=wid.GetColumn(iCol)
            mask=it.m_mask
            if sName is not None:
                it.m_text=sName
                mask|=wx.LIST_MASK_TEXT
            if iAlign is not None:
                it.m_format=self._getAlignment(iAlign)
                mask|=wx.LIST_MASK_FORMAT
            if iImg is not None:
                it.m_image=iImg
                mask|=wx.LIST_MASK_IMAGE
            elif iSort is not None:
                if iSort==0:
                    it.m_image=self.IMG_EMPTY
                elif iSort<0:
                    it.m_image=self.IMG_SORT_DESC
                elif iSort>0:
                    it.m_image=self.IMG_SORT_ASC
                else:
                    self.__logError__('undef sort:%d'%(iSort))
                    it.m_image=IMG_EMPTY
                mask|=wx.LIST_MASK_IMAGE
            if mask!=0:
                it.m_mask=mask
                wx.ListCtrl.SetColumn(wid,iCol,it)
                return 1
            return 0
        except:
            self.__logTB__()
        return -1
    def SetColumns(self,cols):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            #self.DeleteAllItems()
            iCnt=self.wid.GetColumnCount()
            bDbg=False
            if VERBOSE>0:
                if self.__isLogDebug__():
                    bDbg=True
            if bDbg==True:
                self.__logDebug__(['cols;',cols])
            if cols is None:
                self.__logDebug__('exit nothing to do'%())
                return
            # check sortable col def
            iColSort=-1
            dSortDef=None
            iTmp=0
            for lCol in cols:
                if len(lCol)>3:
                    if lCol[3]!=0:
                        if iColSort==-1:
                            iColSort=iTmp
                            dSortDef={-1:{'iSort':1}}
                            dSortDef[iTmp]={'iSort':lCol[3]}
                        else:
                            dSortDef[iTmp]={'iSort':lCol[3]}
                iTmp=iTmp+1
            if iColSort>=0:
                if self.IMG_SORT_ASC==-1:
                    imgLst,dImg=self.CreateImageList([
                        ('empty',       self.getBmp('core','Invisible')),
                        ('asc',         self.getBmp('core','UpSml')),
                        ('desc',        self.getBmp('core','DnSml')),
                        ])
                    self.SetImageList(imgLst,dImg['empty'],
                            dImg['asc'],dImg['desc'])
                    wx.ListCtrl.AssignImageList(self.wid,imgLst,
                            which=wx.IMAGE_LIST_SMALL)
            iCol=0
            lStretch=[]
            iSort=0
            for lCol in cols:
                if bDbg==True:
                    self.__logDebug__('iCol:%d;iCnt:%d;lCol;%r'%(iCol,iCnt,lCol))
                iW=lCol[2] or self._iColDftWidth
                iWType=type(iW)
                if iWType==types.FloatType:
                    lStretch.append((iCol,iW))
                    iW=self._iColDftWidth
                elif iWType==types.StringType:
                    iWsz=self.wid.GetTextExtent(iW)
                    iWedge=wx.SystemSettings.GetMetric(wx.SYS_EDGE_X)
                    iW=iWsz[0]+(iWedge<<2)#+2
                elif iW<0:
                    lStretch.append((iCol,iW))
                    iW=self._iColDftWidth
                if iSort==0:
                    iSortTmp=lCol[3]
                    iSort=iSortTmp
                else:
                    iSortTmp=0
                if iCol<iCnt:
                    self.SetColumn(iCol,lCol[0],lCol[1],iW,iSortTmp,None)
                else:
                    self.wid.InsertColumn(iCol,lCol[0],self._getAlignment(lCol[1]),iW)
                    self.SetColumn(iCol,lCol[0],lCol[1],None,iSortTmp,None)
                iCol+=1
            for i in xrange(iCol,iCnt):
                self.wid.DeleteColumn(iCol)
            self.SetStretchLst(lStretch)
            if dSortDef is not None:
                self.SetSortDef(dSortDef)
                #self._iSort=dSortDef[iColSort]['iSort']
                #self._iSortCol=iColSort
                self.SetSort(iColSort,dSortDef[iColSort]['iSort'])
        except:
            self.__logTB__()
    def SetItem(self,idx,i,v):
        try:
            if self.IsMainThread()==False:
                return -2
            if i<0:
                i=self.wid.GetItemData(idx)
                i=self.__add_data__(v,i)
                self.wid.SetItemData(idx,i)
                return 0
            t=type(v)
            if t==types.TupleType:
                self.wid.SetStringItem(idx,i,v[0],v[1])
            elif t==types.IntType:
                self.wid.SetStringItem(idx,i,u'',v)
            else:
                self.wid.SetStringItem(idx,i,v,-1)
            return 0
        except:
            self.__logTB__()
    def GetItemTup(self,idx,i):
        try:
            if self.IsMainThread()==False:
                return None
            if i==-1:
                i=self.wid.GetItemData(idx)
                if i>0:
                    return self.dDat.get(i,None)
                return None
            elif i==-3:
                return self.wid.GetItemData(idx)
            elif i<0:
                return None
            it=self.wid.GetItem(idx,i)
            t=(it.m_text[:],self._it.m_image)
            return t
        except:
            self.__logTB__()
    def GetItemStr(self,idx,i):
        try:
            if self.IsMainThread()==False:
                return None
            if i==-1:
                i=self.wid.GetItemData(idx)
                if i>0:
                    return self.dDat.get(i,None)
                return None
            elif i==-3:
                return self.wid.GetItemData(idx)
            elif i<0:
                return None
            it=self.wid.GetItem(idx,i)
            return it.m_text[:]
        except:
            self.__logTB__()
    def GetCount(self):
        try:
            if self.IsMainThread()==False:
                return -1
            return self.wid.GetItemCount()
        except:
            self.__logTB__()
    def __setItemDict__(self,lst,idx,d):
        #print tr.GetItemText(ti,0)
        for s,i in self.MAP:
            if s in d:
                v=d[s]
                t=type(v)
                if t==types.TupleType:
                    lst.SetStringItem(idx,i,v[0],v[1])
                elif t==types.IntType:
                    lst.SetStringItem(idx,i,u'',v)
                else:
                    lst.SetStringItem(idx,i,v,-1)
        if 0 in d:
            self._it.Clear()
            img=d[0]
            bSkipImg=True
            t=type(img)
            if t==types.IntType:
                self._it.SetImage(img)
                bSkipImg=False
            elif t==types.StringType:
                if self._dImg is not None:
                    self._it.SetImage(self._dImg[img])
                    bSkipImg=False
            if bSkipImg==False:
                self._it.SetMask(wx.LIST_MASK_IMAGE)
                self._it.SetId(idx)
                lst.SetItem(self._it)
                #lst.SetItem(idx,self._it)
        if -1 in d:
            i=lst.GetItemData(idx)
            i=self.__add_data__(d[-1],i)
            lst.SetItemData(idx,i)
        #if -2 in d:
        #    self.__setValue__(tr,ti,d[-2])
    def __setItemLst__(self,lst,idx,l):
        try:
            if l[0] is not None:
                i=self.wid.GetItemData(idx)
                i=self.__add_data__(l[0],i)
                lst.SetItemData(idx,i)
            i=0
            for v in l[1]:
                t=type(v)
                if t==types.TupleType:
                    lst.SetStringItem(idx,i,v[0],v[1])
                elif t==types.IntType:
                    lst.SetStringItem(idx,i,u'',v)
                else:
                    lst.SetStringItem(idx,i,v,-1)
                i+=1
        except:
            self.__logTB__()
            self.__logError__('l:%r;l[0]:%r;l[1]:%r'%(l,
                    l[0],l[1]))
    def __setItemTup__(self,tr,ti,t):
        pass
    def __setItem__(self,lst,idx,val):
        t=type(val)
        if t==types.DictType:
            self.__setItemDict__(lst,idx,val)
        elif t==types.ListType:
            self.__setItemLst__(lst,idx,val)
        elif t==types.TupleType:
            self.__setItemTup__(lst,idx,val)
    def __setValue__(self,lst,l,idx=-1):
        if idx<0:
            idx=0
        iCnt=lst.GetItemCount()
        for val in l:
            if idx>=iCnt:
                idx=lst.InsertImageStringItem(idx,'',self.IMG_EMPTY)
                iCnt+=1
            self.__setItem__(lst,idx,val)
            idx+=1
        return idx-1
    def __getValue__(self,lst,idx,lMap,l):
        def getVal(lst,idx,i):
            if i==-1:
                i=lst.GetItemData(idx)
                if i>0:
                    return self.dDat.get(i,None)
                return None
            elif i==-2:
                return idx
            elif i==-3:
                return lst.GetItemData(idx)
            else:
                it=lst.GetItem(idx,i)
                return it.m_text[:]
        if lMap is None:
            l.append(getVal(lst,idx,-2))
        else:
            l.append([getVal(lst,idx,i) for i in lMap])
    def __getValueData__(self,lst,idx,l):
        i=lst.GetItemData(idx)
        if i>0:
            l.append(self.dDat.get(i,None))
    def __insert__(self,lst,l,idx=-1):
        try:
            if idx<0:
                idx=sys.maxint
                for val in l:
                    idx=lst.InsertImageStringItem(sys.maxint,'',self.IMG_EMPTY)
                    self.__setItem__(lst,idx,val)
            else:
                idx-=1
                for val in l:
                    idx+=1
                    idx=lst.InsertImageStringItem(idx,'',self.IMG_EMPTY)
                    self.__setItem__(lst,idx,val)
            return idx
        except:
            self.__logTB__()
        return -1
    def Insert(self,l,idx=-1):
        try:
            if self.IsMainThread()==False:
                return -2
            w=self.wid
            return self.__insert__(w,l,idx=idx)
        except:
            self.__logTB__()
        return -1
    def SetValue(self,l,idx=-1):
        try:
            if self.IsMainThread()==False:
                return -2
            self.__logDebug__(''%())
            w=self.wid
            #iCols=w.GetColumnCount()
            iRet=-2
            if idx==-1:
                w.DeleteAllItems()
                iRet=self.__insert__(w,l,idx=idx)
            else:
                iRet=self.__setValue__(w,l,idx=idx)
            self.Sort()
            return iRet
        except:
            self.__logTB__()
        return -1
    def GetValue(self,lMap=None,idx=None):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return None
            l=[]
            lst=self.wid
            if idx is None:
                idx=0
                iCnt=-1
            elif idx<0:
                idx=0
                iCnt=-1
            else:
                iCnt=1
            if iCnt<0:
                iCnt=lst.GetItemCount()
            if lMap is None:
                for i in xrange(idx,idx+iCnt):
                    self.__getValueData__(lst,i,l)
            else:
                for i in xrange(idx,idx+iCnt):
                    self.__getValue__(lst,i,lMap,l)
            if self.__isLogDebug__():
                self.__logDebug__(l)
            return l
        except:
            self.__logTB__()
        return None
    def PrcFct(self,idx,bDbg,fct,*args,**kwargs):
        try:
            lst=self.wid
            if idx is None:
                idx=0
                iCnt=-1
            elif idx<0:
                idx=0
                iCnt=-1
            else:
                iCnt=1
            if iCnt<0:
                iCnt=lst.GetItemCount()
            iOfs=0
            for i in xrange(idx,idx+iCnt):
                iRet=fct(iOfs,iCnt,i,*args,**kwargs)
                if iRet<=0:
                    return iRet
                iOfs+=1
            return 1
        except:
            self.__logTB__()
            return -1
    def FindChar(self,sK,iSel=1,lMapChk=None):
        try:
            if self.IsMainThread()==False:
                return None
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'sK':sK,'iSel':iSel})
            if lMapChk is None:
                lMapChk=[0]
            lst=self.wid
            iIdx=None
            if self._dFindChar is None:
                self._dFindChar={'chr':sK,'iIdx':None}
            else:
                if self._dFindChar['chr']!=sK:
                    self._dFindChar={'chr':sK,'iIdx':None}
                else:
                    self._dFindChar['iIdx']=self._dFindChar['iIdx']+1
            if self._dFindChar['iIdx'] is None:
                #lIdx=self.GetSelected([-2]+lMapChk)
                lIdx=self.GetSelected([-2])
                if bDbg:
                    self.__logDebug__(lIdx)
                if len(lIdx)>0:
                    #try:
                    #    if lIdx[0][1][0]==sK:
                    #        self._dFindChar['iIdx']=lIdx[0][0]+1
                    #except:
                    #    self.__logTB__()
                    self._dFindChar['iIdx']=lIdx[0][0]+1
                else:
                    self._dFindChar['iIdx']=0
            iIdx=self._dFindChar['iIdx']
            if iIdx is not None:
                if iIdx>=lst.GetItemCount():
                    iIdx=0
                    self._dFindChar['iIdx']=iIdx
            
            lIdx=self.FindValue([sK+'*'],lMapChk=lMapChk,lMapGet=[-2,-1,0],
                    idx=iIdx,iMode=1,iMax=1)
            iL=len(lIdx)
            if bDbg:
                self.__logDebug__({'lIdx':lIdx,'iIdx':iIdx,'sK':sK,
                        '_dFindChar':self._dFindChar})
            if (iL==0) and (iIdx>=0):
                iIdx=0
                self._dFindChar['iIdx']=iIdx
                lIdx=self.FindValue([sK+'*'],lMapChk=lMapChk,lMapGet=[-2,-1,0],
                        idx=iIdx,iMode=1,iMax=1)
                iL=len(lIdx)
                if bDbg:
                    self.__logDebug__({'lIdx':lIdx,'iIdx':iIdx,'sK':sK,
                            '_dFindChar':self._dFindChar})
                    
            if iL>0:
                iIdx=lIdx[0][0]
                self._dFindChar['iIdx']=iIdx
            else:
                iIdx=None
            if bDbg:
                self.__logDebug__({'lIdx':lIdx,'iIdx':iIdx,'sK':sK,
                        '_dFindChar':self._dFindChar})
            if iSel>0:
                if iIdx is not None:
                    self.SetSelected([iIdx])
            return iIdx
        except:
            self.__logTB__()
        return None
    def FindValue(self,val,lMapChk=None,lMapGet=None,idx=None,iMode=0,iMax=-1):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return None
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({
                        'val':val,
                        'lMapChk':lMapChk,
                        'lMapGet':lMapGet,
                        'idx':idx,
                        'iMode':iMode,
                        'iMax':iMax,
                        })
            l=[]
            lst=self.wid
            iCntItm=lst.GetItemCount()
            if idx is None:
                idx=0
                iCnt=-1
            elif idx<0:
                idx=0
                iCnt=-1
            else:
                iCnt=iCntItm-idx
                #iCnt=1
            if iCnt<0:
                iCnt=iCntItm
            if iMode==0:
                funcCmp=cmpExact
            elif iMode==1:
                funcCmp=cmpSimilar
            else:
                funcCmp=cmpExact
            iFound=0
            lIdx=[]
            if lMapChk is None:
                for i in xrange(idx,idx+iCnt):
                    ll=[]
                    self.__getValueData__(lst,i,ll)
                    iR=funcCmp(ll[0],val)
                    #if ll[0]==val:
                    if bDbg:
                        self.__logDebug__({'iR':iR,'i':i,'ll[0]':ll[0],'val':val})
                    if iR==0:
                        lIdx.append(i)
                        iFound+=1
                    if iFound>=iMax:
                        break
            else:
                for i in xrange(idx,idx+iCnt):
                    ll=[]
                    self.__getValue__(lst,i,lMapChk,ll)
                    iR=funcCmp(ll[0],val)
                    #if ll[0]==val:
                    if bDbg:
                        self.__logDebug__({'iR':iR,'i':i,'ll[0]':ll[0],'val':val})
                    if iR==0:
                        lIdx.append(i)
                        iFound+=1
                    if iFound>=iMax:
                        break
            if bDbg:
                self.__logDebug__({'lIdx':lIdx,'val':val})
            
            if lMapGet is None:
                for i in lIdx:
                    self.__getValueData__(lst,i,l)
            else:
                for i in lIdx:
                    self.__getValue__(lst,i,lMapGet,l)
            
            if self.__isLogDebug__():
                self.__logDebug__(l)
            return l
        except:
            self.__logTB__()
        return None
    def __setState__(self,lst,l,bFlag,iState):
        try:
            iCnt=lst.GetItemCount()
            iIdxFirst=-1
            if bFlag==True:
                if l is None:
                    pass
                else:
                    if self._bMultiple==False:      # 20150727 wro:single sel 
                        lst.SetItemState(l[0],iState,iState)
                        return l[0]
                    for i in l:
                        lst.SetItemState(i,iState,iState)
                        if iIdxFirst<0:
                            iIdxFirst=i
                    return iIdxFirst
            else:
                if l is None:
                    for i in xrange(iCnt):
                        lst.SetItemState(i,0,iState)
                        if iIdxFirst<0:
                            iIdxFirst=i
                    return iIdxFirst
                else:
                    for i in l:
                        lst.SetItemState(i,0,iState)
                        if iIdxFirst<0:
                            iIdxFirst=i
                    return iIdxFirst
            return 0
        except:
            self.__logTB__()
        return -1
    def __setSelected__(self,lst,l,bFlag):
        return self.__setState__(lst,l,bFlag,wx.LIST_STATE_SELECTED)
    def __getState__(self,lst,lMap,l,iState):
        try:
            iCnt=lst.GetItemCount()
            iSel=0
            for i in xrange(0,iCnt):
                if (lst.GetItemState(i,iState)&iState)==iState:
                    self.__getValue__(lst,i,lMap,l)
                    iSel+=1
            return iSel
        except:
            self.__logTB__()
        return -1
    def __getSelected__(self,lst,lMap,l):
        return self.__getState__(lst,lMap,l,wx.LIST_STATE_SELECTED)
    def SetSelected(self,l,bFlag=True,lMapChk=None,bKeepSel=False,bEnsure=True):
        try:
            self.__logDebug__(l)
            bDb0=self.GetVerboseDbg(0)
            bDbg=self.GetVerboseDbg(10)
            if self.IsMainThread()==False:
                return -2
            lst=self.wid
            if self._bMultiple==True:
                if bKeepSel==False:
                    lSel=[]
                    self.__getSelected__(lst,None,lSel)
                    self.__setSelected__(lst,lSel,bFlag=False)
            t=type(l)
            if t==types.DictType:
                if lMapChk is None:
                    lKeys=l.keys()
                    lMapChk=[]
                    ll=[]
                    for sK in lKeys:
                        if type(sK)==types.IntType:
                            lMapChk.append(sK)
                            ll.append(l[sK])
                        else:
                            try:
                                bFound=False
                                for tM in self.MAP:
                                    if tM[0]==sK:
                                        lMapChk.append(tM[1])
                                        bFound=True
                                        break
                                if bFound==True:
                                    ll.append(l[sK])
                                else:
                                    self.__logErrorAdd__('maapping faulty',
                                            {'sK':sK,'MAP':self.MAP})
                            except:
                                self.__logTB__()
                    l=[ll]
                    if bDb0:
                        self.__logDebug__({'lMapChk':lMapChk})
            if lMapChk is None:
                idx=self.__setSelected__(lst,l,bFlag=bFlag)
            else:
                iCntChk=len(lMapChk)
                iCnt=lst.GetItemCount()
                if iCntChk==1:
                    if iCnt>0:
                        t=type(l[0])
                        if t==types.ListType:
                            pass
                        elif t==types.ListType:
                            pass
                        else:   # 20150118 wro:values need to be list,
                                #       convert l from [1,2] to l=[[1],[2]]
                            ll=[]
                            for v in l:
                                ll.append([v])
                            l=ll
                lIdx=[]
                
                for i in xrange(iCnt):
                    ll=[]
                    self.__getValue__(lst,i,lMapChk,ll)
                    for val in l:
                        if bDbg:
                            self.__logDebug__({'ll':ll,'val':val})
                        if ll[0]==val:
                            
                            if bDbg:
                                self.__logDebug__({'ll[0]':ll[0],'val':val})
                            lIdx.append(i)
                if bDb0:
                    self.__logDebug__(lIdx)
                idx=self.__setSelected__(lst,lIdx,bFlag=bFlag)
            if bEnsure==True:
                if idx>=0:
                    lst.EnsureVisible(idx)
            return idx
        except:
            self.__logTB__()
        return -1
    def GetSelected(self,lMap=None):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            l=[]
            lst=self.wid
            self.__getSelected__(lst,lMap or [-2,-1],l)
            return l
        except:
            self.__logTB__()
        return None
    def DelIdx(self,iIdx):
        try:
            self.wid.DeleteItem(iIdx)
        except:
            self.__logTB__()
    def DelSelected(self):
        try:
            lSel=self.GetSelected([-2,-1])
            self.__logDebug__(lSel)
            lSel.reverse()
            for t in lSel:
                self.DelIdx(t[0])
        except:
            self.__logTB__()
    def __showItem__(self,lst,iIdx):
        try:
            self.__logDebug__(''%())
            lst.EnsureVisible(iIdx)
        except:
            self.__logTB__()
    def ShowItem(self,iIdx):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            l=[]
            lst=self.wid
            self.__showItem__(lst,iIdx)
            return l
        except:
            self.__logTB__()
        return None
    def __getStateByName__(self,state):
        if state in ['sel','selected']:
            iState=wx.LIST_STATE_SELECTED
        elif state in ['foc','focus','focused']:
            iState=wx.LIST_STATE_FOCUSED
        elif state in ['sel_foc','sel_focus']:
            iState=wx.LIST_STATE_SELECTED|wx.LIST_STATE_FOCUSED
        elif state=='cut':
            iState=wx.LIST_STATE_CUT
        elif state=='sel_cut':
            iState=wx.LIST_STATE_SELECTED|wx.LIST_STATE_CUT
        else:
            iState=wx.LIST_STATE_SELECTED
        return iState
    def SetByStateName(self,l,bFlag=True,state='sel'):
        try:
            self.__logDebug__(''%())
            lst=self
            iState=self.__getStateByName__(state)
            self.__setState__(lst,l,bFlag,iState)
            return 0
        except:
            self.__logTB__()
        return None
    def GetByStateName(self,state='sel',lMap=None):
        try:
            self.__logDebug__(''%())
            l=[]
            lst=self
            iState=self.__getStateByName__(state)
            self.__getState__(lst,lMap or [-2,-1],l,iState)
            return l
        except:
            self.__logTB__()
        return None
    def OnListColClick(self, evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            if self._dSortDef is None:
                return
            iCol=evt.GetColumn()
            bSort=True
            if iCol in self._dSortDef:
                dDef=self._dSortDef[iCol]
            elif -1 in self._dSortDef:
                dDef=self._dSortDef[-1]
            else:
                dDef={}
                bSort=False
            if VERBOSE:
                if self.__isLogDebug__():
                    self.__logDebug__('iCol:%d;dDef;%r;dSortDef;%r'%(iCol,
                            dDef,self._dSortDef))
            iSort=dDef.get('iSort',0)
            func=dDef.get('func',None)
            if iSort!=0:
                if iCol==self._iSortCol:
                    iSort=-self._iSort
                #else:
                #    iSort=self._iSort
            if func is None:
                self.SetSort(iCol,iSort,bSort,self._funcSort,
                            **self._kwargsSort)
            else:
                #arg=dDef.get('args',())
                kwargs=dDef.get('kwargs',{})
                
                self.SetSort(iCol,iSort,bSort,func,**kwargs)
        except:
            self.__logTB__()
    def SetSortDef(self,d):
        try:
            self.__logDebug__(d)
            self._dSortDef=d
        except:
            self.__logTB__()
    def SetSort(self,iCol,iSort,bSort=False,func=None,**kwargs):
        try:
            self.__logDebug__('iCol:%d,iSort:%d;bSort:%d'%(iCol,iSort,
                        bSort))
            iOldSort=self._iSort
            iOldCol=self._iSortCol
            self.__logDebug__('iCol:%d;iOldCol:%d;iSort:%d;iOldSort:%d;bSort:%d'%(iCol,iOldCol,
                        iSort,iOldSort,bSort))
            lst=self.wid
            self._iSort=iSort
            self._iSortCol=iCol
            
            self._funcSort=func
            #self._argsSort=args
            self._kwargsSort=kwargs
            if (iOldSort!=self._iSort) or (iOldCol!=self._iSortCol):
                if iOldCol>=0:
                    self.SetColumn(iOldCol,None,iSort=0)
                elif (iOldCol==-1) and (self._iSortCol!=0):
                    self.SetColumn(0,None,iSort=0)
                if iOldCol>=0:
                    self._dSort=None
                if self._iSortCol>=0:
                    self.SetColumn(self._iSortCol,None,iSort=self._iSort)
                else:
                    self.SetColumn(0,None,iSort=self._iSort)
            if bSort:
                self.Sort()
        except:
            self.__logTB__()
    def OnCompare(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=cmp(dA,dB)
        if self._iSort<0:
            return -i
        return i
    def OnCompareFunc(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=self._funcSort(dA,dB,**self._kwargsSort)
        if self._iSort<0:
            return -i
        return i
    def OnCompareCol(self,iA,iB):
        dA=self._dSort.get(iA,'')
        dB=self._dSort.get(iB,'')
        i=cmp(dA,dB)
        if self._iSort<0:
            return -i
        return i
    def OnCompareColFunc(self,iA,iB):
        dA=self._dSort.get(iA,'')
        dB=self._dSort.get(iB,'')
        i=self._funcSort(dA,dB,**self._kwargsSort)
        if self._iSort<0:
            return -i
        return i
    def OnCompareColDatLst(self,iA,iB):
        dA=self.dDat.get(iA,'')
        dB=self.dDat.get(iB,'')
        i=cmp(dA[self._iSortCol],dB[self._iSortCol])
        if self._iSort<0:
            return -i
        return i
    def OnCompareColDatFunc(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=self._funcSort(dA,dB,self._iSortCol,**self._kwargsSort)
        if self._iSort<0:
            return -i
        return i
    def OnCompareColDatFuncOrder(self,iA,iB):
        dA=self.dDat.get(iA,None)
        dB=self.dDat.get(iB,None)
        i=self._funcSort(dA,dB,self._iSortCol,self._iSort,**self._kwargsSort)
        return i
    def Sort(self):
        try:
            self.__logDebug__('iSort:%d;iSortCol:%d'%(self._iSort,self._iSortCol))
            if self._iSort==0:
                self._dSort=None
                return
            lst=self.wid
            iSort=abs(self._iSort)
            if self._iSortCol<0:
                if iSort==1:
                    if self._funcSort is None:
                        lst.SortItems(self.OnCompare)
                    else:
                        lst.SortItems(self.OnCompareFunc)
                elif iSort==2:
                    _dSort={}
                    self._dSort=_dSort
                    if self._funcSort is None:
                        lst.SortItems(self.OnCompareColDatLst)
                    else:
                        lst.SortItems(self.OnCompareColDatFunc)
                elif iSort==3:
                    _dSort={}
                    self._dSort=_dSort
                    if self._funcSort is None:
                        lst.SortItems(self.OnCompareColDatLst)
                    else:
                        lst.SortItems(self.OnCompareColDatFuncOrder)
            else:
                _dSort={}
                if iSort==1:
                    iCnt=lst.GetItemCount()
                    for idx in xrange(iCnt):
                        i=lst.GetItemData(idx)
                        s=lst.GetItem(idx,self._iSortCol).m_text
                        _dSort[i]=s
                    self._dSort=_dSort
                    if self._funcSort is None:
                        lst.SortItems(self.OnCompareCol)
                    else:
                        lst.SortItems(self.OnCompareColFunc)
                elif iSort==2:
                    self._dSort=_dSort
                    if self._funcSort is None:
                        lst.SortItems(self.OnCompareColDatLst)
                    else:
                        lst.SortItems(self.OnCompareColDatFunc)
                elif iSort==3:
                    self._dSort=_dSort
                    if self._funcSort is None:
                        lst.SortItems(self.OnCompareColDatLst)
                    else:
                        lst.SortItems(self.OnCompareColDatFuncOrder)
                self._dSort=None
        except:
            self.__logTB__()
    def SetToolTipString(self,sTool):
        pass
