#----------------------------------------------------------------------
# Name:         vGuiDirBrowser.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121214
# CVS-ID:       $Id: vGuiDirBrowser.py,v 1.4 2015/02/22 11:37:16 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import os
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiDirBrowser(vGuiCoreWX):
    SIZE_DFT=(76,24)
    def __initWid__(self,*args,**kwargs):
        #vGuiCoreWX.__init__(self)
        #self.SetVerbose(__name__)
        
        szCb=kwargs.get('size_button',wx.Size(32,32))
        sz=kwargs.get('size',self.SIZE_DFT)
        bmp=kwargs.get('bmp',None)
        self.bNew=kwargs.get('bNew',False)
        self.bMulti=kwargs.get('bMulti',False)
        self.bPosixFN=kwargs.get('bPosixFN',True)
        self.sMsg=kwargs.get('sMsg',_(u'choose directory'))
        #self.sWildCard=kwargs.get('sWildCard',_(u'xml files|*.xml|all files|*.*'))
        self.sDftDN=kwargs.get('sDftDN',os.path.expanduser(u'~'))
        self.sDN=kwargs.get('sDN',self.sDftDN)
        if bmp is None:
            bmp=self.getBmp('core','Open')
        
        _args,_kwargs=self.GetGuiArgs(kwargs,
                ['id','name','parent','pos','size','style'],
                {'pos':(0,0),'size':(-1,-1),'style':wx.TAB_TRAVERSAL})
        self.wid=wx.Panel(*_args,**_kwargs)
        self.wid.SetAutoLayout(True)
        bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
        
        _args,_kwargs=self.GetGuiArgs({},
                ['id','name','parent','pos','size','style'],
                {
                'pos':(0,0),'size':sz,'style':0,
                'name':'txtDN','parent':self.wid,
                })
        self.txtDN = wx.TextCtrl(*_args,**_kwargs)
        bxs.AddWindow(self.txtDN, 1, border=0, flag=wx.EXPAND|wx.ALL)
        self.txtDN.Bind(wx.EVT_TEXT,self.OnTxtDirNameText)
        self.txtDN.Bind(wx.EVT_TEXT_ENTER,self.OnTxtDirNameEnter)
        
        _args,_kwargs=self.GetGuiArgs({},
                ['id','name','parent','pos','size','style','bitmap'],
                {
                'pos':(0,0),'size':szCb,'style':wx.BU_AUTODRAW,
                'name':'cbBmp','parent':self.wid,
                'bitmap':bmp,
                })
        self.cbBmp=wx.lib.buttons.GenBitmapButton(*_args,**_kwargs)
        self.cbBmp.Bind(wx.EVT_BUTTON,self.OnCbBrowseClick,self.cbBmp)
        bxs.AddWindow(self.cbBmp, 0, border=0, flag=wx.EXPAND|wx.ALL)
        
        self.SetOriginByWid(self.wid)
        self.wid.SetSizer(bxs)
        bxs.Layout()
        bxs.Fit(self.wid)
        return self.wid
    def GetWid(self):
        return self.wid
    def GetWidMod(self):
        return self.txtDN
    def GetValue(self):
        return self.sDN
    def GetDN(self):
        try:
            return self.sDN
        except:
            return None
    def SetValue(self,sDN):
        try:
            if self.bPosixFN==True:
                sDN=sDN.replace('\\','/')
            else:
                sDN=sDN.replace('/','\\')
            self.sDN=sDN
            self.txtDN.SetValue(self.sDN)
        except:
            self.__logTB__()
    def OnTxtDirNameText(self,evt):
        try:
            self.sDN=self.txtDN.GetValue()
        except:
            self.__logTB__()
    def OnTxtDirNameEnter(self,evt):
        try:
            self.sDN=self.txtDN.GetValue()
            self.Post('dir',self.sDN)
        except:
            self.__logTB__()
    def OnCbBrowseClick(self,evt):
        try:
            self.__logDebug__('sDN:%r'%(self.sDN))
            if self.ChooseDir() is not None:
                self.__logDebug__('sDN:%r'%(self.sDN))
                self.txtDN.SetValue(self.sDN)
        except:
            self.__logTB__()
    def ChooseDir(self):
        try:
            self.__logInfo__(''%())
            if self.bNew==True:
                iStyle=wx.DD_DEFAULT_STYLE|wx.DD_NEW_DIR_BUTTON 
            else:
                iStyle=wx.DEFAULT_DIALOG_STYLE|wx.RESIZE_BORDER
            #if self.bMulti==True:
            #    iStyle|=wx.MULTIPLE
            dlg=wx.DirDialog(self.wid,message=self.sMsg,
                #defaultDir='.',
                defaultPath=self.sDftDN,
                #wildcard=self.sWildCard,
                style=iStyle)
            dlg.Centre()
            try:
                if self.sDN is not None:
                    if len(self.sDN)>0:
                        dlg.SetPath(self.sDN)
            except:
                self.__logTB__()
            iRet=dlg.ShowModal()
            if iRet==wx.ID_OK:
                sDN=dlg.GetPath()
                if self.bPosixFN==True:
                    sDN=sDN.replace('\\','/')
                dlg.Destroy()
                if self.sDN!=sDN:
                    self.PostMOD('dirSel',sDN)
                self.sDN=sDN
                self.Post('dir',self.sDN)
                self.PostFB('dirSel',sDN)
                return sDN
            dlg.Destroy()
        except:
            self.__logTB__()
        return None
