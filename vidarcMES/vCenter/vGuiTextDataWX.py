#----------------------------------------------------------------------------
# Name:         vGuiTextDataWX.py
# Purpose:      list data popup button
# Author:       Walter Obweger
#
# Created:      20130121
# CVS-ID:       $Id: vGuiTextDataWX.py,v 1.19 2016/02/23 17:23:00 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import types
    from vGuiTextPopupWX import vGuiTextPopupWX
    #from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiTextDataWX(vGuiTextPopupWX):
    SIZE_DFT=(76,24)
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=-1,
                kwVal={'pos'...},
                ):
    """
    def __initOld__(self,*args,**kwargs):
        kw=kwargs.copy()
        if 'kwVal' not in kw:
            kw['kwVal']={'mode':'txtRd','name':'txtVal',
                'size':self.GetDft('size_val',(60,24),kwargs)}
        if 'szVal' in kwargs:
            kw['kwVal']['size']=kwargs['szVal']
        vGuiTextPopupWX.__init__(self,*args,**kw)
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,iVerbose=-1,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            vGuiTextPopupWX.__initObj__(self,*args,**kwargs)
            #self.lMapVal=kwargs.get('lMapVal',[])
            lTmp=kwargs.get('lMapVal',None)
            if lTmp is not None:
                self.__logErrorAdd__('lMapVal shall be replaced by mapPop',
                    kwargs)
                self.mapPop=lTmp
            self.fctPopGet=kwargs.get('fctPopGet',None)
            self.fctPopSet=kwargs.get('fctPopSet',None)
        except:
            self.__logTB__()
    def __get_widget_lst__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            lW=vGuiTextPopupWX.__get_widget_lst__(self,*args,**kwargs)
            #return lW
            lWid=kwargs.get('lWid',[])
            if bDbg:
                self.__logDebugAdd__('lWid',lWid)
            for sWid,iProp,iBorder,dKw in lWid:
                if sWid=='txt':
                    dKw['evt']={
                        #'txt':              self.OnTextText,
                        'keyDn':            self.OnKeyPressed,
                        }
                    if 'szMin' not in dKw:
                        if 'size' not in dKw:
                            dKw['szMin']=(30,-1)
                # do not add to lW, because base class is going to
                # add widgets given by kwargs anyway.
                # adding event to dictionary does the trick
                #lW.append((sWid,iProp,iBorder,dKw))
            return lW
        except:
            self.__logTB__()
        return []
    def __Clear__(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.IsMainThread()==False:
                return
            for w in self.lWidVal:
                w.SetValue('')
        except:
            self.__logTB__()
    def __SetWidMod__(self,state):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'state':state,'lWidVal':self.lWidVal})
            if self.IsMainThread()==False:
                return
            #self.__setWidModGui__(state,self.txtVal)
            for w in self.lWidVal:
                if bDbg:
                    self.__logDebug__({'state':state,'w':w,'sN':w.GetName()})
                self.__setWidModGui__(state,w)
        except:
            self.__logTB__()
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            #vGuiTextPopupWX.OnPopupOk(self,evt)
            if self.widPop is not None:
                if hasattr(self.widPop,'GetWid'):
                    widPop=self.widPop.GetWid()
                else:
                    widPop=self.widPop
                pn=self.widPop.pn
                lVal=pn.GetValue(sName=self.widVal)
                if self.fctPopGet is not None:
                    if bDbg:
                        self.__logDebugAdd__(self.widVal,lVal)
                    lVal=self.fctPopGet(lVal,self,pn)
                    #lVal=pn.GetValue(sName=None)
                if bDbg:
                    self.__logDebugAdd__(self.widVal,lVal)
                t=type(lVal)
                if t==types.ListType:
                    iOfs=0
                    for sVal in lVal:
                        self.lWidVal[iOfs].SetValue(sVal)
                        iOfs+=1
                    self.SetModified(True)
                elif t==types.DictType:
                    iOfs=0
                    dMod={}
                    bMod=False
                    for sK in self.mapPop:
                        if sK in lVal:
                            sOld=self.lWidVal[iOfs].GetValue()
                            sVal=lVal[sK]
                            self.lWidVal[iOfs].SetValue(sVal)
                            if sOld!=sVal:
                                dMod[sK]=[sOld,sVal]
                                bMod=True
                        iOfs+=1
                    if bMod:
                        self.SetModified(bMod)
                        #self.PostADT('mod',dMod)
                        self.PostNFY('mod',dMod)
                        self.PostMOD('val',lVal)   # 130207 wro:fix me post only if really modified
                self.PostOk('ok',lVal)
            else:
                for w in self.lWidVal:
                    w.SetValue('')
        except:
            self.__logTB__()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
