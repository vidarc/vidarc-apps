#----------------------------------------------------------------------------
# Name:         vGuiCorePanWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20141222
# CVS-ID:       $Id: vGuiCorePanWX.py,v 1.4 2015/07/25 15:58:24 wal Exp $
# Copyright:    (c) 2014 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    from vGuiCoreWidWX import vGuiCoreWidWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiCorePanWX(wx.Panel,vGuiCoreWidWX):
    def __init__(self,*args,**kwargs):
        vGuiCoreWidWX.__init__(self,*args,**kwargs)
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initCreate__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.__initWid__(*args,**kwargs)
            self.SetOriginByWid(self)
            self.__initSizer__(*args,**kwargs)
            self.__initCtrl__(*args,**kwargs)
            self.__initCtrlBt__(*args,**kwargs)
            self.__initPopup__(*args,**kwargs)
            self.__initEvt__(*args,**kwargs)
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        wid=None
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            sz=kwargs.get('size',self.SIZE_DFT)
            _args,_kwargs=self.GetGuiArgs(kwargs,[
                    'id','name','parent','pos','size',
                    'style',
                    ],{
                    'pos':(0,0),'size':(-1,-1),
                    'style':0
                    })
            wx.Panel.__init__(self,*_args,**_kwargs)
            wx.Panel.SetAutoLayout(self,True)
            #self.SetAutoLayout(True)
        except:
            self.__logTB__()
        return self
    def GetWid(self):
        return self
