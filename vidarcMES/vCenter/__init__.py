#----------------------------------------------------------------------------
# Name:         __init__.py
# Purpose:      The presence of this file turns this directory into a
#               Python package.
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: __init__.py,v 1.1 2012/02/12 08:44:15 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import __config__

VERSION=__config__.VERSION
try:
    import imgCtr as imgPlg
    getPluginData=imgPlg.getPluginData
except:
    getPluginData=None

PLUGABLE='vCenterMDIFrame'
PLUGABLE_FRAME=True
DOMAINS=['tools','analysis','vBrowseDir']

__all__=['DOMAINS','VERSION','PLUGABLE','PLUGABLE_FRAME','getPluginData',]

SSL=0
import sys
if hasattr(sys, 'importers'):
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=1
else:
    try:
        from build_options import *
    except:
        VIDARC_IMPORT=0
