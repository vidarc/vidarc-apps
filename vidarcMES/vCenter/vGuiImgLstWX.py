#----------------------------------------------------------------------------
# Name:         vGuiImgLstWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20131024
# CVS-ID:       $Id: vGuiImgLstWX.py,v 1.7 2015/05/14 09:15:19 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import types
    #import img_Core as img_Core
    import vGuiArt as vGuiArt
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

gdImgLst={}

def vGuiImgLstGet(sName,which=0,dft=None,lDef=[],**kw):
    try:
        global gdImgLst
        if sName in gdImgLst:
            return gdImgLst[sName],False
        else:
            w=vGuiImgLstWX(which=which,dft=dft,lDef=lDef)
            gdImgLst[sName]=w
            return w,True
    except:
        vpc.logTB(__name__)
    return None,False

class vGuiImgLstWX:
    def __init__(self,which=0,dft='list',lDef=[],**kw):
        if (which is None) or (which==wx.IMAGE_LIST_SMALL):
            imgLst=wx.ImageList(16,16)
        elif which==0:
            imgLst=wx.ImageList(16,16)
        else:
            imgLst=wx.ImageList(16,16)
            which=None
        self._dImg={}
        self._imgLst=imgLst
        if dft=='list':
            self.CreateDftList()
        elif dft=='tree':
            self.CreateDftTree()
        self.AddImageList(lDef,bForce=False)
    def GetObjs(self):
        return self._dImg,self._imgLst
    def AddImageList(self,lDef,bForce=False):
        try:
            if lDef is None:
                return
            dImg=self._dImg
            imgLst=self._imgLst
            for sK,it in lDef:
                if bForce==False:
                    if sK in dImg:
                        continue
                t=type(it)
                def getImg(bmp):
                    if type(bmp)==types.StringType:
                        iOfs=bmp.find('imgCore.get')
                        if iOfs==0:
                            return vGuiArt.getBmp('core',bmp[11:-8])
                        iOfs=bmp.find('.')
                        if iOfs==-1:
                            return vGuiArt.getBmp('core',bmp)
                        return eval(bmp)
                    else:
                        return bmp
                if t==types.TupleType:
                    lImg=[imgLst.Add(getImg(bmp)) for bmp in it]
                elif t==types.ListType:
                    lImg=[imgLst.Add(getImg(bmp)) for bmp in it]
                else:
                    lImg=imgLst.Add(getImg(it))
                dImg[sK]=lImg
        except:
            vpc.logTB(__name__)
    def CreateDftList(self):
        try:
            self.AddImageList([
                    ('empty',       vGuiArt.getBmp('core','Invisible')),
                    ('asc',         vGuiArt.getBmp('core','UpSml')),
                    ('desc',        vGuiArt.getBmp('core','DnSml')),
                    ])
        except:
            vpc.logTB(__name__)
    def CreateDftTree(self):
        try:
            self.AddImageList([
                    ('empty',       vGuiArt.getBmp('core','Invisible')),
                    #('asc',         vGuiArt.getBmp('core','UpSml')),
                    #('desc',        vGuiArt.getBmp('core','DnSml')),
                    ])
        except:
            vpc.logTB(__name__)
    def GetImg(self,sTag,sFB):
        try:
            if self._dImg is not None:
                iImg=self._dImg.get(sTag,None)
                if iImg is None:
                    iImg=self._dImg.get(sFB,-1)
                return iImg
        except:
            vpc.logTB(__name__)
        return -1
