#----------------------------------------------------------------------------
# Name:         vGuiArt.py
#               inhereted from vtArt.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20150115
# CVS-ID:       $Id: vGuiArt.py,v 1.4 2015/05/12 15:08:19 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from wx import NullBitmap,NullImage,EmptyIcon,EmptyBitmap,EmptyImage
    
    import fnmatch
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiArtProvider:
    def __init__(self):
        self.bmpNull=NullBitmap
        self.imgNull=NullImage
        self.dCache={}
        self.dCacheImg={}
        self.dMod={}
        self.dImg={}
        self.setUp()
    def setUp(self):
        try:
            import imgCore as imgCore
            self.AddMod('core',imgCore)
        except:
            vpc.logTB(__name__)
        try:
            import imgCtr as imgCtr
            self.AddMod('ctr',imgCtr)
        except:
            vpc.logTB(__name__)
        try:
            import imgOpen as imgOpen
            self.AddMod('open',imgOpen)
        except:
            vpc.logTB(__name__)
        try:
            import imgStat as imgStat
            self.AddMod('stat',imgStat)
        except:
            vpc.logTB(__name__)
        try:
            import imgDat as imgDat
            self.AddMod('dat',imgDat)
        except:
            vpc.logTB(__name__)
        try:
            import imgCmd as imgCmd
            self.AddMod('cmd',imgCmd)
        except:
            vpc.logTB(__name__)
        try:
            import imgNet as imgDat
            self.AddMod('net',imgDat)
        except:
            vpc.logTB(__name__)
    def addMod(self,sK,mod):
        self.dMod[sK]=mod
    def AddMod(self,sK,mod):
        try:
            self.addMod(sK,mod)
            self.addBitmaps(sK)
        except:
            vpc.logTB(__name__)
    def __getImgModByName__(self,sClt,iRec=5):
        if sClt in self.dMod:
            m=self.dMod[sClt]
            if type(m)==type(''):
                if iRec>0:
                    return self.__getImgModByName__(m,iRec=iRec-1)
                else:
                    return None
            return self.dMod[sClt]
        else:
            return None
        if sClt in ['core']:
            return imgCore
        if sClt in ['ctr']:
            return imgCtr
        if sClt in ['open']:
            return imgOpen
        if sClt in ['stat']:
            return imgStat
        if sClt in ['cmd']:
            return imgCmd
        return None
    def addBitmaps(self,sClt):
        m=self.__getImgModByName__(sClt)
        if m is None:
            return
        lBitmaps=dir(m)
        
        if sClt in self.dImg:
            dBmp=self.dImg[sClt]
        else:
            dBmp={None:[]}
            self.dImg[sClt]=dBmp
        lName=dBmp[None]
        for sN in lBitmaps:
            if fnmatch.fnmatch(sN,'get*Bitmap'):
                sBmp=sN[3:-6]
                if sBmp not in dBmp:
                    lName.append(sBmp)
                    dBmp[sBmp]=sN
                    if VERBOSE>20:
                        print '',sBmp
                        vpc.logDebug('addBitmaps %s %s'%(sBmp,sN),__name__)
        lName.sort()
        vpc.logDebug(lName,__name__)
        vpc.logDebug(dBmp,__name__)
        vpc.logDebug(self.dImg,__name__)
    def GetBitmap(self,sClt,sArt,sz=(16,16)):
        if sClt not in self.dImg:
            return self.bmpNull
        sArtId='.'.join([sClt,sArt])
        if sArtId in self.dCache:
            return self.dCache[sArtId]
        else:
            img=self.CreateBitmap(sClt,sArt,sz)
            self.dCache[sArtId]=img
            return img
    def CreateBitmap(self,sClt,sArt,sz):
        try:
            m=self.__getImgModByName__(sClt)
            if m is not None:
                bmp=getattr(m,''.join(['get',sArt,'Bitmap']))()
                return bmp
        except:
            return EmptyBitmap(sz[0],sz[1])
        return self.bmpNull
    def GetImage(self,sClt,sArt,sz=(16,16)):
        if sClt not in self.dImg:
            return self.bmpNull
        sArtId='.'.join([sClt,sArt])
        if sArtId in self.dCacheImg:
            return self.dCacheImg[sArtId]
        else:
            img=self.CreateImage(sClt,sArt,sz)
            self.dCacheImg[sArtId]=img
            return img
    def CreateImage(self,sClt,sArt,sz):
        try:
            m=self.__getImgModByName__(sClt)
            if m is not None:
                img=getattr(m,''.join(['get',sArt,'Image']))()
                return img
        except:
            return EmptyImage(sz[0],sz[1])
        return self.imgNull

__artProv=None
__bInstalled=False

def Install(oProvider=None):
    if VERBOSE>0:
        vpc.logDebug('register oProvider;%o'%(oProvider),__name__)
    global __bInstalled
    if __bInstalled:
        return
    __bInstalled=True
    global __artProv
    if oProvider is None:
        __artProv=vGuiArtProvider()
    else:
        __artProv=oProvider
def DeInstall():
    global __bInstalled
    if __bInstalled==False:
        return
    __bInstalled=False
    global __artProv
    __artProv=None
def GetProv(sN,mod):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv
def AddMod(sN,mod):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    __artProv.AddMod(sN,mod)
def GetBmp(sN,sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    iOfs=sN.find('imgCore.get')
    if iOfs==0:
        sClt,sArt='core',sN[11:-8]
        return __artProv.GetBitmap(sClt,sArt,sz)
    iOfs=sN.find('.')
    if iOfs==-1:
        sClt,sArt='core',sN
        return __artProv.GetBitmap(sClt,sArt,sz)
    else:
        sClt,sArt=sN[:iOfs],sN[iOfs+1:]
    return __artProv.GetBitmap(sClt,sArt,sz)
def getBmp(sClt,sArt,sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv.GetBitmap(sClt,sArt,sz)
def getImg(sClt,sArt,sz=(16,16)):
    global __bInstalled
    if __bInstalled:
        pass
    else:
        Install()
    global __artProv
    return __artProv.GetImage(sClt,sArt,sz)
def getIcn(sClt,sArt,sz=(16,16)):
    bmp=getBmp(sClt,sArt,sz)
    icon = EmptyIcon()
    icon.CopyFromBitmap(bmp)
    return icon
def getIcon(bmp):
    icon = EmptyIcon()
    icon.CopyFromBitmap(bmp)
    return icon

#Install()
