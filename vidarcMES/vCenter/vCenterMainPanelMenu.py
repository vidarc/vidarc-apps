#----------------------------------------------------------------------------
# Name:         vCenterMainPanelMenu.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20150726
# CVS-ID:       $Id: vCenterMainPanelMenu.py,v 1.1 2015/08/03 07:24:39 wal Exp $
# Copyright:    (c) 2015 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    
    VERBOSE=vpc.getVerboseType(__name__)
    vpc.logDebug('VERBOSE:%d'%(VERBOSE),__name__)
except:
    vpc.logTB(__name__)

class vCenterMainPanelMenu:
    def MenuOpenFile(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            sMsg=_(u'Choose vCenter data file.')
            sFN=self.ChooseFile('vCenter.xml',sMsg=sMsg,
                    sDftFN='vCenter.xml')
            if sFN is None:
                return
            self.__logInfo__({'sFN':sFN})
            self.OpenFile(sFN)
        except:
            self.__logTB__()
