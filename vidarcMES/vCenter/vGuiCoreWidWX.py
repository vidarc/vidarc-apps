#----------------------------------------------------------------------------
# Name:         vGuiCoreWidWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20141222
# CVS-ID:       $Id: vGuiCoreWidWX.py,v 1.16 2016/02/23 17:21:55 wal Exp $
# Copyright:    (c) 2014 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import types
    import wx
    #import imgCore
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    #vGuiArt.Install()
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiCoreWidWX(vGuiCoreWX):
    SIZE_DFT=(-1,-1)
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            self.wid=None
            self.widBt=None
            self.bAnchor=kwargs.get('bAnchor',False)
            self.iArrange=kwargs.get('iArrange',0x40)
            self.widPop=None
            self.bBusy=False
            #self.lMapVal=kwargs.get('lMapVal',[])
            self.mapVal=kwargs.get('mapVal',[])
            self.mapPop=kwargs.get('mapPop',[])
            self.fctPopGet=kwargs.get('fctPopGet',None)
            self.fctPopSet=kwargs.get('fctPopSet',None)
            self.__defProp__(
                'iWidValStyle',None,sDoc='valid values;'\
                        '0...str;'\
                        '1...lst;'\
                        '2...dict;')
            self.iOfsWid=0
            self.__logDebug__(self._dProp)
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        wid=None
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            szCb=kwargs.get('size_button',wx.Size(24,24))
            sz=kwargs.get('size',self.SIZE_DFT)
            _args,_kwargs=self.GetGuiArgs(kwargs,[
                    'id','name','parent','pos','size',
                    'style',
                    ],{
                    'pos':(0,0),'size':(-1,-1),
                    'style':wx.TAB_TRAVERSAL
                    })
            self.wid=wx.Panel(*_args,**_kwargs)
            self.wid.SetAutoLayout(True)
        except:
            self.__logTB__()
        return self.wid
    def __initSizer__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            w=self.GetWid()
            if w is None:
                self.__logError__('you are not supposed to be here')
                return
            bxs = wx.BoxSizer(orient=wx.HORIZONTAL)
            w.SetSizer(bxs)
            self.bxs=bxs
        except:
            self.__logTB__()
    def __initLayout__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            w=self.GetWid()
            if w is None:
                self.__logError__('you are not supposed to be here')
                return
            self.bxs.Layout()
            self.bxs.Fit(w)
        except:
            self.__logTB__()
    def __initCtrlBt__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            szCb=kwargs.get('size_button',wx.Size(24,24))
            if szCb is None:
                return
            bmp=kwargs.get('bmp',None)
            id=self.GetCtrlId('cbPopup')
            #cls,_kw,flag=self.__getClsDftArgs__('tgBmp')
            dW=self.__getClsDftArgs__('tgBmp')
            cls,_kw,flag=dW['CLS'],dW['kw'],dW['flag']
            self.__logDebug__(dW)
            
            w=self.GetWid()
            if w is None:
                self.__logError__('you are not supposed to be here')
                return
            _args,_kwargs=self.GetGuiArgs(_kw,
                    ['id','name','parent','pos','size','style','bitmap'],
                    {'id':id,'parent':w,'name':u'cbPopup',
                    'pos':(0,0),'size':szCb,'style':wx.BU_AUTODRAW,
                    'bitmap':bmp or self.getBmp('core','Down'),
                    })
            self.__logDebugAdd__(_kw,_kwargs)
            #if bmp is not None:
            #    _kwargs['bitmap']=bmp# or imgCore.getDownBitmap()
            if bDbg:
                self.__logDebugAdd__(_kw,_kwargs)
                self.__logDebug__({'id':id,'_kwargs':_kwargs,
                        'CLS':cls})
            self.widBt=cls(*_args,**_kwargs)
            self.bxs.AddWindow(self.widBt, 0, border=0, flag=flag)
            #self.widBt.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.widBt)
            self.BindEventWid(self.widBt,'btn',self.OnPopupButton)
        except:
            self.__logTB__()
    def OnPopupButton(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(''%())
            w=self.GetWid()
            if self.widPop is not None:
                if self.widBt.GetValue()==True:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    if self.iArrange>=0:
                        vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                    self.widPop.Show(True)
                else:
                    self.widPop.Show(False)
            else:
                self.__logError__('no popup widget set yet'%())
                #vtGuiCoreArrangeWidget(self,self,iArrange)
        except:
            self.__logTB__()
    def __initPopup__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.widBt is None:
                return
            wid=kwargs.get('wid',None)
            if self.bAnchor==True:
                par=kwargs.get('parent',None)
                par=self.GetAnchor(par)
                if par is not None:
                    #par.Bind(wx.EVT_MOVE,self.OnMoveWidArrange)
                    self.BindEventWid(par,'move',self.OnMoveWidArrange)
                    if self.GetVerboseDbg(10):
                        self.__logDebug__({'par':par,
                                'name':par.GetName()})
            else:
                self.BindEventWid(self.widBt,'move',self.OnMoveWidArrange)
                if self.GetVerboseDbg(10):
                    self.__logDebug__({'widBt':self.widBt,
                                'name':self.widBt.GetName()})
            self.widBt.Enable(False)
            #wx.EVT_LEFT_UP(self, self.OnLeftDown)
            #wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
            if wid is not None:
                self.SetWidPopup(wid)
            else:
                self.__setUpPopUp__(*args,**kwargs)
        except:
            self.__logTB__()
    def SetWidPopup(self,wid,widVal=None):
        """
        """
        try:
            if self.IsMainThread()==False:
                return
            self.widPop=wid
            w=self.GetWid()
            self.widBt.Enable(True)
            self.bBusy=True
            if widVal is not None:
                self.widVal=widVal
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                if hasattr(self.widPop,'BindEvent'):
                    self.widPop.BindEvent('ok',self.OnPopupOk)
                    self.widPop.BindEvent('cancel',self.OnPopupCancel)
        except:
            self.__logTB__()
    def GetWidPopup(self):
        return self.widPop
    def __setUpPopUp__(self,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            if 'popup' in kwargs:
                from vGuiFrmWX import vGuiFrmWX
                _kw=kwargs['popup'].copy()
                if bDbg:
                    self.__logDebug__(_kw)
                _args,_kwargs=self.GetGuiArgs(_kw,
                        ['name','bFlexGridSizer','kind',
                        'lGrowRow','lGrowCol','lWid',
                        'iArrange','bAnchor','widArrange',
                        'parent','pos','size',
                        'VERBOSE'],
                        {'name':'frmPop','pos':(200,200),'size':None,#'size':(150,100),
                        'bFlexGridSizer':0,'kind':'popup',
                        'bAnchor':0,'iArrange':0,'widArrange':None,
                        'lGrowRow':[],#[(0,1)],
                        'lGrowCol':[(1,1)],
                        'parent':self,
                        'VERBOSE':-1,
                        },
                        bValidate=False)
                if bDbg:
                    self.__logDebug__({'_args':_args,'_kwargs':_kwargs})
                frm=vGuiFrmWX(*_args,**_kwargs)
                self.SetWidPopup(frm)
                if 'popupEvt' in kwargs:
                    d=kwargs['popupEvt']
                    for sK,sFct in d.iteritems():
                        frm.BindEvent(sK,sFct)
                    #if 'cmd' in 
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            self.__logDebug__(''%())
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            if self.widPop is not None:
                if hasattr(self.widPop,'GetWid'):
                    widPop=self.widPop.GetWid()
                else:
                    widPop=self.widPop
                widPop.Show(False)
                if hasattr(widPop,'CallWidChild'):
                    widPop.CallWidChild(-1,'__Close__')
            if self.widBt is not None:
                self.widBt.SetValue(False)
        except:
            self.__logTB__()
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #self.Post('ok')
            self.__Close__()
        except:
            self.__logTB__()
    def OnPopupCancel(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            #self.Post('cancel')
            self.__Close__()
        except:
            self.__logTB__()
    def OnMoveWidArrange(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerboseDbg(20)
            w=self.GetWid()
            if bDbg:
                self.__logDebug__({'w':w,'widPop':self.widPop,
                    'iArrange':self.iArrange})
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    if bDbg:
                        self.__logDebugAdd__('before',{
                                'w':w.GetSize(),
                                'pnMain':self.widPop.pnMain.GetSize(),
                                'widPop':widPop.GetSize(),
                                'iArrange':self.iArrange})
                    vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                    if bDbg:
                        self.__logDebugAdd__('arranged',{
                                'w':w.GetSize(),
                                'pnMain':self.widPop.pnMain.GetSize(),
                                'widPop':widPop.GetSize(),
                                'iArrange':self.iArrange})
                    if 0:
                        w=self.widPop.pn
                        w.Layout()
                        w.bxsData.Layout()
                        #print 'pn',w
                    if 1:
                        w=self.widPop.pnMain
                        w.Layout()
                        #w.bxsData.Layout()
                        #print 'ma',w
                    if bDbg:
                        self.__logDebugAdd__('after',{
                                'w':w.GetSize(),
                                'pnMain':self.widPop.pnMain.GetSize(),
                                'widPop':widPop.GetSize(),
                                'iArrange':self.iArrange})
        except:
            self.__logTB__()
    def OnSizeWidArrange(self,evt):
        try:
            if evt is not None:
                evt.Skip()
            bDbg=self.GetVerboseDbg(0)
            w=self.GetWid()
            if bDbg:
                self.__logDebug__({'w':w,'widPop':self.widPop,
                    'iArrange':self.iArrange})
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
        except:
            self.__logTB__()
    def __getClsDftArgs__(self,cls):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
                self.__logDebug__('cls:%s'%(cls))
        if cls=='txt':return {
            'cls':  'txt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TAB_TRAVERSAL,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='Txt':return {
            'cls':  'Txt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TAB_TRAVERSAL,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.LEFT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txT':return {
            'cls':  'txT',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TAB_TRAVERSAL,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtLn':return {
            'cls':  'txtLn',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_MULTILINE,'size':(40,24),
                    'value':'',
                    },
            'flag': wx.EXPAND,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='TxtLn':return {
            'cls':  'TxtLn',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_MULTILINE,'size':(40,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.LEFT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txTLn':return {
            'cls':  'txTLn',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_MULTILINE,'size':(40,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRd':return {
            'cls':  'txtRd',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='TxtRd':return {
            'cls':  'TxtRd',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.LEFT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txTRd':return {
            'cls':  'txTRd',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRdWrp':return {
            'cls':  'txtRdWrp',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.TE_WORDWRAP,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='TxtRdWrp':return {
            'cls':  'TxtRdWrp',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.TE_WORDWRAP,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.LEFT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txTRdWrp':return {
            'cls':  'txTRdWrp',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.TE_WORDWRAP,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txtRdRg':return {
            'cls':  'txtRdRg',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='TxtRdRg':return {
            'cls':  'TxtRdRg',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND|wx.LEFT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='txTRdRg':return {
            'cls':  'txTRdRg',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TE_READONLY|wx.ALIGN_RIGHT,'size':(20,24),
                    'value':'',
                    },
            'flag'  :wx.EXPAND|wx.RIGHT,
            'szMin':(100,-1),               # 20151209 wro:wx3.0 port
            }
        if cls=='int':return {
            'cls':  'int',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'size':(42,24),
                    'style':wx.ALIGN_RIGHT,
                    'value':'',
                    },
            'evt':{
                    'char':self.OnCharInt,
                    },
            'flag': wx.EXPAND|wx.RIGHT,
            'szMin':(30,-1),
            }
        if cls=='tg':return {
            'cls':  'tg',
            'CLS':  wx.ToggleButton,
            'key':  ['id','name','parent','pos','size','style',
                    'label'],
            'kw':   {
                    'size':(12,24),
                    },
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'flag': wx.ALIGN_CENTER,#|wx.RIGHT,
            'silent':True,
            'szMin':(4,4),                # 20151209 wro:wx3.0 port
            }
        if cls=='tgLbl':return {
            'cls':  'tgLbl',
            'CLS':  wx.ToggleButton,
            'key':  ['id','name','parent','pos','size','style',
                    'label'],
            'kw':   {
                    'size':(12,24),
                    },
            'evtyp':{
                    'btn':wx.EVT_BUTTON,
                    },
            'flag': wx.ALIGN_CENTER,#|wx.RIGHT,
            'silent':True,
            'szMin':(80,4),                # 20151209 wro:wx3.0 port
            }
        if cls=='tgBmp':return {
            'cls':  'tgBmp',
            'CLS':  wx.lib.buttons.GenBitmapToggleButton,
            'key':  ['id','name','parent','pos','size','style','bitmap'],
            'kw':   {
                    'style':wx.BU_AUTODRAW,
                    'size':(24,24),
                    },
            'fct':  {
                    None:['bitmapLbl','bitmapSel','bitmapDis','bitmapFcs',],
                    'bitmapLbl':'SetBitmapLabel',
                    'bitmapSel':'SetBitmapSelected',
                    'bitmapDis':'GetBitmapDisabled',
                    'bitmapFcs':'GetBitmapFocus',
                    },
            'flag': wx.ALIGN_CENTER,#|wx.RIGHT,
            'silent':True,
            'szMin':(4,4),                # 20151209 wro:wx3.0 port
            }
        if cls=='cbLbl':return {
            'cls':  'cbLbl',
            'CLS':   wx.Button,
            'key':  ['id','name','parent','pos','size','style',
                    'label',],
            'kw':   {
                    'style':0,#wx.SUNKEN_BORDER,
                    'size':(24,24),
                },#{'bitmap':wx.EmptyButton(16,16)},
            #'evtyp':{
            #        'btn':wx.EVT_BUTTON,
            #        },
            'flag': wx.ALIGN_CENTER,
            'silent':True,
            'szMin':(80,4),                # 20151209 wro:wx3.0 port
            }
        if cls=='cbBmp':return {
            'cls':  'cbBmp',
            'CLS':  wx.lib.buttons.GenBitmapButton,
            'key':  ['id','name','parent','pos','size','style','bitmap'],
            'kw':   {
                    'style':wx.SUNKEN_BORDER,
                    'size':(24,24),
                    },
            'flag': wx.ALIGN_CENTER,#|wx.RIGHT,
            'silent':True,
            'szMin':(4,4),                # 20151209 wro:wx3.0 port
            }
        return {
            'cls':  'txt',
            'CLS':  wx.TextCtrl,
            'key':  ['id','name','parent','pos','size','style','value'],
            'kw':   {
                    'style':wx.TAB_TRAVERSAL,'size':(20,24),
                    'value':'',
                    },
            'flag': wx.EXPAND,
            'szMin':(30,-1),                # 20151209 wro:wx3.0 port
            }
    def OnCharInt(self,evt):
        try:
            key=self.GetKeyCode(evt)
            dKeyMod=self.GetKeyModifiers(evt)
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebugAdd__('key:0x%03x %d'%(key,key),dKeyMod)
            bOk=False
            sAbbr=dKeyMod.get('abbr','')
            if sAbbr=='num':
                bOk=True
            if sAbbr in ['lf','rg']:
                bOk=True
            if sAbbr in ['dn','up','pgdn','pgup','-','+']:
                try:
                    w=evt.GetEventObject()
                    v=int(w.GetValue())
                    if sAbbr=='dn':
                        v=v-1
                    if sAbbr=='up':
                        v=v+1
                    if sAbbr=='pgdn':
                        v=v-10
                    if sAbbr=='pgup':
                        v=v+10
                    if sAbbr=='-':
                        v=-abs(v)
                    if sAbbr=='+':
                        v=abs(v)
                    w.SetValue(str(v))
                    return
                except:
                    pass
            
            sKind=dKeyMod.get('kind','')
            if sKind=='edit':
                bOk=True
            if sKind=='ctrl':
                bOk=True
            if bOk==True:
                evt.Skip()
        except:
            self.__logTB__()
    def __get_widget_lst__(self,*args,**kwargs):
        lW=[]
        return lW
    def addWid(self,lW,sWid,p=0,s=4,kw={}):
        try:
            t=(sWid,p,s,kw)
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__(t)
            lW.append(t)
        except:
            self.__logTB__()
    def __addWidget__(self,sWid,iProp,iBorder,kkw,bDbg=False):
        try:
            if bDbg:
                self.__logDebug__('iOfsWid:%d'%(self.iOfsWid))
            if sWid is None:
                if type(iBorder)==types.TupleType:
                    self.AddSzSpacer(iProp,iBorder)
                else:
                    self.AddSzSpacer(iProp,(iBorder,1))
                return 0
            dW=self.__getClsDftArgs__(sWid)
            if bDbg:
                self.__logDebug__(dW)
            cls,lKey,_kw,flag=dW['CLS'],dW['key'],dW['kw'],dW['flag']
            self.__logDebug__('%r'%(dW))
            
            _kw.update(kkw)
            _kw['parent']=self.GetWid()
            sNameWid='widDat%02d'%(self.iOfsWid)
            _args,_kwargs=self.GetGuiArgs(_kw,lKey,{
                    'pos':(0,0),'size':(-1,-1),
                    'style':0,
                    'name':sNameWid,
                    })
            for sK in ['bitmap']:
                if sK in _kwargs:
                    sN=_kwargs[sK]
                    t=type(sN)
                    _kwargs[sK]=self.GetBmp(sN)
            if bDbg:
                self.__logDebugAdd__(sNameWid,
                        {'_args':_args,'_kwargs':_kwargs})
            w = cls(*_args,**_kwargs)
            sFlag=kkw.get('szFlag',' ')
            if type(iProp)==types.TupleType:
                self.InsSz(iProp[0],w,iProp[1],border=iBorder,
                        flag=flag,sFlag=sFlag)
            else:
                self.AddSz(w,iProp,border=iBorder,
                        flag=flag,sFlag=sFlag)
            sN=_kw.get('name',None)
            if sN is not None:
                self.__dict__[sN]=w
            if 'silent' in kkw:
                bSilent=kkw.get('silent',False)
                if bSilent==False:
                    self.lWidVal.append(w)
            else:
                bSilent=dW.get('silent',False)
                if bSilent==False:
                    self.lWidVal.append(w)
            if 'szMin' in _kw:           # 20160223 wro:min size by kwargs
                tSzMin=kkw['szMin']
            else:
                if 'size' in _kw:
                    tSzMin=_kw['size']
                else:
                    if 'szMin' in dW:
                        tSzMin=dW['szMin']
                    else:
                        tSzMin=None
            if 'fct' in dW:
                dFct=dW.get('fct')
                for sK in dFct[None]:
                    if sK in kkw:
                        try:
                            if bDbg:
                                self.__logDebug__('sK:%s'%(sK))
                            f=getattr(w,dFct[sK])
                            if sK.startswith('bitmap'):
                                a=self.GetBmp(kkw[sK])
                                f(a)
                            else:
                                v=kkw[sK]
                                t=type(v)
                                if t==types.TupleType:
                                    f(*v)
                                elif t==types.ListType:
                                    f(*v[0],**v[1])
                                else:
                                    f(v)
                        except:
                            self.__logTB__()
            try:
                #if 'szMin'in dW:
                #    w.SetMinSize(dW.get('szMin',(-1,-1)))
                if tSzMin is not None:
                    w.SetMinSize(tSzMin)
            except:
                self.__logTB__()
            if 'evt' in dW:
                self.BindEventDict(w,dW.get('evt',None))
            if 'evt' in kkw:
                self.BindEventDict(w,kkw.get('evt',None))
            return w
        except:
            self.__logTB__()
            return -1
    def __add_widget__(self,lWid,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebugAdd__('lWid',lWid)
            self.lWidVal=[]
            for sWid,iProp,iBorder,kkw in lWid:
                self.__addWidget__(sWid,iProp,iBorder,kkw,bDbg=bDbg)
                self.iOfsWid+=1
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            
            lWid=self.__get_widget_lst__(*args,**kwargs)
        except:
            self.__logTB__()
        try:
            lW=kwargs.get('lWid',None)
            if bDbg:
                self.__logDebug__(lW)
            if lW is not None:
                lWid+=lW
        except:
            self.__logTB__()
        try:
            self.__add_widget__(lWid)
            iTmp=kwargs.get('iWidValStyle',None)
            if iTmp is not None:
                self.iWidValStyle=iTmp
            else:
                if self.iWidValStyle is None:
                    iLen=self.lWidVal
                    if iLen==0:
                        self.iWidValStyle=-1
                    if iLen==1:
                        self.iWidValStyle=0
                    else:
                        self.iWidValStyle=1
        except:
            self.__logTB__()
    def SetValueLst(self,lVal):
        try:
            if self.IsMainThread()==False:
                return
            iOfs=0
            iCnt=len(self.lWidVal)
            for sVal in lVal:
                if sVal is not None:
                    if iOfs<iCnt:
                        try:
                            wid=self.lWidVal[iOfs]
                            if wid is not None:
                                wid.SetValue(sVal)
                        except:
                            self.__logTB__()
                            self.__logError__({'iOfs':iOfs,'iCnt':iCnt,
                                    'sVal':sVal,'wid':wid})
                    else:
                        break
                iOfs+=1
        except:
            self.__logTB__()
    def SetValueDict(self,dVal):
        try:
            if self.IsMainThread()==False:
                return
            iOfs=0
            iCnt=len(self.lWidVal)
            for sK in self.mapVal:
                if sK in dVal:
                    sVal=dVal[sK]
                    if iOfs<iCnt:
                        try:
                            wid=self.lWidVal[iOfs]
                            if wid is not None:
                                wid.SetValue(sVal)
                        except:
                            self.__logTB__()
                            self.__logError__({'iOfs':iOfs,'iCnt':iCnt,
                                    'sVal':sVal,'wid':wid,'sK':sK})
                    else:
                        break
                iOfs+=1
        except:
            self.__logTB__()
    def SetValue(self,val):
        try:
            if self.IsMainThread()==False:
                return 
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__(val)
            iOfs=0
            t=type(val)
            if t==types.ListType:
                self.SetValueLst(val)
            elif t==types.DictType:
                #self.__logError__('dict not implemented yet.')
                self.SetValueDict(val)
                return
            else:
                val=[val]
            self.SetValueLst(val)
        except:
            self.__logTB__()
    def GetValueLst(self):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            
            lTmp=[]
            iOfs=0
            for w in self.lWidVal:
                try:
                    v=w.GetValue()
                except:
                    self.__logTB__()
                    self.__logError__({'iOfs':iOfs,'w':w})
                    v=None
                lTmp.append(v)
                iOfs+=1
            if bDbg:
                self.__logDebug__(lTmp)
            return lTmp
        except:
            self.__logTB__()
    def GetValueDict(self):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            dTmp={}
            iOfs=0
            for w in self.lWidVal:
                try:
                    n=self.mapVal[iOfs]
                except:
                    self.__logTB__()
                    self.__logError__({'iOfs':iOfs,'w':w})
                    n=w.GetName()
                    self.mapVal.append(n)
                try:
                    v=w.GetValue()
                    dTmp[n]=v
                except:
                    self.__logTB__()
                    self.__logError__({'iOfs':iOfs,'w':w})
                iOfs+=1
            if bDbg:
                self.__logDebug__(dTmp)
            return dTmp
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            if self.IsMainThread()==False:
                return None
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'iWidValStyle':self.iWidValStyle,
                        'mapVal':self.mapVal,'mapPop':self.mapPop})
            if self.iWidValStyle==0:
                v=self.lWidVal[0].GetValue()
                return v
            if self.iWidValStyle==1:
                return self.GetValueLst()
            if self.iWidValStyle==2:
                return self.GetValueDict()
        except:
            self.__logTB__()
        return ''
