#----------------------------------------------------------------------------
# Name:         vDatCsv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20130418
# CVS-ID:       $Id: vDatCsv.py,v 1.3 2016/02/07 06:26:02 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import os
    import codecs
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vDatCsv(vpc.vcOrg):
    def __init__(self,sOrigin=None,sName=None,iVerbose=-1):
        if sOrigin is None:
            sOrigin='vDatCsv'
        if sName is None:
            sName=__name__
        vpc.vcOrg.__init__(self)
        self.SetOrigin(sOrigin,sName)
        self.fCsv=None
        self.sSep=';'
        self.SetVerbose(sName,iVerbose)
        self.__clr__()
    def SetVerbose(self,sName,iVerbose=-1,kwargs={}):
        VERBOSE=vpc.getVerboseType(sName)
        if VERBOSE>0:
            self._iVerbose=VERBOSE
        else:
            self._iVerbose=-1
        iTmpC=vpc.getVerboseType(self.__class__.__name__)
        if iTmpC>0:
            self._iVerbose=max(iTmpC,self._iVerbose)
        if iVerbose>0:
            self._iVerbose=max(iVerbose,self._iVerbose)
        if kwargs.get('VERBOSE',0)>0:
            self._iVerbose=max(kwargs.get('VERBOSE',0),self._iVerbose)
    def GetVerbose(self,iVerbose=-1):
        if iVerbose>=0:
            if self._iVerbose>=iVerbose:
                return True
            else:
                return False
        else:
            return self._iVerbose
    def GetVerboseDbg(self,iVerbose=-1):
        if self.GetVerbose(iVerbose=iVerbose):
            if self.__isLogDebug__():
                return True
            else:
                return False
        else:
            return False
    def __clr__(self):
        self.dDat={}
        self.lDat={}
        self.lHdr=[]
    def Open(self,sFN,sEnc='utf-8',sSep=';',sMode='r'):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if sSep is not None:
                self.sSep=sSep
            self.fCsv=codecs.open(sFN,sMode,encoding=sEnc,errors='ignore')
        except:
            self.__logTB__()
            return -1
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
        return 1
    def Close(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.fCsv is not None:
                self.fCsv.close()
                self.fCsv=None
                return 1
        except:
            self.__logTB__()
            return -1
        return 0
    def WriteLst(self,lDat):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            for l in lDat:
                s=self.sSep.join(l)
                self.fCsv.write(s)
                self.fCsv.write(os.linesep)
        except:
            self.__logTB__()
    def WriteFct(self,fct,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            iRet=1
            while iRet>0:
                iRet,l=fct(*args,**kwargs)
                s=self.sSep.join(l)
                self.fCsv.write(s)
                self.fCsv.write(os.linesep)
                
        except:
            self.__logTB__()
