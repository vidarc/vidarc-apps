#----------------------------------------------------------------------------
# Name:         vCenterTree.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20131030
# CVS-ID:       $Id: vCenterTree.py,v 1.5 2016/02/07 06:36:05 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiTreeXmlWX import vGuiTreeXmlWX
    from vGuiImgLstWX import vGuiImgLstGet
    from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmd import vXmlNodeExplorerLaunchCmd
    from vidarc.vTools.vExplorer.vXmlNodeExplorerSrc import vXmlNodeExplorerSrc
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vCenterTree(vGuiTreeXmlWX):
    def __init__(self,*args,**kwargs):
        vGuiTreeXmlWX.__init__(self,*args,**kwargs)
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            wid=vGuiTreeXmlWX.__initWid__(self,*args,**kwargs)
            return wid
        except:
            self.__logTB__()
    def __initProperties__(self,*args,**kwargs):
        try:
            vGuiTreeXmlWX.__initProperties__(self,*args,**kwargs)
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.bSort=True
        except:
            self.__logTB__()
    def __initImageLst__(self):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            if self._bImgLstCreated==False:
                return
            vGuiTreeXmlWX.__initImageLst__(self)
            oSrc=vXmlNodeExplorerSrc()
            oCmd=vXmlNodeExplorerLaunchCmd()
            bmp=oSrc.GetBitmap()
            lDef=[]
            for o in [vXmlNodeExplorerSrc(),
                    vXmlNodeExplorerLaunchCmd()]:
                lDef.append((o.GetTagName(),o.GetBitmap()))
            w=self._getImgLstObj()
            w.AddImageList(lDef)
        except:
            self.__logTB__()
