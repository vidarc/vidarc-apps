#----------------------------------------------------------------------------
# Name:         vGuiTextStyledWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20120516
# CVS-ID:       $Id: vGuiTextStyledWX.py,v 1.5 2015/01/21 12:05:37 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import wx.stc as stc
    from vGuiCoreWidWX import vGuiCoreWidWX
    #from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

if wx.Platform == '__WXMSW__':
    faces = { 'times': 'Times New Roman',
              'mono' : 'Courier New',
              'helv' : 'Courier New',
              'other': 'Comic Sans MS',
              'size' : 10,
              'size2': 8,
             }
else:
    faces = { 'times': 'Times',
              'mono' : 'Courier',
              'helv' : 'Helvetica',
              'other': 'new century schoolbook',
              'size' : 12,
              'size2': 10,
             }

class vGuiTextStyledWX(vGuiCoreWidWX):
    SIZE_DFT=(76,24)
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=-1):
    """
    def __initCtrl__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            self.stcData = wx.stc.StyledTextCtrl(id=-1, name='stcData',
                  parent=self.wid, pos=(0,0), size=(-1,24),#sz,
                  style=0,)
            self.AddSz(self.stcData, 1, border=0,sFlag='ax')
            #self.bxs.AddWindow(self.stcData, 1, border=0, 
            #        flag=wx.EXPAND|wx.ALL)
            
            self.__initStc__(kwargs.get('lexer','latex'))
            self.stcData.SetUseVerticalScrollBar(True)
            self.stcData.SetUseHorizontalScrollBar(True)
            
            sVal=kwargs.get('value',None)
            if sVal is not None:
                self.stcData.SetText(sVal)
        except:
            self.__logTB__()
    def __initCtrlBt__(self,*args,**kwargs):
        bEnPopup=kwargs.get('bEnPopup',0)
        if bEnPopup>0:
            vGuiCoreWidWX.__initCtrlBt__(self,*args,**kwargs)
    def __initStc__(self,lexer):
        self.stcData.CmdKeyAssign(ord('B'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMIN)
        self.stcData.CmdKeyAssign(ord('N'), stc.STC_SCMOD_CTRL, stc.STC_CMD_ZOOMOUT)
        
        self.stcData.Bind(stc.EVT_STC_UPDATEUI, self.OnUpdateUI)
        self.stcData.Bind(stc.EVT_STC_MARGINCLICK, self.OnMarginClick)
        self.stcData.Bind(wx.EVT_KEY_DOWN, self.OnKeyPressed)
        self.stcData.Bind(stc.EVT_STC_DOUBLECLICK, self.OnDoubleClick)
        self.stcData.Bind(stc.EVT_STC_USERLISTSELECTION, self.OnUsrListSel)
        #self.stcData.Bind(stc.EVT_STC_MODIFIED, self.OnModified)

        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDEROPEN,    stc.STC_MARK_CIRCLEMINUS,          "white", "#404040");
        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDER,        stc.STC_MARK_CIRCLEPLUS,           "white", "#404040");
        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDERSUB,     stc.STC_MARK_VLINE,                "white", "#404040");
        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDERTAIL,    stc.STC_MARK_LCORNERCURVE,         "white", "#404040");
        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDEREND,     stc.STC_MARK_CIRCLEPLUSCONNECTED,  "white", "#404040");
        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDEROPENMID, stc.STC_MARK_CIRCLEMINUSCONNECTED, "white", "#404040");
        self.stcData.MarkerDefine(stc.STC_MARKNUM_FOLDERMIDTAIL, stc.STC_MARK_TCORNERCURVE,         "white", "#404040");
        
        # Global default styles for all languages
        self.stcData.StyleSetSpec(stc.STC_STYLE_DEFAULT,     "face:%(helv)s,size:%(size)d" % faces)
        #self.stcData.StyleClearAll()  # Reset all to be like the default
        
        self.__setLexer__(lexer)
        #self.__setLexer__('python')
    def __setLexer__(self,lexer):
        bDbg=False
        if VERBOSE>0:
            if self.__isLogDebug__():
                self.__logDebug__(''%())
                bDbg=True
        if bDbg:
            self.__logDebug__('lexer:%s'%(lexer))
        self.stcData.StyleClearAll()  # Reset all to be like the default
        self.stcData.SetMarginType(1, stc.STC_MARGIN_NUMBER)
        self.stcData.SetMarginWidth(1, 25)
        
        iNum=wx.SYS_COLOUR_WINDOW
        oCol=wx.SystemSettings.GetColour(iNum)
        sBkg="back:#%02x%02x%02x"%(oCol.Red(),oCol.Green(),oCol.Blue())
        dFaces=faces.copy()
        dFaces['back']=sBkg
        
        # Global default styles for all languages
        self.stcData.StyleSetSpec(stc.STC_STYLE_DEFAULT,     "face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
        #self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER,  )
        self.sLineMarkUpMod="back:#CCCCCC,face:%(helv)s,size:%(size2)d" % faces
        self.sLineMarkUpNotMod="face:%(helv)s,size:%(size2)d,%(back)s" % dFaces
        self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER, self.sLineMarkUpNotMod)
        self.stcData.StyleSetSpec(stc.STC_STYLE_CONTROLCHAR, "face:%(other)s,%(back)s" % dFaces)
        self.stcData.StyleSetSpec(stc.STC_STYLE_BRACELIGHT,  "fore:#0000FF,bold")
        self.stcData.StyleSetSpec(stc.STC_STYLE_BRACEBAD,    "fore:#FF0000,bold")
        
        #if os.linesep=='\r\n':
        #    self.stcData.SetEOLMode(stc.STC_EOL_CRLF)
        #elif os.linesep=='\n':
        #    self.stcData.SetEOLMode(stc.STC_EOL_LF)
        #elif os.linesep=='\r':
        #    self.stcData.SetEOLMode(stc.STC_EOL_CR)
        self.stcData.SetEOLMode(stc.STC_EOL_LF)
        
        if lexer=="latex":
            self.stcData.SetLexer(stc.STC_LEX_LATEX)
            self.stcData.SetProperty("fold", "1")
            self.stcData.SetProperty("tab.timmy.whinge.level", "1")
            self.stcData.SetMargins(0,0)
            #self.stcData.SetViewWhiteSpace(True)
            self.stcData.SetViewWhiteSpace(False)
            
            self.stcData.SetEdgeMode(stc.STC_EDGE_BACKGROUND)
            self.stcData.SetEdgeColumn(78)
            self.stcData.SetEdgeColour(wx.Colour(oCol.Red()-20,oCol.Green()-20,oCol.Blue()-20))
            
            self.stcData.SetMarginType(2, stc.STC_MARGIN_SYMBOL)
            self.stcData.SetMarginWidth(2, 4)
            self.stcData.SetMarginMask(2, stc.STC_MASK_FOLDERS)
            self.stcData.SetMarginSensitive(2, True)
            
            #self.stcData.StyleSetSpec(stc.STC_STYLE_DEFAULT,sBkg)
            self.stcData.StyleSetSpec(stc.STC_TEX_DEFAULT,"fore:#000000,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            self.stcData.StyleSetSpec(stc.STC_TEX_SPECIAL,"fore:#0000FF,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            self.stcData.StyleSetSpec(stc.STC_TEX_GROUP,"fore:#880088,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            self.stcData.StyleSetSpec(stc.STC_TEX_COMMAND,"fore:#AA0000,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            self.stcData.StyleSetSpec(stc.STC_TEX_TEXT,"fore:#00AA00,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            self.stcData.SetCaretForeground("BLACK")
            self.stcData.SetSelForeground(True,wx.Colour(oCol.Red(),oCol.Green(),oCol.Blue()))
            self.stcData.SetSelBackground(True,wx.Colour(0,0,0))
            
            self.stcData.SetTabWidth(2)
        elif lexer=="python":
            self.stcData.SetLexer(stc.STC_LEX_PYTHON)
            #self.stcData.SetKeyWords(0, " ".join(keyword.kwlist))
    
            self.stcData.SetProperty("fold", "1")
            self.stcData.SetProperty("tab.timmy.whinge.level", "1")
            self.stcData.SetMargins(0,0)
    
            self.stcData.SetViewWhiteSpace(False)
            #self.SetBufferedDraw(False)
            #self.SetViewEOL(True)
            #self.SetUseAntiAliasing(True)
            
            self.stcData.SetEdgeMode(stc.STC_EDGE_BACKGROUND)
            self.stcData.SetEdgeColumn(78)
            
            # Setup a margin to hold fold markers
            #self.SetFoldFlags(16)  ###  WHAT IS THIS VALUE?  WHAT ARE THE OTHER FLAGS?  DOES IT MATTER?
            self.stcData.SetMarginType(2, stc.STC_MARGIN_SYMBOL)
            self.stcData.SetMarginMask(2, stc.STC_MASK_FOLDERS)
            self.stcData.SetMarginSensitive(2, True)
            self.stcData.SetMarginWidth(2, 12)
            
            self.stcData.StyleSetSpec(stc.STC_STYLE_DEFAULT,sBkg)
            # Python styles
            # Default 
            self.stcData.StyleSetSpec(stc.STC_P_DEFAULT, "fore:#000000,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            # Comments
            self.stcData.StyleSetSpec(stc.STC_P_COMMENTLINE, "fore:#007F00,face:%(other)s,size:%(size)d,%(back)s" % dFaces)
            # Number
            self.stcData.StyleSetSpec(stc.STC_P_NUMBER, "fore:#007F7F,size:%(size)d,%(back)s" % dFaces)
            # String
            self.stcData.StyleSetSpec(stc.STC_P_STRING, "fore:#7F007F,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            # Single quoted string
            self.stcData.StyleSetSpec(stc.STC_P_CHARACTER, "fore:#7F007F,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            # Keyword
            self.stcData.StyleSetSpec(stc.STC_P_WORD, "fore:#00007F,bold,size:%(size)d,%(back)s" % dFaces)
            # Triple quotes
            self.stcData.StyleSetSpec(stc.STC_P_TRIPLE, "fore:#7F0000,size:%(size)d,%(back)s" % dFaces)
            # Triple double quotes
            self.stcData.StyleSetSpec(stc.STC_P_TRIPLEDOUBLE, "fore:#7F0000,size:%(size)d,%(back)s" % dFaces)
            # Class name definition
            self.stcData.StyleSetSpec(stc.STC_P_CLASSNAME, "fore:#0000FF,bold,underline,size:%(size)d,%(back)s" % dFaces)
            # Function or method name definition
            self.stcData.StyleSetSpec(stc.STC_P_DEFNAME, "fore:#007F7F,bold,size:%(size)d,%(back)s" % dFaces)
            # Operators
            self.stcData.StyleSetSpec(stc.STC_P_OPERATOR, "bold,size:%(size)d,%(back)s" % dFaces)
            # Identifiers
            self.stcData.StyleSetSpec(stc.STC_P_IDENTIFIER, "fore:#000000,face:%(helv)s,size:%(size)d,%(back)s" % dFaces)
            # Comment-blocks
            self.stcData.StyleSetSpec(stc.STC_P_COMMENTBLOCK, "fore:#7F7F7F,size:%(size)d,%(back)s" % dFaces)
            # End of line where string is not closed
            self.stcData.StyleSetSpec(stc.STC_P_STRINGEOL, "fore:#000000,face:%(mono)s,back:#E0C0E0,eol,size:%(size)d,%(back)s" % dFaces)
    
            self.stcData.SetCaretForeground("BLACK")
            
            self.stcData.SetTabWidth(2)
        #self.ERROR_STYLE=9000
        #self.stcData.StyleSetSpec(self.ERROR_STYLE, "fore:#FF0000,italic,size:%(size)d" % faces)
    
        self.lexer=lexer
    def AddValue(self,sVal):
        try:
            self.stcData.AddText(sVal)
            #self.stcData.SetSelection(0)
            self.stcData.SetFocus()
            self.PostMOD('add')
        except:
            self.__logTB__()
    def SetValue(self,sVal):
        try:
            self.stcData.SetText(sVal)
            self.stcData.SetUseVerticalScrollBar(True)
            self.stcData.SetUseHorizontalScrollBar(True)
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            sVal=self.stcData.GetText()
            return sVal
        except:
            self.__logTB__()
        return ''
    def SetWidPopup(self,wid):
        """
        """
        try:
            if self.IsMainThread()==False:
                return
            if self.widBt is not None:
                self.__logError__('popup disabled'%())
                return
            self.widPop=wid
            w=self.GetWid()
            self.widBt.Enable(True)
            self.bBusy=True
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                if hasattr(self.widPop,'BindEvent'):
                    self.widPop.BindEvent('ok',self.OnPopupOk)
                    self.widPop.BindEvent('cancel',self.OnPopupCancel)
        except:
            self.__logTB__()
    def GetWidPopup(self):
        return self.widPop
    def OnTextText(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return 
            #if self.bBlock:
            #    return
            #self.__markModified__()
            sVal=self.stcData.GetValue()
            self.PostFB('txt',sVal)
            #wx.PostEvent(self,vtInputTextChanged(self,sVal))
        except:
            self.__logTB__()
    def OnPopupButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            w=self.GetWid()
            if self.widPop is not None:
                if self.widBt.GetValue()==True:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    if self.iArrange>=0:
                        vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                    self.widPop.Show(True)
                else:
                    self.widPop.Show(False)
            else:
                self.__logError__('no popup widget set yet'%())
                #vtGuiCoreArrangeWidget(self,self,iArrange)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            self.__logDebug__(''%())
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            if self.widPop is not None:
                if hasattr(self.widPop,'GetWid'):
                    widPop=self.widPop.GetWid()
                else:
                    widPop=self.widPop
                widPop.Show(False)
            if self.widBt is not None:
                self.widBt.SetValue(False)
        except:
            self.__logTB__()
    Close=__Close__
    def __Clear__(self):
        try:
            bDbg=self.GetVerbose(iVerbose=5)
            if bDbg:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            self.stcData.ClearAll()
            self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER, self.sLineMarkUpNotMod)
        except:
            self.__logTB__()
    Clear=__Clear__
    def GetWid(self):
        #return self.pn
        return self.wid
    def GetWidMod(self):
        return self
        return self.stcData
    def Popup(self,bFlag=True):
        self.SetValue(bFlag)
        self.OnPopupButton(None)
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
    def OnPopupCancel(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
    def OnKeyPressed(self, event):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if self.stcData.CallTipActive():
            self.stcData.CallTipCancel()
        if wx.VERSION >= (2,8):
            key=event.GetKeyCode()
        else:
            key = event.KeyCode()
        #self.SetModified(True)
        bOk=False
        if key>=40 and key<300:
            bOk=True
        elif key==8:
            bOk=True
        if bOk==True:
            self.PostMOD('key')
        if key == 32 and event.ControlDown():
            pos = self.stcData.GetCurrentPos()

            # Tips
            if event.ShiftDown():
                self.stcData.CallTipSetBackground("yellow")
                self.stcData.CallTipShow(pos, 'lots of of text: blah, blah, blah\n\n'
                                 'show some suff, maybe parameters..\n\n'
                                 'fubar(param1, param2)')
            # Code completion
            else:
                #lst = []
                #for x in range(50000):
                #    lst.append('%05d' % x)
                #st = " ".join(lst)
                #print len(st)
                #self.AutoCompShow(0, st)

                kw = []
                for cmd in self.objRegNode.GetProcessingCmdsVisible():#PROCESSING_CMDS_VIS:
                    kw.append(cmd)
                kw.sort()  # Python sorts are case sensitive
                self.stcData.AutoCompSetIgnoreCase(False)  # so this needs to match
                #print keyword.kwlist
                # Images are specified with a appended "?type"
                #for i in range(len(kw)):
                #    if kw[i] in keyword.kwlist:
                #        kw[i] = kw[i] #+ "?1"

                self.stcData.AutoCompShow(0, " ".join(kw))
        else:
            event.Skip()
    def OnUpdateUI(self, evt):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # check for matching braces
        braceAtCaret = -1
        braceOpposite = -1
        charBefore = None
        caretPos = self.stcData.GetCurrentPos()

        if caretPos > 0:
            charBefore = self.stcData.GetCharAt(caretPos - 1)
            styleBefore = self.stcData.GetStyleAt(caretPos - 1)

        # check before
        if charBefore and chr(charBefore) in "[]{}()" and styleBefore == stc.STC_P_OPERATOR:
            braceAtCaret = caretPos - 1

        # check after
        if braceAtCaret < 0:
            charAfter = self.stcData.GetCharAt(caretPos)
            styleAfter = self.stcData.GetStyleAt(caretPos)

            if charAfter and chr(charAfter) in "[]{}()" and styleAfter == stc.STC_P_OPERATOR:
                braceAtCaret = caretPos

        if braceAtCaret >= 0:
            braceOpposite = self.stcData.BraceMatch(braceAtCaret)

        if braceAtCaret != -1  and braceOpposite == -1:
            self.stcData.BraceBadLight(braceAtCaret)
        else:
            self.stcData.BraceHighlight(braceAtCaret, braceOpposite)
            #pt = self.PointFromPosition(braceOpposite)
            #self.Refresh(True, wxRect(pt.x, pt.y, 5,5))
            #print pt
            #self.Refresh(False)

    def OnMarginClick(self, evt):
        if VERBOSE>0:
            self.__logDebug__(''%())
        # fold and unfold as needed
        if evt.GetMargin() == 2:
            if evt.GetShift() and evt.GetControl():
                self.stcData.FoldAll()
            else:
                lineClicked = self.stcData.LineFromPosition(evt.GetPosition())

                if self.stcData.GetFoldLevel(lineClicked) & stc.STC_FOLDLEVELHEADERFLAG:
                    if evt.GetShift():
                        self.stcData.SetFoldExpanded(lineClicked, True)
                        self.stcData.Expand(lineClicked, True, True, 1)
                    elif evt.GetControl():
                        if self.stcData.GetFoldExpanded(lineClicked):
                            self.stcData.SetFoldExpanded(lineClicked, False)
                            self.stcData.Expand(lineClicked, False, True, 0)
                        else:
                            self.stcData.SetFoldExpanded(lineClicked, True)
                            self.stcData.Expand(lineClicked, True, True, 100)
                    else:
                        self.stcData.ToggleFold(lineClicked)


    def FoldAll(self):
        if VERBOSE>0:
            self.__logDebug__(''%())
        lineCount = self.stcData.GetLineCount()
        expanding = True

        # find out if we are folding or unfolding
        for lineNum in range(lineCount):
            if self.stcData.GetFoldLevel(lineNum) & stc.STC_FOLDLEVELHEADERFLAG:
                expanding = not self.stcData.GetFoldExpanded(lineNum)
                break;

        lineNum = 0

        while lineNum < lineCount:
            level = self.stcData.GetFoldLevel(lineNum)
            if level & stc.STC_FOLDLEVELHEADERFLAG and \
               (level & stc.STC_FOLDLEVELNUMBERMASK) == stc.STC_FOLDLEVELBASE:

                if expanding:
                    self.stcData.SetFoldExpanded(lineNum, True)
                    lineNum = self.stcData.Expand(lineNum, True)
                    lineNum = lineNum - 1
                else:
                    lastChild = self.stcData.GetLastChild(lineNum, -1)
                    self.stcData.SetFoldExpanded(lineNum, False)

                    if lastChild > lineNum:
                        self.stcData.HideLines(lineNum+1, lastChild)

            lineNum = lineNum + 1



    def Expand(self, line, doExpand, force=False, visLevels=0, level=-1):
        if VERBOSE>0:
            self.__logDebug__(''%())
        lastChild = self.stcData.GetLastChild(line, level)
        line = line + 1

        while line <= lastChild:
            if force:
                if visLevels > 0:
                    self.stcData.ShowLines(line, line)
                else:
                    self.stcData.HideLines(line, line)
            else:
                if doExpand:
                    self.stcData.ShowLines(line, line)

            if level == -1:
                level = self.stcData.GetFoldLevel(line)

            if level & stc.STC_FOLDLEVELHEADERFLAG:
                if force:
                    if visLevels > 1:
                        self.stcData.SetFoldExpanded(line, True)
                    else:
                        self.stcData.SetFoldExpanded(line, False)

                    line = self.stcData.Expand(line, doExpand, force, visLevels-1)

                else:
                    if doExpand and self.stcData.GetFoldExpanded(line):
                        line = self.stcData.Expand(line, True, force, visLevels-1)
                    else:
                        line = self.stcData.Expand(line, False, force, visLevels-1)
            else:
                line = line + 1;

        return line

    def OnUsrListSel(self,evt):
        self.__logError__('you are not supposed to be here'%())
    def GetDocElementInfos(self,iPos):
        if VERBOSE>0:
            self.__logDebug__(''%())
        d={'[':-1,']':-1,'{':-1,'}':-2,'(':-1,')':-2,'\n':-3,'\r':-3,
                '\t':-4,' ':-4}
        if iPos<0:
            iPos=self.stcData.GetCurrentPos()
        iMax=self.stcData.GetLength()
        def __getValues__():
            if d['[']<d[']']:
                s1=self.stcData.GetTextRange(d['[']+1,d[']'])
            else:
                s1=''
            if d['{']<d['}']:
                s2=self.stcData.GetTextRange(d['{']+1,d['}'])
            else:
                s2=''
            if d['(']<d[')']:
                s3=self.stcData.GetTextRange(d['(']+1,d[')'])
            else:
                s3=''
            return (s1,s2,s3,d)
        def isBracketOpen(d):
            if d['[']>=0 and d[']']<0:
                return True
            if d['(']>=0 and d[')']<0:
                return True
            if d['{']>=0 and d['}']<0:
                return True
            return False
        while iPos<iMax:
            c=chr(self.stcData.GetCharAt(iPos))
            if d.has_key(c):
                if d[c]==-4 and isBracketOpen(d)==False:
                    return ('','','',d)
                elif d[c]==-3:
                    return __getValues__()
                elif d[c]==-2:
                    cn=chr(self.stcData.GetCharAt(iPos+1))
                    try:
                        val=d[cn]
                        if val<-2:
                            d[c]=iPos
                            return __getValues__()
                        d[c]=iPos
                        iPos+=1
                        continue
                    except:
                        d[c]=iPos
                        return __getValues__()
                    d[c]=iPos
                elif d[c]<0:
                    d[c]=iPos
            iPos+=1
        if d.has_key(c):
            return __getValues__()
        else:
            return ('','','',d)
    def ChgDocElementInfos(self,iPos,tup,iRet,tupInfos):
        if VERBOSE>0:
            self.__logDebug__(''%())
        if iPos<0:
            iPos=self.stcData.GetCurrentPos()
        if 1:
            if iRet==1:
                iEnd=tup[-1]['}']
                if iEnd<0:
                    iEnd=self.stcData.GetLength()
                else:
                    iEnd+=1
                self.stcData.SetTargetStart(iPos)
                self.stcData.SetTargetEnd(iEnd)
                self.stcData.ReplaceTarget(u'[%s]{%s}'%tupInfos)
                #self.SetModified(True)
                self.PostMOD('chg')
            elif iRet==2:
                #tupInfos=self.dlgBrowseElementAttr.GetDocElementInfo()
                iEnd=tup[-1][')']
                if iEnd<0:
                    iEnd=self.stcData.GetLength()
                else:
                    iEnd+=1
                self.stcData.SetTargetStart(iPos)
                self.stcData.SetTargetEnd(iEnd)
                self.stcData.ReplaceTarget(u'[%s]{%s}(%s)'%tupInfos)
                #self.SetModified(True)
                self.PostMOD('chg')
    def OnDoubleClick(self,evnt):
        if VERBOSE>0:
            self.__logDebug__(''%())
        sVal=self.stcData.GetTextRange(self.stcData.GetSelectionStart()-1,
                        self.stcData.GetSelectionEnd())
        self.Post('dblclk',sVal)
        return
        iBrowseable=self.objRegNode.IsBrowseable(sVal)
        if iBrowseable>0:
            iPos=self.stcData.GetCurrentPos()
            tup=self.__getDocElementInfos__(iPos)
            dlg=self.GetBrowseDlg()
            if dlg is None:
                dlg=vtmMsgDialog(self,_(u'No browsing dialog defined!\nThis feature can not be used now.') ,
                            'veRepDoc',
                            wx.OK|wx.NO_DEFAULT|wx.ICON_EXCLAMATION )
                dlg.Centre()
                dlg.ShowModal()
                dlg.Destroy()
                return
            nodeRel=self.doc.GetRelBaseNode(self.node)
            dlg.SetActNode(self.node)
            dlg.SetRelNode(nodeRel)
            dlg.SetLang(self.viLang.GetValue())
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iPos:%d;iBrowseable:%d;tup:%s'%
                    (iPos,iBrowseable,vtLog.pformat(tup)),self)
            iRet,tupInfos=self.objRegNode.DoBrowse(iBrowseable,tup,self)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'iRet:%d;iBrowseable:%d;tupInfos:%s'%
                    (iPos,iBrowseable,vtLog.pformat(tupInfos)),self)
            if self.VERBOSE:
                vtLog.CallStack('')
                print iRet,tupInfos
            if iRet==1:
                iEnd=tup[-1]['}']
                if iEnd<0:
                    iEnd=self.stcData.GetLength()
                else:
                    iEnd+=1
                self.stcData.SetTargetStart(iPos)
                self.stcData.SetTargetEnd(iEnd)
                self.stcData.ReplaceTarget(u'[%s]{%s}'%tupInfos)
                #self.SetModified(True)
                self.PostMOD('dblclk')
            elif iRet==2:
                #tupInfos=self.dlgBrowseElementAttr.GetDocElementInfo()
                iEnd=tup[-1][')']
                if iEnd<0:
                    iEnd=self.stcData.GetLength()
                else:
                    iEnd+=1
                self.stcData.SetTargetStart(iPos)
                self.stcData.SetTargetEnd(iEnd)
                self.stcData.ReplaceTarget(u'[%s]{%s}(%s)'%tupInfos)
                #self.SetModified(True)
                self.PostMOD('dblclk')
    def UpdateLinks(self,objRegNode,doc,node,par,silent=False):
        if VERBOSE>0:
            self.__logDebug__(''%())
        #FIXME
        if node is None:
            return
        if doc is None:
            return
        rootNode=doc.GetBaseNode(node)
        iPos=0
        relNode=doc.GetRelBaseNode(node)
        lstMissingID=[]
        lBrowsableCmds=objRegNode.GetBrowsableCmds()
        while iPos<self.stcData.GetLength():
            c=chr(self.stcData.GetCharAt(iPos))
            if c=='\\':
                tup=self.GetDocElementInfos(iPos)
                if tup[0]!='' and tup[1]!='':
                    d=tup[-1]
                    iEnd=d['[']
                    if iEnd>0:
                        if d['}']>0:
                            sVal=self.stcData.GetTextRange(iPos,iEnd)
                            if sVal in lBrowsableCmds:
                                tupID=doc.GetIdStr(tup[0])
                                if tupID[1] is None:
                                    #iPos=d['[']
                                    iEnd=d['}']
                                    if iEnd<0:
                                        iEnd=self.stcData.GetLength()
                                    else:
                                        iEnd+=1
                                    #self.stcData.StartStyling(98,0xff)
                                    #self.stcData.SetStyling(iPos,self.ERROR_STYLE)
                                    if silent==False:
                                        self.stcData.SetSelection(iPos+1,d['['])
                                        iRet=par.ShowMsgDlg(par.YESNO,_(u'At missing ID! Unable to update link.')+u'\n\n'+
                                                _(u'Do you want to edit it now, and restart update after modification?'),
                                                None)
                                        if iRet==1:
                                            self.OnDoubleClick(None)
                                            self.__updateLinks__()
                                        else:
                                            lstMissingID.append((tup[0],tup[1]))
                                            break
                                    pass
                                else:
                                    node=tupID[1]
                                    sName=doc.GetTagNames(node,rootNode,relNode)
                                    try:
                                        sNameAdd=tup[1].split('|')[1]
                                        sName += '|'+sNameAdd
                                    except:
                                        pass
                                    if sName!=tup[1]:
                                        iPos=d['[']
                                        iEnd=d['}']
                                        if iEnd<0:
                                            iEnd=self.stcData.GetLength()
                                        else:
                                            iEnd+=1
                                        self.stcData.SetTargetStart(iPos)
                                        self.stcData.SetTargetEnd(iEnd)
                                        self.stcData.ReplaceTarget(u'[%s]{%s}'%(tup[0],sName))
                                        self.PostMOD('upd')
                            iPos=d['}']
            iPos+=1
        if len(lstMissingID):
            iRet=par.ShowMsgDlg(par.YESNO,u'At least one ID is missing!',None)
            if iRet==wx.ID_YES:
                pass
    def __SetWidMod__(self,state):
        self.__logDebug__('state:%d'%(state))
        try:
            if state==True:
                self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER, self.sLineMarkUpMod)
            else:
                self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER, self.sLineMarkUpNotMod)
        except:
            self.__logTB__()
        return
        if VERBOSE>0:
            self.__logDebug__(''%())
        if obj is None:
            bRefresh=False
            if self.GetModified()!=state:
                bRefresh=True
        #vtXmlNodePanel.SetModified(self,state,obj=obj)
        bRefresh=True
        if bRefresh:
            if self.IsMainThread()==False:
                return
            if self.bModified==True:
                self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER, self.sLineMarkUpMod)
            else:
                self.stcData.StyleSetSpec(stc.STC_STYLE_LINENUMBER, self.sLineMarkUpNotMod)
