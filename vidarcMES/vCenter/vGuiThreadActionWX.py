#----------------------------------------------------------------------------
# Name:         vGuiThreadActionWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120501
# CVS-ID:       $Id: vGuiThreadActionWX.py,v 1.17 2015/09/11 09:57:34 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import time
    from vGuiPanelWX import vGuiPanelWX
    from vGuiFrmWX import vGuiFrmWX
    
    import vidarc.tool.lang.vtLgBase as vtLgBase
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)

class vGuiThreadActionWX(vGuiPanelWX):
    FLEX_SIZER=1
    FMT_CNT="%4d-%4d"
    MAP_STATUS={'break':'>','done':'-','proc':'+','empty':''}
    def __initGetWidMain__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            lWidMain=[
                ('cbBmp',   0,4,{'name':'cbStart',
                        'tip':_(u'start thread'),
                        'bitmap':self.getBmp('stat','Start')},
                        {'btn':self.OnCbStartButton}),
                ('cbBmp',   0,4,{'name':'cbStop',
                        'tip':_(u'stop thread'),
                        'bitmap':self.getBmp('stat','Stop')},
                        {'btn':self.OnCbStopButton}),
                ('txtRd',   4,4,{'name':'txtVal','value':u''},None),
                ('txtRd',   0,4,{'name':'txtCnt','size':(60,-1),'value':u''},None),
                ('tgPopup', 0,0,{
                    'name':'tgView',#'iArrange':-1,
                    },None),
                ]
            return lWidMain
        except:
            self.__logTB__()
        return []
    def __setUpPopUp__(self,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        try:
            self.__setUpPopUpView__(*args,**kwargs)
        except:
            self.__logTB__()
    def __setUpPopUpView__(self,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        try:
            p=vGuiFrmWX(parent=self.tgView.GetWid(),kind='popup',
                iArrange=0x40,widArrange=self.txtVal,bAnchor=True,
                lGrowRow=[(0,1),],
                #lGrowCol=[(0,1),(1,1)],
                lGrowCol=[(0,1),],
                kwTopCmd={'lWid':[
                    ('szBoxHor',(0,0),(1,1),{'name':'bxsBt'},[
                        ('txtRdInt',     0,4,{'name':'txtOfs',
                            'tip':_(u'action offset'),
                            'value':kwargs.get('offset',u'0')},None),
                        ('txtRdInt',     0,0,{'name':'txtCnt',
                            'tip':_(u'action count'),
                            'value':kwargs.get('count',u'0')},None),
                        ]),
                    ],
                    },
                lWid=[
                    ('lstCtrl', (0,0),(1,2),{'name':'lstAction','size':(400,200),
                            'cols':[
                                ['',-3,20,0],
                                [_(u'name'),-1,-1,1],
                                [_(u'result'),-2,80,1],
                                ],
                            'map':[('status',0),('name',1),('result',2)],
                            'tip':_(u'action'),
                            },None),
                        #('txt',     (0,1),(1,1),{'name':'txtDSN','value':kwargs.get('dsn',u'')},None),
                    ('szBoxHor',(1,0),(1,2),{'name':'bxsBt'},[
                        ('txt',     1,4,{'name':'txtAct',
                            'tip':_(u'action name'),
                            'value':u''},None),
                        ('txt',     0,0,{'name':'txtRes',
                            'tip':_(u'action result'),
                            'value':u'','size':(80,-1)},None),
                            ]),
                    ('szBoxVert',(0,2),(1,1),{'name':'bxsBt'},[
                        ('cbBmp',   0,4,{'name':'cbClr',
                            'tip':_(u'clear actions'),
                            'bitmap':self.getBmp('core','Erase')},
                            {'btn':self.OnClr}),
                        #('cbBmp',   (0,4),(1,1),{'name':'cbConn',
                        #    'tip':_(u'connect to database'),
                        #    'bitmap':vtArtState.getBitmap(vtArtState.Start)},
                        #    {'btn':self.OnConnect}),
                        ]),
                    ])
            self.tgView.SetWidPopup(p)
        except:
            self.__logTB__()
    def __initThread__(self,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        try:
            v=kwargs.get('createThread',0)
            self.thd2Ctrl=None
            self.funcStart=None
            self.funcStop=None
            self.iAct=0
            self.iCnt=0
            self.CB(self.ShowProc)
            if v!=0:
                self.thd2Ctrl=vpc.vcpThd()
                self.thd2Ctrl.Inz(self.GetOrigin(),'thd')
        except:
            self.__logTB__()
    def __setUpThread__(self,*args,**kwargs):
        if self.IsMainThread()==False:
            return
        try:
            if self.thd2Ctrl is None:
                return 0
            self.thd2Ctrl.Start()
            self.thd2Ctrl.Do(self.GetThread)
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        global _
        _=vtLgBase.assignPluginLang('db')
        lWidMain=self.__initGetWidMain__(*args,**kwargs)
        lWid=[
            ('szBoxHor',1,0,
                    {'name':'bxsInfo',},lWidMain),
            ('szBoxHor',1,0,
                    {'name':'bxsInfo',},[
                ('bar',      1,0,{'name':'pbProc','range':1000},None),
                ]),
            ]
        vGuiPanelWX.__initCtrl__(self,lWid=lWid)
        self.__initThread__(*args,**kwargs)
        self.__setUpPopUp__(*args,**kwargs)
        self.__setUpThread__(*args,**kwargs)
        return [],[(0,1)]
    def ShowProc(self):
        try:
            if self.IsMainThread()==False:
                return
            self.txtCnt.SetValue(self.FMT_CNT%(self.iAct,self.iCnt))
        except:
            self.__logTB__()
    def SetThread(self,thd):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(''%())
            self.thd2Ctrl=thd
        except:
            self.__logTB__()
    def GetThread(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(''%())
            return self.thd2Ctrl
        except:
            self.__logTB__()
        return None
    def Do(self,func,*args,**kwargs):
        try:
            self.thd2Ctrl.Do(func,*args,**kwargs)
        except:
            self.__logTB__()
    def DoCB(self,func,*args,**kwargs):
        try:
            self.thd2Ctrl.Do(self.CB,func,*args,**kwargs)
        except:
            self.__logTB__()
    def GetActionWid(self):
        try:
            if self.IsMainThread()==False:
                return
            p=self.tgView.GetWidPopup()
            return p.lstAction
        except:
            self.__logTB__()
        return None
    def AddAct(self,sName,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            w.Insert([{-1:(sName,0,func,args,kwargs),
                    'name':sName
                    }])
            self.iCnt=self.iCnt+1
            #self.thd2Ctrl.Do(self.CB,func,*args,**kwargs)
        except:
            self.__logTB__()
    def AddActCB(self,sName,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            w.Insert([{-1:(sName,1,func,args,kwargs),
                    'name':sName
                    }])
            self.iCnt=self.iCnt+1
            #self.thd2Ctrl.Do(self.CB,func,*args,**kwargs)
        except:
            self.__logTB__()
    def AddActBreakCB(self,sName,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            w.Insert([{-1:(sName,2,func,args,kwargs),
                    'name':sName
                    }])
            self.iCnt=self.iCnt+1
            #self.thd2Ctrl.Do(self.CB,func,*args,**kwargs)
        except:
            self.__logTB__()
    def AddBreak(self,sName):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            w.Insert([{-1:(sName,-2),
                    'name':sName
                    }])
            self.iCnt=self.iCnt+1
        except:
            self.__logTB__()
    def AddMsg(self,sMsg):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            sName=sMsg.replace('\n',' ')
            w.Insert([{-1:(sName,-4,sMsg),
                    'name':sName,
                    'msg':sMsg
                    }])
            self.iCnt=self.iCnt+1
        except:
            self.__logTB__()
    def AddQst(self,sMsg):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            sName=sMsg.replace('\n',' ')
            w.Insert([{-1:(sName,-3,sMsg),
                    'name':sName,
                    'msg':sMsg
                    }])
            self.iCnt=self.iCnt+1
        except:
            self.__logTB__()
    def qstAnswer(self,iRsp,sQst):
        if iRsp<1:
            self.__logError__('%s;failed'%(sQst))
            self.PostCancel(self.iAct-1)
        elif iRsp==1:
            self.DoCB(self.procAct)
            self.PostOk(self.iAct-1)
    def AddBranch(self,sMsg,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            sName=sMsg.replace('\n',' ')
            w.Insert([{-1:(sName,-5,sMsg,func,args,kwargs),
                    'name':sName,
                    'msg':sMsg
                    }])
            self.iCnt=self.iCnt+1
        except:
            self.__logTB__()
    def branchAnswer(self,iRsp,sQst,func,*args,**kwargs):
        try:
            self.__logDebug__('iRsp:%d'%(iRsp))
            self.Do(self.procFuncDly,func,iRsp,*args,**kwargs)
        except:
            self.__logTB__()
    def AddBranchCB(self,sMsg,func,*args,**kwargs):
        try:
            if self.IsMainThread()==False:
                return
            w=self.GetActionWid()
            sName=sMsg.replace('\n',' ')
            w.Insert([{-1:(sName,-6,sMsg,func,args,kwargs),
                    'name':sName,
                    'msg':sMsg
                    }])
            self.iCnt=self.iCnt+1
        except:
            self.__logTB__()
    def branchAnswerCB(self,iRsp,sQst,func,*args,**kwargs):
        try:
            self.__logDebug__('iRsp:%d'%(iRsp))
            self.Do(self.procFuncDlyCB,func,iRsp,*args,**kwargs)
        except:
            self.__logTB__()
    def procFuncDly(self,func,*args,**kwargs):
        try:
            time.sleep(0.1)
            func(*args,**kwargs)
        except:
            self.__logTB__()
    def procFuncDlyCB(self,func,*args,**kwargs):
        try:
            time.sleep(0.1)
            self.CB(func,*args,**kwargs)
        except:
            self.__logTB__()
    def procAct(self):
        try:
            if self.IsMainThread()==False:
                return
            self.__logDebug__({'iAct':self.iAct,'iCnt':self.iCnt})
            w=self.GetActionWid()
            if self.iAct>0:
                w.SetItem(self.iAct-1,0,self.MAP_STATUS['done'])
                w.SetSelected([self.iAct-1],False)
            if self.iAct>=self.iCnt:
                self.iAct=-1    # restart
                return
            if self.iAct<0:
                for iIdx in xrange(self.iCnt):
                    w.SetItem(iIdx,0,self.MAP_STATUS['empty'])
                self.iAct=0
            if self.iCnt==0:
                return
            l=w.GetValue(None,self.iAct)
            t=l[0]
            #print self.iAct,t
            self.__logDebug__('iAct:%d;iCnt:%d;check action to perform:%d'%(self.iAct,self.iCnt,t[1]))
            w.SetItem(self.iAct,0,self.MAP_STATUS['proc'])
            w.ShowItem(self.iAct)
            self.iAct=self.iAct+1
            
            sTmp=t[0].replace('\n',' ')
            self.txtVal.SetValue(sTmp)
            self.ShowProc()
            if t[1]==-4:
                self.__logDebug__('ShowMsgDlg'%())
                self.Post(self.iAct-1)
                self.DoCB(self.ShowMsgDlg,0x302,t[2],None)
                return
            elif t[1]==-3:
                self.__logDebug__('ShowMsgDlg;quest answer function call'%())
                self.Post(self.iAct-1)
                self.DoCB(self.ShowMsgDlg,0x401,t[2],self.qstAnswer,t[0])
                return
            elif t[1]==-5:
                self.__logDebug__('ShowMsgDlg;quest branch answer function call'%())
                self.Post(self.iAct-1)
                self.DoCB(self.ShowMsgDlg,0x401,t[2],self.branchAnswer,t[0],
                        t[3],*t[4],**t[5])
                return
            elif t[1]==-6:
                self.__logDebug__('ShowMsgDlg;quest branch answer callback function call'%())
                self.Post(self.iAct-1)
                self.DoCB(self.ShowMsgDlg,0x401,t[2],self.branchAnswerCB,t[0],
                        t[3],*t[4],**t[5])
                return
            elif t[1]==-2:
                self.__logDebug__('break'%())
                self.Post(self.iAct-1)
                w.SetItem(self.iAct-1,0,self.MAP_STATUS['break'])
                w.SetSelected([self.iAct-1],True)
                w.ShowItem(self.iAct-1)
                return
            elif t[1]==0:
                self.__logDebug__('function call delayed'%())
                self.Post(self.iAct-1)
                self.Do(self.procFuncDly,t[2],*t[3],**t[4])
            elif t[1]==1:
                self.__logDebug__('callback function call delayed'%())
                self.Post(self.iAct-1)
                self.Do(self.procFuncDlyCB,t[2],*t[3],**t[4])
            elif t[1]==2:
                self.__logDebug__('callback function call delayed break'%())
                self.Post(self.iAct-1)
                self.Do(self.procFuncDlyCB,t[2],*t[3],**t[4])
                return
            else:
                self.__logError__(t)
            self.DoCB(self.procAct)
            self.__logDebug__('iAct:%d;iCnt:%d;procAct scheduled'%(self.iAct,self.iCnt))
        except:
            self.__logTB__()
    def DoAct(self):
        self.DoCB(self.procAct)
    # ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # + database processing
    def OnThdDbProc(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnThdDbAbort(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnThdDbFin(self,evt):
        try:
            self.__logDebug__(''%())
            s=self.txtStat.GetValue()
            self.txtStat.SetValue(s+' '+_(u'done.'))
        except:
            self.__logTB__()
    # - database processing
    # --------------------------------------------------------------------------------
    def prcStart(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def prcStop(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def DoStart(self):
        try:
            self.__logInfo__(''%())
            self.Post('start')
            if self.thd2Ctrl is None:
                self.thd2Ctrl=vpc.vcpThd()
                self.thd2Ctrl.Inz(self.GetOrigin(),'thd')
            if self.thd2Ctrl is not None:
                self.thd2Ctrl.Start()
            self.prcStart()
            if self.funcStart is not None:
                func,args,kwargs=self.funcStart
                func(*args,**kwargs)
            self.DoAct()
            #self.Start()
        except:
            self.__logTB__()
    def DoStop(self):
        try:
            self.Post('stop')
            if self.funcStop is not None:
                func,args,kwargs=self.funcStop
                func(*args,**kwargs)
            self.prcStop()
            if self.thd2Ctrl is not None:
                self.thd2Ctrl.Stop()
                #self.__stop__(bEnStart=not self.thd2Ctrl.IsRunning(),bClr=False)
            #else:
            #    self.__stop__(bEnStart=True,bClr=False)
        except:
            self.__logTB__()
    def ShutDown(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if self.thd2Ctrl is not None:
                #self.thd2Ctrl.ShutDown()
                self.thd2Ctrl.Stop()
                self.thd2Ctrl=None
        except:
            self.__logTB__()
    def OnCbStartButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.DoStart()
        except:
            self.__logTB__()
    def OnCbStopButton(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            self.DoStop()
        except:
            self.__logTB__()
    def OnClr(self,evt):
        evt.Skip()
        try:
            self.__logInfo__(''%())
            w=self.GetActionWid()
            w.DeleteAllItems()
        except:
            self.__logTB__()
