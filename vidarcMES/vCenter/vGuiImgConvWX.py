#----------------------------------------------------------------------------
# Name:         vGuiImgConvWX.py
# Purpose:      image conversion object
# Author:       Walter Obweger
#
# Created:      20131103
# CVS-ID:       $Id: vGuiImgConvWX.py,v 1.2 2015/01/21 12:02:57 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import array
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)


class vGuiImgConvWX:
    def ConvBmpAlpha(self,bmpInst,f):
        img=bmpInst.ConvertToImage()
        return self.ConvImgAlpha(img,f)
    def ConvImgAlpha(self,imgInst,f):
        ar=array.array('B',imgInst.GetData())
        iLen=ar.buffer_info()[1]
        for i in xrange(0, iLen, 3):
            if ar[i]!=1:
                if ar[i+1]!=0:
                    if ar[i+2]!=0:
                        def chRgb(a):
                            a=int(a*f)
                            if a>254:
                                a=254
                            return a
                        ar[i]=chRgb(ar[i])
                        ar[i+1]=chRgb(ar[i+1]) 
                        ar[i+2]=chRgb(ar[i+2])
        imgInst.SetData(ar.tostring())
        return imgInst.ConvertToBitmap()
    def MergeImg(self,imgTmp,imgOverlay,mergeMask=None):
        arTmp=array.array('B',imgTmp.GetData())
        arOvl=array.array('B',imgOverlay.GetData())
        iLen=arTmp.buffer_info()[1]
        if arOvl.buffer_info()[1]!=iLen:
            vtLog.vtLngCur(vtLog.CRITICAL,'bitmap size mismatch',self)
        for i in xrange(0, iLen, 3):
            if arOvl[i]!=1 or arOvl[i+1]!=0 or arOvl[i+2]!=0:
                if mergeMask is not None:
                    if arOvl[i]==mergeMask[0] and arOvl[i+1]==mergeMask[1] and arOvl[i+2]==mergeMask[2]:
                        arTmp[i]=1
                        arTmp[i+1]=0
                        arTmp[i+2]=0
                        continue
                arTmp[i]=arOvl[i]
                arTmp[i+1]=arOvl[i+1] 
                arTmp[i+2]=arOvl[i+2]
        imgTmp.SetData(arTmp.tostring())
    def __buildGrpBmp__(self,bmp,imgOverlay):
        imgTmp=bmp.ConvertToImage()
        self.MergeImg(imgTmp,imgOverlay,mergeMask=[255,255,255])
        return imgTmp.ConvertToBitmap()
    def ConvImg2Bmp(self,imgInst):
        return imgInst.ConvertToBitmap()
    def ConvImgInvalid(self,imgInst,f):
        ar=array.array('B',imgInst.GetData())
        iLen=ar.buffer_info()[1]
        for i in xrange(0, iLen, 3):
            if ar[i]!=1:
                if ar[i+1]!=0:
                    if ar[i+2]!=0:
                        def chRgb(a):
                            a=int(a*f)
                            if a>254:
                                a=254
                            return a
                        ar[i]=chRgb(ar[i])
                        ar[i+1]=chRgb(ar[i+1]) 
                        ar[i+2]=chRgb(ar[i+2])
        imgInst.SetData(ar.tostring())
        xCenter=imgInst.GetWidth()/2
        yCenter=imgInst.GetHeight()/2
        if xCenter<yCenter:
            dW=xCenter
        else:
            dW=xCenter
        def set(x,y):
            try:
                imgInst.SetRGB(x,y,180,0,0)
            except:
                pass
        for i in range(dW-1):
            set(xCenter-i,yCenter-i)
            set(xCenter-i-1,yCenter-i)
            #set(xCenter-i+1,yCenter-i)
            
            set(xCenter+i,yCenter-i)
            set(xCenter+i-1,yCenter-i)
            #set(xCenter+i+1,yCenter-i)
            
            set(xCenter-i,yCenter+i)
            set(xCenter-i-1,yCenter+i)
            #set(xCenter-i+1,yCenter+i)
            
            set(xCenter+i,yCenter+i)
            set(xCenter+i-1,yCenter+i)
            #set(xCenter+i+1,yCenter+i)
        j=imgInst.GetHeight()-1
        for i in range(0,imgInst.GetWidth()):
            set(i,0)
            #set(i,1)
            set(i,j)
            #set(i,j-1)
        i=imgInst.GetWidth()-1
        for j in range(0,imgInst.GetHeight()):
            set(0,j)
            #set(1,j)
            set(i,j)
            #set(i-1,j)
        return imgInst.ConvertToBitmap()
