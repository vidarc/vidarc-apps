#----------------------------------------------------------------------------
# Name:         vGuiPopupEditListWX.py
# Purpose:      popup button with edit list
# Author:       Walter Obweger
#
# Created:      20121215
# CVS-ID:       $Id: vGuiPopupEditListWX.py,v 1.5 2015/07/25 16:03:32 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vLang
    from vGuiPopupButtonWX import vGuiPopupButtonWX
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiPopupEditListWX(vGuiPopupButtonWX):
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(32,32),name='tgPopup',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=-1):
    """
    def __init__(self,*args,**kwargs):
        global _
        _=vLang.assignPluginLang('vGui')
        if self.IsMainThread()==False:
            return
        vGuiPopupButtonWX.__init__(self,*args,**kwargs)
        self.__setupPopUp__(**kwargs)
    def __setupPopUp__(self,**kwargs):
        try:
            from vGuiFrmWX import vGuiFrmWX
            
            self.__logDebug__(''%())
            wid=kwargs.get('wid',None)
            if wid is not None:
                return
            
            self.dVal={}
            self.dWrk={}
            szLst=kwargs.get('size_list',(80,60))
            lWid=[]
            iRow=0
            lWid.append(('lstCtrl',(iRow,0),(1,2),{
                'name':'lstDat','size':szLst,
                'multiple_sel':True,'mark':True,
                'cols':[
                    (_(u'key'),-2,60,1),
                    (_(u'value'),-1,-1,0),
                    ],
                },{
                'lst_item_sel':    self.OnLstDatItemSelected,
                'lst_item_desel':  self.OnLstDatItemDeselected,
                }))
            lWid.append(
                ('szBoxVert',(iRow,2),(1,1),{'name':'bxsDatBt',},[
                    ('cbBmp',0,20,{'name':'cbDatDel',
                        'tip':_(u'delete data'),
                        'bitmap':self.getBmp('core','Del')},{
                        'btn':self.OnDatDel
                        }),
                ])
                )
            iRow+=1
            lWid.append(('txt',(iRow,0),(1,1),{
                'name':'txtKey','value':u'0','size':(60,-1),
                'tip':_('key')},None))
            lWid.append(('txt',(iRow,1),(1,1),{
                'name':'txtVal','value':u'',
                'tip':_(u'value')},None))
            lWid.append(('cbBmp',(iRow,2),(1,1),{
                'name':'cbDatApply',
                'tip':_(u'apply data'),
                'bitmap':self.getBmp('core','Apply')},{
                'btn':self.OnDatApply
                }))
            p=vGuiFrmWX(name='popMulti',parent=self.wid,kind='popup',
                        iArrange=0,widArrange=self.wid,bAnchor=True,
                        lGrowRow=[(0,1),],
                        lGrowCol=[(1,1),],
                        lWid=lWid)
            self.popFrm=p
            self.SetWidPopup(p)
            self.popFrm.BindEvent('cmd',self.OnPopupCmd)
        except:
            self.__logTB__()
    def __Clear__(self):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(self.dVal)
            self.dVal={}
            self.dWrk={}
            if self.IsMainThread()==False:
                return
            self.__ShowValues__()
            self.popFrm.SetModified(False)
        except:
            self.__logTB__()
    def __ShowValues__(self):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(self.dVal)
            if self.IsMainThread()==False:
                return
            lVal=[]
            for k,v in self.dWrk.iteritems():
                lVal.append([k,[k,v]])
            self.popFrm.lstDat.SetValue(lVal)
        except:
            self.__logTB__()
    def SetValue(self,dVal):
        try:
            bDbg=self.GetVerboseDbg(2)
            if bDbg:
                self.__logDebug__({'new dVal':dVal,'old dVal':self.dVal})
            self.dVal=dVal.copy()
            self.dWrk=dVal.copy()
            self.__ShowValues__()
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            bDbg=self.GetVerboseDbg(2)
            if bDbg:
                self.__logDebug__(self.dVal)
            return self.dVal
        except:
            self.__logTB__()
    def SetValueEdit(self,sVal):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            self.popFrm.txtVal.SetValue(sVal)
        except:
            self.__logTB__()
    def OnLstDatItemSelected(self,evt):
        try:
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            lSel=self.popFrm.lstDat.GetSelected([-1,0,1])
            if len(lSel)>0:
                self.popFrm.txtKey.SetValue(lSel[0][1])
                self.popFrm.txtVal.SetValue(lSel[0][2])
                self.popFrm.SetModified(False,self.popFrm.txtKey)
                self.popFrm.SetModified(False,self.popFrm.txtVal)
        except:
            self.__logTB__()
    def OnLstDatItemDeselected(self,evt):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(''%())
        except:
            self.__logTB__()
    def OnDatDel(self,evt):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            lSel=self.popFrm.lstDat.GetSelected([-1])
            for tK in lSel:
                k=tK[0]
                if k in self.dWrk:
                    del self.dWrk[k]
            self.__ShowValues__()
            self.popFrm.SetModified(True,self.popFrm.lstDat)
        except:
            self.__logTB__()
    def OnDatApply(self,evt):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            sK=self.popFrm.txtKey.GetValue()
            sV=self.popFrm.txtVal.GetValue()
            if sK in self.dWrk:
                lIdx=self.popFrm.lstDat.FindValue([sK],[-1],[-2])
                self.__logDebug__(lIdx)
                if len(lIdx)>0:
                    self.popFrm.lstDat.SetItem(lIdx[0][0],1,sV)
                    self.popFrm.SetModified(True,self.popFrm.lstDat)
            else:
                self.popFrm.lstDat.Insert([[sK,[sK,sV]]],-1)
                self.popFrm.SetModified(True,self.popFrm.lstDat)
            self.dWrk[sK]=sV
            self.popFrm.SetModified(False,self.popFrm.txtKey)
            self.popFrm.SetModified(False,self.popFrm.txtVal)
        except:
            self.__logTB__()
    def OnPopupCmd(self,evt):
        try:
            sCmd=evt.GetCmd()
            oDat=evt.GetData()
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'sCmd':sCmd,'oDat':oDat})
            if self.IsMainThread()==False:
                return
            if sCmd=='show':
                pass
                #if self.dAct is None:
                #    self.dAct=self.dVal.copy()
            elif sCmd=='ok':
                if bDbg:
                    self.__logDebugAdd__(self.dWrk,self.dVal)
                self.dVal=self.dWrk.copy()
                self.PostOk(1)
                self.Post('ok')
            elif sCmd=='cancel':
                self.dWrk=self.dVal.copy()
                self.__ShowValues__()
                self.popFrm.SetModified(False,self.popFrm.txtKey)
                self.popFrm.SetModified(False,self.popFrm.txtVal)
                self.popFrm.SetModified(False,self.popFrm.lstDat)
                self.PostCancel(-1)
                self.Post('cancel')
                #if self.dAct is not None:
                #    self.dVal=self.dAct
                #    self.dAct=None
                #    self.__ShowValues__()
                #    self.popFrm.SetModified(False,self.popFrm.txtKey)
                #    self.popFrm.SetModified(False,self.popFrm.txtVal)
        except:
            self.__logTB__()
