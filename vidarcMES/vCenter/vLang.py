#----------------------------------------------------------------------------
# Name:         vLang.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20121120
# CVS-ID:       $Id: vLang.py,v 1.3 2015/01/21 12:05:37 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vCust
    import os,os.path,sys
    import gettext,traceback
    import wx
    import locale
    import vSystem
    
    import __init__
    VERBOSE=0
    VERBOSE_STD=0
    VERBOSE_LOG=0
    
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

MAP_LOCALE_ON_MSWIN={
    'en_US':'English_US',
    'de_DE':'German_Germany',
    'de_CH':'German_Switzerland',
    'de_AT':'German_Austria',
    }
    
APPL_BASE_DN=''
PLUGIN_BASE_DN={}
def getApplBaseDN():
    global APPL_BASE_DN
    if len(APPL_BASE_DN)==0:
        basepath = vSystem.getPlugInDN()
        localedir = vSystem.getPlugInLocaleDN()
        APPL_BASE_DN=basepath
    if VERBOSE_LOG:
        if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
            vpc.logDebug('basepath:%s'%(APPL_BASE_DN),__name__)
    return APPL_BASE_DN
def getPlugInBaseDN(domain):
    global PLUGIN_BASE_DN
    if VERBOSE_LOG:
        if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
            vpc.logDebug('domain:%s;plugin:%s'%(domain,
                    vpc.fmtLimit(PLUGIN_BASE_DN)),__name__)
    if domain in PLUGIN_BASE_DN:
        return PLUGIN_BASE_DN[domain]
    elif None in PLUGIN_BASE_DN:
        return PLUGIN_BASE_DN[None]
    else:
        return 'en'
APPL_LANG='en'
def getApplLang():
    global APPL_LANG
    return APPL_LANG
def setApplLang(lang):
    global APPL_LANG
    APPL_LANG=lang
    setLocale(APPL_LANG)        # 070907:wro
APPL_DOMAIN={}
def getApplDomain():
    global APPL_DOMAIN
    return APPL_DOMAIN
def initAppl(domain):
    langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    lang=locale.getdefaultlocale()[0]
    try:
        sTmp=vCust.getCmdLineOpt(None,'--lang',None)
        if sTmp is not None:
            lang=o[1]
        else:
            sTmp=vCust.getCmdLineOpt(None,'-l',None)
            if sTmp is not None:
                lang=o[1]
        sTmp=vCust.getCmdLineOpt(None,'--langlog',None)
        if sTmp is not None:
            global VERBOSE
            global VERBOSE_STD
            global VERBOSE_LOG
            try:
                iLv=int(sTmp)
                if iLv>1:
                    VERBOSE_LOG=1
                if iLv>2:
                    VERBOSE_STD=1
                if iLv>3:
                    VERBOSE=1
            except:
                VERBOSE_LOG=0
                vpc.logTB(__name__)
    except:
        vpc.logTB(__name__)
    ll=wx.Locale.FindLanguageInfo(lang)
    if ll is None:
        ll=wx.Locale.FindLanguageInfo('en')
    langid=ll.Language
    lang=lang[:2]
    basepath = vSystem.getPlugInDN()
    localedir = vSystem.getPlugInLocaleDN()
    
    global APPL_LANG
    APPL_LANG=lang
    global APPL_BASE_DN
    APPL_BASE_DN=basepath
    global APPL_DOMAIN
    APPL_DOMAIN=domain
    global PLUGIN_BASE_DN
    PLUGIN_BASE_DN[None]=vSystem.getPlugInDN()    #basepath
    if VERBOSE_LOG:
        try:
            if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                vpc.logDebug('basepath:%s'%(basepath),__name__)
                vpc.logDebug('plugin:%s'%(vpc.fmtLimit(PLUGIN_BASE_DN)),__name__)
        except:
            VERBOSE_LOG=0
    # Set locale for wxWidgets
    if wx.GetApp() is not None:
        mylocale = wx.Locale(langid)
        mylocale.AddCatalogLookupPathPrefix(localedir)
        mylocale.AddCatalog(domain)
    # Set up Python's gettext
    mytranslation = gettext.translation(domain, localedir,
        [lang], fallback = True)
    mytranslation.install(unicode=1)
    #locale.setlocale(locale.LC_ALL,'')
    setLocale(APPL_LANG)        # 070907:wro
def getPossibleLang(domain,basepath):
    if VERBOSE:
        traceback.print_stack()
        print domain,basepath
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\ngetPossibleLang\n')
        sys.stderr.write(' '.join(['\n    ',domain,basepath,'\n']))
    if VERBOSE_LOG:
        if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
            vpc.logDebug('domain:%s;basepath:%s'%(domain,basepath),__name__)
    langs=[]
    localedir = os.path.join(basepath, "locale")
    try:
        if os.path.isdir(localedir)==False:
            localedir = vSystem.getPlugInLocaleDN()
            basepath,sTmp=os.path.split(localedir)
        files=os.listdir(localedir)
        for fn in files:
            try:
                sFullFN=os.path.join(localedir,fn)
                if os.path.isdir(sFullFN):
                    if os.path.isdir(os.path.join(sFullFN,'LC_MESSAGES')):
                        langs.append(fn)
            except:
                pass
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ']+langs+['\n']))
        if VERBOSE_LOG:
            if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                vpc.logDebug('langs:%s'%(vtLog.pformat(langs)),__name__)
    except:
        vpc.logTB(__name__)
    if VERBOSE_STD:
        sys.stderr.write('-'*30)
        sys.stderr.write('\n\n')
    return langs
def setPluginLang(domain,dn,lang):
    if VERBOSE:
        traceback.print_stack()
        print domain,dn,lang
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\nsetPluginLang\n')
        sys.stderr.write(' '.join(['    ',domain,`dn`,lang,'\n']))
    if VERBOSE_LOG:
        if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
            vpc.logDebug('domain:%s;dn:%s;lang:%s'%(domain,dn,lang),__name__)
    try:
        if lang is None:
            lang=getApplLang()
    except:
        lang=locale.getdefaultlocale()[0]
        ll=wx.Locale.FindLanguageInfo(lang)
        if ll is None:
            wx.Locale.FindLanguageInfo('en')
        langid=ll.Language
        lang=lang[:2]
    global PLUGIN_BASE_DN
    if VERBOSE_LOG:
        if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
            vpc.logDebug('plugin:%s'%(vtLog.pformat(PLUGIN_BASE_DN)),__name__)
    if dn is None:
        try:
            dn=PLUGIN_BASE_DN[domain]
        except:
            if None in PLUGIN_BASE_DN:
                dn=PLUGIN_BASE_DN[None]
            else:
                dn = vSystem.getPlugInDN()
    else:
        PLUGIN_BASE_DN[domain]=dn
        if None not in PLUGIN_BASE_DN:
            PLUGIN_BASE_DN[None]=dn
    if VERBOSE_STD:
        sys.stderr.write(' '.join(['    ','act',domain,dn,lang,'\n']))
    # Set up Python's gettext
    try:
        if dn.endswith('locale'):
            localedir = dn
        else:
            localedir = os.path.join(dn, "locale")
        if VERBOSE_LOG:
            if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                vpc.logDebug('domain:%s;dn:%s;lang:%s;check'%(domain,localedir,lang),__name__)
        mytranslation = gettext.translation(domain, localedir,
                    [lang], fallback = False)
        if VERBOSE_LOG:
            if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                vpc.logDebug('domain:%s;dn:%s;lang:%s;found'%(domain,localedir,lang),__name__)
    except:
        try:
            localedir = vSystem.getPlugInLocaleDN()
            if VERBOSE_LOG:
                if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                    vpc.logDebug('domain:%s;dn:%s;lang:%s;check'%(domain,localedir,lang),__name__)
            mytranslation = gettext.translation(domain, localedir,
                    [lang], fallback = True)
            if VERBOSE_LOG:
                if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                    vpc.logDebug('domain:%s;dn:%s;lang:%s;found'%(domain,localedir,lang),__name__)
        except:
            if VERBOSE_LOG:
                vpc.logTB(__name__)
    dnStore,sTmp=os.path.split(localedir)
    PLUGIN_BASE_DN[domain]=dnStore
    try:
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ','domains']))
            sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in PLUGIN_BASE_DN.items()]))
            sys.stderr.write(' '.join(['\n']))
        if not hasattr(__init__,'PLUGIN_LANG_DICT'):
            __init__.PLUGIN_LANG_DICT={}
            __init__.PLUGIN_TRANS={}
        __init__.PLUGIN_LANG=lang
        __init__.PLUGIN_LANG_DICT[domain]=lang
        __init__.PLUGIN_TRANS[domain]=mytranslation
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ','set']))
            sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
            sys.stderr.write(' '.join(['\n']))
        if VERBOSE_LOG:
            if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                vpc.logDebug('domain:%s;lang:%s;plugin:%s;trans:%s;dn:%s'%(domain,
                        __init__.PLUGIN_LANG,
                        vpc.fmtLimit(__init__.PLUGIN_LANG_DICT),
                        vpc.fmtLimit(__init__.PLUGIN_TRANS),
                        vpc.fmtLimit(PLUGIN_BASE_DN)),__name__)
    except:
        if VERBOSE_LOG:
            vpc.logTB(__name__)
        pass
    if VERBOSE_STD:
        sys.stderr.write('-'*30)
        sys.stderr.write('\n\n')
    return mytranslation.ugettext
def getPluginLang():
    try:
        if VERBOSE_STD:
            sys.stderr.write('+'*30)
            sys.stderr.write('\ngetPluginLang\n')
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_DOMAIN,__init__.PLUGIN_LANG,__init__.__file__,'\n']))
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_LANG,__init__.__file__,'\n']))
            try:
                sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
                sys.stderr.write('\n')
            except:
                sys.stderr.write(''.join(['      no plugin dict','\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return __init__.PLUGIN_LANG
    except:
        return 'en'
def setPluginDefaultLang(lang):
    try:
        if VERBOSE_STD:
            sys.stderr.write('+'*30)
            sys.stderr.write('\nsetPluginDefaultLang\n')
            sys.stderr.write(' '.join(['    ',lang,'\n']))
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_LANG,__init__.__file__,'\n']))
            try:
                sys.stderr.write(' '.join([':'.join([`k`,v]) for k,v in __init__.PLUGIN_LANG_DICT.items()]))
                sys.stderr.write('\n')
            except:
                sys.stderr.write(''.join(['      no plugin dict','\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        __init__.PLUGIN_LANG=lang
    except:
        __init__.PLUGIN_LANG='en'
def getPluginDomain():
    try:
        if VERBOSE_STD:
            sys.stderr.write('+'*30)
            sys.stderr.write('\ngetPluginDomain\n')
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_DOMAIN,'\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return __init__.PLUGIN_DOMAIN
    except:
        return ''
def assignPluginLang(domain):
    if VERBOSE:
        traceback.print_stack()
    vpc.logDebug('',__name__)
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\nassignPluginLang domain:%s\n'%domain)
    try:
        try:
            lang=__init__.PLUGIN_LANG
        except:
            lang=getApplLang()
        try:
            if VERBOSE_LOG:
                if vpc.vLogIsLogged(vpc.vLog_DEBUG,__name__):
                    vpc.logDebug('domain:%s;lang:%s;dict:%s'%(domain,
                        lang,vpc.fmtLimit(__init__.PLUGIN_LANG_DICT)),__name__)
            if VERBOSE_STD:
                sys.stderr.write('domain:%s;lang:%s;dict:%s\n'%(domain,
                    lang,vpc.fmtLimit(__init__.PLUGIN_LANG_DICT)))
            if domain not in __init__.PLUGIN_LANG_DICT:
                setPluginLang(domain,None,lang)
            elif __init__.PLUGIN_LANG_DICT[domain]!=lang:
                setPluginLang(domain,None,lang)
        except:
            setPluginLang(domain,None,lang)
        return __init__.PLUGIN_TRANS[domain].ugettext
    except:
        if VERBOSE_STD:
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        if VERBOSE_LOG:
            vpc.logTB(__name__)
        return _
def assignPluginLangOld():
    if VERBOSE:
        traceback.print_stack()
    if VERBOSE_STD:
        sys.stderr.write('+'*30)
        sys.stderr.write('\nassignPluginLang\n')
    try:
        if VERBOSE_STD:
            sys.stderr.write(' '.join(['    ',__init__.PLUGIN_DOMAIN,__init__.PLUGIN_LANG,__init__.__file__,getApplBaseDN(),'\n']))
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return __init__.PLUGIN_TRANS.ugettext
    except:
        if VERBOSE_STD:
            sys.stderr.write('-'*30)
            sys.stderr.write('\n\n')
        return _
class vtLangSetLocale(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return 'unknow locale:'+self.value
def setLocale(str=None):
    if str is None:
        locale.setlocale(locale.LC_ALL,'')
    elif len(str)==0:
        locale.setlocale(locale.LC_ALL,str)
    elif wx.Platform=='__WXMSW__':
        if str=='en':
            strWin='en_US'
        elif str=='de':
            strWin='de_DE'
        else:
            strWin=str
        try:
            locale.setlocale(locale.LC_ALL,MAP_LOCALE_ON_MSWIN[strWin])
        except:
            raise vtLangSetLocale(strWin)
    else:
        locale.setlocale(locale.LC_ALL,str)
def format(fmt,val):
    return locale.format(fmt,val)
def getNumSeperatorsFromNumMask():
    ld=locale.localeconv()
    sDec=ld['decimal_point']
    sTho=ld['thousands_sep']
    if len(sTho)==0:
        if sDec=='.':
            sTho=','
        else:
            sTho='.'
    return sDec,sTho
