#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vCenterAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120210
# CVS-ID:       $Id: vCenterAppl.py,v 1.4 2014/03/30 19:22:29 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import __init__
    import __config__
    import vGuiAppl
    import vCust
    import vLang

    import os,sys
    import vSystem
    import vtLogDef
    import vGuiCoreWX

    import images_splash
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

#modules ={u'_NAME_MainFrm': [0, '', u'_NAME_MainFrm.py']}

OPT_SHORT,OPT_LONG='l:f:c:h',['lang=','file=','config=','log=','help',
        'readTag=','readFN=','writeTag=','writeFN=','ackTag=','ackFN=',
        'close','connect=']

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")

if __name__ == '__main__':
    try:
        vpc.logDebug('main',__name__)
        vLang.initAppl('vCenter')
        global _
        _=vLang.assignPluginLang('vCenter')
        vGuiAppl.main('MES','vCenter',__config__.VERSION,
            iSockPort=60505,lActPre=[{
            'label':_(u'  import %s ...')%'vp',
            'eval':'__import__("vp",globals(),locals(),["vp"])'
            },{
            'label':_(u'  import %s ...')%'vGuiCoreWX',
            'eval':'__import__("vGuiCoreWX",globals(),locals(),["vGuiCoreWX"])'
            },{
            'label':_(u'  import %s ...')%'vidarc',
            'eval':'__import__("vidarc",globals(),locals(),["vidarc"])'
            },{
            'label':_(u'  import %s ...')%'vidarc.tool',
            'eval':'__import__("vidarc.tool",globals(),locals(),["vidarc"])'
            },{
            'label':_(u'  import %s ...')%'vidarc.config',
            'eval':'__import__("vidarc.config",globals(),locals(),["vidarc"])'
            },{
            'label':_(u'  import %s ...')%'vidarc.tool.art',
            'eval':'__import__("vidarc.tool.art",globals(),locals(),["vidarc"])'
            },{
            'label':_(u'  import %s ...')%'vidarc.tool.xml',
            'eval':'__import__("vidarc.tool.xml",globals(),locals(),["vidarc"])'
            },{
            'label':u'  import engSys ...',
            'eval':'__import__("vidarc.engSys",globals(),locals(),["vidarc"])'
            },{
            'label':u'  import comm ...',
            'eval':'__import__("vidarc.engSys.comm",globals(),locals(),["vidarc"])'
            },{
            'label':u'  import vCenter ...',
            'eval':'__import__("vCenterMainFrame",globals(),locals(),["vidarc"])'
            },{
            'label':u'  Create vWinCCeng ...',
            'eval':'__import__("vCenterMainFrame",globals(),locals(),["vCenterMainFrame"])'
            },{
            'label':u'  Create vCenter ...',
            'eval':'self.res[-1].create'
            },{
            'label':u'  Create vCenter ...',
            'eval':'self.res[-1](None)'
            },{
            'label':u'  Finished.'
            },
            ],bVidImp=__init__.VIDARC_IMPORT)
    except:
        vpc.logTB(__name__)
