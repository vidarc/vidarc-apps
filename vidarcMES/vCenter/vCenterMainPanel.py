#Boa:FramePanel:vCenterMainPanel
#----------------------------------------------------------------------------
# Name:         vCenterMainPanel.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20100116
# CVS-ID:       $Id: vCenterMainPanel.py,v 1.11 2016/02/07 06:36:05 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import imgCore
    import vLang
    from vGuiPanelWX import vGuiPanelWX
    
    import imgCtr as imgCtr
    
    from vCenterXml import vCenterXml
    from vCenterTree import vCenterTree
    from vCenterList import vCenterList
    from vCenterMainPanelMenu import vCenterMainPanelMenu
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

def getAboutData():
    import __config__
    desc=_(u"""Center module.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
""")
    return _(u"VIDARC Center"),desc,__config__.VERSION,__config__.APP

#def create(parent):
#    return vCenterMainPanel(parent)

#def getPluginImage():
#    return imgCtr.getPluginImage()

#def getApplicationIcon():
#    icon = EmptyIcon()
#    icon.CopyFromBitmap(imgCtr.getPluginBitmap())
#    return icon

class vCenterMainPanel(vGuiPanelWX,vCenterMainPanelMenu):
    APPL_DFT=['appl_viewer_directory','appl_viewer_file',
        'appl_editor_directory','appl_editor_file',
        'appl_CVS',
        ]
    def __initCtrlWid__(self):
        vpc.logDebug('start',__name__)
        global _
        _=vLang.assignPluginLang('vCenter')
        #self.bEnMark=False
        #self.bEnMod=False
        #lWidAdd2=kwargs.get('lWid',[])
        
        lWid={
            'slwNav':{
                'size':(200,10),
                'lWid':[
                    (vCenterTree,   (0,0),(1,1),{'name':'trNav',
                        #'map':[('sTag',0),('sName',0)],
                        'map':[('sTag',0),],
                        },None),
                ],
                'lGrowRow':[(0,1),],
                'lGrowCol':[(0,1),],
                },
            'slwTop0':{
                'size':(100,200),
                'lWid':[
                    ('lblRg',   (0,0),(1,1),{'name':'lblTag','label':'tag'},None),
                    ('txt',     (0,1),(1,1),{'name':'txtTag','value':'tag0'},None),
                    ('txt',     (0,2),(1,1),{'name':'txtTagLarge','value':'tag1'},None),
                    
                    ('lblRg',   (1,0),(1,1),{'name':'lblName','label':'name'},None),
                    ('txt',     (1,1),(1,1),{'name':'txtName','value':'name0'},None),
                    ('txt',     (1,2),(1,1),{'name':'txtNameLarge','value':'name1'},None),
                    
                    ('lblRg',   (2,0),(1,1),{'name':'lblDesc','label':'description'},None),
                    ('txt',     (2,1),(1,1),{'name':'txtDesc','value':'desc0'},None),
                    ('txt',     (2,2),(1,1),{'name':'txtDescLarge','value':'desc1'},None),
                ],
                'lGrowRow':[(1,1),(2,2)],
                'lGrowCol':[(1,1),(2,2)],
                },
            'pnData':{
                'lWid':[
                    (vCenterList, (0,0),(1,1),{'name':'lstMain',
                        #'cols':[
                        #    [_('nr')    ,-2,60,1],
                        #    [_('name')  ,-1,-1,1],
                        #    ],
                        #'map':[('iNr',0),('sN',1),],
                        #
                        },
                        {
                        },
                    )
                    ],
                'lGrowRow':[(0,1),],
                'lGrowCol':[(0,1),],
            },
            'ctrl':{
                },
            }
        vpc.logDebug('done',__name__)
        #self.netMain=None
        #self.netMaster=None
        #self.trMaster=None
        return lWid
        print lWid
        kwargs['lWid']=lWid
        vGuiPanelWX.__init__(self,*args,**kwargs)
        #vGuiPanelWX.__init__(self,iLayout=0,bFlexGridSizer=0,
        #        lGrowRow=[(1,1),(2,2)],
        #        lGrowCol=[(1,1),(2,2)],
        #        lWid=lWid)
        #return [(0,1)],[(0,1)]
    def __initCtrl__(self,*args,**kwargs):
        vpc.logDebug('start',__name__)
        bNet=True
        self.__logDebug__(''%())
        try:
            from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmd import vXmlNodeExplorerLaunchCmd
            from vidarc.vTools.vExplorer.vXmlNodeExplorerSrc import vXmlNodeExplorerSrc
            #self.netMain=vpc.vcxXmlDomReg('vCenter','main')
            #oSrc=vXmlNodeExplorerSrc()
            #self.netMain.RegisterNode(oSrc,False)
            self.netMain=vCenterXml('vCenter','main')
        except:
            self.__logTB__()
            self.netMain=None
        try:
            self.netMaster=None
        except:
            self.__logTB__()
            self.netMaster=None
        try:
            self.trMaster=self.pnNav.trNav
            self.trMaster.SetDoc(self.netMain,True)
        except:
            self.__logTB__()
            self.trMaster=None
        vpc.logDebug('done',__name__)
        return [],[]
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def SaveCfgFile(self,fn=None):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def GetDocMain(self):
        #vpc.logDebug('ret',__name__)
        return self.netMain
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.trMaster
    def OpenFile(self,fn):
        try:
            self.__logDebug__({})
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'fn':fn})
            if self.netMain is not None:
                #if self.netMain.ClearAutoFN()>0:
                iR=self.netMain.Read(fn)
                self.__logDebug__('done')
                if bDbg:
                    self.__logDebug__({'fn':fn,'iR':iR})
                oIter=self.netMain.GetIter("xml","base")
                #oIter.CfgState()
                if bDbg:
                    self.__logDebug__('iter got')
                #oIter.GetNodeById(64)
                oIter.GetNodeByLst(['ExplorerRoot'])
                oIter.SetIterMode(1,0);
                if bDbg:
                    self.__logDebug__('iter mode set')
                oIter.AddMap(0,'tag','sTag')
                oIter.AddMap(0,'name','sName')
                if bDbg:
                    self.__logDebug__('iter defined')
                d=oIter.GetValDict(iDepth=1,iDepthNode=3)
                self.__logDebug__('got')
                #print d
                self.pnNav.trNav.SetValue(d)
            #self.CallTopWid('DoCmdArgs')
        except:
            self.__logTB__()
    def OpenCfgFile(self,fn=None):
        try:
            self.__logDebug__(''%())
            self.__logCritical__(''%())
            return
        except:
            self.__logTB__()
    def __setCfg__(self):
        try:
            self.__logDebug__(''%())
            self.SetCfgData()
        except:
            self.__logTB__()
    def SetCfgData(self):
        try:
            v=self.GetCfgVal(['main_fn'],fallback=u'vCenter.xml')
            self.OpenFile(v)
        except:
            self.__logTB__()
    def GetCfgData(self):
        try:
            self.__logDebug__(''%())
            
        except:
            self.__logTB__()
    def GetAboutData(self):
        return getAboutData()
    def ShutDown(self):
        try:
            self.__logDebug__(''%())
            return False
            #if self.thdCmd.IsXXX:
            #    self.thdCmd.ShutDown()
            #self.thdCmd.DoStop()
            self.thdCmd.ShutDown()
            if self.oDb.GetDB() is not None:
                self.oDb.DoDisConnect()
                #return True
            self.netMain.ShutDown() # ?
            return False
            #for w in self.lExp:
            #    w.ShutDown()
            #for w in self.lExp:
            #    if w.IsBusy():
            #        return True
            if self.oPlc.IsServing():
                self.oPlc.ShutDown()
            if self.oPlc.IsShutDown==False:
                return True
            return False
        except:
            self.__logTB__()
        return 0
    def __clr__(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
