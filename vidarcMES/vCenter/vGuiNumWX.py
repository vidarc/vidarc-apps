#----------------------------------------------------------------------------
# Name:         vGuiNumWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20140318
# CVS-ID:       $Id: vGuiNumWX.py,v 1.15 2016/02/07 06:23:59 wal Exp $
# Copyright:    (c) 2014 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    import types
    from vGuiCoreWidWX import vGuiCoreWidWX
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)

class vGuiNumWX(vGuiCoreWidWX):
    #SIZE_DFT=(76,24)
    SIZE_DFT=(64,24)
    """def __init__(self,id=-1,parent=None,pos=(0,0),size=(74,24),name='tgPopText',
                style=0,bmp=None,wid=None,
                bAnchor=True,iArrange=-1,mode='txt'|'txtRd'....,
                kwVal={'pos'...}
                ):
    """
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __get_widget_lst__(self,*args,**kwargs):
        lW=[]
        sWid=kwargs.get('mode','int')
        iProp=kwargs.get('iProp',1)
        dKw={'name':'txtVal','value':'0',
            'size':self.GetDft('size_val',(30,24),kwargs)}
        if 'kwVal' in kwargs:
            kkw=kwargs.get('kwVal',{'name':'txtVal'})
            dKw.update(kkw)
            if 'szVal' in kkw:
                dKw['size']=kkw['szVal']
            if 'size_val' in kkw:
                dKw['size']=kkw['size_val']
        else:
            if 'name' in kwargs:
                dKw.update({'name':kwargs['name']})
            else:
                kkw=kwargs.get('kwVal',{'name':'txtVal'})
                dKw.update(kkw)
        if 'szVal' in kwargs:
            dKw['size']=kwargs['szVal']
        dKw['sNameSub']='txtVal'
        lW.append((sWid,   iProp,    0, dKw))
        return lW
    def __initCtrlBt__(self,*args,**kwargs):
        szCb=self.GetDft('size_button',wx.Size(24,24),kwargs)
        bmpDec=self.GetDft('bmpDec',('core','DnBlSml'),kwargs)
        bmpInc=self.GetDft('bmpInc',('core','UpBlSml'),kwargs)
        if szCb is not None:
            _idDec=self.GetCtrlId('cbDec')
            self.widBtDec=wx.lib.buttons.GenBitmapButton(id=_idDec,
                    parent=self.wid,pos=(0,0),size=szCb,
                    bitmap=self.GetBmp(bmpDec),
                    name=u'cbDec',
                    style=wx.BU_AUTODRAW)
            #self.bxs.AddWindow(self.widBtDec, 0, border=0, flag=wx.ALIGN_CENTER)
            #self.widBtDec.Bind(wx.EVT_BUTTON,self.OnCbDecButton,self.widBtDec)
            self.widBtDec.SetMinSize(szCb)
            self.AddSz(self.widBtDec,0,border=0,sFlag='C ')
            self.BindEventWid(self.widBtDec,'btn',self.OnCbDecButton)
        if szCb is not None:
            _idInc=self.GetCtrlId('cbInc')
            self.widBtInc=wx.lib.buttons.GenBitmapButton(id=_idInc,
                    parent=self.wid,pos=(0,0),size=szCb,
                    bitmap=self.GetBmp(bmpInc),
                    name=u'cbInc',
                    style=wx.BU_AUTODRAW)
            #self.bxs.AddWindow(self.widBtInc, 0, border=0, flag=wx.ALIGN_CENTER)
            #self.widBtInc.Bind(wx.EVT_BUTTON,self.OnCbIncButton,self.widBtInc)
            self.widBtInc.SetMinSize(szCb)
            self.AddSz(self.widBtInc,0,border=0,sFlag='C ')
            self.BindEventWid(self.widBtInc,'btn',self.OnCbIncButton)
        #vGuiCoreWX.__init__(self)
        #wx.EVT_BUTTON(self.OnPopupButton,self)
        
    def __initObj__(self,*args,**kwargs):
        vGuiCoreWidWX.__initObj__(self,*args,**kwargs)
        self._numDft=kwargs.get('numDft',0)
        self._numMin=kwargs.get('numMin',None)
        self._numMax=kwargs.get('numMax',None)
        self._numDla=kwargs.get('numDelta',1)
        self._numBase=kwargs.get('numBase','d')
        if self._numBase=='d':
            self._num2str=kwargs.get('num2str',self._val2str)
            self._numFmt=kwargs.get('numFmt','%d')
            #self._str2num=kwargs.get('numConv',int)
        elif self._numBase=='h':
            self._num2str=kwargs.get('num2str',self._val2str)
            self._numFmt=kwargs.get('numFmt','%xh')
            #self._str2num=kwargs.get('numConv',self._str2hex)
        elif self._numBase=='o':
            self._num2str=kwargs.get('num2str',self._val2str)
            self._numFmt=kwargs.get('numFmt','%oo')
            #self._str2num=kwargs.get('numConv',self._str2oct)
        #elif self._numBase=='b':
        #    self._num2str=kwargs.get('num2str',self._val2str)
        #    self._numFmt=kwargs.get('numFmt','%bb')
        #    self._str2num=kwargs.get('numConv',self._str2bit)
        else:
            self._num2str=kwargs.get('num2str',self._val2str)
            self._numFmt=kwargs.get('numFmt','%d')
            #self._str2num=kwargs.get('numConv',int)
        v=kwargs.get('numMax',None)
        #self.CB(self.SetValue)
        #if wid is not None:
        #    self.SetPopupWid(wid)
    def __initProperties__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
        try:
            lTmp=kwargs.get('value',None)
            if lTmp is not None:
                if bDbg:
                    self.__logDebug__({'lTmp':lTmp})
                self.SetValue(lTmp)
            else:
                lTmp=kwargs.get('numDft',None)
                if lTmp is not None:
                    if bDbg:
                        self.__logDebug__({'lTmp':lTmp})
                    self.SetValue(lTmp)
                
        except:
            self.__logTB__()
    def SetDict(self,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__(kwargs)
            v=kwargs.get('numDft',None)
            if v is not None:
                self._numDft=self._str2num(v)
            v=kwargs.get('numMin',None)
            if v is not None:
                self._numMin=self._str2num(v)
            v=kwargs.get('numMax',None)
            if v is not None:
                self._numMax=self._str2num(v)
            v=kwargs.get('numDelta',None)
            if v is not None:
                self._numDla=self._str2num(v)
            v=kwargs.get('numBase',None)
            if v is not None:
                self._numBase=v
                if self._numBase=='d':
                    self._num2str=kwargs.get('num2str',self._val2str)
                    self._numFmt=kwargs.get('numFmt','%d')
                    #self._str2num=kwargs.get('numConv',int)
                elif self._numBase=='h':
                    self._num2str=kwargs.get('num2str',self._val2str)
                    self._numFmt=kwargs.get('numFmt','%xh')
                    #self._str2num=kwargs.get('numConv',self._str2hex)
                elif self._numBase=='o':
                    self._num2str=kwargs.get('num2str',self._val2str)
                    self._numFmt=kwargs.get('numFmt','%oo')
                    #self._str2num=kwargs.get('numConv',self._str2oct)
                #elif self._numBase=='b':
                #    self._num2str=kwargs.get('num2str',self._val2str)
                #    self._numFmt=kwargs.get('numFmt','%bb')
                #    self._str2num=kwargs.get('numConv',self._str2bit)
                else:
                    self._num2str=kwargs.get('num2str',self._val2str)
                    self._numFmt=kwargs.get('numFmt','%d')
                    #self._str2num=kwargs.get('numConv',int)
            v=kwargs.get('value',None)
            if v is not None:
                self.SetValue(v)
            else:
                self._valChg(0)
            if bDbg:
                self.__logDebug__({
                        'numMax':self._numMax,
                        'numMin':self._numMin,
                        'numDft':self._numDft,
                        'numDla':self._numDla,
                        'numFmt':self._numFmt,
                        'numBase':self._numBase,
                        'value':self.GetValue(),
                        })
            
        except:
            self.__logTB__()
    def _val2str(self,v):
        try:
            s=self._numFmt%v
        except:
            s=self._numFmt%self._numDft
        return s
    def _str2bit(self,s):
        try:
            v=int(s[:-1],2)
        except:
            v=self._numDft
        return v
    def _str2oct(self,s):
        try:
            v=int(s[:-1],8)
        except:
            v=self._numDft
        return v
    def _str2hex(self,s):
        try:
            v=int(s[:-1],16)
        except:
            v=self._numDft
        return v
    def _str2dec(self,s):
        try:
            v=int(s[:-1])
        except:
            v=self._numDft
        return v
    def _str2num(self,s):
        try:
            sB=s[-1]
            if sB=='h':
                v=self._str2hex(s)
            elif sB=='o':
                v=self._str2oct(s)
            elif sB=='d':
                v=self._str2dec(s)
            else:
                try:
                    v=int(s)
                except:
                    v=self._numDft
            return v
        except:
            if s=='':
                pass
            else:
                self.__logTB__()
            return self._numDft
    def conv(self,s):
        t=type(s)
        if t in [types.StringType,types.UnicodeType]:
            try:
                v=self._str2num(s)
            except:
                v=int(s)
            return v
        else:
            return s
    def _valChg(self,iOp):
        try:
            bDbg=self.GetVerboseDbg(5)
            w=self.lWidVal[0]
            s=w.GetValue()
            try:
                v=self._str2num(s)
            except:
                v=int(s)
            if bDbg:
                self.__logDebug__({'iOp':iOp,'s':s,'v':v,
                        'numMax':self._numMax,
                        'numMin':self._numMin,
                        'numDft':self._numDft,
                        'numDla':self._numDla,
                        'numFmt':self._numFmt,
                        'numBase':self._numBase,
                        'value':self.GetValue(),
                        })
            o=v
            iLmt=0
            if iOp<999900:
                if iOp>0:
                    v=v+(self._numDla*iOp)
                elif iOp<0:
                    v=v+(self._numDla*iOp)
            if self._numMin is not None:
                if v<self._numMin:
                    v=self._numMin
                    iLmt=-1
            if self._numMax is not None:
                if v>self._numMax:
                    v=self._numMax
                    iLmt=1
            if iOp<999999:
                w.SetValue(self._num2str(v))
            if bDbg:
                self.__logDebug__({'iOp':iOp,'s':s,'v':v,'o':o,
                        'iLmt':iLmt})
            if o==v:
                return 0,iLmt
            else:
                return 1,iLmt
        except:
            self.__logTB__()
            return -1,0
    def GetMenuContext(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            l=[
                (40,_('increment *10'),self.getBmp('core','UpBlSml'),
                        self.DoMenuContext,(40,),{},),
                (35,_('increment *5'),self.getBmp('core','UpBlSml'),
                        self.DoMenuContext,(35,),{},),
                (31,_('increment'),self.getBmp('core','UpBlSml'),
                        self.DoMenuContext,(31,),{},),
                (21,_('decrement'),self.getBmp('core','DnBlSml'),
                        self.DoMenuContext,(21,),{},),
                (25,_('decrement *5'),self.getBmp('core','DnBlSml'),
                        self.DoMenuContext,(25,),{},),
                (30,_('decrement *10'),self.getBmp('core','DnBlSml'),
                        self.DoMenuContext,(30,),{},),
                None,
                (12,_('max'),self.getBmp('open','UpMan'),
                        self.DoMenuContext,(12,),{},),
                (11,_('default'),self.getBmp('open','HoMan'),
                        self.DoMenuContext,(11,),{},),
                (10,_('min'),self.getBmp('open','DnMan'),
                        self.DoMenuContext,(10,),{},),
                None,
                (0,_('clear'),self.getBmp('open','Clr'),
                        self.DoMenuContext,(0,),{},),
                ]
            return l
        except:
            self.__logTB__()
        return None
    def DoMenuContext(self,iCmd):
        try:
            #lO=self.GetValue()
            #self.__logDebug__(''%())
            #self._valChg(1)
            bDbg=self.GetVerboseDbg(0)
            #if bDbg:
            #    self.__logDebug__({'lO':lO})
            if iCmd==12:
                self.ValChg(999912)
                #if self._numMax is not None:
                #    self.SetValue(str(self._numMax))
            elif iCmd==11:
                self.ValChg(999911)
                #if self._numDft is not None:
                #    self.SetValue(str(self._numDft))
            elif iCmd==10:
                self.ValChg(999910)
                #if self._numMin is not None:
                #    self.SetValue(str(self._numMin))
            elif (iCmd>=21) and (iCmd<=30):
                iCnt=iCmd-20
                self.ValChg(-iCnt)
                #for iOfs in xrange(iCnt):
                #    self._valChg(-1)
            elif (iCmd>=31) and (iCmd<=40):
                iCnt=iCmd-30
                self.ValChg(iCnt)
                #for iOfs in xrange(iCnt):
                #    self._valChg(1)
            elif iCmd==0:
                self.ValChg(999900)
                #self.txtVal.SetValue('')
            #lN=self.GetValue()
            #self.Post('inc',self._numDla)
            #if bDbg:
            #    self.__logDebug__({'lO':lO,'lN':lN})
            #if lO<>lN:
            #    self.__SetWidMod__(1)
            #    self.PostMOD('val',lN)
        except:
            self.__logTB__()
    def ValChg(self,iCmd):
        try:
            bDbg=self.GetVerboseDbg(0)
            lO=self.GetValue()
            if bDbg:
                self.__logDebug__({'lO':lO,'iCmd':iCmd})
            if iCmd==999912:
                if self._numMax is not None:
                    self.SetValue(str(self._numMax))
                    self.Post('max',self._numMax)
                    lN=self.GetValue()
            elif iCmd==999911:
                if self._numDft is not None:
                    self.SetValue(str(self._numDft))
                    self.Post('dft',self._numDft)
                    lN=self.GetValue()
            elif iCmd==999910:
                if self._numMin is not None:
                    self.SetValue(str(self._numMin))
                    self.Post('min',self._numMin)
                    lN=self.GetValue()
            elif iCmd==999900:
                self.lWidVal[0].SetValue('')
                self.Post('clr','')
                lN=''
            elif iCmd>0:
                self._valChg(iCmd)
                lN=self.GetValue()
                self.Post('inc',self._numDla*iCmd)
            elif iCmd==0:
                self._valChg(iCmd)
                lN=self.GetValue()
            elif iCmd<0:
                self._valChg(iCmd)
                lN=self.GetValue()
                self.Post('dec',self._numDla*(-iCmd))
            if lO<>lN:
                self.__SetWidMod__(1)
                self.PostMOD('val',lN)
        except:
            self.__logTB__()
    def OnCbDecButton(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.ValChg(-1)
        except:
            self.__logTB__()
    def OnCbIncButton(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.ValChg(1)
        except:
            self.__logTB__()
    def OnClr(self,evt):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def __getClsDftArgs__(self,cls):
        bDbg=self.GetVerboseDbg(0)
        if bDbg:
            self.__logDebug__('cls:%s'%(cls))
        if cls=='txtNumRd':
            return vGuiCoreWidWX.__getClsDftArgs__(self,'txTRd')
        if cls=='txtNumRdRg':
            return vGuiCoreWidWX.__getClsDftArgs__(self,'txTRdRg')
        if cls=='int':
            d=vGuiCoreWidWX.__getClsDftArgs__(self,cls)
            d['evt']={
                'keyDn':            self.OnKeyPressed,
                'cxtmn':            self.OnMenuContext,
                }
            if bDbg:
                self.__logDebugAdd__('cls:%s'%(cls),d)
            return d
        return vGuiCoreWidWX.__getClsDftArgs__(self,cls)
    def SetValue(self,sVal):
        try:
            bDbg=True#self.GetVerboseDbg(0)
            
            t=type(sVal)
            if bDbg:
                self.__logDebug__({'t':t,'sVal':sVal})
            if t in [types.StringType,types.UnicodeType]:
                pass
            else:
                try:
                    sTmp=self._val2str(sVal)
                    sVal=sTmp
                except:
                    self.__logTB__()
            self.lWidVal[0].SetValue(sVal)
            self._valChg(0)
        except:
            self.__logTB__()
    def GetValue(self):
        try:
            sVal=self.lWidVal[0].GetValue()
            return sVal
        except:
            self.__logTB__()
        return ''
    def GetWid(self):
        #return self.pn
        return self.wid
    def GetWidMod(self):
        return self.lWidVal[0]
    def __SetWidMod__(self,state):
        self.__setWidModGui__(state,self.lWidVal[0])
    def OnTextText(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return 
            #if self.bBlock:
            #    return
            #self.__markModified__()
            self._valChg(0)
            sVal=self.GetValue()
            self.PostFB('txt',sVal)
            #wx.PostEvent(self,vtInputTextChanged(self,sVal))
        except:
            self.__logTB__()
    def _doPostKey(self,w,key):
        try:
            iMod,iLmt=self._valChg(999999)
            if iLmt!=0:
                self._valChg(0)
            lVal=self.GetValue()
            self.PostNFY('key',key)
            self.PostMOD('val',lVal)
        except:
            self.__logTB__()
    def OnKeyPressed(self,evt):
        try:
            #evt.Skip()
            if self.IsMainThread()==False:
                return #-1
            key=self.GetKeyCode(evt)
            dKeyMod=self.GetKeyModifiers(evt)
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebugAdd__('key:0x%03x %d'%(key,key),dKeyMod)
            #if self.bBlock:
            #    return
            #self.__markModified__()
            bOk=False
            if key>=40 and key<300:
                bOk=True
            elif key==8:
                bOk=True
            elif dKeyMod['abbr']=='dn':         # 20141125 wro:cursor down
                if dKeyMod['ctrl']==True:
                    self.ValChg(999910)
                elif dKeyMod['shift']==True:
                    self.ValChg(-5)
                elif dKeyMod['alt']==True:
                    self.ValChg(-10)
                else:
                    self.ValChg(-1)
                return
            elif dKeyMod['abbr']=='up':         # 20141125 wro:cursor up
                if dKeyMod['ctrl']==True:
                    self.ValChg(999912)
                elif dKeyMod['shift']==True:
                    self.ValChg(5)
                elif dKeyMod['alt']==True:
                    self.ValChg(10)
                else:
                    self.ValChg(1)
                return
            elif dKeyMod['abbr']=='home':       # 20141125 wro: HOME
                self.ValChg(999911)
            elif dKeyMod['abbr']=='end':        # 20141125 wro: END
                self.ValChg(999912)
            elif key==13:
                self.OnTextText(None)
            elif key==27:
                self.ValChg(999900)
                #if self._numDft is not None:
                #    self.SetValue(self._num2str(self._numDft))
                #else:
                #    self.txtVal.SetValue('')
            if bOk==False:
                evt.Skip()
                return #0
            evt.Skip()
            #sVal=self.txtVal.GetValue()
            #self.PostMOD('key',sVal)
            self.__SetWidMod__(1)
            self.CB(self._doPostKey,evt.GetEventObject(),dKeyMod)
            return #1
            #wx.PostEvent(self,vtInputTextChanged(self,sVal))
        except:
            self.__logTB__()
