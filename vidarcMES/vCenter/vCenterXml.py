#----------------------------------------------------------------------------
# Name:         vCenterTree.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20131030
# CVS-ID:       $Id: vCenterXml.py,v 1.4 2015/01/21 12:07:36 wal Exp $
# Copyright:    (c) 2013 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    from vGuiTreeXmlWX import vGuiTreeXmlWX
    from vGuiImgLstWX import vGuiImgLstGet
    from vidarc.vTools.vExplorer.vXmlNodeExplorerRoot import vXmlNodeExplorerRoot
    from vidarc.vTools.vExplorer.__register__ import RegisterNodes as RegNodeExplorer
    #from vidarc.vTools.vExplorer.vXmlNodeExplorerLaunchCmd import vXmlNodeExplorerLaunchCmd
    #from vidarc.vTools.vExplorer.vXmlNodeExplorerSrc import vXmlNodeExplorerSrc
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vCenterXml(vpc.vcxXmlDomReg):
    def __init__(self,sOrigin,sSuffix):
        try:
            vpc.vcxXmlDomReg.__init__(self,sOrigin,sSuffix)
            
            oRoot=vXmlNodeExplorerRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeExplorerRoot()
            self.RegisterNode(oRoot,False)
            RegNodeExplorer(self,bRegAsRoot=True,oRoot=oRoot)
            
            self.SetNodeBase(['ExplorerRoot','cfg',])
            #self.SetNodeBase(['ExplorerRoot',])
            #oSrc=vXmlNodeExplorerSrc()
            #oCmd=vXmlNodeExplorerLaunchCmd()
            #self.RegisterNode(oSrc,False)
            #self.RegisterNode(oCmd,False)
        except:
            self.__logTB__()
