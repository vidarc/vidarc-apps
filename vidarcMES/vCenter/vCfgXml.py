#----------------------------------------------------------------------------
# Name:         vCfgXml.py
# Purpose:      
# Author:       Walter Obweger
#
#   requirements:
#       mixin logging feature (__logDebug__,__logTB__ required for self)
#
# Created:      20100302
# CVS-ID:       $Id: vCfgXml.py,v 1.5 2016/02/07 06:27:06 wal Exp $
# Copyright:    (c) 2010 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import sys,types
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vCfgXml:
    def __init__(self,lBase,dDft):
        self._dCfg={}
        self._dDft=dDft
        self._lBase=[s[:] for s in lBase]
        self.oXml=vpc.vcxXmlDom()
        self.oXml.Inz("vCfgXml","xml")
        self.oIter=vpc.vcxXmlDomIter()
        self.oIter.Inz("vCfgXml","iter")
        self.oXml.AssignIter(self.oIter,-1)
    def OpenCfgFile(self,sFN):
        try:
            self.__logDebug__('sFN:%s;lBase:%r'%(sFN,self._lBase))
            self._sCfgFN=sFN[:]
            iRet=self.oXml.Read(sFN)
            if iRet<0:
                self.oXml.Create('config')
            #self.getCfgDataDict(self._lBase)
            self.buildCfgDict()
        except:
            self.__logTB__()
    def SaveCfgFile(self,sFN=None):
        try:
            self.setCfgData()
            self.oXml.Write(sFN or self._sCfgFN)
        except:
            self.__logTB__()
    def buildCfgDict(self):
        self.__logDebug__(''%())
        self._dCfg=self.getCfgDataDict(self._lBase)
        self.setCfgDft(self._dDft,self._dCfg)
        self.__logDebug__([self._dDft,self._dCfg])
    def setCfgDft(self,dDft,d=None):
        if d is None:
            d=self._dDft
        self.__logDebug__(['dCfg;',self._dCfg,'dDft;',dDft])
        def _setCfgDft(d,dft):
            for k,v in dft.iteritems():
                if k not in d:
                    d[k]=v
                elif type(v)==types.DictType:
                    if type(d[k])==types.DictType:
                        _setCfgDft(d[k],v)
                    else:
                        d[k]=v
                        vpc.logWarn('k:%s replaced'%(k),__name__)
        _setCfgDft(d,dDft)
    def getCfgVal(self,l,d=None,fallback=None,funcConv=None,**kwargs):
        try:
            if d is None:
                d=self._dCfg
            self.__logDebug__({'d':d})
            for k in l:
                if k in d:
                    d=d[k]
                else:
                    return fallback
            if funcConv is None:
                return d
            else:
                return funcConv(d,**kwargs)
        except:
            vpc.logTB(__name__)
            return fallback
    def setCfgVal(self,v,l,d=None,fallback=None,funcConv=None,**kwargs):
        try:
            if d is None:
                d=self._dCfg
            dd=d
            self.__logDebug__({'d':d})
            for k in l:
                if k not in d:
                    d[k]={}
                dd=d
                d=d[k]
            if funcConv is None:
                dd[l[-1]]=v
            else:
                try:
                    dd[l[-1]]=funcConv(v,**kwargs)
                except:
                    if fallback is None:
                        del dd[l[-1]]
                    else:
                        dd[l[-1]]=fallback
            self.__logDebug__({'d':self._dCfg})
            return 0
        except:
            vpc.logTB(__name__)
            return -1
    def getCfgDataDict(self,l):
        try:
            self.__logDebug__(''%())
            self.oIter.GetNodeById(-1)
            self.oIter.GetNodeByLst(l,1)
            d=self.oIter.GetDict()
            self.__logDebug__(d)
            return d
        except:
            vpc.logTB(__name__)
            return {}
    def setCfgData(self,l=None,d=None):
        try:
            self.__logDebug__(self._dCfg)
            self.oIter.GetNodeById(-1)
            self.oIter.GetNodeByLst(l or self._lBase,1)
            self.oIter.SetDict(self._dCfg)
            d=self.oIter.GetDict()
            self.__logDebug__(d)
            return 0
        except:
            vpc.logTB(__name__)
            return -1

class vCfgXmlOrg(vCfgXml,vpc.vcOrg):
    def __init__(self,lBase,dDft,sOrg=None,sSuf=''):
        if sOrg is None:
            sOrg=self.__class__.__name__
        vpc.vcOrg.__init__(self)
        self.SetOrigin(sOrg,sSuf)
        vCfgXml.__init__(self,lBase,dDft)

dLog={}
def set2Log(name,flag,what='__VERBOSE__'):
    global dLog
    d=dLog
    strs=name.split('.')
    for s in strs:
        if not d.has_key(s):
            d[s]={}
        d=d[s]
    d[what]=flag
def is2Log(name,what='__VERBOSE__',fallback=False):
    global dLog
    d=dLog
    bIs2=fallback
    strs=name.split('.')
    for s in strs:
        if s in d:
            d=d[s]
        else:
            break
        if what in d:
            bIs2=d[what]
    return bIs2
