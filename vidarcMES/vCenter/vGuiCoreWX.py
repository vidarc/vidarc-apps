#----------------------------------------------------------------------------
# Name:         vGuiCoreWX.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20120210
# CVS-ID:       $Id: vGuiCoreWX.py,v 1.54 2016/02/07 06:29:01 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import os,time,types
    import wx
    import vSystem
    from vGuiScreen import getScreenDict
    import vGuiArt as vGuiArt
    #from vGuiArt import getBmp,getImg,getIcn
    from vGuiDft import vGuiDftGet
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

gWidTopWidget=None
gsTopWidgetName=''
giTopWidIdx=-1
gdTopWid={}
gdCtrlId={}
gdMenuSysDict={}
gdSysEvtName={}

def vGuiCoreKeyCode(evt):
    if wx.VERSION >= (2,8):
        return evt.GetKeyCode()
    else:
        return evt.KeyCode()

def vGuiCoreGetIcon(bmp):
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(bmp)
    return icon

vEVT_GUI_CORE_CMD=wx.NewEventType()
def EVT_GUI_CORE_CMD(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_CMD,func)
def EVT_GUI_CORE_CMD_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_CMD,func)
class vGuiCoreCmd(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_CMD(<widget_name>, xxx)
    """
    def __init__(self,obj,cmd,data=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_CMD)
        self.cmd=cmd
        self.data=data
    def GetCmd(self):
        return self.cmd
    def GetData(self):
        return self.data
    GetDat=GetData

vEVT_GUI_CORE_OK=wx.NewEventType()
def EVT_GUI_CORE_OK(win,func):
    if hasattr(win,'GetWid'):
        win.__logDebug__(func)
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_OK,func)
def EVT_GUI_CORE_OK_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_OK,func)
class vGuiCoreOk(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_OK(<widget_name>, xxx)
    """
    def __init__(self,obj,res,data=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_OK)
        self.res=res
        self.data=data
    def GetResult(self):
        return self.res
    GetRlt=GetResult
    def GetData(self):
        return self.data
    GetDat=GetData

vEVT_GUI_CORE_CANCEL=wx.NewEventType()
def EVT_GUI_CORE_CANCEL(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_CANCEL,func)
def EVT_GUI_CORE_CANCEL_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_CANCEL,func)
class vGuiCoreCancel(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_CANCEL(<widget_name>, xxx)
    """
    def __init__(self,obj,res,data=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_CANCEL)
        self.res=res
        self.data=data
    def GetResult(self):
        return self.res
    GetRlt=GetResult
    def GetData(self):
        return self.data
    GetDat=GetData

vEVT_GUI_CORE_FB=wx.NewEventType()
def EVT_GUI_CORE_FB(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_FB,func)
def EVT_GUI_CORE_FB_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_FB,func)
class vGuiCoreFB(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_FB(<widget_name>, xxx)
    """
    def __init__(self,obj,fb,data=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_FB)
        self.fb=fb
        self.data=data
    def GetFB(self):
        return self.fb
    def GetData(self):
        return self.data
    GetDat=GetData

vEVT_GUI_CORE_MOD=wx.NewEventType()
def EVT_GUI_CORE_MOD(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_MOD,func)
def EVT_GUI_CORE_MOD_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_MOD,func)
class vGuiCoreMOD(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_MOD(<widget_name>, xxx)
    """
    def __init__(self,obj,res,data=None,gui=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_MOD)
        self.res=res
        self.data=data
        self.gui=gui
    def GetResult(self):
        return self.res
    GetRlt=GetResult
    def GetData(self):
        return self.data
    GetDat=GetData
    def GetGUI(self):
        return self.gui

vEVT_GUI_CORE_NFY=wx.NewEventType()
def EVT_GUI_CORE_NFY(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_NFY,func)
def EVT_GUI_CORE_NFY_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_NFY,func)
class vGuiCoreNFY(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_NFY(<widget_name>, xxx)
    """
    def __init__(self,obj,res,data=None,gui=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_NFY)
        self.res=res
        self.data=data
        self.gui=gui
    def GetResult(self):
        return self.res
    GetRlt=GetResult
    def GetData(self):
        return self.data
    GetDat=GetData
    def GetGUI(self):
        return self.gui

vEVT_GUI_CORE_ADT=wx.NewEventType()
def EVT_GUI_CORE_ADT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Connect(-1,-1,vEVT_GUI_CORE_ADT,func)
def EVT_GUI_CORE_ADT_DISCONNECT(win,func):
    if hasattr(win,'GetWid'):
        win=win.GetWid()
    win.Disconnect(-1,-1,vEVT_GUI_CORE_ADT,func)
class vGuiCoreADT(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_GUI_CORE_ADT(<widget_name>, xxx)
    """
    def __init__(self,obj,res,old=None,new=None,gui=None):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(vEVT_GUI_CORE_ADT)
        self.res=res
        self.old=old
        self.new=new
        self.gui=gui
    def GetResult(self):
        return self.res
    GetRlt=GetResult
    def GetOld(self):
        return self.old
    def GetNew(self):
        return self.new
    def GetGUI(self):
        return self.gui

class vGuiEvt:
    _MAP_EVENT={}
    _MAP_EVENT_UNBIND={}
    def BindEvent(self,name,func,par=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebugAdd__(name,{'func':func,'par':par})
            # ++++++++++
            # 20141125 wro: event mapping added
            evt=None
            if name in self._MAP_EVENT:
                evt=self._MAP_EVENT[name]
                if type(evt)==types.StringType:
                    name=evt
                    if name in self._MAP_EVENT:
                        evt=self._MAP_EVENT[name]
            else:
                #self.BindEventWid(par,name,func)
                #return
                evt=self.__get_widget_event__(name)
                if type(evt)==types.StringType:
                    name=evt
                    evt=self.__get_widget_event__(name)
                if evt is not None:
                    return -2
            # ----------
            if evt is not None:
                if par is None:
                    evt(self,func)
                else:
                    evt(par,func)
                return 0
            else:
                self.__logCritical__('evt name:%s not resolved'%(name))
        except:
            self.__logTB__()
        return -1
    def UnBindEvent(self,name,func,par=None):
        try:
            # ++++++++++
            # 20141125 wro: event mapping added
            evt=None
            if name in self._MAP_EVENT_UNBIND:
                evt=self._MAP_EVENT_UNBIND[name]
                if type(evt)==types.StringType:
                    name=evt
                    if name in self._MAP_EVENT_UNBIND:
                        evt=self._MAP_EVENT_UNBIND[name]
            # ----------
            if evt is not None:
                if par is None:
                    evt(self,func)
                else:
                    evt(par,func)
                return 0
            else:
                self.__logCritical__('evt name:%s not resolved'%(name))
        except:
            self.__logTB__()
        return -1
    def __get_widget_event__(self,sK):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if sK=='txt':
                return wx.EVT_TEXT
            elif sK=='ent':
                return wx.EVT_TEXT_ENTER
            elif sK=='btn':
                return wx.EVT_BUTTON
            elif sK=='tg':
                return wx.EVT_TOGGLEBUTTON
            elif sK=='choice':
                return wx.EVT_CHOICE
            elif sK=='check':
                return wx.EVT_CHECKBOX
            elif sK=='checkBox':
                return wx.EVT_CHECKLISTBOX
            elif sK=='lstSel':
                return wx.EVT_LISTBOX
            elif sK=='lstDblclk':
                return wx.EVT_LISTBOX_DCLICK
            elif sK=='lstCtrlSel':
                return wx.EVT_LIST_ITEM_SELECTED
            elif sK=='lstCtrlDesel':
                return wx.EVT_LIST_ITEM_DESELECTED
            elif sK=='lstCtrlDblClk':
                return wx.EVT_LIST_ITEM_ACTIVATED
            elif sK=='lstCtrlAct':
                return wx.EVT_LIST_ITEM_ACTIVATED
            elif sK=='lstCtrlActivate':
                return wx.EVT_LIST_ITEM_ACTIVATED
            elif sK=='lstCtrlColClk':
                return wx.EVT_LIST_COL_CLICK
            elif sK=='lstCtrlColClkLf':
                return wx.EVT_LIST_COL_CLICK
            elif sK=='lstCtrlColClkRg':
                return wx.EVT_LIST_COL_RIGHT_CLICK
            elif sK=='lstCtrlKey':
                return wx.EVT_LIST_KEY_DOWN
            elif sK=='trCtrlSel':
                return wx.EVT_TREE_ITEM_SELECTED
            elif sK=='trCtrlDesel':
                return wx.EVT_TREE_ITEM_DESELECTED
            elif sK=='trCtrlDbClk':
                return wx.EVT_TREE_ITEM_ACTIVATED
            elif sK=='trCtrlAct':
                return wx.EVT_TREE_ITEM_ACTIVATED
            elif sK=='trCtrlActivate':
                return wx.EVT_TREE_ITEM_ACTIVATED
            elif sK=='trCtrlDel':
                return wx.EVT_TREE_DELETE_ITEM
            elif sK=='trCtrlCollapsing':
                return wx.EVT_TREE_ITEM_COLLAPSING
            elif sK=='trCtrlCollapsed':
                return wx.EVT_TREE_ITEM_COLLAPSED
            elif sK=='trCtrlExpanding':
                return wx.EVT_TREE_ITEM_EXPANDING
            elif sK=='trCtrlExpanded':
                return wx.EVT_TREE_ITEM_EXPANDED
            elif sK=='trCtrlSelChg':
                return wx.EVT_TREE_SEL_CHANGING
            elif sK=='trCtrlSel':
                return wx.EVT_TREE_SEL_CHANGED
            elif sK=='trCtrlLclk':
                return wx.EVT_TREE_ITEM_LEFT_CLICK
            elif sK=='trCtrlMclk':
                return wx.EVT_TREE_ITEM_MIDDLE_CLICK
            elif sK=='trCtrlRclk':
                return wx.EVT_TREE_ITEM_RIGHT_CLICK
            elif sK=='trCtrlMenu':
                return wx.EVT_TREE_ITEM_MENU
            elif sK=='trCtrlDragBeg':
                return wx.EVT_TREE_BEGIN_DRAG
            elif sK=='trCtrlDragBegRg':
                return wx.EVT_TREE_BEGIN_RDRAG
            elif sK=='trCtrlDragEnd':
                return wx.EVT_TREE_END_DRAG
            elif sK=='trCtrlKey':
                return wx.EVT_TREE_KEY_DOWN
            elif sK=='char':
                return wx.EVT_CHAR
            elif sK=='char_hook':
                return wx.EVT_CHAR_HOOK
            elif sK=='keyDn':
                return wx.EVT_KEY_DOWN
            elif sK=='keyUp':
                return wx.EVT_KEY_UP
            elif sK=='mouse':
                return wx.EVT_MOUSE_EVENTS
            elif sK=='wheel':
                return wx.EVT_MOUSEWHEEL
            elif sK=='rgDbl':
                return wx.EVT_RIGHT_DCLICK
            elif sK=='mdDbl':
                return wx.EVT_MIDDLE_DCLICK
            elif sK=='lfDbl':
                return wx.EVT_LEFT_DCLICK
            elif sK=='motion':
                return wx.EVT_MOTION
            elif sK=='rgDn':
                return wx.EVT_RIGHT_DOWN
            elif sK=='rgUp':
                return wx.EVT_RIGHT_UP
            elif sK=='mdUp':
                return wx.EVT_MIDDLE_UP
            elif sK=='mdDn':
                return wx.EVT_MIDDLE_DOWN
            elif sK=='lfUp':
                return wx.EVT_LEFT_UP
            elif sK=='lfDn':
                return wx.EVT_LEFT_DOWN
            elif sK=='enter':
                return wx.EVT_ENTER_WINDOW
            elif sK=='leave':
                return wx.EVT_LEAVE_WINDOW
            elif sK=='move':
                return wx.EVT_MOVE
            elif sK=='size':
                return wx.EVT_SIZE
            elif sK=='erase':
                return wx.EVT_ERASE_BACKGROUND
            elif sK=='paint':
                return wx.EVT_PAINT
            elif sK=='focusKill':
                return wx.EVT_KILL_FOCUS
            elif sK=='focusSet':
                return wx.EVT_SET_FOCUS
            elif sK=='help':
                return wx.EVT_HELP
            elif sK=='colorChanged':
                return wx.EVT_SYS_COLOUR_CHANGED
            # dialog events
            elif sK=='idle':
                return wx.EVT_IDLE
            elif sK=='navKey':
                return wx.EVT_NAVIGATION_KEY
            elif sK=='iconize':
                return wx.EVT_ICONIZE
            elif sK=='maximize':
                return wx.EVT_MAXIMIZE
            elif sK=='dropFiles':
                return wx.EVT_DROP_FILES
            elif sK=='close':
                return wx.EVT_CLOSE
            elif sK=='activate':
                return wx.EVT_ACTIVATE
            elif sK=='init':
                return wx.EVT_INIT_DIALOG
            elif sK=='ctxmn':
                return wx.EVT_CONTEXT_MENU
            elif sK=='cxtmn':
                return wx.EVT_CONTEXT_MENU
            return None
        except:
            self.__logTB__()
        return None
    def GetSysEvtNameByEvt(self,evt):
        try:
            global gdSysEvtName
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            iEvtId=evt.GetEventType()
            if iEvtId in gdSysEvtName:
                return gdSysEvtName[iEvtId]
            for s in dir(wx):
                if s.startswith('EVT'):
                    #print s
                    e=getattr(wx,s)
                    if hasattr(e,'evtType'):
                        lEvtId=e.evtType
                        #print iEvtId,lEvtId
                        if iEvtId in lEvtId:
                            gdSysEvtName[iEvtId]=s[:]
                            return s
        except:
            self.__logTB__()
    def BindEventDict(self,wid,dEvt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'wid':wid,'dEvt':dEvt})
            for sK,fct in dEvt.iteritems():
                if wid is None:
                    self.BindEvent(sK,fct)
                else:
                    self.BindEventWid(wid,sK,fct)
        except:
            self.__logTB__()
    def BindEventWid(self,wid,sEvt,fct):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebugAdd__(sEvt,{'fct':fct,'wid':wid})
            evt=self.__get_widget_event__(sEvt)
            if type(evt)==types.StringType:
                sEvt=evt
                evt=self.__get_widget_event__(sEvt)
            if evt is not None:
                t=type(wid)
                if t==types.StringType:
                    w=self.__dict__.get(wid,None)
                elif t==types.UnicodeType:
                    w=self.__dict__.get(wid,None)
                else:
                    w=wid
                if w is None:
                    return 
                w.Bind(evt,fct)
        except:
            self.__logTB__()
def GetRelevantNamesWX(par,appl=None):
    vpc.logDebug('GetRelevantNamesWX',__name__)
    if hasattr(par,'_originLogging'):
        #if VERBOSE:
        #    vtLngCur(DEBUG,';'.join(['par def',par._originLogging]),'vtLog')
        return par._originLogging
    lName=[]
    if appl is not None:
        lName.append(appl)
    lName.append(par.GetName())
    
    wid=par
    par=par.GetParent()
    while par is not None:
        if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog']:
            lName.append(par.GetName())
        elif par.GetClassName() in ['wxMDIParentFrame',]:
            lName.append(par.GetName())
            break
        elif getattr(par,'IsLogRelevant',False):
            lName.append(par.GetName())
        p=par
        par=par.GetParent()
        if p==par:
            break
    lName.reverse()
    s='.'.join(lName)
    #if VERBOSE:
    #    vtLngCur(DEBUG,';'.join(['calc',s]),'vtLog')
    wid._originLogging=s
    del lName
    return s
def InitMsgWid():
    global gWidTopWidget
    global gsTopWidgetName
    try:
        from wx import GetApp
        app=GetApp()
        if app is not None:
            gWidTopWidget=app.GetTopWindow()
            if gWidTopWidget is None:
                gsTopWidgetName=''
            else:
                gsTopWidgetName=gWidTopWidget.GetName()
        else:
            gWidTopWidget=None
            gsTopWidgetName=''
    except:
        gWidTopWidget=None
        gsTopWidgetName=''
def vGuiCoreTopWidReg(wid):
    try:
        global giTopWidIdx
        global gdTopWid
        if wid is None:
            return -2
        giTopWidIdx+=1
        if hasattr(wid,'GetWid'):
            w=wid.GetWid()
        else:
            w=None
        gdTopWid[giTopWidIdx]=wid
        if w is not None:
            w._oTopWidIdx=giTopWidIdx
        wid._oTopWidIdx=giTopWidIdx
        return giTopWidIdx
    except:
        vpc.logTB(__name__)
    return -1
def vGuiCoreTopWidUnReg(wid):
    try:
        global giTopWidIdx
        global gdTopWid
        if wid is None:
            return -2
        for iTopWidIdx,wTmp in gdTopWid.iteritems():
            if wid==wTmp:
                del gdTopWid[giTopWidIdx]
                if hasattr(wid,'GetWid'):
                    w=wid.GetWid()
                else:
                    w=None
                if w is not None:
                    del w._oTopWidIdx
                
                return iTopWidIdx
    except:
        vpc.logTB(__name__)
    return -1
def vGuiCoreCtrlId(sKey,sNameSub=None):
    try:
        global gdCtrlId
        if sKey in gdCtrlId:
            d=gdCtrlId[sKey]
        else:
            d={}
            gdCtrlId[sKey]=d
        if sNameSub in d:
            iId=d[sNameSub]
        else:
            iId=wx.NewId()
            d[sNameSub]=iId
        return iId
    except:
        vpc.logTB(__name__)
        return wx.NewId()
def vGuiMenuSysDict(sKey,sNameSub=None):
    try:
        global gdMenuSysDict
        if sKey in gdMenuSysDict:
            d=gdMenuSysDict[sKey]
        else:
            d={}
            gdMenuSysDict[sKey]=d
        if sNameSub in d:
            dd=d[sNameSub]
        else:
            dd={}
            d[sNameSub]=dd
        return dd
    except:
        vpc.logTB(__name__)
        return wx.NewId()
def vGuiCoreTopWidGet(wid):
    global gdTopWid
    if wid is None:
        k=gdTopWid.keys()
        if len(k)<1:
            return None
        wid=gdTopWid[k[0]]
        if wid is None:
            return None
    if hasattr(wid,'_oTopWidIdx'):
        iIdx=wid._oTopWidIdx
        if iIdx in gdTopWid:
            return gdTopWid[iIdx]
        return None
    par=wid
    while par is not None:
        p=par
        par=par.GetParent()
        if p==par:
            break
        if hasattr(par,'_oTopWidIdx'):
            if par._oTopWidIdx>=0:
                wid._oTopWidIdx=par._oTopWidIdx
                if par._oTopWidIdx in gdTopWid:
                    return gdTopWid[par._oTopWidIdx]
                return par
    return None
def GetPrintMsgWid(wid):
    vpc.logCritical('you are not supposed to be here',__name__)
    if hasattr(wid,'_originPrint'):
        return wid._originPrint
    if hasattr(wid,'PrintMsg'):
        return wid
    par=wid
    while par is not None:
        p=par
        par=par.GetParent()
        if p==par:
            break
        if hasattr(par,'PrintMsg'):
            wid._originPrint=par
            return par
    return None
def PrintMsg(sMsg,wid=None,bForce=False):
    try:
        widTop=vGuiCoreTopWidGet(wid)
        if widTop is not None:
            widTop.PrintMsg(sMsg,bForce=bForce)
    except:
        vpc.logTB(__name__)
def SetProcBarVal(iVal,iCount,wid=None,bForce=False):
    try:
        widTop=vGuiCoreTopWidGet(wid)
        if widTop is not None:
            widTop.SetProcBarVal(iVal,iCount,bForce=bForce)
    except:
        vpc.logTB(__name__)

class vGuiCoreEvt(vGuiEvt):
    _MAP_EVENT={
            'cmd':EVT_GUI_CORE_CMD,
            'ok':EVT_GUI_CORE_OK,
            'cancel':'cnc',                         # 20141125 wro: event mapping added
            'cnc':EVT_GUI_CORE_CANCEL,
            'fb':EVT_GUI_CORE_FB,
            'mod':EVT_GUI_CORE_MOD,
            'notify':'nfy',
            'nfy':EVT_GUI_CORE_NFY,
            'audit':'adt',
            'adt':EVT_GUI_CORE_ADT,
            }
    _MAP_EVENT_UNBIND={
            'cmd':EVT_GUI_CORE_CMD_DISCONNECT,
            'ok':EVT_GUI_CORE_OK_DISCONNECT,
            'cancel':'cnc',                         # 20141125 wro: event mapping added
            'cnc':EVT_GUI_CORE_CANCEL_DISCONNECT,
            'fb':EVT_GUI_CORE_FB_DISCONNECT,
            'mod':EVT_GUI_CORE_MOD_DISCONNECT,
            'notify':'nfy',
            'nfy':EVT_GUI_CORE_NFY_DISCONNECT,
            'audit':'adt',
            'adt':EVT_GUI_CORE_ADT_DISCONNECT,
            }

class vGuiCoreWX(vGuiCoreEvt):
    def __init__(self,*args,**kwargs):
        #vtLogOriginWX.__init__(self,bExtended=bExtended)
        self._iVerbose=-1
        sOrigin=kwargs.get('sOrigin',None)
        if sOrigin is None:
            #sOrigin='vGuiCoreWX'
            sOrigin=self.__class__.__name__
        self._vcOrg=vpc.vcOrg()#,"vGuiCoreWX","")
        self._dProp={}
        if sOrigin is not None:
            self.SetOrigin(sOrigin)
        try:
            self.__initCls__(*args,**kwargs)
            self.__initObj__(*args,**kwargs)
            self.__initCreate__(*args,**kwargs)
            self.__initProperties__(*args,**kwargs)
        except:
            self.__logTB__()
        try:
            self.__initLayout__(*args,**kwargs)
        except:
            self.__logTB__()
        #self._originPrint
    def __initCreate__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.wid=self.__initWid__(*args,**kwargs)
            if self.wid is not None:
                self.SetOriginByWid(self.wid)
            else:
                self.__logErrorAdd__(self.__class__.__name__,
                        'you are not supposed to be here;'\
                        'wid is None')
            self.__initSizer__(*args,**kwargs)
            self.__initCtrl__(*args,**kwargs)
            self.__initCtrlBt__(*args,**kwargs)
            self.__initPopup__(*args,**kwargs)
            self.__initEvt__(*args,**kwargs)
        except:
            self.__logTB__()
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __defProp__(self,sProp,val,bRd=True,bWr=True,bSilent=False,sDoc=None,dft=None):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'sProp':sProp,'val':val,
                        'bRd':bRd,'bWr':bWr,'bSilent':bSilent})
            if sProp in self._dProp:
                if bSilent==False:
                    self.__logErrorAdd__('sProp:%s already defined'%(sProp),
                            self._dProp)
            dProp={
                    'val':val,
                    'dft':val,
                    'bRd':bRd,
                    'bWr':bWr,
                    }
            if sDoc is not None:
                dProp['doc']=sDoc
            if dft is not None:
                dProp['dft']=dft
            self._dProp[sProp]=dProp
            setattr(self,sProp,val)
        except:
            self.__logTB__()
    def SetProp(self,sProp,val):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('sProp:%s;val:%r'%(sProp,val))
            if sProp in self._dProp:
                dProp=self._dProp[sProp]
                if 'bWr' in dProp:
                    if dProp['bWr']==False:
                        self.__logErrorAdd__('sProp:%s write protected'%(sProp),
                                dProp)
                        return
                dProp['val']=val
                setattr(self,sProp,val)
                if bDbg:
                    self.__logDebugAdd__('sProp:%s'%(sProp),dProp)
            else:
                self.__logErrorAdd__('sProp:%s unknown'%(sProp),
                        self._dProp)
        except:
            self.__logTB__()
    def GetProp(self,sProp):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            if sProp in self._dProp:
                v=getattr(self,sProp)
                return v
        except:
            self.__logTB__()
        return None
    def GetPropDef(self,sProp):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__('')
            if sProp in self._dProp:
                dProp=self._dProp[sProp]
                dP=dProp.copy()
                dP['cur']=v
                return dP
        except:
            self.__logTB__()
        return None
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        wid=None
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
        return wid
    def __initSizer__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({})
        except:
            self.__logTB__()
    def __initLayout__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({})
        except:
            self.__logTB__()
    def __initCtrl__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
    def __initCtrlBt__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(15)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
    def __initPopup__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(15)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
    def __initEvt__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__({})
        except:
            self.__logTB__()
    def __initProperties__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(5)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
        except:
            self.__logTB__()
        try:
            lTmp=kwargs.get('value',None)
            if lTmp is not None:
                if bDbg:
                    self.__logDebug__({'lTmp':lTmp})
                self.SetValue(lTmp)
        except:
            self.__logTB__()
    def __str__(self):
        return self._vcOrg.GetOrigin()
    def __repr__(self):
        return self._vcOrg.GetOrigin()
    def SetOrigin(self,s):
        bDbg=False
        if VERBOSE>0:
            bDbg=True
            print s,VERBOSE
        if hasattr(self,'_iVerbose')==False:
            self._iVerbose=-1
        self._vcOrg.SetOrigin(s)
        if bDbg:
            print 'SetOrigin',self._vcOrg.GetOrigin()
        iTmp=vpc.getVerboseOrigin(s)
        if bDbg:
            print '','name',iTmp
        if iTmp>0:
            self._iVerbose=max(iTmp,self._iVerbose)
        iTmp=vpc.getVerboseOrigin(self._vcOrg.GetOrigin())
        if bDbg:
            print '','orig',iTmp
        if iTmp>0:
            self._iVerbose=max(iTmp,self._iVerbose)
        if bDbg:
            print '','set',self._iVerbose
    def SetVerbose(self,sName,iVerbose=-1,kwargs={}):
        if hasattr(self,'_iVerbose')==False:
            self._iVerbose=-1
        bDbg=False
        if VERBOSE>0:
            bDbg=True
        if bDbg:
            self.__logDebug__({'sName':sName,'iVerbose':iVerbose,
                    'kwargs':kwargs,'clsName':self.__class__.__name__})
        iTmpT=vpc.getVerboseType(sName)
        if bDbg:
            print '','type name',iTmpT
        if iTmpT>0:
            self._iVerbose=max(iTmpT,self._iVerbose)
        iTmpC=vpc.getVerboseType(self.__class__.__name__)
        if bDbg:
            print '','type cls',iTmpC
        if iTmpC>0:
            self._iVerbose=max(iTmpC,self._iVerbose)
        if iVerbose>0:
            self._iVerbose=max(iVerbose,self._iVerbose)
        if kwargs.get('VERBOSE',0)>0:
            self._iVerbose=max(kwargs.get('VERBOSE',0),self._iVerbose)
        if bDbg:
            self.__logDebug__({'VERBOSE':VERBOSE,'_iVerbose':self._iVerbose,
                    'type':iTmpT,'cls':iTmpC,'kw':kwargs.get('VERBOSE',0),
                    'arg':iVerbose})
        if bDbg:
            print '','set',self._iVerbose
    def GetVerbose(self,iVerbose=-1):
        if iVerbose>=0:
            if self._iVerbose>=iVerbose:
                return True
            else:
                return False
        else:
            return self._iVerbose
    def GetVerboseDbg(self,iVerbose=-1):
        if self.GetVerbose(iVerbose=iVerbose):
            if self.__isLogDebug__():
                return True
            else:
                return False
        else:
            return False
    def SetOriginByWid(self,wid):
        try:
            s=GetRelevantNamesWX(wid)
            self.__logDebug__(s)
            self.SetOrigin(s)
            #if hasattr(wid,'_originPrint')==False:
            #    self.__logError__('no top widget available'%())
            #else:
            #    self._originPrint=self.wid._originPrint
        except:
            self.__logTB__()
    def CB(self,func,*args,**kwargs):
        wx.CallAfter(func,*args,**kwargs)
    def CallBackDelayed(self,zSleep,func,*args,**kwargs):
        try:
            wx.CallAfter(wx.FutureCall,int(zSleep*1000),func,*args,**kwargs)
        except:
            self.__logTB__()
    def DoMenuAction(self,sAct):
        try:
            self.__logDebug__({'sAct':sAct})
        except:
            self.__logTB__()
    def AddMenuItems(self,mn,dCall,iOfs,l,mnb,dId,widEvt,bDbg=False):
        for tup in l:
            t=type(tup)
            if tup is None:
                mn.InsertSeparator(iOfs)
                iOfs+=1
            elif tup[0] in dCall:
                self.__logError__('key:%s already in dCall;%s'%(tup[0],
                        self.__logFmt__(dCall)))
                #return
            elif t==types.ListType:
                if bDbg:
                    self.__logDebug__('t:%r;list'%(t))
                    
                iId=wx.NewId()
                mnSub=wx.Menu()
                self.AddMenuItems(mnSub,dCall,0,tup[6],mnb,dId,widEvt,bDbg)
                if mn is None:
                    mnb.Insert(iOfs,menu=mnSub,title=tup[1])
                    dCall[tup[0]]=iId
                else:
                    mnIt=wx.MenuItem(mn,iId,tup[1],subMenu=mnSub)
                    if tup[2] is not None:
                        mnIt.SetBitmap(tup[2])
                    wx.EVT_MENU(widEvt,iId,self.OnAddMenuItems)
                    dCall[tup[0]]=iId
                    dId[iId]=(tup[3],tup[4],tup[5])
                    mn.InsertItem(iOfs,mnIt)
                iOfs+=1
            else:
                if bDbg:
                    self.__logDebug__('t:%r'%(t))
                iId=wx.NewId()
                mnIt=wx.MenuItem(mn,iId,tup[1])
                if tup[2] is not None:
                    mnIt.SetBitmap(tup[2])
                wx.EVT_MENU(widEvt,iId,self.OnAddMenuItems)
                #widEvt.Bind(wx.EVT_MENU,self.OnAddMenuItems,id=iId)
                dCall[tup[0]]=iId
                dId[iId]=(tup[0],tup[3],tup[4],tup[5])
                mn.InsertItem(iOfs,mnIt)
                iOfs+=1
    def OnAddMenuItems(self,evt):
        try:
            eId=evt.GetId()
            sLang=None
            self.__logDebug__('eId:%d processing;'%(eId))
            if eId in self._dMenuIdSys:
                t=self._dMenuIdSys[eId]
                if t[1] is None:
                    self.DoMenuAction(t[0])
                else:
                    t[1](*t[2],**t[3])
            else:
                self.__logError__(['eId:%d not found;'%(eId),
                        self._dMenuIdSys])
        except:
            self.__logTB__()
    def GetMenu(self,bRunDry=False,bDel=False):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            sAttrIdSys='_dMenuIdSys'
            sAttrIdCall='_dMenuIdCall'
            if hasattr(self,'GetWid'):
                self.__logDebug__('GetWid'%())
                widEvt=self.GetWid()
            else:
                widEvt=self
            if hasattr(self,sAttrIdSys):
                if bDel==True:
                    delattr(self,sAttrIdSys)
                    dMenuSys=None
                else:
                    dMenuSys=getattr(self,sAttrIdSys)
            else:
                if bRunDry==False:
                    dMenuSys={}
                    setattr(self,sAttrIdSys,dMenuSys)
                else:
                    dMenuSys=None
                    return None,None,widEvt
            if hasattr(self,sAttrIdCall):
                if bDel==True:
                    delattr(self,sAttrIdCall)
                    dMenuCall=None
                else:
                    dMenuCall=getattr(self,sAttrIdCall)
            else:
                if bRunDry==False:
                    dMenuCall={}
                    setattr(self,sAttrIdCall,dMenuCall)
                else:
                    dMenuCall=None
                    return None,None,widEvt
            return dMenuSys,dMenuCall,widEvt
        except:
            self.__logTB__()
        return {},{},None
    def OnMenuContext(self,evt):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            dMenuSys,dMenuCall,widEvt=self.GetMenu(bRunDry=True)
            if bDbg:
                self.__logDebug__({'dMenuSys':dMenuSys,'dMenuCall':dMenuCall,
                        'widEvt':widEvt})
            if widEvt is None:
                self.__logError__('widEvt is None')
                return
            dCtx=self.GetCacheDict('_dCtx')
            if 'mnCtx' not in dCtx:
                l=self.GetMenuContext()
                if bDbg:
                    self.__logDebugAdd__('create menu',{'l':l})
                if l is None:
                    return
                dMenuSys,dMenuCall,widEvt=self.GetMenu()
                dMenuCache=self.GetMenuSysData(self.__class__.__name__)
                if bDbg:
                    self.__logDebug__({'dMenuSys':dMenuSys,'dMenuCall':dMenuCall,
                            'widEvt':widEvt,'dCtx':dCtx})
                mn=wx.Menu()
                mnb=None
                iOfs=0
                self.AddMenuItems(mn,dMenuCall,iOfs,l,mnb,dMenuSys,widEvt,bDbg)
                dCtx['mnCtx']=mn
                if bDbg:
                    self.__logDebug__({'dMenuSys':dMenuSys,'dMenuCall':dMenuCall,
                            'widEvt':widEvt,'dCtx':dCtx})
            else:
                if bDbg:
                    self.__logDebug__({'dMenuSys':dMenuSys,'dMenuCall':dMenuCall,
                            'widEvt':widEvt,'dCtx':dCtx})
                mn=dCtx['mnCtx']
            pos=evt.GetPosition()
            if pos==(-1,-1):
                pos=widEvt.GetRect()
                posClt=(pos[2]>>1,pos[3]>>1)
            else:
                posClt=widEvt.ScreenToClient(pos)
            if bDbg:
                self.__logDebug__({'pos':pos,'posClt':posClt})
            widEvt.PopupMenu(mn,posClt)
            if 1:       # 20141201 wro: clean up
                mn.Destroy()
                del dCtx['mnCtx']
                self.GetMenu(bDel=True)
        except:
            self.__logTB__()
    def GetMenuContext(self):
        """ return context menu definition here
        [ (nr,text,bmp,func,args,kwargs),       ... args as tuple, kwargs as dict
          None,                                 ... separator
          ]
        """
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
        return None
    def GetCacheDict(self,sAttr,bChk=False):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if bChk==True:
                if hasattr(self,sAttr):
                    return True
                else:
                    return False
            if hasattr(self,sAttr):
                dTmp=getattr(self,sAttr)
            else:
                dTmp={}
                setattr(self,sAttr,dTmp)
            return dTmp
        except:
            self.__logTB__()
        return None
    def GetMenuSysData(self,sKey,sNameSub=None):
        t=vGuiMenuSysDict(sKey,sNameSub)
        return t
    def GetCtrlId(self,sKey,sNameSub=None):
        return vGuiCoreCtrlId(sKey,sNameSub)
    def GetKw(self,kwargs,lKey,dDft=None,_kwargs=None):
        if _kwargs is None:
            _kwargs={}
        for s in lKey:
            if s in kwargs:
                _kwargs[s]=kwargs[s]
        if dDft is not None:
            for k,it in dDft.iteritems():
                if k not in _kwargs:
                    _kwargs[k]=it
                else:
                    if _kwargs[k] is None:
                        _kwargs[k]=it
        return _kwargs
    def GetGuiArgs(self,kwargs,lKey,dDft,_kwargs=None,sNameSub=None,
            bValidate=True,par=None):
        _kwargs=self.GetKw(kwargs,lKey,dDft,_kwargs=_kwargs)
        if 'pos' not in _kwargs:
            _kwargs['pos']=wx.DefaultPosition
        if 'size' not in _kwargs:
            _kwargs['size']=wx.DefaultSize
        if bValidate==True:
            if _kwargs['pos'] is None:
                _kwargs['pos']=wx.DefaultPosition
            if _kwargs['size'] is None:
                _kwargs['size']=wx.DefaultSize
        #else:
        #    sz=_kwargs['size']
        if sNameSub is None:
            if 'sNameSub' in kwargs:
                sNameSub=kwargs['sNameSub']
                #print self.__class__.__name__,sNameSub
            else:
                # 20150112 wro: is this really a good idea?
                if 'name' in kwargs:
                    #sNameSub=kwargs['name']
                    #print self.__class__.__name__,sNameSub,'name'
                    pass
        if par is None:
            sNamePar=self.__class__.__name__
        else:
            sNamePar=par.GetName()
        if 'id' not in _kwargs:
            iId=vGuiCoreCtrlId(sNamePar,sNameSub)
            _kwargs['id']=iId
        else:
            if _kwargs['id'] is None:
                iId=vGuiCoreCtrlId(self.__class__.__name__,sNameSub)
                _kwargs['id']=iId
            else:
                iId=_kwargs['id']
        if 'parent' in _kwargs:
            parent=_kwargs['parent']
            if hasattr(parent,'GetWid'):
                par=parent.GetWid()
                _kwargs['parent']=par
        #if 'style' not in _kwargs:
        #    _kwargs['style']=wx.TAB_TRAVERSAL
        #else:
        #    if _kwargs['style'] is None:
        #        _kwargs['style']=wx.TAB_TRAVERSAL
        #self.__logDebug__({'_kwargs':_kwargs,'name':self.__class__.__name__,
        #        'kwargs':kwargs,'lKey':lKey,'dDft':dDft})
        return (),_kwargs
    def CallTopWid(self,funcName,*args,**kwargs):
        try:
            widTop=vGuiCoreTopWidGet(self)
            if widTop is None:
                self.__logDummyFunc__(funcName,*args,**kwargs)
            else:
                f=getattr(widTop,funcName,None)
                if f is not None:
                    return f(*args,**kwargs)
                else:
                    self.__logError__('method:%s not available'%(funcName))
        except:
            self.__logTB__()
    def GetTopWidFunc(self,name):
        try:
            widTop=vGuiCoreTopWidGet(self)
            if widTop is None:
                return self.__logDummyFunc__
            else:
                return getattr(widTop,name,self.__logDummyFunc__)
        except:
            self.__logTB__()
    def GetTopWid(self):
        widTop=vGuiCoreTopWidGet(self)
        if widTop is None:
            self.__logError__('no top widget available'%())
        return widTop
    def IsTopWid(self):
        return False
    def PrintMsg(self,sMsg,bForce=False,bImmediately=False):
        try:
            widTop=vGuiCoreTopWidGet(self)
            widTop.PrintMsg(sMsg,bForce=bForce,bImmediately=bImmediately)
        except:
            self.__logTB__()
    def SetProcBarVal(self,iVal,iCount=-1,iRange=1000,iNum=0,bForce=False):
        try:
            widTop=vGuiCoreTopWidGet(self)
            widTop.SetProcBarVal(iVal,iCount=iCount,iRange=iRange,iNum=iNum,bForce=bForce)
        except:
            self.__logTB__()
    def GetAnchor(self,wid,iArrange=-1,bAnchorFrm=True):
        try:
            bFound=False
            if hasattr(wid,'GetWid'):
                par=wid.GetWid()
            else:
                par=wid
            lPar=[]
            while par is not None:
                self.__logDebug__({'cls':par.GetClassName()})
                if bAnchorFrm==True:
                    if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog']:
                        bFound=True
                        break
                else:
                    if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog','wxSashLayoutWindow']:
                        bFound=True
                        break
                p=par
                par=par.GetParent()
                lPar.append(par)
                if par==p:
                    break
            if bFound==True:
                if bAnchorFrm==True:
                    pass
                else:
                    #print lPar
                    if len(lPar)>1:
                        par=lPar[-2]
                    lPar=None
                try:
                    if iArrange>=0:
                        par.Bind(wx.EVT_MOVE,self.OnMoveWidArrange)
                        if (iArrange&0x40)!=0:
                            wx.CallAfter(par.Bind,wx.EVT_SIZE,self.OnSizeWidArrange)
                        else:
                            par.Bind(wx.EVT_SIZE,self.OnMoveWidArrange)
                        
                except:
                    self.__logTB__()
                return par
            else:
                self.__logError__('anchor not possible, no valid parent widget found')
                return None
        except:
            self.__logTB__()
    def __logDummyFunc__(self,*args,**kwargs):
        self.__logError__(['dummy func called;args;',args,'kwargs;',kwargs])
    def __logMeasure__(self,k,iNum=0):
        if self.__isLogInfo__():
            if hasattr(self,'_logMeasure')==False:
                self._logMeasure={}
            if k not in self._logMeasure:
                l=[]
                self._logMeasure[k]=l
            else:
                l=self._logMeasure[k]
            zCPU=reduce(lambda x,y:x+y,os.times())
            l.append((time.time(),zCPU,iNum))
            if iNum<0:
                if k in self._logMeasure:
                    l=self._logMeasure[k]
                    zS=l[0][0]
                    zCPU=l[0][1]
                    ll=['%8d : %6.3f[s] (%6.3f[s])'%(iNum,zA-zS,zCpu-zCPU) for zA,zCpu,iNum in l]
                    self.__logInfo__('measure;%s'%(';'.join(ll)),level2skip=1)
                    del self._logMeasure[k]
    def SetCfgData(self):
        self.GetTopWidFunc('SetCfgData')()
    def GetCfgData(self):
        self.GetTopWidFunc('GetCfgData')()
    def SetCfgDft(self,dDft):
        self.GetTopWidFunc('setCfgDft')(dDft)
    def BuildCfgDict(self):
        self.GetTopWidFunc('buildCfgDict')()
    def SetCfgVal(self,v,lHier,d=None,fallback=None,funcConv=None,**kwargs):
        return self.GetTopWidFunc('setCfgVal')(v,lHier,d=d,
                fallback=fallback,funcConv=funcConv,**kwargs)
    def GetCfgVal(self,lHier,d=None,fallback=None,funcConv=None,**kwargs):
        return self.GetTopWidFunc('getCfgVal')(lHier,d=d,
                fallback=fallback,funcConv=funcConv,**kwargs)
    def UpdKw(self,sKey,val=None,kw=None,bForce=False):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if kw is not None:
                if sKey in kw:
                    if bForce==True:
                        kw[sKey]=val
                else:
                    kw[sKey]=val
        except:
            self.__logTB__()
    def GetDft(self,sKey,fb=None,kw=None,bChkDft=True):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('sKey:%s'%(sKey))
            if kw is not None:
                if sKey in kw:
                    v=kw[sKey]
                    if v!=sKey:     # 20150210 wro: if value is identical to key continue
                        if bDbg:
                            self.__logDebugAdd__('sKey:%s'%(sKey),
                                    {'fb':fb,'v':v,'kw':kw})
                        return v
        except:
            self.__logTB__()
        try:
            bDbg=self.GetVerboseDbg(0)
            if bChkDft==False:
                if bDbg:
                    self.__logDebugAdd__('skip check default;return',fb)
                return fb
                
            sCls=self.__class__.__name__
            iR,v=vGuiDftGet(sCls,sKey,fb)
            if iR<0:
                self.__logErrorAdd__('%s %s not found'%(sCls,sKey),v)
            if bDbg:
                self.__logDebugAdd__('check;sCls%s;  iR:%d'%(sCls,iR),
                        {'fb':fb,'v':v,'sKey':sKey})
            return v
        except:
            self.__logTB__()
            
        return fb
    def __do_call__(self,f,*args,**kwargs):
        if VERBOSE>5:
            self.__logDebug__(''%())
        l=[]
        for k,o in self.__dict__.iteritems():
            if k in ['originLogging','_originPrint','widLogging']:
                continue
            if hasattr(o,f):
                l.append(getattr(o,f)(*args,**kwargs))
        return l
    def GetWid(self):
        return self.wid
    def GetWidMod(self):
        return self.wid
    def GetWidChild(self):
        return None
    def SetModified(self,state,obj=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'state':state,'obj':obj})
            
            if hasattr(self,'__SetWidMod__'):
                self.__SetWidMod__(state)
        except:
            self.__logTB__()
    def __getattr__(self,name):
        try:
            if name.startswith('__isLog'):
                return getattr(self._vcOrg,name)
            elif name.startswith('__log'):
                return getattr(self._vcOrg,name)
            #vpc.logDebug('name:%s'%(name))
            if hasattr(self._vcOrg,name):
                return getattr(self._vcOrg,name)
            wid=self.GetWid()
            if hasattr(wid,name):
                return getattr(wid,name)
            wid=self.GetWidChild()
            if wid is not None:
                if hasattr(wid,name):
                    return getattr(wid,name)
        except:
            vpc.logTB()
            if VERBOSE>10:
                if name!='pn':
                    vpc.logDebug('name:%s'%(name),__name__)
        #vpc.logError('name:%s'%(name))
        raise AttributeError(name)
    def CallWidChild(self,zDly,sFunc,*args,**kwargs):
        """
        zDly ... <0 immediately
             ...  0 callback
             ... >0 call delayed
        """
        try:
            self.__logDebug__(''%())
            l=self.GetWidChild()
            self.__logDebug__(l)
            if l is None:
                l=[self]
                return
            bCB=False
            bDly=False
            if zDly==0:
                bCB=True
            elif zDly>0:
                bDly=True
            else:
                if self.IsMainThread(bSilent=True)==False:
                    bCB=True
            for ww in l:
                for w in ww.__dict__.itervalues():
                    if hasattr(w,sFunc):
                        self.__logDebug__(w)
                        try:
                            f=getattr(w,sFunc)
                            if bCB==True:
                                self.CB(f,*args,**kwargs)
                            elif bDly==True:
                                self.CallBackDelayed(zDly,f,*args,**kwargs)
                            else:
                                f(*args,**kwargs)
                        except:
                            self.__logTB__()
                    #if hasattr(w,'CallWidChild'):
                    #    f=getattr(
        except:
            self.__logTB__()
    def __Close__(self):
        self.CallWidChild(-1,'__Close__')
    def Post(self,cmd,data=None):
        if self.__isLogDebug__():
            self.__logDebug__({'cmd':cmd,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreCmd(w,cmd,data=data))
    def PostOk(self,res,data=None):
        if self.__isLogDebug__():
            self.__logDebug__({'res':res,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreOk(w,res,data=data))
    def PostCancel(self,res,data=None):
        if self.__isLogDebug__():
            self.__logDebug__({'res':res,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreCancel(w,res,data=data))
    def PostFB(self,fb,data=None):
        #bDbg=self.GetVerboseDbg(0)
        #if bDbg:
        if self.__isLogDebug__():
            self.__logDebug__({'fb':fb,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreFB(w,fb,data=data))
    def PostMOD(self,fb,data=None):
        if self.__isLogDebug__():
            self.__logDebug__({'fb':fb,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreMOD(w,fb,data=data,gui=self))
    def PostNFY(self,fb,data=None):
        if self.__isLogDebug__():
            self.__logDebug__({'fb':fb,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreNFY(w,fb,data=data,gui=self))
    def PostADT(self,fb,old=None,new=None):
        if self.__isLogDebug__():
            self.__logDebug__({'fb':fb,'data':data})
        w=self.GetWid()
        wx.PostEvent(w,vGuiCoreADT(w,fb,old=old,new=new,gui=self))
    def IsMainThread(self,bMain=True,bSilent=False):
        if wx.Thread_IsMain()!=bMain:
            if bSilent==False:
                if bMain:
                    self.__logCritical__('called by thread'%())
                else:
                    self.__logCritical__('called by main thread'%())
            return False
        return True
    def HandleKey(self,evt,bRetDict=False):
        evt.Skip()
        iCode=-1
        bAlt=False
        bCtrl=False
        bMeta=False
        bShift=False
        bCmd=False
        try:
            cVal=u'%c'%evt.GetUnicodeKey()
            iCode=evt.GetKeyCode()
            bAlt=evt.AltDown()
            bCtrl=evt.ControlDown()
            bMeta=evt.MetaDown()
            bShift=evt.ShiftDown()
            bCmd=evt.CmdDown()
        except:
            self.__logTB__()
        if bRetDict==True:
            return {'v':cVal,'code':iCode,'shift':bShift,'alt':bAlt,
                    'ctrl':bCtrl,'meta':bMeta,'cmd':bCmd}
    def GetKeyModifiers(self,evt):
        bAlt=evt.AltDown()
        bCtrl=evt.ControlDown()
        bMeta=evt.MetaDown()
        bShift=evt.ShiftDown()
        bCmd=evt.CmdDown()
        sWhat=''
        sKind=''
        key=vGuiCoreKeyCode(evt)
        try:
            if key in [27]:
                sWhat='esc'
                sKind='ctrl'
            if key in [13]:
                sWhat='ret'
                sKind='ctrl'
            if key in [0x2d,wx.WXK_SUBTRACT,wx.WXK_NUMPAD_SUBTRACT]:
                sWhat='-'
                sKind='op'
            if key in [0x2b,wx.WXK_ADD,wx.WXK_NUMPAD_ADD]:
                sWhat='+'
                sKind='op'
            if key in [0x2f,wx.WXK_DIVIDE,wx.WXK_NUMPAD_DIVIDE]:
                sWhat='/'
                sKind='op'
            if key in [0x2a,wx.WXK_MULTIPLY,wx.WXK_NUMPAD_MULTIPLY]:
                sWhat='*'
                sKind='op'
            if key in [0x3d,wx.WXK_NUMPAD_EQUAL]:
                sWhat='='
                sKind='op'
            if key in [wx.WXK_PAGEUP,wx.WXK_NUMPAD_PAGEUP,
                    wx.WXK_PRIOR,wx.WXK_NUMPAD_PRIOR]:
                sWhat='pgup'
                sKind='mov'
            if key in [wx.WXK_PAGEDOWN,wx.WXK_NUMPAD_PAGEDOWN,
                    wx.WXK_NEXT,wx.WXK_NUMPAD_NEXT]:
                sWhat='pgdn'
                sKind='mov'
            if key in [wx.WXK_TAB]:
                sWhat='tab'
            if key in [wx.WXK_BACK]:
                sWhat='back'
                sKind='edit'
            if key in [wx.WXK_UP,wx.WXK_NUMPAD_UP]:
                sWhat='up'
                sKind='mov'
            if key in [wx.WXK_DOWN,wx.WXK_NUMPAD_DOWN]:
                sWhat='dn'
                sKind='mov'
            if key in [wx.WXK_RIGHT,wx.WXK_NUMPAD_RIGHT]:
                sWhat='rg'
                sKind='mov'
            if key in [wx.WXK_LEFT,wx.WXK_NUMPAD_LEFT]:
                sWhat='lf'
                sKind='mov'
            if key in [wx.WXK_DELETE,wx.WXK_NUMPAD_DELETE]:
                sWhat='del'
                sKind='edit'
            if key in [wx.WXK_INSERT,wx.WXK_NUMPAD_INSERT]:
                sWhat='ins'
            if key in [wx.WXK_HOME,wx.WXK_NUMPAD_HOME]:
                sWhat='home'
                sKind='mov'
            if key in [wx.WXK_END,wx.WXK_NUMPAD_END]:
                sWhat='end'
                sKind='mov'
            if (key>=0x30) and (key<=0x39):
                sWhat='num'
                sKind='reg'
            if key in [wx.WXK_SPACE]:
                sWhat='chr'
            if (key>=0x41) and (key<=0x7a):
                sWhat='chr'
                sKind='reg'
        except:
            self.__logTB__()
        return {'shift':bShift,'alt':bAlt,'key':key,
                    'ctrl':bCtrl,'meta':bMeta,'cmd':bCmd,
                    'abbr':sWhat,'kind':sKind}
    def GetKeyCode(self,evt):
        return vGuiCoreKeyCode(evt)
    def IsKey(self,key,sWhat):
        if sWhat=='esc':
            if key in [27]:
                return True
        elif sWhat=='ret':
            if key in [13]:
                return True
        elif sWhat=='pgup':
            if key in [wx.WXK_PAGEUP,wx.WXK_NUMPAD_PAGEUP]:
                return True
        elif sWhat=='pgdn':
            if key in [wx.WXK_PAGEDOWN,wx.WXK_NUMPAD_PAGEDOWN]:
                return True
        elif sWhat=='tab':
            if key in [wx.WXK_TAB]:
                return True
        elif sWhat=='back':
            if key in [wx.WXK_BACK]:
                return True
        elif sWhat=='up':
            if key in [wx.WXK_UP,wx.WXK_NUMPAD_UP]:
                return True
        elif sWhat=='dn':
            if key in [wx.WXK_DOWN,wx.WXK_NUMPAD_DOWN]:
                return True
        elif sWhat=='rg':
            if key in [wx.WXK_RIGHT,wx.WXK_NUMPAD_RIGHT]:
                return True
        elif sWhat=='lf':
            if key in [wx.WXK_LEFT,wx.WXK_NUMPAD_LEFT]:
                return True
        elif sWhat=='del':
            if key in [wx.WXK_DELETE,wx.WXK_NUMPAD_DELETE]:
                return True
        elif sWhat=='ins':
            if key in [wx.WXK_INSERT,wx.WXK_NUMPAD_INSERT]:
                return True
        elif sWhat=='home':
            if key in [wx.WXK_HOME,wx.WXK_NUMPAD_HOME]:
                return True
        elif sWhat=='end':
            if key in [wx.WXK_END,wx.WXK_NUMPAD_END]:
                return True
        return False
    def GetFontByName(self,sName):
        """
        sName
            "fixed"     ... 
        """
        try:
            iSz=8
            sFont='Tahoma'
            iWeight=wx.FONTWEIGHT_NORMAL
            
            if vSystem.isWin():
                if sName=='fixed':
                    iSz=8
                    sFont='Courier New'
                    iWeight=wx.FONTWEIGHT_NORMAL
            elif vSystem.isUnx():
                if sName=='fixed':
                    iSz=8
                    sFont='Helvetica'
                    iWeight=wx.FONTWEIGHT_NORMAL
            if VERBOSE>0:
                self.__logDebug__('font:%s;size:%d;weight:%d'%(sFont,iSz,iWeight))
            font=wx.Font(iSz,wx.FONTFAMILY_SWISS,wx.FONTSTYLE_NORMAL,
                        iWeight,False,sFont,
                        wx.FONTENCODING_ISO8859_1)
            return font
        except:
            self.__logTB__()
        return None
    def __setWidModGui__(self,state,obj):
        f=obj.GetFont()
        if state==True:
            f.SetWeight(wx.FONTWEIGHT_BOLD)
        else:
            f.SetWeight(wx.FONTWEIGHT_NORMAL)
        obj.SetFont(f)
        obj.Refresh()
    def ChooseCmd4Mime(self,fileType,filename,mime):
        try:
            self.__logDebug__('')
            self.__logWarnAdd__('overload me',
                    'filetype:%r;filename:%r;mime:%r'%(fileType,filename,mime))
        except:
            self.__logTB__()
        return 0
    def OpenFileSys(self,sDN,sFN):
        try:
            sExts=sFN.split('.')
            sExt=sExts[-1]
            if sDN is not None:
                filename='/'.join([sDN,sFN])
            else:
                filename=sFN
            #filename=vtIOFI.posix2fn(filename)
            #o=self.netExplorer.GetRegByExtension(sExt)
            #if o is not None:
            #    self.__logDebug__('object for extension:%s found'%(sExt))
            #    self.__execByReg__(o,None,None,None,None,sFN=filename)
            #    return
            fileType = wx.TheMimeTypesManager.GetFileTypeFromExtension(sExt)
            try:
                bShell=False
                #bDetached=False
                bDetached=True
                if fileType is None:
                    self.__logError__('fn:%s;mime type not found'%(filename))
                    cmd=filename
                    bShell=True
                    bDetached=True
                else:
                    mime = fileType.GetMimeType()
                    cmd = fileType.GetOpenCommand(filename, mime or '')
                    if cmd is None:
                        cmd=self.ChooseCmd4Mime(fileType,filename,mime)
                        if cmd==0:
                            return
                    if cmd is None:
                        sMsg=_(u"Command not found for\n\n%s!")%(filename)
                        self.ShowMsgDlg(self.OK,sMsg,None)
                        return
                    if cmd[-1]=='*':
                        cmd=filename
                        bShell=True
                        bDetached=True
                try:
                    self.__logDebug__('cmd:%s'%(cmd))
                except:
                    pass
                if cmd.startswith('WX_DDE#'):
                    wx.Execute(cmd)
                    self.__logPrintMsg__(_(u'execute %s by DDE done.')%sFN)
                    return
                iFlgPrc=0
                if bShell:
                    iFlgPrc|=vpc.vcpProcess_SHELL
                if bDetached:
                    iFlgPrc|=vpc.vcpProcess_DETACH
                p=vpc.vcpProcess()
                p.Inz(self.GetOrigin(),'prc')
                p.Start(cmd,iFlgPrc)
                #frmMain=frmMain=self.GetParent()
                #self.__logPrintMsg__(_(u'execute %s ...')%sFN)
                #p=vtProcess(cmd,shell=bShell,detached=bDetached)
                if bDetached==True:
                    pass
                else:
                    #print 'done'
                    #frmMain.Do(self.doExec,p,_(u'execute %s done.')%sFN)
                    pass
            except:
                self.__logTB__()
                sMsg=_(u"Cann't open %s!")%(filename)
                self.ShowMsgDlg(self.OK,sMsg,None)
        except:
            self.__logTB__()
    def OpenBrowserSys(self,sDN,sFN):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__({'sDN':sDN,'sFN':sFN})
            sCmd=None
            sBrowser=None
            bShell=False
            bDetached=False
            # 20140929 wro:FIXME find sBrowser from configuration
            if vSystem.isWin()==True:
                #sTmpDN=vpc.vcOsGetSys(sDN)
                #sTmpDN=sDN[:]
                sTmpDN=sDN.replace('/','\\')
                sWinDN=os.getenv('windir')
                if sWinDN is None:
                    sBrowser='"explorer.exe"'
                else:
                    sBrowser='"%s\\explorer.exe"'%(sWinDN)
                    #sBrowser=sBrowser.replace('\\','\\\\')
                #sBrowser='"%WinDir%\\\\explorer.exe"'
                sCmd='%s "%s"'%(sBrowser,sTmpDN)
                #sCmd=sCmd.replace('\\','\\\\')
                bShell=False
                bDetached=True
            elif vSystem.isUnx()==True:
                sBrowser=None
                self.__logError__('system file browser not defined')
            else:
                self.__logError__('system file browser not defined')
                return -1
            if sBrowser is None:
                self.__logError__('system file browser not defined')
                self.__logPrintMsg__(_(u'open browser not available.'))
                return -2
            if bDbg:
                self.__logDebug__({'sBrowser':sBrowser,'sCmd':sCmd,
                        'bShell':bShell,'bDetached':bDetached})
            iFlgPrc=0
            if bShell:
                iFlgPrc|=vpc.vcpProcess_SHELL
            if bDetached:
                iFlgPrc|=vpc.vcpProcess_DETACH
            p=vpc.vcpProcess()
            p.Inz(self.GetOrigin(),'prc')
            #self.PrintMsg(_(u'open browser %s ...')%sDN)
            p.Start(sCmd,iFlgPrc)
            
            return 1
        except:
            self.__logTB__()
        return 0
    def __getThread__(self,sName):
        try:
            bDbg=self.GetVerboseDbg(10)
            if bDbg:
                self.__logDebug__(''%())
            sThdName='_oThd'+sName
            if hasattr(self,sThdName)==False:
                oThd=vpc.vcpThdBase()
                oThd.Inz(self._vcOrg.GetOrigin(),sName)
                oThd.Start()
                self.__dict__[sThdName]=oThd
                return oThd
            else:
                return self.__dict__[sThdName]
        except:
            self.__logTB__()
    def GetSzFlag(self,sF):
        if sF=='l':
            flag=wx.LEFT
        elif sF=='r':
            flag=wx.RIGHT
        elif sF=='t':
            flag=wx.TOP
        elif sF=='b':
            flag=wx.BOTTOM
        elif sF=='a':
            flag=wx.ALL
        elif sF=='x':
            flag=wx.EXPAND
        elif sF=='s':
            flag=wx.SHAPED
        elif sF=='C':
            flag=wx.ALIGN_CENTER
        elif sF=='L':
            flag=wx.ALIGN_LEFT
        elif sF=='R':
            flag=wx.ALIGN_RIGHT
        elif sF=='T':
            flag=wx.ALIGN_TOP
        elif sF=='B':
            flag=wx.ALIGN_BOTTOM
        elif sF=='H':
            flag=wx.ALIGN_CENTER_HORIZONTAL
        elif sF=='V':
            flag=wx.ALIGN_CENTER_VERTICAL
        elif sF=='F':
            flag=wx.FIXED_MINSIZE
        elif sF=='0':
            flag=wx.LEFT|wx.RIGHT
        elif sF=='=':
            flag=wx.TOP|wx.BOTTOM
        elif sF=='.':
            flag=wx.LEFT|wx.RIGHT|wx.TOP|wx.BOTTOM
        else:
            flag=0
        return flag
    def __get_flag_sz__(self,flag=0,sFlag=None):
        try:
            if sFlag is not None:
                iL=len(sFlag)
                if iL>0:
                    flag|=self.GetSzFlag(sFlag[0])
                if iL>1:
                    flag|=self.GetSzFlag(sFlag[1])
        except:
            self.__logTB__()
        return flag
    def AddSzSpacer(self,prop,size,sz=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if sz is None:
                sz=self.bxs
            if prop<=0:
                sz.AddSpacer(size)
            else:
                #sz.AddStretchSpacer(prop)
                #sz.Add(width=size,height=size,proportion=prop)
                pass
        except:
            self.__logTB__()
    def __vltSzBorder__(self,border,flag):
        """ validate sizer border"""
        if border>0:
            if (flag&0xff)==0:
                flag|=self.GetSzFlag('r')
        return flag
    def AddSz(self,w,prop,border=0,flag=0,sFlag=None,sz=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if sz is None:
                sz=self.bxs
            flag=self.__get_flag_sz__(flag=flag,sFlag=sFlag)
            flag=self.__vltSzBorder__(border,flag)
            sz.AddWindow(w,prop,border=border,flag=flag)
        except:
            self.__logTB__()
    def InsSz(self,iOfs,w,prop,border=0,flag=0,sFlag=None,sz=None):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            if sz is None:
                sz=self.bxs
            flag=self.__get_flag_sz__(flag=flag,sFlag=sFlag)
            flag=self.__vltSzBorder__(border,flag)
            sz.Insert(iOfs,w,prop,border=border,flag=flag)
        except:
            self.__logTB__()
    def GetBmp(self,sN):
        t=type(sN)
        if t==types.StringType:
            return vGuiArt.GetBmp(sN)
        elif t==types.UnicodeType:
            return vGuiArt.GetBmp(sN)
        elif t==types.TupleType:
            return vGuiArt.getBmp(sN[0],sN[1])
        return sN
    def getBmp(self,sClt,sBmp):
        return vGuiArt.getBmp(sClt,sBmp)
    def getImg(self,sClt,sImg):
        return vGuiArt.getBmp(sClt,sImg)
    def getIcn(self,sClt,sIcn):
        return vGuiArt.getBmp(sClt,sIcn)
    def GetDropSrc(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            return
            wid=self.GetWid()
            dat=wx.DataObject()
            oSrc=wx.DropSource(dat,win=wid)
            oSrc.DoDragDrop(wx.Drag_AllowMove)
        except:
            self.__logTB__()

def vtGuiCoreLimitWindowToScreen(iX,iY,iWidth,iHeight):
    #try:
        dScreens=getScreenDict()
        if dScreens is None:
            iMaxW=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_X)
            iMaxH=wx.SystemSettings.GetMetric(wx.SYS_SCREEN_Y)
            iMaxW-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iMaxH-=wx.SystemSettings.GetMetric(wx.SYS_MENU_Y)
            iXa,iYa=0,0
            iXe,iYe=iMaxW,iMaxH
        else:
            dScr=dScreens[None]
            iXa=dScr.get('iXa',0)
            iYa=dScr.get('iYa',0)
            iXe=dScr.get('iXe',0)
            iYe=dScr.get('iYe',0)
            iMaxW=iXe-iXa
            iMaxH=iYe-iYa
        if iX>iMaxW:
            iX=iMaxW-iWidth
        if iY>iMaxH:
            iY=iMaxH-iHeight
        if iX<iXa:
            iX=0
        if iY<iYa:
            iY=0
        if iX+iWidth>iXe:
            iX=iXe-iWidth
        if iY+iHeight>iYe:
            iY=iYe-iHeight
        if iX<iXa:
            iX=iXa
        if iY<iYa:
            iY=iYa
        if iX+iWidth>iXe:
            iWidth=iMaxW-iX
        if iY+iHeight>iMaxH:
            iHeight=iMaxH-iY
    #except:
    #    iX=iY=0
    #    iWidth=100
    #    iHeight=100
        return iX,iY,iWidth,iHeight

def vtGuiCoreArrangeWidget(widBase,widSub,how):
    """
    how = 0     ... bottom left of widBase size of widSub
    how = 1     ... bottom right of widBase size of widSub
    how = 2     ... top left of widBase size of widSub
    how = 3     ... top right of widBase size of widSub
    how = 4     ... top right of widBase size of widSub
    how =10     ... top right of widBase
    how =20     ... top left of widBase
    
    how =0x40   ... bottom left of widBase height of widSub width of widBase
    """
    iBaseX,iBaseY = widBase.ClientToScreen( (0,0) )
    #iBaseW,iBaseH =  widBase.GetSize()
    iBaseW,iBaseH =  widBase.GetClientSize()
    iSubX,iSubY=widSub.GetPosition()
    iSubW,iSubH=widSub.GetSize()
    vpc.logDebug(   'iBaseX:%6d,iBaseY:%6d,iBaseW:%6d,iBaseH:%6d;'\
            ' iSubX:%6d, iSubY:%6d, iSubW:%6d, iSubH:%6d;'\
            'how:%3d 0x%04x'%(iBaseX,iBaseY,iBaseW,iBaseH,iSubX,iSubY,iSubW,iSubH,how,how),
            __name__)
    if how==0:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    elif how==1:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX-iSubW+iBaseW,iBaseY+iBaseH,iSubW,iSubH)
    elif how==2:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY-iSubH,iSubW,iSubH)
    elif how==3:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX-iSubW+iBaseW,iBaseY-iSubH,iSubW,iSubH)
    elif how==4:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX+iBaseW,iBaseY,iSubW,iSubH)
    elif how==10:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX+iBaseW,iBaseY,iSubW,iBaseH)
    elif how==20:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY-iSubH,iSubW,iSubH)
    elif how==0x40:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY+iBaseH,iBaseW,iSubH)
        vpc.logDebug('    iX:%6d,    iY:%6d,    iW:%6d,    iH:%6d'%(iX,iY,iW,iH),__name__)
    else:
        iX,iY,iW,iH=vtGuiCoreLimitWindowToScreen(iBaseX,iBaseY+iBaseH,iSubW,iSubH)
    iW=max(32,iW)
    iH=max(32,iH)
    widSub.Move((iX,iY))
    widSub.SetSize((iW,iH))

def vtGuiCoreGetBaseWidget(par):
    wid=par
    while wid is not None:
        if wid.GetClassName() in ['wxFrame','wxMDIChildFrame',
                    'wxMDIParentFrame','wxDialog']:
            return wid
        wid=wid.GetParent()
    return par

def vtGuiCoreGetColorRGB(iColor):
    iNum=wx.SYS_COLOUR_WINDOW
    if iColor==0:
        iNum=wx.SYS_COLOUR_WINDOW
    oCol=wx.SystemSettings.GetColour(iNum)
    return [oCol.Red(),oCol.Green(),oCol.Blue()]
