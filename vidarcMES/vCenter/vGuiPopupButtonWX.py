#----------------------------------------------------------------------------
# Name:         vGuiPopupButtonWX.py
# Purpose:      popup button
# Author:       Walter Obweger
#
# Created:      20120317
# CVS-ID:       $Id: vGuiPopupButtonWX.py,v 1.9 2015/07/25 15:58:09 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import wx
    from vGuiCoreWX import vGuiCoreWX
    from vGuiCoreWX import vtGuiCoreArrangeWidget
    VERBOSE=vpc.getVerboseType(__name__)
    VERBOSE=10
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

class vGuiPopupButtonWX(vGuiCoreWX):
    #def __init__(self,id=-1,parent=None,pos=(0,0),size=(32,32),name='tgPopup',
    #            style=0,bmp=None,wid=None,
    #            bAnchor=True,iArrange=-1):
    def __initCls__(self,*args,**kwargs):
        self.SetVerbose(self.__class__.__name__,kwargs=kwargs)
    def __initObj__(self,*args,**kwargs):
        try:
            bDbg=self.GetVerboseDbg(20)
            if bDbg:
                self.__logDebug__({'args':args,'kwargs':kwargs})
            
            self.wid=None
            self.bAnchor=kwargs.get('bAnchor',True)
            self.iArrange=kwargs.get('iArrange',-1)
            self.widPop=None
            self.bBusy=False
        except:
            self.__logTB__()
    def __initWid__(self,*args,**kwargs):
        try:
            parent=kwargs.get('parent',None)
            if parent is None:
                self.__logCritical__('you are not supposed to be here.')
                return
            if hasattr(parent,'GetWid'):
                par=parent.GetWid()
            else:
                par=parent
            bmp=kwargs.get('bmp',None)
            wid=kwargs.get('wid',None)
            _args,_kwargs=self.GetGuiArgs(kwargs,[
                    'id','name','parent','pos','size',
                    'style',
                    ],{
                    'pos':(0,0),'size':(-1,-1),
                    'style':0#wx.TAB_TRAVERSAL
                    })
            _kwargs['bitmap']=bmp or self.getBmp('core','Down')
            self.wid=wx.lib.buttons.GenBitmapToggleButton(*_args,**_kwargs)
            self.wid.Bind(wx.EVT_BUTTON,self.OnPopupButton,self.wid)
            if self.bAnchor==True:
                bFound=False
                par=self.GetAnchor(parent,iArrange=self.iArrange,
                        bAnchorFrm=False)
                if par is not None:
                    par.Bind(wx.EVT_MOVE,self.OnMoveWidArrange)
                    par.Bind(wx.EVT_SIZE,self.OnMoveWidArrange)
                #while par is not None:
                #    if par.GetClassName() in ['wxFrame','wxMDIChildFrame','wxDialog']:
                #        bFound=True
                #        break
                #    par=par.GetParent()
                #if bFound==True:
                #    par.Bind(wx.EVT_MOVE,self.OnMoveButton)
                else:
                    self.__logError__('anchor not possible, no valid parent widget found'%())
            else:
                par.Bind(wx.EVT_MOVE,self.OnMoveWidArrange)
                par.Bind(wx.EVT_SIZE,self.OnMoveWidArrange)
            self.wid.Enable(False)
            ##wx.EVT_LEFT_UP(self, self.OnLeftDown)
            ##wx.EVT_RIGHT_DOWN(self, self.OnRightDown)
            #self.bAnchor=bAnchor
            #self.iArrange=iArrange
            #self.widPop=None
            #self.bBusy=False
            if wid is not None:
                self.SetPopupWid(wid)
        except:
            self.__logTB__()
        return self.wid
    def SetWidPopup(self,wid):
        """
        """
        try:
            if self.IsMainThread()==False:
                return
            self.widPop=wid
            w=self.GetWid()
            w.Enable(True)
            self.bBusy=True
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    self.CB(vtGuiCoreArrangeWidget,w,widPop,self.iArrange)
                if hasattr(self.widPop,'BindEvent'):
                    self.widPop.BindEvent('ok',self.OnPopupOk)
                    self.widPop.BindEvent('cancel',self.OnPopupCancel)
        except:
            self.__logTB__()
    def GetWidPopup(self):
        return self.widPop
    def OnPopupButton(self,evt):
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            w=self.GetWid()
            if self.widPop is not None:
                if w.GetValue()==True:
                    if self.iArrange>=0:
                        if hasattr(self.widPop,'GetWid'):
                            widPop=self.widPop.GetWid()
                        else:
                            widPop=self.widPop
                        vtGuiCoreArrangeWidget(w,widPop,self.iArrange)
                    self.widPop.Show(True)
                else:
                    self.widPop.Show(False)
            else:
                self.__logError__('no popup widget set yet'%())
                #vtGuiCoreArrangeWidget(self,self,iArrange)
        except:
            self.__logTB__()
    def OnMoveWidArrange(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            w=self.GetWid()
            if self.widPop is not None:
                if self.iArrange>=0:
                    if hasattr(self.widPop,'GetWid'):
                        widPop=self.widPop.GetWid()
                    else:
                        widPop=self.widPop
                    self.CB(vtGuiCoreArrangeWidget,w,widPop,self.iArrange)
        except:
            self.__logTB__()
    def __Close__(self):
        try:
            self.__logDebug__(''%())
            if VERBOSE>0:
                self.__logDebug__(''%())
            if self.IsMainThread()==False:
                return
            if self.widPop is not None:
                if hasattr(self.widPop,'GetWid'):
                    widPop=self.widPop.GetWid()
                else:
                    widPop=self.widPop
                widPop.Show(False)
                if hasattr(widPop,'CallWidChild'):
                    widPop.CallWidChild(-1,'__Close__')
            self.wid.SetValue(False)
        except:
            self.__logTB__()
    Close=__Close__
    def GetWid(self):
        return self.wid
    def Popup(self,bFlag=True):
        self.SetValue(bFlag)
        self.OnPopupButton(None)
    def OnPopupOk(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
    def OnPopupCancel(self,evt):
        evt.Skip()
        try:
            if VERBOSE>0:
                self.__logDebug__(''%())
            self.__Close__()
        except:
            self.__logTB__()
