#----------------------------------------------------------------------------
# Name:         vCenterMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20120212
# CVS-ID:       $Id: vCenterMainFrame.py,v 1.12 2016/02/07 06:36:05 wal Exp $
# Copyright:    (c) 2012 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vp.vCore as vpc
vpc.logDebug('import;start',__name__)
try:
    import vSystem
    import vCust
    from vCfgXml import vCfgXml
    from vGuiFrmWX import vGuiFrmWX
    
    import imgCtr as imgCtr
    from vCenterMainPanel import vCenterMainPanel
    VERBOSE=vpc.getVerboseType(__name__)
except:
    vpc.logTB(__name__)
try:
    import vidarc.tool.log.vtLog as vtLog
except:
    vpc.logTB(__name__)
vpc.logDebug('import;done',__name__)

def getPluginImage():
    return imgCtr.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(imgCtr.getApplicationBitmap())
    return icon

def create(parent, pos=None, size=(800,600),style=0, name='vCenterMainFrm'):
    return vCenterMainFrame(parent=parent,pos=pos,size=size,style=style,name=name)


class vCenterMainFrame(vCfgXml,vGuiFrmWX):
    STATUS_CLK_POS=4
    STATUS_LOG_POS=3
    def __init__(self,title=u'vCenter',
                name='vCenterMainFrm',
                kind='frmMain',
                iArrange=-1,widArrange=None,bAnchor=False,
                pnName='pn',
                pnPos=None,pnSz=None,pnStyle=0,
                bmp=None,bToolBar=True,
                lMnCfg=0,lSbCfg=None,
                lWid=[],**kwargs):
        lMnCfg=[
            (0,[[u'file',_(u'&File'),None,None,(),{},[
                    None,
                    (u'exit',_(u'Exit\tAlt+X'),self.getBmp('core','Exit'),
                        self.DoShutDown,(),{}),
                    ]],
                ]),
            ]
        lMnCfg=0
        lSbCfg=[
            ['lbl','sLbl',50,],
            ['prc',None,100,],
            ['msg',None,-1,],
            ['log','',80,],
            ['clk','sClk',140,],
            ]
        #lWid=[
        #    (vCenterMainPanel,      (0,0),(1,1),{'name':'wid01','bFlexGridSizer':0},None),
        #    ]
        vCfgXml.__init__(self,['vCenterGui'],
                {'x':10,'y':10,'width':700,'height':500})
        vGuiFrmWX.__init__(self,title=title,name=name,kind=kind,
                iArrange=iArrange,widArrange=widArrange,bAnchor=bAnchor,
                bmp=bmp or imgCtr.getApplicationBitmap(),
                pnName='pn',pnSz=(400,400),
                #lGrowRow=[(0,1)],lGrowCol=[(0,1)],
                #bFlexGridSizer=0,
                lWid=vCenterMainPanel,
                lMnCfg=lMnCfg,
                lSbCfg=lSbCfg ,
                bToolBar=bToolBar,**kwargs)
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            self.AddToolBar(0,[
                (10,'bt',self.getBmp('core','Build'),'test',
                    self.OnToolBuild,(),{}),
                ('tgPop','tg',self.getBmp('core','Down'),'test',
                    self.OnToolBuild,(),{}),
                ])
            
        except:
            self.__logTB__()
        try:
            doc=self.pn.GetDocMain()
            if doc is not None:
                doc.BindEvent('gotContent',self.OnGotContent)
        except:
            self.__logTB__()
        try:
            sCfgFN=vCust.getCmdLineOpt('-c','--config','vCenterCfg.xml')
            self.OpenCfgFile(sCfgFN)
            self.DoCmdArgs()
        except:
            self.__logTB__()
    def OnToolBuild(self):
        try:
            bDbg=self.GetVerboseDbg(0)
            if bDbg:
                self.__logDebug__('')
            
        except:
            self.__logTB__()
    def OnGotContent(self,evt):
        evt.Skip()
        try:
            self.__logDebug__(''%())
            self.CallBackDelayed(2,self.PrcCmdArgs)
        except:
            self.__logTB__()
    def DoCmdArgs(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def PrcCmdArgs(self):
        try:
            self.__logDebug__(''%())
        except:
            self.__logTB__()
    def __getTitleInfo__(self):
        try:
            doc=self.pn.GetDocMain()
            if doc is not None:
                sFN=doc.GetFN()
                sMod=')'
                if sFN is None:
                    sFN='undef'
                else:
                    if doc.IsModified():
                        sMod='*)'
                return ''.join(['(',sFN,sMod])
        except:
            self.__logTB__()
    def DoMenuAction(self,sAct):
        try:
            self.__logDebug__('sAct:%s'%(sAct))
            if sAct=='open':
                self.pn.MenuOpenFile()
            elif sAct=='save':
                self.pn.MenuSaveFile(1)
            elif sAct=='saveAs':
                self.pn.MenuSaveFile(-1)
            elif sAct=='saveCfg':
                self.pn.MenuSaveFile(0)
        except:
            self.__logTB__()
    def OpenFile(self,sFN):
        try:
            self.__logDebug__('sFN:%s'%(sFN))
            self.pn.OpenFile(sFN)
        except:
            self.__logTB__()
    def OpenCfgFile(self,fn=None):
        try:
            self.__logDebug__(''%())
            vCfgXml.OpenCfgFile(self,fn)
            self.pn.SetCfgData()
            self.SetCfgData()
        except:
            self.__logTB__()
    def SaveCfgFile(self,fn=None):
        try:
            self.__logDebug__(''%())
            self.pn.GetCfgData()
            self.GetCfgData()
            vCfgXml.setCfgData(self)
            vCfgXml.SaveCfgFile(self,fn)
        except:
            vtLog.vtLngTB(self.GetName())
    def SetCfgData(self):
        try:
            self.__logDebug__(''%())
            iX=vCfgXml.getCfgVal(self,['x'],funcConv=int)
            iY=vCfgXml.getCfgVal(self,['y'],funcConv=int)
            iWidth=vCfgXml.getCfgVal(self,['width'],funcConv=int)
            iHeight=vCfgXml.getCfgVal(self,['height'],funcConv=int)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.SetPosFrm((iX,iY))
            self.SetSizeFrm((iWidth,iHeight))
        except:
            self.__logTB__()
    def GetCfgData(self):
        try:
            self.__logDebug__(''%())
            pos=self.GetPosFrm()
            iX=pos[0]
            iY=pos[1]
            vCfgXml.setCfgVal(self,iX,['x'])
            vCfgXml.setCfgVal(self,iY,['y'])
            sz=self.GetSizeFrm()
            iW=sz[0]
            iH=sz[1]
            vCfgXml.setCfgVal(self,iW,['width'])
            vCfgXml.setCfgVal(self,iH,['height'])
            if self.IsMaximized():
                vCfgXml.setCfgVal(self,'1',['maximized'])
            else:
                vCfgXml.setCfgVal(self,'0',['maximized'])
        except:
            self.__logTB__()
    def ShutDown(self):
        try:
            self.__logDebug__('')
            self.GetCfgData()
            self.SaveCfgFile()
            iRet=self.pn.ShutDown()
            #if iRet!=False:
            #    return True
        except:
            self.__logTB__()
        try:
            vGuiFrmWX.ShutDown(self)
        except:
            self.__logTB__()
        return 0
