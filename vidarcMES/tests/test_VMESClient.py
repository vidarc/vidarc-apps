#----------------------------------------------------------------------------
# Name:         test_vMESClient.py
# Purpose:      unit test of pyGatherMD.py
#
# Author:       Walter Obweger
#
# Created:      20200823
# CVS-ID:       $Id$
# Copyright:    (c) 2019 by Walter Obweger
# Licence:      MIT
#----------------------------------------------------------------------------

import unittest   # The test framework

import lindworm.logUtil as logUtil
import vidarcMES.vMESCenter.vMESCenterAppl as UoT

iLogInit=0

def initLog(sLogFN):
    global iLogInit
    print('sLogFN:%s'%(sLogFN))
    if iLogInit==0:
        logUtil.logInit(sLogFN)
        iLogInit=1

class TestBase(unittest.TestCase):
    def setUp(self):
        # +++++ beg:init logging
        initLog("./x_log/test_cfg.log")
        # ----- end:init logging
        # +++++ beg:initialize
        self.sInFN              ='./x_test/README.md'
        self.sBldDN             ='./x_test/build'
        self.sOtFN              =None
        self.sCfgFN             ='./x_test/pyGatherMDCfg.json'
        self.iVerbose           =10
        # ----- end:initialize
        # +++++ beg:construct object
        # ----- end:construct object
    def tearDown(self):
        pass
    def test_buildDN(self):
        # +++++ beg:exec gather 
        iRet=UoT.main()
        # ----- end:exec gather
        self.assertEqual(iRet,1)

if __name__ == '__main__':
    unittest.main()
