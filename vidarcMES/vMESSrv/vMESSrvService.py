#----------------------------------------------------------------------------
# Name:         vMESSrvService.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vMESSrvService.py,v 1.2 2007/07/30 20:39:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import win32serviceutil
import win32service
import win32event
import win32pipe
import win32file
import pywintypes
import winerror
import perfmon
import os,os.path,sys
import string,thread,threading,time,Queue
import traceback
import wx
app=wx.App()

#import __init__
#if __init__.VIDARC_IMPORT:
import vidImp
import identify as vIdentify

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog
from vXmlMESSrv import vXmlMESSrv
from vidarc.tool.net.vtNetXml import *
from vidarc.vApps.vHum.vXmlHum import vXmlHum
#import vMESSrvpluginFind

class vMESSrvService(win32serviceutil.ServiceFramework):
    _svc_name_ = "vMESSrvService"
    _svc_display_name_ = "VIDARC Automation GmbH MES Server"
    #_svc_display_description_ = "hot-plugable XML,PlugIn, messages and so on servers"
    _svc_description_ = "hot-plugable XML,PlugIn, messages and so on servers"
    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        # Create an event which we will use to wait on.
        # The "service stop" request will set this event.
        self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
        self.InitPerfMon()
        
        optshort,optlong='l:h',['lang=','log=','help']
        vtLgBase.initAppl(optshort,optlong,'vXmlSrv')
        cfgFN='vMESSrvCfg.xml'
        self.cfgDN=vIdentify.getCfg('cfgDN')
        self.logDN=vIdentify.getCfg('logDN')
        try:
            self.dataPort=int(vIdentify.getCfg('dataPort'))
        except:
            self.dataPort=50098
        try:
            self.servicePort=int(vIdentify.getCfg('servicePort'))
        except:
            self.servicePort=50099
        #sDNlog=os.path.join(sDN,'log')
        try:
            os.makedirs(self.logDN)
        except:
            pass
        os.chdir(self.logDN)
        iLogLv=vtLog.INFO
        vtLog.vtLngInit('vMESSrv','vMESSrvSvc.log',iLogLv,iSockPort=60098,bTime=True)
    def InitPerfMon(self):
        # Magic numbers (2 and 4) must match header and ini file used
        # at install - could lookup ini, but then Id need it a runtime
        
        # A counter for number of connections per second.
        self.counterConnections=perfmon.CounterDefinition(2) 
        # We arent expecting many, so we set the scale high (ie, x10)
        # Note the scale is 10^DefaultScale = ie, to get 10, we use 1!
        self.counterConnections.DefaultScale = 1

        # Now a counter for the number of bytes received per second.
        self.counterBytes=perfmon.CounterDefinition(4)
        # A scale of 1 is fine for this counter.
        self.counterBytes.DefaultScale = 0

        # Now register our counters with the performance monitor
        perfObjectType = perfmon.ObjectType( 
                          (self.counterConnections, self.counterBytes) )
        
        self.perfManager = perfmon.PerfMonManager(
                        self._svc_name_,
                        (perfObjectType,),
                        "perfmondata")
    def TermPerfMon(self):
        self.perfManager.Close()
        self.perfManager = None
    def SvcStop(self):
        # Before we do anything, tell the SCM we are starting the stop process.
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        # And set my event.
        win32event.SetEvent(self.hWaitStop)
    def __plugInFin__(self):
        self.docMES.BuildPlugInDict()
    def __procStartServer__(self,node,doc):
        try:
            oReg=doc.GetRegByNode(node)
            if oReg is not None:
                vtLog.vtLngCurCls(vtLog.INFO,'tagName:%s'%(oReg.GetTagName()),self)
                dPlugIn=doc.GetPlugInDict()
                if oReg.GetTagName() in dPlugIn:
                    oPlugIn=dPlugIn[oReg.GetTagName()]
                    if oPlugIn.IsServer():
                        vtLog.vtLngCurCls(vtLog.INFO,'tagName:%s is server'%(oReg.GetTagName()),self)
                        oReg.StartServer(nodeSrv)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def SvcDoRun(self):
        import servicemanager
        servicemanager.LogMsg(
                servicemanager.EVENTLOG_INFORMATION_TYPE, 
                servicemanager.PYS_SERVICE_STARTING,
                (self._svc_name_, '\nlogDN:%s\ncfgDN:%s\ndataPort:%d\nservicePort:%d'%(self.logDN,self.cfgDN,self.dataPort,self.servicePort)))
        try:
            self.docMES=vXmlMESSrv(appl='vMESSrv')
            self.docMES.FindPlugIn(None,self.__plugInFin__)
            while self.docMES.IsBusyPlugIn():
                time.sleep(0.2)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        servicemanager.LogMsg(
                servicemanager.EVENTLOG_INFORMATION_TYPE, 
                servicemanager.PYS_SERVICE_STARTED,
                (self._svc_name_, '\nplugin\n%s'%(vtLog.pformat(self.docMES.GetPlugInDict()))))
        self.srv=vtNetSrv(vtSrvXmlSock,self.dataPort,vtNetXmlSock,
                        vtSrvSock,self.servicePort,vtServiceSock,2,1)
        #self.srv.SetServicePasswd(self.pnCfg.GetServicePasswd())
        srvXml=self.srv.GetDataInstance()
        self.srv.Serve()
        #srvXml.SetSaveDelay(10)
        if 1:
            try:
                fn=vIdentify.getCfg('cfgFN')
                vtLog.vtLngCurCls(vtLog.INFO,'fn:%s'%(fn),self)
                self.docMES.Open(os.path.join(self.cfgDN,fn))
                nodeBase=self.docMES.getBaseNode()
                for nodeSrv in self.docMES.getChilds(nodeBase,'vMESSrv'):
                    vtLog.vtLngCurCls(vtLog.INFO,'nodeSrv:%s'%(nodeSrv),self)
                    oMESSrv=self.docMES.GetRegByNode(nodeSrv)
                    #oMESSrv.StartServer(nodeSrv)
                    oMESSrv.Start(nodeSrv,srvXml)
                    self.docMES.procChildsKeys(nodeSrv,self.__procStartServer__,self.docMES)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        else:
            lHosts=vIdentify.getCfg('hosts')
            lAliases=vIdentify.getCfg('aliases')
            for s in lAliases:
                servicemanager.LogMsg(
                            servicemanager.EVENTLOG_INFORMATION_TYPE, 
                            servicemanager.PYS_SERVICE_STARTING,
                            (self._svc_name_, ' starting alias %s'%(s)))
                try:
                    sAppl,fn=s.split(':')
                    doc=vXmlMESSrv(appl=sAppl)
                    alias=':'.join(['vMESSrv',sAppl])
                    srvXml.AddServedXml(alias,os.path.join(self.cfgDN,fn),doc,[])
                    srvXml.SetServedHosts(alias,lHosts)
                    vtLog.vtLngCurCls(vtLog.INFO,'alias:%s;hosts:%s'%(alias,vtLog.pformat(lHosts)),self)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
            lAliases=vIdentify.getCfg('humans')
            for s in lAliases:
                servicemanager.LogMsg(
                            servicemanager.EVENTLOG_INFORMATION_TYPE, 
                            servicemanager.PYS_SERVICE_STARTING,
                            (self._svc_name_, ' starting alias %s'%(s)))
                try:
                    sAppl,fn=s.split(':')
                    doc=vXmlHum(appl=sAppl)
                    alias=':'.join(['vHum',sAppl])
                    srvXml.AddServedXml(alias,os.path.join(self.cfgDN,fn),doc,[])
                    srvXml.SetServedHosts(alias,lHosts)
                    vtLog.vtLngCurCls(vtLog.INFO,'alias:%s;hosts:%s'%(alias,vtLog.pformat(lHosts)),self)
                except:
                    vtLog.vtLngTB(self.__class__.__name__)
        while 1:            
            pass
            timeout = win32event.INFINITE
            waitHandles = [self.hWaitStop]
            rc = win32event.WaitForMultipleObjects(waitHandles, 0, timeout)
            if rc==win32event.WAIT_OBJECT_0:
                # Stop event
                servicemanager.LogMsg(
                        servicemanager.EVENTLOG_INFORMATION_TYPE, 
                        servicemanager.PYS_SERVICE_STOPPING,
                        (self._svc_name_, ' socket server stopping...'))
                self.srv.Stop()
                while self.srv.IsServing():
                    time.sleep(0.3)
                srvXml=self.srv.GetDataInstance()
                l=srvXml.GetServedXmlLst('vMESSrv')
                for alias in l:
                    srvXml.SaveXml(':'.join(['vMESSrv',alias]))
                #self.docMES.Save()
                servicemanager.LogMsg(
                        servicemanager.EVENTLOG_INFORMATION_TYPE, 
                        servicemanager.PYS_SERVICE_STOPPING,
                        (self._svc_name_, ' socket server stopped'))
                
                break
        # cleanup our PerfMon counters.
        self.TermPerfMon()

        # Now log a "service stopped" message
        servicemanager.LogMsg(
                servicemanager.EVENTLOG_INFORMATION_TYPE, 
                servicemanager.PYS_SERVICE_STOPPED,
                (self._svc_name_, '\nlog DN:%s\ncfg DN:%s\ndataPort:%d\nservicePort:%d'%(self.logDN,self.cfgDN,self.dataPort,self.servicePort)))

if __name__=='__main__':
    win32serviceutil.HandleCommandLine(vMESSrvService)
