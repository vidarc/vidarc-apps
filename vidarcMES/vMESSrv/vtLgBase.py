#----------------------------------------------------------------------------
# Name:         vtLgBase.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20051211
# CVS-ID:       $Id: vtLgBase.py,v 1.1 2006/07/17 11:38:21 wal Exp $
# Copyright:    (c) 2005 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
import os,sys,getopt
import gettext,traceback
import wx
import locale

#import __init__
        
MAP_LOCALE_ON_MSWIN={
    'de_DE':'German_Germany',
    'de_CH':'German_Switzerland',
    'de_AT':'German_Austria',
    }
    
APPL_BASE_DN=''
def getApplBaseDN():
    global APPL_BASE_DN
    return APPL_BASE_DN
APPL_LANG='en'
def getApplLang():
    global APPL_LANG
    return APPL_LANG
APPL_DOMAIN={}
def getApplDomain():
    global APPL_DOMAIN
    return APPL_DOMAIN
def initAppl(optshort,optlang,domain):
    langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlang)
        for o in optlist:
            if o[0] in ['--lang','-l']:
                lang=o[1]
                if lang=='en':
                    langid = wx.LANGUAGE_ENGLISH    # use OS default; or use LANGUAGE_JAPANESE, etc.
                elif lang=='de':
                    langid = wx.LANGUAGE_GERMAN
    except:
        traceback.print_exc()
    basepath = os.path.abspath(os.path.dirname(sys.argv[0]))
    localedir = os.path.join(basepath, "locale")
    if langid==wx.LANGUAGE_ENGLISH:
        lang='en'
    elif langid==wx.LANGUAGE_GERMAN:
        lang='de'
    else:
        lang='en'
    global APPL_LANG
    APPL_LANG=lang
    global APPL_BASE_DN
    APPL_BASE_DN=basepath
    global APPL_DOMAIN
    APPL_DOMAIN=domain
    # Set locale for wxWidgets
    mylocale = wx.Locale(langid)
    mylocale.AddCatalogLookupPathPrefix(localedir)
    mylocale.AddCatalog(domain)

    # Set up Python's gettext
    mytranslation = gettext.translation(domain, localedir,
        [mylocale.GetCanonicalName()], fallback = True)
    mytranslation.install(unicode=1)
    locale.setlocale(locale.LC_ALL,'')
def getPossibleLang(domain,basepath):
    langs=[]
    localedir = os.path.join(basepath, "locale")
    try:
        files=os.listdir(localedir)
        for fn in files:
            try:
                sFullFN=os.path.join(localedir,fn)
                if os.path.isdir(sFullFN):
                    if os.path.isdir(os.path.join(sFullFN,'LC_MESSAGES')):
                        langs.append(fn)
            except:
                pass
    except:
        pass
    return langs
def setPluginLang(domain,dn,lang):
    langid = wx.LANGUAGE_DEFAULT    # use OS default; or use LANGUAGE_JAPANESE, etc.
    if lang=='en':
        langid = wx.LANGUAGE_ENGLISH    # use OS default; or use LANGUAGE_JAPANESE, etc.
    elif lang=='de':
        langid = wx.LANGUAGE_GERMAN
    if langid==wx.LANGUAGE_ENGLISH:
        lang='en'
    elif langid==wx.LANGUAGE_GERMAN:
        lang='de'
    else:
        lang='en'
    localedir = os.path.join(dn, "locale")
    # Set locale for wxWidgets
    #mylocale = wx.Locale(langid)
    #mylocale.AddCatalogLookupPathPrefix(localedir)
    #mylocale.AddCatalog(domain)

    # Set up Python's gettext
    mytranslation = gettext.translation(domain, localedir,
        [lang], fallback = True)
    locale.setlocale(locale.LC_ALL,'')
    try:
        __init__.PLUGIN_LANG=lang
        __init__.PLUGIN_TRANS=mytranslation
    except:
        pass
def assignPluginLang():
    try:
        return __init__.PLUGIN_TRANS.ugettext
    except:
        return _
class vtLangSetLocale(Exception):
    def __init__(self,value):
        self.value=value
    def __str__(self):
        return 'unknow locale:'+self.value
def setLocale(str=None):
    if str is None:
        locale.setlocale(locale.LC_ALL,str)
    if len(str)==0:
        locale.setlocale(locale.LC_ALL,str)
    if wx.Platform=='__WXMSW__':
        
        try:
            locale.setlocale(locale.LC_ALL,MAP_LOCALE_ON_MSWIN[str])
        except:
            raise vtLangSetLocale(str)
    else:
        locale.setlocale(locale.LC_ALL,str)
def format(fmt,val):
    return locale.format(fmt,val)
def getNumSeperatorsFromNumMask():
    ld=locale.localeconv()
    sDec=ld['decimal_point']
    sTho=ld['thousands_sep']
    if len(sTho)==0:
        if sDec=='.':
            sTho=','
        else:
            sTho='.'
    return sDec,sTho
