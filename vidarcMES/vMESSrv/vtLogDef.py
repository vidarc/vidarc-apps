#----------------------------------------------------------------------------
# Name:         vtLogDef.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060115
# CVS-ID:       $Id: vtLogDef.py,v 1.2 2007/07/30 20:39:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------
#import logging
VERBOSE=False

DEBUG=10
WARN=30
WARNING=30     # is same as WARN
INFO=20
ERROR=40
CRITICAL=50
FATAL=50         # is same as CRITICAL
