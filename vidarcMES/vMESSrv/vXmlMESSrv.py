#----------------------------------------------------------------------------
# Name:         vXmlMESSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vXmlMESSrv.py,v 1.12 2014/03/30 12:54:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,os.path

import vidarc.tool.log.vtLog as vtLog
from vidarc.tool.xml.vtXmlDomReg import vtXmlDomReg
from vXmlNodeMESSrvRoot import vXmlNodeMESSrvRoot

import __init__
import identify as vIdentify

from vMESSrvpluginFind import thdMESpluginFind
from vidarc.tool.InOut.dirNotify import dirNotify
from vidarc.tool.InOut.dirNotify import dirNotifyReceiver

import __register__ as __register__
from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainer import vXmlNodeXmlSrvAliasContainer
from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainerSummary import vXmlNodeXmlSrvAliasContainerSummary

from vidarc.vSrv.net.vsNetXmlLoginCache import vsNetXmlLoginCache

class vXmlMESSrv(vtXmlDomReg,dirNotifyReceiver):
    VERBOSE=1
    TAGNAME_REFERENCE='tag'
    TAGNAME_ROOT='MESSrvRoot'
    NOTIFY_DELAY=10
    def __init__(self,appl='vMESSrv',attr='id',skip=[],synch=False,verbose=0,
                    audit_trail=True):
        self.dSrv={}
        dirNotifyReceiver.__init__(self)
        for s in ['config']:
            try:
                idx=skip.index(s)
            except:
                skip.append(s)
        try:
            self.dPlugIn={}
            self.dNotify={}
            vtXmlDomReg.__init__(self,appl=appl,attr=attr,skip=skip,synch=synch,verbose=verbose,
                        audit_trail=audit_trail)
            #oBase=vXmlNodeMESSrvRoot(tagName='vMESSrv')
            #self.RegisterNode(oBase,True)
            oRoot=vXmlNodeMESSrvRoot(tagName='root')
            self.RegisterNode(oRoot,False)
            oRoot=vXmlNodeMESSrvRoot()
            self.RegisterNode(oRoot,False)
            
            __register__.RegisterNodes(self,oRoot,False)
            self.thdPlugin=thdMESpluginFind(None,verbose=0x0) # 0xf
            
            # cache login
            self.docLogin=vsNetXmlLoginCache(appl='loginCache',audit_trail=False)
        except:
            vtLog.vtLngTB(self.appl)
        self.SetDftLanguages()
    def SetPlugInParent(self,parent):
        self.thdPlugin.SetParent(parent)
    def getBaseNode(self):
        if self.root is None:
            return None
        return self.root#self.getChild(self.root,'vMESSrv')
    #def New(self,revision='1.0',root='MESSrvRoot',bConnection=False):
    #    vtXmlDomReg.New(self,revision,root)
    def __New__(self,bConnection=False):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCur(vtLog.INFO,'',origin=self.GetOrigin())
        elem=self.createSubNode(self.getRoot(),'MESSrvs')
        #if bConnection==False:
        #    elem=self.createSubNodeTextAttr(elem,'cfg','','id','')
        #    self.checkId(elem)
        #    self.processMissingId()
        #    self.clearMissing()
        elem=self.createSubNode(self.getRoot(),'config')
        elem=self.createSubNode(self.getRoot(),'settings')
        #elem=self.createSubNode(self.getRoot(),'security_acl')
        self.__checkReqBase__()
        #self.AlignDoc()
    def BuildPlugInDict(self,dPlugIn=None):
        if dPlugIn is None:
            self.dPlugIn=self.thdPlugin.GetPlugInDict()
        else:
            self.dPlugIn=dPlugIn
        vtLog.vtLngCur(vtLog.INFO,'dPlugIn:%s'%(vtLog.pformat(self.dPlugIn)),self.appl)
        oMESSrv=self.GetReg('vMESSrv')
        oRoot=self.GetReg('MESSrvRoot')
        for k,oPlugIn in self.dPlugIn.items():
            if oPlugIn.IsServer():
                strs=oPlugIn.impName.split('.')
                #oPlugIn.plugin=__import__('.'.join(strs[:-1]+['__register__']),[],[],['RegisterNodes'])
                self.objReg=getattr(oPlugIn.plugin,'RegisterNodes')(self,oRoot,True)
                o=self.GetReg(k)
                if o is None:
                    oReg=oPlugIn.GetRegObj()
                    self.RegisterNode(oReg,False)
                    self.LinkRegisteredNode(oMESSrv,oReg)
        for k,oPlugIn in self.dPlugIn.items():
            if oPlugIn.IsServer()==False:
                sSrv=oPlugIn.GetServerName()
                oReg=self.GetReg(sSrv)
                imgData=oPlugIn.GetImage()
                if oPlugIn.IsAliasSummary():
                    oAlias=self.GetReg(''.join([sSrv,'AliasSummary']))
                    o=vXmlNodeXmlSrvAliasContainerSummary(oPlugIn.GetDomain(),oPlugIn.GetObjClass(),imgData,
                                    oPlugIn.GetAliasSummary())
                else:
                    oAlias=self.GetReg(''.join([sSrv,'Alias']))
                    o=vXmlNodeXmlSrvAliasContainer(oPlugIn.GetDomain(),oPlugIn.GetObjClass(),imgData)
                self.RegisterNode(o,False)
                self.LinkRegisteredNode(oReg,o)
                if oAlias is not None:
                    self.LinkRegisteredNode(o,oAlias)
                
    def __findPlugInFin__(self):
        vtLog.vtLngCur(vtLog.INFO,'',self.appl)
        self.dPlugIn=self.thdPlugin.GetPlugInDict()
    def FindPlugIn(self,lDN,func,*args,**kwargs):
        if lDN is None:
            vtLog.vtLngCur(vtLog.DEBUG,'lDN:%r;imp:%r;file:%r'%(lDN,
                    __init__.VIDARC_IMPORT,__file__),self.appl)
            try:
                if __init__.VIDARC_IMPORT:
                    lDN=[vIdentify.getCfg('baseDN')]
                else:
                    sDNa,sFNa=os.path.split(__file__)
                    sDNb,sDNc=os.path.split(sDNa)
                    sDN=os.path.join(sDNb,'vidarc')
                    lDN=[sDN]
            except:
                vtLog.vtLngTB(self.appl)
                # for service
                lDN=[vIdentify.getCfg('baseDN')]
        else:
            vtLog.vtLngCur(vtLog.DEBUG,'lDN:%r;imp:%r;file:%r'%(lDN,
                    __init__.VIDARC_IMPORT,__file__),self.appl)
        vtLog.vtLngCur(vtLog.INFO,'start plugin search:%s;func:%s;args:%s;kwargs:%s;thd:%s'%(vtLog.pformat(lDN),func,args,kwargs,self.thdPlugin),self.appl)
        oRoot=self.GetReg('MESSrvRoot')
        self.dNotify={}
        for sDN in lDN:
            self.dNotify[sDN]=dirNotify(sDN,self.NOTIFY_DELAY)
        if func is None:
            self.thdPlugin.Find(lDN,self,oRoot,self.__findPlugInFin__)
        else:
            self.thdPlugin.Find(lDN,self,oRoot,func,*args,**kwargs)
        vtLog.vtLngCur(vtLog.INFO,'start plugin search:%s'%(vtLog.pformat(lDN)),self.appl)
    def GetPlugInDict(self):
        return self.dPlugIn
    def IsBusyPlugIn(self):
        return self.thdPlugin.IsRunning()
    def GetPluginImages(self,bIncludeSrv=False):
        l=[]
        for k,oPlugIn in self.dPlugIn.items():
            if oPlugIn.IsServer():
                if bIncludeSrv:
                    imgData=oPlugIn.GetImage()
                    sDomain=oPlugIn.GetDomain()
                    l.append((sDomain,imgData,True))
                    
                continue
            imgData=oPlugIn.GetImage()
            sDomain=oPlugIn.GetDomain()
            l.append((sDomain,imgData,False))
        return l
    def AddServerInstanceByNode(self,node,srv):
        id=self.getKey(node)
        self.AddServerInstance(id,srv)
    def AddServerInstance(self,id,srv):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID in self.dSrv:
                vtLog.vtLngCurCls(vtLog.ERROR,'server for id:%08d already added'%(iID),self)
            self.dSrv[iID]=srv
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
        except:
            vtLog.vtLngTB(self.GetAppl())
    def HasServerInstanceByNode(self,node):
        id=self.getKey(node)
        return self.HasServerInstance(id)
    def HasServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            return iID in self.dSrv
        except:
            vtLog.vtLngTB(self.GetAppl())
        return False
    def GetServerInstanceByNode(self,node):
        if node is None:
            return None
        id=self.getKey(node)
        return self.GetServerInstance(id)
    def GetServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID not in self.dSrv:
                return None
                #vtLog.vtLngCur(vtLog.DEBUG,'server for id:%08d not added'%(iID),self.appl)
            return self.dSrv[iID]
        except:
            vtLog.vtLngTB(self.GetAppl())
        return None
    def DelServerInstanceByNode(self,node):
        id=self.getKey(node)
        self.DelServerInstance(id)
    def DelServerInstance(self,id):
        try:
            vtLog.vtLngCur(vtLog.DEBUG,'id:%s;dSrv%s'%(id,vtLog.pformat(self.dSrv)),self.appl)
            iID=long(id)
            if iID in self.dSrv:
                del self.dSrv[iID]
        except:
            vtLog.vtLngTB(self.GetAppl())
    def Build(self):
        """ this method is called automatically after the XML-file is read.
        """
        try:
            sFN=self.GetFN()+'.loginCache.xml'
            if self.docLogin.Open(sFN)>=0:
                self.docLogin.Build()
            else:
                vtLog.vtLngCur(vtLog.WARN,'sFN:%s'%(sFN),self)
                self.docLogin.New()
                self.docLogin.Build()
                if self.docLogin.Save(sFN)<0:
                    vtLog.vtLngCur(vtLog.ERROR,'sFN:%s'%(sFN),self)
            vtXmlDomReg.Build(self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def OpenLogin(self):
        try:
            sFN=self.GetFN()+'.loginCache.xml'
            if self.docLogin.Open(sFN)>=0:
                self.docLogin.Build()
            else:
                vtLog.vtLngCur(vtLog.ERROR,'sFN:%s'%(sFN),self)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def BuildLoginCache(self):
        """ this method is called automatically after the XML-file is read.
        """
        try:
            self.docLogin.Build()
        except:
            vtLog.vtLngTB(self.__class__.__name__)
    def GetLoginCache(self,sSrvMain,sSrvSub):
        vtLog.vtLngCur(vtLog.INFO,'srvMain:%s;srvSub:%s'%(
                        sSrvMain,sSrvSub),self.GetAppl())
        try:
            return self.docLogin.GetLoginCache(sSrvMain,sSrvSub)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        return None
    def __isRunning__(self):
        try:
            bBusy=False
            for id,srvInst in self.dSrv.items():
                #dataInst=srvInst.GetDataInstance()
                #if dataInst is not None:
                #    if dataInst.IsServing():
                #        vtLog.vtLngCur(vtLog.INFO,'id:%08d server still serving'%(id),self.GetAppl())
                #        bBusy=True
                
                if srvInst.IsServing():
                    vtLog.vtLngCur(vtLog.INFO,'id:%08d server still serving'%(id),self.GetAppl())
                    bBusy=True
        except:
            vtLog.vtLngTB(self.__class__.__name__)
            bBusy=False
        if bBusy:
            return True
        return vtXmlDomReg.__isRunning__(self)
    def __stop__(self):
        vtLog.vtLngCur(vtLog.INFO,''%(),self.GetAppl())
        try:
            for id in self.dSrv.keys():
                if id in self.dSrv:
                    srvInst=self.dSrv[id]
                    node=self.getNodeByIdNum(id)
                    oReg=self.GetRegByNode(node)
                    oReg.Stop(node,srvInst.GetDataInstance())
                    srvInst.Stop(bWait=False)
        except:
            vtLog.vtLngTB(self.__class__.__name__)
        vtXmlDomReg.__stop__(self)
    # ----------------------------------
    # dir notify stuff
    def added(self,sAct,sFN,zTm):
        vtLog.vtLngCur(vtLog.INFO,'action:%s;FN:%s,time:%s'%(sAct,sFN,time.asctime(time.localtime(zTm))),self.appl)
    def deleted(self,sAct,sFN,zTm):
        vtLog.vtLngCur(vtLog.INFO,'action:%s;FN:%s,time:%s'%(sAct,sFN,time.asctime(time.localtime(zTm))),self.appl)
    def renamed(self,sAct,sFN,zTm):
        vtLog.vtLngCur(vtLog.INFO,'action:%s;FN:%s,time:%s'%(sAct,sFN,time.asctime(time.localtime(zTm))),self.appl)
    def changed(self,sAct,sFN,zTm):
        vtLog.vtLngCur(vtLog.INFO,'action:%s;FN:%s,time:%s'%(sAct,sFN,time.asctime(time.localtime(zTm))),self.appl)

