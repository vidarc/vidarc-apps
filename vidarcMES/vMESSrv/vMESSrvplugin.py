#----------------------------------------------------------------------------
# Name:         vMESSrvplugin.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      200060708
# CVS-ID:       $Id: vMESSrvplugin.py,v 1.3 2006/11/21 21:16:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.lang.vtLgBase as vtLgBase
import vidarc.tool.log.vtLog as vtLog

class vMESSrvplugin:
    def __init__(self,doc,oRoot,name,srvName,aliasName,iOrder,clsName,impName,dn):
        self.name=name
        self.clsName=clsName
        self.srvName=srvName
        self.aliasName=aliasName
        self.iOrder=iOrder
        self.impName=impName
        strs=self.impName.split('.')
        self.plugInName=strs[-1]
        self.dn=dn
        self.domain=''
        self.plugin=None
        self.objClass=None
        self.objReg=None
        self.img=None
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'reg:%s'%(vtLog.pformat(doc.regNodes)),self)
        self.__import__(doc,oRoot)
    def __repr__(self):
        if self.IsServer():
            return ' '.join([':'.join(['name',self.name]),
                            ':'.join(['cls',self.clsName]),
                            ':'.join(['SERVER',self.name]),
                            ':'.join(['imp',self.impName]),
                    ])
        else:
            return ' '.join([':'.join(['name',self.name]),
                            ':'.join(['cls',self.clsName]),
                            ':'.join(['srv',self.srvName]),
                            ':'.join(['imp',self.impName]),
                    ])
    def IsServer(self):
        return self.srvName is None
    def IsAliasSummary(self):
        return self.aliasName is not None
    def GetOrder(self):
        return self.iOrder
    def GetServerName(self):
        return self.srvName
    def GetPlugInName(self):
        return self.plugInName
    def GetName(self):
        return self.name
    def GetDN(self):
        return self.dn
    def GetRegObj(self):
        if self.objReg is not None:
            return self.objReg
        return None
    def GetRegName(self):
        if self.objReg is not None:
            return self.objReg.GetTagName()
        return None
    def GetImage(self):
        return self.img
    def GetObjClass(self):
        return self.objClass
    def GetDomain(self):
        return self.domain
    def GetAliasSummary(self):
        return self.aliasName
    def __import__(self,doc,oRoot):
        try:
            if self.objClass is None:
                #vtLog.CallStack('')
                strs=self.impName.split('.')
                self.domain=strs[-2]
                #print `self`
                #print strs,self.domain
                #print doc,oRoot
                if self.IsServer():
                    #self.plugin=__import__(self.impName,[],[],['RegisterNodes'])
                    self.plugin=__import__('.'.join(strs[:-1]+['__register__']),[],[],['RegisterNodes'])
                    self.objReg=getattr(self.plugin,'RegisterNodes')(doc,oRoot,True)
                    #print self.plugin,self.objReg
                    self.img=self.objReg.GetImage()
                else:
                    #print strs,self.domain
                    self.plugin=__import__(self.impName,[],[],[strs[-1],'getPluginData'])
                    self.objClass=getattr(self.plugin,strs[-1])
                    self.img=getattr(self.plugin,'getPluginData')()
        except:
            vtLog.vtLngTB(self.name)
    def create(self,parent,id,pos,size,style,name):
        self.__import__()
        try:
            parent.SetIcon(self.GetIcon())
        except:
            pass
        return self.objClass(parent,id,pos,size,style,name)
    def createFrame(self,parent,id,pos,size,style,name):
        self.__import__()
        return self.objClass(parent,id,pos,size,style,name)
    def getPossibleLang(self):
        try:
            return vtLgBase.getPossibleLang(self.domain,self.dn)
        except:
            return []
    def setLang(self,lang):
        try:
            vtLgBase.setPluginLang(self.domain,self.dn,lang)
        except:
            pass
