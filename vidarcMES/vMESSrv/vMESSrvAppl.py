#!/usr/bin/env python
#Boa:PyApp:main
#----------------------------------------------------------------------------
# Name:         vMsgSrvAppl.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vMESSrvAppl.py,v 1.8 2011/01/14 01:06:14 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import os,sys,getopt,traceback
import wx
import vSystem

import __init__
if __init__.VIDARC_IMPORT:
    print 'vidimp'
    import vidImp
import identify as vIdentify
mid=vIdentify.getMachineID()

import vtLogDef
import vtLgBase
import vSplashFrame as vSplashFrame

import images_splash as images_splash

modules ={u'vMESSrvMainFrame': [0, '', u'vMESSrvMainFrame.py']}

class BoaApp(wx.App):
    def __init__(self,num,sAppl,cfgFN,fn,iLogLv,iSockPort,localDN,bTime=False):
        self.fn=fn
        self.cfgFN=cfgFN
        self.iLogLv=iLogLv
        self.iSockPort=iSockPort
        self.sAppl=sAppl
        self.localDN=localDN
        self.bTime=bTime
        wx.App.__init__(self,num)
    def OnInit(self):
        optshort,optlong='l:f:c:h',['lang=','file=','config=','local=','log=','help','profile=']
        vtLgBase.initAppl(optshort,optlong,'vMESSrv')
        lang=vtLgBase.getApplLang()
        wx.InitAllImageHandlers()
        actionList=[]
        actionListPost=[]
        import __init__
        if __init__.VIDARC_IMPORT:
            d={'label':_(u'  import library ...'),
                'eval':'__import__("vidImp")'}
            actionList.append(d)
        else:
            d={'label':_(_(u'  import library ...')),
                'eval':'0'}
            actionList.append(d)
        d={'label':_(u'  import vMES Server ...'),
            'eval':'__import__("vMESSrvMainFrame")'}
        actionList.append(d)
        d={'label':_(u'  Create vMES Server ...'),
            'eval':'self.res[1].create'}
        actionList.append(d)
        #d={'label':_(u'  Create MES Server ...'),
        #    'eval':'getattr(self.res[2],"create")'}
        #actionList.append(d)
        d={'label':_(u'  Create vMES Server ...'),
            'eval':'self.res[2](None)'}
        actionList.append(d)
        self.iRes=len(actionList)-1
        
        d={'label':_(u'plugin searching ...')}
        actionList.append(d)
        
        d={'label':_(u'  Open File...'),
            'eval':'self.res[%d].SetLocalDN("%s")'%(self.iRes,
                                    self.localDN.replace('\\',"\\\\"))}
        actionListPost.append(d)
        
        d={'label':_(u'  Open Config (%s)...')%self.cfgFN,
            'eval':'self.res[3].OpenCfgFile("%s")'%self.cfgFN}
        actionListPost.append(d)
        
        d={'label':_(u'  Open File (%s)...')%self.fn,
            'eval':'self.res[3].OpenFile("%s")'%self.fn}
        actionListPost.append(d)
        
        self.splash = vSplashFrame.create(None,'MES','Server',
            None,
            actionList,
            actionListPost,
            self.sAppl,
            self.iLogLv,
            self.iSockPort,
            self.bTime)
        vSplashFrame.EVT_SPLASH_ACTION(self.splash,self.OnSplashAction)
        vSplashFrame.EVT_SPLASH_PROCESSED(self.splash,self.OnSplashProcessed)
        vSplashFrame.EVT_SPLASH_FINISHED(self.splash,self.OnSplashFinished)
        vSplashFrame.EVT_SPLASH_ABORTED(self.splash,self.OnSplashAborted)
        self.splash.Show()
        
        self.fn=None
        self.cfgFN=None
        return True
    def OpenFile(self,fn):
        self.fn=fn
    def OpenCfgFile(self,fn):
        self.cfgFN=fn
    def OnSplashAction(self,evt):
        self.splash.DoProcess()
        evt.Skip()
    def OnSplashProcessed(self,evt):
        self.splash.DoAction()
        evt.Skip()
    def OnSplashFinished(self,evt):
        self.main=self.splash.res[3]
        try:
            import vidarc.tool.log.vtLog as vtLog
            vtLog.vtLngSetLevel(self.splash.GetLogLv())
        except:
            pass
        wx.CallAfter(self.SetTopWindow,self.main)
        wx.CallAfter(vtLog.InitMsgWid)
        wx.CallAfter(self.main.SetLocalDN,self.localDN)
        wx.CallAfter(self.main.Show)
        
        self.splash.Destroy()
        self.splash=None
        evt.Skip()
    def OnSplashAborted(self,evt):
        try:
            self.main=self.splash.res[3]
            self.main.Destroy()
        except:
            pass
        self.splash.Destroy()
        self.splash=None
        evt.Skip()

def showHelp():
    print _('help')
    print sys.version
    print "  ",_("valid flags:")
    print "  ",_("-c <filename>")
    print "  ",_("--config <filename>")
    print "        ",_("open the <filename> to configure application")
    print "  ",_("-f <filename>")
    print "  ",_("--file <filename>")
    print "        ",_("open the <filename> at start")
    print "  ",_("--local <directory>")
    print "        ",_("specify local <directory>")
    print "  ","-h"
    print "  ",_("--help")
    print "        ",_("show this screen")
    print "  ","-l"
    print "  ",_("--lang <language id according ISO639>")
    print "  ",_("--log <level>")
    print "       ",_("0 = debug")
    print "       ",_("1 = information")
    print "       ",_("2 = waring")
    print "       ",_("3 = error")
    print "       ",_("4 = critical")
    print "       ",_("5 = fatal")
    
def main():
    optshort,optlong='l:f:c:h',['lang=','file=','config=','local=','log=','help']
    #vtLgBase.initAppl(optshort,optlong,'vMesSrv')
    
    fn=None
    cfgFN='vMesSrvCfg.xml'
    localDN=vSystem.getDefaultLocalDN()
    iLogLv=vtLogDef.ERROR
    try:
        optlist , args = getopt.getopt(sys.argv[1:],optshort,optlong)
        for o in optlist:
            if o[0] in ['-h','--help']:
                vtLgBase.initAppl(optshort,optlong,'vMESSrv')
                lang=vtLgBase.getApplLang()
                showHelp()
                return
            if o[0] in ['--file','-f']:
                fn=o[1]
            if o[0] in ['--config','-c']:
                cfgFN=o[1]
            if o[0] in ['--local']:
                localDN=o[1]
            if o[0] in ['--log']:
                if o[1]=='0':
                    iLogLv=vtLogDef.DEBUG
                elif o[1]=='1':
                    iLogLv=vtLogDef.INFO
                elif o[1]=='2':
                    iLogLv=vtLogDef.WARN
                elif o[1]=='3':
                    iLogLv=vtLogDef.ERROR
                elif o[1]=='4':
                    iLogLv=vtLogDef.CRITICAL
                elif o[1]=='5':
                    iLogLv=vtLogDef.FATAL
    except:
        vtLgBase.initAppl(optshort,optlong,'vMESSrv')
        lang=vtLgBase.getApplLang()
        showHelp()
        traceback.print_exc()
    try:
        os.makedirs(localDN)
    except:
        pass
    vtLogDef.LEVEL=iLogLv
    os.chdir(localDN)
    application = BoaApp(0,'vMESSrv',cfgFN,fn,iLogLv,60098,localDN,bTime=True)
    application.MainLoop()

if __name__ == '__main__':
    main()
