#Boa:Frame:vMESSrvMainFrame
#----------------------------------------------------------------------------
# Name:         vMESSrvMainFrame.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vMESSrvMainFrame.py,v 1.14 2012/01/09 11:06:32 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import cStringIO
import vidarc.vSrv.net.vsNetSrvControl
import vidarc.tool.xml.vtXmlNodeRegSelector
from wx.lib.anchors import LayoutAnchors
import wx.lib.buttons
import getpass,time

import vidarc.config.vcCust as vcCust
vcCust.set2Import('vidarc.ext',False)
#vcCust.set2Import('vidarc.tool',False)
vcCust.set2Import('vidarc.tool',True)
vcCust.set2Import('vidarc.vApps',False)
vcCust.set2Import('vidarc.vApps.vHum.vGroupNode',True)
vcCust.set2Import('vidarc.vApps.vHum.vGroupsNode',True)
vcCust.set2Import('vidarc.vApps.vHum.vUserNode',True)
vcCust.set2Import('vidarc.vApps.vHum.vUsersNode',True)

import vidarc.tool.xml.vtXmlDom as vtXmlDom

import vtLgBase
import vidarc.tool.log.vtLog as vtLog
try:
    import vidarc.vApps.common.vAboutDialog as vAboutDialog
    import vSystem
    #from vidarc.vApps.common.vMainFrame import vMainFrame
    from vidarc.tool.gui.vtgFrame import vtgFrame
    from vidarc.vApps.common.vMDIFrame import vMDIFrame
    from vidarc.vApps.common.vCfg import vCfg
    from vidarc.config.vcCfg import vcCfg
    from vidarc.vApps.common.vStatusBarMessageLine import vStatusBarMessageLine
    from vidarc.tool.log.vtLogFileViewerFrame import *
    
    from vXmlMESSrvTree  import *
    from vNetMESSrv import *
    from vIniSearchPathsDialog import vIniSearchPathsDialog
    #from vidarc.vSrv.vMsgSrv.vMsgSrvListBook import *
    from vidarc.tool.net.vtNetSecXmlGuiMaster import *
    import vidarc.tool.xml.vtXmlDomConsumer as vtXmlDomConsumer
    import vidarc.tool.InOut.fnUtil as fnUtil
    import vidarc.tool.art.vtArt as vtArt
    from vMsgDialog import vMsgDialog
    
    from vidarc.tool.net.vtNetXml import *
    
    #from vidarc.vApps.vHum.vNetHum import *
    import vMESSrvpluginFind
    import IniFile
    
    from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAlias import vXmlNodeXmlSrvAlias
    from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainer import vXmlNodeXmlSrvAliasContainer
    from vidarc.vSrv.vXmlSrv.vXmlNodeXmlSrvAliasContainerSummary import vXmlNodeXmlSrvAliasContainerSummary
    
    import images
    
    import identify as vIdentify    # 070708:wro shall not be called by a thread fist
    vIdentify.getMachineID()
except:
    vtLog.vtLngTB('import')

def create(parent):
    return vMESSrvMainFrame(parent)

[wxID_VGFPRJMAIN, wxID_VGFPRJMAINSPWMAIN, 
] = map(lambda _init_ctrls: wx.NewId(), range(2))

[wxID_VMESSRVMAINFRAME, wxID_VMESSRVMAINFRAMEPNDATA, 
 wxID_VMESSRVMAINFRAMESBSTATUS, wxID_VMESSRVMAINFRAMESLWNAV, 
 wxID_VMESSRVMAINFRAMEVIREGSEL, 
] = [wx.NewId() for _init_ctrls in range(5)]

def getPluginImage():
    return images.getPluginImage()

def getApplicationIcon():
    icon = wx.EmptyIcon()
    icon.CopyFromBitmap(images.getApplicationBitmap())
    return icon

[wxID_VGFPRJTIMERMAINTOOLBAR1TOOLS0] = [wx.NewId() for _init_coll_toolBar1_Tools in range(1)]

[wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_ABOUT, 
 wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_HELP, 
 wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_LOG, 
 wxID_VMESSRVMAINFRAMEMNHELPMN_FILE_SETTINGS,
] = [wx.NewId() for _init_coll_mnHelp_Items in range(4)]

[wxID_VMESSRVMAINFRAMEMNFILEITEM_EXIT] = [wx.NewId() for _init_coll_mnFile_Items in range(1)]

#class vMESSrvMainFrame(wx.Frame,vMDIFrame,vStatusBarMessageLine):
class vMESSrvMainFrame(vtgFrame,vMDIFrame,vcCfg):
    STATUS_TEXT_POS=2
    STATUS_LOG_POS=3
    STATUS_CLK_POS=4
    CFG_PLUGIN_FN='vMESSrvCfgPlugIn.ini'
    def _init_coll_fgsData_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableRow(0)
        parent.AddGrowableCol(0)

    def _init_coll_fgsData_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.viRegSel, 0, border=4, flag=wx.ALL | wx.EXPAND)

    def _init_coll_mnFile_Items(self, parent):
        # generated method, don't edit

        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMESSRVMAINFRAMEMNFILEITEM_EXIT,
              kind=wx.ITEM_NORMAL, text=_(u'E&xit\tAlt+X'))
        self.Bind(wx.EVT_MENU, self.OnMnFileItem_exitMenu,
              id=wxID_VMESSRVMAINFRAMEMNFILEITEM_EXIT)

    def _init_coll_mbMain_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.mnFile, title=_(u'&File'))
        parent.Append(menu=self.mnView, title=_(u'View'))
        parent.Append(menu=self.mnAnalyse, title=_(u'Analyse'))
        parent.Append(menu=self.mnTools, title=_(u'Tools'))
        parent.Append(menu=self.mnHelp, title=u'?')

    def _init_coll_mnHelp_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_HELP,
              kind=wx.ITEM_NORMAL, text=u'?\tF1')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_ABOUT,
              kind=wx.ITEM_NORMAL, text=_(u'About'))
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_LOG,
              kind=wx.ITEM_NORMAL, text=_(u'Log'))
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_helpMenu,
              id=wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_HELP)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_aboutMenu,
              id=wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_ABOUT)
        self.Bind(wx.EVT_MENU, self.OnMnHelpMn_help_logMenu,
              id=wxID_VMESSRVMAINFRAMEMNHELPMN_HELP_LOG)

    def _init_coll_sbStatus_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(5)

        parent.SetStatusText(number=0, text=u'')
        parent.SetStatusText(number=1, text=u'')
        parent.SetStatusText(number=2, text=u'')
        parent.SetStatusText(number=3, text=u'')
        parent.SetStatusText(number=4, text=u'')

        parent.SetStatusWidths([20, 120, -1, 100, 200])

    def _init_sizers(self):
        # generated method, don't edit
        self.fgsData = wx.FlexGridSizer(cols=1, hgap=0, rows=1, vgap=0)

        self._init_coll_fgsData_Items(self.fgsData)
        self._init_coll_fgsData_Growables(self.fgsData)

        self.pnData.SetSizer(self.fgsData)

    def _init_utils(self):
        # generated method, don't edit
        self.mbMain = wx.MenuBar()

        self.mnFile = wx.Menu(title=u'')

        self.mnAnalyse = wx.Menu(title=u'')

        self.mnTools = wx.Menu(title=u'')

        self.mnView = wx.Menu(title=u'')

        self.mnHelp = wx.Menu(title=u'')

        self._init_coll_mbMain_Menus(self.mbMain)
        self._init_coll_mnFile_Items(self.mnFile)
        self._init_coll_mnHelp_Items(self.mnHelp)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_VMESSRVMAINFRAME,
              name=u'vMESSrvMainFrame', parent=prnt, pos=wx.Point(115, 10),
              size=wx.Size(642, 354), style=wx.DEFAULT_FRAME_STYLE,
              title=u'VIDARC MES Server')
        self._init_utils()
        self.SetClientSize(wx.Size(634, 327))
        self.SetMenuBar(self.mbMain)
        #self.Bind(wx.EVT_CLOSE, self.OnFrameClose)
        #self.Bind(wx.EVT_ACTIVATE, self.OnFrameActivate)
        self.Bind(wx.EVT_SIZE, self.OnFrameSize)
        #self.Bind(wx.EVT_MOVE, self.OnFrameMove)

        self.slwNav = wx.SashLayoutWindow(id=wxID_VMESSRVMAINFRAMESLWNAV,
              name=u'slwNav', parent=self, pos=wx.Point(0, 0), size=wx.Size(200,
              288), style=wx.CLIP_CHILDREN | wx.SW_3D)
        self.slwNav.SetDefaultSize(wx.Size(200, 1000))
        self.slwNav.SetAlignment(wx.LAYOUT_LEFT)
        self.slwNav.SetAutoLayout(True)
        self.slwNav.SetSashVisible(wx.SASH_RIGHT, True)
        self.slwNav.SetOrientation(wx.LAYOUT_VERTICAL)
        self.slwNav.SetLabel(_(u'navigation'))
        self.slwNav.SetToolTipString(_(u'navigation'))
        self.slwNav.Bind(wx.EVT_SASH_DRAGGED, self.OnSlwNavSashDragged,
              id=wxID_VMESSRVMAINFRAMESLWNAV)

        self.pnData = wx.Panel(id=wxID_VMESSRVMAINFRAMEPNDATA, name=u'pnData',
              parent=self, pos=wx.Point(208, 8), size=wx.Size(416, 272),
              style=wx.SUNKEN_BORDER | wx.TAB_TRAVERSAL)

        self.viRegSel = vidarc.tool.xml.vtXmlNodeRegSelector.vtXmlNodeRegSelector(id=wxID_VMESSRVMAINFRAMEVIREGSEL,
              name=u'viRegSel', parent=self.pnData, pos=wx.Point(4, 4),
              size=wx.Size(404, 260), style=0)

        self._init_sizers()

    def __init__(self, parent):
        try:
            self._init_ctrls(parent)
            self.pn=self
            vMDIFrame.__init__(self,u'MESSrv',iNotifyTime=250)
            #vMainFrame.__init__(self)
            #vStatusBarMessageLine.__init__(self,self.sbStatus,self.STATUS_TEXT_POS,-1)
            self.bActivated=False
            self.localDN=vSystem.getDefaultLocalDN()            
            
            self.baseDoc=None
            self.baseSettings=None
            self.dlgPlugInPaths=None
            
            self.xdCfg=vtXmlDom(appl='vMESSrvCfg',audit_trail=False)
            self.cfg=IniFile.Config()
            
            icon = getApplicationIcon()
            self.SetIcon(icon)
            
            appls=['vMESSrv']
            
            rect = self.sbStatus.GetFieldRect(0)
            self.netMaster=vtNetSecXmlGuiMaster(self.sbStatus,'Master',pos=(rect.x+2, rect.y+2),
                        size=(rect.width-4, rect.height-4),verbose=1)
            self.netMaster.BindEvents(
                    funcStart=self.OnMasterStart,
                    funcFinish=self.OnMasterFinish,
                    funcShutDown=self.OnMasterShutDown)
            
            self.netMESSrv=self.netMaster.AddNetControl(vNetMESSrv,'vMESSrv',synch=True,verbose=0,
                                                audit_trail=True)
            #self.netHum=self.netMaster.AddNetControl(vNetHum,'vHum',synch=True,verbose=0)
            self.netMaster.AddNetControl(None,None,verbose=0)
            
            self.netMaster.SetCfg(self.xdCfg,[],appls,'vMsgSrvAppl')
            EVT_NET_XML_OPEN_OK(self.netMESSrv,self.OnOpenOk)
            
            EVT_NET_XML_SYNCH_PROC(self.netMESSrv,self.OnSynchProc)
            EVT_NET_XML_SYNCH_FINISHED(self.netMESSrv,self.OnSynchFinished)
            EVT_NET_XML_SYNCH_ABORTED(self.netMESSrv,self.OnSynchAborted)
            
            EVT_NET_XML_GOT_CONTENT(self.netMESSrv,self.OnGotContent)
            
            self.bAutoConnect=True
            
            
            self.vgpTree=vXmlMESSrvTree(self.slwNav,wx.NewId(),
                    pos=(0,0),size=(198,529),style=0,name="trMESSrv",
                    master=True,verbose=0)
            EVT_VTXMLTREE_ITEM_SELECTED(self.vgpTree,self.OnTreeItemSel)
            self.vgpTree.SetDoc(self.netMESSrv,True)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS_FINISHED(self.vgpTree,self.OnAddElementsFin)
            EVT_VTXMLTREE_THREAD_ADD_ELEMENTS(self.vgpTree,self.OnAddElementsProgress)
            
            oMES=self.netMESSrv.GetRegisteredNode('vMESSrv')
            pnCls=oMES.GetPanelClass()
            pn=pnCls(self.viRegSel,wx.NewId(),
                    pos=(8,8),size=(576, 176),style=0,name="pnMESSrvPlugIn")
            pn.SetDoc(self.netMESSrv,True)
            self.viRegSel.AddWidgetDependent(pn,oMES)
            pn.Show(False)
            
            self.viRegSel.SetDoc(self.netMESSrv,True)
            self.viRegSel.SetRegNode(oMES)
            self.viRegSel.SetNetMaster(self.netMaster)
            
            self.iLogOldSum=0
            self.zLogUnChanged=0
            # setup statusbar
            self.timer = wx.PyTimer(self.Notify)
            self.timer.Start(1000)
            self.Notify()
            
            rect = self.sbStatus.GetFieldRect(1)
            self.gProcess = wx.Gauge(
                        self.sbStatus, -1, 50, (rect.x+2, rect.y+2), 
                        (rect.width-4, rect.height-4), 
                        wx.GA_HORIZONTAL|wx.GA_SMOOTH
                        )
            self.gProcess.SetRange(100)
            self.gProcess.SetValue(0)
            
            #vMainFrame.AddMenus(self , self , self.vgpTree , self.netMaster,
            #        self.mnFile , self.mnView , self.mnTools)
            self.AddMenus(self , self.vgpTree , self.netMaster,
                self.mnFile , self.mnView , self.mnTools , self.mnAnalyse)
            
            menu=self.mnFile
            mnIt=wx.MenuItem(menu,wxID_VMESSRVMAINFRAMEMNHELPMN_FILE_SETTINGS,
                        _(u'Search Paths'))
            wx.EVT_MENU(self,wxID_VMESSRVMAINFRAMEMNHELPMN_FILE_SETTINGS,
                        self.OnSearchPaths)
            menu.InsertItem(0,mnIt)
            mnIt=wx.MenuItem(menu,wx.ID_SEPARATOR)
            menu.InsertItem(1,mnIt)
            
            self.PopulateToolBar(self)
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            #self.Layout()
            #self.Fit()
            #self.OpenCfgFile()
            self.__makeTitle__()
            wx.CallAfter(self.OpenCfgPlugInFile)
            wx.CallAfter(self.doFindPlugInStart)
            self.netMESSrv.SetPlugInParent(self)
            vMESSrvpluginFind.EVT_VMESPLUGIN_THREAD_PROC(self,self.OnPlugInProc)
            vMESSrvpluginFind.EVT_VMESPLUGIN_THREAD_ADD(self,self.OnPlugInAdd)
            vMESSrvpluginFind.EVT_VMESPLUGIN_THREAD_ADD_FINISHED(self,self.OnPlugInFin)
            self.Bind(wx.EVT_ACTIVATE, self.OnMainActivate)
            #self.bPlugInBusy=True
            #self.netMESSrv.FindPlugIn(None,self.__plugInFin__)
        except:
            vtLog.vtLngTB(self.GetName())
    def GetDocMain(self):
        return self.netMESSrv
    def GetNetMaster(self):
        return self.netMaster
    def GetTreeMain(self):
        return self.vgpTree
    def PopulateToolBar(self,parent):
        id=wx.NewId()
        sz=self.GetClientSize()
        self.tbMain = wx.ToolBar(id=id,
                name=u'tbPrj', parent=parent, pos=wx.Point(0,0),
                size=wx.DefaultSize, style=wx.TB_HORIZONTAL | wx.NO_BORDER)
        self.tbMain.SetToolBitmapSize(wx.Size(20,20))
        self.dToolBar={}
        self.vgpTree.PopulateToolBar(parent,self.tbMain,self.dToolBar)
        #self.tbMain.AddSimpleTool(id, vtArt.getBitmap(vtArt.Duplicate), _(u"Duplicate"), 
        #                _(u"duplicate"))
        #self.Bind(wx.EVT_TOOL, self.OnToolDupRecipeClick, id=id)
        self.tbMain.AddSeparator()
        id=wx.NewId()
        self.dToolBar['quickFind']=id
        txtCtrl=wx.TextCtrl(self.tbMain, id, "", size=(150, -1))
        self.tbMain.AddControl(txtCtrl)
        self.tbMain.Bind(wx.EVT_TEXT, self.OnQuickFind, id=id)
        self.tbMain.Bind(wx.EVT_TEXT_ENTER, self.OnQuickFindAgain, id=id)
        self.txtQuickFind=txtCtrl
        
        parent.SetToolBar(self.tbMain)
        self.tbMain.Realize()
        return self.tbMain
    def OnQuickFind(self,event):
        sVal=self.txtQuickFind.GetValue()
        self.idQuickFoundLast=-1
    def OnQuickFindAgain(self,event):
        sVal=self.txtQuickFind.GetValue()
        id=self.vgpTree.GetMoveTargetID()
        self.thdFindQuick.Do(id,self.idQuickFoundLast,sVal,100)
    def OnFindQuickFound(self,evt):
        evt.Skip()
        try:
            vtLog.vtLngCurWX(vtLog.INFO,'id:%08d'%(evt.GetID()),self)
            self.idQuickFoundLast=evt.GetID()
            self.vgpTree.SelectByID('%08d'%self.idQuickFoundLast)
        except:
            vtLog.vtLngTB(self.GetName())
            vtLog.vtLngCurWX(vtLog.ERROR,'id:%08d'%(evt.GetID()),self)
    def OnSearchPaths(self,evt):
        evt.Skip()
        try:
            if self.dlgPlugInPaths is None:
                self.dlgPlugInPaths=vIniSearchPathsDialog(self)
                self.dlgPlugInPaths.SetIniFN(self.CFG_PLUGIN_FN)
                self.dlgPlugInPaths.Centre()
            self.dlgPlugInPaths.SetCfg()
            #self.dlgSettings.SetMode(self.bAdvanced)
            iRet=self.dlgPlugInPaths.ShowModal()
            if iRet>0:
                #self.bAdvanced=self.dlgSettings.GetMode()
                #section=self.cfg.getSection('user')
                #section.setInfo('mode_advanced',str(int(self.bAdvanced)))
                #if self.docCfg:
                #    node=self.docCfg.getChildForced(None,'user')
                #    self.docCfg.SetValue(node,'mode',int(self.bAdvanced))
                    #self.__showRecent__()
                #    self.docCfg.Save()
                self.cfg.readFN(self.CFG_PLUGIN_FN)
                #self.__destroyVidarcObjs__()
                #self.__bUpdated__=True
                #if self.GUI_THREAD:
                #    self.thdGui.Do('__check4Plugins__',self.__check4PlugIns__)
                #else:
                #    wx.CallAfter(self.__check4PlugIns__)
                #return
            #self.__check4PlugIns__()
            #self.__bUpdated__=False
        except:
            vtLog.vtLngTB(self.GetName())
    def doFindPlugInStart(self):
        try:
            self.bPlugInBusy=True
            lDN=self.getSearchPaths()
            if len(lDN)==0:
                lDN=None
            self.netMESSrv.FindPlugIn(lDN,None)
        except:
            vtLog.vtLngTB(self.GetName())
    def OpenCfgPlugInFile(self):
        vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.cfg.readFN(self.CFG_PLUGIN_FN)
    def getSearchPaths(self):
        try:
            vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
            lDN=[]
            section=self.cfg.getSection('plugins')
            for i in xrange(999):
                dn=section.getInfo('path_%03d'%i)
                if dn is None:
                    break
                dn=dn.strip()
                if os.path.exists(dn):
                    lDN.append(dn)
                else:
                    try:
                        vtLog.vtLngCurWX(vtLog.ERROR,'dn:%s does not exit'%(dn),self)
                    except:
                        vtLog.vtLngTB(self.GetName())
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'lDN:%s'%(vtLog.pformat(lDN)),self)
            return lDN
        except:
            pass
    def __plugInFin__(self):
        self.netMESSrv.BuildPlugInDict()
        self.bPlugInBusy=False
        vtLog.vtLngCurWX(vtLog.DEBUG,'plugIn:%s'%(vtLog.pformat(self.netMESSrv.GetPlugInDict())),self)
    def OnPlugInProc(self,evt):
        try:
            iAct=evt.GetAct()
            iSize=evt.GetSize()
            if iSize>0:
                iVal=((iAct/float(iSize))*1000)
            else:
                iVal=0
            self.gProcess.SetValue(iVal)
        except:
            vtLog.vtLngTB(self.GetName())
    def __addRegPanel__(self,oReg):
        img=oReg.GetImage()
        if self.viRegSel.IsDependentAdded(oReg)==False:
            oMESSrv=self.netMESSrv.GetReg('vMESSrv')
            self.netMESSrv.RegisterNode(oReg,False)
            self.netMESSrv.LinkRegisteredNode(oMESSrv,oReg)
            
            self.vgpTree.__addElemImage2ImageList__(oReg.GetTagName(),img,img,False)
            
            pnCls=oReg.GetPanelClass()
            pn=pnCls(self.viRegSel,wx.NewId(),
                        pos=(8,8),size=(576, 176),style=0,name="pn%s"%oReg.GetTagName())
            pn.SetRegNode(oReg)
            pn.SetDoc(self.netMESSrv,True)
            self.viRegSel.AddWidgetDependent(pn,oReg)
            pn.Show(False)
            pn.SetNetMaster(self.netMaster)
            #self.viRegSel.SetRegNode(oReg)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurWX(vtLog.DEBUG,'tag:%s'%(oReg.GetTagName()),self)
        for o in self.netMESSrv.getPossibleReg(oReg):
            if o.GetTagName()!=oReg.GetTagName():
                self.__addRegPanel__(o)
            else:
                self.netMESSrv.LinkRegisteredNode(oReg,oReg)
    def OnPlugInAdd(self,evt):
        try:
            oPlugIn=evt.GetPlugIn()
            if oPlugIn.IsServer():
                #img=oPlugIn.GetImage()
                #self.vgpTree.__addElemImage2ImageList__(oPlugIn.GetRegName(),img,img,False)
                oReg=oPlugIn.GetRegObj()
                self.__addRegPanel__(oReg)
            self.PrintMsg(_('plugin searching ... %s found ')%(oPlugIn.GetName()))
        except:
            vtLog.vtLngTB(self.GetName())
    def OnPlugInFin(self,evt):
        try:
            self.gProcess.SetValue(0)
            dPlugIn=self.netMESSrv.GetPlugInDict()
            for k,oPlugIn in dPlugIn.items():
                if oPlugIn.IsServer():
                    continue
                sSrv=oPlugIn.GetServerName()
                oReg=self.netMESSrv.GetReg(sSrv)
                imgData=oPlugIn.GetImage()
                if oPlugIn.IsAliasSummary():
                    o=vXmlNodeXmlSrvAliasContainerSummary(oPlugIn.GetDomain(),oPlugIn.GetObjClass(),imgData,
                                oPlugIn.GetAliasSummary())
                    oAlias=self.netMESSrv.GetReg(''.join([sSrv,'AliasSummary']))
                else:
                    o=vXmlNodeXmlSrvAliasContainer(oPlugIn.GetDomain(),oPlugIn.GetObjClass(),imgData)
                    oAlias=self.netMESSrv.GetReg(''.join([sSrv,'Alias']))
                self.netMESSrv.RegisterNode(o,False)
                self.netMESSrv.LinkRegisteredNode(oReg,o)
                if oAlias is not None:
                    self.netMESSrv.LinkRegisteredNode(o,oAlias)
                self.__addRegPanel__(o)
                stream = cStringIO.StringIO(imgData)
                img=wx.ImageFromStream(stream)
                self.vgpTree.__addElemImage2ImageList__(oPlugIn.GetDomain(),img,img,False)
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurWX(vtLog.DEBUG,'reg:%s'%(vtLog.pformat(self.netMESSrv.lstRegNodes)),self)
            self.PrintMsg(_('plugin search finished.'))
            self.bPlugInBusy=False
            #self.netMaster.ShowPopup()
        except:
            vtLog.vtLngTB(self.GetName())
    def IsBusy(self):
        return self.bPlugInBusy
    def Notify(self):
        vtLog.vtLngNumTrend()
        vMDIFrame.Notify(self)
    def Notify_old(self):
        try:
            if self.STATUS_CLK_POS>=0:
                t = time.localtime(time.time())
                st = time.strftime(_("%b-%d-%Y   %H:%M:%S wk:%U"), t)
                self.SetStatusText(st, self.STATUS_CLK_POS)
            self.__makeTitle__()
            if self.STATUS_TEXT_POS>=0:
                tup=self.GetPrintMsgTup()
                if tup is not None:
                    #self.rbMsg.put(tup)
                   # self.sb.SetStatusText(tup[0], self.iNum)
                    bIsShown=self.IsShownPopupStatusBar()
                    self.zMsgLast=time.clock()
                    while tup is not None:
                        if bIsShown:
                            self.AddMsg2PopupStatusBar(tup,0)
                        tupLast=tup
                        tup=self.GetPrintMsgTup()
                    self.sb.SetStatusText(tupLast[0], self.iNum)
                else:
                    if (time.clock()-self.zMsgLast)>5:
                        self.sb.SetStatusText('', self.iNum)
            
            if self.STATUS_LOG_POS>=0:
                vtLog.vtLngNumTrend()
                s=vtLog.vtLngGetTrendOrderedStr()
                self.SetStatusText(s, self.STATUS_LOG_POS)
        except:
            vtLog.vtLngTB(self.GetName())
    def __makeTitle__(self):
        sOldTitle=self.GetTitle()
        s=_("VIDARC MES Server")
        fn=self.netMESSrv.GetFN()
        if fn is None:
            s=s+_(" (undef*)")
        else:
            s=s+" ("+fn
            if self.netMaster.IsModified():
                s=s+"*)"
            else:
                s=s+")"
        if s!=sOldTitle:
            self.SetTitle(s)
    def __setModified__(self,state):
        self.__makeTitle__()
    def OnAddElementsProgress(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(evt.GetValue())
            if evt.GetCount()>=0:
                self.gProcess.SetRange(evt.GetCount())
        evt.Skip()
    def OnAddElementsFin(self,evt):
        if self.gProcess is not None:
            self.gProcess.SetValue(0)
        self.PrintMsg(_('generated.'))
        evt.Skip()
    def OnMasterStart(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.__makeTitle__()
        self.PrintMsg(_('master started ...'))
    def OnMasterFinish(self,evt):
        evt.Skip()
        if self.VERBOSE:
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        self.PrintMsg(_('master finished.'))
    def OnMasterShutDown(self,evt):
        evt.Skip()
        vtLog.vtLngCurWX(vtLog.INFO,'',self)
        #self.Destroy()
        #vtLog.CallStack('')
        #self.Show(False)
        import time
        time.sleep(4)
        self.Close()
    def OnTreeItemSel(self,event):
        node=event.GetTreeNodeData()
        self.viRegSel.SetNode(node)
        event.Skip()
    def OnOpenStart(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('file opened ...'))
        evt.Skip()
    def OnOpenOk(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('file opened and parsed.'))
        #vtLog.CallStack('')
        #print self.netMESSrv.getIds()
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnOpenFault(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('file open fault.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnSynchStart(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(0,evt.GetCount()))
        evt.Skip()
    def OnSynchProc(self,evt):
        #vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.gProcess.SetRange(evt.GetCount())
        self.gProcess.SetValue(evt.GetAct())
        self.PrintMsg(_('synch started ...')+' %07d - %07d'%(evt.GetAct(),evt.GetCount()))
        evt.Skip()
    def OnSynchFinished(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('synch finished.'))
        self.__getXmlBaseNodes__()
        self.gProcess.SetValue(0)
        evt.Skip()
    def OnSynchAborted(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('synch aborted.'))
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnConnect(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.__makeTitle__()
        self.PrintMsg(_('connection established ... please wait'))
        evt.Skip()
    def OnDisConnect(self,evt):
        vtLog.vtLngCurWX(vtLog.INFO,''%(),self)
        self.PrintMsg(_('connection lost.'))
        evt.Skip()
    def OnLogin(self,evt):
        self.PrintMsg(_('login ... '))
        evt.Skip()
    def OnLoggedin(self,evt):
        self.PrintMsg(_('get content ... '))
        evt.Skip()
    def OnSetupChanged(self,evt):
        if self.netMESSrv.ReConnect2Srv()==0:
            #self.netHum.ReConnect2Srv()
            pass
    def OnGotContent(self,evt):
        self.PrintMsg(_('content got. generate ...'))
        #vtLog.CallStack('')
        #print self.netMESSrv.getIds().GetIDs()
        #print self.netMESSrv.getIds().GetFPs().keys()
        self.__getXmlBaseNodes__()
        evt.Skip()
    def OnClosed(self,evt):
        self.PrintMsg(_('connection closed. '))
        evt.Skip()
    def OnConnect(self,evt):
        self.__makeTitle__()
        evt.Skip()
    def OnChanged(self,evt):
        self.__setModified__(True)
        evt.Skip()
    def SetLocalDN(self,dn):
        vtLog.vtLngCurWX(vtLog.DEBUG,''%(),self)
        self.localDN=dn
        try:
            import vidarc.config.vcCust as vcCust
            vcCust.USR_LOCAL_DN=self.localDN
        except:
            vtLog.vtLngTB(self.GetName())
        self.netMaster.SetLocalDN(self.localDN)
    def OpenFile(self,fn):
        if self.netMESSrv.ClearAutoFN()>0:
            self.netMESSrv.Open(fn)
    def OpenCfgFile(self,fn=None):
        if fn is not None:
            iRet=self.xdCfg.Open(fn)
            if iRet>=0:
                self.netMaster.SetCfgNode()
                self.__makeTitle__()
                self.__setCfg__()
                return
        self.xdCfg.New(root='config')
        if fn is not None:
            self.xdCfg.SetFN(fn)
            self.netMaster.SetCfgNode()
        self.__setCfg__()
    def CreateNew(self):
        self.netMESSrv.New()
        
    def OnTreeItemAdd(self,event):
        #print "added",event.GetVgpTree(),event.GetTreeNode(),event.GetTreeNodeData()
        #vtXmlDomAlign(self.dom)
        self.__setModified__(True)
        event.Skip()
    def __getSettings__(self):
        return
    def __getXmlBaseNodes__(self):
        self.baseDoc=None
        self.baseSettings=None
        
        self.baseDoc=self.netMESSrv.getBaseNode()
        self.baseSettings=self.netMESSrv.getChildForced(self.netMESSrv.getRoot(),'settings')
        self.__getSettings__()
    def OnMnFileItem_exitMenu(self, event):
        self.Close()
        event.Skip()

    def OnMnFileItem_settingMenu(self, event):
        nodeSetting=self.netMESSrv.getChild(self.netMESSrv.getRoot(),'settings')
        if nodeSetting is not None:
            dlg=vMsgSrvSettingsDialog(self)
            dlg.SetXml(self.netMESSrv,nodeSetting)
            ret=dlg.ShowModal()
            if ret>0:
                dlg.GetXml(self.netMESSrv,nodeSetting)
                self.__getSettings__()
            dlg.Destroy()
        event.Skip()

    def OnFrameClose(self, event):
        vtLog.vtLngCurWX(vtLog.INFO,'CanVeto:%s'%(event.CanVeto()),self)
        try:
            if self.netMaster is not None:
                if self.netMaster.IsShutDownActive()==False:
                    if event.CanVeto():
                        self.GetCfgData()
                        if self.netMaster.IsModified()==True:
                            dlg = vtmMsgDialog(self,#wx.MessageDialog(widDlgPar,#self,
                                        _(u'Unsaved data present!\n\nDo you want to save data?'),
                                        u'VIDARC ' + self.appl + u' '+ _(u'Close'),
                                        wx.YES_NO | wx.YES_DEFAULT |  wx.ICON_QUESTION
                                        #wx.YES_NO | wx.NO_DEFAULT | wx.CANCEL | wx.ICON_INFORMATION
                                        )
                            if dlg.ShowModal()==wx.ID_YES:
                                #self.SaveFile()
                                self.netMaster.Save()
                                pass
                        self.netMaster.ShutDown()
                        event.Veto()
                        vtLog.vtLngCurWX(vtLog.INFO,'veto',self)
                    else:
                        event.Skip()
                else:
                    if self.netMaster.IsShutDownFinished()==False:
                        if event.CanVeto():
                            event.Veto()
                        else:
                            event.Skip()
                    else:
                        event.Skip()
            else:
                event.Skip()
            vtLog.vtLngCurWX(vtLog.INFO,'',self)
        except:
            vtLog.vtLngTB(self.GetName())
            event.Skip()
    def OnMainFrameIdle(self, event):
        event.Skip()
        try:
            self.Unbind(wx.EVT_IDLE)
        except:
            vtLog.vtLngTB(self.GetName())
    def OnMainActivate(self, event):
        if self.IsShown():
            if self.bActivated==False:
                #if self.xdCfg.GetFN() is None:
                #    self.xOpenCfgFile()
                wx.CallAfter(self.netMaster.ShowPopup)
                self.bActivated=True
            self.Unbind(wx.EVT_ACTIVATE)
        event.Skip()
    def __setCfg__(self):
        try:
            node=self.xdCfg.getChildForced(None,'size')
            iX=self.xdCfg.GetValue(node,'x',int,20)
            iY=self.xdCfg.GetValue(node,'y',int,20)
            iWidth=self.xdCfg.GetValue(node,'width',int,800)
            iHeight=self.xdCfg.GetValue(node,'height',int,600)
            iX,iY,iWidth,iHeight=vSystem.LimitWindowToScreen(iX,iY,iWidth,iHeight)
            self.Move((iX,iY))
            self.SetSize((iWidth,iHeight))
            
            node=self.xdCfg.getChildForced(None,'layout')
            
            i=self.xdCfg.GetValue(node,'nav_sash',int,200)
            self.slwNav.SetDefaultSize((i, 1000))
            
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
            
            self.xdCfg.AlignDoc()
            self.xdCfg.Save()
            self.bBlockCfgEventHandling=False
        except:
            vtLog.vtLngTB(self.GetName())
    def OnSlwNavSashDragged(self, event):
        try:
            if event.GetDragStatus() == wx.SASH_STATUS_OUT_OF_RANGE:
                return
            node=self.xdCfg.getChildForced(None,'layout')
            iWidth=event.GetDragRect().width
            #if iWidth>80:
            #    iWidth=80
            self.xdCfg.SetValue(node,'nav_sash',iWidth)
            self.xdCfg.Save()
            
            self.slwNav.SetDefaultSize((iWidth, 1000))
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameSize(self, event):
        event.Skip()
        try:
            #bMod=False
            #sz=event.GetSize()
            #node=self.xdCfg.getChildForced(None,'size')
            #self.xdCfg.SetValue(node,'width',sz[0])
            #self.xdCfg.SetValue(node,'height',sz[1])
            #self.xdCfg.Save()
            wx.LayoutAlgorithm().LayoutWindow(self, self.pnData)
            self.pnData.Refresh()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnFrameMove(self, event):
        event.Skip()
        try:
            pos=self.GetPosition()
            node=self.xdCfg.getChildForced(None,'size')
            self.xdCfg.SetValue(node,'x',pos[0])
            self.xdCfg.SetValue(node,'y',pos[1])
            self.xdCfg.Save()
        except:
            vtLog.vtLngTB(self.GetName())
    def OnCbApplyButton(self, event):
        event.Skip()

    def OnCbCancelButton(self, event):
        event.Skip()

    def OnMnHelpMn_help_helpMenu(self, event):
        try:
            self.hlp=wx.html.HtmlHelpController()
            self.hlp.AddBook('vMesSrvHelpBook.hhp')
            self.hlp.DisplayContents()
        except:
            vtLog.vtLngTB(self.GetName())
        event.Skip()

    def OnMnHelpMn_help_aboutMenu(self, event):
        import __config__
        version=u' %d.%d.%d.%d'%(__config__.VER_MAJOR,__config__.VER_MINOR,
                    __config__.VER_RELEASE,__config__.VER_SUBREL)
        dlg=vAboutDialog.create(self,images.getApplicationBitmap(),
                _(u'VIDARC MES Server'),
                _(u"""This is the MES networking core server.
The MES server can be updated without restarting.

Designed by VIDARC Automation GmbH, Walter Obweger.

Please visit our web site http://www.vidarc.com.
Send questions and feedback to office@vidarc.com. 
We like to here from you.
"""),
                version)
        dlg.Centre()
        dlg.ShowModal()
        dlg.Destroy()
        event.Skip()

    def OnMnHelpMn_help_logMenu(self, event):
        frm=vtLogFileViewerFrame(self)
        frm.Centre()
        sFN=vtLog.vtLngGetFN()
        if sFN is not None:
            frm.OpenFile(sFN)
        frm.Show()
        event.Skip()
    def SetCfgData(self,func,l,d):
        self.cfgFunc=func
        self.cfgOrigin=l
        pass
    def GetCfgData(self):
        d={}
        if self.cfgFunc is not None:
            self.cfgFunc(self.cfgOrigin,d)

    def OnCbStartButton(self, event):
        event.Skip()

    def OnCbStopButton(self, event):
        event.Skip()
