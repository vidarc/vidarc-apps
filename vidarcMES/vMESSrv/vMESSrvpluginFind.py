#----------------------------------------------------------------------------
# Name:         vMESSrvpluginFind.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      200060708
# CVS-ID:       $Id: vMESSrvpluginFind.py,v 1.6 2007/08/27 05:31:34 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import wx
import os,os.path,sys
import string,thread,threading,time,Queue
import traceback,types

import vMESSrvplugin
import vidarc.tool.log.vtLog as vtLog

wxEVT_VMESPLUGIN_THREAD_PROC=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_PROC=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_PROC,1)
def EVT_VMESPLUGIN_THREAD_PROC(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_PROC,func)
class vMESpluginThreadProc(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_PROC(<widget_name>, self.OnProc)
    """
    def __init__(self,obj,iAct,iSize):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.iAct=iAct
        self.iSize=iSize
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_PROC)
    def GetAct(self):
        return self.iAct
    def GetSize(self):
        return self.iSize

wxEVT_VMESPLUGIN_THREAD_ADD=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD,1)
def EVT_VMESPLUGIN_THREAD_ADD(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD,func)
class vMESpluginThreadAdd(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD(<widget_name>, self.OnPlugInAdd)
    """
    def __init__(self,obj,oPlugIn):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.oPlugIn=oPlugIn
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD)
    def GetPlugIn(self):
        return self.oPlugIn

wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD_FINISHED=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED,1)
def EVT_VMESPLUGIN_THREAD_ADD_FINISHED(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED,func)
class vMESpluginThreadAddFinished(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD_FINISHED(<widget_name>, self.OnPlugInFin)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD_FINISHED)
    
wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED=wx.NewEventType()
vEVT_VMESPLUGIN_THREAD_ADD_ABORTED=wx.PyEventBinder(wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED,1)
def EVT_VMESPLUGIN_THREAD_ADD_ABORTED(win,func):
    win.Connect(-1,-1,wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED,func)
class vMESpluginThreadAddAborted(wx.PyEvent):
    """
    Posted Events:
        Tree Item selected event
            EVT_VMESPLUGIN_THREAD_ADD_ABORTED(<widget_name>, self.OnPlugInAbort)
    """
    def __init__(self,obj):
        wx.PyEvent.__init__(self)
        self.SetEventObject(obj)
        self.SetId(obj.GetId())
        self.SetEventType(wxEVT_VMESPLUGIN_THREAD_ADD_ABORTED)

class thdMESpluginFind:
    DIR_2_SKIP=['CVS','locale','build','dist']
    def __init__(self,par,verbose=-1):
        self.par=par
        self.lDN=[]
        self.doc=None
        self.oRoot=None
        self.qPlugIn=Queue.Queue()
        self.running = False
        self.keepGoing = False
        self.verbose=verbose
        self.updateProcessbar=par is not None
    def SetParent(self,par):
        self.par=par
        self.updateProcessbar=par is not None
    def GetPlugInDict(self):
        d={}
        while not self.qPlugIn.empty():
            t=self.qPlugIn.get()
            d[t.GetName()]=t
        return d
    def Find(self,lDN,doc,oRoot,func=None,*args,**kwargs):
        #vtLog.vtLngCur(vtLog.INFO,'start plugin search','thdMESpluginFind')
        #vtLog.vtLngCur(vtLog.INFO,'start plugin search:%s'%(vtLog.pformat(lDN)),'thdMESpluginFind')
        self.running = True
        self.keepGoing = True
        self.lDN=lDN
        self.doc=doc
        self.oRoot=oRoot
        self.zStart=time.clock()
        self.func=func
        self.args=args
        self.kwargs=kwargs
        self.Start()
        #self.Run()
        #vtLog.vtLngCur(vtLog.INFO,'start plugin thread start','thdMESpluginFind')
    def Start(self):
        self.keepGoing = self.running = True
        thread.start_new_thread(self.Run, ())
    def Stop(self):
        self.keepGoing = False
    def IsRunning(self):
        return self.running
    def __getImport__(self,fn):
        try:
            sfn=fn.replace('\\','/')
            bLower=False
            if sys.platform=='win32':
                bLower=True
                sfn=sfn.lower()
            #else:
            #    fn=fn
            for dn in sys.path:
                sdn=dn.replace('\\','/')
                if bLower:
                    sdn=sdn.lower()
                #else:
                #    sdn=dn
                if sfn.find(sdn)==0:
                    iLen=len(dn)
                    if not dn[-1] in ['/','\\']:
                        iLen+=1
                    name=fn[iLen:]
                    name=name.replace('/','.')
                    name=name.replace('\\','.')
                    return name
            vtLog.vtLngCur(vtLog.WARN,'fn:%s could not be resolved by means of sys.path'%(fn),self)
        except:
            vtLog.vtLngTB('thdMESpluginFind')
        return None
    def __findPluginVid__(self,dn):
        try:
            base,name=os.path.split(dn)
            if name in self.DIR_2_SKIP:
                return -1
            lFiles=os.listdir(dn)
            lFiles.sort()
            lFiles.reverse()
            lPlugIns=[]
            for fn in lFiles:
                strs=fn.split('.')
                if strs[-1]=='vidarc':
                    sPlugIn='.'.join(strs[:-4])
                    if sPlugIn not in lPlugIns:
                        lPlugIns.append(sPlugIn)
            bPlugInFound=False
            for sPlugIn in lPlugIns:
                sImp='%s.__init__'%sPlugIn
                try:
                    plgin=__import__(sImp,[],[],['PLUGABLE_SRV'])
                    try:
                        if type(plgin.PLUGABLE_SRV)==types.ListType:
                            lPlgInNames=plgin.PLUGABLE_SRV
                        else:
                            lPlgInNames=[plgin.PLUGABLE_SRV]
                    except:
                        del plgin
                        vtLog.vtLngCurCls(vtLog.WARN,'plugin:%s'%(sImp),self)
                        continue
                    try:
                        try:
                            plginOrder=__import__(sImp,[],[],['PLUGABLE_SRV_ORDER'])
                            if type(plginOrder.PLUGABLE_SRV_ORDER)==types.ListType:
                                lPlgInOrder=plginOrder.PLUGABLE_SRV_ORDER
                            else:
                                lPlgInOrder=[plginOrder.PLUGABLE_SRV_ORDER]
                        except:
                            lPlgInOrder=[0 for sName in lPlgInNames]
                        for sName,iOrder in zip(lPlgInNames,lPlgInOrder):
                        #sName=plgin.PLUGABLE_SRV
                            strs=sName.split('@')
                            sPlgInName=strs[0]
                            sPlgInSrv=None
                            sPlgInSub=None
                            if len(strs)>2:
                                sPlgInSrv=strs[1]
                                sPlgInSub=strs[2]
                            elif len(strs)>1:
                                sPlgInSrv=strs[1]
                            #if len(strs)>1:
                            #    sPlgInSrv=strs[1]
                            #else:
                            #    sPlgInSrv=None
                            bPlugInFound=True
                            if len(sPlgInName):
                                if self.verbose&1:
                                    print '__findPlugin__','found',sPlgInName
                                oPlugIn=vMESSrvplugin.vMESSrvplugin(self.doc,self.oRoot,
                                                sName,sPlgInSrv,sPlgInSub,iOrder,sPlgInName,sPlugIn+'.'+sPlgInName,dn)
                                self.qPlugIn.put(oPlugIn)
                                if self.par is not None:
                                    wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,oPlugIn))
                    except:
                        del plgin
                        vtLog.vtLngCurCls(vtLog.ERROR,'plugin:%s'%(sImp),self)
                        vtLog.vtLngTB(self.__class__.__name__)
                except:
                    #vtLog.vtLngCurCls(vtLog.ERROR,'plugin:%s'%(sImp),self)
                    #vtLog.vtLngTB(self.__class__.__name__)
                    pass
        except:
            try:
                vtLog.vtLngTB(self.par.GetName())
                vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s no init found here'%dn,self.par)
            except:
                pass
        return -1
    def __findPluginSrc__(self,dn):
        try:
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCurCls(vtLog.DEBUG,'dn:%s'%(dn),self)
            sImp=self.__getImport__(dn+'.__init__')
            if sImp is not None:
                try:
                    plgin=__import__(sImp,[],[],['PLUGABLE_SRV'])
                    vtLog.vtLngCurCls(vtLog.INFO,'plugin:%s'%(sImp),self)
                    try:
                        if type(plgin.PLUGABLE_SRV)==types.ListType:
                            lPlgInNames=plgin.PLUGABLE_SRV
                        else:
                            lPlgInNames=[plgin.PLUGABLE_SRV]
                        try:
                            plginOrder=__import__(sImp,[],[],['PLUGABLE_SRV_ORDER'])
                            if type(plginOrder.PLUGABLE_SRV_ORDER)==types.ListType:
                                lPlgInOrder=plginOrder.PLUGABLE_SRV_ORDER
                            else:
                                lPlgInOrder=[plginOrder.PLUGABLE_SRV_ORDER]
                        except:
                            lPlgInOrder=[0 for sName in lPlgInNames]
                        for sName,iOrder in zip(lPlgInNames,lPlgInOrder):
                        #for sName in lPlgInNames:
                        #sName=plgin.PLUGABLE_SRV
                            strs=sName.split('@')
                            sPlgInName=strs[0]
                            sPlgInSrv=None
                            sPlgInSub=None
                            if len(strs)>2:
                                sPlgInSrv=strs[1]
                                sPlgInSub=strs[2]
                            elif len(strs)>1:
                                sPlgInSrv=strs[1]
                            if len(sPlgInName):
                                vtLog.vtLngCurCls(vtLog.INFO,'add plugin:%s'%(sPlgInName),self)
                                if self.verbose&1:
                                    print '__findPlugin__','found',sPlgInName
                                oPlugIn=vMESSrvplugin.vMESSrvplugin(self.doc,self.oRoot,
                                                sName,sPlgInSrv,sPlgInSub,iOrder,sPlgInName,self.__getImport__(dn+'.'+sPlgInName),dn)
                                self.qPlugIn.put(oPlugIn)
                                if self.par is not None:
                                    wx.PostEvent(self.par,vMESpluginThreadAdd(self.par,oPlugIn))
                    except:
                        vtLog.vtLngCurCls(vtLog.INFO,'no plugin:%s'%(sImp),self)
                        #traceback.print_exc()
                        del plgin
                    return 0
                except:
                    pass
                return -1
        except:
            vtLog.vtLngTB(self.par.GetName())
            vtLog.vtLngCurWX(vtLog.DEBUG,'dir:%s no init found here'%dn,self.par)
            pass
        return -1
    def __walkDir__(self,dn):
        try:
            if self.verbose&4:
                print '__walkDir__',dn
            if self.keepGoing==False:
                return
            iRet=self.__findPlugin__(dn)
            if iRet<0:
                if self.updateProcessbar:
                    self.iAct+=self.__walkDirCalc__(dn)
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                return
            files=os.listdir(dn)
            for fn in files:
                if self.verbose&4:
                    print '__walkDir__',dn,fn
                if self.updateProcessbar:
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                sFullFN=os.path.join(dn,fn)
                if os.path.isdir(sFullFN):
                    self.__walkDir__(sFullFN)
        except:
            traceback.print_exc()
    def __walkDirCalc__(self,dn):
        try:
            if self.verbose&2:
                print '__walkDirCalc__',dn
            iSize=0
            if self.keepGoing==False:
                return iSize
            files=os.listdir(dn)
            iSize=len(files)
            for fn in files:
                sFullFN=os.path.join(dn,fn)
                if os.path.isdir(sFullFN):
                    iSize+=self.__walkDirCalc__(sFullFN)
        except:
            traceback.print_exc()
        return iSize
    def Run(self):
        try:
            self.running = True
            self.lSrcDN=[]
            self.lVidDN=[]
            vtLog.vtLngCur(vtLog.INFO,'start plugin search:%s'%(vtLog.pformat(self.lDN)),'thdMESpluginFind')
            for dn in self.lDN:
                for root , dirs,files in os.walk(dn):
                    for dnExcl in self.DIR_2_SKIP:
                        if dnExcl in dirs:
                            dirs.remove(dnExcl)
                    for fn in files:
                        if fn.endswith('.vidarc'):
                            self.lVidDN.append(root)
                            break
                        elif fn.startswith('__init__.'):
                            self.lSrcDN.append(root)
                            break
                #self.__walkDir__(dn)
            if self.updateProcessbar:
                self.iAct=0
                #self.iSize=0
                #for dn in self.lDN:
                #    self.iSize+=self.__walkDirCalc__(dn)
                self.iSize=len(self.lSrcDN)+len(self.lVidDN)
                wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
            vtLog.vtLngCur(vtLog.INFO,'start plugin search finished','thdMESpluginFind')
            if vtLog.vtLngIsLogged(vtLog.DEBUG):
                vtLog.vtLngCur(vtLog.DEBUG,'src:%s;vid:%s'%
                            (vtLog.pformat(self.lSrcDN),
                            vtLog.pformat(self.lVidDN)),'thdMESpluginFind')
            for dn in self.lSrcDN:
                if self.keepGoing==False:
                    break
                if self.updateProcessbar:
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                self.__findPluginSrc__(dn)
            for dn in self.lVidDN:
                if self.keepGoing==False:
                    break
                if self.updateProcessbar:
                    self.iAct+=1
                    wx.PostEvent(self.par,vMESpluginThreadProc(self.par,self.iAct,self.iSize))
                self.__findPluginVid__(dn)
            if self.keepGoing:
                if self.func is not None:
                    self.func(*self.args,**self.kwargs)
                if self.updateProcessbar:
                    wx.PostEvent(self.par,vMESpluginThreadAddFinished(self.par))
            else:
                if self.updateProcessbar:
                    wx.PostEvent(self.par,vMESpluginThreadAddAborted(self.par))
        except:
            vtLog.vtLngTB('thdMESpluginFind')
        self.running = False

