#----------------------------------------------------------------------------
# Name:         vMESXmlNodeSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060709
# CVS-ID:       $Id: vMESXmlNodeSrv.py,v 1.2 2007/07/30 20:39:09 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

from vidarc.vSrv.net.vsNetXmlNodeSrv import vsNetXmlNodeSrv

class vMESXmlNodeSrv(vsNetXmlNodeSrv):
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50098)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50099)
    def __procAliases__(self,node,dataInst,appl,bStart):
        if vtLog.vtLngIsLogged(vtLog.INFO):
            vtLog.vtLngCurCls(vtLog.INFO,'appl:%s;appl:%s;id:%s'%(appl,bStart,self.doc.getKey(node)),self)
        oReg=self.doc.GetRegByNode(node)
        if hasattr(oReg,'GetDoc2Serve'):
            if hasattr(oReg,'HasChilds2Serve'):
                self.doc.procChildsKeys(node,self.__procAliases__,dataInst,oReg.GetTag(node),bStart)
                return 0
            alias=oReg.GetTag(node)
            applAlias=':'.join([appl,alias])
            if bStart:
                doc=oReg.GetDoc2Serve(node)
                fn=oReg.GetFN(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'fn:%s;doc:%s'%(fn,doc),self)
                hosts=oReg.GetHosts(node)
                if vtLog.vtLngIsLogged(vtLog.DEBUG):
                    vtLog.vtLngCurCls(vtLog.DEBUG,'host:%s'%(vtLog.pformat(hosts)),self)
                dataInst.AddServedXml(applAlias,fn,doc,[])
                dataInst.SetServedHosts(applAlias,hosts)
            else:
                dataInst.DelServedXml(applAlias)
        return 0
    def Start(self,node,dataInst):
        appl=self.GetTag(node)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,True)
    def Stop(self,node,dataInst):
        appl=self.GetTag(node)
        self.doc.procChildsKeys(node,self.__procAliases__,dataInst,appl,False)
