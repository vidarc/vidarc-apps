#----------------------------------------------------------------------------
# Name:         vXmlNodeMESSrv.py
# Purpose:      
#
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: vXmlNodeMESSrv.py,v 1.2 2006/08/29 10:06:21 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
import vidarc.config.vcCust as vcCust
import vidarc.tool.lang.vtLgBase as vtLgBase

from vidarc.vSrv.net.vsNetXmlNodeXmlSrv import vsNetXmlNodeXmlSrv
try:
    if vcCust.is2Import(__name__):
        from vidarc.vSrv.net.vsNetXmlNodeSrvPanel import vsNetXmlNodeSrvPanel
        from vidarc.vSrv.net.vsNetXmlNodeSrvEditDialog import vsNetXmlNodeSrvEditDialog
        from vidarc.vSrv.net.vsNetXmlNodeSrvAddDialog import vsNetXmlNodeSrvAddDialog
        GUI=1
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects imported;%s'%__name__,'import')
    else:
        GUI=0
        vtLog.vtLngCur(vtLog.DEBUG,'GUI objects import skipped;%s'%__name__,'import')
except:
    vtLog.vtLngTB('import')
    GUI=0

class vXmlNodeMESSrv(vsNetXmlNodeXmlSrv):
    def __init__(self,tagName='vMESSrv'):
        global _
        _=vtLgBase.assignPluginLang('vMESSrv')
        vsNetXmlNodeXmlSrv.__init__(self,tagName)
    def GetDescription(self):
        return _(u'MES server')
    # ---------------------------------------------------------
    # specific
    def GetPortData(self,node):
        return self.GetVal(node,'portData',int,50098)
    def GetPortService(self,node):
        return self.GetVal(node,'portService',int,50099)
    def GetHosts(self,node):
        nodeHost=self.doc.getChild(node,'Hosts')
        if nodeHost is None:
            return []
        oReg=self.doc.GetReg('Hosts')
        return oReg.GetHosts(nodeHost)
    def GetServerCls(self):
        from vidarc.tool.net.vtNetSrv import vtNetSrv
        return vtNetSrv
    def GetDataSrvCls(self):
        from vidarc.tool.net.vtNetXml import vtSrvXmlSock
        return vtSrvXmlSock
    def GetDataCls(self):
        from vidarc.tool.net.vtNetXml import vtNetXmlSock
        return vtNetXmlSock
    def GetServiceSrvCls(self):
        from vidarc.tool.net.vtNetSrv import vtSrvSock
        return vtSrvSock
    def GetServiceCls(self):
        from vidarc.tool.net.vtNetSrv import vtServiceSock
        return vtServiceSock
    def __procStartServers__(self,node,o):
        sTag=self.doc.getTagName(node)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'tagName:%s;oReg:%s'%(sTag,o.GetTagName()),self)
        if sTag==o.GetTagName():
            try:
                netSrv=o.StartServer(node)
                srvData=netSrv.GetDataInstance()
                #o.Start(node,srvData)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __procStopServers__(self,node,o):
        sTag=self.doc.getTagName(node)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'tagName:%s;oReg:%s'%(sTag,o.GetTagName()),self)
        if sTag==o.GetTagName():
            try:
                #netSrv=self.doc.GetServerInstanceByNode(node)
                #srvData=netSrv.GetDataInstance()
                #o.Stop(node,srvData)
                o.StopServer(node)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __procSetupServers__(self,node,o):
        sTag=self.doc.getTagName(node)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'tagName:%s;oReg:%s'%(sTag,o.GetTagName()),self)
        if sTag==o.GetTagName():
            try:
                o.SetupServer(node)
                #srvData=netSrv.GetDataInstance()
                #o.Start(node,srvData)
            except:
                vtLog.vtLngTB(self.__class__.__name__)
        return 0
    def __getSrvSortedLst__(self):
        l=[]
        dPlugIn=self.doc.GetPlugInDict()
        for k,oPlugIn in dPlugIn.items():
            if oPlugIn.IsServer():
                o=self.doc.GetReg(k)
                if o is not None:
                    l.append((oPlugIn.GetOrder(),k,oPlugIn,o))
        l.sort()
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'lSrv:%s'%(vtLog.pformat(l)),self)
        return l
    def Start(self,node,dataInst):
        import vXmlMESSrv
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        appl=self.GetTag(node)
        hosts=self.GetHosts(node)
        if vtLog.vtLngIsLogged(vtLog.DEBUG):
            vtLog.vtLngCurCls(vtLog.DEBUG,'fn:%s;lHost:%s'%(self.doc.GetFN(),vtLog.pformat(hosts)),self)
        alias=self.GetTag(node)
        applAlias=':'.join([self.GetTagName(),alias])
        doc=vXmlMESSrv.vXmlMESSrv(appl=alias)
        doc.BuildPlugInDict(self.doc.GetPlugInDict())
        dataInst.AddServedXml(applAlias,self.doc.GetFN(),doc,[])
        dataInst.SetServedHosts(applAlias,hosts)
        lSrv=self.__getSrvSortedLst__()
        for iOrder,k,oPlugIn,o in lSrv:
            self.doc.procChildsKeys(node,self.__procStartServers__,o)
            self.doc.procChildsKeys(node,self.__procSetupServers__,o)
        return
        dPlugIn=self.doc.GetPlugInDict()
        for k,oPlugIn in dPlugIn.items():
            if oPlugIn.IsServer():
                o=self.doc.GetReg(k)
                if o is not None:
                    self.doc.procChildsKeys(node,self.__procStartServers__,o)
                    self.doc.procChildsKeys(node,self.__procSetupServers__,o)
                    #self.netSrv=o.StartServer(self.node)
    def Stop(self,node,dataInst):
        vtLog.vtLngCurCls(vtLog.INFO,'',self)
        alias=self.GetTag(node)
        applAlias=':'.join([self.GetTagName(),alias])
        dataInst.SaveXml(applAlias)
        dataInst.DelServedXml(applAlias)
        lSrv=self.__getSrvSortedLst__()
        lSrv.reverse()
        for iOrder,k,oPlugIn,o in lSrv:
            self.doc.procChildsKeys(node,self.__procStopServers__,o)
        return
        dPlugIn=self.doc.GetPlugInDict()
        for k,oPlugIn in dPlugIn.items():
            if oPlugIn.IsServer():
                o=self.doc.GetReg(k)
                if o is not None:
                    self.doc.procChildsKeys(node,self.__procStopServers__,o)
    # ---------------------------------------------------------
    # inheritance
    def getImageData(self):
        return \
'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x10\x00\x00\x00\x10\x08\x06\
\x00\x00\x00\x1f\xf3\xffa\x00\x00\x00\x04sBIT\x08\x08\x08\x08|\x08d\x88\x00\
\x00\x01LIDAT8\x8d\xa5\x93\xddM\xc30\x14\x85?\xa7\x1d\x80\x19*\xf5=\x0e\xf2$\
LP\x99\x08\x89\x11\x18\x80\x1d\x88\x92\x05\xbaJ\x84\x1d\xf1Z)]\xa1\x03\x14\
\x0e\x0f\xf9/T\x80\xb8\xd1\x95\xae\x14\x9f{~,\x1b\x93\xac\xf8O\xad\xaf\xfd\
\x90?\x0b"8\x0bu\x04\xc0T\xce\xfc\xb8@\xfe,\\7\x87\xdc\x02\x90\xd5\x16\x88\
\xbc\xbe\x9f\x05p\xbbZO\x8bL\xb2\x1a\x9b<\x88B]\xcf\xbe )H\xb2\x11\x85\xc2\
\x8b=\x1a0\xc9\x82\xdeu\x8c\xdcL\x04\x01As\x8fo\x0c\x15\x1e\xef*\xecv\x86\
\x19\xd9\x0bi\xa1`\x8f\xd8#\x9b\xf6*\xa2\x97\x8d\xddl#\xa3\x8aIA\xbd\x0c\x8c\
\x93\xe0$\xe2\x13X\x0bYZ\xc1\x0el\x03\xf10\t\x98-\x18\x06\x8bu\x06\xf2\xbe\
\x81\xea\xd1c{w\xec\xfaS\xcf\x97\x16\xf2 \xd2 \x1bQ{l\x15\xa2W(\xbc\xdac;YI\
\x83H\xcbq\xbe\xb0\x008\x88\xbb\xc0\xddiCv\xa8\x18\xae\x13 \xba\xd0O\x19\xb1\
)\xa1\tK\x0b\xe6\xcd\x99\xf9\x92\xa16\xf5\xa6\x93[ww\x02\x01R0\xc9\x83Yf0*\
\xe9\xc0vK\xa7\x02\x88\x94=\x18h2h\xbe\x0bqP1H\xde\x95SXM6\x81\t#;\x80\xb9\
\xf6\x98\xf4\xf1"\x00RF\xc69\xf0\xc7\x05\xbf\xad\xaf\x19\xfc\xb1>\x01\'\xd5\
\xa8A\x03\x84\x96\xda\x00\x00\x00\x00IEND\xaeB`\x82' 
    def GetEditDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvEditDialog
        else:
            return None
    def GetAddDialogClass(self):
        if GUI:
            return vsNetXmlNodeSrvAddDialog
        else:
            return None
    def GetPanelClass(self):
        if GUI:
            return vsNetXmlNodeSrvPanel
        else:
            return None
