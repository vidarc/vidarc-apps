#----------------------------------------------------------------------------
# Name:         __register__.py
# Purpose:      
# Author:       Walter Obweger
#
# Created:      20060708
# CVS-ID:       $Id: __register__.py,v 1.3 2014/03/30 12:54:19 wal Exp $
# Copyright:    (c) 2006 by VIDARC Automation GmbH
# Licence:      VIDARC license
#----------------------------------------------------------------------------

import vidarc.tool.log.vtLog as vtLog
from vXmlNodeMESSrv import vXmlNodeMESSrv
#from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrvDir import vXmlNodePlugInSrvDir
#from vidarc.vSrv.vPlugInSrv.vXmlNodePlugInSrvDirSub import vXmlNodePlugInSrvDirSub
from vidarc.vSrv.net.vXmlNodeHosts import vXmlNodeHosts

def RegisterNodes(self,oRoot,bRegAsRoot=True):
    try:
        oMESSrv=vXmlNodeMESSrv()
        #oPlugInSrvDir=vXmlNodePlugInSrvDir()
        #oPlugInSrvSub=vXmlNodePlugInSrvDirSub()
        oHosts=vXmlNodeHosts()
        
        self.RegisterNode(oMESSrv,bRegAsRoot)
        if bRegAsRoot:
            oRoot=oMESSrv
        self.RegisterNode(oHosts,False)
        #self.RegisterNode(oPlugInSrvDir,False)
        #self.RegisterNode(oPlugInSrvSub,False)
        
        self.LinkRegisteredNode(oRoot,oMESSrv)
        #self.LinkRegisteredNode(oRoot,oHosts)
        self.LinkRegisteredNode(oMESSrv,oHosts)
        self.LinkRegisteredNode(oRoot,oMESSrv)
        #self.LinkRegisteredNode(oMESSrv,oMESSrv)
        #self.LinkRegisteredNode(oPlugInSrv,oPlugInSrvDir)
        #self.LinkRegisteredNode(oPlugInSrvDir,oPlugInSrvSub)
        #self.LinkRegisteredNode(oPlugInSrvSub,oPlugInSrvSub)
    except:
        vtLog.vtLngTB(__name__)
