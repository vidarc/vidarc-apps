# vidarc-apps

## dependencies

Python 2 is required  
use of anaconda recommended.

packages used:

+ [wxPython](https://www.wxpython.org/)

run following command with sufficient permissions

```shell
pip install -r requirements.txt
```

wxPython need a bit more work to get on your system,
please consult [installation](https://wiki.wxpython.org/How%20to%20install%20wxPython) manual.
supported linux distribution can be found [here](https://extras.wxpython.org/wxPython4/extras/linux/gtk3/).

on CentOS 8

```shell
pip3 install -U     -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/centos-8     wxPython
```

also consider installing wheel

```shell
pip install wheel
```

## build

images are converted into embedded python script by `wx.tools.img2py`.
bulk convert can be performed by executing `encode_bitmaps.py` in workspace root. 

```shell
python encode_bitmaps.py med
```

argument `<x>` select `encode_<x>.py` to be processed.

```python
command_lines = [
    "-u -i -n BtnGn00       v_img/baseGn00a.ico            lindworm/ldmWidImgMed.py",
    "-a -u -n BtnGn01       v_img/baseGn01a.ico            lindworm/ldmWidImgMed.py",
  ]
```

## install

run `setup.py` in project root folder,
e.g. following line to create binary wheel file.

```shell
python setup.py bdist_wheel
```

generated wheel file reside in folder `./dist`:  
`vidarcMES-x.y.z-py3-none-any.whl`
assuming `vidarcMES/__init__.py` has variable `__version__` set.

```python
__version__ = "x.y.z"
```

```shell
pip install ./dist/vidarcMES-x.y.z-py3-none-any.whl
```

source install

```shell
python setup.py install
```

## uninstall

in case version number is not changed, pip will not install
update, therefore package can be uninstalled.

```shell
pip uninstall ./dist/vidarcMES-x.y.z-py3-none-any.whl
```

## pypi

steps to perform on linux (CentOS) to
publish on [pypi](https://pypi.org/project/vidarcMES/).

```shell
python3 setup.py sdist bdist_wheel
twine upload dist/*
```

## setup

+ vscode
  set `Python: Env File` to `${workspaceFolder}/dlp.env`
  ![vscode settings](./e_scr/vscode_20191230_094105.png)

`dlp.env` add current workspace folder to python search path,
therefore enable package import without having to install in advance.
Very useful for performing unit tests.

```shell
PYTHONPATH=${workspaceFolder}:${PYTHONPATH}
```
